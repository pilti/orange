@Library('pipeline-library') _

stage("Foundation Service Check"){
            Generic_BuildJob(jobName: '00A_AISP_Sanity', Runner: 'Runner_FS_Shakedown', Env: 'SIT')
}

stage("Sanity Test - AISP") {
    parallel(
            AISP_Role: {
                Generic_BuildJob(jobName: '00A_AISP_Sanity', Runner: 'Runner_AISP_Functional_AISP', Env: 'SIT')
            },
            AISP_PISP_Role: {
                Generic_BuildJob(jobName: '00A_AISP_Sanity', Runner: 'Runner_AISP_Functional_AISP_PISP', Env: 'SIT')
            }

    )
}
