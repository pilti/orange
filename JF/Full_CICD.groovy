@Library('pipeline-library') _

class Result {
    static int totalpass = 0
    static int totalfail = 0
}

// Set Properties
properties([
        parameters([
                choice(
                        choices: 'SIT\nUAT',
                        description: 'Select Environment to Run Test',
                        name: 'Env'
                ),
        ])
])

stage("${params.Env} - Code deployment") {

    parallel(
            FoundationService: {
                node {

                    // Generic_BuildJob('FS_Deployment')
                }
            },
            APIProduct: {
                node {

                }
            }
    )
}
stage("FS Tests in ${params.Env}") {

    node {


    }
}

stage("API Tests in ${params.Env}") {

    node {

    }
}

stage("${params.Env}  TPP Hub") {

    node {

        parallel(
                "Chrome": {
                    //Generic_BuildJob(jobName: '001_TPP_Full', Env: params.Env)
                },
                "IE": {
                    // Generic_BuildJob(jobName: '001_TPP_Full', Env: params.Env)
                }
        )
    }
}

stage("E2E Test - OIDC") {

    parallel(
            AISP_CC_Token: {
                node {
                    Generic_BuildJob(jobName: '002_AISP_CC_Token', Env: params.Env)
                }
            },
            AISP_AccountRequest: {
                node {
                    Generic_BuildJob(jobName: '003_AISP_AccountRequest', Env: params.Env)
                }
            },
            AISP_RefreshTokenRenewal: {
                node {
                    Generic_BuildJob(jobName: '017_AISP_RefreshtokenRenewal', Env: params.Env)
                }
            },
            AISP_PreAuth: {
                node {
                    Generic_BuildJob(jobName: '004_AISP_PreAuth', Env: params.Env)
                }
            }
    )
}

stage("E2E Test - SCA & Consent") {


    parallel(
            "Chrome": {
                node {
                    Generic_BuildJob(jobName: '005_AISP_SCA', Env: params.Env)
                    // Generic_BuildJob(jobName: '006_AISP_Consent', Env: params.Env)
                }
            },
            //  "IE": {
            //      Generic_BuildJob(jobName: '005_AISP_SCA', Env: params.Env)
            //      Generic_BuildJob(jobName: '006_AISP_Consent', Env: params.Env)
            //  }
    )
}


stage("E2E Test - ABT") {

    parallel(
            AISP_SingleAccountInfo: {
                node {
                    Generic_BuildJob(jobName: '010_AISP_SingleAccountInfo', Env: params.Env)
                }
            },
            AISP_MultiAccountInfo: {
                node {
                    Generic_BuildJob(jobName: '009_AISP_MultiAccountInfo', Env: params.Env)
                }
            },
            AISP_Balance: {
                node {
                    Generic_BuildJob(jobName: '011_AISP_AccountBalance', Env: params.Env)
                }
            },
            AISP_Transactions: {
                node {
                    Generic_BuildJob(jobName: '011_AISP_AccountBalance', Env: params.Env)
                }
            }
    )
}

stage("E2E Test - Other") {

    parallel(

            AISP_DirectDebits: {
                node {
                    Generic_BuildJob(jobName: '013_AISP_DirectDebits', Env: params.Env)
                }
            },
            AISP_Beneficiaries: {
                node {
                    Generic_BuildJob(jobName: '016_AISP_Beneficiaries', Env: params.Env)
                }
            },
            AISP_StandingOrder: {
                node {
                    Generic_BuildJob(jobName: '014_AISP_StandingOrders', Env: params.Env)
                }
            },
            AISP_Products: {
                node {
                    Generic_BuildJob(jobName: '015_AISP_AccountProducts', Env: params.Env)
                }
            }

    )
}

stage("Early NFT") {
    parallel(
            "HP Performance Center": {
                node {
                    // Generic_BuildJob('PSD2 Performance Integration')
                }
            },
            "Security": {
                node {
                    //Generic_BuildJob('')
                }
            }
    )
}

stage("Overall Test Result Summary") {
    node {
        print "Overall Pass : " + Result.totalpass
        print "Overall Fail : " + Result.totalfail
        //Generic_BuildJob('')
    }
}
stage("Review Test Results") {

    def userInput = true
    def didTimeout = false
    try {
        timeout(time: 2500, unit: 'SECONDS') { // change to a convenient timeout for you
            userInput = input(
                    id: 'Proceed1', message: 'Test Results are Satisfactory?', parameters: [
                    [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Please confirm you agree with this']
            ])
        }
    } catch (err) { // timeout reached or input false
        def user = err.getCauses()[0].getUser()
        if ('SYSTEM' == user.toString()) { // SYSTEM means timeout.
            didTimeout = true
        } else {
            userInput = false
            echo "Aborted by: [${user}]"
        }
    }
    node {
        if (didTimeout) {
            // do something on timeout
            echo "No input was received before timeout"
        } else if (userInput == true) {
            // do something
            echo "Yes. Test Results Satisfactory"
        } else {
            // do something else
            echo "No. Test Results are not Satisfactory"
            currentBuild.result = 'FAILURE'
        }
    }
}
stage("Pre-Prod Deployment?") {

    try {

        timeout(time: 1500, unit: 'SECONDS') { // change to a convenient timeout for you
            userInput = input(
                    id: 'Proceed1', message: 'Was this successful?', parameters: [
                    [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Please confirm you agree with this']
            ])
        }
    } catch (err) { // timeout reached or input false
        def user = err.getCauses()[0].getUser()
        if ('SYSTEM' == user.toString()) { // SYSTEM means timeout.
            didTimeout = true
        } else {
            userInput = false
            echo "Aborted by: [${user}]"
        }
    }

    node {
        if (didTimeout) {
            // do something on timeout
            echo "no input was received before timeout"
        } else if (userInput == true) {
            // do something
            echo "Proceed with Pre-Prod Deployment?"
        } else {
            // do something else
            echo "this was not successful"
            currentBuild.result = 'FAILURE'
        }
    }
}
stage("Full NFT") {
    parallel(
            "HP Performance Center": {
                node {
                    // Generic_BuildJob('PSD2 Performance Integration')
                }
            }
    )
}

