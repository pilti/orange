node() {

    script
            {
                // System.setProperty("hudson.model.DirectoryBrowserSupport.CSP","")
            }

    def mvnHome
    mvnHome = tool 'Maven 3.3.9'
    env.M2_HOME = "${mvnHome}"
    echo "Maven Home is : ${mvnHome}"
    try {
        stage('Checkout E2E Test Code') {
            cleanWs()
            git branch: 'develop_2', url: 'https://bitbucket.boigroup.net/scm/chn/e2e.git'
        }
        stage('Build and Test Application') {
            mavenSettingsConfig: 'global-maven-settings'
            if (isUnix()) {
                sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
            } else {
                bat ipconfig
                // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\ojdbc6.jar" -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar /)
                // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\phantomjsdriver.jar" -DgroupId=com.codeborne -DartifactId=phantomjsdriver -Dversion=1.4.1 -Dpackaging=jar /)
                // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\gson.jar" -DgroupId=com.google.code.gson -DartifactId=gson -Dversion=1.4.4 -Dpackaging=jar /)
                bat(/"${mvnHome}\bin\mvn" -Dtest=MainRunner clean compile test /)
            }
        }
    } catch (err) {
        //notify("Error ${err}")
        currentBuild.result = 'FAILURE'
    }
    stage('Publish Cucumber Test Results') {
        cucumber fileIncludePattern: '**/*.json',
                jsonReportDirectory: 'target/surefire-reports',
                sortingMethod: 'ALPHABETICAL'
    }
    stage('Publish Business Test Results') {
        publishHTML(target: [allowMissing: true,
                             alwaysLinkToLastBuild: false,
                             keepAll: true,
                             reportDir: 'target/D3_business_viewer/',
                             reportFiles: 'index.html',
                             reportName: 'Business Test results'])
    }
    //stage('Generate Allure reports') {
    //script {
    //        allure([
    //        includeProperties: false,
    //        jdk: '',
    //        properties: [],
    //        reportBuildPolicy: 'ALWAYS',
    //        results: [[path: 'target/surefire-reports']]
    //            ])
    //}

    stage('Archive') {
        archive '/**/*.*'
    }
}
