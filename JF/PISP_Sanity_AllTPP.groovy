@Library('pipeline-library') _

stage("Sanity Test - PISP") {
    parallel(
            PISP_Role: {
                Generic_BuildJob(jobName: '00A_PISP_Sanity', Runner: 'Runner_PISP_Functional_PISP', Env: 'SIT')
            },
            AISP_PISP_Role: {
                Generic_BuildJob(jobName: '00A_PISP_Sanity', Runner: 'Runner_PISP_Functional_AISP_PISP', Env: 'SIT')
            }
    )

}
