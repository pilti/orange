node('!master') {
    script
            {
                // System.setProperty("hudson.model.DirectoryBrowserSupport.CSP","")
            }

    def mvnHome
    mvnHome = tool 'M3'
    try {
        stage("TPP Runner") {
            properties([
                    parameters([
                            choice(
                                    choices: 'Runner_AISP_Functional_AISP\nRunner_AISP_Functional_AISP_PISP\nRunner_AISP_Functional_PISP',
                                    description: 'Select TPP Runner',
                                    name: 'Runner'
                            ),
                            choice(
                                    choices: 'SIT\nUAT\npreProd',
                                    description: 'Select Environment to Run Test',
                                    name: 'Env'
                            )
                    ])
            ])
            print "Selected TPP runner : " + params.Runner
        }
       stage('Checkout E2E Test Code') {
       cleanWs()
            git branch: 'develop_2', url: 'https://bitbucket.boigroup.net/scm/chn/e2e.git'
        }
        stage("B365 User Credentials") {
            tout = false
            startMillis = System.currentTimeMillis()
            timeoutMillis = 120000
            B365User = 682833
            OTP = 123456

            try {
                timeout(time: timeoutMillis, unit: 'MILLISECONDS') {

                    userInput = input(id: 'userInput',
                            message: 'Enter B365 user ID',
                            parameters: [[$class      : 'TextParameterDefinition',
                                          defaultValue: "682833",
                                          name        : 'UserID',
                                          description : 'UserID']])
                    print "B356 USER_ID Provided: " + userInput
                    B365User = userInput

                    userInput1 = input(id: 'userInput',
                            message: 'Enter OTP :',
                            parameters: [[$class      : 'TextParameterDefinition',
                                          defaultValue: "123456",
                                          name        : 'OTP',
                                          description : 'OTP']])
                    print "B365 OTP : Provided" + userInput1
                    OTP = userInput1
                }
            } catch (e) {
                tout = true;
            }
            if (tout) {
                print "timedout using default"
            }
            print "B365UserID : " + B365User
            print "OTP : " + OTP
        }

        mavenSettingsConfig: 'global-maven-settings'
        if (isUnix()) {
            sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
        } else {
            // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\ojdbc6.jar" -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar /)
            // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\phantomjsdriver.jar" -DgroupId=com.codeborne -DartifactId=phantomjsdriver -Dversion=1.4.1 -Dpackaging=jar /)
            // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\gson.jar" -DgroupId=com.google.code.gson -DartifactId=gson -Dversion=1.4.4 -Dpackaging=jar /)
            // bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" -DfeaturesDir="src\test\java\CMA_Release\Test_set\Functional_Shakedown\AISP\01_CCTokenRequest.feature" test /)
            stage("AISP_Sanity_Test_AISP") {
                 bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" -Dtest=\u0024{params.Runner} -DB365_User=$B365User -DB365_OTP=$OTP -Dkarate.env=${params.Env} test /)
            }

        }

    } catch (err) {
        //notify("Error ${err}")
        currentBuild.result = 'FAILURE'
    }
    stage('Publish Cucumber Test Results') {
        cucumber fileIncludePattern: '**/*.json',
                jsonReportDirectory: 'target/surefire-reports',
                sortingMethod: 'ALPHABETICAL'
        print currentBuild.absoluteUrl + 'cucumber-html-reports/overview-features.html'
    }
    stage('Publish Business Test Results') {
        publishHTML(target: [allowMissing         : true,
                             alwaysLinkToLastBuild: false,
                             keepAll              : true,
                             reportDir            : 'target/D3_business_viewer/',
                             reportFiles          : 'index.html',
                             reportName           : 'Business Test results'])

        print currentBuild.absoluteUrl + 'Business_Test_results' + '/'

        step([$class     : 'JUnitResultArchiver',
              testResults: 'target/surefire-reports/TEST-*.xml'])

    }
    //stage('Generate Allure reports') {
    //script {
    //        allure([
    //        includeProperties: false,
    //        jdk: '',
    //        properties: [],
    //        reportBuildPolicy: 'ALWAYS',
    //        results: [[path: 'target/surefire-reports']]
    //            ])
    //}

    stage('Archive') {
        archive '/**/*.*'
        print currentBuild.absoluteUrl + 'artifact' + '/'
    }
}
