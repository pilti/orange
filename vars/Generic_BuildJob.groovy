#!/usr/bin/env groovy

def call(Map buildjobParams) {

    print "Generic buildjob called"
    print buildjobParams

    try {
        stage(buildjobParams.jobName) {
            node {
                def e2e = null
                if (buildjobParams.Runner == null) {
                    e2e = build job: buildjobParams.jobName,
                            propagate: false,
                            parameters: [
                                    string(name: 'Env', value: buildjobParams.Env)
                            ]
                } else {
                    e2e = build job: buildjobParams.jobName,
                            propagate: false,
                            parameters: [
                                    string(name: 'Runner', value: buildjobParams.Runner),
                                    string(name: 'Env', value: buildjobParams.Env)
                            ]
                }
                print 'Allocated Build Number : ' + e2e.getNumber()
                result = e2e.result
                if (result.equals("SUCCESS")) {
                    print 'Build and Test completed sucessfully for   : ' + buildjobParams.jobName + '-' + buildjobParams.Runner + ' Please Refer Reports below'
                    print 'Technical Report : ' + e2e.getAbsoluteUrl() + 'cucumber-html-reports/overview-features.html'
                    print 'Business Report : ' + e2e.getAbsoluteUrl() + 'Business_Test_results' + '/'
                    // print 'Allure Report : ' + e2e.getAbsoluteUrl() + 'allure' + '/'
                    currentBuild.result = 'SUCCESS'
                } else {
                    print 'Test Failure(s) Observed for   : ' + buildjobParams.jobName + '-' + buildjobParams.Runner + '  Please Refer Reports below'
                    print 'Technical Report : ' + e2e.getAbsoluteUrl() + 'cucumber-html-reports/overview-features.html'
                    print 'Business Report : ' + e2e.getAbsoluteUrl() + 'Business_Test_results' + '/'
                    // print 'Allure Report : ' + e2e.getAbsoluteUrl() + 'allure' + '/'
                    error 'FAIL'
                }

            }
        }
    } catch (e) {
        currentBuild.result = 'UNSTABLE'
    }
}