#!/usr/bin/env groovy

def call(Map pipelineParams) {

    print "Generic pipline called"
    print pipelineParams

    node(pipelineParams.node) {

        echo pipelineParams.scmUrl
        def workspace
        def mvnHome

        //Get Build URL
        String B_Url = BUILD_URL
        print B_Url

        //Get the workspace
        workspace = pwd()
        echo "The current workspace for the job is = ${workspace}"

        //Define the MAven version to be used
        if (B_Url.contains("http://VM000001318")){
            mvnHome = "d:\\maven"
        }else{
            mvnHome = tool 'Maven 3.3.9'
        }

        echo "Maven Home is : ${mvnHome}"

        //Define the Java to be used
        //jdk = tool name: 'jdk1.8.0_51'
        // echo "JDK instalation path is : ${jdk}"

        //scmURL
        echo "SCM URL is : ${pipelineParams.scmUrl}"

        try {
            stage("Test Run Details") {
                print "CMA Version ${pipelineParams.CMA_Rel_Ver}"
                print "Test Environment ${pipelineParams.Test_Env}"
            }
            stage('Checkout E2E Test Code') {
                //cleanWs()
                workspace = pwd()
                echo "The current workspace for the job is = ${workspace}"
                git branch: pipelineParams.branch, url: pipelineParams.scmUrl
            }
            stage(pipelineParams.TestName) {

                if (B_Url.contains("http://VM000001318")){
                    if (isUnix()) {
                        sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
                    } else {
                        echo "Maven Build and Test using runner : " + pipelineParams.Runner
                        bat(/"${mvnHome}\bin\mvn" -Dtest=${pipelineParams.Runner} -Dkarate.env=${pipelineParams.Test_Env} -DCMA_Rel_Ver=${pipelineParams.CMA_Rel_Ver} -Dselenium_grid=${pipelineParams.selenium_grid} test /)
                    }

                }else{
                if (isUnix()) {
                    sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
                } else {
                    echo "Install Oracle JDBC Driver"
                    bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" install:install-file -Dfile="C:\maven\Maven_local_setting\ojdbc6.jar" -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0 -Dpackaging=jar /)
                    echo "Maven Build and Test using runner : " + pipelineParams.Runner
                    bat(/"${mvnHome}\bin\mvn" --settings "C:\maven\Maven_local_setting\settings.xml" -Dtest=${pipelineParams.Runner} -Dkarate.env=${pipelineParams.Test_Env} -Dselenium_grid=${pipelineParams.selenium_grid} -DCMA_Rel_Ver=${pipelineParams.CMA_Rel_Ver} test /)
                }}
            }

        } catch (err) {
            currentBuild.result = 'FAILURE'
        }

        stage('JUnitResultArchiver'){
            step([$class     : 'JUnitResultArchiver',
                  testResults: 'target/surefire-reports/TEST-*.xml'])}

        stage('Publish Cucumber Test Results') {
          cucumber fileIncludePattern: '**/*.json',
          jsonReportDirectory: 'target/surefire-reports',
          sortingMethod: 'ALPHABETICAL'
          print currentBuild.absoluteUrl + 'cucumber-html-reports/overview-features.html'
       }


        stage('Publish Business Test Results') {
            publishHTML(target: [allowMissing         : true,
                                 alwaysLinkToLastBuild: false,
                                 keepAll              : true,
                                 reportDir            : 'target/D3_business_viewer/',
                                 reportFiles          : 'index.html',
                                 reportName           : 'Business Test results'])

            print currentBuild.absoluteUrl + 'Business_Test_results' + '/'

        }


     //  stage('Generate Allure reports') {
     //  script {
     //   allure([
     //   includeProperties: false,
     //   jdk: '',
     //   properties: [],
     //   reportBuildPolicy: 'ALWAYS',
     //   results: [[path: 'target/surefire-reports']]
     //   ])
     //   }}


        stage('Archive') {
            archive 'target/**/*.*'
            print currentBuild.absoluteUrl + 'artifact' + '/'
        }
    }
}
