#!/usr/bin/env groovy

def call(Map runnerParams) {

    print "Generic runner called"
    print runnerParams

    properties([
            parameters([
                    choice(
                            choices: 'SIT\nUAT\npreProd',
                            description: 'Select Environment',
                            name: 'Environment'
                    ),
                    choice(
                            choices: '2.0\n1.1',
                            description: 'Select Run CMA Release version',
                            name: 'CMA_Release'
                    ),
                    choice(
                            choices: 'Yes\nNo',
                            description: 'Use Selenium Grid ?',
                            name: 'selenium_grid'
                    )
            ])
    ])

    stage('Welcome') {
        echo "This is auto generated - Pipeline Script for : " + runnerParams.Runner
    }

    Generic_Pipeline(
            TestName: runnerParams.TestName,
            branch: 'develop_3',
            scmUrl: 'https://bitbucket.boigroup.net/scm/chn/e2e.git',
            Runner: runnerParams.Runner,
            Test_Env: params.Environment,
            CMA_Rel_Ver: params.CMA_Release,
            selenium_grid: params.selenium_grid,
            node: 'Jenkins-1318'//runnerParams.node
    )

}