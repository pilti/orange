
function microservicesapp() {

//This is class for to set microservices app which is used in feature files
//This will refer all reusable FS services feature files
//This is expandable for new endpoints and new functionality through Java or Java script. \\

        this.FS_Account_Balance = function(input)
        {
            this.FS_Account_Balance = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/Account_Balance.feature",input);
        }

        this.FS_Account_Beneficiaries = function(input)
        {
            this.FS_Account_Beneficiaries = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/Account_Beneficiaries.feature",input);
        }

        this.FS_Account_SO = function(input)
        {
            this.FS_Account_SO = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/Account_SO.feature",input);
        }

        this.FS_Account_Transactions = function(input)
        {
            this.FS_Account_Transactions = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/Account_Transactions.feature",input)
        }

        this.FS_customerAccountProfile = function(input)
        {
            this.FS_customerAccountProfile = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfile.feature",input)
        }

        this.FS_customerAccountProfileSingle = function(input)
        {
            this.FS_customerAccountProfileSingle = karate.call("classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfileSingle.feature",input);
        }

    return this;
  }

