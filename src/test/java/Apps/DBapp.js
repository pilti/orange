
function microservicesapp() {

//This is class for to set microservices app which is used in feature files
//This will refer all reusable DB feature files
//This is expandable for new endpoints and new functionality through Java or Java script. \\

        this.DB_Account_Balance = function(input)
        {
            this.DB_Account_Balance = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/Account_Balance.feature",input);
        }

        this.DB_Account_Beneficiaries = function(input)
        {
            this.DB_Account_Beneficiaries = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/Account_Beneficiaries.feature",input);
        }

        this.DB_Account_SO = function(input)
        {
            this.DB_Account_SO = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/Account_SO.feature",input);
        }

        this.DB_Account_Transactions = function(input)
        {
            this.DB_Account_Transactions = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/Account_Transactions.feature",input)
        }

        this.DB_customerAccountProfile = function(input)
        {
            this.DB_customerAccountProfile = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/customerAccountProfile.feature",input)
        }

        this.DB_customerAccountProfileSingle = function(input)
        {
            this.DB_customerAccountProfileSingle = karate.call("classpath:CMA_Release/Entities_DB/AISP_DB/customerAccountProfileSingle.feature",input);
        }

    return this;
  }

