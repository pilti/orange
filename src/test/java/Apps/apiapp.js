
function microservicesapp() {

//This is class for to set microservices app which is used in feature files
//This will refer all reusable feature files, reusable Java class and reusable java scripts files
//This is expandable for new endpoints and new functionality through Java or Java script. \\

        this.Access_Token_CCG = function(input)
        {
            this.Access_Token_CCG = karate.call("classpath:CMA_Release/Entities_API/Auth_Tokens/OIDC/Access_Token_CCG.feature",input);
        }

        this.Access_Token_ACG = function(input)
        {
            this.Access_Token_ACG = karate.call("classpath:CMA_Release/Entities_API/Auth_Tokens/OIDC/Access_Token_ACG.feature",input);
        }

        this.Access_Token_RTG = function(input)
        {
            this.Access_Token_RTG = karate.call("classpath:CMA_Release/Entities_API/Auth_Tokens/OIDC/Access_Token_RTG.feature",input);
        }

        this.Account_Request_Setup = function(input)
        {
            this.Account_Request_Setup = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Setup.feature",input);
        }

        this.Account_Request_Retrieve = function(input)
        {
            this.Account_Request_Retrieve = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Retrieve.feature",input);
        }

        this.Multi_Account_Information = function(input)
        {
            this.Multi_Account_Information = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Multi_Account_Information.feature",input);
        }

        this.E2EConsent_GenerateToken = function(input)
        {
            this.E2EConsent_GenerateToken = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature",input);
        }

        this.Account_Information = function(input)
        {
            this.Account_Information = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Information.feature",input);
        }

        this.Account_Balance = function(input)
        {
            this.Account_Balance = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Balance.feature",input);
        }

        this.Customer_Beneficiaries = function(input)
        {
            this.Customer_Beneficiaries = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Customer_Beneficiaries.feature",input);
        }

        this.Account_Standing_Orders = function(input)
        {
            this.Account_Standing_Orders = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Standing_Orders.feature",input);
        }

        this.Account_Product = function(input)
        {
            this.Account_Product = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Product.feature",input);
        }

        this.Account_Transaction = function(input)
        {
            this.Account_Transaction = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Transaction.feature",input);
        }

        this.Account_Direct_Debits = function(input)
        {
            this.Account_Direct_Debits = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Direct_Debits.feature",input);
        }

        this.Bulk_Direct_Debits = function(input)
        {
            this.Bulk_Direct_Debits = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Bulk_Direct_Debits.feature",input);
        }

        this.Bulk_Product = function(input)
        {
            this.Bulk_Product = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Bulk_Product.feature",input);
        }

        this.Bulk_Standing_Orders = function(input)
        {
            this.Bulk_Standing_Orders = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Bulk_Standing_Orders.feature",input);
        }
        this.Bulk_Beneficiaries = function(input)
        {
            this.Bulk_Beneficiaries = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Bulk_Beneficiaries.feature",input);
        }
        this.Bulk_Transaction = function(input)
        {
            this.Bulk_Transaction = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Bulk_Transaction.feature",input);
        }

        this.Account_Request_Delete = function(input)
        {
            this.Account_Request_Delete = karate.call("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Delete.feature",input);

        }

         //////////////////////////////////////////PISP///////////////////////////////////////////////////////////////////////////////////////////////////////
         this.Payment_Setup = function(input)
         {
            this.Payment_Setup = karate.call("classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/Payment_Setup.feature",input);
         }
         this.Payment_Submission = function(input)
          {
            this.Payment_Submission = karate.call("classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/Payment_Submission.feature",input);
          }
           this.Login_Check = function(input)
                   {
                      this.Login_Check = karate.call("classpath:CMA_Release/Test_Set/WIP/Login_Check.feature",input);
                   }

         //////////////////////////////////////////IntentRequestObject///////////////////////////////////////////////////////////////////////////////////////////////////////
         this.ConstructAuthReqUrl = function(input)
          {
            this.ConstructAuthReqUrl = karate.call("classpath:CMA_Release/Entities_API/IntentRequestObject/ConstructAuthReqUrl.feature",input);

          }
          this.CreateRequestObject = function(input)
          {
            this.CreateRequestObject = karate.call("classpath:CMA_Release/Entities_API/IntentRequestObject/CreateRequestObject.feature",input);
          }

         /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         this.configureSSL = function(input)
         {
           var key_store_path = karate.call("classpath:CMA_Release/JavaScript_Lib/stmt_path.js",input);
           var key_store_file = key_store_path + 'oidc.jks';
           karate.configure('ssl', {keyStore: key_store_file, keyStorePassword: 'oidc', keyStoreType: 'jks'});

         }

          this.write = function(input,run_data)
          {
          var fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles');
          var folderCreator = Java.type('CMA_Release.Java_Lib.createFolder');
          var info = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",run_data);
          input.Testname = info.t_name;

          folderCreator.Folder_path(info.path);
          fileWriter.write (info.path + '/' + info.t_name + '.json', karate.pretty(input))
          }

          this.Payment_setup = function(input)
          {
              this.Payment_setup = karate.call("classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/Payment_Setup.feature",input);

          }

           this.PreReq_PaymentSubmission = function(input)
           {
             this.PreReq_PaymentSubmission = karate.call("classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_PaymentSubmission.feature",input);

           }

           this.PreReq_ConstructConsentURl = function(input)
           {
             this.PreReq_ConstructConsentURl = karate.call("classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature",input);

           }


    return this;
  }

