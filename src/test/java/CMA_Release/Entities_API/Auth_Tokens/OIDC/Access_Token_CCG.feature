Feature: Reusable OIDC feature for generating access token using client credential grant

  Input : Client ID, Client Secret and request method
  Output: API Response generating client credential token

  Background:

    * def CID = client_id
    * def SecretID = client_secret
    * def mtd = request_method
    * def Scope = scope
    * def grant = grant_type
    * def ContentType = Content_type
    * def Scope = (scope== null ? '' : "&scope=" + scope)
    * def grant = (grant== null ? '' : "grant_type=" + grant_type)

    # Get Base64 encoded Basic Authorization header using CID and Secrete (Javascript call)
    * def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(CID)', password: '#(SecretID)'}

    Scenario: Get Access Token using client credentials grant
      Given url TokenUrl
      And header Authorization = auth
      And header Content-Type = ContentType
      And request grant  + Scope
      When method mtd

      * set Result.ClientCredentialGrant.Input.TokenUrl = TokenUrl
      * set Result.ClientCredentialGrant.Input.Authorization = auth
      * set Result.ClientCredentialGrant.Input.ContentType = ContentType
      * set Result.ClientCredentialGrant.Input.GrantType = grant
      * set Result.ClientCredentialGrant.Input.Scope = Scope
      * def res = (response == null ? '' : response)
      * set Result.ClientCredentialGrant.Output.response = res
      * set Result.ClientCredentialGrant.Output.responseStatus = responseStatus
      * set Result.ClientCredentialGrant.Output.responseHeaders = responseHeaders




