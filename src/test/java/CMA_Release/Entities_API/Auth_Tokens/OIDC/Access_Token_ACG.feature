Feature: Reusable Feature for generating Access Token and Refresh Token using authorization_code grant type

  Input : Client ID, Client Secret,Auth_code and request method
  Output: API response generating access_token and refresh_token

  Background:

    * def CID = client_id
    * def SecretID = client_secret
    * def mtd = request_method
    * def Auth_code = code
    * def grant =  grant_type
    * def ContentType = Content_type
    * def Redirect_uri = redirect_uri

    * def grant = (grant_type== null ? '' : "grant_type=" + grant_type)
    * def Auth_code = (Auth_code== null ? '' : "&code=" + code)
    * def Redirect_uri = (redirect_uri== null ? '' : "&redirect_uri=" + redirect_uri)
    * def body =  grant + Auth_code + Redirect_uri

    # Get Base64 encoded Basic Authorization header using CID and Secrete (Javascript call)
    * def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(CID)', password: '#(SecretID)'}

  Scenario: Generate Access Token and refresh Token using Authorization_code grant

    Given url TokenUrl
    And header Authorization = auth
    And header Content-Type = ContentType
    And request body
    When method mtd


    * set Result.AccessTokenACG.Input.TokenUrl = TokenUrl
    * set Result.AccessTokenACG.Input.Authorization = auth
    * set Result.AccessTokenACG.Input.ContentType = ContentType
    * set Result.AccessTokenACG.Input.Payload = body
    * set Result.AccessTokenACG.Output.responseStatus = responseStatus
    * set Result.AccessTokenACG.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccessTokenACG.Output.response = res


    * def reftoken_arr =
    """
    {
      RT: '#(response.refresh_token)',
      env: '#(ActiveEnv)',
      client_id: '#(client_id)'
    }
    """

    * def fileName = 'RefreshToken.properties'
    * def name = 'reftokens'
    * def Datapath = './src/test/java/Resources/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * eval var val = []
    * eval if(responseStatus == 200) { val.push(reftoken_arr) }
    * eval if(profile!= '{}'){for(var i = 0;i < profile.size(); i++){val.push(profile[i])}}
    * def array_val = "[" + val + "]"
    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,name,array_val)
