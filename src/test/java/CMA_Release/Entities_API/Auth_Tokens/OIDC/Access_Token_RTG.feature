Feature: Reusable Feature for generating Access Token using refresh_token grant type

  Input : Client ID, Client Secret,refresh_token and request method

  Output: API response generating access_token using refresh_token

  Background:

    * def CID = client_id
    * def SecretID = client_secret
    * def mtd = request_method
    * def Refresh_token = refresh_token
    * def grant =  grant_type
    * def ContentType = Content_type


    * def grant = (grant_type== null ? '' : "grant_type=" + grant_type)
    * def Refresh_token = (refresh_token== null ? '' : "&refresh_token=" + refresh_token)


    * def body =  grant + Refresh_token

    # Get Base64 encoded Basic Authorization header using CID and Secrete (Javascript call)
    * def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(CID)', password: '#(SecretID)'}

  Scenario: Generate Access Token and refresh Token using Authorization_code grant

#    *  print "Data__>" + TokenUrl + "---" + auth + "---"  + ContentType + body + mtd
#    * print "CRED++" + CID + SecretID
    Given url TokenUrl
    And header Authorization = auth
    And header Content-Type = ContentType
    And request body
    When method mtd

    * set Result.AccessTokenRTG.Input.TokenUrl = TokenUrl
    * set Result.AccessTokenRTG.Input.Authorization = auth
    * set Result.AccessTokenRTG.Input.ContentType = ContentType
    * set Result.AccessTokenRTG.Input.refresh_token = refresh_token
    * set Result.AccessTokenRTG.Input.grant = grant
    * set Result.AccessTokenRTG.Output.responseStatus = responseStatus
    * set Result.AccessTokenRTG.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccessTokenRTG.Output.response = res







