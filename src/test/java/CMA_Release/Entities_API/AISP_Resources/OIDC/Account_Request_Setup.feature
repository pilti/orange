Feature: Reusable Feature for generating Account Request ID using Account Requests endpoint

  Input       : Client ID, Client Secret,Client credential token and request method
  Output      : API response generating Account request ID

  Background:

    * def CID = client_id
    * def CSecret = client_secret
    * def Token = access_token
    * def Type = token_type
    * def mtd = request_method

    * eval if (CID != null)  {reqHeader.client_id = CID}
    * eval if (CSecret != null)  {reqHeader.client_secret = CSecret}


  Scenario: Create Account Request ID

    Given url AcctReqUrl
    And header Authorization = Type + " " + Token
    And headers reqHeader
    And request permissions
    When method mtd

    * set Result.AccountRequestSetup.Input.URL = AcctReqUrl
    * set Result.AccountRequestSetup.Input.body = permissions
    * set Result.AccountRequestSetup.Input.Header = reqHeader
    * set Result.AccountRequestSetup.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountRequestSetup.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountRequestSetup.Input.request_method = request_method
    * set Result.AccountRequestSetup.Output.responseStatus = responseStatus
    * set Result.AccountRequestSetup.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountRequestSetup.Output.response = res







