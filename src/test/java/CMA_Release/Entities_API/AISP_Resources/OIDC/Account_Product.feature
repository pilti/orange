@ignore
Feature: Reusable Feature for getting single Account Product information using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountId}/product

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account Product API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account Product information for the accountID
    Given url AcctInfoUrl + accountID + '/product'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.AccountProduct.Input.URL = AcctInfoUrl + accountID + '/product'
    * set Result.AccountProduct.Input.Header = reqHeader
    * set Result.AccountProduct.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountProduct.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountProduct.Input.request_method = request_method
    * set Result.AccountProduct.Output.responseStatus = responseStatus
    * set Result.AccountProduct.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountProduct.Output.response = res