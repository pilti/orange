Feature: This feature is to cover the positive flow till launching consent URL

  Background:

   #####################################Background End####################################################################################################
@123456
  Scenario: Create and login to Consent

    Given def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
     #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(user_details.Generic.G_User_2.user)',"otp":'#(user_details.Generic.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform_selection = Functions.scaLogin(webApp,data)
    Then match perform_selection.Status == 'Pass'
    #And print perform_selection.AccountSelectionPage
    And def perform = Functions.selectAllAccounts(webApp,data)
    And match perform.Status == 'Pass'
    And print perform
    And print perform.AccountSelectionPage
    And print perform.AccountReviewPage
#    And def perform_review = Functions.reviewConfirm(webApp,data)
