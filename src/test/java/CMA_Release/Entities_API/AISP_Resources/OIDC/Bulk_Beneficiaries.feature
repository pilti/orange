@ignore
Feature: Reusable Feature for getting Bulk Account Beneficiaries information using access_token
    HTTP Method : GET
    API URI : /beneficiaries

    Input   : access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Bulk Account Beneficiaries API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Bulk Account Beneficiaries information
    Given url BulkApiUrl + '/beneficiaries'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################