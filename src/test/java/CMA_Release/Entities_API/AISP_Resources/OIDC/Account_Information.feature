@ignore
Feature: Reusable Feature for getting Single Account information for an accountID using Access Token
  HTTP Method : GET
  API URI : /accounts/{accountID}

  Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
  Output  : Response for Single Account Info API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account information for the accountID
    Given url AcctInfoUrl + accountID
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################
    * set Result.AccountInformation.Input.URL = AcctInfoUrl + accountID
    * set Result.AccountInformation.Input.Header = reqHeader
    * set Result.AccountInformation.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountInformation.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountInformation.Input.request_method = request_method
    * set Result.AccountInformation.Output.responseStatus = responseStatus
    * set Result.AccountInformation.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountInformation.Output.response = res