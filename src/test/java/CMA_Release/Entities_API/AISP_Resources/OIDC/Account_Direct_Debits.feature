@ignore
Feature: Reusable Feature for getting single Account Direct Debits information using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountId}/direct-debits

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account Direct Debits API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}
###########################################################################################################

  Scenario: Get Account direct-debits information for the accountID
    Given url AcctInfoUrl + accountID + '/direct-debits'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.DirectDebits.Input.URL = AcctInfoUrl + accountID + '/direct-debits'
    * set Result.DirectDebits.Input.Header = reqHeader
    * set Result.DirectDebits.Input.Header.client_id = reqHeader.client_id
    * set Result.DirectDebits.Input.Header.client_secret = reqHeader.client_secret
    * set Result.DirectDebits.Input.request_method = request_method
    * set Result.DirectDebits.Output.responseStatus = responseStatus
    * set Result.DirectDebits.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.DirectDebits.Output.response = res
    * eval if (responseStatus == 200) { Result.DirectDebits.Output.TotalDirectDebits = response.Data.DirectDebit.length }