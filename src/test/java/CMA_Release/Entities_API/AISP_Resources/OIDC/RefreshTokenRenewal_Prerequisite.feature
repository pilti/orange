Feature: This feature is to launch ConsentURL , save the accounts/and other required info before submitting consent

Background:

  * def apiApp = new apiapp()
  * def webApp = new webapp()

  * json Result = {}
  * set Result.Testname = null
  * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

Scenario:
    Given json Application_Details = active_tpp.AISP_PISP

    #Do consent for the first time and complete SCA
    * def RESPONSE = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    # Do consent second time with account request ID generated from first consent

    * print Application_Details.selectedAccounts
    * print Application_Details.AccountRequestId

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

   Then call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print  consenturl
    * set Application_Details.consenturl = consenturl
    * print Application_Details

