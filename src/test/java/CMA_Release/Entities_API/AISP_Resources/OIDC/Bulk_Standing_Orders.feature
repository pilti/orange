@ignore
Feature: Reusable Feature for getting Bulk Account Standing Orders information using access_token
    HTTP Method : GET
    API URI : /standing-orders

    Input   : access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Bulk Account Standing Orders API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Bulk Account Standing Orders information
    Given url BulkApiUrl + '/standing-orders'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################