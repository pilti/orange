@ignore
Feature: Reusable Feature for getting Multi Account information using Access Token
    HTTP Method : GET
    API URI : /accounts

    Input   : access_token, client_id, client_secret, request_method, token_type
    Output  : Response from Multi Account Info API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}
###########################################################################################################

  Scenario: Get Account information for the consent
    Given url AcctInfoUrl
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

    * set Result.MultiAccountInfo.Input.URL = AcctInfoUrl
    * set Result.MultiAccountInfo.Input.Header = reqHeader
    * set Result.MultiAccountInfo.Input.Header.client_id = reqHeader.client_id
    * set Result.MultiAccountInfo.Input.Header.client_secret = reqHeader.client_secret
    * set Result.MultiAccountInfo.Input.request_method = request_method
    * set Result.MultiAccountInfo.Output.responseStatus = responseStatus
    * set Result.MultiAccountInfo.Output.responseHeaders = responseHeaders
    * copy res = (response == null ? '' : response)
    * set Result.MultiAccountInfo.Output.response = res