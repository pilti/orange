Feature: This Reusable feature to get AISP Consent

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * json AccReQ = {}
    * set Result.ReusableFeatureFor = 'AISP Consent Start'
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')


  Scenario: Get AISP Consent page

    #Hit Certificate request to server
    And call apiApp.configureSSL(Application_Details)
    #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"

    ##########################################################################################################################################

    #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)

    #Validate response and update result json
    And set Result.Access_Token_CCG.Input = Application_Details
    And set Result.Access_Token_CCG.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.Access_Token_CCG.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.Access_Token_CCG.response = apiApp.Access_Token_CCG.response
    And set Result.Access_Token_CCG.responseHeaders = apiApp.Access_Token_CCG.responseHeaders

    #Perform Assertion
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    ##########################################################################################################################################

    #Update ip json with parameters required by next API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"

    #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)

    #Valideate response and form result json
    And set Result.Account_Request_Setup.Input = Application_Details
    And set Result.Account_Request_Setup.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.Account_Request_Setup.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.Account_Request_Setup.response = apiApp.Account_Request_Setup.response
    And set Result.Account_Request_Setup.responseHeaders = apiApp.Account_Request_Setup.responseHeaders

    ############################################Write output File##############################################################################################
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * set Result.CreateRequestObject.Input = ReqObjIP
    * set Result.CreateRequestObject.Input.SiningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    * set Result.CreateRequestObject.JWT = apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * set Result.CreateRequestObject.URL = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    #added to save the consnet url , & launch again during authorisation renewal
    * print consenturl
    * set Application_Details $.consenturl = consenturl
    #* def consenturl = 'http://www.bbc.com'
##########################################################################################################################
    * print "Starting Browser", default_browser
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    And def path = call  webApp.path1 = Application_Details.TestCasePath
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.launchConsentURL_Status = perform.Status
    And set Result.UIOutput.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.useKeyCodeApp_Status = perform.Status
    And set Result.UIOutput.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.ScaLogin_Status = perform.Status
    And set Result.UIOutput.ScaLogin_Landing_Page = perform.Landing_Page




