Feature: This reusable feature is to do Consent Authorisation and generate Authcode for given Application details

  Background:
    * json Result1 = {}
    * json UIResult = {}
    * json AccReQ = {}
    #added just in case to run this file individually
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def usr = Application_Details.usr
    * def otp = Application_Details.otp
    * def User = usr
    * def OTP = otp
    * def User = (usr == null ? user_details.Generic.G_User_1.user : usr)
    * def OTP = (otp == null ? user_details.Generic.G_User_1.otp : otp)
    * def data = {"usr":'#(User)',"otp":'#(OTP)',"action":"continue","Confirmation":"Yes"}

  @AISP_Get_Token
  Scenario: Consent Authorisation and generate Authcode
    #Step 1: Get Access_Token_CCG
    Given def Application_Details = Application_Details
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Result1.ActualOutput.Access_Token_CCG.Input.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result1.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result1.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And set Result.Access_Token_CCG.Input = Application_Details
    And set Result.Access_Token_CCG.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.Access_Token_CCG.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.Access_Token_CCG.response = apiApp.Access_Token_CCG.response
    And set Result.Access_Token_CCG.responseHeaders = apiApp.Access_Token_CCG.responseHeaders
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Step 2: Account_Request_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    And set Result1.ActualOutput.Account_Request_Setup.Input.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result1.ActualOutput.Account_Request_Setup.Output.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result1.ActualOutput.Account_Request_Setup.Output.response = apiApp.Account_Request_Setup.response
    And set Result.Account_Request_Setup.Input = Application_Details
    And set Result.Account_Request_Setup.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.Account_Request_Setup.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.Account_Request_Setup.response = apiApp.Account_Request_Setup.response
    And set Result.Account_Request_Setup.responseHeaders = apiApp.Account_Request_Setup.responseHeaders
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    #Step 3: Create RequestObject and Construct consenturl
    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result1.ActualOutput.CreateRequestObject.Input = ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.Input = ReqObjIP
    And set Result.CreateRequestObject.Input.SigningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    And set Result.CreateRequestObject.JWT = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
  #'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result1.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    Then call apiApp.ConstructAuthReqUrl(Application_Details)
    And set Result.CreateRequestObject.URL = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And set Application_Details $.consenturl = consenturl
    And set Result1.ActualOutput.ConstructAuthReqUrl.ConsentUrl = Application_Details.consenturl

    #Step 4: Consent Authorisation
    Given def webApp = new webapp()
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result1.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result1.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And set Result1.ActualOutput.UI.Output.launchConsentURL_Co-relationId = perform.scaCorelationId

    Then def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result1.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result1.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page
    Then def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result1.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result1.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    Then def perform = Functions.selectAllAccounts(webApp,data)
    And set UIResult.AccountSelectionPage = perform
    Then match perform.Status == 'Pass'
    And set Result1.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result1.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle
    And set data $.SelectedAccounts = perform.SelectedAccounts
      #added to save the accounts selected during SCA, & verify during authorisation renewal
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result1.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    And set Result1.ActualOutput.UI.Output.SelectAllAccounts_CurrencyAccountPair = perform.CurrencyAccountPair
    Then def perform = Functions.reviewConfirm(webApp,data)
    * json val = perform.Permissions.PermissionsInDetail
    * def retrieved_permission_heading = Java.type('CMA_Release.Java_Lib.GetKey').getKeyVal(val)

    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.retrieved_permission_heading = retrieved_permission_heading
      #added to save the permissions selected during SCA , & verify during authorisation renewal
    And set Application_Details.Permissions = perform.Permissions
    And set Result.UIOutput.ReviewConfirm.Permissions =  perform.Permissions
    And set Result1.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Result1.ActualOutput.UI.Output.ReviewConfirm.ConsentExpiry = perform.ConsentExpiry
    And set UIResult.ReviewConfirmPage = perform
    And set Application_Details.code = perform.Action.Authcode
    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    And set Result1.ActualOutput.UI.Output.code = perform.Action.Authcode
    And set Result1.ActualOutput.UI.Output.idtoken = perform.Action.idtoken
    Then def stop = webApp.stop()

  #Step 4: Access_Token_ACG
    Given set Application_Details $.grant_type = "authorization_code"
    When call apiApp.Access_Token_ACG(Application_Details)
    Then eval if (apiApp.Access_Token_ACG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}
    And set Result1.ActualOutput.Access_Token_ACG.Output.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result1.ActualOutput.Access_Token_ACG.Output.response = apiApp.Access_Token_ACG.response
    And set Result.Access_Token_ACG.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.Access_Token_ACG.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
