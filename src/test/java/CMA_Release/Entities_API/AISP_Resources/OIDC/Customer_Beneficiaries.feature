@ignore
Feature: Reusable Feature for getting single Account beneficiaries using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountId}/beneficiaries

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account beneficiaries API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account beneficiaries information for the accountID
    Given url AcctInfoUrl + accountID + '/beneficiaries'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.CustomerBeneficiaries.Input.URL = AcctInfoUrl + accountID + '/beneficiaries'
    * set Result.CustomerBeneficiaries.Input.Header = reqHeader
    * set Result.CustomerBeneficiaries.Input.Header.client_id = reqHeader.client_id
    * set Result.CustomerBeneficiaries.Input.Header.client_secret = reqHeader.client_secret
    * set Result.CustomerBeneficiaries.Input.request_method = request_method
    * set Result.CustomerBeneficiaries.Output.responseStatus = responseStatus
    * set Result.CustomerBeneficiaries.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.CustomerBeneficiaries.Output.response = res
    * eval if (responseStatus == 200) { Result.CustomerBeneficiaries.Output.TotalBeneficiaries = response.Data.Beneficiary.length }