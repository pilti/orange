Feature: Pre-requisite for PreAuth Validation API


  Scenario:

        #Hit Certificate request to server
    Given call apiApp.configureSSL(Application_Details)

     #Set input with the request Method
    And set Application_Details.request_method = 'POST'

     #Set grant_type as 'client_credentials'
    And set Application_Details $.grant_type = 'client_credentials'

     #Set Content_type as 'application/x-www-form-urlencoded'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_client_secret = Application_Details.client_secret

     #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)

    #Validate response and update result json
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response

     #Perform Assertion
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Update ip json with parameters required by next API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type

     #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)

     #Validate response and form result json
    And set Result.ActualOutput.Account_Request_Setup.Input.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.ActualOutput.Account_Request_Setup.Output.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.ActualOutput.Account_Request_Setup.Output.response = apiApp.Account_Request_Setup.response

   #Perform Assertion
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    #* print Application_Details


    #########################################################################################################################################################
