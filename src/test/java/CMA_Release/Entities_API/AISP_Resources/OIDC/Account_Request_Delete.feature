Feature: Reusable Feature for Deleting Account Request ID using Account Requests endpoint

  Input       : Client ID, Client Secret,Client credential token and request method
  Output      : API response Revokes Account request ID

  Background:

    * def AccountRequestId = AccountRequestId
    * def CID = client_id
    * def CSecret = client_secret
    * def Token = access_token
    * def Type = token_type
    * def mtd = request_method

    * eval if (CID != null)  {reqHeader.client_id = CID}
    * eval if (CSecret != null)  {reqHeader.client_secret = CSecret}


  Scenario: Create Account Request ID

    Given url AcctReqUrl + "/" + AccountRequestId
    And header Authorization = Type + " " + Token
    And headers reqHeader
    * print reqHeader
    When method mtd


    * set Result.AccountRequestDelete.Input.URL = AcctReqUrl + "/" + AccountRequestId
    * set Result.AccountRequestDelete.Input.Header = reqHeader
    * set Result.AccountRequestDelete.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountRequestDelete.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountRequestDelete.Input.request_method = request_method
    * set Result.AccountRequestDelete.Output.responseStatus = responseStatus
    * set Result.AccountRequestDelete.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountRequestDelete.Output.response = res







