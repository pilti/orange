@ignore
Feature: Reusable Feature for getting single Account balance information using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountID}/balances

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account Balance API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account Balance information for the accountID
    Given url AcctInfoUrl + accountID + '/balances'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.AccountBalance.Input.URL = AcctInfoUrl + accountID + '/balances'
    * set Result.AccountBalance.Input.Header = reqHeader
    * set Result.AccountBalance.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountBalance.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountBalance.Input.request_method = request_method
    * set Result.AccountBalance.Output.responseStatus = responseStatus
    * set Result.AccountBalance.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountBalance.Output.response = res