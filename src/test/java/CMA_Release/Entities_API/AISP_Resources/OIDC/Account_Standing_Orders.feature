@ignore
Feature: Reusable Feature for getting single Account Standing Orders using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountId}/standing-orders

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account Standing Orders API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account standing-orders information for the accountID
    Given url AcctInfoUrl + accountID + '/standing-orders'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.AccountStandingOrders.Input.URL = AcctInfoUrl + accountID + '/standing-orders'
    * set Result.AccountStandingOrders.Input.Header = reqHeader
    * set Result.AccountStandingOrders.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountStandingOrders.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountStandingOrders.Input.request_method = request_method
    * set Result.AccountStandingOrders.Output.responseStatus = responseStatus
    * set Result.AccountStandingOrders.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.AccountStandingOrders.Output.response = res
    * eval if (responseStatus == 200) { Result.AccountStandingOrders.Output.TotalStandingOrders = response.Data.StandingOrder.length }
