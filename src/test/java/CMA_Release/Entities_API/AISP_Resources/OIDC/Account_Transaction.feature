@ignore
Feature: Reusable Feature for getting single Account transactions information using accountID and access_token
    HTTP Method : GET
    API URI : /accounts/{accountId}/transactions

    Input   : AccountId, access_token, client_id, client_secret, request_method, token_type
    Output  : Response for Single Account Transactions API
###########################################################################################################
  Background:

    * def oAuth_token = access_token
    * def CID = client_id
    * def CSecret = client_secret
    * def method1 = request_method
    * def accountID = AccountId
    * def Type = token_type

    * eval if (CID != null) {reqHeader.client_id = CID}
    * eval if (CSecret != null) {reqHeader.client_secret = CSecret}

###########################################################################################################

  Scenario: Get Account Transactions information for the accountID
    Given url AcctInfoUrl + accountID + '/transactions'
    And headers reqHeader
    And header Authorization = Type + ' ' + oAuth_token
    When method method1

##################<End of Feature>#########################################################################

    * set Result.AccountTransactions.Input.URL = AcctInfoUrl + accountID + '/transactions'
    * set Result.AccountTransactions.Input.Header = reqHeader
    * set Result.AccountTransactions.Input.Header.client_id = reqHeader.client_id
    * set Result.AccountTransactions.Input.Header.client_secret = reqHeader.client_secret
    * set Result.AccountTransactions.Input.request_method = request_method
    * set Result.AccountTransactions.Output.responseStatus = responseStatus
    * def res = (response == null ? '' : response)
    * set Result.AccountTransactions.Output.response = res
    #* set Result.AccountTransactions.Output.TotalTransactions = get (response.Data.Transaction).length
    * eval if (responseStatus == 200) { Result.AccountTransactions.Output.responseHeaders = responseHeaders }
