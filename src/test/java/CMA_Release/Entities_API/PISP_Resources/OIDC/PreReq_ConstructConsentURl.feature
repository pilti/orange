#This Feature takes Application_Details and consentData as input
Feature: PISP_Flow until Generation of Consent URL

  Background:
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

  Scenario: This feature will create ConsentURL

     # Network Handshake for test tpp
    * call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And print Application_Details
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And set Application_Details.idempotency_key = apiApp.Payment_Setup.idempotency_value

    #CreateRequestObject
    Given set Application_Details.PaymentId = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details $.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And print Application_Details
    And print ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    #ConstructAuthReqUrl
    Given set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And print Application_Details
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then set Application_Details $.consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
