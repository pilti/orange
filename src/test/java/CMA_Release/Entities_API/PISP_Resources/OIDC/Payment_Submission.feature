@ignore
Feature: Reusable Feature for Payment Submission using PISP_Consent.feature
  HTTP Method : POST
  API URI : /open-banking/v1.1/payment-submissions
  Input : client_id/client_secret/orgUnit/access_token/PaymentId/idempotencyKey

  ###########################################################################################################
  Background:

    #Below fields should be sent as input while this feature is called:
    * def client_id = client_id
    * def client_secret = client_secret
    * def access_token = access_token
    * def request_method = request_method
    * def idempotency_value = idempotency_key
    * def PaymentId = PaymentId
    * def Type = token_type
    * def client_id = (client_id == null ? '' :  client_id)
    * def client_secret = (client_secret == null ? '' : client_secret)
    * def access_token = (access_token == null ? '' : access_token)
    * def request_method = (request_method == null ? '' : request_method)
    * def idempotency_value = (idempotency_key == null ? '' : idempotency_key)
    * def PaymentId = (PaymentId == null ? '' : PaymentId)
    * def Type = (token_type == null ? '' : token_type)

    #tpp credentials
    * json tpp_credentials = {"client_id": '#(client_id)',"client_secret": '#(client_secret)'}

    #* set pisp_reqHeader $.x-idempotency-key = idempotency_value
    * set pisp_reqHeader $.x-fapi-interaction-id = Java.type('CMA_Release.Java_Lib.RandomValue').random_id()
    * set pisp_payload $.Data.PaymentId = PaymentId
    ###########################################################################################################

  Scenario: POST Payment ID

    Given url PmtSubUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header x-idempotency-key = idempotency_value
    And header Authorization = Type + " " + access_token
    When method request_method

    * set Result.PaymentSubmission.Input.URL = PmtSubUrl
    * set Result.PaymentSubmission.Input.SubmissionPayload = pisp_payload
    * set Result.PaymentSubmission.Input.SubmissionHeader = pisp_reqHeader
    * set Result.PaymentSubmission.Input.SubmissionHeader.x_idempotency_key = idempotency_value
    * set Result.PaymentSubmission.Input.SubmissionHeader.client_id = tpp_credentials.client_id
    * set Result.PaymentSubmission.Input.SubmissionHeader.client_secret = tpp_credentials.client_secret
    * set Result.PaymentSubmission.Input.request_method = request_method
    * set Result.PaymentSubmission.Output.responseStatus = responseStatus
    * set Result.PaymentSubmission.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.PaymentSubmission.Output.response = res