@ignore
Feature: Reusable Feature for getting Payment ID using Payment Initiation API
  HTTP Method : POST
  API URI : /open-banking/v1.1/payments

  Input: Client Id/Client Secret/access_token/OrgaizationUnit/request_method

  ###########################################################################################################
  Background:
  # Parameters

    * def ReqMethod = request_method

    #Below fields should be sent as input while this feature is called:
    * def client_id = client_id
    * def client_secret = client_secret
    * def access_token = access_token
    * def request_method = request_method
    * def client_id = (client_id == null ? '' :  client_id)
    * def client_secret = (client_secret == null ? '' : client_secret)
    * def access_token = (access_token == null ? '' : access_token)
    * def request_method = (request_method == null ? '' : request_method)

    #tpp credentials
    * json tpp_credentials = {"client_id": '#(client_id)',"client_secret": '#(client_secret)'}
    * def interaction_id = Java.type('CMA_Release.Java_Lib.RandomValue').random_id()
    * def idempotency_value = Java.type('CMA_Release.Java_Lib.RandomValue').random_id()
    * set pisp_reqHeader $.x-idempotency-key = idempotency_value
    * set pisp_reqHeader $.x-fapi-interaction-id = interaction_id

  ###########################################################################################################

  Scenario: POST Payment Setup

    Given url PmtSetupUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header Authorization = 'Bearer ' + access_token
    When method request_method

    * set Result.PaymentSetup.Input.URL = PmtSetupUrl
    * set Result.PaymentSetup.Input.Payload = pisp_payload
    * set Result.PaymentSetup.Input.header = pisp_reqHeader
    * set Result.PaymentSetup.Input.tpp_credentials = tpp_credentials
    * set Result.PaymentSetup.Input.request_method = request_method
    * set Result.PaymentSetup.Output.responseStatus = responseStatus
    * set Result.PaymentSetup.Output.responseHeaders = responseHeaders
    * def res = (response == null ? '' : response)
    * set Result.PaymentSetup.Output.response = res

