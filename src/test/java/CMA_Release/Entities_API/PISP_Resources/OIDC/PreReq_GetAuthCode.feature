#This Feature takes Application_Details and consentData as input
Feature: PISP_Flow until Authcode Generation

  Background:
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

  Scenario: PISP Test Case Sample
    #ConsentFlow
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_PISP_Functions.launchConsentURL(webApp,Application_Details.consenturl)
    Then match perform.Status == 'Pass'
    And print perform

    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    Then match perform_loginConsent.Status == 'Pass'
    And print perform_loginConsent

    Given set consentData.action = "continue"
    And set consentData.Confirmation = "Yes"
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    When def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    Then match perform_selectFromAccount.Status == 'Pass'
    And print perform_selectFromAccount

    Given set consentData.Confirmation = "Yes"
    When def performConsentAction = oidc_PISP_Functions.performConsentAction(webApp,consentData)
    Then match performConsentAction.AuthCode !contains "http"
    And set Application_Details.code = performConsentAction.AuthCode
    And print performConsentAction