Feature: Request PSU for SCA and Consent - Construct URL

  Input to this Feature is Scope,JWT for Intent Request Object,client Id and redirect_uri

  Background:

    * def Scope = scope
    * def Request = jwt
    * def Client_id = client_id
    * def Redirect_uri = redirect_uri
    * def Response_type = response_type
    * def State = state
    * def Nonce = nonce

  Scenario: This Scenario creates a URL for Consent

    * def scope = (Scope=='null' ? '' : "scope=" + scope)
    * def client_id = (Client_id=='null' ? '' : "client_id=" + client_id)
    * def response_type = (Response_type=='null' ? '' : "response_type=" + response_type)
    * def state = (State=='null' ? '' : "state=" + state)
    * def nonce = (Nonce=='null' ? '' : "nonce=" + nonce)
    * def redirect_uri = (Redirect_uri=='null' ? '' : "redirect_uri=" + redirect_uri)
    * def Request = (Request=='null' ? '' : "request=" + jwt)
    * print "----construct url----" + redirect_uri

    * def constructurl = AuthUrl + "&" + client_id + "&" + response_type + "&" + scope + "&" + state + "&" + nonce + "&" + redirect_uri + "&" + Request
    * string constructurl = constructurl.replace('&&','&')
    * json Curl = {"consenturl": '#(constructurl)'}

    * print constructurl
    * set Result.ConstructAuthReqUrl.Input.AuthUrl = AuthUrl
    * set Result.ConstructAuthReqUrl.Input.client_id = client_id
    * set Result.ConstructAuthReqUrl.Input.response_type = response_type
    * set Result.ConstructAuthReqUrl.Input.state = state
    * set Result.ConstructAuthReqUrl.Input.nonce = nonce
    * set Result.ConstructAuthReqUrl.Input.redirect_uri = redirect_uri
    * set Result.ConstructAuthReqUrl.Input.Request = jwt
    * set Result.ConstructAuthReqUrl.Output.ConsentUrl = constructurl






