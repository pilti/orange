Feature: Create Intent Request Object(jwt)

  Input of this feature is a json file containing below:
  1)JWT Header containing KID ID (Signing)
  2)JWT Body (Account Request ID/PaymentID)

  File having Certificate:

  1) Signing private Key(SigningKey)

  Background:

    * string pathToSigningKey = path
    #Function returns signed JWT
    * def FunctionJwt = Java.type('CMA_Release.Java_Lib.Jwt')

  Scenario: This scenario creates Intent Request Object

    #Validating KID should not be null

    * def status = (ReqObjIP.header.kid == 'null' ? 'Stop' : 'Proceed')
    * def testout =  (status == 'Stop' ? 'KID is null...stopping execution' : 'KID is not null...continue execution')
    * print testout
    * match status == 'Proceed'

    #Generating JWT
    * string header = ReqObjIP.header
    * set ReqObjIP.payload.aud = authServer
    * string payload = ReqObjIP.payload
    * def auth = FunctionJwt.JWT(header,payload,pathToSigningKey)
    * print "signing key path=" + pathToSigningKey
    * json jwt = {"request": '#(auth)'}

    * set Result.CreateRequestObject.Input.header = ReqObjIP.header
    * set Result.CreateRequestObject.Input.payload = ReqObjIP.payload
    * set Result.CreateRequestObject.Output.JWT = auth