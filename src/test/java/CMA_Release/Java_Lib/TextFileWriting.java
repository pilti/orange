package CMA_Release.Java_Lib;


import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * This program demonstrates how to write characters to a text file.
 *
 * @author www.codejava.net
 */
public class TextFileWriting {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(TextFileWriting.class);

    public static String consent(String S, String p, String fName, String folName) {
        String status = "";
        try {
            String path = "./src/test/java/CMA_Release/Base_Data/" + p;
            File file = new File(path);
            if (!file.exists()) {
                file.mkdir();

                path = ("./src/test/java/CMA_Release/Entities_API/" + folName + "/");
                 FileWriter writer = new FileWriter(path + fName, true);
                writer.write(S);
                writer.flush();
                writer.close();
                status = "ok";
            } else {

                path = ("./src/test/java/CMA_Release/Entities_API/" + folName + "/" + fName);
                File file1 = new File(path);
                if (file1.exists()) {
                    file1.delete();
                }
                //File file2 = new File(path);
                path = ("./src/test/java/CMA_Release/Entities_API/" + folName + "/");
                FileWriter writer1 = new FileWriter(path + fName, true);
                writer1.write(S);
                writer1.flush();
                writer1.close();
                status = "ok";

            }

        } catch (IOException e) {
            e.printStackTrace();
            status = e.toString();
        }
        return status;
    }

    public static void FileOP(String path, String op) {

        String dr = path;
                //"./src/test/java/CMA_Release/Test_Output/" + path;
        File file = new File(dr);
        if (!file.exists()) {
            file.mkdirs();
            LOGGER.info("Directory is been Created!");
        } else {
            LOGGER.info("Directory already exists!!!!");
        }

        try {
            File f1 = new File(dr);
            if (f1.exists()) {
                f1.delete();
                FileWriter writer = new FileWriter(dr , true);
                writer.write(op);
                writer.flush();
                writer.close();
            } else {
                FileWriter writer = new FileWriter(dr , true);
                writer.write(op);
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}

