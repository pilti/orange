package CMA_Release.Java_Lib;

import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by c961818 on 08/10/2017.
 */
public class createFolder {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(createFolder.class);
    public static String output_path() {
            java.util.Date date = new java.util.Date();
            String dr = "./src/test/java/CMA_Release/Test_Output/"+ date.toString().replace(":","_").replace(" ","_")+"/";
            File file = new File(dr);

            if (!file.exists()) {
                if (file.mkdir()) {
                    LOGGER.info("Output Directory is created!");

                } else {

                    LOGGER.info("Failed to create directory!");
                     dr = ("./src/test/java/CMA_Release/Test_Output/default");
                }
            }

        return dr;

       }

    public static void Folder_path(String path){


        String path1 = null;
        try {
            path1 = URLDecoder.decode(path, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Path path2 = Paths.get(path1);
        System.out.print("Folder Path----"+path1);
        //if directory exists?
        if (!Files.exists(path2)) {
            try {
                Files.createDirectories(path2);
                LOGGER.info("Directory is created!");
            } catch (IOException e) {

                e.printStackTrace();
            }

        }
        else {
            LOGGER.info("Directory already present!");
        }


    }

}
