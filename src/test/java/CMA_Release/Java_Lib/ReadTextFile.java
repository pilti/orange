package CMA_Release.Java_Lib;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by C963294 on 26/03/2018.
 */
public class ReadTextFile {

    public static String ReadFile(String path) throws IOException {
        if (path.contains("classpath")) {
            path = path.replace("classpath:", "./src/test/java/");
        }
        String privateKey = "";
        File file = new File(path);
        Scanner sc = new Scanner(file);
        sc.useDelimiter("\\Z");
        privateKey = sc.next();
        return privateKey;
    }

}
