package CMA_Release.Java_Lib.Crypto;

public class CryptoException extends Exception {

	public CryptoException() {
	}

	public CryptoException(String message, Throwable throwable) {
		super(message, throwable);
	}
}