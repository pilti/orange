package CMA_Release.Java_Lib.AISP_Scenarios;

import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by C965116 on 24/09/2018.
 */
public class FieldValueValidations {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OIDC_AISP.class);

    public static void CheckMrktClassification(List<String> mrkt_val_db, List<String> acc_typ) {
        try {
            List<Integer> mrkt_classification = new ArrayList<>();
            for (int i = 0; i < mrkt_val_db.size(); i++) {
                int val = Integer.parseInt(mrkt_val_db.get(i));
                System.out.print(val);
                mrkt_classification.add(val);
            }

            if (mrkt_val_db.size() == acc_typ.size()) {
                for (int i = 0; i < mrkt_val_db.size(); i++) {
                    if (mrkt_classification.get(i) >= 960 && mrkt_classification.get(i) <= 972)
                        Assert.assertTrue(acc_typ.get(i).equals("Personal"));
                    else
                        Assert.assertTrue(acc_typ.get(i).equals("Business"));
                }
            } else {
                Assert.assertTrue(false);
                LOGGER.info("Assert Failed both arrays not of same size");
            }
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
        }
    }


    public static List<String> convertDBColumnValToArray(String[] arr) {
        try {
            List<String> arr2 = new ArrayList<>();

            for (int i = 0; i < arr.length; i++) {
                String val = null;
                String[] arr1 = null;
                arr1 = arr[i].split(":");
                val = arr1[1];
                val = val.replace("}", "").replace("\"", "");
                arr2.add(val);
            }
            return arr2;
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
            return null;
        }
    }

    public static List<String> convertDBResultSetToArray(String[] arr) {
        try {
            List<String> arr2 = new ArrayList<>();
            String[] arr3 = null;
            String[] arr1 = null;
            String val = null;
            // break record sets into individual records
            for (int i = 0; i < arr.length; i++) {
                arr3 = arr[i].split(",");
                //break each record into attribute and value
                for (int j = 0; j < arr3.length; j++) {
                    if (arr3[j].contains(":")) {
                        arr1 = arr3[j].split(":");
                        val = arr1[1];
                        val = val.replace("}", "").replace("\"", "");
                    }
                }
                arr2.add(val);
            }
            return arr2;
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
            return null;
        }
    }

    public static void CheckAccountSubType(List<String> acctyp_db, List<String> acc_subtyp) {
        try {
            if (acctyp_db.size() == acc_subtyp.size()) {
                for (int i = 0; i < acctyp_db.size(); i++) {
                    if (acctyp_db.get(i).equals("01"))
                        Assert.assertTrue(acc_subtyp.get(i).equals("CurrentAccount"));
                    if (acctyp_db.get(i).equals("02"))
                        Assert.assertTrue(acc_subtyp.get(i).equals("SavingsAccount"));

                }
            } else {
                Assert.assertTrue(false);
                LOGGER.info("Assert Failed both arrays not of same size");
            }
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
        }
    }

//Compare accounts from DB query with Those in API response
    public static void NonPresenceOfAccounts(List<String> accts_db, List<String> accts_api) {
        try {
            boolean flag = false;
            List<String> List1 = new ArrayList<>();
            String[] arr1 = null;
            for (int i = 0; i < accts_db.size(); i++) {
                flag = false;
                arr1 = accts_db.get(i).split(":");
                List1.add(arr1[1].replace("}", "").replace("\"", ""));

                for (int j = 0; j < accts_api.size(); j++) {
                    if (accts_api.get(j) == List1.get(i))
                        flag = true;
                }
                Assert.assertTrue(flag == false);
            }
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
        }
    }
    //Compare accounts from DB query with Those in API response
    public static void PresenceOfAccounts(List<String> accts_db, List<String> accts_api) {
        try {
            boolean flag = false;
            List<String> List1 = new ArrayList<>();
            String[] arr1 = null;
            for (int i = 0; i < accts_db.size(); i++) {
                flag = false;
                arr1 = accts_db.get(i).split(":");
                List1.add(arr1[1].replace("}", "").replace("\"", ""));

                for (int j = 0; j < accts_api.size(); j++) {
                    if (accts_api.get(j) == List1.get(i))
                        flag = true;
                }
                Assert.assertTrue(flag == true);
            }
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
        }
    }

    public static void CheckList1NotInList2 (String s1,String s2) {
        try {
            String[] arr1 = null;
            String[] arr2 = null;
            s1 = s1.replace("[", "").replace("]", "");
            s2 = s2.replace("[", "").replace("]", "");
            arr1 = s2.split(",");
            arr2 = s2.split(",");
            for (int i = 0; i < arr1.length; i++) {
                boolean flag = false;
                for (int j = 0; j < arr2.length; j++) {
                    if (arr1[i] == arr2[j]) {
                        flag = true;
                        break;
                    }
                }
                Assert.assertTrue(flag == false);
            }
        } catch (Exception e) {
            LOGGER.info("Exception:" + e.getMessage());
        }
    }
}
