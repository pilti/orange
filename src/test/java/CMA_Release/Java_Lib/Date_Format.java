package CMA_Release.Java_Lib;


import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by C963294 on 15/12/2017.
 */
public class Date_Format {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Date_Format.class);

    //This method is for converting date time into acceptable format
    public static String return_date(String d1) {
        String date_format = d1;
        String date_string = date_format.substring(0, 10);
        String year = date_string.substring(0, 4);
        String month = date_string.substring(5, 7);
        String date = date_string.substring(8, 10);
        String month_val = "";
        String date_string_val = "";

        switch (month) {
            case "01":
                month_val = "January";
                break;
            case "02":
                month_val = "February";
                break;
            case "03":
                month_val = "March";
                break;
            case "04":
                month_val = "April";
                break;
            case "05":
                month_val = "May";
                break;
            case "06":
                month_val = "June";
                break;
            case "07":
                month_val = "July";
                break;
            case "08":
                month_val = "August";
                break;
            case "09":
                month_val = "September";
                break;
            case "10":
                month_val = "October";
                break;
            case "11":
                month_val = "November";
                break;
            case "12":
                month_val = "December";
                break;
        }

        date_string_val = date + " " + month_val + " " + year;
        return date_string_val;

    }

    //Get date as per API requirement
    public static String getFormattedDate(int year, int month, int day, int hour, int minute, int second) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Calendar cal = Calendar.getInstance();
        LOGGER.info(dateFormat.format(cal.getTime()));
        cal.add(Calendar.DAY_OF_MONTH, day);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);
        cal.add(Calendar.SECOND, second);
        String a = dateFormat.format(cal.getTime());
        String b = a.substring(20, 24);
        String c = a.substring(0, 20);
        c = c + b.substring(0, 2) + ':' + b.substring(2, 4);
        LOGGER.info(c);
        return c;
    }

    //Get Booking Date
    public static String getBookingDate(int year, int month, int day, int hour, int minute, int second) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//"2019-04-19T14:00:00+01:00"
        Calendar cal = Calendar.getInstance();
        LOGGER.info(dateFormat.format(cal.getTime()));
        cal.add(Calendar.DAY_OF_MONTH, day);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);
        cal.add(Calendar.SECOND, second);
        String a = dateFormat.format(cal.getTime());
        return a;
    }

    public static String compareTwoDates(String strDate1, String strDate2) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        java.util.Date d1 = sdf.parse(strDate1);
        java.util.Date d2 = sdf.parse(strDate2);

        if (d1.before(d2)) {
            return "Before";
        } else if (d1.after(d2)) {
            return "After";
        } else {
            return "Equal";
        }


    }
}
