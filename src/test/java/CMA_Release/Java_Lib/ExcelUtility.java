package CMA_Release.Java_Lib;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

/**
 * Created by C962653 on 23/01/2018.
 */
public class ExcelUtility {


    public static String setIntValue(String fileName, int row, int column, String value1) {

        Float value = Float.parseFloat(value1);
        FileInputStream fsIP = null;
        try {
            fsIP = new FileInputStream(new File(fileName));

            //Access the workbook
            XSSFWorkbook wb = new XSSFWorkbook(fsIP);
            //Access the worksheet, so that we can update / modify it.
            XSSFSheet worksheet = wb.getSheetAt(0);
            // declare a Cell object
            Cell cell = null;
            // Access the second cell in second row to update the value
            cell = worksheet.getRow(row).getCell(column);
            // Get current cell value value and overwrite the value
            cell.setCellValue(value);
            //Close the InputStream
            fsIP.close();
            //Open FileOutputStream to write updates
            FileOutputStream output_file = new FileOutputStream(new File(fileName));
            //write changes
            wb.write(output_file);
            //close the stream
            output_file.close();

            return "success";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "failed";
    }

    public static String setTextValue(String fileName, int row, int column, String value) {

        FileInputStream fsIP = null;
        try {
            fsIP = new FileInputStream(new File(fileName));

            //Access the workbook
            XSSFWorkbook wb = new XSSFWorkbook(fsIP);
            //Access the worksheet, so that we can update / modify it.
            XSSFSheet worksheet = wb.getSheetAt(0);
            // declare a Cell object
            Cell cell = null;
            // Access the second cell in second row to update the value
            cell = worksheet.getRow(row).getCell(column);
            // Get current cell value value and overwrite the value
            cell.setCellValue(value);
            //Close the InputStream
            fsIP.close();
            //Open FileOutputStream to write updates
            FileOutputStream output_file = new FileOutputStream(new File(fileName));
            //write changes
            wb.write(output_file);
            //close the stream
            output_file.close();

            return "success";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "failed";
    }


}


