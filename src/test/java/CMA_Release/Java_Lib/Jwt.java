package CMA_Release.Java_Lib;

import org.slf4j.LoggerFactory;

import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;


/**
 * Created by C963294 on 23/03/2018.
 */
public class Jwt {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Jwt.class);
    //creates JWT used in all consent and pre-auth test cases of AISP and PISP
    public static String JWT(String header, String payload, String path) throws IOException, ScriptException, NoSuchMethodException, GeneralSecurityException {
        if (path.contains("classpath")) {
            path = path.replace("classpath:", "./src/test/java/");
        }

        File f1 = new File(path);
        byte[] headerval = Base64.getEncoder().encode(header.getBytes());
        byte[] payloadval = Base64.getEncoder().encode(payload.getBytes());

        String head = new String(headerval);
        String pay = new String(payloadval);
        String s = head.replace("==", "") + "." + pay.replace("==", "");
        s = s.replace("=", "");

        if (path.length() > 0 && f1.exists()) {
            String str = signSHA256RSA(s, path);
            return s + "." + str.replace("==", "").replace("/", "_").replace("+", "-");
        } else {
            LOGGER.info("Path to Signing key is empty or Path doesnt exists!!!\n Partial token is being generated!!");
            return s + ".";
        }

    }

    //Encodes header and payload with signing key
    public static String signSHA256RSA(String data, String path) throws GeneralSecurityException, IOException {

        //Read key from path and convert in to string using PEM READER
       LOGGER.info("This is the path to Signing key : " + path);
        File f1 = new File(path);
        PrivateKey key = CMA_Release.Java_Lib.Crypto.SSL.PemReader.PKCS8(f1);
        String privatekey = Base64.getEncoder().encodeToString(key.getEncoded());

        // Convert in to key string in to BYTES
        byte[] b1 = Base64.getDecoder().decode(privatekey);

        //Create Specification with byte-format key
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);

        //Create signature with SHA256withRS Algorithm
        Signature privateSig = Signature.getInstance("SHA256withRSA");

        //Key Factory to hole RSA key
        KeyFactory kf = KeyFactory.getInstance("RSA");

        //Add Sing to signature
        privateSig.initSign(kf.generatePrivate(spec));
        privateSig.update(data.getBytes("UTF-8"));
        byte[] s = privateSig.sign();

        return Base64.getEncoder().encodeToString(s);


    }

    //Creates JWT for API
    public static String Create_JWT(String header, String payload, String key) throws NoSuchMethodException, ScriptException, GeneralSecurityException, IOException {

        String path = "./src/test/java/temp/temp.key";

        CreateFiles.write(path, key);
        try {
            String jwt = JWT(header, payload, path);
            return jwt;
        } catch (Exception e) {
            return "Error";
        }


    }
}
