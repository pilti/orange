package CMA_Release.Java_Lib;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class CreateFiles {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CopyFile.class);
    public static void write(String p, String contant) {
        try{
            // Create new file
            String content = contant;

            String path1 = p + "/";
            try {
                path1 = URLDecoder.decode(path1, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            File file = new File(path1);

            // If file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            // Write in file
            bw.write(content);

            // Close connection
            bw.close();
        }
        catch(Exception e){
            LOGGER.info(e.getMessage());
        }
    }
}