package CMA_Release.Java_Lib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class PdfToByteArray {

    public static  byte [] getByteArrayold() throws Exception {
        File file = new File("./src/test/java/CMA_Release/Java_Lib/ALM.pdf");
        FileInputStream fis = new FileInputStream(file);
        byte [] data = new byte[(int)file.length()];
        fis.read(data);
        //ByteArrayOutputStream bos = new ByteArrayOutputStream();
       //data = bos.toByteArray();
        return data;


    }
    public static  byte [] getByteArray() throws Exception {
        File file = new File("./src/test/java/CMA_Release/Java_Lib/ALM.pdf");
        FileInputStream fis = new FileInputStream(file);
        byte [] buf = new byte[(int)file.length()];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
                //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException ex) {
          //  Logger.getLogger(genJpeg.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] bytes = bos.toByteArray();


        return bytes;
    }
    public static void main(String[] args) throws Exception {
        System.out.println(getByteArray().toString());
    }
}

