package CMA_Release.Java_Lib;

/**
 * Created by C963294 on 19/02/2018.
 */

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonTree {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(JsonTree.class);
    public static Integer Gpass = 0;
    public static Integer Gfail = 0;

    public static String CMA_Ver = System.getProperty("CMA_Rel_Ver");
    public static String Env = System.getProperty("karate.env");

    //This method is to create json input fr Business report
    public static void jsonTree(String RunnerName) throws IOException {

        //////////////////////////Pre Condition//////////////////////////////////////////////////////

        String src = "./src/test/java/Resources/D3_business_viewer";
        String dest = "./target/D3_business_viewer";
        String src1 = "./target/testout/CMA_Release/Test_Set";
        String dest1 = "./target/D3_business_viewer/testout/CMA_Release/Test_Set";

        copyFolder(src, dest);
        copyFolder(src1, dest1);

        //////////////////Tree Logic/////////////////////////////////////////////////////////////////
        JSONObject jsonObject = new JSONObject();
        File currentDir = new File(dest1 + "/");
        jsonObject.put("name", currentDir.getName());
        jsonObject.put("children", displayDirectoryContents(currentDir));
        jsonObject.put("Gpass", Gpass);
        jsonObject.put("Gfail", Gfail);
        jsonObject.put("CMA_Ver",CMA_Ver);
        jsonObject.put("Env",Env);
        jsonObject.put("Runner",RunnerName);

        String filepath = "./target/D3_business_viewer/treeData.json";
        File nwfil = new File(filepath);
        if (nwfil.exists()) {
            nwfil.delete();
            FileWriter writer = new FileWriter(filepath, true);
            writer.write(jsonObject.toString());
            writer.close();
        } else {
            FileWriter writer = new FileWriter(filepath, true);
            writer.write(jsonObject.toString());
            writer.close();
        }
    }


    //This method is to create a json of directory and files structure
    public static List<Object> displayDirectoryContents(File dir) {
        List<Object> childrenList = new ArrayList<>();


        try {
            File[] files = dir.listFiles();
            int cnt = 0;


            for (File file : files) {

                if (file.isDirectory()) {
                    Map<String, Object> folderMap = new HashMap<>();
                    folderMap.put("name", file.getName());
                    List<Object> sublist = displayDirectoryContents(file);
                    folderMap.put("children", sublist);
                    childrenList.add(folderMap);
                } else {
                    Map<String, Object> filesMap = new HashMap<>();
                    filesMap.put("name", file.getName());

                    filesMap.put("url", file.getPath().replace(".\\target\\D3_business_viewer\\", ""));

                    //////////////////Code to fetch Status of test/////////////////
                    if (!file.getName().contains("jpg")) {
                        JSONParser parser = new JSONParser();
                        Object obj = parser.parse(new FileReader(file));
                        JSONObject jsonObject = (JSONObject) obj;
                        // Object obj1 = jsonObject.get("TestDescription");
                        // JSONObject jsonObject1 = (JSONObject) obj1;
                        String Status = (String) jsonObject.get("TestStatus");
                        //////////////////////////////////////////////////////////////
                        filesMap.put("TestStatus", Status.toLowerCase());

                        if ((Status.toLowerCase()).equals("pass")) {
                            Gpass = Gpass + 1;
                        }

                        if ((Status.toLowerCase()).equals("fail")) {
                            Gfail = Gfail + 1;
                        }

                    }

                    childrenList.add(filesMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return childrenList;
    }


    //Method to copy folder from one path to another
    public static void copyFolder(String src, String dest) {

        File source = new File(src);
        File destination = new File(dest);
        if (source.exists()) {
            if (destination.exists()) {
                try {
                    FileUtils.deleteDirectory(destination);
                    FileUtils.copyDirectory(source, destination);
                } catch (IOException e) {
                    LOGGER.info("The error is :" + e.getMessage());
                }


            } else {
                try {
                    FileUtils.copyDirectory(source, destination);
                } catch (IOException e) {
                    LOGGER.info("The error is :" + e.getMessage());
                }

            }
        } else {
            LOGGER.info("TestSet folder does not exists for generating Business report!!!");
        }
    }


}
