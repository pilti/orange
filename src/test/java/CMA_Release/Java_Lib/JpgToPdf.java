package CMA_Release.Java_Lib;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JpgToPdf {

    public static void main(String arg[]) throws Exception {
        File root = new File("./null/");
        String outputFile = "output.pdf";
        List<String> names = new ArrayList<String>(Arrays.asList(root.list()));
       // List<String> files = new ArrayList<String>();
        //files.add("2018-09-28T10_10_24.711_useKeyCodeApp.png");
        //files.add("1234.json");

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File(root, outputFile)));
        document.open();
        for (String f : names) {
            document.newPage();
            Image image = Image.getInstance(new File(root, f).getAbsolutePath());
            image.setAbsolutePosition(0, 0);
            image.setBorderWidth(0);
            image.scaleToFit(PageSize.A4);
            document.add(image);
        }
        document.close();
    }
}
