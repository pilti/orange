package CMA_Release.Java_Lib;

import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * Created by C963294 on 13/02/2018.
 */
public class PropertiesFileWritingReading {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PropertiesFileWritingReading.class);

    //This method is to write properties file
    public static String WriteFile(String path, String fileName, String id, String value) throws IOException {

        File f1 = new File(path + fileName);
        Properties prop = new Properties();

        OutputStream output = null;
        FileInputStream in = null;
        if (f1.exists()) {

            /////////////////////Code to Create Properties file Dynamically if doesn't exist//////////////////////
            FileWriter writer = new FileWriter(path + fileName, true);
            writer.write("");
            writer.flush();
            writer.close();
            //////////////////////////////////////////////////////////////////////////////////////////////////////
            in = new FileInputStream(path + fileName);
            prop = new Properties();
            prop.load(in);
            in.close();
            output = new FileOutputStream(path + fileName);
            // set the properties value
            prop.setProperty(id, value);
            // save properties to project root folder
            prop.store(output, null);
        } else {

            /////////////////////Code to Create Properties file Dynamically if doesn't exist//////////////////////
            File file = new File(path);
            if (file.exists()) {
                FileWriter writer = new FileWriter(path + fileName, true);
                writer.write("");
                writer.flush();
                writer.close();
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                in = new FileInputStream(path + fileName);
                prop = new Properties();
                prop.load(in);
                in.close();
                output = new FileOutputStream(path + fileName);
                // set the properties value
                prop.setProperty(id, value);
                // save properties to project root folder
                prop.store(output, null);
                return "str";
            } else {
                file.mkdirs();
                FileWriter writer = new FileWriter(path + fileName, true);
                writer.write("");
                writer.flush();
                writer.close();
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                in = new FileInputStream(path + fileName);
                prop = new Properties();
                prop.load(in);
                in.close();
                output = new FileOutputStream(path + fileName);
                // set the properties value
                prop.setProperty(id, value);
                // save properties to project root folder
                prop.store(output, null);
                return "str";
            }
        }
        return "str";
    }

    //This method is to read properties file
    public static String ReadFile(String path, String fileName, String id) throws IOException {
        File f1 = new File(path + fileName);
        if (f1.exists()) {
            Properties prop = new Properties();
            InputStream input = null;
            input = new FileInputStream(path + fileName);

            prop.load(input);

            if (prop.getProperty(id).contains("{")) {
                String result = prop.getProperty(id).replace("=", ":");
                return result;
            } else {
                String result = prop.getProperty(id);
                return result;
            }

        } else
        {
            return "{}";
        }
    }


}
