package CMA_Release.Java_Lib;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PDFGenerator {


    public static void imgToPDF(String path) throws Exception {
        File root = new File(path);
        String outputFile = "UI_Screen.pdf";
        List<String> names = new ArrayList<String>(Arrays.asList(root.list()));

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File(root, outputFile)));
        document.open();
        for (String f : names) {
            if(f.contains("png")) {
                document.newPage();
                Image image = Image.getInstance(new File(root, f).getAbsolutePath());
                image.setAbsolutePosition(0, 0);
                image.setBorderWidth(0);
                image.scaleToFit(PageSize.A4);
                document.add(image);
            }
        }
        document.close();
    }

    public static void main(String[] args) throws IOException, DocumentException {

        File dest = new File("./src/test/java/CMA_Release/Java_Lib/outputtxt.pdf");

        JSONObject jsonObj = new JSONObject("{\"phonetype\":\"N95\",\"cat\":\"WP\"}");
        copyFileUsingStream(jsonTopdf(jsonObj),dest);
    }


    public static void process(Document document, JSONObject json) throws JSONException, DocumentException {
            for (String k : json.keySet()) {
                Object object = json.get(k);
                if (object instanceof JSONArray) {
                    JSONArray list = json.getJSONArray(k);
                    process(document, list);
                } else if (object instanceof JSONObject) {
                    process(document, json.getJSONObject(k));
                } else {
                    document.add(new Paragraph(k + " " + json.get(k)));
                }

            }
        }

        public static void process(Document document, JSONArray json) throws JSONException, DocumentException {
            for (int x = 0; x < json.length(); x++) {
                Object object = json.get(x);
                if (object instanceof JSONArray) {
                    JSONArray list = json.getJSONArray(x);
                    process(document, list);
                } else if (object instanceof JSONObject) {
                    process(document, json.getJSONObject(x));
                } else {
                    document.add(new Paragraph(json.get(x).toString()));
                }

            }
        }

        public static File jsonTopdf(JSONObject json) throws IOException, DocumentException {

            Document document = new Document(PageSize.A4, 70, 55, 100, 55);
            File file = File.createTempFile("consulta", ".pdf");
            FileOutputStream output = new FileOutputStream(file);

            PdfWriter writer = PdfWriter.getInstance(document, output);
            //writer.setEncryption("a".getBytes(), "b".getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.STANDARD_ENCRYPTION_128);
            writer.createXmpMetadata();
            writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));


            document.open();
            document.addCreationDate();
            document.addTitle("documento");
            document.newPage();

            process(document, json);

            document.close();

            return file;
        }


    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    }

