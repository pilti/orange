package CMA_Release.Java_Lib;

import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by C963294 on 26/05/2018.
 */
public class CopyFile {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CopyFile.class);
    public static void copyFile(String sourcePath, String destinationPath) {
        String file_name = sourcePath.replaceAll(".*/", "");

        if (destinationPath.contains(file_name)) {
            LOGGER.info("Filename " + file_name + " exists in the path!!");
        } else {
            destinationPath = destinationPath + file_name;
            LOGGER.info(file_name);
        }

        if (sourcePath.contains("classpath") || destinationPath.contains("classpath")) {
            destinationPath = destinationPath.replace("classpath:", "./src/test/java/");
            sourcePath = sourcePath.replace("classpath:", "./src/test/java/");
        }

        File src = new File(sourcePath);
        File dest = new File(destinationPath);
        try {
            FileUtils.copyFile(src, dest);
            LOGGER.info("File copied at the desired location");
        } catch (IOException e) {
            LOGGER.info("File copy failed due to " + e.getMessage());
        }
    }
}
