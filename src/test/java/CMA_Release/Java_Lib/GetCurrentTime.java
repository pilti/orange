package CMA_Release.Java_Lib;


import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Created by C963294 on 19/12/2017.
 */
public class GetCurrentTime {
    //This method is to add delay to current time stamp
    public static String Time_stamp(long x) {
        Instant i = Instant.now();
        String time1 = i.plusSeconds(x).toString();
        String Time2 = time1.substring(0, 19);
        return Time2 + "+00:00";
    }
    public static String CurrSysTime(){
        Instant i = Instant.now();
        String time_stamp = i.toString().substring(0,16);
        return time_stamp;
    }
    public static String CurrentTimeStamp(){
        Instant i = Instant.now();
        String time_stamp = i.toString().substring(0,19);
        return time_stamp;
    }

}
