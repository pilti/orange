Feature:  Create Network key & Certificate Signing Request
          Create Signing key & Certificate Signing Request

  Input : Organisation and ClientID. This features require these inputs.
  Output : New folder (Organisation/ClientID) will be Created under Resources. Keys and CSR are saved at newly created folder

  Background:
    * def keycrs = Java.type('CMA_Release.Java_Lib.Crypto.SSL.CSR')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    # organizationalUnit(OnBehalfOfOrganisation) - change as per SSA
    * def orgUnit = "nBRcYAcACnghbGFOBk"
    # CommonName(ClientID) - change as per SSA
    * def CID = "5MXp8JTU0V2OEDluNDaFGt"
    * def outfolderpath = "src/test/java/Resources/Org_" + orgUnit + "/S_STMT/STMT_" + CID + "/"
    * def k = Java.type('CMA_Release.Java_Lib.createFolder')
    * def m = k.Folder_path(outfolderpath)

  Scenario: Create Network private key & Certificate Signing Request
    #Network
    # Generate keys and Certificate Signing Request using java
    * def kc = keycrs.getkeyandCSR(orgUnit,CID)
    * def w = fileWriter.write( outfolderpath + "transport_private.key", kc.PrivateKey)
    * def w = fileWriter.write( outfolderpath + "transport.csr", kc.CSR)
    * print karate.pretty(kc)

  Scenario: Create Signing private key & Certificate Signing Request
    #SelfSinging
    # Generate keys and Certificate Signing Request using java
    * def kc = keycrs.getkeyandCSR(orgUnit,CID)
    * def w = fileWriter.write( outfolderpath + "signin_private.key", kc.PrivateKey)
    * def w = fileWriter.write( outfolderpath + "signin.csr", kc.CSR)
    * print karate.pretty(kc)

