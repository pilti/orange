Feature: Create Keystore to for SSL

  Input : Organisation, ClientID and name of Openbanking approved certificate file (pem).
  Dependent :  Openbanking approved certificate file(pem)and
               Network private key are available at Resources folder as per Organisation and ClientID

  Output : Keystore will be Created under Resources as per Organisation and ClientID.

  Background:
    * def orgUnit = "Prod"
    * def CID = "5yyOsAuI5pLb9ciQQs2Ydt"
    * def pem_file = "transportTechnicalLiveProvingApp.pem"

  Scenario: Create Keystore for given Software Statement

    * def keystore = Java.type('CMA_Release.Java_Lib.Crypto.SSL.PemReader')
    * def path = "src/test/java/Resources/Org_" + orgUnit + "/S_STMT/STMT_" + CID + "/"
    * def ca_certificate = path + pem_file
    * def networkkey = path + 'transport_private.key'
    * def ks = keystore.loadKeyStore1(ca_certificate,networkkey)
    * def k = keystore.writekeystore(ks,path)

