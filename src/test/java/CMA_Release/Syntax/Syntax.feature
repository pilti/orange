Feature: This feature demonstrates how to implement for loop and conditional logic in karate

  Scenario: This scenario demonstrates how to use conditional logic in karate

    * def inputJson =
                    """
                      {
                          release: CMA1
                          env: SIT
                      }
                    """
    #below statement states if the value of inputJson.release is equal to CMA1 then assign value of logic_1 variable as
    # true else false
    * def logic_1 = (inputJson.release == 'CMA1' ? 'true' : 'false')
    * print karate.pretty(logic_1)

    #below statement states if the value of inputJson.release is not to CMA1 then assign value of logic_2 variable as
    # false else true
    * def logic_2 = (inputJson.release != 'CMA1' ? 'false' : 'true')
    * print karate.pretty(logic_2)

  Scenario: This scenario demonstrates how to use if conditional statement in karate

    * def logic_1 = null
    * def logic_2 = null
    * def inputJson =
                    """
                      {
                          release: CMA1,
                          env: SIT
                      }
                    """
    #below statement states if the value of inputJson.release is equal to CMA1 then assign value of logic_1 variable as hello

    * eval
      """
         if(inputJson.release == 'CMA1')
            {
                logic_1 = 'hello'
            }
       """
    * print karate.pretty(logic_1)

    #below statement states if the value of inputJson.release is equal to CMA1 then assign value of logic_2 variable as world else xxxx

    * eval
      """
        if(inputJson.release == 'CMA1')
            {
              logic_2 = 'world'
            }
        else
            {
              logic_2 = 'xxxx'
            }
      """
    * print karate.pretty(logic_2)

  Scenario: This scenario demostrates how to use for loop

    #Below array contains json holding 2 keys,namely env and RT
    #This scenario will demostrate how to create arrays of RT based on the env key for every element in the profile array
    * def profile =
                    """
                    [
                      {
                        env:SIT,
                        RT:new1
                      },
                      {
                        env:SIT,
                        RT:new2
                      },
                      {
                        env:UAT,
                        RT:new3
                      },
                      {
                        env:preProd,
                        RT:new4
                      },
                      {
                        env:PIT,
                        RT:new5
                      },
                      {
                        env:preProd,
                        RT:new6
                      }
                    ]
                    """

    #We will have 4 different arrays that will store value of RT from profile based on env
    * eval var preProd_arr = []
    * eval var SIT_arr = []
    * eval var UAT_arr = []
    * eval var PIT_arr = []

    #Logic and syntax to write for loop in karate(This is javascript code)
    * eval
    """
      for(var i = 0 ;i < profile.size(); i++)
      {
           if(profile[i].env == "preProd")
             preProd_arr.push(profile[i].RT)

           if(profile[i].env == "SIT")
              SIT_arr.push(profile[i].RT)

           if(profile[i].env == "UAT")
              UAT_arr.push(profile[i].RT)

          if(profile[i].env == "PIT")
              PIT_arr.push(profile[i].RT)

      }
     """

    #Printing arrays obtained from above logic
    * print "PIT array is: "
    * print PIT_arr
    * print "UAT array is: "
    * print UAT_arr
    * print "SIT array is: "
    * print SIT_arr
    * print "preProd array is: "
    * print preProd_arr

