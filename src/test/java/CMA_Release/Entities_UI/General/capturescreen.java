package CMA_Release.Entities_UI.General;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.Instant;

public class capturescreen {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(capturescreen.class);
    //This is flag to set capture screen on or off
    public static String str = System.getProperty("screenlogs");

    //This method will capture screenshot of entire page and requires screenshot PATH as app.path1
    public static void captureScreenShot2(GetApplication app) {

        if (str.equals("Yes")) {
            if (StringUtils.isBlank(app.path1)) {
                LOGGER.info("Path for screenshots not set");
            } else {
                try {
                    app.path1 = URLDecoder.decode(app.path1, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            String dr = app.path1 + "/";
            File file = new File(dr);
            if (!file.exists()) {
                file.mkdirs();
                LOGGER.debug("Directory is been Created!");
            } else {
                LOGGER.debug("Directory already exists!!!!");
            }
            try {
                Instant i = Instant.now();
                String t = i.toString().replace("Z", "_");
                String c = t.replace(":", "_");

                Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(app.driver1);
                ImageIO.write(screenshot.getImage(), "JPG", new File(dr + c + app.moduleName1 + ".jpg"));

            } catch (
                    IOException e
                    )

            {

                LOGGER.debug(e.getMessage());

            }
        } else {
            LOGGER.info("Screenshot flag is set to No! Screenshots will not be taken");}

    }

    //This method will capture screenshot of focused part of the page and requires screenshot PATH as app.path1
    public static void TakeScreenShot(GetApplication app) {
        if (str.equals("Yes")) {

            if (StringUtils.isBlank(app.path1)) {
                LOGGER.info("Path for screenshots not set");
            } else {
                try {
                    app.path1 = URLDecoder.decode(app.path1, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            String dr = app.path1 + "/";
            File file = new File(dr);
            File DestFile = null;
            try {
                if (!file.exists()) {
                    file.mkdirs();
                    LOGGER.debug("Directory is been Created!");
                } else {
                    LOGGER.debug("Directory already exists!!!!");
                }

                Instant i = Instant.now();
                String t = i.toString().replace("Z", "_");
                String c = t.replace(":", "_");
                DestFile = new File(dr + c + app.moduleName1 + ".jpg");

                TakesScreenshot scrShot = ((TakesScreenshot) app.driver1);
                File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(SrcFile, DestFile);

            } catch (Exception e) {
                LOGGER.debug("Error is due to: " + e.getMessage());
            }
        } else {
            LOGGER.info("Screenshot flag is set to No! Screenshots will not be taken");
        }
    }
}

