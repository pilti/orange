package CMA_Release.Entities_UI.General;

import CMA_Release.Entities_UI.Browsers.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;

public class GetApplication {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(GetApplication.class);
    public String t_name1;
    public String path1;
    public WebDriver driver1;
    public String browser1;
    public java.lang.String moduleName1;
    public ChromeDriverService chrome_service1 = new ChromeDriverService.Builder()
            .usingDriverExecutable(new File("./src/Z_Drivers/chromedriver2.41.exe"))
            .usingAnyFreePort()
            .build();
    public PhantomJSDriverService pjs_service1 = new PhantomJSDriverService.Builder()
            .usingPhantomJSExecutable(new File("./src/Z_Drivers/phantomjs/bin/phantomjs.exe"))
            .usingAnyFreePort()
            .build();

    //This method is to initiate browser
    public WebDriver start1(String browser) throws IOException {
        browser1 = browser;
        if (browser == "PJS") {
            LOGGER.info("Browser {} Requested for test {} ", browser, t_name1);
            try {
                //driver1 = BrowserFactory.getPhantomJS(pjs_service1, "PJS");
            } catch (Exception e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Error initiating {} browser with error {}", browser, e);
                }
            }
        } else {
            LOGGER.info("Browser {} Requested for test {} ", browser, t_name1);
            ;
            try {
                driver1 = BrowserFactory.getChrome(chrome_service1, "Chrome");
            } catch (Exception e) {
                System.out.print("error" +e);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Error initiating {} browser with error {}", browser, e);
                }
            }
        }
        return driver1;
    }

    //This method is to stop browser
    public void stop() {
        if (browser1 == "Chrome") {
            driver1.close();
            driver1.quit();
            chrome_service1.stop();
        }
        if (browser1 == "PJS") {
            driver1.close();
            driver1.quit();
            pjs_service1.stop();
        }

    }
}
