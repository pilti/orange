package CMA_Release.Entities_UI.Browsers;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

    public class iphonesimulator {

        static RemoteWebDriver driver;
        DesiredCapabilities capabilities;

        @Before
        public void setUp() throws Exception {
            capabilities = DesiredCapabilities.safari();
            capabilities.setCapability("automationName", "XCUITest");
            capabilities.setCapability("platformName", "iOS");
            capabilities.setCapability("platformVersion", "10.11");
            capabilities.setCapability("deviceName", "iPhone 6");
            capabilities.setCapability("browserName", "Safari");
            capabilities.setCapability("launchTimeout", 120000);
            //capabilities.setCapability("appPackage", "\"C:\\Orange\\newe2e\\src\\test\\java\\Resources\\mobilePackages\\Android\\Bank_Of_Ireland_Android_App.apk\"");
            URL url = new URL("http://10.64.146.90:4725/wd/hub"); //IP Address of Appium Server
            driver = new RemoteWebDriver(url, capabilities);
            driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

        }

        @Test
        public void webTest1() {
            String email = "adkajdaks@sa.com";
            driver.get("https://www.boi.com");
            driver.findElement(By.xpath("//*[@class='mhf-links']/a[2]")).click();
            getElement(By.xpath("//*[text()='Register']")).click();
            getElement(By.id("firstname")).sendKeys("abcd");
            getElement(By.id("lastname")).sendKeys("adftyh");
            getElement(By.xpath("//input[@id='email']")).sendKeys(email);
            getElement(By.xpath("//input[@id='remail']")).sendKeys(email);
            getElement(By.id("PASSWORD")).sendKeys("pass@1234");
            System.out.println("passed");
        }

        WebElement getElement(By locator) {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return element;

        }

        @After
        public void tearDown() {
            driver.quit();
        }
    }

