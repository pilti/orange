package CMA_Release.Entities_UI.Browsers;

import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.ie.InternetExplorerOptions;

public class BrowserFactory {

    private static Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();
    public static WebDriver driver;
    public static String selenium_grid = System.getProperty("selenium_grid");

    public static WebDriver getChrome(ChromeDriverService s, String st) throws IOException{

        if( selenium_grid.equalsIgnoreCase("Yes"))
            driver =  getChromeGrid(st);
        else
            driver =  getChromeLocal( s, st);

        return driver;
    }

    public static WebDriver getIE() throws MalformedURLException {
        driver = drivers.get("IE");
     //   InternetExplorerOptions ieOption = new InternetExplorerOptions();
      //  ieOption.setCapability("platform", "windows");
     //   driver = new RemoteWebDriver(new URL("http://adsci208:5555/wd/hub"), ieOption);
        driver.manage().window().maximize();
        return driver;
    }

    public static void main(String[] args) throws MalformedURLException {
        WebDriver d = getIE();
        d.get("https://boi.com");
    }
    //This method is to initiate Chrome browser
    public static WebDriver getChromeLocal(ChromeDriverService s, String st) throws IOException {
        driver = drivers.get("Chrome");
        s.start();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        options.setExperimentalOption("useAutomationExtension", false);
        //options.addArguments("--headless");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability
                (CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                        UnexpectedAlertBehaviour.ACCEPT);
        capabilities.setCapability("chrome.switches",
                Arrays.asList("--ignore-certificate-errors"));

        driver = new ChromeDriver(s, capabilities);
        drivers.put("Chrome", driver);
        //driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getChromeGrid(String st) throws IOException {
        driver = drivers.get("Chrome");
        String nodeURL = System.getProperty("SeleniumHubUrl");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        options.setExperimentalOption("useAutomationExtension", false);
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        capability.setBrowserName("chrome");
        capability.setPlatform(Platform.WINDOWS);
        driver = new RemoteWebDriver(new URL(nodeURL), capability);
        drivers.put("Chrome", driver);
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver iphone() throws IOException {
        DesiredCapabilities capabilities;
        capabilities = DesiredCapabilities.safari();
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "8.4");
        capabilities.setCapability("deviceName", "iPhone 6");
        capabilities.setCapability("browserName", "Safari");
        capabilities.setCapability("launchTimeout", 120000);
        URL url = new URL("http://10.64.146.90:4725/wd/hub"); //IP Address of Appium Server
        driver = new RemoteWebDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        return driver;
    }

    public static WebDriver Android() throws IOException {
        //Set up desired capabilities and pass the Android app-activity
        //and app-package to Appium
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("BROWSER_NAME", "chrome");
        capabilities.setCapability("VERSION", "4.4.2");
        capabilities.setCapability("deviceName","emulator64-arm");
        capabilities.setCapability("platformName","Android");

        // This package name of your app (you can get it from apk info app)
       capabilities.setCapability("appPackage", "\"C:\\Orange\\newe2e\\src\\test\\java\\Resources\\mobilePackages\\Android\\ANDROID-1.5.apk\"");

        // This is Launcher activity of your app (you can get it from apk info app)
       // capabilities.setCapability("appActivity","com.android.calculator2.Calculator");

        //Create RemoteWebDriver instance and connect to the Appium server
        //It will launch the Calculator App in Android Device using the configurations
        //specified in Desired Capabilities
        driver = new RemoteWebDriver(new URL("http://10.64.146.90:4723/wd/hub"), capabilities);
        return driver;
    }

    //This method is to close All browser
    public static void closeAllDriver() {
        for (String key : drivers.keySet()) {
            drivers.get(key).close();
            drivers.get(key).quit();
        }

    }

}

