package CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;
//This is Page object Model for Review_confirm Page of PISP
public class Review_confirm {

    public static final By confirm_chk = id("accpt_cond");
    public static final By confirm_payment = xpath("//button[text()='Confirm payment']");
    public static final By back_payment = xpath("//button[text() = \"Back\" ]");
    public static final By cancel_Button = xpath("//button[text() = \"Cancel\" ]");
    public static final By cancel_popUp = xpath("//span[text()=\"Cancel request\"]");
    public static final By cancel_No= xpath("// button [@ng-click=\"cancelBtnClicked()\"]");
    public static final By table_review = xpath("//div[@class=\"row content-container\"]//table");
    public static final By cancel_Yes= xpath("// button [@ng-click=\"okBtnClicked()\"]");
    public static final By payee_detail_header = xpath("//div[@class=\"row content-container\"]//tr/th");
    public static final By payee_detail_value = xpath("//div[@class=\"row content-container\"]//tr/td");
    public static final By timeout = xpath("//h2[text() = \"Session timeout\"]");
    public static final By refreshErr = xpath("//div[@role=\"alert\"]");
    public static final By confirm_text = xpath("//div/label[contains(text(),'I confirm')]");
}


