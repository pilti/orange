package CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963294 on 05/12/2017.
 */
//This is Page object Model for Account selection Page of PISP


public class Select_Debit_Account {
    public static final By table = xpath("//div[@class=\"row content-container\"]//table");
    public static final By payee_detail_header = xpath("//div[@class=\"row content-container\"]//tr/th");
    public static final By payee_detail_value = xpath("//div[@class=\"row content-container\"]//tr/td");
    public static final By timeout = xpath("//h2[text() = \"Session timeout\"]");
    public static final By refreshErr = xpath("//div[@role=\"alert\"]");
    public static final By select_acc = xpath("//button[text()='Select account']");
    public static final By drop_down_list = xpath("//button[text()='Select account']/following-sibling::ul/li/a");
    public static final By continue_button = xpath("//button[text()='Continue']");
    public static final By textBelowAccountSelection = xpath("//h4[contains(@class,'main-header page-description ng-binding')]");
    public static final By invalidUserError = xpath("//span[text()='No eligible accounts for this request.']");
    public static final By payeeInitiatedByText = xpath("//td[contains(@ng-bind,'payInstBy')]");
    public static final By payeeNameText = xpath("//td[contains(@ng-bind,'payeeName')]");
    public static final By amountText = xpath("//td[contains(@ng-bind,'amount | isoCurrency:currency')]");
    public static final By payeeReferenceText = xpath("//td[contains(@ng-bind,'payeeReference')]");
    public static final By payFromList = xpath("//button[contains(@ng-bind,'selectedAcctObject?(selectedAcctObject.Nickname)+(selectedAcctObject.Account.Identification | maskNumber:maskAccountNumberLength):selectAccountText')]");
    public static final By cancel_button = xpath("//button[text()='Cancel']");
    public static final By review_confirm = xpath("//h3[text()='Review and confirm']");
    public static final By cancel_popUp = xpath("//span[text()=\"Cancel request\"]");
    public static final By cancel_No = xpath("// button [@ng-click=\"cancelBtnClicked()\"]");
    public static final By cancel_Yes = xpath("// button [@ng-click=\"okBtnClicked()\"]");
    public static final By account_selection = xpath("//h3[text()='Account selection']");
    public static final By return_to_TPP = xpath("//button[text()='Return to third party']");
}
