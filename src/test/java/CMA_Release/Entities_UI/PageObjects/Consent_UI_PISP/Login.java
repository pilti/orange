package CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;
//This is Page object Model for Login Page of PISP
public class Login {
    public static final By username = id("username");
    public static final By pwd = id("pwd");
    public static final By continue_button = xpath("//button[text()='Continue']");
}
