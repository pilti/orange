package CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP;

import org.openqa.selenium.By;
import static org.openqa.selenium.By.xpath;



/**
 * Created by C965116 on 23/04/2018.
 */
//This class is Page object model for Refresh TokenRenewal Page of AISP

public class RefreshTokenRenewal {

    public static final By refreshTokenRenewalHeading = xpath("//*[text()='Authorisation renewal']");
    public static final By refreshTokenRenewalTextPart1 = xpath("//*[text()='Authorisation renewal']//following::span[1]");
    public static final By refreshTokenRenewalTextPart2= xpath("//*[text()='Authorisation renewal']//following::span[2]");
    public static final By refreshTokenRenewalTextPart3 = xpath("//*[text()='Authorisation renewal']//following::span[3]");
    public static final By refreshTokenRenewalTextPart4 = xpath("//*[text()='Authorisation renewal']//following::span[4]");
    public static final By refreshTokenRenewalTextPart5 = xpath("//*[text()='Authorisation renewal']//following::span[5]");
    public static final By denyButton = xpath("//button[text()='Deny authorisation']");
    public static final By renewButton = xpath("//button[text()='Renew authorisation']");
    public static final By selectedAccountNames = xpath("//caption[text()='Selected account']/following-sibling::tbody/tr[not(contains(@class,'collapse'))]/td[contains(@ng-bind,'acct.Account.Identification')]");
    public static final By selectedAccountNamesAll = xpath("//caption[text()='Selected account']/following-sibling::tbody/tr/td[contains(@ng-bind,'acct.Account.Identification')]");
    public static final By selectedPermissions = xpath("//ul[@class='permission-list']/li");
    public static final By ImpInfoheading = xpath("//*[text()='Important Information']");
    public static final By ImpInfoText1 = xpath("//*[text()='You can revoke access to this service through the third party provider or by calling Bank of Ireland.']");
    public static final By ImpInfoText2 = xpath("//*[text()='We will provide permissions information to the extent that it is currently available on our digital banking channels.']");
    public static final By consentValidityText = xpath("//*[text()='Requested transactions access']//following::*[text()='This consent is valid until']");
    public static final By consentValidityDate = xpath("//*[text()='This consent is valid until']//following::span[1]");
    public static final By requestedtransactionaccess = xpath("//*[text()='Requested transactions access']");
    public static final By transactionFromDate = xpath("//*[text()='From date:']/following-sibling::span");
    public static final By transactionEndDate = xpath("//*[text()='To date:']/following-sibling::span");
    public static final By denybutton = xpath("//button[text()='Deny authorisation']");
    public static final By denyPopUpHeading = xpath("//*[text()='Deny authorisation']");
    public static final By denyPopUpmsg = xpath("//div[@class='modal-body pop-confirm-body']//p");
    public static final By goBackbutton = xpath("//button[text()='Go back']");
    public static final By confrimDenybutton = xpath("//button[text()='Yes, deny']");
    public static final By closeSymbol = xpath("//*[@title ='Close']");
    public static final By timeoutheading = xpath("//*[text()='Session timeout']");
    public static final By timeoutMsg = xpath("//*[text()='Your session has timed out and you must return to the third party site.']");
    public static final By timeoutReturnButton = xpath("//button[text()='Return to third party']");
    public static final By errorMsg = xpath("//*[text()='error']//p");
    public static final By loginErrorOnDiffuser = xpath("//*[text()='Un-authorized user to run the renewal flow.']");
}

