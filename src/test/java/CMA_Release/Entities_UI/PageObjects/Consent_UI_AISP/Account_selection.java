package CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

//This class is Page object model for Account Selection Page of AISP
public class Account_selection {

    public static final By Check_all_checkbox = xpath("//input[@id='checkall']");
    public static final By td_accnt_tb_data_chck = xpath("//div[3]//div[1]/table/tbody/tr/td[1]/div/input");
    public static final By td_accnt_tb_data_cnt = xpath("//div[3]//div[1]/table/tbody/tr/td[2]");
    public static final By sel_accnt_tb_data_cnt = xpath("//div[4]//div[1]/table/tbody/tr/td[2]");
    public static final By sel_accnt_txt = xpath("//h4[@class = 'ng-binding'][ @ng-bind ='selectedAccountText']");
    public static final By sel_accnt_tb_data_cross = xpath("//div[4]//div[1]/table/tbody/tr/td[5]");
    public static final By cancel_button = xpath("//button[text()='Cancel']");
    public static final By continue_button = xpath("//button[@class ='btn btn-primary pull-right ng-binding'][@ng-bind='continueBtn']");
    public static final By td_selected_tb_data_cnt = xpath("//div[4]//div[1]/table/tbody/tr/td");
    public static final By cancel_request = xpath("//button[@class='btn btn-primary pull-right ng-binding'][@ng-bind=\" modal.btn.okbtn.label\"]");
    public static final By NO = xpath("//button[@class='btn btn-secondary pull-left ng-binding'][@ng-click=\"cancelBtnClicked()\"]");
    public static final By paginationNext = xpath("//a[@aria-label=\"Next\"]");
    public static final By paginationPrevious = xpath("//a[@aria-label=\"Previous\"]");
    public static final By selectAllButton = xpath("/html/body/div[1]/div/section/div[3]/div/div[1]/button[1]");
    public static final By selectAccountsHeadding = xpath("/html/body/div[1]/div/section/div[4]/div/div[1]/h4");
    public static final By checkBoxCnt = xpath("//div[3]/div/div[2]/div/div[1]/table/tbody/tr/td[1]/div[1]/label");
    public static final By AccountSelectionPage = xpath("//h3[@class=\"ng-binding\"][@ng-bind=\"accountSelText\"]");
    public static final By AccountSelectionPage_selected = xpath("//*[@id=\"selected-table\"]/div/div[2]/div/div[1]/table/tbody/tr[*]/td[2]");
    public static final By AccountDataTable = xpath("//div[contains(@class,'table-view')]/table[@id='table-data']/tbody/tr");
    public static final By SelectedAccountDataTable = xpath("//div[contains(@class,'table-view-selected')]/table[@id='table-data']/tbody/tr");
    public static final By ReviewDataTable = xpath("//div[contains(@class,'account-review-table')]/table/tbody/tr");
    public static final By buttonShowAll = xpath("//button[contains(@class, 'btn btn-default')]/span/i[@class='fa fa-plus']");



}
