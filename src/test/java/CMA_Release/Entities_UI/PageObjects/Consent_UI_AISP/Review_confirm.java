package CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

//This class is Page object model for Review Confirm Page of AISP
public class Review_confirm {

    public static final By Permission_list_tg_name = xpath("//span[@class='consent-header boi-accname ng-binding ng-scope']");
    public static final By Check_box_confirm = xpath("//input[@id='accpt_cond'][ @aria-label=\"Confirm button enable\"]");
    public static final By Check_box_confirm1 = xpath("/html/body/div[1]/div/section/div[7]/div/div/div/input");
    public static final By Back_button = xpath("//button[@class ='btn btn-secondary pull-left ng-binding'][@ng-bind ='BACK_BUTTON']");
    public static final By Cancel_button = xpath("//button[@class =\"btn btn-secondary pull-left ng-binding\"][@ng-bind =\"CANCEL_BUTTON\"]");
    public static final By Confirm_button = xpath("//button[@class =\"btn btn-primary pull-right ng-binding\"][@ng-bind =\"CONFIRM_BUTTON\"]");
    public static final By Account_nickname = xpath("//div[3]//div[1]/table/tbody/tr/td");
    public static final By Permission_headding = xpath("//ul[@class='permission-list']/li//span[@ng-bind='perm_list']");
    public static final By Permission_headding_desc = xpath("//li//span[@id=\"collapsAccrd\"]");
    public static final By Permission_headding_dropdown = xpath("//li//i[@class=\"fa ng-scope fa-plus\"]");
    public static final By From_Date_Label = xpath("//span[@class=’boi-input from-date ng-binding’]");
    public static final By To_Date_Label = xpath("//span[@class=’boi-input to-date ng-binding’]");
    public static final By All_Date = xpath("//span[@class='boi-input date-data ng-binding']");
    public static final By show_all = xpath("//i[@class=\"fa fa-plus\"]");
    public static final By No_Cancel = xpath("//button[@class='btn btn-secondary pull-left ng-binding']");
    public static final By Yes_Cancel = xpath("//button[@class=\"btn btn-primary pull-right ng-binding\"][@role=\"button\"][@ng-click=\"okBtnClicked()\"]");
    public static final By Selected_Account = xpath("//div[3]//div[1]/table/tbody/tr/td[1]");
    public static final By PageName = xpath("//h3[@class=\"ng-binding\"][@ng-bind=\"REVIEW_CONFIRM\"]");
    public static final By show_less = xpath("//*[text()='Show less']");
    public static final By consent_validity = xpath("//div[contains(@class,'consent-validity-section')]/span[2]");

}




