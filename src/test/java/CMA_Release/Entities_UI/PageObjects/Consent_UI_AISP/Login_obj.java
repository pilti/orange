package CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

/**
 * Created by C963294 on 29/11/2017.
 */
//This class is Page object model for Login Page of AISP
public class Login_obj {

    public static final By have_key_app_bttn = cssSelector("body > div.modal.pop-confirm.fade.ng-scope.ng-isolate-scope.in > div > div > div > div > div > div.modal-footer.pop-confirm-footer.button-section.xs-button-section.footer-lightbox1 > div > button.btn.btn-lightbox1.btn-left > span.ng-binding");
    public static final By correlation = xpath("//input[@id=\"correlationId\"][@name=\"correlationId\"]");
    public static final By dont_have_key_app_bttn = xpath("//span[contains(text(), \"I don't have the Bank\")]");
    public static final By page_click = xpath("//body");
    public static final By username_text = xpath("//input[@id = \"username\"]");
    public static final By otp_text = xpath("//input[@id = \"pwd\"]");
    public static final By back_to_tpp_button = xpath("//button[@class =\"btn btn-secondary ng-binding\"] [@ng-bind =\"backtoTPPText\"][text()=\"Back to Third Party\"]");
    public static final By continue_button = xpath("//button[@class =\"btn btn-primary pull-right ng-binding\"] [@ng-bind =\"continueText\"][text()=\"Continue\"]");
    public static final By error_msg = xpath("//span[@class='ng-binding'][@ng-bind='errorMsgText']");
    public static final By error_msg_line2 = xpath("//p[contains(@class, 'message')]/following::p");
    public static final By preAuth_error_text = xpath("//div[@class='details-container']//p/span[1]");
    public static final By preAuth_error_text1 = xpath("//div[@class='details-container']//p/span[2]");

    public static final By SCA_error_text = xpath("//span[text()='Error:']/following-sibling::span");
    public static final By Error_text1 = xpath("//section//div[@class='alert page-alert']//p[1]//span[2]");
    public static final By Error_text2 = xpath("//section//div[@class='alert page-alert']/p[2]");
    public static final By error_msg_signing = xpath("//section//div[@class='alert page-alert']/p[2]");
    public static final By boilogo = xpath("//img[@class=\"logo ng-scope\"][@title=\"Bank of Ireland logo\"]");

    public static final By portalName = xpath("//h1[text()='Account Access']\n");
    public static final By regulatoryText = xpath("//p[@class=\"regulatory-statement ng-binding\"]");

    public static final By portalthirdpartytext = xpath("//h2[text()='Manage third party access to your Bank of Ireland accounts']");

    public static final By LoginHeaderText = xpath("//h3[text()='Login here to manage third party access to your Bank of Ireland accounts']");
    public static final By UsernameTextboxlabel = xpath("//label[text()='365 User ID']");
    public static final By cross_lightbox = xpath("//button[@type=\"button\"][@class=\"close\"]");

    public static final By UsernameinFieldText = xpath("//input[@id=\"username\"][@ng-model=\"userId\"][@placeholder=\"User ID\"]");
    public static final By PasswordTextboxlabel = xpath("//label[text()='Password']");
    public static final By PasswordinFieldText = xpath("//input[@ng-model=\"passcode\"][@placeholder=\"Generated in Bank of Ireland KeyCode app\"]");


    public static final By AboutUsLink = xpath("//span[text()='About us']");
    public static final By CookiePrivacy = xpath("//span[text()='Cookie and Privacy policy ']");
    public static final By TermsConditions = xpath("//span[text()='Terms and conditions']");
    public static final By Help = xpath("//span[text()='Help']");


    public static final By lightbox1 = xpath("//*[contains(@class,'modal-body pop-confirm')]");
    public static final By lightbox2 = xpath("//div[@class=\"modal-body  lightbox2 ng-scope\" ]");
    public static final By lightbox3 = xpath("/html/body/div[1]/div/div/div/div/div");

    public static final By lightbox1HeadingText = xpath("//*[contains(@class,'modal-body pop-confirm')]//p");
    public static final By lightbox2HeadingText = xpath("//p[@ng-bind='modelPopUpConf.lightBox2Description']");
    public static final By lightbox3HeadingText = xpath("/html/body/div[1]/div/div/div/div/div/div[1]");

    public static final By lightbox2CTA1 = xpath("//span[@ng-bind=\"modelPopUpConf.lightBox2ReturnToThirdParty\"]");
    public static final By lightbox2CTA2 = xpath("//span[@ng-bind=\"modelPopUpConf.lightBox2RegisterKeycodeApp\"]");
    public static final By lightbox2CTA3 = xpath("//span[@ng-bind=\"modelPopUpConf.lightBox2ContinueWithLogin\"]");

    public static final By lightbox3CTA1 = xpath("/html/body/div[1]/div/div/div/div/div/div[2]/button[1]");
    public static final By lightbox3CTA2 = xpath("/html/body/div[1]/div/div/div/div/div/div[2]/button[2]");


    public static final By lightbox1CTA1 = xpath("//span[contains(@ng-bind,'HaveBoiKeycodeApp')]");
    public static final By lightbox1CTA2 = xpath("//span[contains(@ng-bind,'dontHaveBoiKeycodeApp')]");

    public static final By timeoutpopup = xpath("//p[@class=\"text-center ng-binding\"][@ng-bind=\"sessionDescText\"]");
    public static final By returnToThirdParty = xpath("//button[text()='Return to third party']");

    public static final By NotebelowPasswordfield = xpath("//form[@id='loginForm']/p");
    public static final By NeedHelplink = xpath("//span[@ng-bind=\"needHelpText\"][text()=\"Need help?\"]");
    public static final By RightpanelLabel = xpath("//h4[text()=\"Further information\"]");
    public static final By BOIKeyCodeHeader = xpath("//h5[text()=\"Bank of Ireland KeyCode\"]");
    public static final By Registerkeycodeappheader = xpath("//h5[text()=\"Registering KeyCode app\"]");
    public static final By TextunderBOIkeyCodeHeader1 = xpath("//div[@ng-show=\"furtherText.length\"]/p");
    public static final By TextunderBOIkeyCodeHeader2 = xpath("//div[@ng-show=\"furtherText.length\"]/p[2]");
    public static final By TextunderRegkeyCodeappHeader = xpath("//div[@ng-show=\"furtherText.length\"]/ul");
    public static final By KeyCodehelpLink = xpath("//span[text()=\"KeyCode help \"]");
    public static final By loginlinkonrightPanel = xpath("//span[text()=\"Login to 365 online now\"]");
    public static final By lightbox3Head = xpath("/html/body/div[3]/div[1]/header/div");
    public static final By UserIDToolTipText = xpath("//label[text()='365 User ID']/following::span[@class='help-holder']/div");
    public static final By UserIDToolTip = xpath("//label[text()='365 User ID']/following::span[@class='help-holder']/a/i");
    public static final By PasswordToolTip = xpath("//label[text()='Password']/following::span[@class='help-holder']/a/i");
    public static final By PasswordToolTipText = xpath("//label[text()='Password']/following::span[@class='help-holder']/div");

}



