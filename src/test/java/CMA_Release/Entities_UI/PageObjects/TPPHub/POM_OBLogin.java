package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 27/03/2018.
 */
public class POM_OBLogin {

    public static final By OBLoginUsername_Input = xpath("//input[@id='username']");

    public static final By OBLoginPassword_Input = xpath("//input[@id='password']");
    public static final By OBLoginSignOn_Button = xpath("//*[@title='Sign On']");
    public static final By OBLoginChangePassword_Button = xpath("//a[text()='Change Password?']");
    public static final By OBLoginTroubleshootSignOn_Button = xpath("//a[text()='Trouble Signing On?']");
    public static final By MailUsrname_Text= xpath("//input[@id='username']");
    public static final By MailPasswd_Text= xpath("//input[@id='password']");
    public static final By MailSubmit_Button=xpath("//*[@class='signinbutton']");
    //public static final By MailSubmit_Button= By.cssSelector("html body.signInBg form div#mainLogonDiv.mouse div.logonContainer div#lgnDiv.logonDiv div.signInEnter div.signinbutton span.signinTxt");
    public static final By MailMain_Section= xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[1]/div[2]/div/div[5]/div[2]/div/div/div[2]/div/div[2]/div/div/div/div[1]/span/div/span[1]");
    public static final By MailInbox_Section= xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[1]/div[2]/div/div[5]/div[2]/div/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div[1]/span/div/span[1]");
    public static final By MaiLSrchFolders_Section= xpath("//html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[1]/div[2]/div/div[5]/div[2]/div/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div[2]/div[*]");
    public static final By MailOTP_Click= xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[4]/div/div/div[6]/div[2]/div[1]/div[1]/div/div/div[1]/div[2]/div[1]");
    public static final By MailOTP_Select= xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[7]/div/div/div/div[2]/div/div[2]/div[3]/div/div/div[1]/div[1]/div[3]/div[3]/div[1]/div/div/div/table[1]/tbody/tr/td[2]/div/table/tbody/tr/td/p[2]");
    public static final By OBAuthSignOn_Button = xpath("//input[@value='Sign On']");
    public static final By OBLoginError_Text = xpath("/html/body/div/div[2]/div/form/div[1]/div");
    public static final By OBLoginOTP_Text = xpath("//input[@id='otp']");
    public static final By OBAuthError_Text = xpath("//div[text()='Invalid passcode']");
}
