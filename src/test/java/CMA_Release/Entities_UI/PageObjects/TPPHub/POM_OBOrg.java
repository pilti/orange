package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 27/03/2018.
 */
public class POM_OBOrg {

    public static final By OBOrgUsrName_Check= xpath("//*[@id='userTop']");
    //public static final By OBOrgUsrName_Check= xpath("/html/body/div[1]/div[1]/div/div/header/div/div/div[3]/div[1]/div[2]/span");
    public static final By OBOrgLogout_Check= xpath("//*[@class ='btn btn-danger btn-block btn-sm']");
    public static final By OBOrgSTBoth_Select = xpath("//*[text()='nBRcYAcACnghbGFOBk']");
    public static final By OBOrgSTAISP_Select = xpath("//*[text()='DcE9AoI4XnPZgC5JDx']");
    public static final By OBOrgSTPISP_Select = xpath("//*[text()='Q5wqFjpnTAeCtDc1Qx']");
    public static final By OBOrgSIT1Both_Select = xpath("//*[text()='YEIRsmLHVgczndxmJb']");
    public static final By OBOrgSIT2AISP_Select = xpath("//*[text()='KOrg5dS2shSCqWoQPM']");
    public static final By OBOrgSIT3PISP_Select = xpath("//*[text()='YuQRnNKCczeFgFvevg']");
    public static final By OBOrgAddapplication_Btn = xpath("//*[@class='btn btn-outline-primary float-right m-t--30 m-b-20']");
    public static final By UncheckASPSP_Checkbox= xpath("/html/body/div[1]/div[1]/div/div/div/div[2]/div[4]/label/input");

}
