package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 12/03/2018.
 */
public class POM_TPPApp {

    public static final By HeadApp_Button = xpath("/html/body/div[1]/div/header/div/nav/div/ul/li[2]/a/span[1]");
    public static final By Upload_SSAFile = xpath("//input[@id='uploadSoftwareStatement']");
    public static final By Browse_Button = xpath("//button[@id='btn_browse']");
    public static final By Upload_Button = xpath("//button[@id='btn_upload']");
    public static final By addApplication_Button = xpath("//button[@id='btn_addApp']");
    public static final By Cancel_Button = xpath("//button[text()='Cancel']");
    public static final By AddApp_Button = xpath("//button[@id='btn-add-application']");
    public static final By InvalidSSA = xpath("//*[text()='Invalid SSA. Please upload a valid SSA.]");
    public static final By CopyCSBtn = xpath("//*[@ng-bind='showSecretBtnText1']");
    public static final By AppSuccess_Msg = xpath("//*[@ng-class='isClosable']");
    public static final By CloseSuccess_Msg=xpath("//div/button[@id='btn-close']");
    public static final By ShwMoreCID_button=xpath("//*[text()='Application list']/parent::*/tbody/tr[2]/td[1]/button[1]");
    public static final By GetCID_details=xpath("/html/body/div[1]/div/section/div[2]/div/table/tbody[1]/tr[3]/td/div/table/tbody/tr[2]/td");
    public static final By ShwCS_Button=xpath("//*[text()='Application list']/parent::*/tbody/tr[1]/td[5]/button[1]");
    public static final By GetCS_details=xpath("//*[@id='showSecretPopup']/div/div[2]/p");
    public static final By AppNames_AllElements=xpath("//td[@ng-bind='applicationData.cn']");
    //public static final By InvalidSSA =xpath("//*[@id='noTppList']//span[2]");
    public static final By OKBtn=xpath("//button[text()='OK']");
    public static final By CloseAlert_Button = xpath("//*[@class='modal-header']/button[@title='Close']");
    public static final By headerMsg_Popup = xpath("//div[@class='modal-header']/span[2]");
    public static final By BodyMsg_Popup1 = xpath("//div[@class='modal-body']/p[1]");
    public static final By BodyMsg_Popup2 = xpath("//div[@class='modal-body']/p[2]");
    public static final By donotRemoveApp = xpath("//button[text()='No - do not remove']");
    public static final By orgLabel = xpath("//p[contains(text(),'Select the organisation to add')]");
    public static final By organizationTab = xpath("//a/span[text()='Organisations']");
    public static final By TppOrg_Help = xpath("//a/span[text()='Help']");
    public static final By TppOrg_FooterHelp = xpath("//*[@class='footer-links ng-scope']//a/span[text()='Help']");
    public static final By TppOrg_Cookie_Privacy = xpath("//a/span[text()='Cookie & Privacy policy']");
    public static final By applicationFieldTbl = xpath("//table[@id='addApplicationDetails']");
    public static final By showMoreApplicationFieldTbl = xpath("//*[@aria-expanded='true' and @aria-hidden='false']/table");
    public static final By showMoreBtn = xpath("//span[text()='Show more details']");
    public static final By showLessBtn = xpath("//span[text()='Show less details']");
    public static final By failureErrorMessage1 = xpath("//div[@ng-class='alertCssClass']/p/span[1]");
    public static final By failureErrorMessage2 = xpath("//div[@ng-class='alertCssClass']/p/span[2]");
    public static final By helpQuesMark = xpath("//*[text()='Role']/following-sibling::span");
    public static final By helpInfo = xpath("//*[text()='Role']/following-sibling::span/div");
    public static final By ClientSecretQuesMark = xpath("//*[text()='Client Secret']/following-sibling::span");
    public static final By ClientSecretInfo = xpath("//*[text()='Client Secret']/following-sibling::span/div");
    public static final By errorMessageApp = xpath("//form//p");
    public static final By applicationName = xpath("//*[text()='Show more details']/preceding-sibling::span");
    public static final By firstApp = xpath("//tbody/tr/td");
    public static final By termsOfUse = xpath("//*[text()='Terms of Use']");
    public static final By orgNameApplicationPage = xpath("//*[@class='header-myapp']/span[1]");

    public static final By applicationDetailsOneAtTime = xpath("//*[@aria-expanded='true' and @aria-hidden='false']/table//preceding::tbody//*[text()='Show more details']");










    //div[@aria-expanded='true']











}
