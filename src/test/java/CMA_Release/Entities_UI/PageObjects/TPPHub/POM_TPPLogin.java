package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class POM_TPPLogin {


    public static final By LoginHeader_Help = xpath("//*[@ng-bind='helpNav']");
    public static final By LoginOB_Link = xpath("//a[@id='loginButton']");
    public static final By OpenBanking_Link = xpath("/html/body/div[1]/div/section/div/div[1]/p[3]/a/span[1]");
    public static final By LoginFooterCookiesPrivacy_Link = xpath("//footer//a/span[text()='Cookie & Privacy policy']");
    public static final By LoginFooterHelp_Link = xpath("//footer[@class='ng-isolate-scope']//a/span[text()='Help']");
    public static final By OBLoginUsername_Button = xpath("//input[@id='username']");
    public static final By OBLoginPassword_Button = xpath("//input[@id='password']");
    public static final By OBLoginSignOn_Button = xpath("//*[@title='Sign On']");
    public static final By OBMyOrg_Page = xpath("//*[text()='My Organisations']");
    public static final By OBAuthSignOn_Button = xpath("//input[@value='Sign On']");
    public static final By OBLoginError_Text = xpath("//div[@class='ping-error']");
    public static final By OBLoginOTP_Text = xpath("//input[@id='otp']");
    public static final By OBAuthError_Text = xpath("//div[text()='Invalid passcode']");
    public static final By MailUsrname_Text= xpath("/html/body/form/div/div[2]/div/div[3]/input");
    public static final By MailPasswd_Text= xpath("/html/body/form/div/div[2]/div/div[5]/input");
    public static final By MailSubmit_Button= By.cssSelector("html body.signInBg form div#mainLogonDiv.mouse div.logonContainer div#lgnDiv.logonDiv div.signInEnter div.signinbutton span.signinTxt");

}
