package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 12/03/2018.
 */
public class POM_TPPOrg {

    public static final By TPPOrg_Username_Display =  xpath("//span[@class='text-capitalize ng-binding']");
    public static final By TppOrg_Logout_Link = xpath("//*[@ng-bind='logout']");
    public static final By TPPOrg_STAISPPISP_Select = xpath("//input[@id='nBRcYAcACnghbGFOBk']");
    public static final By TPPOrg_STAISP_Select     = xpath("//input[@id='DcE9AoI4XnPZgC5JDx']");
    public static final By TPPOrg_STPISP_Select     = xpath("//input[@id='Q5wqFjpnTAeCtDc1Qx']");
    public static final By TPPOrg_SIT1AISP_Select = xpath("//input[@id='YEIRsmLHVgczndxmJb']");
    public static final By TPPOrg_SIT2PISP_Select   = xpath("//input[@id='KOrg5dS2shSCqWoQPM']");
    public static final By TPPOrg_SIT3AISPPISP_Select    = xpath("//input[@id='YuQRnNKCczeFgFvevg']");
    public static final By Continue_Button = xpath("//button[@type='submit']");
    public static final By TppOrg_organizationTab = xpath("//a/span[text()='Organisations']");
    public static final By TppOrg_Applications = xpath("//a/span[text()='Applications']");
    public static final By TppOrg_Help = xpath("//a/span[text()='Help']");
    public static final By TppOrg_lst_App = xpath("//*[@class='panel-default ng-scope ng-isolate-scope panel']//label/span");
    public static final By TppOrg_lst_AppFields = xpath("//div[@aria-hidden='false']//li");




//    @FindBy(xpath=("//a/span[text()='Organisations']"))
//    public WebElement orgTab;




    }
