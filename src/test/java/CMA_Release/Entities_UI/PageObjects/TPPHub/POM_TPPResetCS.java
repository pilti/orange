package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 12/03/2018.
 */
public class POM_TPPResetCS {

    public static final By TopAlert_Msg = xpath("//span[@id='resetSecretPopupHeaderLabel']");
    public static final By ContinueAlert_Msg= xpath("//p[@class='text-center desc ng-binding']");
    public static final By ChcekBoxAlert_Msg= xpath("//*[@ng-bind='resetSecretCheckboxLabel']");
    public static final By Tickbox_Check = xpath("//input[@id='accpt_cond']");
    public static final By NoReset_Button = xpath("//button[contains(text(),'do not reset secret')]");
    public static final By CloseAlert_Button = xpath("//*[@class='modal-header']/button[@title='Close']");
    public static final By YesReset_Button = xpath("//button[@class='btn btn-submit ng-binding']");
    public static final By ResetSuccess_Msg = xpath("/*//*[@ng-class='isClosable']");


    
    }
