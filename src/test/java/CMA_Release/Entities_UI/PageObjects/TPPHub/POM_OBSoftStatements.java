package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 27/03/2018.
 */
public class POM_OBSoftStatements {

    public static final By AppSuccess_Msg = xpath("/html/body/div[1]/div[1]/div/div/div/div[4]/div");
    public static final By AppStatus = xpath("/html/body/div[1]/div[1]/div/div/div/div[5]/div[1]/span");
    public static final By ClientID = xpath("/html/body/div[1]/div[1]/div/div/div/div[5]/div[2]");
    public static final By Generate_Btn=xpath("//span[@class='btn btn-dark btn-block pointer']");
    public static final By OrgId=xpath("/html/body/div[1]/div[1]/div/div/div/div[7]/div[1]");
    public static final By CopySSATxt=xpath("//textarea[@class='form-control ssaTextArea visible']");
    public static final By DwnLoad_Network_pem=xpath("//*[text()='transport']/following::*[text()=' Download PEM File'][1]");
    public static final By DwnLoad_Signing_pem=xpath("//*[text()='signing']/following::*[text()=' Download PEM File']");
    public static final By Transport_TxtBox=xpath("//*[text()='Transport']/following-sibling::div/*[@class='dropzone']/span[1]");
    public static final By Signing_TxtBox=xpath("//*[text()='Signing']/following-sibling::div/*[@class='dropzone']/span[1]");
    public static final By Transport_UploadBtn=xpath("//*[text()='Transport']/following-sibling::div/button");
    public static final By Signing_UploadBtn=xpath("//*[text()='Signing']/following-sibling::div/button");
    public static final By PayloadJson=xpath("//*[text()='payload']");

}
