package CMA_Release.Entities_UI.PageObjects.TPPHub;

import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

/**
 * Created by C963829 on 27/03/2018.
 */
public class POM_OBAppDetails {

    public static final By DisOrgId= xpath("/html/body/div[1]/div[1]/div/div/div/div[2]/table/tbody/tr/td[2]");
    public static final By Client_Name = xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[1]/div[1]/div[1]/div/div/input");
    public static final By Descrption_Name= xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[1]/div[2]/div[1]/div/div/textarea");
    public static final By PolicyURL = xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[2]/div[1]/div[1]/div/div/input");
    public static final By TermsofUseURL =xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[2]/div[2]/div[1]/div/div/input");
    public static final By RedirectURL= xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[2]/div[3]/div[1]/div/div/div/input");
    public static final By Version=xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[1]/div[3]/div[1]/div/div/input");
    public static final By PISPRole=xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[1]/div[4]/div[1]/div/div[1]/div[1]/label");
    public static final By AISPRole=xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[1]/div[1]/div[4]/div[1]/div/div/div[2]/label");
    public static final By AppSubmit=xpath("/html/body/div[1]/div[1]/div/div/div/div[3]/div[2]/div[2]/button");
}
