package CMA_Release.Entities_UI.Selenium;

import Apps.webapp;
import CMA_Release.Entities_UI.Browsers.BrowserFactory;
import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.General.capturescreen;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static CMA_Release.Entities_UI.General.capturescreen.TakeScreenShot;
import static CMA_Release.Entities_UI.General.capturescreen.captureScreenShot2;


/**
 * Created by C961818 on 28/03/2018.
 */
//This is a reuseable class consisting of methods used for UI automation

public class ReusableFunctions {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ReusableFunctions.class);

    //Find Webelement method of Selenium

    public static WebElement findUIElement(GetApplication app, By obj) {

        WebElement webObj = null;
        try {
            webObj = app.driver1.findElement(obj);
            return webObj;
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error finding element {} with error {}", obj, e.getMessage());
            }
            return null;
        }

    }

    //Find Web elements(List) method of Selenium
    public static List<WebElement> findUIElementsList(GetApplication app, By obj) {
       /* JavascriptExecutor js = (JavascriptExecutor)driver;
       WebElement selectedCompGlass = (WebElement) js.executeScript("return document.evaluate('//*[text()='Authorisation renewal']' ,document, null, XPathResult.ANY_TYPE, null ).singleNodeValue;");*/

        List<WebElement> webObj = null;
        try {
            webObj = app.driver1.findElements(obj);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error finding elements {} with error {}", obj, e.getMessage());
            }
        }
        return webObj;

    }

    //Click Webelement method of selenium
    public static void ClickElement(GetApplication app, By obj) {
        try {
            WebElement we = findUIElement(app, obj);
            we.click();
            Thread.sleep(2000);
            captureScreenShot2(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error clicking the element {} with error {}", obj, e.getMessage());
            }
        }

    }


    //setClipboardData is a method that can be used for copy and paste operations.
    public static void setClipboardData(String Strdata) {

        StringSelection stringSelection = new StringSelection(Strdata);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
    }

    //This method is to fetch current url of webpage
    public static String getCurrentURL(GetApplication app) {
        try {
            String cUrl = app.driver1.getCurrentUrl();
            return cUrl;
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error launching the URL with error {}", e.getMessage());
            }
            return null;
        }
    }

    //This method uses robot class to upload a file
    public static void UploadFileRobot(String path1) {

        Robot robot = null;
        try {
            robot = new Robot();
            File f = new File(path1);
            String path = f.getCanonicalPath().toString();
            StringSelection stringSelectionpATH = new StringSelection(path);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelectionpATH, null);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (Exception exp) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in uploadFileRobot {}", exp.getMessage());
            }
            BrowserFactory.closeAllDriver();
        }
    }

    //This method is to open a new tab
    public static void NewTabRobot() {

        Robot robot = null;
        try {
            robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_T);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (Exception exp) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in openeing new tab {}", exp.getMessage());
            }
        }
    }

    //This method is to open new Window
    public static void NewWindowRobot() {

        Robot robot = null;
        try {
            robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_N);
            robot.keyRelease(KeyEvent.VK_N);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (Exception exp) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in opening new window {}", exp.getMessage());
            }
        }
    }

    //This method is used for mouse click
    public static void mouseclick(GetApplication app) {
        try {
            Actions actions = new Actions(app.driver1);
            Robot robot = new Robot();
            robot.mouseMove(50, 50);
            actions.click().build().perform();
            /*robot.mouseMove(200, 70);
            actions.click().build().perform();*/
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in mouseclick {}", e.getMessage());
            }

        }
    }

    //This selenium method is used to get text of a webelement
    public static String getElementText(GetApplication app, By obj) {
        WaitForElement(app.driver1, obj);
        String str = null;
        try {
            str = app.driver1.findElement(obj).getText().trim();
            TakeScreenShot(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in getText method {}", e.getMessage());
            }
        }
        return str;
    }

    //This selenium method is used to check if webelement exists
    public static boolean CheckElementPresent(GetApplication app, By obj) {
        try {
            WaitForElement(app.driver1, obj);
            if (app.driver1.findElements(obj).size() > 0) {
                return true;
            } else
                return false;
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in checking presence of element {}", e.getMessage());
            }
            return false;
        }
    }

    //This selenium method is used for wait
    public static void WaitForElement(WebDriver driver, By obj) {
        try {
            // set the wait parameters
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(60, TimeUnit.SECONDS) // set the timeout
                    .pollingEvery(2, TimeUnit.SECONDS) // set the interval
                    .ignoring(NoSuchElementException.class);
            //declare the webElement and use a function to find it
            WebElement waitingElement = wait.until(ExpectedConditions.presenceOfElementLocated(obj));
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in wait for element", e.getMessage());
            }
        }
    }

    //This method is to use keyboard functions
    public static void PressKeyboardFunctions(WebDriver driver, String str) {
        Robot robot = null;
        try {
            robot = new Robot();
            switch (str) {
                case "esc":
                    robot.keyPress(KeyEvent.VK_ESCAPE);
                    break;
                default:
                    LOGGER.info("wrong string passed to PressKeyboardFunctions");
            }
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in keyboard Actions Function  {}", e.getMessage());
            }

        }
    }

    //This method performs sendkeys to webelement
    public static void sendKey(GetApplication app, By obj, Map data) {
        try {
            WaitForElement(app.driver1, obj);
            String str = data.get("text").toString();
            WebElement we = findUIElement(app, obj);
            we.sendKeys(str);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in SendKeys {}", e.getMessage());
            }
            app.stop();
        }
    }

    //This is to be called to display authcode in a dummy page
    public static void AuthCodeDisplay(GetApplication app, String authCode) {
        try {
            File f1 = new File("./src/test/java/CMA_Release/JavaScript_Lib/byte-counter");
            String str = f1.getCanonicalPath();
            String str2 = str.replaceAll("\\\\src.*", "").replaceAll(".*\\\\", "");
            String uiUrl = "http://localhost:63342/" + str2 + "/karate-junit4/CMA_Release/JavaScript_Lib/byte-counter/index.html";
            LOGGER.info("Redirect URL after submitting consent", uiUrl);
            app.driver1.get(uiUrl);
            //WaitForElement(app.driver1, (By.xpath("/html/body/p[1]/textarea")));
            app.driver1.findElement(By.xpath("/html/body/p[1]/textarea")).clear();
            app.driver1.findElement(By.xpath("/html/body/p[1]/textarea")).sendKeys("Authcode generated for the consent is : " + authCode);
            captureScreenShot2(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in AuthCodeDisplay {}", e.getMessage());
            }
            app.stop();
        }
    }

    //This method is useds to get Page title
    public static String getPageTitle(GetApplication app) {

        String str = null;
        try {
            str = app.driver1.getTitle();
            captureScreenShot2(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in getPageTitle {}", e.getMessage());
            }
            app.stop();
        }
        return str;
    }

    //This method is to change focus of driver
    public static void SwitchfocustoLatestWindow(WebDriver driver) {
        for (String handle1 : driver.getWindowHandles()) {
            LOGGER.info("Window: ", handle1);
            driver.switchTo().window(handle1);
        }
    }

    //This method is to handle multiple window instances
    public static void HandleWindow(GetApplication app) {
        try {
            List<String> win = new ArrayList<>(app.driver1.getWindowHandles());
            LOGGER.info("Window: ", win);
            app.driver1.switchTo().window(win.get(1).toString());
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in HandleWindow {}", e.getMessage());
            }
            app.stop();
        }
    }

    //This method is to perform browser back
    public static void BrowserBack(GetApplication app) {
        app.driver1.navigate().back();
    }

    //This method is to perform Browser forward
    public static void BrowserForward(GetApplication app) {
        app.driver1.navigate().forward();
    }

    //This method is to focus on a webelement
    public static void focusOnElement(GetApplication app, By obj) {
        WaitForElement(app.driver1, obj);
        WebElement we = findUIElement(app, obj);
        new Actions(app.driver1).moveToElement(we).perform();
    }

    //This method is to check if webelement is enabled
    public static boolean CheckElementEnabled(GetApplication app, By obj) throws IOException {
        WaitForElement(app.driver1, obj);

        if (findUIElement(app, obj).isEnabled())
            return true;
        else
            return false;
    }

    //This method is to get size of List
    public static int sizeOfList(GetApplication app, By obj) {
        List<WebElement> webObj = null;
        webObj = app.driver1.findElements(obj);
        return (webObj.size());
    }

    //This method is to Mouse Hover on Element
    public static void MouseHoveronElement(webapp app, By obj) {
        try {
            Actions action = new Actions(app.driver1);
            WebElement we = app.driver1.findElement(obj);
            action.moveToElement(we).build().perform();
            focusOnElement(app, obj);
            TakeScreenShot(app);
        } catch (Exception e) {
            e.getMessage();
            app.stop();
        }
    }

    //This method is to get value by Attribute
    public static String getvaluebyAttribute(GetApplication app, By obj, String s) {
        String str = null;
        try {
            str = app.driver1.findElement(obj).getAttribute(s);
            captureScreenShot2(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in GetValueByAttribute {}", e.getMessage());
            }
        }
        return str;
    }

    //This method is to scroll Till Element
    public static void scrollTillElement(GetApplication app, By obj) {
        JavascriptExecutor js = (JavascriptExecutor) app.driver1;
        js.executeScript("arguments[0].scrollIntoView();", app.driver1.findElement(obj));
    }

    //This method is to apply static sleep
    public static void sleep(long millisecs) {
        try {
            Thread.sleep(millisecs);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in static wait {}", e.getMessage());
            }
        }
    }

    //This method is to perform browser Navigations
    public static HashMap browserNavigations(webapp app, String navigation) {
        try {
            HashMap<String, String> map = new HashMap<>();


            app.moduleName1 = navigation;
            switch (navigation.toLowerCase().trim()) {
                case "forward":
                    app.driver1.navigate().forward();
                    map.put("Status", "Pass");
                    map.put("Message", "Clicked on forward button");
                    break;

                case "backward":
                    app.driver1.navigate().back();
                    map.put("Status", "Pass");
                    map.put("Message", "Clicked on backward button");
                    break;

                case "refresh":
                    app.driver1.navigate().refresh();
                    map.put("Status", "Pass");
                    map.put("Message", "Clicked on refresh button");
                    break;

                default:
                    map.put("Status", "Fail");
                    map.put("Message", "Wromng input data, Please correct the test data");
                    break;

            }
            sleep(3000);
            capturescreen.TakeScreenShot(app);
            return map;
        } catch (Exception e) {
            e.getMessage();
            app.stop();
            return null;
        }
    }

    //This method performs Window Handler
    public static ArrayList getAllWindowHandles(webapp app) {
        ArrayList<String> windows = new ArrayList<String>(app.driver1.getWindowHandles());
        return windows;
    }

    //This method is for switching between windows
    public static HashMap switchToWindow(webapp app, ArrayList<String> windows, int index) {
        HashMap<String, String> map = new HashMap<String, String>();
        app.driver1.switchTo().window(windows.get(index));
        map.put("Status", "Pass");
        map.put("Message", "Driver focus is switched to window -" + app.driver1.getTitle());
        return map;
    }

    // method for dynamic wait
    public static void waitUntillElementExist(webapp app, By eleXpath, int waitTime) {
        boolean bElementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(app.driver1, waitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(eleXpath));
            LOGGER.info("Element found");
            bElementFound = true;
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in waitUntillElementExist {}", e.getMessage());
            }
        }
    }

    //This method is to perform clicking
    public static HashMap ClickElement(GetApplication app, By obj, String strName) {
        HashMap<String, String> map = new HashMap<>();

        WebElement we = findUIElement(app, obj);
        we.click();
        sleep(2000);
        TakeScreenShot(app);
        map.put("Status", "Pass");
        map.put("Message", "Element clicked successfully - " + strName);
        return map;
    }

    //This method is to perform Mouse Hover
    public static HashMap MouseHoveronElement(webapp app, By obj, String strObjName) {
        HashMap<String, String> map = new HashMap<>();
        Actions action;

        action = new Actions(app.driver1);
        WebElement we = app.driver1.findElement(obj);
        action.moveToElement(we).build().perform();

        map.put("Message", "Mouse hover is performed towards element - " + strObjName);

        return map;
    }

    //This method is to clear text in a werbelement
    public static void cleartext(webapp app, By obj) {
        try {
            WaitForElement(app.driver1, obj);
            focusOnElement(app, obj);
            app.driver1.findElement(obj).clear();
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in waitUntillElementExist {}", e.getMessage());
            }
        }
    }

    public static List<String> Table_data(webapp app, By table_obj) {
        List<WebElement> row = null;
        List<String> data = new ArrayList<>();
        try {
            WebElement table = app.driver1.findElement(table_obj);
            row = table.findElements(By.tagName("tr"));
            for (int i = 0; i < row.size(); i++) {
                String val = row.get(i).findElement(By.tagName("td")).getText();
                data.add(i, val);
            }
            return data;
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return null;
        }

    }

    public static boolean Ischecked(webapp app, By obj) {
        try {
            WebElement chck_box = app.driver1.findElement(obj);
            String attr = chck_box.getAttribute("class");
            if (attr.contains("ng-empty")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }
        //This method is to clear text in a werbelement

    public static String selectElementByIndexFromList(webapp app, By obj, int n) {
        String strResult = null;

        try {
            Select slct = new Select(app.driver1.findElement(obj));
            slct.selectByIndex(n);
            strResult = "Pass";
            sleep(2000);
            TakeScreenShot(app);
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error in waitUntillElementExist {}", e.getMessage());
            }

        }
        return strResult;

    }

}
