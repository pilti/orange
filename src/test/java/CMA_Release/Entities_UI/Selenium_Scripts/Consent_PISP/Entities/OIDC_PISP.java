package CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities;

import Apps.webapp;
import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Login;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Review_confirm;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static CMA_Release.Entities_UI.General.capturescreen.captureScreenShot2;
import static CMA_Release.Entities_UI.Selenium.ReusableFunctions.WaitForElement;
import static CMA_Release.Entities_UI.Selenium.ReusableFunctions.focusOnElement;


/**
 * Created by C965187 on 09/04/2018.
 */
public class OIDC_PISP {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OIDC_PISP.class);
    //This method is to launch url
    public static Map launchConsentURL(GetApplication app, String UniResourceloc) {

        app.moduleName1 = "launchConsentURL";
        int var = 0;
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            LOGGER.info("URL sent to driver");
            app.driver1.get(UniResourceloc);
            LOGGER.info("Browser Opened URL");
            String str = app.driver1.getTitle();
            LOGGER.info("Page Title : " + str);
            captureScreenShot2(app);

            var = str.compareTo("Login - Bank of Ireland");
            if (var == 0) {
                map.put("Landing_Page", str);
                map.put("Status", "Pass");
            } else {
                map.put("reasonText", "Landing Page is not as expected!");
                map.put("Status", "Fail");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Status", "Technical Error");
            app.stop();
        }
        return map;
    }
    //This method performs Consent Login by launching the consent URL and clicking on "I have Key code app" button
    public static HashMap loginConsent(webapp app, HashMap data) {

        HashMap<String, String> map = new HashMap<>();

        try {

            WaitForElement(app.driver1, Login_obj.have_key_app_bttn);
            focusOnElement(app, Login_obj.have_key_app_bttn);
            app.driver1.findElement(Login_obj.have_key_app_bttn).click();
            WaitForElement(app.driver1, Login.username);
            focusOnElement(app, Login.username);

            app.driver1.findElement(Login.username).sendKeys(data.get("usr").toString());
            app.driver1.findElement(Login.pwd).sendKeys(data.get("otp").toString());
            captureScreenShot2(app);
            focusOnElement(app, Login.continue_button);
            app.driver1.findElement(Login.continue_button).click();

            waitUntillElementExist(app, By.xpath("//caption[text()='Payee details']/parent::table"), 60);

            String PageTitle = app.driver1.getTitle();

            int var = PageTitle.compareTo("Consent - Bank of Ireland");
            if (var == 0) {
                map.put("Landing Page", "Application login successful " + PageTitle);
                map.put("Status", "Pass");
            } else {
                map.put("reasonText", "Application login failed. Landing Page is not as expected!");
                map.put("Status", "Fail");
                map.put("Landing Page", PageTitle);
            }

        } catch (Exception e) {
            map.put("Status", "Exception occurred -" + e.getMessage());

        }

        return map;
    }

    //This method is to select first account from drop down
    public static HashMap<String, String> selectFromAccount(webapp app, HashMap data) {

        HashMap<String, String> map = new HashMap<>();
        List<WebElement> lstEle = null;

        try {
            String debtorFlag = data.get("debtorFlag").toString();
            if(debtorFlag.equals("No")) {
                if (checkIfElementPresent(app, Select_Debit_Account.select_acc)) {

                    app.driver1.findElement(Select_Debit_Account.select_acc).click();
                    lstEle = app.driver1.findElements(Select_Debit_Account.drop_down_list);
                    Thread.sleep(2000);

                    if (lstEle.size() > 0) {
                        lstEle.get(0).click();
                    }

                }
                captureScreenShot2(app);
                LOGGER.info("Flag is set as: " + debtorFlag);
                map.put("SelectAccount", lstEle.get(0).getText());
                map.put("Status", "Pass");
                map.put("debtorFlag", debtorFlag);
            }
            else {
                LOGGER.info("Flag is set as: " + debtorFlag);
                map.put("Status", "Pass");
                map.put("debtorFlag", debtorFlag);
            }

        } catch (Exception e) {

            map.put("Exception", "Exception occurred -" + e.getMessage());
            map.put("Status", "Fail");

        }
        return map;
    }

    //Performing action on Review Confirm page
    public static Map<String, String> performConsentAction(webapp app, HashMap data) {

        Map<String, String> map = new HashMap<>();
        Map<String, String> strCode = new HashMap<>();
        String strAction = null, PageTitle = null;
        try {

            strAction = data.get("action").toString().toLowerCase();

            if (strAction.equals("continue")) {
                map = getAuthCode(app);
                map.put("Status", "Pass");
//               map.put("AuthCode", strCode);


            } else if (strAction.equals("cancel")) {
                //code to click on back and verify the the page

                app.driver1.findElement(Account_selection.cancel_button).click();
                if (data.get("Confirmation").toString().toLowerCase().equals("yes")) {
                    Thread.sleep(2000);
                    app.driver1.findElement(Account_selection.cancel_request).click();
                    Thread.sleep(2000);
                    captureScreenShot2(app);
                    String strCallBackUrl = "https://app.getpostman.com/oauth2/callback#state=af0ifjsldkj&error=access_denied";
                    if (app.driver1.getCurrentUrl().equals(strCallBackUrl)) {
                        map.put("Status", "Pass");
                        map.put("CallbackUrl", strCallBackUrl);
                    } else {
                        map.put("Status", "Fail");
                        map.put("CallbackUrl", strCallBackUrl);
                    }

                } else {
                    Thread.sleep(2000);
                    app.driver1.findElement(Account_selection.NO).click();
                    // continue with the positive flow to get the auth code
                    map = getAuthCode(app);
                    captureScreenShot2(app);
                    map = getAuthCode(app);
                    map.put("Status", "Pass");

                }
            }

        } catch (Exception e) {
            map.put("Status", "Fail" + e.getMessage());

        }
        return map;


    }

    //This method is to click on continue button on Review confirm page and get the auth code from call back url
    private static Map<String, String> getAuthCode(webapp app) {
        String authCodeUrl, strCode, id_token;
        Map<String, String> map = new HashMap<>();
        waitUntillElementExist(app, Select_Debit_Account.continue_button, 10);
        app.driver1.findElement(Select_Debit_Account.continue_button).click();
        LOGGER.info("Continue clicked");
        captureScreenShot2(app);
        waitUntillElementExist(app, Review_confirm.confirm_chk, 10);
        //code to scroll to the element
        JavascriptExecutor js = (JavascriptExecutor) app.driver1;
        js.executeScript("arguments[0].scrollIntoView();", app.driver1.findElement(Review_confirm.confirm_chk));
        app.driver1.findElement(Review_confirm.confirm_chk).click();
        LOGGER.info("Checkbox clicked");
        captureScreenShot2(app);
        app.driver1.findElement(Review_confirm.confirm_payment).click();
        LOGGER.info("Confirm button clicked");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        authCodeUrl = app.driver1.getCurrentUrl();
        strCode = authCodeUrl.split("code=")[1].split("&")[0];
        id_token = authCodeUrl.split("id_token=")[1].split("&")[0];
        map.put("AuthCodeURL", authCodeUrl);
        map.put("AuthCode", strCode);
        map.put("id_token", id_token);


        return map;
    }

    //This method verifies the Payment details displayed on Account Selection Page
    public static HashMap verifyPaymentDetails(webapp app) {

        //code to get the expected values from JSON input for payment

        waitUntillElementExist(app, By.xpath("//caption[text()='Payee details']/parent::table"), 10);
        HashMap<String, String> paymentDetails = new HashMap<>();
        // code to get the values from the Consent page
        WebElement tbl_payment = app.driver1.findElement(By.xpath("//caption[text()='Payee details']/parent::table"));
        List<WebElement> rows = tbl_payment.findElements(By.tagName("tr"));

        int row_cnt = rows.size();
        String fieldName, fieldVal;
        for (int i = 1; i < row_cnt - 1; i++) {
            fieldName = rows.get(i).findElement(By.tagName("th")).getText();
            fieldVal = rows.get(i).findElement(By.tagName("td")).getText();
            if (fieldName.equals("Amount")) {
                fieldVal = fieldVal.substring(1);
            }
            paymentDetails.put(fieldName, fieldVal);

        }


        return paymentDetails;
    }


    //method to check if Element is present
    public static boolean checkIfElementPresent(webapp app, By eleXpath) {
        boolean eleFound = false;
        WebElement ele = null;
        try {
            ele = app.driver1.findElement(eleXpath);
            eleFound = true;

        } catch (Exception e) {
            LOGGER.info("Element not found : ");
        }
        return eleFound;
    }

    // method for dynamic wait
    public static void waitUntillElementExist(webapp app, By eleXpath, int waitTime) {
        boolean bElementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(app.driver1, waitTime);
            wait.until(ExpectedConditions.visibilityOfElementLocated(eleXpath));
            bElementFound = true;
        } catch (Exception e) {
            LOGGER.info("Element is not found -" + e.getMessage());
        }
    }

}
