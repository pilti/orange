package CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities;


import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPLogin;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static CMA_Release.Entities_UI.General.capturescreen.captureScreenShot2;
import static org.openqa.selenium.By.*;

public class TPPLogin {
    //To Launch TPP Launch URL

    public static Map<String, Object> Launch_TPP_Hub(GetApplication app, String URL) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "Launch_TPP_Portal";
        map.put("Test_Name", app.t_name1);
        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.manage().window().maximize();
            app.driver1.get(URL);
            Thread.sleep(1000);
            captureScreenShot2(app);
            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("Third Party Hub")) {
                map.put("Status", "Pass");
                map.put("Message", "URL Launch Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", ActPageTitle);
                System.out.println("Actual Title of page: " + ActPageTitle);
            }


        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    //Test to Navigate to OB Portal Login
    public static Map<String, Object> PTC_Navigate_TPP_Through_OBLink(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Navigate_TPP_Through_OBLink";
        map.put("Test_Name", app.t_name1);

        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            captureScreenShot2(app);
            Thread.sleep(3000);
            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("Sign On")) {

                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual Title of page: " + ActPageTitle);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }


    //Test to check OB link from Contenet
    public static Map<String, Object> Content_OB_Link_Check(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Navigate_TPP_Through_OBLink";
        map.put("Test_Name", app.t_name1);
        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.OpenBanking_Link).click();
            captureScreenShot2(app);
            Thread.sleep(5000);
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String ActPageTitle = app.driver1.getTitle();
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            if (ActPageTitle.equals("Home – Open Banking")) {

                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual Title of page: " + ActPageTitle);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    //Test to check OB link from Contenet
    public static Map<String, Object> Header_Help_Link_Check(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Click_Header_Help_Link";
        map.put("Test_Name", app.t_name1);
        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginHeader_Help).click();
            captureScreenShot2(app);
            Thread.sleep(5000);
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String ActPageURL = app.driver1.getCurrentUrl();
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            if (ActPageURL.equals("https://www.bankofireland.com/api/thirdparty/help/")) {

                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual URL of page: " + ActPageURL);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    //Test to check Cookies and Privacy links from Footer
    public static Map<String, Object> Footer_CookieandPrivacy_Link_Check(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Click_Footer_Help_Link";
        map.put("Test_Name", app.t_name1);
        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginFooterCookiesPrivacy_Link).click();
            captureScreenShot2(app);
            Thread.sleep(5000);
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String ActPageTitle = app.driver1.getTitle();
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            if (ActPageTitle.equals("Privacy Statement - Bank of Ireland Group Website")) {
                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual Title of page: " + ActPageTitle);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }

    //Test to check OB link from Contenet
    public static Map<String, Object> Footer_Help_Link_Check(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Click_Footer_Help_Link";
        map.put("Test_Name", app.t_name1);
        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginFooterHelp_Link).click();
            captureScreenShot2(app);
            Thread.sleep(5000);
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String ActPageURL = app.driver1.getCurrentUrl();
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            if (ActPageURL.equals("https://www.bankofireland.com/api/thirdparty/help/")) {

                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual URL of page: " + ActPageURL);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    public static Map<String, Object> PTC_Invalid_FirstFactor_Login_Check_with_OB(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Login_with_OB";
        map.put("Test_Name", app.t_name1);

        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
           //app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            //captureScreenShot2(app);
            Thread.sleep(1000);
            app.driver1.findElement(POM_TPPLogin.OBLoginUsername_Button).sendKeys(data.get("Email"));
            app.driver1.findElement(POM_TPPLogin.OBLoginPassword_Button).sendKeys(data.get("Password"));
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBLoginSignOn_Button).click();
            Thread.sleep(5000);
            WebElement ActMsg = app.driver1.findElement(POM_TPPLogin.OBLoginError_Text);
            captureScreenShot2(app);
            String ActMsgText = ActMsg.getText();
           // System.out.println("\\n Error Message:" +ActMsgText);
            if (ActMsgText.equals("We didn't recognize the username or password you entered. Please try again.")) {
                map.put("Status", "Pass");
                map.put("Message", "Login Details are Incorrect");
                map.put("Actual UserName is: ", ActMsgText);

            } else {
                map.put("Status", "Fail");
                map.put("Message", "Login UnSuccessful");
                map.put("Actual UserName is: ", ActMsgText);
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }

    public static Map<String, Object> PTC_Valid_FirstFactor_Login_Check_with_OB(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Login_with_OB";
        map.put("Test_Name", app.t_name1);

        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            //captureScreenShot2(app);
            Thread.sleep(1000);
            app.driver1.findElement(POM_TPPLogin.OBLoginUsername_Button).sendKeys(data.get("Email"));
            app.driver1.findElement(POM_TPPLogin.OBLoginPassword_Button).sendKeys(data.get("Password"));
            //captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBLoginSignOn_Button).click();
            Thread.sleep(5000);
            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("PingID")) {
                map.put("Status", "Pass");
                map.put("Message", "Login Details are Correct");

            } else {
                map.put("Status", "Fail");
                map.put("Message", "Login Details are InCorrect");
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }

    public static Map<String, Object> PTC_Invalid_OTP_SecondFactAuth_Login_OB_Check(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Login_with_OB";
        map.put("Test_Name", app.t_name1);

        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.OBLoginOTP_Text).sendKeys(data.get("InvalidOTP"));
            Thread.sleep(2000);
            app.driver1.findElement(POM_TPPLogin.OBAuthSignOn_Button).click();
            //captureScreenShot2(app);
            Thread.sleep(3000);
            WebElement ActMsg = app.driver1.findElement(POM_TPPLogin.OBAuthError_Text);
            //captureScreenShot2(app);
            String ActMsgText = ActMsg.getText();
            if (ActMsgText.equals("Invalid passcode")) {
                map.put("Status", "Pass");
                map.put("Message", "OTP Entered is Incorrect");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Someting Wrong with OTP");
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }


}


