package CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities;

import Apps.webapp;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp;
import CMA_Release.Entities_UI.Selenium.ReusableFunctions;

import java.util.HashMap;

/**
 * Created by C965187 on 30/04/2018.
 */
public class applications {

//    public static Map getClientId(webapp app) {
//
//        Map<String, Object> map = new HashMap<String, Object>();
//        try {
//            WebElement CopyCSPopup = app.driver1.findElement(By.xpath("//*[@id='showSecretPopup']/div/div[3]/button[1]"));
//            String CopyCSMsgText = CopyCSPopup.getText();
//            System.out.println("\n" + CopyCSMsgText);
//            if (CopyCSMsgText.contains("Copy client secret")) {
//                WebElement CS = app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[2]/p"));
//                String CSPopup = CS.getText();
//                String ClientSecretId = CSPopup.split(" ")[6];
//                System.out.println("\n" + "Client Secret ID:" + ClientSecretId);
//                //click on ok button
////                app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[3]/button[2]")).click();
//                clickOK(app);
//                //System.out.println("Clicked OK");
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                map.put("Status", "Pass");
//                map.put("Message", "Client Secrete Copied");
//                map.put("Application_ClientID", ClientSecretId);
//            } else {
//                map.put("Status", "Fail");
//                map.put("Message", "Client Secrete does Not Copied");
//            }
//        } catch (Exception e) {
//            map.put("Status", "Technical_Issue");
//            map.put("Error", "'" + e + "'");
//            System.out.println(e);
//        }
//        return map;
//    }

    public static HashMap performActionShowCLientId(webapp app, String strAction) {

        HashMap<String,String> map = new HashMap<>();


        switch (strAction.toLowerCase().trim()) {
            case "ok":
                clickOK(app);
                map.put("Status","Pass");
                map.put("Action","ok clicked");
                break;

            case "showclient":
                clickShowClientSecret(app);
                map.put("Status","Pass");
                map.put("Action","showclient clicked");
                break;

            case "close":
                clickCloseDialogBox(app);
                map.put("Status","Pass");
                map.put("Action","close clicked");
                break;

            default:
                map.put("Status","Fail");
//                map.put("Verify user navigate back to application page","input is not correct please correct the test data");
        }


        return map;
    }

    public static HashMap clickOK(webapp app) {
        HashMap<String,String> map = new HashMap<>();
        ReusableFunctions.ClickElement(app, POM_TPPApp.OKBtn);
        map.put("Status","Pass");
        map.put("Action","ok clicked");
        //System.out.println("Clicked OK");
        return map;
    }

    public static void clickShowClientSecret(webapp app) {
        HashMap<String,String> map = new HashMap<>();
        ReusableFunctions.ClickElement(app, POM_TPPApp.GetCS_details);
        map.put("Status","Pass");
        map.put("Action","ShowClientSecret clicked");
        //System.out.println("Clicked show client secret");
    }

    public static void clickCloseDialogBox(webapp app) {
        HashMap<String,String> map = new HashMap<>();
        ReusableFunctions.ClickElement(app, POM_TPPApp.CloseAlert_Button);
        map.put("Status","Pass");
        map.put("Action","Close clicked");
        //System.out.println("Clicked close");
    }


}
