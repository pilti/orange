package CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities;


import Apps.webapp;
import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.General.capturescreen;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPLogin;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPOrg;
import CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS;
import CMA_Release.Entities_UI.Selenium.ReusableFunctions;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static CMA_Release.Entities_UI.General.capturescreen.*;
import static org.openqa.selenium.By.xpath;

public class TPPHubResusables {
    //To Launch TPP Launch URL

    public static Map<String, Object> Launch_TPP_Hub(webapp app, String URL) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "Launch_TPP_Portal";
        map.put("Test_Name", app.t_name1);
        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.manage().window().maximize();
            app.driver1.get(URL);
//            Thread.sleep(1000);
            ReusableFunctions.waitUntillElementExist(app, POM_TPPLogin.OBLoginUsername_Button, 10);
            capturescreen.TakeScreenShot(app);
            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("Third Party Hub")) {
                map.put("Status", "Pass");
                map.put("Message", "URL Launch Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "URL not Launch Successfully -" + ActPageTitle);
            }


        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    public static Map<String, Object> PTC_Navigate_TPP_Through_OBLink(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Navigate_TPP_Through_OBLink";
        map.put("Test_Name", app.t_name1);

        try {
            //For Loging to TPP Portal
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            capturescreen.TakeScreenShot(app);
            Thread.sleep(15000);
            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("Third Party Hub")) {

                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Actual Title of page: " + ActPageTitle);
            }
            map.put("Test_Name", app.t_name1);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", e);
        }

        return map;
    }

    /*
    //Click on Loging with Open Banking
    public static Map<String, Object> PTC_Successful_Login_with_OB(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        String strOTP;
        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Login_with_OB";
        map.put("Test_Name", app.t_name1);

        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            //captureScreenShot2(app);
            Thread.sleep(1000);
            app.driver1.findElement(POM_TPPLogin.OBLoginUsername_Button).sendKeys(data.get("Email"));
            app.driver1.findElement(POM_TPPLogin.OBLoginPassword_Button).sendKeys(data.get("Password"));
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBLoginSignOn_Button).click();
            //Open new tab in chrome
            ((JavascriptExecutor) app.driver1).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(app.driver1.getWindowHandles());
            app.driver1.switchTo().window(tabs.get(1));
            System.out.println("PingID Window Opened");
            //get the otp
            strOTP = OBDirectory.LaunchMailGETOTP(app.driver1, data.get("EUsrName"), data.get("EPasswd"));
            app.driver1.close();
            app.driver1.switchTo().window(tabs.get(0));
            app.driver1.findElement(POM_TPPLogin.OBLoginOTP_Text).sendKeys(strOTP);
            System.out.println("PingID OTP Entered");
            Thread.sleep(1000);
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBAuthSignOn_Button).click();
            Thread.sleep(8000);
            WebElement ActMsg = app.driver1.findElement(POM_TPPOrg.TPPOrg_Username_Display);
            String ActMsgText = ActMsg.getText();

            if (ActMsgText.contains(data.get("Name"))) {
                map.put("Status", "Pass");
                map.put("Message", "Login Successful");
                map.put("OTP: ", strOTP);
                map.put("Actual UserName is: ", ActMsgText);

            } else {
                map.put("Status", "Fail");
                map.put("Message", "Login UnSuccessful");
                map.put("OTP: ", strOTP);
                map.put("Actual UserName is: ", ActMsgText);
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    } */

    public static Map<String, Object> PTC_Login_with_OB(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        String strOTP;
        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Login_with_OB";
        map.put("Test_Name", app.t_name1);

        try {
            app.driver1.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            app.driver1.findElement(POM_TPPLogin.LoginOB_Link).click();
            capturescreen.TakeScreenShot(app);
            Thread.sleep(1000);
            app.driver1.findElement(POM_TPPLogin.OBLoginUsername_Button).sendKeys(data.get("Email"));
            app.driver1.findElement(POM_TPPLogin.OBLoginPassword_Button).sendKeys(data.get("Password"));
            capturescreen.TakeScreenShot(app);
            app.driver1.findElement(POM_TPPLogin.OBLoginSignOn_Button).click();
            capturescreen.TakeScreenShot(app);
            //Open new tab in chrome
            ((JavascriptExecutor) app.driver1).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(app.driver1.getWindowHandles());
            app.driver1.switchTo().window(tabs.get(1));
            System.out.println("PingID Window Opened");
            //get the otp
            strOTP = OBDirectory.LaunchMailGETOTP(app.driver1, data.get("EUsrName"), data.get("EPasswd"));
            app.driver1.close();
            app.driver1.switchTo().window(tabs.get(0));
            app.driver1.findElement(POM_TPPLogin.OBLoginOTP_Text).sendKeys(strOTP);
            System.out.println("PingID OTP Entered");
            Thread.sleep(1000);
            capturescreen.TakeScreenShot(app);
            app.driver1.findElement(POM_TPPLogin.OBAuthSignOn_Button).click();

            Thread.sleep(8000);
            capturescreen.TakeScreenShot(app);
            WebElement ActMsg = app.driver1.findElement(POM_TPPOrg.TPPOrg_Username_Display);
            String ActMsgText = ActMsg.getText();

            if (ActMsgText.contains(data.get("Name"))) {
                map.put("Status", "Pass");
                map.put("Message", "Login Successful");
                map.put("OTP: ", strOTP);
                map.put("UserName: ", ActMsgText);

            } else {
                map.put("Status", "Fail");
                map.put("Message", "Login UnSuccessful");
                map.put("OTP: ", strOTP);
                map.put("UserName: ", ActMsgText);
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }


    //Function to to Login to TPP Portal with OB Credentials
    public static Map<String, Object> Verify_PTC_Login_Details(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "Verify_PTC_Login_Details";
        map.put("Test_Name", app.t_name1);

        try {
            Thread.sleep(5000);
            WebElement UsrName = app.driver1.findElement(POM_TPPOrg.TPPOrg_Username_Display);
            String UsrNameText = UsrName.getText();
            if (UsrNameText.contains(data.get("Name"))) {
                map.put("Status", "Pass");
                map.put("Message", "Username Verified after Logon");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Username doesn't match");

            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
        }

        return map;
    }

    // Functions to Logout from TPP Portal
    public static Map<String, Object> PTC_Logout_TPP_Hub(webapp app) {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Logout_TPP_Hub";
        map.put("Test_Name", app.t_name1);

        try {
            Thread.sleep(5000);
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPOrg.TppOrg_Logout_Link).click();
            Thread.sleep(8000);
            String AftrLogoutPage = app.driver1.getTitle();
            captureScreenShot2(app);
            if (AftrLogoutPage.equals("Sign Off Successful")) {

                map.put("Status", "Pass");
                map.put("Message", "Logout Successful");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Logout Not Successful");
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }


    // Function to select Organisation on TPP Hub
    public static Map<String, Object> PTC_Select_Organisation(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Select_Organisation";
        map.put("Test_Name", app.t_name1);

        try {

            Thread.sleep(2000);

            //Paras
            boolean bSelectApp = false;
//            List<WebElement> lstApp = ReusableFunctions.findUIElementsList(app,POM_TPPOrg.TppOrg_lst_App);
            List<WebElement> lstApp = app.driver1.findElements(By.xpath("//*[@class='panel-default ng-scope ng-isolate-scope panel']//label/span"));

            String strExpVal = data.get("OrganisationName");
            for (WebElement rdbApp : lstApp) {
                String strActVal = rdbApp.getText();
                if (strActVal.equals(strExpVal)) {
                    bSelectApp = true;
                    rdbApp.click();
                    break;
                }
            }


            // Paras - commented the below continue click as of now. will create the separate method to click button
            Thread.sleep(3000);
            //captureScreenShot2(app);
            app.driver1.findElement(POM_TPPOrg.Continue_Button).click();
            Thread.sleep(3000);
            //captureScreenShot2(app);
            boolean AppTab = app.driver1.findElement(POM_TPPApp.HeadApp_Button).isDisplayed();
            if (bSelectApp) {
                map.put("Status", "Pass");
                map.put("Message", "Organisation Selected Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Organisation Selection Failed");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    // Function to Add Application For selected Organisation
    public static Map<String, Object> Add_Application_For_Selected_Organisation(webapp app, Map<String, String> data,boolean forceRemove) {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "Add_Application_For_Selected_Organisation";
        map.put("Test_Name", app.t_name1);
        boolean bSuccess = false, bAppFound = false;
        String strAppName = data.get("ApplicationName");
        try {
            Thread.sleep(2000);
            //Paras - remove the application if present already before adding
            if(forceRemove){
                bAppFound = checkApplicationPresent(app, strAppName);
                if (bAppFound) {
                    selectApplicationForRemoval(app, data,true);
                    clickRemoveApplicationButton(app);
                }
            }


            ReusableFunctions.ClickElement(app, POM_TPPApp.AddApp_Button);
            ReusableFunctions.sleep(6000);
            String strFilePath = data.get("SSAFilePath");
            clickBrowseAndSelectFile(app, strFilePath);
            clickUpload(app);

            ReusableFunctions.ClickElement(app, POM_TPPApp.addApplication_Button);
            ReusableFunctions.waitUntillElementExist(app, POM_TPPApp.AppSuccess_Msg, 40);


            bSuccess = verifySuccessMessage(app);

            if (bSuccess) {
                map.put("Status", "Pass");
                map.put("Message", "Application Added Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Application Not Added");
            }

            //Paras - need to write the code to verify the application name which is added
        } catch (Exception e) {
            map.put("Status", "Fail");
            map.put("Message", "Exception -" + e.getMessage());
        }

        return map;
    }

    // Functions to Search/Select given Application
    public static Map<String, Object> Search_Application_From_Existing_List(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        boolean bAppFound = false;
        app.moduleName1 = "Select_Application_From_Existing_List";
        map.put("Test_Name", app.t_name1);
        String expAppName = null;
        try {
            List<WebElement> lstApplication = app.driver1.findElements(POM_TPPApp.AppNames_AllElements);
            //print values
            expAppName = data.get("ApplicationName");
            for (WebElement appName : lstApplication) {
                //System.out.println(appName.getText());
                if (appName.getText().contains(expAppName)) {
                    bAppFound = true;
                    break;
                }
                TakeScreenShot(app);
            }

            if (bAppFound) {
                map.put("Status", "Pass");
                map.put("Message", "Application found -" + expAppName);
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Application not found -" + expAppName);
            }
            //Also add for expanding Application Details
        } catch (Exception e) {
            map.put("Status", "Fail");
            map.put("Message", "Exeption - " + e.getMessage());
        }
        return map;
    }

    public static Map<String, Object> Get_ClientSecrete_For_Given_Application(webapp app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "Get_ClientSecrete_For_Given_Application";
        map.put("Test_Name", app.t_name1);
        try {
            //captureScreenShot2(app);
            //click on show secret id
//            String xpathShowCS = "/*//*[text()='" + strAppName + "']/following::button[@class='btn-show' and @ng-click='showSecretData()']";
//            app.driver1.findElement(By.xpath(xpathShowCS)).click();
            clickShowClient(app, strAppName);
            Thread.sleep(5000);
            map = getClientId(app);
//            WebElement CopyCSPopup = app.driver1.findElement(By.xpath("//*[@id='showSecretPopup']/div/div[3]/button[1]"));
//            String CopyCSMsgText = CopyCSPopup.getText();
//            System.out.println("\n" + CopyCSMsgText);
//            if (CopyCSMsgText.contains("Copy client secret")) {
//                WebElement CS = app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[2]/p"));
//                String CSPopup = CS.getText();
//                String ClientSecretId = CSPopup.split(" ")[6];
//                System.out.println("\n" + "Client Secret ID:" + ClientSecretId);
//                //click on ok button
//                app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[3]/button[2]")).click();
//                //System.out.println("Clicked OK");
//                Thread.sleep(2000);
//                map.put("Status", "Pass");
//                map.put("Message", "Client Secrete Copied");
//                map.put("Application_ClientID", ClientSecretId);
//            }
//            else {
//                map.put("Status", "Fail");
//                map.put("Message", "Client Secrete does Not Copied");
//            }
//
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    public static Map<String, Object> Sucessfully_Reset_ClientSecrete_For_Given_Application(webapp app, Map<String, String> data) throws InterruptedException, IOException {

        /// put one more varaible for Cnacel Reset CS
        Map<String, Object> map = new HashMap<String, Object>();
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "Sucessfully_Reset_ClientSecrete_For_Given_Application";
        map.put("Test_Name", app.t_name1);

        try {
            //click on Reset CS Button
//            String xpathResetCS = "/*//*[text()='" + strAppName + "']/following::button[@class='btn-reset' and @ng-click='resetSecretData()']";
//            app.driver1.findElement(By.xpath(xpathResetCS)).click();
            clickResetClient(app, strAppName);
            System.out.println("\n" + "Clicked on ResetCS button for given application");
            Thread.sleep(10000);
            app.driver1.findElement(By.xpath("//input[@id='accpt_cond']")).click();
            Thread.sleep(5000);
            System.out.println("\n" + "Clicked on tickbox for Reset CS");
            app.driver1.findElement(By.xpath("//button[@class='btn btn-submit ng-binding']")).click();
            System.out.println("\n" + "Clicked on Submit Button the Reset Client Secrete Alert");
            Thread.sleep(10000);
            WebElement ResetMsg = app.driver1.findElement(By.xpath(("/*//*[@ng-class='isClosable']")));
            String ResetSuccess = ResetMsg.getText();

            if (ResetSuccess.contains("Success: Client secret for application")) {
                System.out.println(ResetSuccess);
                Thread.sleep(2000);
                map.put("Status", "Pass");
                map.put("Message", "Client Secrete for Given Application Reseted successfully");
            } else {
                System.out.println("Client Secrete Not Reseted");
                map.put("Status", "Fail");
                map.put("Message", "Reset Client Secrete failed");
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }

    public static Map<String, Object> Cancel_Reset_ClientSecrete_For_Given_Application(webapp app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        WebElement eleAction = null;
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "Cancel_Reset_ClientSecrete_For_Given_Application";
        map.put("Test_Name", app.t_name1);

        try {
            //click on Reset CS Button
//            String xpathResetCS = "/*//*[text()='" + strAppName + "']/following::button[@class='btn-reset' and @ng-click='resetSecretData()']";
//            app.driver1.findElement(By.xpath(xpathResetCS)).click();
            clickResetClient(app, strAppName);
            System.out.println("\n" + "Clicked on ResetCS button for given application");
            Thread.sleep(10000);
            WebElement ResetCSYes = app.driver1.findElement(By.xpath("//input[@id='accpt_cond']"));
            ResetCSYes.click();
            Thread.sleep(5000);
            System.out.println("\n" + "Clicked on tickbox for Reset CS");

            // Paras - case to handle the cancel or close button operation
            if (data.get("action").toLowerCase().trim().equals("cancel")) {
                eleAction = app.driver1.findElement(POM_TPPResetCS.NoReset_Button);

            } else {
                eleAction = app.driver1.findElement(POM_TPPResetCS.CloseAlert_Button);
            }
            eleAction.click();
            System.out.println("\n" + "Clicked on Cancel Button of the Reset Client Secrete Alert");
            Thread.sleep(5000);
            WebElement ResetCanSucess = app.driver1.findElement(POM_TPPApp.AddApp_Button);

            if ((ResetCanSucess.isDisplayed())) {
                Thread.sleep(2000);
                map.put("Status", "Pass");
                map.put("Message", "Reset Client Secret Cancellation was Successful");
            } else {
                //System.out.println("Client Secrete Not Reseted");
                map.put("Status", "Fail");
                map.put("Message", "Reset Client Secrete failed");
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }


    public static Map<String, Object> selectApplicationForRemoval(webapp app, Map<String, String> data,boolean addAppRequire) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "Remove_Selected_Application_From_Portal";
        map.put("Test_Name", app.t_name1);


        try {


            //click on Reset CS Button
//            String xpathRemoveApp = "/*//*[text()='" + strAppName + "']/following::button[@title='Remove' and @ng-click='!tppBlocked?deleteApplicaiton():true']";
//            app.driver1.findElement(By.xpath(xpathRemoveApp)).click();
            clickRemoveClient(app, data,addAppRequire);
            System.out.println("\n" + "Clicked on RemoveApp button for given application");
            Thread.sleep(10000);
            app.driver1.findElement(By.xpath("//input[@id='accpt_cond']")).click();
            Thread.sleep(5000);

            map.put("Status", "Pass");
            map.put("Message", "Remove Application is clicked successfully checkbox is selected");

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", e);
            System.out.println(e);
        }
        return map;
    }

    public static Map clickRemoveApplicationButton(webapp app) {

        Map<String, Object> map = new HashMap<String, Object>();
        WebElement RemoveSuccess;
        app.driver1.findElement(By.xpath("//button[@class='btn btn-submit ng-binding']")).click();
        System.out.println("Clicked on Yes-Remove Application");
        ReusableFunctions.sleep(10000);

        RemoveSuccess = app.driver1.findElement(By.xpath(("//*[@ng-class='isClosable']")));
        String RemoveSuccessMsg = RemoveSuccess.getText();
        if (RemoveSuccessMsg.contains("Success: The application")) {
            System.out.println(RemoveSuccessMsg);
            ReusableFunctions.sleep(2000);
            map.put("Status", "Pass");
            map.put("Message", "Application is removed successfully");
        } else {
            map.put("Status", "Fail");
            map.put("Message", "Application is not removed successfully");
        }
        return map;
    }


    public static Map clickDonotRemoveApplicationButton(webapp app) {

        Map<String, Object> map = new HashMap<String, Object>();
        WebElement RemoveSuccess;
        app.driver1.findElement(POM_TPPApp.donotRemoveApp).click();

        if (ReusableFunctions.CheckElementPresent(app, POM_TPPApp.AddApp_Button)) {
            map.put("Status", "Pass");
            map.put("Message", "Donot remove Application is clicked");

        } else {
            map.put("Status", "Fail");
            map.put("Message", "Donot remove Application is clicked");

        }
        return map;
    }

    public static Map<String, Object> Cancel_Remove_Application_Process_From_Given_ApplicationList(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "Cancel_Remove_Application_Process_From_Given_ApplicationList";
        map.put("Test_Name", app.t_name1);

        try {
            //click on Reset CS Button
            String xpathRemoveApp = "/*//*[text()='" + strAppName + "']/following::button[@title='Remove' and @ng-click='!tppBlocked?deleteApplicaiton():true']";
            app.driver1.findElement(By.xpath(xpathRemoveApp)).click();
            System.out.println("\n" + "Clicked on Remove App button for given application");
            Thread.sleep(10000);
            WebElement ResetCSYes = app.driver1.findElement(By.xpath("//input[@id='accpt_cond']"));
            ResetCSYes.click();
            Thread.sleep(5000);
            System.out.println("\n" + "Clicked on tickbox for Reset CS");
            app.driver1.findElement(By.xpath("//*[@ng-bind='deleteApplicationBtnText1']")).click();
            System.out.println("\n" + "Clicked on Cancel Button of the Remove Application Alert");
            Thread.sleep(5000);
            WebElement ResetCanSucess = app.driver1.findElement(POM_TPPApp.AddApp_Button);

            if ((ResetCanSucess.isDisplayed())) {
                Thread.sleep(2000);
                map.put("Status", "Pass");
                map.put("Message", "Remove Application Cancellation was Successful");
            } else {
                //System.out.println("Client Secrete Not Reseted");
                map.put("Status", "Fail");
                map.put("Message", "Remove Application process not cancelled");
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }


    //Paras
//    public static void verifyOrganisationTab(webapp app){
//        POM_TPPOrg org = PageFactory.initElements(app.driver1,POM_TPPOrg.class);
//
//        String strOrgTab = org.orgTab.getText();
//        if(strOrgTab.equals("Organisations"))
//    }

    public static Map verifyApplicationFields(webapp app) {

        HashMap<String, String> allFieldVal = new HashMap<>();
//        ArrayList<String> actAllFieldsLst = new ArrayList<>();
//        String[] expAllFieldsLst ={"TPP Role,","TPP Email ID","TPP Phone Number","TPP Organisation ID","TPP Member State Competent Authority","TPP Registration ID"};
//        ArrayList<String> Act1AllFieldsLst ={"TPP Role,","TPP Email ID","TPP Phone Number","TPP Organisation ID","TPP Member State Competent Authority","TPP Registration ID"};
        String strFieldName = null, strFieldVal = null;


        List<WebElement> lstAppFields = ReusableFunctions.findUIElementsList(app, POM_TPPOrg.TppOrg_lst_AppFields);

        for (WebElement field : lstAppFields) {
            strFieldName = field.findElement(By.xpath("./span[1]")).getText();
            strFieldVal = field.findElement(By.xpath("./span[2]")).getText();
//            actAllFieldsLst.add(strFieldName);
            if (strFieldVal.contains(",")) {
                String temp = field.findElement(By.xpath("./span[3]")).getText();
                strFieldVal = strFieldVal + temp;
            }
            allFieldVal.put(strFieldName, strFieldVal);
        }


//        if(Arrays.asList(actAllFieldsLst).equals(expAllFieldsLst))
//            allFieldVal.put("count","6");
//            else
//            allFieldVal.put("count","6");


        return allFieldVal;
    }

    public static void clickShowClient(webapp app, String appName) throws UnsupportedEncodingException {
        String strAppName = appName;
        app.moduleName1 = "clickShowClient";

        boolean bAppFound = false;
        bAppFound = checkApplicationPresent(app, strAppName);
        if (!bAppFound) {
//            JSONObject data = new JSONObject();
//            data.put("ApplicationName", strAppName);
//            data.put("SSAFilePath", "./src/test/java/CMA_Release/Base_Data/SSA.txt");
//            Add_Application_For_Selected_Organisation(app, data);
            strAppName = ReusableFunctions.getElementText(app,POM_TPPApp.firstApp);
        }

        String xpathShowCS = "/*//*[text()='" + strAppName + "']/following::button[@class='btn-show' and @ng-click='showSecretData()']";
        app.driver1.findElement(By.xpath(xpathShowCS)).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        capturescreen.TakeScreenShot(app);
    }

    public static void clickResetClient(webapp app, String appName) {
        String strAppName = appName;
        app.moduleName1 = "clickResetClient";

        boolean bAppFound = false;
        bAppFound = checkApplicationPresent(app, strAppName);
        if (!bAppFound) {
            JSONObject data = new JSONObject();
            data.put("ApplicationName", strAppName);
            data.put("SSAFilePath", "./src/test/java/CMA_Release/Base_Data/SSA.txt");
            Add_Application_For_Selected_Organisation(app, data,true);
//            strAppName = ReusableFunctions.getElementText(app,POM_TPPApp.firstApp);
        }

        String xpathResetCS = "/*//*[text()='" + strAppName + "']/following::button[@class='btn-reset' and @ng-click='resetSecretData()']";
        app.driver1.findElement(By.xpath(xpathResetCS)).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        capturescreen.TakeScreenShot(app);
    }

    public static Map getClientId(webapp app) {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            WebElement CopyCSPopup = app.driver1.findElement(By.xpath("//*[@id='showSecretPopup']/div/div[3]/button[1]"));
            String CopyCSMsgText = CopyCSPopup.getText();
            System.out.println("\n" + CopyCSMsgText);
            if (CopyCSMsgText.contains("Copy client secret")) {
                WebElement CS = app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[2]/p"));
                String CSPopup = CS.getText();
                String ClientSecretId = CSPopup.split(" ")[6];
                System.out.println("\n" + "Client Secret ID:" + ClientSecretId);
                //click on ok button
                app.driver1.findElement(By.xpath("/*//*[@id='showSecretPopup']/div/div[3]/button[2]")).click();
                //System.out.println("Clicked OK");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                map.put("Status", "Pass");
                map.put("Message", "Client Secrete Copied");
                map.put("CS_ID", ClientSecretId);
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Client Secrete does Not Copied");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }

    public static HashMap verifyUrlInNewBrowserTab(webapp app, String strUrl) {
        HashMap<String, String> map = new HashMap<String, String>();
        ((JavascriptExecutor) app.driver1).executeScript("window.open()");


//        Set<String> allwindows = app.driver1.getWindowHandles();
//
//        app.driver1.switchTo().window(allwindows.)

        ArrayList<String> tabs = new ArrayList<String>(app.driver1.getWindowHandles());
        app.driver1.switchTo().window(tabs.get(1));
        app.driver1.get(strUrl);
        System.out.println(strUrl);

        // verify the url
        String strCurrent = app.driver1.getCurrentUrl();
        if (strCurrent.equals(strUrl)) {
            map.put("Status", "Pass");
            map.put("Message", "Url is working as expected in new browser tab");
        } else {
            map.put("Status", "Fail");
            map.put("Message", "Url is not working as expected in new browser tab");
        }
        map.put("Actual", strCurrent);
        map.put("Expected", strUrl);

        app.driver1.close();
        app.driver1.switchTo().window(tabs.get(0));

        return map;
    }


    public static void clickRemoveClient(webapp app, Map<String,String> data, boolean sameAppRemove) {
        String strAppName = data.get("ApplicationName");
        app.moduleName1 = "clickRemoveClient";

        boolean bAppFound = false;
        bAppFound = checkApplicationPresent(app, strAppName);
        if (!bAppFound) {
            if(sameAppRemove){
                JSONObject JSONdata = new JSONObject();
                JSONdata.put("ApplicationName", strAppName);
                JSONdata.put("SSAFilePath", data.get("FilePath"));
                Add_Application_For_Selected_Organisation(app, JSONdata,true);
//            strAppName = ReusableFunctions.getElementText(app,POM_TPPApp.firstApp);

            }else{

                strAppName = ReusableFunctions.getElementText(app,POM_TPPApp.firstApp);
            }


        }

        String xpathRemoveApp = "//*[text()='" + strAppName + "']/following::button[@title='Remove' and @ng-click='!tppBlocked?deleteApplicaiton():true']";
        app.driver1.findElement(By.xpath(xpathRemoveApp)).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        capturescreen.TakeScreenShot(app);
    }


    public static Map<String, Object> verifyHyperLinkUrlInTabbedBrowser(webapp app, By locator, String expUrl) {

        Map<String, Object> map = new HashMap<String, Object>();
        ArrayList<String> arrWindows = new ArrayList<>();
        String strHelpUrl = null;
        try {
            //For Loging to TPP Portal
            ReusableFunctions.waitUntillElementExist(app, locator, 10);
            ReusableFunctions.scrollTillElement(app, locator);
            app.driver1.findElement(locator).click();
            ReusableFunctions.sleep(5000);
            arrWindows = ReusableFunctions.getAllWindowHandles(app);
            ReusableFunctions.switchToWindow(app, arrWindows, 1);
            strHelpUrl = app.driver1.getCurrentUrl();
            if (strHelpUrl.equals(expUrl)) {
                map.put("Status", "Pass");
                map.put("Message", "Help URL is verified successfully - " + strHelpUrl);

            } else {
                map.put("Status", "Fail");
                map.put("Message", "Help URL is not verified successfully - " + strHelpUrl);
            }
            app.driver1.close();
        } catch (Exception e) {
            map.put("Status", "Fail");
            map.put("Message", "Help URL is not verified successfully - " + strHelpUrl);
        }

        return map;
    }

    public static Map clickBrowseAndSelectFile(webapp app, String strFilePath) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            app.moduleName1 = "clickBrowseAndSelectFile";
            app.driver1.findElement(POM_TPPApp.Browse_Button).click();
            Thread.sleep(5000);
            //To upload SSA file from given path
            ReusableFunctions.UploadFileRobot(strFilePath);
            Thread.sleep(10000);
            capturescreen.TakeScreenShot(app);

            map.put("Status", "Pass");
            map.put("Message", "Application file is browsed Successfully -" + strFilePath);


        } catch (Exception e) {
            System.out.println(e);
        }

        return map;
    }


    public static void clickUpload(webapp app) {
        try {
            app.moduleName1 = "clickUpload";
            ReusableFunctions.ClickElement(app, POM_TPPApp.Upload_Button);
            System.out.println("Certificate Loaded");
            //To input the filename along with path
            Thread.sleep(10000);
            capturescreen.TakeScreenShot(app);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void clickCancel(webapp app) {
        try {
            ReusableFunctions.ClickElement(app, POM_TPPApp.Cancel_Button);
            System.out.println("Certificate Loaded");
            //To input the filename along with path
            Thread.sleep(10000);
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public static HashMap verifyAddedApplicationFields(webapp app) {

        String[] expFieldNames = {"Application name", "Application description", "Client ID", "Redirect URI(s)", "Terms of service URI", "Policy URI", "JWKS end point"};
        WebElement tblApplication;
        String fieldName = null;
        ArrayList<String> actFieldNames = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        app.moduleName1 = "verifyAddedApplicationFields";
        capturescreen.TakeScreenShot(app);

        tblApplication = ReusableFunctions.findUIElement(app, POM_TPPApp.applicationFieldTbl);
        List<WebElement> tblRows = tblApplication.findElements(By.tagName("tr"));

        for (WebElement row : tblRows) {
            fieldName = row.findElement(By.tagName("th")).getText();
            actFieldNames.add(fieldName);
        }

        if (Arrays.asList(expFieldNames).equals(actFieldNames)) {
            map.put("Status", "Pass");
            map.put("Message", "Application field values are displaying correctly -" + actFieldNames.toString());

        } else {
            map.put("Status", "Fail");
            map.put("Message", "Application field values are not displaying correctly -" + actFieldNames.toString());
        }
        map.put("Actual", actFieldNames.toString());
        map.put("Expected", Arrays.asList(expFieldNames).toString());
        return map;
    }


    public static HashMap verifyShowMoreApplicationFields(webapp app) {

        String[] expFieldNames = {"Application description", "Client ID", "Redirect URI(s)", "Terms of service URI", "Policy URI", "JWKS end point"};
        WebElement tblApplication;
        String fieldName = null;
        ArrayList<String> actFieldNames = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        app.moduleName1 = "verifyShowMoreApplicationFields";
        capturescreen.TakeScreenShot(app);


        tblApplication = ReusableFunctions.findUIElement(app, POM_TPPApp.showMoreApplicationFieldTbl);
        List<WebElement> tblRows = tblApplication.findElements(By.tagName("tr"));

        for (int i = 0; i < tblRows.size() - 1; i++) {
            fieldName = tblRows.get(i).findElement(By.tagName("th")).getText();
            actFieldNames.add(fieldName);
        }

        if (Arrays.asList(expFieldNames).equals(actFieldNames)) {
            map.put("Status", "Pass");
            map.put("Message", "Application field values are displaying correctly -" + actFieldNames.toString());

        } else {
            map.put("Status", "Fail");
            map.put("Message", "Application field values are not displaying correctly -" + actFieldNames.toString());
        }
        map.put("Actual", actFieldNames.toString());
        map.put("Expected", Arrays.asList(expFieldNames).toString());
        return map;
    }


    public static boolean verifySuccessMessage(webapp app) {
        WebElement AddAppSuccessPopup;
        String AddAppSuccessText;
        boolean bSuccess = false;
        app.moduleName1 = "verifySuccessMessage";
        HashMap<String, String> map = new HashMap<>();

        AddAppSuccessPopup = ReusableFunctions.findUIElement(app, POM_TPPApp.AppSuccess_Msg);
        AddAppSuccessText = AddAppSuccessPopup.getText();

        if (AddAppSuccessText.contains("Success: The application")) {
            bSuccess = true;
        }

        app.moduleName1 = "verifySuccessMessage";
        capturescreen.TakeScreenShot(app);

        if (bSuccess) {
            map.put("Status", "Pass");
            map.put("Message", "Application Added Successfully");
        } else {
            map.put("Status", "Fail");
            map.put("Message", "Application Not Added");
        }

        return bSuccess;
    }


    public static HashMap verifyRole(webapp app, String strAppName, String expectedRole) {
        HashMap<String, String> map = new HashMap<>();

        String xpath, actRole = null;

        app.moduleName1 = "verifyRole";
        capturescreen.TakeScreenShot(app);

        xpath = "//*[text()='" + strAppName + "']/ancestor::tr/td[3]/a";

        List<WebElement> lstRoles = app.driver1.findElements(By.xpath(xpath));
        ArrayList<String> lstRolesNames = new ArrayList<>();

        for (WebElement role : lstRoles) {
            lstRolesNames.add(role.findElement(By.tagName("span")).getText());
        }

        if (lstRolesNames.size() > 1) {
            actRole = lstRolesNames.get(0) + "," + lstRolesNames.get(1);

        }

        if (expectedRole.equals(actRole)) {
            map.put("Status", "Pass");
            map.put("Message", "Role is correctly verified");
            map.put("Actual", actRole);
            map.put("Expected", expectedRole);

        } else {
            map.put("Status", "Fail");
            map.put("Message", "Role is not correctly verified");
            map.put("Actual", actRole);
            map.put("Expected", expectedRole);
        }
        return map;

    }

    public static HashMap<String, String> verifyAppFailureMessage(webapp app, String strExpErrorMessage) {
        WebElement AddAppElementError1, AddAppElementError2;
        String strError1, strError2;
        boolean bSuccess = false;
        app.moduleName1 = "verifySuccessMessage";
        HashMap<String, String> map = new HashMap<>();

        AddAppElementError1 = ReusableFunctions.findUIElement(app, POM_TPPApp.failureErrorMessage1);
        AddAppElementError2 = ReusableFunctions.findUIElement(app, POM_TPPApp.failureErrorMessage2);
        strError1 = AddAppElementError1.getText();
        strError2 = AddAppElementError2.getText();

        if (strError1.toLowerCase().contains("error") && strError2.equals(strExpErrorMessage)) {
            bSuccess = true;
        }

        app.moduleName1 = "verifySuccessMessage";
        capturescreen.TakeScreenShot(app);

        map.put("Actual", strError2);
        map.put("Expected", strExpErrorMessage);

        if (bSuccess) {
            map.put("Status", "Pass");
            map.put("Message", "User got the expected error.");
        } else {
            map.put("Status", "Fail");
            map.put("Message", "User didnot get the expected error.");
        }

        return map;
    }


    public static HashMap<String, String> verifyExplanatoryInformation(webapp app) {
        WebElement AddAppSuccessPopup, AddAppFailureMessage;
        String AddAppSuccessText, AddAppFailureMessageText;
        boolean bSuccess = false;
        app.moduleName1 = "verifySuccessMessage";
        HashMap<String, String> map = new HashMap<>();

        ReusableFunctions.MouseHoveronElement(app, POM_TPPApp.helpQuesMark);
        ReusableFunctions.CheckElementPresent(app, POM_TPPApp.helpInfo);

        ReusableFunctions.MouseHoveronElement(app, POM_TPPApp.ClientSecretQuesMark);
        ReusableFunctions.CheckElementPresent(app, POM_TPPApp.ClientSecretInfo);


        return map;
    }


    public static Map<String, Object> browseApplication(webapp app, Map<String, String> data) {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "Add_Application_For_Selected_Organisation";
        map.put("Test_Name", app.t_name1);
        boolean bSuccess = false;
        try {
            Thread.sleep(2000);
            ReusableFunctions.ClickElement(app, POM_TPPApp.AddApp_Button);
            Thread.sleep(6000);
            String strFilePath = data.get("SSAFilePath");
            map = clickBrowseAndSelectFile(app, strFilePath);


            //Paras - need to write the code to verify the application name which is added
        } catch (Exception e) {
            map.put("Status", "Fail");
            map.put("Message", "Exception -" + e.getMessage());
        }

        return map;
    }


    public static Map<String, Object> verifyApplicationFormatErrorMessage(webapp app, String strError) {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "verifyApplicationFormatErrorMessage";
        map.put("Test_Name", app.t_name1);
        boolean bSuccess = false;
        try {
            Thread.sleep(2000);
            ReusableFunctions.ClickElement(app, POM_TPPApp.AddApp_Button);
            Thread.sleep(6000);
            String strFilePath = strError;
            map = clickBrowseAndSelectFile(app, strFilePath);
            clickUpload(app);

            map.put("Status", "Pass");
            map.put("Message", "Application uploaded Successfully");

            //Paras - need to write the code to verify the application name which is added
        } catch (Exception e) {
            map.put("Status", "Fail");
            map.put("Message", "Exception -" + e.getMessage());
        }

        return map;
    }


    public static HashMap verifyUrlInNewBrowserTab(webapp app, String strActUrl, String strExpUrl) {
        HashMap<String, String> map = new HashMap<String, String>();
        ((JavascriptExecutor) app.driver1).executeScript("window.open()");


//        Set<String> allwindows = app.driver1.getWindowHandles();
//
//        app.driver1.switchTo().window(allwindows.)

        ArrayList<String> tabs = new ArrayList<String>(app.driver1.getWindowHandles());
        app.driver1.switchTo().window(tabs.get(1));
        app.driver1.get(strActUrl);
        System.out.println(strActUrl);

        // verify the url
        String currentUrl = app.driver1.getCurrentUrl();
        if (currentUrl.equals(strExpUrl)) {
            map.put("Status", "Pass");
            map.put("Message", "Url is working as expected in new browser tab");
        } else {
            map.put("Status", "Fail");
            map.put("Message", "Url is not working as expected in new browser tab");
        }
        map.put("Actual", currentUrl);
        map.put("Expected", strExpUrl);

        app.driver1.close();
        app.driver1.switchTo().window(tabs.get(0));

        return map;
    }


    public static Map<String, Object> PTC_Select_Organisation_WithoutContinueClick(GetApplication app, String orgName) {

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Select_Organisation";
        try {

            ReusableFunctions.sleep(2000);

            //Paras
            boolean bSelectApp = false;
//            List<WebElement> lstApp = ReusableFunctions.findUIElementsList(app,POM_TPPOrg.TppOrg_lst_App);
            List<WebElement> lstApp = app.driver1.findElements(By.xpath("//*[@class='panel-default ng-scope ng-isolate-scope panel']//label/span"));

            String strExpVal = orgName;
            for (WebElement rdbApp : lstApp) {
                String strActVal = rdbApp.getText();
                if (strActVal.equals(strExpVal)) {
                    bSelectApp = true;
                    rdbApp.click();
                    break;
                }
            }

            if (bSelectApp) {
                map.put("Status", "Pass");
                map.put("Message", "Organisation Selected Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Organisation Selection Failed");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    public static boolean checkApplicationPresent(webapp app, String strAppName) {
        boolean bAppFound = false;
        By appNameXpath;
        appNameXpath = xpath("//*[text()='" + strAppName + "']");


        if (ReusableFunctions.CheckElementPresent(app, appNameXpath)) {
            bAppFound = true;
        }

        return bAppFound;
    }


    public static Map<String, Object> PTC_Select_Organisation(GetApplication app, String strAppName){

        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "PTC_Select_Organisation";

        try {

            Thread.sleep(2000);

            //Paras
            boolean bSelectApp = false;
//            List<WebElement> lstApp = ReusableFunctions.findUIElementsList(app,POM_TPPOrg.TppOrg_lst_App);
            List<WebElement> lstApp = app.driver1.findElements(By.xpath("//*[@class='panel-default ng-scope ng-isolate-scope panel']//label/span"));

            String strExpVal = strAppName;
            for (WebElement rdbApp : lstApp) {
                String strActVal = rdbApp.getText();
                if (strActVal.equals(strExpVal)) {
                    bSelectApp = true;
                    rdbApp.click();
                    break;
                }
            }

            // Paras - commented the below continue click as of now. will create the separate method to click button
            Thread.sleep(3000);
            //captureScreenShot2(app);
            app.driver1.findElement(POM_TPPOrg.Continue_Button).click();
            Thread.sleep(3000);
            //captureScreenShot2(app);
            boolean AppTab = app.driver1.findElement(POM_TPPApp.HeadApp_Button).isDisplayed();

            if (bSelectApp) {
                map.put("Status", "Pass");
                map.put("Message", "Organisation Selected Successfully");
            } else {
                map.put("Status", "Fail");
                map.put("Message", "Organisation Selection Failed");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }

        return map;
    }

    public static Map verifyApplicationRemoved(webapp app,String strAppName){

        boolean bAppRemoved = false;
        By xpathApp ;
        HashMap<String,String> map = new HashMap<>();

        xpathApp = xpath("//*[text()='"+strAppName+"']");
        bAppRemoved = ReusableFunctions.CheckElementPresent(app, xpathApp);

        if(! bAppRemoved){
            map.put("Status","Pass");
            map.put("Message","Removed Application is not found. List is updated correctly after removing the application");

        }else{
            map.put("Status","Fail");
            map.put("Message","Removed Application is found. List is not updated correctly after removing the application");
        }

        return map;

    }


}


