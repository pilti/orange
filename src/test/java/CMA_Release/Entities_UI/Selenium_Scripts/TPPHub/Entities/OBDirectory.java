package CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities;

import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.PageObjects.TPPHub.*;
import CMA_Release.Entities_UI.Selenium.ReusableFunctions;
import org.openqa.selenium.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static CMA_Release.Entities_UI.General.capturescreen.captureScreenShot2;

//import CMA_Release.Java_Lib.CustomFolder;

public class OBDirectory {
    //Function to Launch OB Portal URL

    public static Map<String, Object> launch_OB_Portal(GetApplication app, String URL) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            app.moduleName1 = "Launch_OB_Portal";
            app.driver1.manage().window().maximize();
            app.driver1.get(URL);
            Thread.sleep(2000);
            captureScreenShot2(app);

            String ActPageTitle = app.driver1.getTitle();
            if (ActPageTitle.equals("Sign On")) {

                map.put("Status", "Pass");
                map.put("Actual Title of page: ", ActPageTitle);
            } else {
                map.put("Status", "Fail");
                map.put("Actual Title of page: ", ActPageTitle);
            }

        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
        }

        return map;
    }

    //Internal Function to get OTP to Login

       public static String LaunchMailGETOTP(WebDriver driver, String EUsrName, String EPasswd) throws InterruptedException {
        String str = "\\";
        int cnt = 0;
        driver.get("https://exch.boigroup.net/owa");
        Thread.sleep(500);
        driver.findElement(POM_OBLogin.MailUsrname_Text).sendKeys("boigroup" + str + EUsrName);
        Thread.sleep(500);
        driver.findElement(POM_OBLogin.MailPasswd_Text).sendKeys(EPasswd);
        Thread.sleep(500);
        driver.findElement(POM_OBLogin.MailSubmit_Button).click();
        Thread.sleep(50000);

        //////////////////////Mail body///////////////////////////////////////////////////////////////////////////////
        WebElement ele = driver.findElement(POM_OBLogin.MailMain_Section);
        ele.click();
        Thread.sleep(1500);
        List<WebElement> ele1 = driver.findElements(POM_OBLogin.MailInbox_Section);
        ele1.get(0).click();
        Thread.sleep(1500);
        List<WebElement> element = driver.findElements(POM_OBLogin.MaiLSrchFolders_Section);
        System.out.println(element.size());
        if (!element.get(0).getText().contains("")) {
            for (int i = 0; i < element.size(); i++) {
                if (element.get(i).getText().contains("TPPAuthentication")) {
                    cnt = i;
                    cnt = cnt + 1;
                }
            }
        } else {
            ele1.get(0).click();
            Thread.sleep(1000);
            for (int i = 0; i < element.size(); i++) {
                if (element.get(i).getText().contains("TPPAuthentication")) {
                    cnt = i;
                    cnt = cnt + 1;
                }
            }
        }
        WebElement element1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[1]/div[2]/div/div[5]/div[2]/div/div/div[2]/div/div[2]/div/div/div/div[2]/div[1]/div/div[2]/div[" + cnt + "]/div/div[1]/span/div/span[2]"));
        element1.click();
        Thread.sleep(1000);
        driver.findElement(POM_OBLogin.MailOTP_Click).click();
        Thread.sleep(1000);
        //WebElement otp = driver.findElement(By.xpath("/html/body/div[2]/div/div[3]/div[3]/div/div[1]/div[2]/div[7]/div/div/div/div[2]/div/div[2]/div[3]/div/div[1]/div[1]/div[1]/div[3]/div[3]/div[1]/div/div/span/div/div[3]/table[1]/tbody/tr/td[2]/span/div/table/tbody/tr/td/div[3]/font/span/font/span"));
        WebElement otp = driver.findElement(POM_OBLogin.MailOTP_Select);
        String OTP = otp.getText();
        //System.out.println(otp.getText());
        ele.click();
        return OTP;
    }

    //Function to Login to OB Portal with UserName and Password

    public static Map<String, Object> Login_OB_Portal(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        String strOTP;
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "Login_OB_Portal";
            Thread.sleep(1000);
            app.driver1.findElement(POM_TPPLogin.OBLoginUsername_Button).sendKeys(data.get("Email"));
            app.driver1.findElement(POM_TPPLogin.OBLoginPassword_Button).sendKeys(data.get("Password"));
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBLoginSignOn_Button).click();
            //Open new tab in chrome
            ((JavascriptExecutor) app.driver1).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(app.driver1.getWindowHandles());
            app.driver1.switchTo().window(tabs.get(1));
            System.out.println("PingID Window Opened");
            //get the otp
            strOTP = LaunchMailGETOTP(app.driver1, data.get("EUsrName"), data.get("EPasswd"));
            app.driver1.close();
            app.driver1.switchTo().window(tabs.get(0));
            app.driver1.findElement(POM_TPPLogin.OBLoginOTP_Text).sendKeys(strOTP);
            System.out.println("PingID OTP Entered");
            Thread.sleep(1000);
            captureScreenShot2(app);
            app.driver1.findElement(POM_TPPLogin.OBAuthSignOn_Button).click();
            Thread.sleep(20000);
            //JavascriptExecutor jse = (JavascriptExecutor)app.driver1;
            //String ActMsgText = (jse.executeScript("return arguments[0].innerHTML;",app.driver1.findElement(POM_OBOrg.OBOrgUsrName_Check).getText())).toString();
            WebElement ActMsg = app.driver1.findElement(POM_OBOrg.OBOrgUsrName_Check);
            String ActMsgText = ActMsg.getText();

            if (ActMsgText.contains(data.get("Name"))) {
                map.put("Status", "Pass");
                map.put("OTP: ", strOTP );
                map.put("Actual UserName is: ", ActMsgText );

            } else {
                map.put("Status", "Fail");
                map.put("OTP: ", strOTP );
                map.put("Actual UserName is: ", ActMsgText );
            }
            System.out.println("Login Successful");


        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
        }

        return map;
    }

    // Function to Verify OB Login Details

    public static Map<String, Object> Verify_OB_Logindetails(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "Verify_OB_Portal";
            app.driver1.manage().window().maximize();
            Thread.sleep(10000);
            WebElement User = app.driver1.findElement(POM_OBOrg.OBOrgUsrName_Check);
            String Username = User.getText();
            System.out.println("Dsiplayed user is: "+Username);
            captureScreenShot2(app);
            if (Username.contains(data.get("Name"))) {
                map.put("Status", "Pass");
                map.put("Logged in UserName is: " , Username);
            } else {
                map.put("Status", "Fail");
                map.put("Logged in UserName is: " , Username);
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'" + e + "'");
            System.out.println(e);
        }
        return map;
    }



    // Function to Verify OB Logout from Portal
    public static Map<String, Object> Logout_OB_Portal(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            app.moduleName1 = "Logout_OB_Portal";
            app.driver1.manage().window().maximize();
            Thread.sleep(10000);
            app.driver1.findElement(POM_OBOrg.OBOrgUsrName_Check).click();
            captureScreenShot2(app);
            app.driver1.findElement(POM_OBOrg.OBOrgLogout_Check).click();
            Thread.sleep(6000);
            String AftrLogoutPage = app.driver1.getTitle();
            captureScreenShot2(app);
            if (AftrLogoutPage.equals("Signed Off")) {
                map.put("Status", "Pass");
                map.put("Actual Title after Logout is: ", AftrLogoutPage);
            } else {
                map.put("Status", "Fail");
                map.put("Actual Title after Logout is: ", AftrLogoutPage);
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
        }
        return map;
    }

    //Function to select Organisation on OB Portal
    public static Map<String, Object> OBPortal_Select_Organisation(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        String OrgBfrXpath = "//*[@id='__next']/div/div/div/table/tbody/tr[";
        String OrgAftrXpath = "]/td[3]";
        try {
            app.moduleName1 = "OB_Portal_Select_Organisation";
            app.driver1.findElement(POM_OBOrg.UncheckASPSP_Checkbox).click();
            Thread.sleep(5000);
            for(int i=1; i<=6; i++) {
                String OrgID = app.driver1.findElement(By.xpath(OrgBfrXpath + i + OrgAftrXpath)).getText();
               // System.out.println("Organisation List Under Selection: " + OrgID);
                if (OrgID.equals(data.get("Organisation"))) {
                    app.driver1.findElement(By.xpath(OrgBfrXpath + i + OrgAftrXpath)).click();
                    System.out.println("Selected Organisation is: " + OrgID);
                    break;
                }
                //captureScreenShot2(app);
            }
            Thread.sleep(10000);
            WebElement AddNewStat = app.driver1.findElement(By.xpath("//*[@class='btn btn-outline-primary float-right m-t--30 m-b-20']"));
            String AddNewStatText = AddNewStat.getText();
            captureScreenShot2(app);
            //System.out.println(AddNewStatText);

            if (AddNewStatText.equals("Add new statement")) {
                map.put("Status", "Pass");
            } else {
                map.put("Status", "Fail");
                System.out.println("Application can not be added");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
       }
        return map;
    }

    //Function to add Add new Software Statement on OB Portal
    public static Map<String, Object> OB_Add_New_software_Statement(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "OB_Add_New_software_Statement";
            Thread.sleep(5000);
            // To click on Add Application Button on Add application button page
            app.driver1.findElement(POM_OBOrg.OBOrgAddapplication_Btn).click();
            Thread.sleep(2000);
            //Add Application name
            app.driver1.findElement(POM_OBAppDetails.Client_Name).sendKeys(data.get("Client_Name"));
            //Add Application Description
            app.driver1.findElement(POM_OBAppDetails.Descrption_Name).sendKeys(data.get("Description_Name"));
            //Add URL
            app.driver1.findElement(POM_OBAppDetails.PolicyURL).sendKeys(data.get("Policy_URI"));
            //Add Terms of Use URL
            app.driver1.findElement(POM_OBAppDetails.TermsofUseURL).sendKeys(data.get("Terms_of_Service_URI"));
            //Add Redirect URL
            app.driver1.findElement(POM_OBAppDetails.RedirectURL).sendKeys(data.get("Redirect_URI"));
            //Give Version Control
            app.driver1.findElement(POM_OBAppDetails.Version).sendKeys(data.get("Version"));
            Thread.sleep(1000);
            if (data.get("Role").equals("AISP")) {
                //Select Only AISP as Role
                app.driver1.findElement(POM_OBAppDetails.AISPRole).click();
            } else if (data.get("Role").equals("PISP")) {
                //Select only PISP as Role
                app.driver1.findElement(POM_OBAppDetails.PISPRole).click();
                ;
            } else {
                //Select both AISP and PISP as Role
                app.driver1.findElement(POM_OBAppDetails.AISPRole).click();
                app.driver1.findElement(POM_OBAppDetails.PISPRole).click();
                ;
            }
            captureScreenShot2(app);
            //Click on Submit button
            app.driver1.findElement(POM_OBAppDetails.AppSubmit).click();
            Thread.sleep(15000);
            WebElement SuccessApp = app.driver1.findElement(POM_OBSoftStatements.AppSuccess_Msg);
            String SuccessAppText = SuccessApp.getText();
            System.out.println(SuccessAppText);
            captureScreenShot2(app);
            if (SuccessAppText.equals("Software Statement successfully created.")) {
                map.put("Status", "Pass");
                map.put("Display", "Software Statement successfully created.");
            } else {
                map.put("Status", "Fail");
                map.put("Display","Application not added to Portal. Please Check Inputs");
            }
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
        }

        return map;
    }

    // Function to get Software Statement Details
    public static Map<String, Object> OB_Get_software_Statement_Details(GetApplication app) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();

        try {
            app.moduleName1 = "OB_Get_software_Statement_Details";
            Thread.sleep(5000);
            Thread.sleep(180000); // Temperory pause added manual activities-Creating CSR File
            //findout OrganisationID of the application
            WebElement OrgI = app.driver1.findElement(POM_OBSoftStatements.OrgId);
            String OrgID = OrgI.getText();
            //System.out.println("The aaded Application: "+OrgID);
            String strOrganID = OrgID.split("\\n")[1];
            System.out.println("Organisation ID for Application is: " + strOrganID);

            //findout ClinetID of the application from Open Banking Site
            WebElement ClientI = app.driver1.findElement(POM_OBSoftStatements.ClientID);
            String AppClientID = ClientI.getText();
            String ClientID = AppClientID.split("\\n")[1];
            System.out.println("Client ID for Application is: " + ClientID);

            //click on generate statement
            app.driver1.findElement(POM_OBSoftStatements.Generate_Btn).click();
            Thread.sleep(8000);

            //Copy SSA text from Generated Text
            WebElement SSA = app.driver1.findElement(POM_OBSoftStatements.CopySSATxt);
            String SSAText = SSA.getText();
            System.out.println("Application SSA details are: " + SSAText);
            Thread.sleep(8000);
            //Write Copied to txt file stored in given path
            File path1 = new File("./src/test/java/CMA_Release/Entities_OB/AppSSA.txt");
            try {
                FileWriter fw = new FileWriter(path1);
                fw.write(SSAText);
                fw.close();
            } catch (IOException e) {
                System.out.println(e);
            }

            boolean Payload = app.driver1.findElement(POM_OBSoftStatements.PayloadJson).isDisplayed();
            if (Payload) {
                map.put("Status", "Pass");
                map.put("Application_ClientID", ClientID);
                map.put("Application_OrganisationID", strOrganID);
                map.put("Application_SSA_Certificate", SSAText);
            } else {
                map.put("Status", "Fail");
                map.put("Display","Software Statement not generated ");
            }
            captureScreenShot2(app);
        } catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e+ "'");
            System.out.println(e);
        }

        return map;
    }

    // Function to Upload Trasnport Certificate

    public static Map<String, Object> OB_Upload_Transport_Certificate(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
                app.moduleName1 = "OB_Upload_Transport_Certificate";
                Thread.sleep(2000);
                WebElement Ele = app.driver1.findElement(POM_OBSoftStatements.Transport_TxtBox);
                ((JavascriptExecutor) app.driver1).executeScript(
                        "arguments[0].scrollIntoView();", Ele);
                try {
                    Ele.click();
                    Ele.click();
                    //System.out.println("Double clicked the element");
                } catch (StaleElementReferenceException e) {
                    System.out.println("Element is not attached to the page document "
                            + e.getStackTrace());
                }
                Thread.sleep(5000);
            // Call Reusable Function for Uploading File
                ReusableFunctions.UploadFileRobot(data.get("TransCSRFilepath"));
                Thread.sleep(5000);
                app.driver1.findElement(POM_OBSoftStatements.Transport_UploadBtn).click();
                System.out.println("Transport Certificate Loaded");
                //To input the filename along with path
               Thread.sleep(20000);
                WebElement TransCertCheck = app.driver1.findElement(POM_OBSoftStatements.DwnLoad_Network_pem);
                String TransCertCheckText= TransCertCheck.getText();
            captureScreenShot2(app);
              //  System.out.println(TransCertCheckText);
            if(TransCertCheckText.contains("Download PEM File")) {
              // Thread.sleep(2000);
                map.put("Status", "Pass");
            map.put("Message", "Transport_Certificate_Uploded");
            }
            else {
                map.put("Status", "Fail");
                System.out.println("Transport Certificate not added");
            }
        }

        catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", e);
            System.out.println(e);
        }
        return map;
    }

    // Function to Upload Signing Certificate
    public static Map<String, Object> OB_Upload_Signing_Certificate(GetApplication app, Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "OB_Upload_Signing_Certificate";
            Thread.sleep(5000);
        //    Thread.sleep(180000); // Temperory pause added manual activities-Creating CSR File
            WebElement Ele = app.driver1.findElement(POM_OBSoftStatements.Signing_TxtBox);
            ((JavascriptExecutor) app.driver1).executeScript(
                    "arguments[0].scrollIntoView();", Ele);
            try {
                Ele.click();
                Ele.click();
                //System.out.println("Double clicked the element");
            } catch (StaleElementReferenceException e) {
                System.out.println("Element is not attached to the page document "
                        + e.getStackTrace());
            }
            Thread.sleep(8000);
            // Call Reusable Function for Uploading File
            ReusableFunctions.UploadFileRobot(data.get("SignCSRFilepath"));
            Thread.sleep(5000);
            app.driver1.findElement(POM_OBSoftStatements.Signing_UploadBtn).click();
            System.out.println("Signing Certificate Loaded");
            //To input the filename along with path
            Thread.sleep(20000);
            WebElement SignCertCheck = app.driver1.findElement(POM_OBSoftStatements.DwnLoad_Signing_pem);
            String SignCertCheckText= SignCertCheck.getText();
            captureScreenShot2(app);
            //  System.out.println(TransCertCheckText);
            if(SignCertCheckText.contains("Download PEM File")) {
                // Thread.sleep(2000);
                map.put("Status", "Pass");
                map.put("Message", "Signing_Certificate_Uploded");
            }
            else {
                map.put("Status", "Fail");
                System.out.println("Signing Certificate not added");
            }
        }
        catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", e);
            System.out.println(e);
        }
        return map;
    }

    //Function to Download Transport PEM File
    public static Map<String, Object> OB_Download_Transport_PEMFile(GetApplication app,Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "OB_Download_Transport_PEMFile";
            Thread.sleep(30000);
            WebElement TransPEM = app.driver1.findElement(POM_OBSoftStatements.DwnLoad_Network_pem);
            String TransPEMTxt= TransPEM.getText();
            //System.out.println("Click on Transport PEM download: "+TransPEMTxt);
            TransPEM.click();
            //Open new tab in chrome
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String TransPEMURL = app.driver1.getCurrentUrl();
            String str1 = TransPEMURL.replaceAll(".*qa/", "").replaceAll(".*/","");
            System.out.println(str1);
            WebElement Tpemcnt = app.driver1.findElement(By.xpath("/html/body/pre"));
            String TransPEMContent = Tpemcnt.getText();
            System.out.println(TransPEMContent);
           //File path1 = new File("./src/test/java/CMA_Release/Entities_OB/" +str1);
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            System.out.println(data.get("DownFilepath") +str1);
            File path1 = new File(data.get("DownFilepath") +str1);
            System.out.println(path1);
            try {
                FileWriter fw = new FileWriter(path1);
                fw.write(TransPEMContent);
                fw.close();
            } catch (IOException e) {
                System.out.println(e);
            }
            captureScreenShot2(app);
            //System.out.println("Copies URL: "+TransPEMURL);
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            map.put("Status", "Pass");
            map.put("TransPEMFile_URL", TransPEMURL);
        }
        catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", "'"+ e + "'");
        }
        return map;
    }

    /*public static Map<String, Object> OB_Download_Signing_PEMFile(GetApplication app,Map<String, String> data) throws InterruptedException, IOException {

        Map<String, Object> map = new HashMap<String, Object>();
        try {
            app.moduleName1 = "OB_Download_Signing_PEMFile";
            Thread.sleep(20000);
            WebElement SignPEM = app.driver1.findElement(POM_OBSoftStatements.DwnLoad_Signing_pem);
            String SignPEMTxt= SignPEM.getText();
            //System.out.println("Click on Signing PEM download: "+SignPEMTxt);
            SignPEM.click();
            //Open new tab in chrome
            String windowHandleBefore = app.driver1.getWindowHandle();
            for (String winHandle : app.driver1.getWindowHandles()) {
                app.driver1.switchTo().window(winHandle);
            }
            String SignPEMURL = app.driver1.getCurrentUrl();
            String str1 = SignPEMURL.replaceAll(".*qa/", "").replaceAll(".*//*","");
            System.out.println(str1);
            WebElement Spemcnt = app.driver1.findElement(By.xpath("/html/body/pre"));
            String SignPEMContent = Spemcnt.getText();
            System.out.println(SignPEMContent);
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            System.out.println(data.get("DownFilepath") +str1);
            File path2 = new File(data.get("DownFilepath") +str1);
            System.out.println(path2);
            try {
                FileWriter fw = new FileWriter(path2);
                fw.write(SignPEMContent);
                fw.close();
            } catch (IOException e) {
                System.out.println(e);
            }
            captureScreenShot2(app);
            System.out.println("Copies URL: "+SignPEMURL);
            app.driver1.close();
            app.driver1.switchTo().window(windowHandleBefore);
            map.put("Status", "Pass");
            map.put("SignPEMFile_URL", SignPEMURL);
        }
        catch (Exception e) {
            map.put("Status", "Technical_Issue");
            map.put("Error", e);
        }
        return map;
    }*/

}


