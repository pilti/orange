@TPPHubReusable
Feature: UI Baseline Functional Test Flow for TPP Hub

  Background:
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def OBFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.OBDirectory')
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def ob = read('classpath:Resources/OB_DIC/OB.json')
    * def ss = read('classpath:Resources/OB_DIC/Soft_Stmt_template.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def k = Java.type('CMA_Release.Java_Lib.createFolder')
  #  * def info = call test_info karate.info

  Scenario: One UI test for launch TPP Portal
    * def app = new webapp
  #  * def info = call test_info karate.info
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    Given def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Two UI test for PTC is successfully able to Log in to Open Banking
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Three UI test to Verify PTC Successfully Log in to TPPHub
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    Given def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    When match perform.Status == "Pass"
    Then def perform = app.stop()


  Scenario: Four UI test for PTC Logout from TPP Hub
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    Given def perform = TPPFunctions.PTC_Logout_TPP_Hub(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()


  Scenario Outline: Five UI test for Selection of Organisation on TPP Portal
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
    |GiveOrganisationName|
    |'Bank of Ireland TPP - AISP and PISP'|


  Scenario Outline: Six UI test to Add Application for Selected Organisation on TPP Portal
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    Given def perform = TPPFunctions.Add_Application_For_Selected_Organisation(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName|FilePath|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA.txt'|


  Scenario Outline: UI test to findout given Application from list of Applications under selected Organisation
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    Given def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client14'|

  Scenario Outline: UI test to get ClientSecrete for given Application under selected Organisation
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    * def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Given def perform = TPPFunctions.Get_ClientSecrete_For_Given_Application(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName |
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client13'|

  Scenario Outline: UI test to Successfully Reset ClientSecrete for given Application under selected Organisation
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    * def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Given def perform = TPPFunctions.Sucessfully_Reset_ClientSecrete_For_Given_Application(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client13'|

  Scenario Outline: UI test to Cancel Reset ClientSecrete procedure for given Application under selected Organisation
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    * def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Given def perform = TPPFunctions.Cancel_Reset_ClientSecrete_For_Given_Application(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client13'|


  Scenario Outline: UI test to Remove Given Application under selected Organisation on TPP Portal
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    * def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Given def perform = TPPFunctions.Remove_Selected_Application_From_Portal(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client4'  |

  Scenario Outline: UI test to Cancel Remove Application procedure for Given Application under selected Organisation on TPP Portal
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """
  #  * def setname = call  app.t_name1 = info.t_name + "test"
  #  * def setpath = call  app.path1 = info.path + "test"
    * def c = call  app.driver1 = app.start1(default_browser)
    * def perform = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * def perform = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    * def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    * def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Given def perform = TPPFunctions.Cancel_Remove_Application_Process_From_Given_ApplicationList(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client13' |