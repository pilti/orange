package CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities;

import CMA_Release.Entities_UI.Browsers.BrowserFactory;
import CMA_Release.Entities_UI.General.GetApplication;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal;
import CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm;
import CMA_Release.Entities_UI.Selenium.ReusableFunctions;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static CMA_Release.Entities_UI.General.capturescreen.TakeScreenShot;
import static CMA_Release.Entities_UI.General.capturescreen.captureScreenShot2;
import static CMA_Release.Entities_UI.Selenium.ReusableFunctions.*;

/**
 * Created by C963294 on 02/04/2018.
 * This Class is for Consent AISP functionality
 */
public class OIDC_AISP {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OIDC_AISP.class);
    //This method is to launch url
    public static Map launchConsentURL(GetApplication app, String UniResourceloc) {

        app.moduleName1 = "launchConsentURL";
        int var = 0;
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            LOGGER.info("URL sent to driver");
            app.driver1.get(UniResourceloc);
            LOGGER.info("Browser Opened URL");
            String str = app.driver1.getTitle();
            LOGGER.info("Page Title : " + str);
            //captureScreenShot2(app);

            var = str.compareTo("Login - Bank of Ireland");
            if (var == 0) {
                map.put("Landing_Page", str);
                map.put("Status", "Pass");
                map.put("scaCorelationId", app.driver1.findElement(Login_obj.correlation).getAttribute("value"));
                LOGGER.info("SCA page Launch Successful");
                LOGGER.info("scaCorelationId" + app.driver1.findElement(Login_obj.correlation).getAttribute("value"));
            } else {
                map.put("reasonText", "SCA page Launch unsuccessful!");
                map.put("Status", "Fail");
                LOGGER.info("SCA page Launch unsuccessful");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Status", "Technical Error - " + e.getMessage());
            LOGGER.debug("error" + e.getMessage());
            //app.stop();
        }
        return map;
    }

    //This method is to click on "I have a keycode app" buttons
    public static Map useKeyCodeApp(GetApplication app) throws InterruptedException {
        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "useKeyCodeApp";

        try {
            WaitForElement(app.driver1, Login_obj.have_key_app_bttn);
            focusOnElement(app, Login_obj.have_key_app_bttn);
            app.driver1.findElement(Login_obj.have_key_app_bttn).click();
            app.driver1.manage().window().maximize();
            String PageTitle = app.driver1.getTitle();
            TakeScreenShot(app);
            int var = PageTitle.compareTo("Login - Bank of Ireland");
            if (var == 0) {
                map.put("Landing_Page", PageTitle);
                map.put("Status", "Pass");
                map.put("CorelationId", app.driver1.findElement(Login_obj.correlation).getAttribute("value"));
            } else {
                map.put("reasonText", "Landing Page is not as expected!");
                map.put("Status", "Fail");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Status", "Techncal Error");
            app.stop();
        }
        return map;
    }

    //This method is to perform Login
    public static Map scaLogin(GetApplication app, Map data) throws InterruptedException {
        Map<String, Object> map = new HashMap<String, Object>();
        app.moduleName1 = "scaLogin";
        try {
            app.moduleName1 = "User_Name";
            WaitForElement(app.driver1, Login_obj.username_text);
            focusOnElement(app, Login_obj.username_text);
            app.driver1.findElement(Login_obj.username_text).sendKeys(data.get("usr").toString());
            app.driver1.manage().window().maximize();

            app.moduleName1 = "Password";
            WaitForElement(app.driver1, Login_obj.otp_text);
            app.driver1.findElement(Login_obj.otp_text).sendKeys(data.get("otp").toString());
            app.driver1.manage().window().maximize();

            captureScreenShot2(app);

            WaitForElement(app.driver1, Login_obj.continue_button);

            app.moduleName1 = "After SCALogin";
            app.driver1.findElement(Login_obj.continue_button).click();
            WaitForElement(app.driver1, Account_selection.selectAllButton);
            captureScreenShot2(app);
            app.driver1.manage().window().maximize();
            String PageTitle = app.driver1.getTitle();
            int var = PageTitle.compareTo("Consent - Bank of Ireland");
            if (var == 0) {
                map.put("Landing_Page", PageTitle);
                map.put("Status", "Pass");
            } else {
                map.put("reasonText", "Landing Page is not as expected!");
                map.put("Status", "Fail");
                map.put("Landing_Page", PageTitle);
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Status", "Technical Error");
            app.stop();
        }

        return map;
    }

    //This method is to select all accounts on consent Select account page
    public static Map selectAllAccounts(GetApplication app, Map data) throws InterruptedException, IOException {
        int cnt = 0;
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> paginationOp = new HashMap<String, Object>();
        paginationOp = Pagination(app);

        app.moduleName1 = "selectAllAccounts";
        List<String> SelectedAccounts = new ArrayList<>();
        String PageTitle = "";
        try {
            app.moduleName1 = "Sel_All_Account";
            WaitForElement(app.driver1, Account_selection.selectAllButton);
            WebElement selectAllButton = app.driver1.findElement(Account_selection.selectAllButton);

            app.driver1.manage().window().maximize();
            captureScreenShot2(app);

            if (paginationOp.get("Next").toString().equals("false")) {
                selectAllButton.click();
                captureScreenShot2(app);
            } else {
                selectAllButton.click();
                while (paginationOp.get("nextEnabled").toString().equals("Continue")) {
                    cnt = cnt + 1;
                    app.driver1.findElement((Account_selection.paginationNext)).click();
                    WaitForElement(app.driver1, Account_selection.selectAllButton);
                    selectAllButton.click();
                    paginationOp = Pagination(app);
                    LOGGER.info(paginationOp.toString());
                }

                for (int i = 0; i < cnt; i++) {
                    app.driver1.findElement((Account_selection.paginationPrevious)).click();
                }
            }

            //code to get all accounts from 'Account selection' screen
            map.put("AccountSelectionPage",getConsentData_AccSelection(app,Account_selection.AccountDataTable));

            //Validating selected accounts with number of accounts selected from select account selection
            SelectedAccounts = AccountSelectionValidateAccounts(app);
            map.put("SelectedAccounts", SelectedAccounts.toString());
            map.put("CurrencyAccountPair", AccountSelectionReturnCurrency(app));

            //Cancel the Consent request (further select if user wants to really cancel....if not then click on continue

            if (data.get("action").toString().toLowerCase().equals("continue")) {
                WaitForElement(app.driver1, Account_selection.continue_button);
                app.driver1.findElement(Account_selection.continue_button).click();
                PageTitle = app.driver1.getTitle();
                captureScreenShot2(app);
                map.put("PageTitle", PageTitle);
                map.put("Status", "Pass");

            } else if (data.get("action").toString().toLowerCase().equals("cancel")) {
                WaitForElement(app.driver1, Account_selection.cancel_button);
                app.driver1.findElement(Account_selection.cancel_button).click();
                if (data.get("Confirmation").toString().toLowerCase().equals("yes")) {
                    WaitForElement(app.driver1, Account_selection.cancel_request);
                    app.driver1.findElement(Account_selection.cancel_request).click();

                    map.put("URL", app.driver1.getCurrentUrl());
                    map.put("Status", "Pass");
                } else {
                    WaitForElement(app.driver1, Account_selection.NO);
                    app.driver1.findElement(Account_selection.NO).click();
                    WaitForElement(app.driver1, Account_selection.continue_button);
                    app.driver1.findElement(Account_selection.continue_button).click();

                    PageTitle = app.driver1.getTitle();
                    captureScreenShot2(app);
                    map.put("PageTitle", PageTitle);
                    map.put("Status", "Pass");

                }
            } else {
                map.put("Status", "Fail");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("Status", "Technical Error " + e.getMessage());
            app.stop();

                 }
        //Code to to Click on 'Show All' button on Review and confirm' page
        ReusableFunctions.ClickElement(app,Account_selection.buttonShowAll);
        //code to get all accounts from 'review and confirm' screen
        map.put("AccountReviewPage",getConsentData_AccReview(app,Account_selection.ReviewDataTable));
        return map;
    }


    /*This method expects an array input named SelectedAccounts which holds an array od selected accounts from Account Selection Page
    Also action is the key that is expected which holds the action to be performed like Cancel/Continue,back*/

    public static Map reviewConfirm(GetApplication app, Map data) throws InterruptedException, IOException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            app.moduleName1 = "reviewConfirm";
            Map<String, Object> actionperformed = new HashMap<String, Object>();
            List<String> Accounts = new ArrayList<>();
            Map<String, Object> permission = new HashMap<String, Object>();
            String input = data.get("SelectedAccounts").toString().replace("[", "").replace("]", "").replace(" ", "");


            //Accounts validation
            Accounts = ReviewConfirmValidateAccounts(app, input);

            map.put("SelectedAccounts", Accounts);

            //Permission validation

            permission = Permission(app);
            map.put("Permissions", permission);
            captureScreenShot2(app);

            //Consnet Expiry Confirmation
            List<WebElement> expdate = app.driver1.findElements(Review_confirm.consent_validity);
            if (expdate.size() > 0)
                map.put("ConsentExpiry", expdate.get(0).getText());

            //Continue or cancel or back
            String action = data.get("action").toString();
            if (action.toLowerCase().equals("continue")) {
                WaitForElement(app.driver1, Review_confirm.Check_box_confirm1);
                app.driver1.findElement(Review_confirm.Check_box_confirm1).click();
                app.driver1.manage().window().maximize();
                captureScreenShot2(app);

                WaitForElement(app.driver1, Review_confirm.Confirm_button);
                String continueButton = app.driver1.findElement(Review_confirm.Confirm_button).getText();
                actionperformed.put("ButtonText", continueButton);
                app.driver1.findElement(Review_confirm.Confirm_button).click();

                captureScreenShot2(app);
                Thread.sleep(5000); // Should not use that
                String Curl = app.driver1.getCurrentUrl();
                LOGGER.info("Curl : " + Curl);
                String str1 = Curl.replaceAll(".*&id_token=", "").replaceAll("&state.*", "");
                String str2 = Curl.replaceAll("&id.*", "").replaceAll(".*#code=", "");
                String []str3= Curl.split("#code");
                actionperformed.put("Action Performed", "Click on Confirm Button");
                actionperformed.put("idtoken", str1);
                actionperformed.put("Authcode", str2);
                actionperformed.put("redirect_uri",str3[0]);
                System.out.println("redirect"+str3[0]);
                System.out.println("CURL)))) "+ Curl);

                map.put("Action", actionperformed);

            } else if (action.toLowerCase().equals("Back")) {
                actionperformed.put("Action Performed", "Click on Back Button");
                actionperformed.put("ButtonText", app.driver1.findElement(Review_confirm.Back_button).getText());
                WaitForElement(app.driver1, Review_confirm.Back_button);
                app.driver1.findElement(Review_confirm.Back_button).click();
                WaitForElement(app.driver1, Account_selection.AccountSelectionPage);
                captureScreenShot2(app);

                actionperformed.put("PageName", app.driver1.findElement(Account_selection.AccountSelectionPage).getText());
                map.put("Action", actionperformed);
            } else {
                app.driver1.findElement(Review_confirm.Cancel_button).click();
                captureScreenShot2(app);
                if (data.get("Confirmation").toString().toLowerCase().equals("yes")) {
                    actionperformed.put("Action Performed", "Click on Cancel Button and Click Yes Cancel");
                    actionperformed.put("ButtonText", app.driver1.findElement(Review_confirm.Yes_Cancel).getText());
                    app.driver1.findElement(Review_confirm.Yes_Cancel).click();

                    actionperformed.put("CancelConsentURL", app.driver1.getCurrentUrl());
                    map.put("Action", actionperformed);
                } else {
                    actionperformed.put("Action Performed", "Click on Cancel Button and Click No");
                    actionperformed.put("ButtonText", app.driver1.findElement(Review_confirm.No_Cancel).getText());
                    app.driver1.findElement(Review_confirm.No_Cancel).click();
                    captureScreenShot2(app);

                    actionperformed.put("Landing_Page", app.driver1.findElement(Review_confirm.PageName).getText());
                    map.put("Action", actionperformed);
                }
            }
            return map;
        } catch (Exception e) {
            LOGGER.info("Exception in reviewConfirm: " + e.getMessage());
            app.stop();
            return null;
        }

    }

    ///////////////////////////////////Utility Methods///////////////////////////////////////////////////////


    //This Method verifies if selected accounts in Select accounts and accounts in selected accounts section are same
    public static List AccountSelectionValidateAccounts(GetApplication app) throws InterruptedException, IOException {
        try {
            int row_cnt;
            WaitForElement(app.driver1, Account_selection.sel_accnt_tb_data_cnt);
            app.moduleName1 = "Sel_Account_Validate";
            List<String> accountNumber = new ArrayList<String>();
            List<WebElement> cnt_accnt = app.driver1.findElements(Account_selection.checkBoxCnt);
            List<WebElement> selectedAccnt = app.driver1.findElements(Account_selection.sel_accnt_tb_data_cnt);
            Map<String, Object> paginationOp = new HashMap<String, Object>();
            paginationOp = Pagination(app);

            if (paginationOp.get("nextEnabled").toString().equals("Continue")) {
                while (paginationOp.get("nextEnabled").toString().equals("Continue")) {
                    cnt_accnt = app.driver1.findElements(Account_selection.checkBoxCnt);
                    row_cnt = 1;
                    for (int i = 0; i < cnt_accnt.size(); i++) {

                        WebElement row_chk = app.driver1.findElement(By.xpath("//div[3]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[1]/div[1]/input"));
                        boolean status = row_chk.isEnabled();
                        String attr = row_chk.getAttribute("class");


                        if (attr.equals("ng-pristine ng-untouched ng-valid ng-not-empty")) {
                            WebElement acc_num = app.driver1.findElement(By.xpath("//div[3]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[3]"));
                            accountNumber.add(acc_num.getText());
                        }
                        row_cnt = row_cnt + 1;

                    }
                    app.driver1.findElement((Account_selection.paginationNext)).click();
                    paginationOp = Pagination(app);
                    Thread.sleep(300);
                }
            }
            WaitForElement(app.driver1, Account_selection.checkBoxCnt);
            row_cnt = 1;
            cnt_accnt = app.driver1.findElements(Account_selection.checkBoxCnt);
            for (int i = 0; i < cnt_accnt.size(); i++) {

                WebElement row_chk = app.driver1.findElement(By.xpath("//div[3]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[1]/div[1]/input"));

                boolean status = row_chk.isEnabled();
                String attr = row_chk.getAttribute("class");


                if (attr.equals("ng-pristine ng-untouched ng-valid ng-not-empty")) {
                    WebElement acc_num = app.driver1.findElement(By.xpath("//div[3]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[3]"));
                    accountNumber.add(acc_num.getText());
                }
                row_cnt = row_cnt + 1;

            }


            if (accountNumber.size() == selectedAccnt.size()) {
                for (int i = 0; i < selectedAccnt.size(); i++) {
                    Assert.assertTrue(accountNumber.get(i).equals(selectedAccnt.get(i).getText()));

                }
            } else {
                LOGGER.info("There is a mismatch in the number of accounts selected and number of accounts displayed!");
                Assert.assertFalse(true);
            }
            return accountNumber;
        } catch (Exception e) {
            LOGGER.info("Exception in AccountSelectionValidateAccounts " + e.getMessage());
            app.stop();
        }
        return null;
    }

    // This method is to store Currency from Account Selection Page
    public static Map AccountSelectionReturnCurrency(GetApplication app) throws InterruptedException, IOException {
        try {
            int row_cnt;
            WaitForElement(app.driver1, Account_selection.AccountSelectionPage_selected);
            app.moduleName1 = "Sel_Account_Validate_currency";
            List<WebElement> cnt_accnt1 = app.driver1.findElements(Account_selection.AccountSelectionPage_selected);
            Map<String, Object> Currency_map = new HashMap<String, Object>();
            Currency_map = null;
            Map<String, Object> temp = new HashMap<String, Object>();
            List<String> temp1 = new ArrayList<String>();
            row_cnt = 1;
            for (int i = 0; i < cnt_accnt1.size(); i++) {
                WebElement acc_num = app.driver1.findElement(By.xpath("//*[@id=\"selected-table\"]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[2]"));
                WebElement curr = app.driver1.findElement(By.xpath("//*[@id=\"selected-table\"]/div/div[2]/div/div[1]/table/tbody/tr[" + row_cnt + "]/td[3]"));
                //Currency_acc_pair.clear();
                Map<String, Object> Currency_acc_pair = new HashMap<String, Object>();
                Currency_acc_pair.put("Account", acc_num.getText());
                Currency_acc_pair.put("Currency", curr.getText());
                Currency_map.put(acc_num.getText().replace("~", "") + "_ACC_CURR_PAIR", Currency_acc_pair);
                row_cnt = row_cnt + 1;
            }
            return Currency_map;
        } catch (Exception e) {
            LOGGER.info("Exception in AccountSelectionValidateAccounts " + e.getMessage());
        }
        return null;
    }

    //This method validates if accounts from Account selection Page and Accounts on Review confirm Page are same
    public static List ReviewConfirmValidateAccounts(GetApplication app, String input) throws InterruptedException, UnsupportedEncodingException {
        try {
            List<String> SelectedAccounts = new ArrayList<>();
            input = input.replace("[", "").replace("]", "").replace(" ", "");
            String[] arr = input.trim().split(",");
            int size = arr.length;


            for (int i = 0; i < size; i++) {
                SelectedAccounts.add(arr[i]);
            }

            app.moduleName1 = "Click Show All";
            TakeScreenShot(app);
            if (app.driver1.findElement(Review_confirm.show_all).isDisplayed()) {
                app.driver1.findElement(Review_confirm.show_all).click();
            }
            List<WebElement> sel_accnt = app.driver1.findElements(Review_confirm.Selected_Account);
            List<String> Accounts_rp = new ArrayList<>();
            List<String> Name_rp = new ArrayList<>();

            int cnt = 1;
            for (int i = 0; i < sel_accnt.size(); i++) {
                Accounts_rp.add(app.driver1.findElement(By.xpath("//div[3]//div[1]/table/tbody/tr[" + cnt + "]/td[2]")).getText());
                Name_rp.add(app.driver1.findElement(By.xpath("//div[3]//div[1]/table/tbody/tr[" + cnt + "]/td[3]")).getText());
                cnt = cnt + 1;
            }

            if (sel_accnt.size() == SelectedAccounts.size()) {
                for (int i = 0; i < sel_accnt.size(); i++) {
                    if (Accounts_rp.get(i).equals(SelectedAccounts.get(i))) {
                        Assert.assertTrue(true);
                    } else {
                        Assert.assertTrue(false);
                        LOGGER.info("Assert Failed since account" + SelectedAccounts.get(i) + "on Account Selection Page do not match with accounts on review confirm page!");
                    }

                }
            }

            return Accounts_rp;
        } catch (Exception e) {
            LOGGER.info("Exception in AccountSelectionValidateAccounts " + e.getMessage());
            app.stop();
        }
        return null;
    }

    //This method is to create a json holding permissions on Review Confirm Page
    public static Map Permission(GetApplication app) throws InterruptedException {
        try {
            List<WebElement> perm_headdings = app.driver1.findElements(Review_confirm.Permission_headding);
            List<WebElement> perm_description = app.driver1.findElements(Review_confirm.Permission_headding_desc);
            List<String> Label = new ArrayList<>();
            List<String> Description = new ArrayList<>();
            List<String> Permission = new ArrayList<>();
            Map<String, Object> permission_on_ui = new HashMap<String, Object>();
            Map<String, Object> analyse_permission = new HashMap<String, Object>();
            List<WebElement> arrow = app.driver1.findElements(Review_confirm.Permission_headding_dropdown);

            for (int i = 0; i < perm_headdings.size(); i++) {

                Label.add(perm_headdings.get(i).getText().toString());

                switch (perm_headdings.get(i).getText().toString().toLowerCase()) {
                    case "basic account information":
                        Permission.add("ReadAccountsBasic");
                        break;
                    case "detailed account information":
                        Permission.add("ReadAccountsDetail");
                        break;
                    case "balance information":
                        Permission.add("ReadBalances");
                        break;
                    case "basic payee information":
                        Permission.add("ReadBeneficiariesBasic");
                        break;
                    case "detailed payee information":
                        Permission.add("ReadBeneficiariesDetail");
                        break;
                    case "direct debit information":
                        Permission.add("ReadDirectDebits");
                        break;
                    case "basic standing order information":
                        Permission.add("ReadStandingOrdersBasic");
                        break;
                    case "detailed standing order information":
                        Permission.add("ReadStandingOrdersDetail");
                        break;
                    case "basic transaction information":
                        Permission.add("ReadTransactionsBasic");
                        break;
                    case "detailed transaction information":
                        Permission.add("ReadTransactionsDetail");
                        break;
                    case "product information":
                        Permission.add("ReadProducts");
                        break;
                }
                arrow.get(i).click();

                Description.add(perm_description.get(i).getText());
                Map<String, Object> perm = new HashMap<String, Object>();
                perm.put("Label", perm_headdings.get(i).getText().toString());
                perm.put("Description", perm_description.get(i).getText().toString());
                permission_on_ui.put(Label.get(i).toString().replace(" ","_"), perm);
            }
            analyse_permission.put("RetrivedPemissionArray", Permission);
            analyse_permission.put("PermissionsInDetail", permission_on_ui);
            return analyse_permission;
        } catch (Exception e) {
            LOGGER.info("Exception in AccountSelectionValidateAccounts " + e.getMessage());
            app.stop();
        }
        return null;
    }

    //This is supportive method used in AccountSelection method to achieve Pagination
    public static Map Pagination(GetApplication app) throws InterruptedException {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            boolean next = app.driver1.findElement((Account_selection.paginationNext)).isDisplayed();
            String nextEnabled = app.driver1.findElement((Account_selection.paginationNext)).getAttribute("disabled");

            if (StringUtils.isBlank(nextEnabled)) {
                nextEnabled = "Continue";
            }
            boolean previous = app.driver1.findElement((Account_selection.paginationPrevious)).isDisplayed();
            String previousEnabled = app.driver1.findElement((Account_selection.paginationPrevious)).getAttribute("disabled");
            if (StringUtils.isBlank(previousEnabled)) {
                previousEnabled = "Disabled";

            }
            map.put("Next", next);
            map.put("Previous", previous);
            map.put("nextEnabled", nextEnabled);
            map.put("previousEnabled", previousEnabled);
        } catch (Exception e) {
            LOGGER.info("Exception in Pagination method:" + e.getMessage());
            // app.driver1.quit();
            BrowserFactory.closeAllDriver();
        }
        return map;
    }

    //This is a general method for validating webelement on any page
    public static Map validateWebElementText(GetApplication app) throws InterruptedException {
        WaitForElement(app.driver1, Login_obj.error_msg_signing);
        Map<String, Object> map = new HashMap<String, Object>();
        String str = null;
        try {
            str = getElementText(app, Login_obj.error_msg_signing);
            map.put("ActualText", str);
        } catch (Exception e) {
            e.getMessage();
            map.put("status", "technical error");
            app.stop();
        }
        return map;
    }

    //This method isto get current url of a wepage
    public static Map getUrl(GetApplication app) {
        Map<String, Object> map = new HashMap<String, Object>();
        String str = null;
        try {
            Thread.sleep(3000);
            str = getCurrentURL(app);
            map.put("ActualUrl", str.trim());
        } catch (Exception e) {
            e.getMessage();
            map.put("status", "technical error");
            app.stop();
        }
        return map;
    }

    //This method validates the text message on Webelement
    public static Map validateWebElementErrorText(GetApplication app) throws InterruptedException {
        WaitForElement(app.driver1, Login_obj.Error_text1);
        Map<String, Object> map = new HashMap<String, Object>();
        String str1 = null;
        String str2 = null;
        try {
            str1 = getElementText(app, Login_obj.Error_text1);
            map.put("ActualText1", str1);

            str2 = getElementText(app, Login_obj.Error_text2);
            map.put("ActualText2", str2);

        } catch (Exception e) {
            e.getMessage();
            map.put("status", "technical error");
            app.stop();
        }
        return map;
    }

    //This method is for timeout notification
    public static Map timeOut(GetApplication app) throws InterruptedException {
        Map<String, Object> map = new HashMap<String, Object>();
        WaitForElement(app.driver1, Login_obj.returnToThirdParty);
        try {
            if (app.driver1.findElement(Login_obj.returnToThirdParty).isDisplayed()) {
                map.put("Popup_Text", app.driver1.findElement(Login_obj.timeoutpopup).getText());
                map.put("Button_Text", app.driver1.findElement(Login_obj.returnToThirdParty).getText());
                WaitForElement(app.driver1, Login_obj.returnToThirdParty);
                captureScreenShot2(app);
                focusOnElement(app, Login_obj.returnToThirdParty);
                app.driver1.findElement(Login_obj.returnToThirdParty).click();
            }

        } catch (Exception e) {
            e.getMessage();
            map.put("status", "technical error");
            app.stop();
        }
        return map;
    }

    //This method is a supportive method to compare two ArrayList
    public static boolean CompareLists(List accountNumber, List selectedAccnt) {
        try {
            if (accountNumber.size() == selectedAccnt.size()) {
                for (int i = 0; i < selectedAccnt.size(); i++) {

                    Assert.assertTrue(accountNumber.get(i).toString().equals(selectedAccnt.get(i).toString()));
                    return true;
                }
            } else {
                LOGGER.info("There is a mismatch in the size of two lists!");
                Assert.assertFalse(true);
                return false;
            }

        } catch (Exception e) {
            System.out.print("Exception in CompareLists " + e.getMessage());
            BrowserFactory.closeAllDriver();
        }
        return false;
    }

    //This method is to fetch Accounts on Account Selection Page
    public static void GetAccountsOnConsentRenewalPage(GetApplication app, Map data) {
        List<String> RenewalAccountsNames = new ArrayList<>();
        List<String> SelectedAccounts = new ArrayList<>();
        List<WebElement> RenewalAccounts = app.driver1.findElements(RefreshTokenRenewal.selectedAccountNames);
        try {
            for (WebElement ele : RenewalAccounts) {
                RenewalAccountsNames.add(ele.getText());
            }
            String input = data.get("SelectedAccounts").toString().replace("[", "").replace("]", "").replace(" ", "");
            String[] arr = input.trim().split(",");
            for (int i = 0; i < arr.length; i++) {
                //SelectedAccounts.add(arr[i].replace("\"", ""));
                String ele = arr[i];
                SelectedAccounts.add(ele);
            }
            //Accounts validation
            CompareLists(RenewalAccountsNames, SelectedAccounts);
        } catch (Exception e) {
            LOGGER.info("Exception in GetAccountsOnConsentRenewalPage " + e.getMessage());
            BrowserFactory.closeAllDriver();
        }
    }

    //This method is to verify accounts on ReAuthorization page
    public static boolean VerifyListsOnReAuthorisarionPage(GetApplication webApp, By obj, String data) {
        List<String> List1 = new ArrayList<>();
        List<String> List2 = new ArrayList<>();
        List<WebElement> ListUI = webApp.driver1.findElements(obj);
        try {
            for (WebElement ele : ListUI) {
                List1.add(ele.getText());
            }

            String input = data.toString().replace("[", "").replace("]", "").replace(" ", "").replace("\"", "");
            String[] arr = input.trim().split(",");
            for (int i = 0; i < arr.length; i++) {
                List2.add(arr[i]);
            }
            //Accounts validation

            boolean flag = CompareLists(List1, List2);
            return flag;
        } catch (Exception e) {
            LOGGER.info("Exception in VerifyListsOnReAuthorisarionPage " + e.getMessage());
            BrowserFactory.closeAllDriver();
            return false;
        }
    }

    //method overloading: arguments type is different
    public static boolean VerifyListsOnReAuthorisarionPage(GetApplication webApp, String data1, String data2) {
        List<String> List1 = new ArrayList<>();
        List<String> List2 = new ArrayList<>();
        String input1 = data1.toString().replace("[", "").replace("]", "").replace(" ", "");
        String[] arr1 = input1.trim().split(",");
        for (int i = 0; i < arr1.length; i++) {
            List1.add(arr1[i]);
        }

        String input2 = data2.toString().replace("[", "").replace("]", "").replace(" ", "");
        String[] arr2 = input2.trim().split(",");
        for (int i = 0; i < arr2.length; i++) {
            List2.add(arr2[i]);
        }
        //Accounts validation

        boolean flag = CompareLists(List1, List2);
        return flag;
    }

    //This method is to set an Expirt date time
    public static String getConsentExpiryTime(String min) {
        try {

            String currtime = java.time.LocalDateTime.now().minusHours(1).plusMinutes(Integer.valueOf(min)).toString().substring(0, 19);

            return (currtime + "+00:00");
        } catch (Exception e) {
            LOGGER.info("error in getConsentexpiryTime" + e.getMessage());
            BrowserFactory.closeAllDriver();
            return null;
        }
    }

    //This method is to fetch Authcode from call back URL
    public static Map<String, Object> getAuthCodeFromURL(GetApplication app) throws InterruptedException {
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            Thread.sleep(3000);
            String Curl = app.driver1.getCurrentUrl();
            String str1 = Curl.replaceAll(".*&id_token=", "").replaceAll("&state.*", "");
            String str2 = Curl.replaceAll("&id.*", "").replaceAll(".*#code=", "");
            map.put("Action Performed", "Click on Confirm Button");
            map.put("idtoken", str1);
            map.put("Authcode", str2);
            return map;
        } catch (Exception e) {
            BrowserFactory.closeAllDriver();
            return null;
        }
    }

    //This method is to split and replace consent url used in Renewal of refresh token
    public static String split_replace_Consenturl(String s1, String word) {
        try {
            String[] List1 = null;
            List1 = s1.split(word);
            return "https:" + List1[1].replace(':', '=') + "https:" + List1[2].replace(':', '=');
        } catch (Exception e) {
            BrowserFactory.closeAllDriver();
            return null;
        }

    }

    //Tiis method is small code to remove last character (") from the Consent URL - PISP
    public static String getCorrectURL(String ulrIncorrect){
        String urlNew = null;
        urlNew = ulrIncorrect.substring(0,ulrIncorrect.length()-1);
        return urlNew;
    }


    //This method is to to convert Account review Table to Json
    public static Map getConsentData_AccReview(GetApplication app, By obj1) {

        List<WebElement> arrOfData = app.driver1.findElements(obj1);
        LinkedHashMap[] accountArray = new LinkedHashMap[arrOfData.size()];
        LinkedHashMap mapData1 = new LinkedHashMap();

        for(int i=1;i<=arrOfData.size();i++){
            LinkedHashMap<String, String> mapData = new LinkedHashMap<String, String>();
            mapData.put("Acc_number",app.driver1.findElement(By.xpath("//div[contains(@class,'account-review-table')]/table/tbody/tr[" + i +"]/td[2]")).getText());
            mapData.put("Nickname",app.driver1.findElement(By.xpath("//div[contains(@class,'account-review-table')]/table/tbody/tr[" + i +"]/td[1]")).getText());
            mapData.put("Currency",app.driver1.findElement(By.xpath("//div[contains(@class,'account-review-table')]/table/tbody/tr[" + i +"]/td[3]")).getText());
            mapData.put("Account type",app.driver1.findElement(By.xpath("//div[contains(@class,'account-review-table')]/table/tbody/tr[" + i +"]/td[4]")).getText());
            accountArray[i-1] = mapData;
        }

        mapData1.put("Account_Review",Arrays.asList(accountArray));
        return mapData1;
    }

    //This method is to to convert Account Selection Table to Json
    public static Map getConsentData_AccSelection(GetApplication app, By obj1) {

        List<WebElement> arrOfData = app.driver1.findElements(obj1);
        LinkedHashMap[] accountArray = new LinkedHashMap[arrOfData.size()];
        LinkedHashMap mapData1 = new LinkedHashMap();

        for(int i=1;i<=arrOfData.size();i++){
            LinkedHashMap<String, String> mapData = new LinkedHashMap<String, String>();
            mapData.put("Acc_number",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[3]")).getText());
            mapData.put("Nickname",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[2]")).getText());
            mapData.put("Currency",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[4]")).getText());
            mapData.put("Account type",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[5]")).getText());
            boolean selectValue = app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[1]")).isSelected();
            String type1 = app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[1]/div")).getAttribute("Class");
            String valueSelect = null;
            if(type1.equalsIgnoreCase("checkbox")){
            valueSelect = "Selectable";

            }else{
                valueSelect = "Non Selectable";
            }
            mapData.put("Select",valueSelect);
            accountArray[i-1] = mapData;
        }

        mapData1.put("Account_Selection",Arrays.asList(accountArray));
        return mapData1;
    }


    //This method is to to convert Account Selection Table to Json for Selected accounts table
    public static Map getConsentData_SelectedAccounts(GetApplication app, By obj1) {

        List<WebElement> arrOfData = app.driver1.findElements(obj1);
        LinkedHashMap[] accountArray = new LinkedHashMap[arrOfData.size()];
        LinkedHashMap mapData1 = new LinkedHashMap();

        for(int i=1;i<=arrOfData.size();i++){
            LinkedHashMap<String, String> mapData = new LinkedHashMap<String, String>();
            mapData.put("Acc_number",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[2]")).getText());
            mapData.put("Nickname",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[1]")).getText());
            mapData.put("Currency",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[3]")).getText());
            mapData.put("Account type",app.driver1.findElement(By.xpath("//div[contains(@class,'table-view')]/table/tbody/tr[" + i +"]/td[4]")).getText());
            accountArray[i-1] = mapData;
        }

        mapData1.put("Account_Selection",Arrays.asList(accountArray));
        return mapData1;
    }


}

