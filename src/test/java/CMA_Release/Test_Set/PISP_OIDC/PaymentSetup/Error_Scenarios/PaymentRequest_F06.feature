@PaymentSetup
@PISP_CR28_Error
@PISP_CR28_Error2

Feature: Error conditions2 of CR28 FS Codes

  Background:
    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * def Application_Details = active_tpp.AISP_PISP
    * json Result = {}
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
      """


  # Getting AcceptedTechnicalValidation- Need to confirm for expected for below scenarios
  @PISP_RC_CR02811
  Scenario Outline: Verify 201 status for INACTIVE Account #key# for PISP PaymentSetup Payload

    * def info = karate.info
    * set info.subset = 'InActive Account'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAccount.Identification = "<SortcodeAccountNo>"
    * print pisp_payload
    * def AccountNo = pisp_payload.Data.Initiation.CreditorAccount.Identification
    * def NSC = ( AccountNo.substring(0,6) )
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "ACCOUNT_STATUS='INACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    * string a = "ACCOUNT_STATUS='ACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Examples:
      |count| Condition            | SortcodeAccountNo |Response |    Status                    |
      |01   | "InActive Account"   | 30102750091131    | 201     | AcceptedTechnicalValidation |

  # Getting AcceptedTechnicalValidation- Need to confirm for expected for below scenarios
  @PISP_RC_CR02812
  Scenario Outline: Verify 201 status when Account Block and Status for #key# for PISP PaymentSetup Payload

    * def info = karate.info
    * set info.subset = 'Account Block and Status Check'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAccount.Identification = "<SortcodeAccountNo>"
    * print pisp_payload
    * def AccountNo = pisp_payload.Data.Initiation.CreditorAccount.Identification
    * def NSC = ( AccountNo.substring(0,6) )
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "<BlockCondition>='1'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
      And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
      And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
      And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

   * string a = "<BlockCondition>='0'"
   * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Examples:
      |count| Condition          | SortcodeAccountNo | BlockCondition   | Response |        Status              |
      |01   | "BlockedToAll"     | 30102750091131    | BLOCKED_TO_ALL   |   201    |AcceptedTechnicalValidation |
      |02   | "BlockedToAll"     | 30102750091131    | BLOCKED_TO_CREDIT|   201    |AcceptedTechnicalValidation |
      |03   | "BlockedToAll"     | 30102750091131    | BLOCKED_TO_DEBIT |   201    |AcceptedTechnicalValidation |
      |04   | "DormantAccount"   | 30102750091131    | DORMANT          |   201    |AcceptedTechnicalValidation |
      |05   | "LienAccount"      | 30102750091131    | LIEN             |   201    |AcceptedTechnicalValidation |
