@PaymentSetup
@PISP_CR07_Error
@PISP_Rup_CR\
@PISP
Feature: Error conditions of CR07 Delivery Address Field

  Background:
    * def apiApp = new apiapp()
    * def Application_Details = active_tpp.AISP_PISP
    * json Result = {}
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
      """

  @PISP_Rup_CR081
  Scenario Outline: Verify 400 status when Mandatory fields #key# is passed

    * def info = karate.info
    * set info.subset = 'Missing Mandatory fields'
    * set info.key = "<count>_" + <Field>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And remove pisp_payload.Risk.DeliveryAddress.<Mandatory>
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 400

    Examples:
    |count|Field              | Mandatory  |
    |01   |"TownName Removed" | TownName   |
    |02   |"Country Removed"  | Country    |


  @PISP_Rup_CR082
  Scenario Outline: Verify 400 status for Length Limit fields #key# Exceed of Delivery Address

    * def info = karate.info
    * set info.subset = 'Fields Length Validations'
    * set info.key = "<count>_" + <Field>

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Risk.DeliveryAddress =
    """
    {
      "AddressLine": [
                <Address Line1>,
                <Address Line2>
            ],
            "StreetName": <StreetName>,
            "BuildingNumber": <BuildingNumber>,
            "PostCode": <PostCode>,
            "TownName": <TownName>,
            "CountrySubDivision": [
                <CountrySDiv Line1>,
                <CountrySDiv Line2>
            ],
            "Country": <Country>
    }
    """
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 400

    Examples:
      |count| Field                                    | Address Line1                                                          | Address Line2                                                          | StreetName                                                              | BuildingNumber  | PostCode        | TownName                       | CountrySDiv Line1              | CountrySDiv Line2              | Country |
      |01   | "Add1 greater than 70 character"         | AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345AB | Acacia Lodge                                                           | AcaciaAvenue                                                            | "27"            | GU31 2ZZ        | Sparsholt                      | ABCDABCDABCDABCDAB             | DEFG                           | GB      |
      |02   | "Add2 is greater than 70 character"      | Flat 7                                                                 | AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345AB | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | Sparsholt                     | ABCDABCDABCDABCDAB             | DEFG                           | GB      |
      |03   | "Country SDiv1 is grtr than 35 character"| Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | Sparsholt                      | AAaaa12345BBbbb12345CCccc12345DDddd12 | '      DEFG '                  | GB      |
      |04   | "Country SDiv2 is grtr than 35 character"| Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | Sparsholt                      | ABCDA                          | AAaaa12345BBbbb12345CCccc12345DDddd12 | GB      |
      |05   | "StreetName is grter than 70 character"  | Flat 7                                                                 | 'Acacia Lodge'                                                         | 'AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345A'| "27"              | GU31 2ZZ        | Sparsholt                      | ABCDABCD                       | DEFG                      | GB      |
      |06   | "BuildingNumber is grter than 16 character"| Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "0123456789123456AB" | GU31 2ZZ        |Sparsholt                      | ABCDABCDABCDABCDAB             | DEFG                      | GB      |
      |07   | "PostCode is greter than 16 character"     | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | AAaaa12345BBbbb12 | Sparsholt                      | ABCDABCDABCDABCDAB             | DEFG                      | GB      |
      |08   | "TownName is gter than character 35"       | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | AAaaa12345BBbbb12345CCccc12345DDddd12 | ABCDAB                         | DEFG                           | GB      |
      |09   | "Country is greter than 2 character"       | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | AAaa                           | ABCDAB                         | DEFG                           | 'GBP'    |
      |10   | "Empty value in TownName"                  | Flat 7                                                                 | 'Acacia Lodge'| AcaciaAvenue | 27             | GU31 2ZZ |''| ABCDABCD         |AAcvcDefeccv       | GB      |
      |11   | "Empty value in Country"                   | Flat 7                                                                 | 'Acacia Lodge'| AcaciaAvenue | 27             | GU31 2ZZ | Sparsholt | ABCDABCD         |AAcvcDefeccv  |''|
      |12   | "Invalid value in Country"                 | Flat 7                                                                 | 'Acacia Lodge'| AcaciaAvenue | 27             | GU31 2ZZ | Sparsholt | ABCDABCD         |AAcvcDefeccv  |'YZ'|
      |13   | "Numbers in Country"                       | Flat 7                                                                 | 'Acacia Lodge'| AcaciaAvenue | 27             | GU31 2ZZ | Sparsholt | ABCDABCD         |AAcvcDefeccv  |'01'|
      |14   | "Add1 spaces exceeding 70 charts"      | 'AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345   '| Acacia Lodge                                                       | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | Sparsholt                      | ABCDABCDABCDABCDAB             | DEFG                           | GB      |
      |15   | "StreetName spaces exceeding 70 charts"| Flat 7                                                                 | 'Acacia Lodge'                                                         | 'AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345  '| "27"            | GU31 2ZZ        | Sparsholt                      | ABCDA                          | AAaaa12345BBbbb12345CCccc12345 | GB      |
      |16   | "BuildNo spaces exceeding 16 charts"  | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "0123456789123456  " | GU31 2ZZ        | Sparsholt                      | ABCDABCDABCDABCDAB             | DEFG    | GB      |
      |17   | "PostCode spaces exceeding 16 charts"| Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"             | '     AAaaa12345BBbbb' | Sparsholt                      | ABCDABCDABCDABCDAB             |DEFG                | GB      |
      |18   | "Country  spaces exceeding 2 charts"| Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                            | "27"              | GU31 2ZZ        | AAaa                           | ABCDAB                         | DEFG                           | 'GB   ' |

