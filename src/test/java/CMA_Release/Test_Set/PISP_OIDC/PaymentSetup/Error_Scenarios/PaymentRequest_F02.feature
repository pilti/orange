@PaymentSetup
@PISP
@PISP_API
@PISP_PaymentSetup
@payal15

Feature: Feature to getting error Payment ID using Payment Initiation API

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null


    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
      """


  @test123

  Scenario:  Validation of 400 bad request for header parameters

    * def info = karate.info
    * set info.subset = '400 bad request validation for Header'
    * json tpp_credentials = {"client_id": '#(Application_Details.client_id)',"client_secret": '#(Application_Details.client_secret)'}

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)

    And match apiApp.Access_Token_CCG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
     #modify the Header
    And set pisp_reqHeader.x-idempotency-key = null
    And remove pisp_reqHeader.x-idempotency-key
    And print pisp_reqHeader

    And set Application_Details.request_method = 'POST'

    Given url PmtSetupUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header Authorization = 'Bearer ' + Application_Details.access_token
    When method POST

    And set Result.ActualOutput.Payment_setup.Input = Application_Details
    And set Result.ActualOutput.Payment_setup.Input.Endpoint = PmtSetupUrl
    And set Result.ActualOutput.Payment_setup.Output.response =  response
    And set Result.ActualOutput.Payment_setup.Output.responseStatus = responseStatus
    And match responseStatus == 400



  Scenario: validation of 400 bad request for invalid  paylaod
    * def info = karate.info
    * set info.subset = 'invalid paymentcontext parameter'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)

    And match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
   #modify the payload
    And remove pisp_payload.Risk.MerchantCategoryCode

    #Call Account_Request_Setup API
    And set Application_Details.request_method = 'POST'
    When call apiApp.Payment_Setup(Application_Details)
    And match apiApp.Payment_Setup.responseStatus == 400


  Scenario: validation of 400 bad request for invalid  paylaod
    * def info = karate.info
    * set info.subset = 'Paymentcontext with no address field'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)

    And match apiApp.Access_Token_CCG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
#modify the payload
    And remove pisp_payload.Risk.DeliveryAddress

#Call Account_Request_Setup API
    And set Application_Details.request_method = 'POST'
    When call apiApp.Payment_Setup(Application_Details)

    And match apiApp.Payment_Setup.responseStatus == 400



  Scenario: validation of 400 bad request for invalid  access token
    * def info = karate.info
    * set info.subset = 'Invalid access token'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)

    And match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.token_type = "abchderty"
    And set Application_Details.request_method = 'POST'
    When call apiApp.Payment_Setup(Application_Details)

    And match apiApp.Payment_Setup.responseStatus == 201

























