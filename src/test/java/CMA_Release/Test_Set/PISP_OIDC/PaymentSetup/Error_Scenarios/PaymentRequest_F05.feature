@PaymentSetup
@PISP_CR28_Error
@PISP_CR28_Error1

Feature: Error conditions1 of CR28 FS Codes

  Background:
    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * def Application_Details = active_tpp.AISP_PISP
    * json Result = {}
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
      """

  @PISP_sam_CR0281
  Scenario Outline: Verify Rejected status for invalid BIC #key# for PISP Payload

    * def info = karate.info
    * set info.subset = 'Invalid BIC & IBAN'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAgent =
    """
    {
          "SchemeName": "BICFI",
          "Identification": "<BIC>"
    }
    """
    And set pisp_payload $.Data.Initiation.CreditorAccount =
    """
    {
        "SchemeName": "IBAN",
        "Identification": "<IBAN>",
        "Name": "Current Account - Long Account Name for GB Account Test 1234"
    }
    """
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      |count| Condition              | BIC        | IBAN                 |Response| Status  |
      |01   | "Invalid BIC"          |BOFTGB2BXXX |GB72BOFI30115413966656| 201    |Rejected |
      |02   | "NOT SEPA BIC"         |BMAGBHBMXXX |GB72BOFI30115413966656| 201    |Rejected |


  @PISP_IBAN_CR0282
  Scenario Outline: Verify Rejected status for invalid IBAN #key# for PISP Payload

    * def info = karate.info
    * set info.subset = 'Invalid BIC & IBAN'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAgent =
    """
    {
          "SchemeName": "BICFI",
          "Identification": "<BIC>"
    }
    """
    And set pisp_payload $.Data.Initiation.CreditorAccount =
    """
    {
        "SchemeName": "IBAN",
        "Identification": "<IBAN>",
        "Name": "Current Account - Long Account Name for GB Account Test 1234"
    }
    """
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>

    Examples:
      |count| Condition              | BIC        | IBAN                 |Response|
      |01   | "Invalid IBAN"         |BOFIGB2BXXX |GB72BOFT30115413966656| 400    |
      |02   | "Invalid IBAN Checksum"|BOFIGB2BXXX |GB72BOFI301154139666  | 400    |
      |03   | "NOT SEPA IBAN"        |BMAGBHBMXXX |BH67BMAG00001299123456| 400    |
 # 05_Failed No SEPA IBAN defect 1621 is raised for the same


  @PISP_Ruc_CR0283
  Scenario Outline: Verify 400 status for Invalid NSC #key# for PISP Payload

    * def info = karate.info
    * set info.subset = 'Invalid NSC'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAccount =
    """
    {
        "SchemeName": "SortCodeAccountNumber",
        "Identification":"<NSCAccount>",
        "Name": "NSC Test",
        "SecondaryIdentification":"ADHOC"
      }
    """
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      |count| Condition               |  NSCAccount    |Response| Status  |
      |01   | "Invalid NSC1"          | 00000073618733 |201     |Rejected |
      |02   | "Invalid NSC2"          | 12345673618733 |201     |Rejected |
      |03   | "Invalid NSC3"          | 90220073618733 |201     |Rejected |

  @PISP_CC_CR0284
  Scenario Outline: Verify 201 status for Invalid Country and Currency #key# for PISP Payload

    * def info = karate.info
    * set info.subset = 'Invalid Country and Currency'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.InstructedAmount.Currency = "<Currency>"
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      |count| Condition            | Currency |Response| Status  |
      |01   | "Invalid Currency1"  | INR      |201     |Rejected |
      |02   | "Invalid Currency2"  | EUR      |201     |Rejected |


    # Getting AcceptedTechnicalValidation- Need to confirm for expected for below scenarios
  @PISP_RC_CR0285
  Scenario Outline: Verify 201 status for Credit Grade 6 or 7 #key# for PISP PaymentSetup Payload

    * def info = karate.info
    * set info.subset = 'Invalid Credit Grade'
    * set info.key = "<count>_" + <Condition>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    Given set pisp_payload $.Data.Initiation.CreditorAccount.Identification = "<SortcodeAccountNo>"
    * def AccountNo = pisp_payload.Data.Initiation.CreditorAccount.Identification
    * def NSC = ( AccountNo.substring(0,6) )
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "CREDIT_GRADING='<Grade>'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <Response>
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == '<Status>'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}
    * string a = "CREDIT_GRADING='2'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Examples:
      |count| Condition            | SortcodeAccountNo | Grade |Response|        Status               |
      |01   | "Credit Grade 6"     | 30102750091131    |  6    |  201   | AcceptedTechnicalValidation |
      |02   | "Credit Grade 7"     | 30102750091131    |  7    |  201   | AcceptedTechnicalValidation |