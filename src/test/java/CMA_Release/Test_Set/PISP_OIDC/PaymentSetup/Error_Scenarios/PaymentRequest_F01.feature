@PaymentSetup
@PISP
Feature: Feature to getting error Payment ID using Payment Initiation API

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
      """


  Scenario Outline: 400 bad request validation for invalid payload

    * def info = karate.info
    * set info.subset = '400 bad request validation'

      #modify the payload
    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    And  set pisp_payload.Data.Initiation.CreditorAccount.SchemeName = <SchemeName>
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 400

    Examples:
      | SchemeName |
      | 1234       |


  Scenario Outline: validation of 400 bad request for invalid  currency value in paylaod
    * def info = karate.info
    * set info.subset = '400 bad request validation for paylaod'


    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    Given set pisp_payload.Data.Initiation.RemittanceInformation = <Unstructured>
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 400


    Examples:
      | Unstructured |
      | "$$abc!!"    |


  Scenario Outline: validation of 400 bad request for invalid  currency value in paylaod
    * def info = karate.info
    * set info.subset = '400 bad request validation for paylaod'


    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    Given set pisp_payload.Data.Initiation.InstructedAmount = <Currency>
    And set Application_Details.request_method = 'POST'
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 400
    And set Result.defectID = '620'

#    And set Result.defectID = ''


    Examples:
      | Currency |
      | "EUR"    |
      | "USD"    |


#  Scenario:  Validation of 400 bad request for header parameters
#
#    * def info = karate.info
#    * set info.subset = 'validation of x-idempotency-key '
#
#    Given json Application_Details = active_tpp.AISP_PISP
#    And call apiApp.configureSSL(Application_Details)
#    And set Application_Details.request_method = 'POST'
#    And set Application_Details $.grant_type = 'client_credentials'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#    When call apiApp.Access_Token_CCG(Application_Details)
#    Then match apiApp.Access_Token_CCG.responseStatus == 200
#    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#
#    #Given set pisp_reqHeader.x-idempotency-key = null
#    Given remove pisp_reqHeader.x-idempotency-key
#    And print pisp_reqHeader
##    When call apiApp.Payment_Setup(Application_Details)
#
#    Given url PmtSetupUrl
#    And request pisp_payload
#    And headers pisp_reqHeader
#    And headers tpp_credentials
#    And header Authorization = 'Bearer ' + Application_Details.access_token
#    When method POST
#
#    And set Result.ActualOutput.PaymentID_setup.Input = Application_Details
#    And set Result.ActualOutput.PaymentID_setup.Input.Endpoint = PmtSetupUrl
#    And set Result.ActualOutput.PaymentID_setup.Output.response =  apiApp.Payment_Setup.response
#    And set Result.ActualOutput.PaymentID_setup.Output.responseStatus = apiApp.Payment_Setup.ResponseStatus
#    Then match apiApp.Payment_Setup.responseStatus == 400


  @testdemo
  Scenario Outline: Validation of error code for different Header parameters #key#

    * def info = karate.info
    * set info.subset = 'validation of different error codes'
    * set info.key = "Field_"+ <count>

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    Given set pisp_reqHeader $.<Field> = <value>
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == <error>

    Examples:
      | Field                            | value         | count | error |
      #| Accept                           | 'application/jso' | 01   |406   |
      | x-fapi-customer-last-logged-time | ' '           | 02    | 400   |
      | x-fapi-financial-id              | 'abc23'       | 03    | 403   |
      | Content-Type                     | 'application' | 04    | 400   |











