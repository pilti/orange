@PaymentSetup
@PISP
@PISP_API
@PISP_PaymentSetup
@payal45
Feature: Feature to test  test cases of CR38 negative Scenario

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    #* json UIResult = {}
   * set Result.Testname = null


    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
   """

  Scenario Outline:  TC01 #key# Validation of  400 bad request for different paylaod parameters

    * def info = karate.info
    * set info.key = "Field_"+ <count>
    * set info.subset = 'validation of different paylaod parameters'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_client_secret = Application_Details.client_secret
    When call apiApp.Access_Token_CCG(Application_Details)
    And match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token


    Given set pisp_payload $.<Field> = '<value>'
    And set Application_Details.request_method = 'POST'
    When call apiApp.Payment_Setup(Application_Details)
    And match apiApp.Payment_Setup.responseStatus == 400




    Examples:
      | Field                                                      | value      | count |
      | Risk.PaymentContextCode                                    | !@ABcc&*_= | '001' |
      | Data.Initiation.RemittanceInformation.BeneficiaryReference | $$$$$      | '002' |
      | Data.Initiation.CreditorAccount.Name                       | !@#~!!!$   | '003' |
      | Data.Initiation.CreditorAccount.SecondaryIdentification    | @~!!!!$$78 | '004' |
      | Risk.MerchentCategoryCode                                  | @#=_Sab    | '006' |
      |Data.Initiation.RemittanceInformation.Unstructured          |@@!!$$      | '009' |
#      |Risk.MerchantCustomerIdentification                         | $$@@@dbht!  | '010' |
#      |Risk.DeliveryAddress.BuildingNumber                         |@345$$       | '011' |




