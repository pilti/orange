@PaymentSetup

@PISP_API
@PS1
Feature: This feature is to test Payment submission Id validations

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def dbFunctions = Java.type('CMA_Release.Entities_DB.PAYMENT_REQUEST')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  Scenario: Verify that PaymentID and Submission ID both are generated during Payment setup
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And print Application_Details
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And def query = dbFunctions.get_PaymentDetails(apiApp.Payment_Setup.response.Data.PaymentId,PSD2_PISP_PID_Conn)
    And json query = query
    And match query[0] contains {"PYMT_SUBMISSION_ID": '#string'}




