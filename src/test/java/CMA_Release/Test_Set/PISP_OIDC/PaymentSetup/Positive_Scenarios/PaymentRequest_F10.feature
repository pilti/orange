@PaymentSetup
@PISP
Feature: Feature to test  test cases of CR22 Positive Scenario

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null


    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
       """
#  @UKNSC
#
#  Scenario: validation of payment initiation response with correct UK NSC
#    * def info = karate.info
#    * set info.subset = 'validation of payment initiation response with correct details'
#
#    Given def Application_Details = active_tpp.AISP_PISP
#    Then call apiApp.configureSSL(Application_Details)
#
#    Given set Application_Details.request_method = 'POST'
#    And set Application_Details $.grant_type = 'client_credentials'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#    And print Application_Details
#    When call apiApp.Access_Token_CCG(Application_Details)
#    Then match apiApp.Access_Token_CCG.responseStatus == 200
#    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
#
#    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
#    And set pisp_payload.Data.Initiation.CreditorAccount.Identification = "30115413966656"
#    When call apiApp.Payment_Setup(Application_Details)
#    And match apiApp.Payment_Setup.responseStatus == 201

  @GBP
  Scenario: validation of payment initiation response with Currency_GBP
    * def info = karate.info
    * set info.subset = 'validation of payment initiation response with correct details'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set pisp_payload.Data.Initiation.InstructedAmount.Currency = "GBP"
    When call apiApp.Payment_Setup(Application_Details)
    And match apiApp.Payment_Setup.responseStatus == 201

