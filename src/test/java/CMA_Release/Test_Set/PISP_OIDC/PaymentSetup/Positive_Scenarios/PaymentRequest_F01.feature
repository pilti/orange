@PaymentSetup
@PISP
@PISP_API
@PISP_PaymentInitiation


Feature: This feature is to test PreAuth validation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario Outline: Verify that API response retrieves payment ID with status

    Given def info = karate.info
    And set info.subset = 'Response validation as per CMA'
    And json Application_Details = active_tpp.<TPPRole>
    # Network Handshake for test tpp
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Payment_Setup(Application_Details)
    And match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      | TPPRole   |
      | AISP_PISP |

  Scenario Outline: Verify that API response retrieves Initiation details of payment request

    * def info = karate.info
    * set info.subset = 'Response validation as per CMA'
    Given json Application_Details = active_tpp.<TPPRole>
    # Network Handshake for test tpp
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data.Initiation contains {"InstructionIdentification": '#string'}
    And match apiApp.Payment_Setup.response.Data.Initiation.InstructionIdentification == pisp_payload.Data.Initiation.InstructionIdentification
    And match apiApp.Payment_Setup.response.Data.Initiation contains {"EndToEndIdentification": '#string'}
    And match apiApp.Payment_Setup.response.Data.Initiation.EndToEndIdentification == pisp_payload.Data.Initiation.EndToEndIdentification
    And match apiApp.Payment_Setup.response.Data.Initiation contains {"InstructedAmount": {"Amount": '#string' , "Currency": '#string'}}
    And match apiApp.Payment_Setup.response.Data.Initiation.InstructedAmount == pisp_payload.Data.Initiation.InstructedAmount

    Examples:
      | TPPRole   |
      | AISP_PISP |

  Scenario Outline: Verify that API response retrieves CreditorAccount details of payment request

    * def info = karate.info
    * set info.subset = 'Response validation as per CMA'
    Given json Application_Details = active_tpp.<TPPRole>
    # Network Handshake for test tpp
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount contains {"SchemeName": '#string'}
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.SchemeName == pisp_payload.Data.Initiation.CreditorAccount.SchemeName
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount contains {"Identification": '#string'}
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.Identification == pisp_payload.Data.Initiation.CreditorAccount.Identification
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount contains {"Name": '#string'}
    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.Name == pisp_payload.Data.Initiation.CreditorAccount.Name
#    And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount contains {"SecondaryIdentification": '#string'}
   # And match apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.SecondaryIdentification == pisp_payload.Data.Initiation.CreditorAccount.SecondaryIdentification

    Examples:
      | TPPRole   |
      | AISP_PISP |


  Scenario Outline: Verify that API response retrieves details under Risk section

    * def info = karate.info
    * set info.subset = 'Response validation as per CMA'
    Given json Application_Details = active_tpp.<TPPRole>
    # Network Handshake for test tpp
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Risk contains {"PaymentContextCode": '#string'}
    And match apiApp.Payment_Setup.response.Risk.PaymentContextCode == pisp_payload.Risk.PaymentContextCode
    And match apiApp.Payment_Setup.response.Risk contains {"MerchantCategoryCode": '#string'}
    And match apiApp.Payment_Setup.response.Risk.MerchantCategoryCode == pisp_payload.Risk.MerchantCategoryCode
    And match apiApp.Payment_Setup.response.Risk contains {"MerchantCustomerIdentification": '#string'}
    And match apiApp.Payment_Setup.response.Risk.MerchantCustomerIdentification == pisp_payload.Risk.MerchantCustomerIdentification

    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"StreetName":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"BuildingNumber":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"PostCode":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"PostCode":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"TownName":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"Country":'#string'}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"AddressLine":['#string','#string']}
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress contains {"CountrySubDivision":['#string','#string']}


    Examples:
      | TPPRole   |
      | AISP_PISP |


