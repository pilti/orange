@PaymentSetup
@PISP
Feature: Feature to test  test cases of CR38 Positive Scenario

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null


    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp.write(Result,info);

       }
   """



@CR38_rup
  Scenario Outline:  TC01#key# Validation of  201 for different paylaod parameters

    * def info = karate.info
    * set info.key = "Field_"+ <count>
    * set info.subset = 'validation of different paylaod parameters'
    * set pisp_payload $.<Field> = '<value>'

  Given def Application_Details = active_tpp.AISP_PISP
  Then call apiApp.configureSSL(Application_Details)

  Given set Application_Details.request_method = 'POST'
  And set Application_Details $.grant_type = 'client_credentials'
  And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
  And print Application_Details
  And call apiApp.Access_Token_CCG(Application_Details)
  Then match apiApp.Access_Token_CCG.responseStatus == 200

  Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
  And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
  When call apiApp.Payment_Setup(Application_Details)
  Then match apiApp.Payment_Setup.responseStatus == 201

  Examples:
      | Field                                                            |   value                               |count|
      #|Risk.PaymentContextCode                                           |                                       |     |
     | Data.Initiation.RemittanceInformation.Reference                  | 1236589                             |'1'    |
    | Data.Initiation.RemittanceInformation.Unstructured               | 1236589                           |'2'    |
    | Data.Initiation.EndToEndIdentification                           | +/45BCDR*                           |'3'   |
    | Data.Initiation.CreditorAccount.Name                             | +/WXYZ                           |'4'    |
    | Data.Initiation.CreditorAccount.SecondaryIdentification          | abcdefghijlmnopqr                    |'5'   |
     | Risk.MerchantCustomerIdentification                              | STUVWXYZ019                          |'6'    |
    | Risk.MerchantCategoryCode                                        | q(-)                                 |'7'    |
      | Risk.DeliveryAddress.BuildingNumber                              | 11111                               |'8'     |
      |Risk.DeliveryAddress.AddressLine[0]                                 | abc()abc                              |  '9'  |
     | Risk.DeliveryAddress.AddressLine[1]                                 | bc34Bc                              |  '9'  |
      |  Risk.DeliveryAddress.PostCode                                    | 12abcdA                            |'10'    |
     |  Risk.DeliveryAddress.TownName                                    | ABaa1234                            |'11'    |
      |  Risk.DeliveryAddress.CountrySubDivision[0]                        | 123AA))                            |'12'    |
      |  Risk.DeliveryAddress.CountrySubDivision[1]                        |abc2345CD                            |'12'    |
      | Data.Initiation.InstructionIdentification                         | a123+AbCoffey()+                             |'13'    |





