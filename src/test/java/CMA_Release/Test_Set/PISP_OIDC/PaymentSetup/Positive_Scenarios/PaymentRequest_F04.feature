@PaymentSetup
@PISP
Feature: This feature is to Fieldlenth validation

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """


  @InsID
  Scenario: Validate field length of InstructionIdentification in payment response payload

    * def info = karate.info
   * set info.subset = 'Fieldlenth validation of payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And def InsID_len = apiApp.Payment_Setup.response.Data.Initiation.InstructionIdentification.length()
    * print InsID_len
    And assert InsID_len >= 1
    And assert InsID_len <= 35


  @EndID
  Scenario: Validate field length of EndToEndIdentification in payment response payload
    * def info = karate.info
    * set info.subset = 'Fieldlenth validation of payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And def EndID_len =  apiApp.Payment_Setup.response.Data.Initiation.EndToEndIdentification.length()
    * print EndID_len
    And assert EndID_len >= 1
    And assert EndID_len <= 35

  @Inst_amount
  Scenario: Validate InstructedAmount under Data Initiation in payment response payload

    * def info = karate.info
    * set info.subset = 'Fieldlenth validation of payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data.Initiation.InstructedAmount.Amount == pisp_payload.Data.Initiation.InstructedAmount.Amount
    And match apiApp.Payment_Setup.response.Data.Initiation.InstructedAmount.Currency == pisp_payload.Data.Initiation.InstructedAmount.Currency

