@PISP_CR07_Positive_x

Feature: Positive conditions of CR07 Delivery Address Fields

  Background:

    * def apiApp = new apiapp()
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);


       }
      """

  @Sample
  Scenario Outline: Verify 201 code when Non mandatory fields #key# is passed

    * def info = karate.info
    * set info.subset = 'Missing Nonmandatory fields'
    * set info.key = "<count>_" + <Field>

    Given remove pisp_payload.Risk.DeliveryAddress.<NonMandatory>
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And print Application_Details
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      | count | Field                  | NonMandatory          |
      | 01    | "Add Line2 Removed"    | AddressLine[1]        |
      | 02    | "County SDiv2 Removed" | CountrySubDivision[1] |
      | 03    | "StreetName"           | StreetName            |
      | 04    | "PostCode"             | PostCode              |
      | 05    | "BuildingNumber"       | BuildingNumber        |
      | 06    | "Add Line Removed"     | AddressLine           |
      | 07    | "County SDiv Removed"  | CountrySubDivision    |


  Scenario Outline: 201 created status after Removing 2nd Lines of both fields of Delivery Address

    * def info = karate.info
    * set info.key = "<count>_" + <Field>
    * set info.subset = 'Both 2nd Lines Removed'

    Given remove pisp_payload.Risk.DeliveryAddress.AddressLine[1]
    And remove pisp_payload.Risk.DeliveryAddress.CountrySubDivision[1]
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And print Application_Details
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      | count | Field                |
      | 01    | "Both Line2 Removed" |


  Scenario Outline: Verify 201 status when #key# is passed in Delivery Address

    * def info = karate.info
    * set info.subset = 'Boundary Value Analysis fields'
    * set info.key = "<count>_" + <Field>

    Given set pisp_payload $.Risk.DeliveryAddress =
    """
    {
      "AddressLine": [
                <Address Line1>,
                <Address Line2>
            ],
            "StreetName": <StreetName>,
            "BuildingNumber": <BuildingNumber>,
            "PostCode": <PostCode>,
            "TownName": <TownName>,
            "CountrySubDivision": [
                <CountrySDiv Line1>,
                <CountrySDiv Line2>
            ],
            "Country": <Country>
    }
    """
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And print Application_Details
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      | count | Field                                  | Address Line1                                                          | Address Line2                                                          | StreetName                                                               | BuildingNumber     | PostCode         | TownName                            | CountrySDiv Line1                   | CountrySDiv Line2                   | Country |
      | 01    | "All Valid details"                    | "           "                                                          | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 02    | "All Spaces in Add Line2"              | Flat 7                                                                 | '                    '                                                 | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 03    | "Spaces in Country SDiv2"              | Flat 7                                                                 | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | '          '                        | GB      |
      | 04    | "Line1 is 70 character"                | AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345 | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 05    | "Line2 is 70 character"                | Flat 7                                                                 | AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345 | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 06    | "SDiv1 is 35 character"                | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | AAaaa12345BBbbb12345CCccc12345DDddd | '      DEFG '                       | GB      |
      | 07    | "SDiv2 is 35 character"                | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDA                               | AAaaa12345BBbbb12345CCccc12345DDddd | GB      |
      | 08    | "StreetName is 70 character"           | Flat 7                                                                 | 'Acacia Lodge'                                                         | 'AAaaa12345BBbbb12345CCccc12345DDddd12345EEeee12345FFfff12345GGggg12345' | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCD                            | DEFG                                | GB      |
      | 09    | "BuildingNumber is 16 character"       | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "0123456789123456" | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 10    | "PostCode is 16 character"             | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | AAaaa12345BBbbb1 | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 11    | "TownName is 35 character"             | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | AAaaa12345BBbbb12345CCccc12345DDddd | ABCDAB                              | DEFG                                | GB      |
      | 12    | "Country is 2 character"               | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | AAaa                                | ABCDAB                              | DEFG                                | 'GB'    |
      | 13    | "CountrySDiv1 spes exding 35 charts"   | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | 'AAaaa12345BBbbb12345CCccc12345   ' | DEFG                                | GB      |
      | 14    | "TownName spaces exceeding 35 charts"  | Flat 7                                                                 | 'Acacia Lodge'                                                         | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | 'AAaaa12345BBbbb12345CCccc12345  '  | ABCDAB                              | DEFG                                | GB      |
      | 15    | "SpES in Add Line1 & dtls in AddL2"    | '                    '                                                 | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 16    | "SpES in Add Line1 & nothing in AddL2" | '                    '                                                 |                                                                        | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | ABCDABCDABCDABCDAB                  | DEFG                                | GB      |
      | 17    | "Spaces in Country SDiv1"              | Flat 7                                                                 | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | '           '                       | DEFG                                | GB      |
      | 18    | "SpES in CSD1 & nothing in CSD2"       | Flat 7                                                                 | Acacia Lodge                                                           | AcaciaAvenue                                                             | "27"               | GU31 2ZZ         | Sparsholt                           | '           '                       |                                     | GB      |