@PaymentSetup

Feature: Payment response payload

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """

  @Initiation1
  Scenario: Validate Initiation payload in payment response payload

    * def info = karate.info
    * set info.subset = 'Initiation payload in payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    Then match apiApp.Payment_Setup.response ==
    """
    {
  "Data": {
    "PaymentId": '#string',
    "Status":'#string',
    "CreationDateTime": '#string',
    "Initiation": {
      "InstructionIdentification": '#string',
      "EndToEndIdentification": '#string',
      "InstructedAmount": {
        "Amount": "1.61",
        "Currency": '#string'
      },
      "CreditorAccount": {
        "SchemeName": '#string',
        "Identification": '#string',
        "Name": '#string',
        "SecondaryIdentification": '#string'
      },
      "RemittanceInformation": {
        "Unstructured": '#string',
        "Reference": '#string'
      }
    }
  },
  "Risk": {
    "PaymentContextCode": '#string',
    "MerchantCategoryCode": '#string',
    "MerchantCustomerIdentification": '#string',
    "DeliveryAddress": {
      "AddressLine": [
        '#string',
        '#string'
      ],
      "StreetName": '#string',
      "BuildingNumber": '#string',
      "PostCode": '#string',
      "TownName": '#string',
      "CountrySubDivision": [
        '#string',
        '#string'
      ],
      "Country": '#string'
    }
  },
  "Links": {
    "Self": '#string'
  },
  "Meta": {
  }
}
    """



  @Val_len
  Scenario: Validate field length of fields in payment response payload
    * def info = karate.info
    * set info.subset = 'CreditorAccount in payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)

    And match apiApp.Payment_Setup.responseStatus == 201
    And def Id_len =  apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.Identification.length()
    * print Id_len
    And assert Id_len >= 1
    And assert Id_len <= 34
    And def name_len =  apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.Name.length()
    * print name_len
    And assert name_len >= 1
    And assert name_len <= 70
    And def SecId_len =  apiApp.Payment_Setup.response.Data.Initiation.CreditorAccount.SecondaryIdentification.length()
    * print SecId_len
    And assert SecId_len >= 1
    And assert SecId_len <= 34