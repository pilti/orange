@PaymentSetup
@PISP
Feature: Payment response payload

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """

  @Header
  Scenario: Validate  Post Payment request header fields(mandatory)

    * def info = karate.info
    * set info.subset = 'validation of Post Payment request header fields'
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201


  @ClientID_Rup
  Scenario: Validate Post Payment request header field (Clientid AND Clientsecret)

    * def info = karate.info
    * set info.subset = 'validation of Post Payment request header field (Clientid)'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type

    Given url PmtSetupUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And remove Application_Details.client_id
    And remove Application_Details.client_secret
    And header Authorization = 'Bearer ' + Application_Details.access_token
    When method POST

    And set Result.PaymentSetup.Input.URL = PmtSetupUrl
    And set Result.PaymentSetup.Input.Payload = pisp_payload
    And set Result.PaymentSetup.Input.header = pisp_reqHeader
    And set Result.PaymentSetup.Output.responseStatus = responseStatus
    Then match responseStatus == 201


  @optional_rup
  Scenario Outline: Validate 201 code in Payment Setup when header has optional field #key#

    * def info = karate.info
    * def key = '<key>'
    * set info.key = key
    And set info.subset = 'Optional Fields Validation'
    And def Application_Details = active_tpp.AISP_PISP
    And set pisp_reqHeader $.'<Field>' = <value>
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    Examples:
      | Field                            | value                          |key|
      | Accept                          | "application/json"              |Accept|
     | x-fapi-customer-last-logged-time | "Sun, 10 Sep 2017 19:43:31 UTC"| logged time|
     | x-fapicustomer-ipaddress         | "127.10.101"                   | IPaddress  |
     |   x-jwssignature                 | "dsad343smfdkfa3434ksndkfsdad" | signature  |
     | Content-Type                     |"application/json"             | contentType |

