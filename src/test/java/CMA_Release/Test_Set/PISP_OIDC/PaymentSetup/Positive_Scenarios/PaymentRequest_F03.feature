@PaymentSetup
@PISP
Feature: This feature is to test Payment setup 200 ok validation

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """
    @Rupal
  Scenario: Payment Instruction setup_Success 201

    * def info = karate.info
    * set info.subset = 'Payment ID with AcceptedTechnicalValidation status'

      Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

      Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

  @rup
  Scenario: validate that in API create payment setup resourse is updated to AcceptedTechnicalValidation

    * def info = karate.info
    * set info.subset = 'Payment ID with AcceptedTechnicalValidation status'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    And match apiApp.Payment_Setup.response.Data.Status == 'AcceptedTechnicalValidation'


  @length
  Scenario: Validate field length of paymentID in payment response payload
    * def info = karate.info
    * set info.subset = 'Payment ID with AcceptedTechnicalValidation status'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And def pay_len =  apiApp.Payment_Setup.response.Data.PaymentId.length()
    * print pay_len
    And assert pay_len >= 1
    And assert pay_len <= 128



