@PaymentSetup
@PISP
@PISP_API
@PISP_PaymentInitiation
@PISP_try_1

Feature: This feature is to test PreAuth validation

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


#  Scenario Outline: Verify that Payment Id with Rejected status will be generated in response when invalid creditor account number is sent in the request payload
#
#    Given def info = karate.info
#    And set info.subset = 'Payment ID with Rejected status'
#    And json Application_Details = active_tpp.<TPPRole>
#    Given def Application_Details = active_tpp.AISP_PISP
#    Then call apiApp.configureSSL(Application_Details)
#
#    #CC_Token
#    Given set Application_Details.request_method = 'POST'
#    And set Application_Details $.grant_type = 'client_credentials'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#    And print Application_Details
#    And call apiApp.Access_Token_CCG(Application_Details)
#    Then match apiApp.Access_Token_CCG.responseStatus == 200
#
#    #Payment_Setup
#    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#    And set pisp_payload.Data.Initiation.CreditorAccount.Identification = <SortCodeAccountNo>
#    And call apiApp.Payment_Setup(Application_Details)
#    And match apiApp.Payment_Setup.responseStatus == 201
#    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
#    And match apiApp.Payment_Setup.response.Data.Status == 'Rejected'
#    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}
#
#    Examples:
#      | TPPRole   | SortCodeAccountNo |
#      | AISP_PISP | '30115413966657'  |


  Scenario Outline: Verify that Payment Id with Rejected status will be generated in response when creditor account has SEPA account

    Given def info = karate.info
    And set info.subset = 'Payment ID with Rejected status'
    And json Application_Details = active_tpp.<TPPRole>
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set pisp_payload.Data.Initiation.CreditorAgent.SchemeName = <BICFI>
    And set pisp_payload.Data.Initiation.CreditorAgent.Identification = <Identification>
    And set pisp_payload.Data.Initiation.CreditorAccount.SchemeName = <SchemeName>
    And set pisp_payload.Data.Initiation.CreditorAccount.Identification = <SortCodeAccountNo>
    And set pisp_payload.Data.Initiation.CreditorAccount.Name = <Name>
    And remove pisp_payload.Data.Initiation.CreditorAccount.SecondaryIdentification

    And call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data contains {"PaymentId": '#string'}
    And match apiApp.Payment_Setup.response.Data.Status == 'Rejected'
    And match apiApp.Payment_Setup.response.Data contains {"CreationDateTime": '#string'}

    Examples:
      | TPPRole   | SortCodeAccountNo        | SchemeName | Name            | BICFI   | Identification |
      | AISP_PISP | 'IE29BOFI90001718228011' | 'IBAN'     | 'Joint account' | 'BICFI' | 'BOFIIE2DXXX'  |


  Scenario Outline: Verify that unique Payment Id will not be generated in response when duplicate idempotency key is passed in the request

    Given def info = karate.info
    And set info.subset = 'Idempotency check'
    And json Application_Details = active_tpp.<TPPRole>

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Payment_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And call apiApp.Payment_Setup(Application_Details)
    And set pisp_reqHeader $.x-idempotency-key = apiApp.Payment_Setup.idempotency_value
    And json tpp_credentials = {"client_id": '#(Application_Details.client_id)',"client_secret": '#(Application_Details.client_secret)'}

    Given url PmtSetupUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header Authorization = 'Bearer ' + apiApp.Access_Token_CCG.response.access_token
    When method POST

    And set Result.Input.TPPRole = '<TPPRole>'
    And set Result.Input.scope = Application_Details.scope
    And set Result.Input.PmtSetupUrl = PmtSetupUrl
    And set Result.Input.idempotency_value = apiApp.Payment_Setup.idempotency_value
    And set Result.Output.responseStatus = responseStatus
    And set Result.Output.response = response
    And match responseStatus == 201
    And match response.Data.PaymentId == apiApp.Payment_Setup.response.Data.PaymentId
    And match response.Data.Status == 'AcceptedTechnicalValidation'

    Examples:
      | TPPRole   |
      | AISP_PISP |

