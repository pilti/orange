@PaymentSetup
@PISP
Feature: This feature validates the 201 status code if user inputs valid URI post payment

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
       """

  @URLVal

  Scenario: Verify Post payment URI Validation

    Given def info = karate.info
    And set info.subset = 'URI validation'
    And def Application_Details = active_tpp.AISP_PISP
    And def call_setup = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/Payment_Setup.feature.') Application_Details







