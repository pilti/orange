@PaymentSetup
@PISP
Feature: This feature is to test Currency and country fields  validation in payment setup response

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """
  @Currency
  Scenario: Validate Currency field under Data Initiation in payment response payload

    * def info = karate.info
    * set info.subset = 'Data Initiation in payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Data.Initiation.InstructedAmount.Currency == 'GBP'
    And print apiApp.Payment_Setup.response.Data.Initiation.InstructedAmount.Currency


  @Country
  Scenario: Validate Country field under Data Initiation in payment response payload

    * def info = karate.info
    * set info.subset = 'Data Initiation in payment response payload'

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And print Application_Details
    And call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
    And match apiApp.Payment_Setup.response.Risk.DeliveryAddress.Country == 'GB'
    And print apiApp.Payment_Setup.response.Risk.DeliveryAddress.Country


