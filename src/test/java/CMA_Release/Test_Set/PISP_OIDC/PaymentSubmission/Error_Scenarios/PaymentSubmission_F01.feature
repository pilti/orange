@PaymentSubnoclientdetails
@PISP
Feature: Payment Submission Error scenarios

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }

      """
    * def Application_Details = {}



  Scenario: PISP_Submission_validheaders_NoCIDCSecret

    * def info = karate.info
    * set info.subset = "Access_Token_CCG"
    Given json Application_Details = active_tpp.AISP_PISP

      # Forming consent data for use
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    And match apiApp.Payment_Submission.response.Data.Status == 'AcceptedSettlementInProcess'