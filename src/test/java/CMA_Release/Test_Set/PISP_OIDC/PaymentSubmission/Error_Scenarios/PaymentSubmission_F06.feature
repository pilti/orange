@PISP
Feature: This feature is to test Payment submission error conditions

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @11try
  Scenario: verify that 400 bad request error message when Structural check fails in the payment submission API
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    #modifying payload structurally
    And def Data = pisp_payload.Data
    And remove pisp_payload.Data.{
    And set pisp_payload.Data: = Data
    And print pisp_payload
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400

  @TRY
  Scenario: verify that 400 bad request error message when Semantic check fails in the payment submission API
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    #modifying payload semantically
    And def Data = pisp_payload.Data
    And replace Data.{ = "$"
    And set pisp_payload.Data = Data
    And print pisp_payload
    And set Application_Details $.request_method = 'POST'

    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400
    And print apiApp.Payment_Submission.response
    And match apiApp.Payment_Submission.response !contains apiApp.Payment_Submission.response.PaymentSubmissionId

  @Regression_19th_Nov
  Scenario Outline: verify 201 response when Accept optional #key# field is passed in payment submission API
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

   #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And set pisp_reqHeader $.Accept = <value>
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == <error>

    Examples:
      | value               | key          | error |
     | " "                 | blank        | 201   |
      | "application//json" | wrong format | 406   |
      | "@json"             | invalid      | 406   |





