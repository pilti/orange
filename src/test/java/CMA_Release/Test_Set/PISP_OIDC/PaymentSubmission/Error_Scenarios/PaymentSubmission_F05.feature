@PaymentSubErr
@PISP
Feature: Payment Submission Error scenarios

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }

      """
    * def Application_Details = {}



  Scenario: PISP_Submission_Authorisation_Blank

    * def info = karate.info
    * set info.subset = "Access_Token_CCG"
    Given json Application_Details = active_tpp.AISP_PISP

      # Forming consent data for use
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

    Then call apiApp.PreReq_PaymentSubmission(Application_Details)


      #Submission
      Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
      And set Application_Details $.request_method = 'POST'
      And set Application_Details $.token_type = ""
      And set Application_Details $.access_token = ""
      And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
      And print Application_Details
      And call apiApp.Payment_Submission(Application_Details)
      Then match apiApp.Payment_Submission.responseStatus == 401
      And print apiApp.Payment_Submission.response



    Scenario: PISP_Submission_Authorisation_Removed
      * def info = karate.info
      * set info.subset = "Access_Token_CCG"
      Given json Application_Details = active_tpp.AISP_PISP

    # Forming consent data for use
      And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

      Then call apiApp.PreReq_PaymentSubmission(Application_Details)

      * set pisp_payload $.Data.PaymentId = Application_Details.PaymentId

    #Submission
      Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
      And def request_method = 'POST'
      And remove pisp_reqHeader.Authorisation

      And url PmtSubUrl
      And request pisp_payload
      And headers pisp_reqHeader

      And header x-idempotency-key = Application_Details.idempotency_key

      And print pisp_reqHeader
      When method request_method
      Then set Result.PaymentSubmission.Input.URL = PmtSubUrl
      And set Result.PaymentSubmission.Input.SubmissionPayload = pisp_payload
      And set Result.PaymentSubmission.Input.SubmissionHeader = pisp_reqHeader
      And set Result.PaymentSubmission.Input.SubmissionHeader.x_idempotency_key = Application_Details.idempotency_key
      And set Result.PaymentSubmission.Input.request_method = request_method
     And set Result.PaymentSubmission.Output.responseStatus = responseStatus
      And set Result.PaymentSubmission.Output.responseHeaders = responseHeaders

      Then match responseStatus == 401
      And print Result.PaymentSubmission.Output


  Scenario: PISP_Submission_FinancialID_Invalid

    * def info = karate.info
    * set info.subset = "Access_Token_CCG"
    Given json Application_Details = active_tpp.AISP_PISP

      # Forming consent data for use
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

    Then call apiApp.PreReq_PaymentSubmission(Application_Details)


      # Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_reqHeader.x-fapi-financial-id = "11111"
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 403
    And print apiApp.Payment_Submission.response


  Scenario: PISP_Submission_FinancialID_Blank

    * def info = karate.info
    * set info.subset = "Access_Token_CCG"
    Given json Application_Details = active_tpp.AISP_PISP

    # Forming consent data for use
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

    Then call apiApp.PreReq_PaymentSubmission(Application_Details)


    #Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_reqHeader.x-fapi-financial-id = ""
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 403
    And print apiApp.Payment_Submission.response