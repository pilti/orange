
Feature: CR10_Positive Scenario for Payment_Submission

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  Scenario Outline: Validation of 201 for invalid account
    * def info = karate.info
    * set info.key = "<count>_"+<Condition>
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    * def acc = user_details.PISP.G_User_2.AccountNo1
    * def accno = acc.substr(6,acc.length())
    * def NSC = acc.substring(0,6)

    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,accno,SP_SHARED_DATA_DB_Conn,a)
    * string a = "<BlockCondition>='1'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)


    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And match apiApp.Payment_Submission.response.Data.Status == 'Rejected'

    * string a = "<BlockCondition>='0'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,accno,SP_SHARED_DATA_DB_Conn,a)

  Examples:
  |count| Condition                | BlockCondition  |
  |01   |   "BlockedToAll"        |BLOCKED_TO_CREDIT|
  |02   |   "BlockedToAll"        |BLOCKED_TO_DEBIT |
  |03   |   "BlockedToAll"        |BLOCKED_TO_ALL    |
  | 04  |  "DormantAccount"       |DORMANT           |
  |05   |   "LienAccount"        |LIEN              |
