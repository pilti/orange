
Feature: This feature is to test Payment submission business validation failures by Database Manipulation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  @f1
  Scenario: verify that Status of payment submission API is REJECTED when account status is BlockedToALL
    * def info = karate.info
    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')

    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

      #Incorporate Business validation failure through bLockedToAll status in SP_Shared
    * def AccountNo = user_details.PISP.G_User_2.AccountNo1
      #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
      #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "BLOCKED_TO_ALL ='1'"
    * def setBlockedAcc = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    * print "after update"
    * print setBlockedAcc
      #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.response.Data.Status == "Rejected"
    And match apiApp.Payment_Submission.responseStatus == 201
    And set Result.DefectID = '1657'

    #revert database Changes
    * string a = "BLOCKED_TO_ALL ='0'"
    * def setBlockedAcc = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    * print setBlockedAcc

  @f2
  Scenario: verify that Status of payment submission API is REJECTED when IBAN is invalid
    * def info = karate.info
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    # for IBAN validation, payload creditor info should have IBAN
    * set pisp_payload.Data.Initiation.CreditorAccount =
   """
  {
  "SchemeName": "IBAN",
  "Identification": "GB97BOFI90210069542561",
  "Name": "PayeeBlocktoDebit"
  }
  """
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_3.user)',"otp":'#(user_details.PISP.G_User_3.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Incorporate Business validation failure through bLockedToAll status in SP_Shared
    * def AccountNo = user_details.PISP.G_User_2.AccountNo1
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * def Acc_Details = ABT_DB.get_ABT_Details(NSC,AcNo,ABT_DB_Conn)
    * json iban_temp = Acc_Details
    * def iban_original = iban_temp.IBAN
    * def iban_modified = iban_original + 'Xy'
    * def update_Details = ABT_DB.update_ABT_Details(NSC,AcNo,ABT_DB_Conn,"IBAN ='"+iban_modified+"'")
    * def Acc_Details = ABT_DB.get_ABT_Details(NSC,AcNo,ABT_DB_Conn)
    * print Acc_Details

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.response.Status == "Rejected"
    And match apiApp.Payment_Submission.responseStatus == 201

    # revert database changes
    * def update_Details = ABT_DB.update_ABT_Details(NSC,AcNo,ABT_DB_Conn,"IBAN ='"+iban_original+"'")
    * def Acc_Details = ABT_DB.get_ABT_Details(NSC,AcNo,ABT_DB_Conn)
    * print Acc_Details.IBAN

  Scenario: Verify response is correct when payment Submission Request created with invalid Currency
    * def info = karate.info
    * def dbPISP = Java.type('CMA_Release.Entities_DB.PAYMENT_REQUEST')

    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    * def AccountNo = user_details.PISP.G_User_2.AccountNo1
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * def Payment_Details = dbPISP.get_PaymentDetails(NSC,AcNo,PSD2_PISP_PID_Conn)
    * json Currency_temp = Payment_Details
    * def Currency_original = Currency_temp.PAYER_CUR
    * def Currency_modified = Currency_original.substring(0,1) + 'X'
    * def update_Details = dbPISP.update_PaymentRequest_Details(NSC,AcNo,PSD2_PISP_PID_Conn,"PAYER_CUR ='"+Currency_modified+"'")
    * def Payment_DetailsUpdated = dbPISP.get_PaymentDetails(NSC,AcNo,PSD2_PISP_PID_Conn)
    * print Payment_DetailsUpdated

  #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response.Data.Status = 'Rejected'

    # revert database changes
    * def update_Details = dbPISP.update_PaymentRequest_Details(NSC,AcNo,PSD2_PISP_PID_Conn,"PAYER_CUR ='"+Currency_original+"'")
    * def Pay_Details = dbPISP.get_PaymentDetails(NSC,AcNo,PSD2_PISP_PID_Conn)
    * print Pay_Details



