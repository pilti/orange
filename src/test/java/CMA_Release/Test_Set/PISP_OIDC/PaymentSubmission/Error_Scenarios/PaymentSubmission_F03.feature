@PISP
Feature: This feature is to test Payment submission validations

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.PISP
    * def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @PISP_Submission_Error01
  Scenario: Verification of Payment submission service for x-idempotency-key_Blank
    * def info = karate.info

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details.idempotency_value = ""
    And set Application_Details.idempotency_key = ""
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    When call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400


  @PISP_Submission_Error02
  Scenario: Verification of Payment submission service for x-idempotency-key_duplicate
    * def info = karate.info

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'

    And set Application_Details.idempotency_key = "935ff361-64ff-46ad-900a-a9a65d3a9bb0"
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    When call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400

  #Positive Scenario- 201 as response
  @PISP_Submission_Error03
  Scenario: Verification of Payment submission service for x-idempotency-key_Not included in header
    * def info = karate.info
    * def request_method = 'POST'
         #tpp credentials
    * json tpp_credentials = {"client_id": '#(Application_Details.client_id)',"client_secret": '#(Application_Details.client_secret)'}

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And set pisp_reqHeader $.x-fapi-interaction-id = Java.type('CMA_Release.Java_Lib.RandomValue').random_id()
    And set pisp_payload $.Data.PaymentId = Application_Details.PaymentId

    #Payment Submission
    And url PmtSubUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header x-idempotency-key = Application_Details.idempotency_key
    And header Authorization = 'Bearer ' + apiApp.Access_Token_ACG.response.access_token
    When method request_method
    Then match responseStatus == 201


    #For Payload structure not valid - added one more field in pisp_payload.Data.Initiation.InstructedAmount as Error =123
  @PISP_Submission_Error04
  Scenario: Verification of Payment submission service for Payload structure not valid
    * def info = karate.info
    * def request_method = 'POST'
       #tpp credentials
    * json tpp_credentials = {"client_id": '#(Application_Details.client_id)',"client_secret": '#(Application_Details.client_secret)'}

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And set pisp_payload.Data.Initiation.InstructedAmount.Error = '123'
    And print Application_Details
    When set pisp_reqHeader $.x-fapi-interaction-id = Java.type('CMA_Release.Java_Lib.RandomValue').random_id()
    Then set pisp_payload $.Data.PaymentId = Application_Details.PaymentId

  #Payment Submission
    Given url PmtSubUrl
    And request pisp_payload
    And headers pisp_reqHeader
    And headers tpp_credentials
    And header x-idempotency-key = Application_Details.idempotency_key
    And header Authorization = 'Bearer ' + apiApp.Access_Token_ACG.response.access_token
    When method request_method
    Then match responseStatus == 400


  @PISP_Submission_Error05
  Scenario: Verification of Payment submission service for PaymentID not matching with payment setup payload
    * def info = karate.info

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)

   #Payment Submission
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details.PaymentId = "FSPID_201808171434170009270"
    And print "**&&!!" + Application_Details.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 403

  @PISP_Submission_Error06
  Scenario: Verification of Payment submission service for Payload with Amount mismatch
    * def info = karate.info

    Given call apiApp.PreReq_PaymentSubmission(Application_Details)

   #Payment Submission
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.Initiation.InstructedAmount.Amount = '99.99'
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400

  @PISP_Submission_Error07
  Scenario: Verification of Payment submission service for Payload with Currency mismatch
    * def info = karate.info

    Given json Application_Details = active_tpp.PISP
    And call apiApp.PreReq_PaymentSubmission(Application_Details)

 #Payment Submission
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    When set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.Initiation.InstructedAmount.Currency = 'EUR'
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 400