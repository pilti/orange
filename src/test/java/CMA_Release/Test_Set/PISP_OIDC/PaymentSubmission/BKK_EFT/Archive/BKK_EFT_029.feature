@BKK_EFT
Feature: Payment E2E

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
    * def Application_Details = {}


  Scenario Outline: PISP_BKK_EFT_001

    * def info = karate.info
    * set info.subset = "PISP_BKK_EFT_001"

    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given json Application_Details = active_tpp.AISP_PISP
    And set pisp_payload.Data.Initiation.InstructedAmount.Amount = '<amount>'
    And remove pisp_payload.Data.Initiation.RemittanceInformation.Reference
    And set pisp_payload.Data.Initiation.CreditorAccount =
      """
      {
        "SchemeName": "SortCodeAccountNumber",
        "Identification":'<C_Account>',
        "Name": "Current Account - Long Account Name for GB Account Test 1234",
        "SecondaryIdentification":'<C_SecondaryIdentification>'
      }
      """
    And set pisp_payload.Data.Initiation.DebtorAccount =
      """
      {
        "SchemeName": "SortCodeAccountNumber",
        "Identification":'<D_Account>',
        "Name": "Current Account - Long Account Name for GB Account Test 1234",
        "SecondaryIdentification":'<D_SecondaryIdentification>'
      }
      """
    And def consentData = {"usr":'<user>',"otp":'123456'}
    When call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.token_type = "Bearer"
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response

    Examples:
      | user   | amount | D_Account      | D_SecondaryIdentification | C_Account      | C_SecondaryIdentification | Reference |
      | 986676 | 5.12   | 90029515349571 | Debtor5.12SecIden         | 90239710344150 | EFSEC5.12NItoNICreITIONAL |           |
