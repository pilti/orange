
Feature: CR_10 Positive Payment Submission

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def CHANNEL_PROFILE_DB = Java.type('CMA_Release.Entities_DB.CHANNEL_PROFILE_DB')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  Scenario: Validation of 201 for invalid Account ID

    * def info = karate.info

    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
#    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    * def acc = user_details.PISP.P_User_2.AccountNo1
    * def accno = acc.substr(6,acc.length())
    * def NSC = acc.substring(0,6)
    * def record_detail = CHANNEL_PROFILE_DB.get_Channel_Profile(consentData.usr,CHANNEL_PROFILE_DB_Conn)
    * print record_detail
    * print accno

    * def perform_update = CHANNEL_PROFILE_DB.update_Channel_Profile(NSC,accno,CHANNEL_PROFILE_DB_Conn,"accno = '1234'")
    * print record_detail
#    * print perform_update

#    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token

#    * def query = "'" + IBAN + "'"
#    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,'IBAN = ' + query)
#    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
#    * print record_detail

#    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
#    And set Application_Details $.request_method = 'POST'
#    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
#    And print Application_Details
#    And call apiApp.Payment_Submission(Application_Details)
##    Then match apiApp.Payment_Submission.response.Status == "Created"
#    Then match apiApp.Payment_Submission.responseStatus == 201