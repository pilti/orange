@PISP

Feature: This feature is to test Payment submission Id validations

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  @31
  Scenario: verify length of submission Id generated in payment submission
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    And def ID = apiApp.Payment_Submission.response.Data.PaymentSubmissionId
    And def size = ID.length()
    And match size == 27

  @32
  Scenario: verify that Payment Id and Submission Id are different
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    And match apiApp.Payment_Submission.response.Data.PaymentSubmissionId != Application_Details.PaymentId

  @33
  Scenario: verify SubmissionID returned during PaymentSubmissionAPI is same as that created during PaymentSetupAPI
    * def info = karate.info
    * def dbFunctions = Java.type('CMA_Release.Entities_DB.PAYMENT_REQUEST')

    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)
    * print "Printsssss"
    #Query Payment Request DB to fetch Payment_Submission_ID created during payment_setup
    And def query = dbFunctions.get_PaymentDetails(Application_Details.PaymentId,PSD2_PISP_PID_Conn)
    * json query = query
    And print query[0]
    And def PaymentSetup_PaymentSubmissionID = query[0].PYMT_SUBMISSION_ID
#
    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    And print PaymentSetup_PaymentSubmissionID
    And match PaymentSetup_PaymentSubmissionID == apiApp.Payment_Submission.response.Data.PaymentSubmissionId

