@PISP

Feature: This feature is to test Payment submission Id validations

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  @21
  Scenario: Verify that same Submission ID is obtained in all Payment Submission requests after first hit
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response

    And def PaymentSubmissonID = apiApp.Payment_Submission.response.PaymentSubmissionId
    And set Result.ActualOutput.Payment_Submission_Call_1.Output.PaymentSubmissonID =  apiApp.Payment_Submission.response.PaymentSubmissionId

    And def apiApp1 = new apiapp()
    Then call apiApp1.Payment_Submission(Application_Details)
    And match apiApp1.Payment_Submission.responseStatus == 201
    And def PaymentSubmissonID1 = apiApp1.Payment_Submission.response.PaymentSubmissionId
    And set Result.ActualOutput.Payment_Submission_Call_2.Output.PaymentSubmissonID1 =  apiApp.Payment_Submission.response.PaymentSubmissionId

    And def apiApp2 = new apiapp()
    Then call apiApp2.Payment_Submission(Application_Details)
    And match apiApp2.Payment_Submission.responseStatus == 201
    And def PaymentSubmissonID2 = apiApp2.Payment_Submission.response.PaymentSubmissionId
    And set Result.ActualOutput.Payment_Submission_Call_3.Output.PaymentSubmissonID2 =  apiApp.Payment_Submission.response.PaymentSubmissionId

    And match PaymentSubmissonID == PaymentSubmissonID1
    And match PaymentSubmissonID == PaymentSubmissonID2

  @22
  Scenario: verify that resource status is AcceptedSettlementInProcess after payment submission API is executed successfully
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    And match apiApp.Payment_Submission.response.Data.Status == 'AcceptedSettlementInProcess'

  @23
  Scenario Outline: verify 201 response when Accept optional #key# field is passed in payment submission API
    * def info = karate.info
    Given json Application_Details = active_tpp.PISP
    And def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    Then call apiApp.PreReq_PaymentSubmission(Application_Details)

    #Payment Submission
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And set pisp_reqHeader $.Accept = <value>
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == <error>
    And print "seeee"
    And print apiApp.Payment_Submission.response
  Examples:
  |value                          |key|error|
   | "application/json"              |valid|201|
