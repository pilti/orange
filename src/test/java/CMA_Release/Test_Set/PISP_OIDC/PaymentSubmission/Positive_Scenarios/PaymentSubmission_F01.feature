@PISP

Feature: This feature is to test Payment submission validations

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.PISP
    * def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)'}
    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    #Authorisation_valid, x-fapifinancial_valid, Content-Type_valid, x-idempotency-key_valid and unique
  @PISP_Submission_mandatory
   Scenario: verify that payment submission API is executed successfully for mandatory Header parameters - all Valid
    * def info = karate.info

    Given json Application_Details = active_tpp.PISP
    And call apiApp.PreReq_PaymentSubmission(Application_Details,consentData)

    #Payment Submission
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    When set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.responseHeaders
    And match apiApp.Payment_Submission.response.Data.Status == 'AcceptedSettlementInProcess'

  @PISP_Submission_optional
  Scenario Outline: verify that payment submission API is executed successfully with optional header parameters #key#
    * def info = karate.info
    * def key = '<key>'
    * set info.key = key

    Given json Application_Details = active_tpp.PISP
    And call apiApp.PreReq_PaymentSubmission(Application_Details,consentData)

    #Payment Submission
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    When set pisp_reqHeader $.<Field> = <value>
    And set Application_Details $.request_method = 'POST'
    And set pisp_payload.Data.PaymentId = Application_Details.PaymentId
    And print Application_Details
    And call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And print apiApp.Payment_Submission.response
    #And print apiApp.Payment_Submission.responseHeaders
    And match apiApp.Payment_Submission.response.Data.Status == 'AcceptedSettlementInProcess'

  Examples:
  | Field                            | value                          |key|
  | x-fapi-customer-last-logged-time | "Sun, 10 Sep 2017 19:43:31 UTC"| Logged time_1|
  | x-fapi-customer-last-logged-time | "Sun, 10-10-2017 19:43:31 IST" | Logged time_2|
  | x-fapi-customer-last-logged-time | ""                             | Logged time_3|
  | x-fapi-customer-last-logged-time | "10-10-2017"                   | Logged time_4|
  | x-fapicustomerip-address        | "127.10.101.25"                  | IPaddress_1  |
  | x-fapicustomerip-address        | "1050.10.101.288"                | IPaddress_2  |
  | x-fapicustomerip-address       | ""                               | IPaddress_3  |
  | x-fapicustomerip-address       | "127.10"                         | IPaddress_4  |
  | x-fapiinteractionid             | "9d22905f-7e60-495b-a7cc-762a5eee4334"| Interaction Id_1  |
  | x-fapiinteractionid             | "9d22905762a5eee4334"                  | Interaction Id_2  |
  | x-fapiinteractionid             | ""                   | Interaction Id_3 |
  | x-fapiinteractionid             | "9d22905f--495b--"                    | Interaction Id_4  |


