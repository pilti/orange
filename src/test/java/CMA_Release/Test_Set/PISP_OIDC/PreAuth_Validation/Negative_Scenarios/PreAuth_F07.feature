@PISP1
@PISP_API
@PISP_PreAuthValidation


Feature: This feature is to test PreAuth validation for Intent ID parameter

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);
       webApp.stop();
       }
      """

  Scenario Outline: TC#key#_Verify that error message is received on UI when Intent ID for which consent is provided is passed in request object payload

    * def info = karate.info
    * set info.key = '<Count>'
    * set info.Screens = 'Yes'
    * set info.subset = "IntentID for which consent is provided"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.<TPPRole>
    When call apiApp.configureSSL(Application_Details)
    And call apiApp.PreReq_PaymentSubmission(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200
    And match apiApp.Payment_Setup.response.Data.Status = 'AcceptedTechnicalValidation'

    #Create request object for Intent ID for which consent is provided and access token is generated
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    And print ReqObjIP
    When call apiApp1.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt =  apiApp1.CreateRequestObject.jwt.request
    When call apiApp1.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp1.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
    Then def perform = oidc_AISP_Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl
    And set Result.Additional_Details = Application_Details
    And match perform.ActualUrl == <ExpectedURL>


    Examples:
      | Count | TPPRole | ExpectedURL                                                                                                         |
      | 001   | PISP    | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |


  Scenario Outline: TC#key#_Verify that error message is received on UI when Intent ID having Rejected Status is provided is passed in request object payload

    * def info = karate.info
    * set info.key = '<Count>'
    * set info.Screens = 'Yes'
    * set info.subset = "Rejected Intent ID passed in request object"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.<TPPRole>
    When call apiApp.configureSSL(Application_Details)

    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #PaymentSetup
    #Create payment ID with SEPA account which will give us payment ID in Rejected Status
    #Setting up PISp payload with SEPA account details
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set pisp_payload $.Data.Initiation.CreditorAgent = {"SchemeName": "BICFI","Identification": "BOFIIE2DXXX"}
    And set pisp_payload $.Data.Initiation.CreditorAccount.SchemeName = "IBAN"
    And set pisp_payload $.Data.Initiation.CreditorAccount.Identification = "IE29BOFI90001718228011"
    And set pisp_payload $.Data.Initiation.CreditorAccount.Name = "Joint account"
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

        #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt =  apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.<TPPRole>.client_id
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
    And def perform = oidc_AISP_Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl
    Then match perform.ActualUrl == <ExpectedURL>

    Examples:
      | Count | TPPRole | ExpectedURL                                                                                                         |
      | 001   | PISP    | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |

