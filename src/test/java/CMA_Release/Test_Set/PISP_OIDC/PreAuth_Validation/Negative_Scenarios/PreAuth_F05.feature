@PISP1
@PISP_API
@PISP_PreAuthValidation

Feature: This feature is to test PreAuth validation for request Object and scope parameter

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  Scenario Outline: TC#key#_Verify that Error message is displayed on UI browser when request object validation fails

    * def info = karate.info
    * set info.key = '<Count>'
    * set info.Screens = 'Yes'
    * set info.subset = "Request Object validation"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given json Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)
    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201
     #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details $.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = <Request_object>

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.PISP.client_id
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text)
    And def ExpectedUIErrorMessage = 'Error:'
    Then match ActualText == ExpectedUIErrorMessage
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage
    Examples:
      | Count | Request_object                               |
      | 001   | ' '                                          |
      | 002   | apiApp.CreateRequestObject.jwt.request + '/' |
      | 003   | 'null'                                       |

  @Query_1634
  Scenario Outline: TC#key#_Verify that Error message details are sent to TPP in callback URL whn scope validation fails while request object creation

    * def info = karate.info
    * set info.key = '<count>'
    * set info.Screens = 'Yes'
    * set info.subset = "Scope validation during request object creation"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.<TPPRole>
    When call apiApp.configureSSL(Application_Details)
    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = <scope>
    And set Application_Details $.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.<TPPRole>.client_id
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And def perform = oidc_AISP_Functions.getUrl(webApp)
    Then match perform.ActualUrl == <Expected>

    Examples:
      | count | TPPRole   | scope                      | Expected                                                                                                                                                                          |
      | 001   | PISP      | 'openid accounts'          | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | 002   | PISP      | ' '                        | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | 003   | PISP      | 'openid accounts payments' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | 004   | PISP      | 'openid payments accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | 005   | PISP      | 'abcd'                     | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | 006   | PISP      | 'openid'                   | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | 007   | PISP      | 'payments'                 | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | 008   | AISP_PISP | 'openid accounts'          | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'                                                               |


  Scenario Outline: TC#key#_Verify that error message is not received while launching SCA URL when scope is modified while creating SCA URL

    Given def info = karate.info
    And set info.key = '<count>'
    And set info.Screens = 'Yes'
    And set info.subset = "Scope validation in query parameter"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.<TPPRole>

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

    #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = <JWTscope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    And print ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <QueryParameterScope>
    And set Application_Details $.client_id = active_tpp.<TPPRole>.client_id

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    * print Result


    Examples:
      | count | TPPRole | JWTscope          | QueryParameterScope        |
      | 001   | PISP    | 'openid payments' | 'openid'                   |
      | 002   | PISP    | 'openid payments' | ' '                        |
      | 003   | PISP    | 'openid payments' | 'payments'                 |
      | 004   | PISP    | 'openid payments' | 'openid accounts'          |
      | 005   | PISP    | 'openid payments' | 'null'                     |
      | 006   | PISP    | 'openid payments' | 'abcd'                     |
      | 007   | PISP    | 'openid payments' | 'openid accounts payments' |

