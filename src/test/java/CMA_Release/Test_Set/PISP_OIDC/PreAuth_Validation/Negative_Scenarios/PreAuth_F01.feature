@PISP_neg
@PISP_API
@PISP_PreAuthValidation

Feature: This feature is to test PreAuth validation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  Scenario: Verify that URL launch is successful when URL is constructed after passing the valid value of payment request ID in both the fields of openbaking_intent_id  providing all other valid values

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = "PreAuth Validation-Correct intentIDs in the request"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

     #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.scope = 'openid payments'
    * set Application_Details $.scope = 'openid payments'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    Given def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page


  Scenario: Verify that SCA URL launch is successful when valid parameters are passed while request object creation and query parameter

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = "PreAuth Validation-valid parameters"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.AISP_PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

   #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.scope = 'openid payments'
    * set Application_Details $.scope = 'openid payments'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    Given def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page


  Scenario: Verify that SCA URL launch is successful when correct signing key is passed in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = "PreAuth Validation-correct signing key"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.AISP_PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

 #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.scope = 'openid payments'
    * set Application_Details $.scope = 'openid payments'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    Given def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page


  Scenario Outline: TC#key#_Verify that SCA URL launch is successful when invalid intent_id is passed under userInfo claim

    Given def info = karate.info
    And set info.key = '<count>'
    And set info.Screens = 'Yes'
    And set info.subset = "PreAuth Validation-Invalid intent_id under userinfo claim"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.AISP_PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

#Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = <intent_id>
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.scope = 'openid payments'
    * set Application_Details $.scope = 'openid payments'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    Given def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Examples:
    |count|intent_id|
    |001  |' '      |
    |002  |'abcd'   |
