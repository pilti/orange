@PISP_neg
@PISP_API
@PISP_PreAuthValidation


Feature: This feature is to test PreAuth validation for invalid client id value

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """




  Scenario Outline: TC#key#_Verify that Error message is displayed on UI browser when invalid client_id is passed in payload for request object creation

    Given def info = karate.info
    And set info.key = '<count>'
    And set info.Screens = 'Yes'
    And set info.subset = "Client Id validation in request object"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

     #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = <client_id>
    And set ReqObjIP $.payload.client_id = <client_id>
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details $.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    And print ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.PISP.client_id

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    And def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
    And set Result.Additional_Details = Application_Details

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Error:'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage


    Examples:
      | count | client_id                      |
      | 001   | active_tpp.AISP_PISP.client_id |
      | 002   | ' '                            |
      | 003   | 'abcd '                        |

  Scenario Outline: TC#key#_Verify that Error message is displayed on UI browser when invalid client_id is passed in query parameter

    Given def info = karate.info
    And set info.key = '<count>'
    And set info.Screens = 'Yes'
    And set info.subset = "Client Id validation in query parameter"
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path =  info1.path
    And def path = call  webApp.path1 = path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.PISP

    And def TestRes = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/GetPaymentId.feature') Application_Details

       #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    * set ReqObjIP $.payload.scope = 'openid payments'
    * set Application_Details $.scope = 'openid payments'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    * print ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * set Application_Details $.client_id = <client_id>

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    And def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
    And set Result.Additional_Details = Application_Details


    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Error:'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage


    Examples:
      | count | client_id                      |
      | 001   | active_tpp.AISP_PISP.client_id |
      | 002   | ' '                            |
      | 003   | 'abcd'                        |
      | 004   | 'null'                       |

