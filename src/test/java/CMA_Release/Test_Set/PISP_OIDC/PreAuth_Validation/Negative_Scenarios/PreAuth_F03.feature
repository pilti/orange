@PISP1
@PISP_API
@PISP_PreAuthValidation

Feature: This feature is to test PreAuth validation for Signing key parameter

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')


    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  Scenario: Verify that Error message is displayed on UI browser when signing key of different TPP is passed in payload for request object creation

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Signing key validation-Invalid signing key"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    Given json Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)

    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

     #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And def Application_Details_dummy = active_tpp.AISP
    And def path = stmtpath(Application_Details_dummy) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.PISP.client_id
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text)
    And def ExpectedUIErrorMessage = 'Error:'
    Then match ActualText == ExpectedUIErrorMessage
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage


  Scenario: Verify that Error message is displayed on UI browser when signing key is not passed in payload for request object creation

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Signing key validation-Missing signing key"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)
    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details.path = ' '
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.client_id = active_tpp.PISP.client_id
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Fail'
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text)
    And def ExpectedUIErrorMessage = 'Error:'
    Then match ActualText == ExpectedUIErrorMessage
    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And print ActualText
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage

