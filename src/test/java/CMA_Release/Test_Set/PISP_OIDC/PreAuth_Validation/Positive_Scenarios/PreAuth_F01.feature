@PISP_pos
@PISP_API
@PISP_PreAuthValidation

Feature: This feature is to test PreAuth validation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


#  Scenario: Verify that SCA URL launch is successful when valid parameters are passed while request object creation and query parameter
#
#
#    * def info = karate.info
#    * set info.Screens = 'Yes'
#    * set info.subset = "PreAuth Validation-Correct intentIDs in the request"
#    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
#    * def path =  info1.path
#    * def path = call  webApp.path1 = path
#
#    Given json Application_Details = active_tpp.AISP_PISP
#    When call apiApp.configureSSL(Application_Details)
#
#    #CCG
#    Given set Application_Details.request_method = 'POST'
#    And set Application_Details $.grant_type = 'client_credentials'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#    When call apiApp.Access_Token_CCG(Application_Details)
#    Then match apiApp.Access_Token_CCG.responseStatus == 200
#
#    #PaymentSetup
#    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
#    When call apiApp.Payment_Setup(Application_Details)
#    Then match apiApp.Payment_Setup.responseStatus == 201
#
#   #Set input parameters for CreateRequestObject
#    Given set ReqObjIP $.header.kid = Application_Details.kid
#    And set ReqObjIP $.payload.iss = Application_Details.client_id
#    And set ReqObjIP $.payload.client_id = Application_Details.client_id
#    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
#    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
#    And set ReqObjIP $.payload.scope = 'openid payments'
#    And set Application_Details $.scope = 'openid payments'
#    And def path = stmtpath(Application_Details) + 'signin_private.key'
#    And set Application_Details.path = path
#    When call apiApp.CreateRequestObject(Application_Details)
#    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
#
#    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
#    And set Application_Details $.response_type = 'code id_token'
#    And set Application_Details $.state = 'af0ifjsldkj'
#    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
#    When call apiApp.ConstructAuthReqUrl(Application_Details)
#    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
#
#    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
#    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
#    Then match perform.Status == 'Pass'

  Scenario: Verify that SCA URL launch is successful when correct signing key is passed in request object

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "PreAuth Validation-Correct intentIDs in the request"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    #CCG
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #PaymentSetup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Payment_Setup.response.Data.PaymentId
    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details $.scope = 'openid payments'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'