@PISP_Consent
Feature: This feature is to perform validation on Account Selection Page

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
    * def callpreReq = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj_Acc_sel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account')

    * configure afterScenario =
     """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  Scenario: TC022_PISP Consent_Account Selection Screen_Session_timeout

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC022_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And print "Wait Applied"
    And def applywait = Java.type('CMA_Release.Java_Lib.Wait').delay(300000)
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def checkElementPresent =  Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.timeout)
    And print checkElementPresent


  Scenario: TC019_PISP Consent_Account Selection Screen_Verify_error_clicked_browser_refresh_button

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC019_PISP Consent_Account Selection Screen_Verify_error_clicked_browser_refresh_button"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def refresh =  Reuseable.browserNavigations(webApp,"refresh")
    And def checkElePresent =  Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.refreshErr)
    And match checkElePresent == true

  Scenario: TC015_PISP Consent_Account_Selection_Screen_URL_copy_new_tab
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC010_PISP Consent_Review screen_Verify_error_page_customer_cancelled_application_UI_launch_URL"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def AccntselUrl = Reuseable.getCurrentURL(webApp)
    And def geturl = Reuseable.NewTabRobot()
    And def geturl = Reuseable.SwitchfocustoLatestWindow(webApp.driver1)
    And def perform = oidc_AISP_Functions.launchConsentURL(webApp,AccntselUrl)
    And def getelement = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.refreshErr)
    And match getelement == true

  @Consent
  Scenario: TC021_PISP Consent_Account_Selection_Screen_URL_copy_new_Window
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC021_PISP Consent_Account_Selection_Screen_URL_copy_new_Window"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def AccntselUrl = Reuseable.getCurrentURL(webApp)
    And def geturl = Reuseable.NewWindowRobot()
    And def geturl = Reuseable.SwitchfocustoLatestWindow(webApp.driver1)
    And def perform = oidc_AISP_Functions.launchConsentURL(webApp,AccntselUrl)
    And def getelement = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.refreshErr)
    Then match getelement == true