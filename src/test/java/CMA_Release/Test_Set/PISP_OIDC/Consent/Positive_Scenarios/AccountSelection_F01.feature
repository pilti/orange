@PISP_Consent
Feature: Test feature for PISP Consent Account Selection Screen tests

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsAccount_Selection = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account')
    * def ObjectLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Login')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')

    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

# call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    * json Application_Details = active_tpp.AISP_PISP
    * def callpreReq = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature')
    * def curl = callpreReq.Application_Details.consenturl

   ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(Application_Details)

    #Launch consent renewal screen
    * def c = call  webApp.driver1 = webApp.start1(user_details.PISP.G_User_1.browser)
    * def path = call  webApp.path1 = info1.path
    * def perform = Functions.launchConsentURL(webApp,curl)
    * print perform
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    * match perform.Status == 'Pass'

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }

      """

  @PISP_Consent_TS1
  Scenario: Verify that text as 'Please check the details......' is displayed below 'Account selection' page.
    * set info.Screens = 'Yes'
    * set info.subset = 'Text on Account selection page'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def element = ReusableFunctions.getElementText(webApp, ObjectsAccount_Selection.textBelowAccountSelection)
    Then match element == "Please check the details below are correct. Select 'Cancel' if you no longer wish to continue with this payment."


  @PISP_Consent_TS2
  Scenario: Verify that error message is displayed when user is not registered to 365 Online
    * set info.Screens = 'Yes'
    * set info.subset = 'No eligible accounts for this request'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def element = ReusableFunctions.getElementText(webApp, ObjectsAccount_Selection.invalidUserError)
    Then match element == "No eligible accounts for this request."


  @PISP_Consent_TS3
  Scenario: Verify the information under Payee details section.
    * set info.Screens = 'Yes'
    * set info.subset = 'Text on Account selection page'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def payeeInitiatedByText = ReusableFunctions.getElementText(webApp, ObjectsAccount_Selection.payeeInitiatedByText)
    And def payeeName = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.payeeNameText)
    And def amountText =  ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.amountText)
    And def referenceText = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.payeeReferenceText)
    And def payFromList = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.payFromList)
    And def payeeDetails = payeeName && amountText && referenceText && payFromList
    Then match payeeInitiatedByText == Application_Details.Application_Name
    And match payeeDetails == true

# TS4 - Need to check how we can verify that elements are not editable...
  @PISP_Consent_TS4
  Scenario: Verify that only Pay from dropdown list is editable under Payee details section.
    * set info.Screens = 'Yes'
    * set info.subset = 'Text on Account selection page'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def payeeInitiatedByText = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeInitiatedByText)
    And def payeeName = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeNameText)
    And def amountText =  ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.amountText)
    And def referenceText = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeReferenceText)
    And def payFromList = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payFromList)
    And def payeeDetails = payeeInitiatedByText && payeeName && amountText && referenceText && payFromList
    Then match payeeDetails == true

# need to check NI and GB
  @PISP_Consent_TS5
  Scenario: Verify that only  NI and GB current accounts are displayed in the pay from dropdown
    * set info.Screens = 'Yes'
    * set info.subset = 'Text on Account selection page'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def payeeInitiatedByText = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeInitiatedByText)
    And def payeeName = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeNameText)
    And def amountText =  ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.amountText)
    And def referenceText = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payeeReferenceText)
    And def payFromList = ReusableFunctions.CheckElementEnabled(webApp, ObjectsAccount_Selection.payFromList)
    And def payeeDetails = payeeInitiatedByText && payeeName && amountText && referenceText && payFromList
    Then match payeeDetails == true


  @PISP_Consent_TS6
  Scenario: Verify that continue button is disabled until debitor account is selected from pay from dropdown
    * set info.Screens = 'Yes'
    * set info.subset = 'Continue button is disabled'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def continueButton = ReusableFunctions.getvaluebyAttribute(webApp, ObjectsAccount_Selection.continue_button,"disabled")
    Then match continueButton == true

  
  @PISP_Consent_TS7
  Scenario: Verify that continue button is enabled if the debitor account number is selected by TPP
    * set info.Screens = 'Yes'
    * set info.subset = 'Continue button is enabled'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And match perform_selectFromAccount.Status == 'Pass'
    And def continueButton = ReusableFunctions.getvaluebyAttribute(webApp, ObjectsAccount_Selection.continue_button,"disabled")
    Then match continueButton == null


  @PISP_Consent_TS8
  Scenario: Verify that Review and confirm screen is displayed when user clicks on continue button
    * set info.Screens = 'Yes'
    * set info.subset = 'Review and confirm screen is displayed'
    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And def perform = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.continue_button,"Continue");
    And match perform.Status == 'Pass'
    And def reviewConfirm = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.review_confirm);
    Then match reviewConfirm == true

  @PISP_Consent_TS9
  Scenario: Verify that Cancel request pop up is displayed when user clicks on cancel button
    * set info.Screens = 'Yes'
    * set info.subset = 'Cancel request pop up is displayed'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And def perform = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_button,"Cancel")
    And match perform.Status == 'Pass'
    And def cancelPopup = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.cancel_popUp);
    Then match cancelPopup == true

  @PISP_Consent_TS10
  Scenario: Verify that Account Selection screen is displayed when user clicks on No button from Cancel request pop up
    * set info.Screens = 'Yes'
    * set info.subset = 'Account Selection screen is displayed'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And def perform = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_button,"Cancel");
    And match perform.Status == 'Pass'
    And def cancelPopup = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.cancel_popUp);
    And match cancelPopup == true
    And def cancelNo = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_No,"No");
    And def accountSelection = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.account_selection);
    Then match accountSelection == true

  @PISP_Consent_TS11
  Scenario: Verify that user is navigated to redirect url of TPP when clicked on Yes cancel from Cancel request pop up
    * set info.Screens = 'Yes'
    * set info.subset = 'Navigated to redirect url of TPP'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And def perform = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_button,"Cancel");
    And match perform.Status == 'Pass'
    And def cancelPopup = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.cancel_popUp);
    And match cancelPopup == true
    And def cancelYes = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_Yes,"Yes");
    And def returnToTPP = ReusableFunctions.getPageTitle(webApp);
    Then match returnToTPP == ""


  @PISP_Consent_TS12
  Scenario: Verify that user is navigated to redirect url of TPP when clicked on Yes cancel from Cancel request pop up
    * set info.Screens = 'Yes'
    * set info.subset = 'Account Selection screen is displayed'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,data)
    And def perform = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_button,"Cancel");
    And match perform.Status == 'Pass'
    And def cancelPopup = ReusableFunctions.CheckElementPresent(webApp, ObjectsAccount_Selection.cancel_popUp);
    And match cancelPopup == true
    And def cancelYes = ReusableFunctions.ClickElement(webApp, ObjectsAccount_Selection.cancel_Yes,"Yes");
    And def returnToTPP = ReusableFunctions.getPageTitle(webApp);
    And def errorURL = ReusableFunctions.getCurrentURL(webApp);
    And def perform = Functions.launchConsentURL(webApp,curl)
    Then match perform.Status == 'Fail'

# In Complete
  @PISP_Consent_TS13
  Scenario: Verify that browser forward button is disabled on 'Account Selection' page
    * set info.Screens = 'Yes'
    * set info.subset = 'Account Selection - Forward button is disabled'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def urlBeforeForward = ReusableFunctions.getCurrentURL(webApp);
    And def perform = ReusableFunctions.browserNavigations(webApp,"forward");
    And def urlAfterForward = ReusableFunctions.getCurrentURL(webApp);
    Then match urlAfterForward == urlBeforeForward


  @PISP_Consent_TS14
  Scenario: Verify that error message is displayed when clicked on browser back button on this page
    * set info.Screens = 'Yes'
    * set info.subset = 'Account Selection - error message is displayed on Backward'

    Given def data = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def urlBeforeForward = ReusableFunctions.getCurrentURL(webApp);
    And def perform = ReusableFunctions.browserNavigations(webApp,"backward");
    And def urlAfterForward = ReusableFunctions.getCurrentURL(webApp);
    * print "Error**" + urlAfterForward
