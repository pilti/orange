@PISP_Consent
Feature: This feature is to perform validation on Review Confirm Page

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
    * def callpreReq = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj_Acc_sel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account')
    * def PageObj_Review_cnfrm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Review_confirm')

    * configure afterScenario =
     """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """



  Scenario: TC002_PISP_Consent_Review_Screen_Verify_Page_Title

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC002_Page_Title"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def PageTitle = Reuseable.getPageTitle(webApp)
    And print PageTitle
    Then match PageTitle == 'Consent - Bank of Ireland'

  Scenario: TC005_PISP Consent_Review Screen_Verify_confirm_payment_button_disabled

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC005_confirm_payment_button_disabled"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def chckEnabled = Reuseable.CheckElementEnabled(webApp,PageObj_Review_cnfrm.confirm_payment)
    Then match chckEnabled == false

  Scenario: TC006_PISP Consent_Review Screen_Verify_user_redirected_Account_selection_clicked_back_button

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC006_confirm_user_redirected_Account_selection_clicked_back_button"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def click_back = Reuseable.ClickElement(webApp,PageObj_Review_cnfrm.back_payment)
    And def getPage = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.continue_button)
    Then match getPage == true

  Scenario: TC007_PISP Consent_Review Screen_Verify_Cancel_pop_displayed

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC007_PISP Consent_Review Screen_Verify_Cancel_pop_displayed"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def click_back = Reuseable.ClickElement(webApp,PageObj_Review_cnfrm.cancel_Button)
    And def check_present = Reuseable.CheckElementPresent(webApp,PageObj_Review_cnfrm.cancel_popUp)
    And match check_present == true


  Scenario: TC008_PISP Consent_Review Screen_Verify_review_confirm_screen_displayed_user_clicks_No

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC008_PISP Consent_Review Screen_Verify_review_confirm_screen_displayed_user_clicks_No"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def click_back = Reuseable.ClickElement(webApp,PageObj_Review_cnfrm.cancel_Button)
    And def click_No = Reuseable.ClickElement(webApp,PageObj_Review_cnfrm.cancel_No)
    And def ElementPresent_back = Reuseable.CheckElementPresent(webApp,PageObj_Review_cnfrm.cancel_Button)
    And match ElementPresent_back == true
