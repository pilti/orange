@PISP_Consent
Feature: This feature is to perform validation on Review Confirm Page

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Testname = null
    * def callpreReq = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj_Acc_sel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account')
    * def PageObj_Review_cnfrm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Review_confirm')

    * configure afterScenario =
     """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  Scenario: TC012_PISP Consent_Review Screen_Verify_Payee_Details


    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC012_PISP Consent_Review Screen_Verify_Payee_Details"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def details_Acc_sel = Reuseable.Table_data(webApp,PageObj_Acc_sel.table)
    And def continue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def details_Review_confirm = Reuseable.Table_data(webApp,PageObj_Review_cnfrm.table_review)
    And match details_Acc_sel == details_Review_confirm

  Scenario: TC013_PISP Consent_Review Screen_Verify_user_redirected_Account_selection_clicked_browser_back

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC013_confirm_user_redirected_Account_selection_clicked_browser_back"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def click_back = Reuseable.browserNavigations(webApp,"backward")
    And def getPage = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.continue_button)
    Then match getPage == true

  Scenario: TC017_PISP Consent_Account Selection Screen_Session_timeout

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC017_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And print "Wait Applied"
    And def applywait = Java.type('CMA_Release.Java_Lib.Wait').delay(300000)
    And def click = Reuseable.ClickElement(webApp,PageObj_Review_cnfrm.back_payment)
    And def checkElementPresent =  Reuseable.CheckElementPresent(webApp,PageObj_Review_cnfrm.timeout)
    And print checkElementPresent


  Scenario: TC014_PISP Consent_Review Screen_Verify_error_browser_refresh_button

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC014_PISP Consent_Review Screen_Verify_error_browser_refresh_button"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def refresh =  Reuseable.browserNavigations(webApp,"refresh")
    And def checkElePresent =  Reuseable.CheckElementPresent(webApp,PageObj_Review_cnfrm.refreshErr)
    And match checkElePresent == true

  Scenario: TC004_PISP Consent_Review Screen_Verify_confirm_text_displayed

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC004_PISP Consent_Review Screen_Verify_confirm_text_displayed"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def getText =  Reuseable.getElementText(webApp,PageObj_Review_cnfrm.confirm_text)
    And def ischecked = Reuseable.Ischecked(webApp,PageObj_Review_cnfrm.confirm_chk)
    Then match getText == "I confirm and authorise the payment instruction as set out above."
    And match ischecked == false