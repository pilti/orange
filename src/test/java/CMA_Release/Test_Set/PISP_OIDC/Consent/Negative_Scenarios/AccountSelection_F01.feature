@PISP_Consent

Feature: This feature is to check error codes when certain combination in DB is changed

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
    * def callpreReq = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj_Acc_sel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_PISP.Select_Debit_Account')
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')

    * configure afterScenario =
     """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       //webApp.stop();
       }
      """


  Scenario: TC033_Error_PISP_API_Invalid IBAN length results 400 bad request

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC022_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":#'(user_details.PISP.P_User_2.user)',"otp":#'(user_details.PISP.P_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')

    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'


    * def acc = user_details.PISP.P_User_2.AccountNo1
    * def str = acc.substr(0,6)
    * def accno = acc.substr(6,acc.length())

    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'

    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * json record = record_detail.replace("[","").replace("]","")
    * def IBAN = record.IBAN
    * print IBAN

    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,"IBAN = 'GB87BOFI301027500911'")
    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def element_present = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.payerErr)

    And def query = "'" + IBAN + "'"
    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,'IBAN = ' + query)
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * print record_detail
    Then match element_present == true


  Scenario: TC035_Error_PISP_API_Invalid IBAN checksum results 400 bad request

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC022_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(user_details.PISP.P_User_2.user)',"otp":'#(user_details.PISP.P_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')

    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'

    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    * def acc = user_details.PISP.P_User_2.AccountNo1
    * def str = acc.substr(0,6)
    * def accno = acc.substr(6,acc.length())
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * json record = record_detail.replace("[","").replace("]","")
    * def IBAN = record.IBAN
    * print IBAN

    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,"IBAN = 'GB87BOFI301027500911XX'")
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * print "This is record detail after manipulation" + record_detail

    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def element_present = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.payerErr)

    And def query = "'" + IBAN + "'"
    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,'IBAN = ' + query)
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * print record_detail
    Then match element_present == true

  @abc
  Scenario: TC038_Error_PISP_API_BIC and IBAN do not match results 400 bad request

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "TC022_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(user_details.PISP.P_User_2.user)',"otp":'#(user_details.PISP.P_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')

    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'

    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    * def acc = user_details.PISP.P_User_2.AccountNo1
    * def str = acc.substr(0,6)
    * def accno = acc.substr(6,acc.length())
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * json record = record_detail.replace("[","").replace("]","")
    * def BIC = record.BIC
    * print BIC

    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,"BIC = 'CEFXBRSP'")
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * print "This is record detail after manipulation" + record_detail

    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def element_present = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.payerErr)

    And def query = "'" + BIC + "'"
    * def perform_update = ABT_DB.update_ABT_Details(str,accno,ABT_DB_Conn,'BIC = ' + query)
    * def record_detail = ABT_DB.get_ABT_Details(str,accno,ABT_DB_Conn)
    * print record_detail
    Then match element_present == true


  @abcd
  Scenario Outline: TC039_XX_Error_PISP_API_BIC and IBAN do not match results 400 bad request #key#

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = "TC022_PISP Consent_Account Selection Screen_Session_timeout"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def consentUrl = callpreReq.Application_Details.consenturl
    And def startconsent = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consentUrl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Given def consentData = {"usr":'#(user_details.PISP.P_User_2.user)',"otp":'#(user_details.PISP.P_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')

    And set Result.ActualOutput.UI.Output.user_name = consentData.usr
    And set Result.ActualOutput.UI.Output.otp = consentData.otp
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    And match perform_loginConsent.Status == 'Pass'

    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    * def acc = user_details.PISP.P_User_2.AccountNo1
    * def str = acc.substr(0,6)
    * def accno = acc.substr(6,acc.length())
    * def record_detail = SP_SHARED_DATA_DB.get_SP_Shared_Data(str,accno,SP_SHARED_DATA_DB_Conn)
    * print record

    * def perform_update = SP_SHARED_DATA_DB.update_SP_Shared_Data(str,accno,SP_SHARED_DATA_DB_Conn,<Query>)
    * def record_detail = SP_SHARED_DATA_DB.get_SP_Shared_Data(str,accno,SP_SHARED_DATA_DB_Conn)
    * print "This is record detail after manipulation" + record_detail

    And def clickContinue = Reuseable.ClickElement(webApp,PageObj_Acc_sel.continue_button)
    And def element_present = Reuseable.CheckElementPresent(webApp,PageObj_Acc_sel.payerErr)

    * def perform_update = SP_SHARED_DATA_DB.update_SP_Shared_Data(str,accno,SP_SHARED_DATA_DB_Conn,<Query1>)
    * def record_detail = SP_SHARED_DATA_DB.get_SP_Shared_Data(str,accno,SP_SHARED_DATA_DB_Conn)
    * print record_detail
    Then match element_present == true

    Examples:
      | Query                   | Query1                  | key   |
      | 'BLOCKED_TO_ALL = 1'    | 'BLOCKED_TO_ALL = 0'    | '001' |
      | 'BLOCKED_TO_DEBIT = 1'  | 'BLOCKED_TO_DEBIT = 0'  | '002' |
      | 'CREDIT_GRADING = 6'    | 'CREDIT_GRADING = 2'    | '003' |
      | 'CREDIT_GRADING = 7'    | 'CREDIT_GRADING = 2'    | '004' |
      | 'DORMANT = 1'           | 'DORMANT = 0'           | '005' |
      | 'LIEN = 1'              | 'LIEN = 0'              | '006' |


