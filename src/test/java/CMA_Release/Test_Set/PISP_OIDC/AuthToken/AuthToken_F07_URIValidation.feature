@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth
Feature: This feature validates the error status code if user inputs the invalid URI

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    ##############################################################################################################################################

  Scenario Outline: TC#key#_Verify that Status code 404 is displayed in the response when invalid URL is passed in API request

    Given def info = karate.info
    And def key = <count>
    And set info.subset = 'URI validation'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.AISP_PISP
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * print consentData
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details

    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"

    # change the api url based on env
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')

    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And def body =  "grant_type=" + Application_Details.grant_type  + "&code=" + Application_Details.code + "&redirect_uri=" + Application_Details.redirect_uri

    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 504

    Examples:
      | count | TokenUrl_SIT                                     | TokenUrl_UAT                                             |
      | '1'   | https://api.boitest.net/oauth/as/token           | https://api.u2.psd2.boitest.net/oauth/as/token           |
      | '2'   | https://api.boitest.net/oauth/as/oauth2          | https://api.u2.psd2.boitest.net/oauth/as/oauth2          |
      | '3'   | https://api.boitest.net/oauth/as/token.Oauth2    | https://api.u2.psd2.boitest.net/oauth/as/token.Oauth2    |
      | '4'   | https://api.boitest.net/oauth/as/Token.oauth2    | https://api.u2.psd2.boitest.net/oauth/as/Token.oauth2    |
      | '5'   | https://api.boitest.net/oauth/as//token.oauth2   | https://api.u2.psd2.boitest.net/oauth/as//token.oauth2   |
      | '6'   | https://api.boitest.net/oauth/token.oauth2       | https://api.u2.psd2.boitest.net/oauth/token.oauth2       |
      | '7'   | https://api.boitest.net/oauth//as/token.oauth2   | https://api.u2.psd2.boitest.net/oauth//as/token.oauth2   |
      | '8'   | https://api.boitest.net/as/token.oauth2          | https://api.u2.psd2.boitest.net/as/token.oauth2          |
      | '9'   | https://api.boitest.net/oauth/as/token.oauth2/// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/// |
      | '10'  | https://api.boitest.net/oauth/as/token.oauth2//  | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2//  |

  @Regression_19th_Nov
  Scenario Outline: TC#key#_Verify that access_token is generated successfully with 200 OK received in response when single extra forward slash is placed after domain name or at the end of URI

    Given def info = karate.info
    And def key = <count>
    And set info.subset = 'URI validation'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.AISP_PISP
  * def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
  * print consentData
  * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
  Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    # change the api url based on env
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And def body =  "grant_type=" + Application_Details.grant_type  + "&code=" + Application_Details.code + "&redirect_uri=" + Application_Details.redirect_uri

    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == <Expected>
    And match response contains {"access_token": '#string'}

    Examples:
      | count | TokenUrl_SIT                                   | TokenUrl_UAT                                           | Expected |
      | '11'   | https://api.boitest.net//oauth/as/token.oauth2 | https://api.u2.psd2.boitest.net//oauth/as/token.oauth2 | 200      |
      | '12'   | https://api.boitest.net/oauth/as/token.oauth2/ | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/ | 200      |


