@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth
Feature: This feature validates scope parameter to be sent in Access token using Auth code API request

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    ##############################################################################################################################################

  @PISP_testAPI
  Scenario Outline: TC#key#_Verify that error is not received when scope header is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Scope parameter validation'

    # code to get authcode for payment submission

    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * print consentData
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    # added scope parameter
    And set Application_Details $.scope = <scope>
    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>
    Examples:
      | count | TPPRole | scope         | Expected |
      | 001   | AISP_PISP | 'accounts'    | 200      |
      | 002   | AISP_PISP | 'payments123' | 200      |


#  @PISP_testAPI
#  Scenario Outline: TC#key#_Verify that error is not received when scope parameter is passed in the request body
#
#    Given def info = karate.info
#    And def key = '<count>'
#    And set info.subset = 'Scope parameter validation'
#
#  # code to get authcode for payment submission
#
#    * def Application_Details = active_tpp.<TPPRole>
#    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
#    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details
#
# # code to generate the access token using auth code
#    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
#    And set Application_Details $.code = AuthCode.performConsent.AuthCodeJson.code
#    When set Application_Details $.grant_type = "authorization_code"
#
#    And set Application_Details $.scope = <scope>
#    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
#    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + Application_Details.code + "&redirect_uri=" + Application_Details.redirect_uri + "&scope=" + Application_Details.scope
#    And set Application_Details $.request_body = body
#
#
#    Given url TokenUrl
#    And header Authorization = auth
#    And header Content-Type = "application/x-www-form-urlencoded"
#    And request body
#    When method POST
#    Then match responseStatus == <Expected>
#    Examples:
#      | count | TPPRole | scope         | Expected |
#      | 001   | AISP_PISP | 'accounts'    | 200      |
#      | 002   | AISP_PISP | 'payments123' | 400      |
