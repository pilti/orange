@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth

Feature: This feature validates the expiry duration of access token received in API response

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    ##############################################################################################################################################


  Scenario Outline: TC#key#_Verify that Access Token expiry should be mentioned as 298 or 299 or 300 secs in the response

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Access Token expiry'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * print consentData
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"
    And call apiApp.Access_Token_ACG(Application_Details)

    * print apiApp.Access_Token_ACG.response.expires_in
    * def expTime = [298,299,300]
    Then match expTime contains apiApp.Access_Token_ACG.response.expires_in

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |

  Scenario Outline: TC#key#_Validate Access Token using authcode API response as per CMA

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Access Token response validation'

     #################
    # Generate AuthCode

    * def Application_Details = active_tpp.<TPPRole>

    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"
    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.response contains  {"access_token": '#string'}
    And match apiApp.Access_Token_ACG.response.token_type == 'Bearer'

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |

  @checking

  Scenario Outline: TC#key#_Verify error is received when expired access token is used to invoke payment submissino API

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Access Token validation'

    #################
    # Generate AuthCode
    And def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"
    And call apiApp.Access_Token_ACG(Application_Details)
    And print "Wait starts"
    And def sleep = Resuable.sleep(301000)
    And print "Wait ends-Access token expired"

    And set pisp_payload.Data.PaymentId = Result.PaymentSetup.Output.response.Data.PaymentId
    And set Application_Details.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_ACG.response.token_type
    And set Application_Details.PaymentId = pisp_payload.Data.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    And match apiApp.Payment_Submission.responseStatus == 401

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |

  @checking
  Scenario Outline: TC#key#_Verify error is received when expired access token is used to invoke payment submission API

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Access Token validation'

    #################
    # Generate AuthCode
    And def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"
    And call apiApp.Access_Token_ACG(Application_Details)

    And print "Wait starts"
    And def sleep = Resuable.sleep(310000)
    And print "Wait ends-Access token expired"

    And set pisp_payload.Data.PaymentId = Result.PaymentSetup.Output.response.Data.PaymentId
    And set Application_Details.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_ACG.response.token_type
    And set Application_Details.PaymentId = pisp_payload.Data.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    And match apiApp.Payment_Submission.responseStatus == 401

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |

  Scenario Outline: TC#key#_Verify Code and id_token is available in the call back url generated post consent creation

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Call back URL validation'

   # Generate AuthCode and verify AuthCodeURL
    And def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    * def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |
