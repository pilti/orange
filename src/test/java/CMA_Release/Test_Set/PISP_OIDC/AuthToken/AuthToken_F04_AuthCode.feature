
@PISP_Consent
@PISP_AccessTokenUsingOAuth

Feature: This feature validates auth code

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')


    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webapp.stop();
       }
      """

    ##############################################################################################################################################

  Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when invalid or empty authCode is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Invalid or empty auth code'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * print consentData
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole | AuthCode     | Expected |
      | 001   | AISP_PISP | 'XCXCXCXCXC' | 400      |
      | 002   | AISP_PISP | ' '           | 400      |


  Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when consumed authCode is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Consumed auth code'

   #################
  # code to get authcode for payment submission
    And def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    And call apiApp.Access_Token_ACG(Application_Details)
    And set pisp_payload.Data.PaymentId = Result.Payment_Setup.Output.response.Data.PaymentId
    #And set Application_Details.idempotency_key = AuthCode.Result.ActualOutput.Payment_Setup.Input.idempotency_key
    And set Application_Details.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_ACG.response.token_type
    And set Application_Details.PaymentId = pisp_payload.Data.PaymentId
    And call apiApp.Payment_Submission(Application_Details)
    And match apiApp.Payment_Submission.responseStatus == 201

    And call apiApp1.Access_Token_ACG(Application_Details)
    Then match apiApp1.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole | Expected |
      | 001   | AISP_PISP | 400      |


  Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when authCode parameter is not passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Missing auth code'

    #################
   # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * print consentData
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

# code to generate the access token using auth code
# Authcode parameter is not sent in API request
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = "POST"
#    And set Application_Details $.code = <code>
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"
    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole |code| Expected |
      | 001   | AISP_PISP |null| 400      |


  Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when expired authCode is sent in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Expired Auth code'

 #################
# code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

# code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    * set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"


    #wait for 60 seconds to expire auth code and consume it after expiration
    And print "Wait starts"
    And def sleep = Resuable.sleep(60000)
    And print "Wait ends-Auth code expired"
    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole | Expected |
      | 001   | AISP_PISP | 400      |


  Scenario Outline: TC#key#_Verify that Status code 200 is displayed in the response when authCode which is about to expire is sent in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Consumed auth code'

#################
# code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

# code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

  #Cosume auth code after waiting for 45 secs
    And def sleep = Resuable.sleep(45000)
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole | Expected |
      | 001   | AISP_PISP | 200      |

 Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when valid authCode of another TPP is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Auth code not associated with originating TPP '

 #################
# Generate authcode using TPP1
    And def Application_Details = active_tpp.<TPP1Role>
   * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
   * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

# code to generate the access token using auth code
    And def Application_Details = active_tpp.<TPP2Role>
    And call apiApp.configureSSL(Application_Details)
   Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
   # And set Application_Details $.code = AuthCode.performConsent.AuthCodeJson.code
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"

   When call apiApp.Access_Token_ACG(Application_Details)
   Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count |TPP1Role| TPP2Role | Expected |
      | 001   |AISP_PISP    | PISP | 400      |



