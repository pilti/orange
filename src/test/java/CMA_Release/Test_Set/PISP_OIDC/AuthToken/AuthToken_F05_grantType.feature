@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth

Feature: This feature validates grant_type header

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    ##############################################################################################################################################
  @Regression_19th_Nov
  Scenario Outline: TC#key#_Verify that error is received when TPP has not provided valid value of mandatory parameter of grant_type

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'grant_type validation'

    # Get Auth code
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_1.user)',"otp":'#(user_details.PISP.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    When set Application_Details $.grant_type = <grant_type>
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"

    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole   | grant_type              | Expected |
      | 001   | AISP_PISP | ' '                     | 400      |
      | 002   | AISP_PISP | 'authorization_code123' | 400      |
      | 003   | AISP_PISP | null                    | 400      |
      | 004   | AISP_PISP | 'refresh_token'         | 400      |

  Scenario Outline: TC#key#_Verify that error is received when TPP has provided not allowed grant type client_credentails in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'grant_type validation'

    #Get Auth code
    And def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.grant_type = <grant_type>
    And set Application_Details $.scope = 'openid payments'

    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + Application_Details.code + "&redirect_uri=" + Application_Details.redirect_uri + "&scope=" + Application_Details.scope
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth

  # call Access_token using client_credentials grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST

    And set Result.ActualOutput.Access_Token_ACG.Input.TPPRole = 'AISP_PISP'
    And set Result.ActualOutput.Access_Token_ACG.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Access_Token_ACG.Input.HTTPMethod = Application_Details.request_method
    And set Result.ActualOutput.Access_Token_ACG.Input.scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_ACG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_ACG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_ACG.Input.AuthCode = Application_Details.code
    And set Result.ActualOutput.Access_Token_ACG.Input.grant_type = Application_Details.grant_type
    And set Result.ActualOutput.Access_Token_ACG.Input.redirect_uri = Application_Details.redirect_uri
    And set Result.ActualOutput.Access_Token_ACG.Output.responseStatus = responseStatus
    And set Result.ActualOutput.Access_Token_ACG.Output.response =  response
    Then match responseStatus == <Expected>

    And set Application_Details.access_token = response.access_token
    And set Application_Details.token_type = response.token_type

    And def apiApp1 = new apiapp()
    And call apiApp1.Payment_Setup(Application_Details)
    And set Result.ActualOutput.Payment_Setup.Input.access_token = response.access_token
    And set Result.ActualOutput.Payment_Setup.Input.token_type = response.token_type
    And set Result.ActualOutput.Payment_Setup.Output.responseStatus =  apiApp1.Payment_Setup.responseStatus
    And set Result.ActualOutput.Payment_Setup.Output.response =  apiApp1.Payment_Setup.response

    And match apiApp1.Payment_Setup.responseStatus == 201

    Examples:
      | count | TPPRole   | grant_type           | Expected |
      | 005   | AISP_PISP | 'client_credentials' | 200      |






