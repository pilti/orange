@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth
@run
Feature: This feature validates the error status code if user inputs the wrong client id or secret

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    ##############################################################################################################################################

  Scenario Outline: TC#key#_Verify that Status code 401 is displayed in the response when invalid client id or secret is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Client credentials validation'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    And set Application_Details.client_id = <clientId>
    And set Application_Details.client_secret = <clientSecret>
    And call apiApp.Access_Token_ACG(Application_Details)
    * print apiApp.Access_Token_ACG.response
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole   | clientId                 | Expected | clientSecret                       |
      | 001   | AISP_PISP | 'XXXXXXX'                | 401      | 'ZxarXMWZ9gOG9utcIzfUq7SHxvyh0Eby' |
      | 002   | AISP_PISP | '3LtmurZm2dIuPOvu8WBKMh' | 401      | 'ZxarXMWZ9gOG9utcIzfUq7SHxvyh0Eby' |
      | 003   | AISP_PISP | ''                       | 401      | 'ZxarXMWZ9gOG9utcIzfUq7SHxvyh0Eby' |
      | 004   | AISP_PISP | '5R85IgDTmYPZgkUDy3r2sJ' | 401      | 'XZXZXZXZXZXZX'                    |
      | 005   | AISP_PISP | '5R85IgDTmYPZgkUDy3r2sJ' | 401      | 'vxgYfafuHIyP8HmZBiJsrmFRx6PWYYeA' |
      | 006   | AISP_PISP | '5R85IgDTmYPZgkUDy3r2sJ' | 401      | ''                                 |
      | 007   | AISP_PISP | 'QWQWQWQWQWQW'           | 401      | 'QWQWQWQWQWQW'                     |
      | 008   | AISP_PISP | '3LtmurZm2dIuPOvu8WBKMh' | 401      | 'vxgYfafuHIyP8HmZBiJsrmFRx6PWYYeA' |
      | 009   | AISP_PISP | '29339sqDksgpE8opMzSgtS' | 401      | 'MZ740KtfQh64uXYW74unI5V4yYZroQwU' |
      | 010   | AISP_PISP | ''                       | 401      | ''                                 |


  Scenario Outline: TC#key#_Verify that Status code 400 is displayed in the response when Authorization header is not sent in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'Missing Authorization header'

  #################
 # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
    When set Application_Details $.grant_type = "authorization_code"
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + Application_Details.code + "&redirect_uri=" + Application_Details.redirect_uri
    And set Application_Details $.request_body = body

    Given url TokenUrl
#    And header Authorization = auth
    And header Content-Type = "application/x-www-form-urlencoded"
    And request body
    When method POST
    Then match responseStatus == 401

    Examples:
      | count | TPPRole   |
      | 001   | AISP_PISP |

  Scenario Outline: TC#key#_Verify whether error is received when TPP having AISP role invokes API

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'TPP validation'

   #################
  # TPP1 having PISP role generates auth code
    And def Application_Details = active_tpp.<TPP1Role>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details

  # TPP2 having AISP role invokes API
    * def authcode = Application_Details.code
    And def Application_Details = active_tpp.<TPP2Role>
    And set Application_Details.code = authcode
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"

    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPP1Role | TPP2Role | Expected |
      | 001   | PISP     | AISP     | 400      |


#  Scenario Outline: TC#key#_Verify whether error is received when network certificate is not configured for TPP invoking API
#
#    Given def info = karate.info
#    And def key = '<count>'
#    And set info.subset = 'TPP validation'
#
# #################
## TPP1 having PISP role generates auth code
#    And def Application_Details = active_tpp.<TPP1Role>
#    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
#    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details
#
#    # code to generate the access token using auth code
#    Given call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details
#    * def authcode = Application_Details.code
#
#  # TPP2 having AISP and PISP role invokes API,network certificate is not configured
#    And def Application_Details = active_tpp.<TPP2Role>
#    And set Application_Details.code = authcode
#    And set Application_Details $.request_method = 'POST'
#    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
#    And set Application_Details $.grant_type = "authorization_code"
#    When call apiApp.Access_Token_ACG(Application_Details)
#    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>
#
#    Examples:
#      | count | TPP1Role | TPP2Role  | Expected |
#      | 001   | PISP     | AISP_PISP | 401      |