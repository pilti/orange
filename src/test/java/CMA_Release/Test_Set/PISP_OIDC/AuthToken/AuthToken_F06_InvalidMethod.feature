@PISP
@PISP_Consent
@PISP_AccessTokenUsingOAuth

Feature: This feature validates the error status code if user inputs the invalid method

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    ##############################################################################################################################################

  Scenario Outline: TC#key#_Verify that Status code 405 is displayed in the response when invalid method is passed in API request

    Given def info = karate.info
    And def key = '<count>'
    And set info.subset = 'HTTP Method validation'

     #################
    # code to get authcode for payment submission
    * def Application_Details = active_tpp.<TPPRole>
    * def consentData = {"usr":'#(user_details.PISP.G_User_2.user)',"otp":'#(user_details.PISP.G_User_2.otp)',"action":"continue","Confirmation":"Yes"}
    * call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_ConstructConsentURl.feature') Application_Details

    # code to generate the access token using auth code
    Given def AuthCode = call read('classpath:CMA_Release/Entities_API/PISP_Resources/OIDC/PreReq_GetAuthCode.feature') Application_Details

    * set Application_Details $.request_method = <method>
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When set Application_Details $.grant_type = "authorization_code"

    And call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == <Expected>

    Examples:
      | count | TPPRole   | method     | Expected |
      | 001   | AISP_PISP | 'GET'      | 405      |
      | 002   | AISP_PISP | 'DELETE'   | 405      |
      | 003   | AISP_PISP | 'COPY'     | 405      |
      | 004   | AISP_PISP | 'VIEW'     | 405      |
      | 005   | AISP_PISP | 'LINK'     | 405      |
      | 006   | AISP_PISP | 'UNLINK'   | 405      |
      | 007   | AISP_PISP | 'HEAD'     | 405      |
      | 008   | AISP_PISP | 'OPTIONS'  | 405      |
      | 009   | AISP_PISP | 'PATCH'    | 405      |
      | 010   | AISP_PISP | 'PUT'      | 405      |
      | 011   | AISP_PISP | 'PROPFIND' | 405      |
      | 012   | AISP_PISP | 'LOCK'     | 405      |
      | 013   | AISP_PISP | 'UNLOCK'   | 405      |
      | 014   | AISP_PISP | 'PURGE'    | 405      |

