package CMA_Release.Test_Set;

import CMA_Release.Test_Set.AISP_OIDC.CCToken.Runner_AISP_CCTokens;
import CMA_Release.Test_Set.AISP_OIDC.M_AccInfo.Runner_AISP_MultiAccoutInfo;
import CMA_Release.Test_Set.AISP_OIDC.PreAuthValidation.Runner_AISP_PreAuth;
import CMA_Release.Test_Set.AISP_OIDC.RefToken.Runner_AISP_RefToken;
import CMA_Release.Test_Set.AISP_OIDC.RefTokenRenewal.Runner_AISP_RefreshTokenRenewal;
import CMA_Release.Test_Set.AISP_OIDC.SCA.Runner_AISP_SCA;
import CMA_Release.Test_Set.AISP_OIDC.S_AccBal.Runner_AISP_AccBalance;
import CMA_Release.Test_Set.AISP_OIDC.S_AccBen.Runner_AISP_Beneficiaries;
import CMA_Release.Test_Set.AISP_OIDC.S_AccDD.Runner_AISP_DirectDebits;
import CMA_Release.Test_Set.AISP_OIDC.S_AccInfo.Runner_AISP_SingleAccoutInfo;
import CMA_Release.Test_Set.AISP_OIDC.S_AccProd.Runner_AISP_Products;
import CMA_Release.Test_Set.AISP_OIDC.S_AccSO.Runner_AISP_StandingOrder;
import CMA_Release.Test_Set.AISP_OIDC.S_AccTxn.Runner_AISP_Transactions;

import CMA_Release.Test_Set.AISP_OIDC.SetAcReq.Runner_AISP_SetAccRequest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        // Runner_Environment_Shakedown.class,
        //  Runner_TPP_Hub.class,
        Runner_AISP_CCTokens.class,
       Runner_AISP_SetAccRequest.class,
        Runner_AISP_PreAuth.class,
        // Runner_AISP_Consent.class,
        Runner_AISP_SCA.class,
        Runner_AISP_MultiAccoutInfo.class,
        Runner_AISP_SingleAccoutInfo.class,
        Runner_AISP_RefToken.class,
        Runner_AISP_AccBalance.class,
        Runner_AISP_Transactions.class,
        Runner_AISP_Beneficiaries.class,
        Runner_AISP_DirectDebits.class,
        Runner_AISP_Products.class,
        Runner_AISP_RefreshTokenRenewal.class,
        Runner_AISP_StandingOrder.class,
})
public class Runner_TestSuite {

}
