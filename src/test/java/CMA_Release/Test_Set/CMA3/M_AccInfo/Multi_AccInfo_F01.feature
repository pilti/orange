@AISP_MultiAccInfo
Feature: API - Multi account request CMA3 validation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * def CPdb = Java.type('CMA_Release.Entities_DB.CHANNEL_PROFILE_DB')
    * def SPShareddb = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def ABTdb = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def FieldValueValidations = Java.type('CMA_Release.Java_Lib.AISP_Scenarios.FieldValueValidations')
    * def reusable = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @AISP_MultiAccInfo
  Scenario: Verify that new field AccountType updated correctly as Personal account/Business Account for savings account
  #Also check if FS and DB values are matching
  #Logic : If the market classification is between 960-972 inclusive, then the account is a personal account otherwise Business accouont

    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    * def arr_acc = get apiApp.Multi_Account_Information.response.Data.Account[*].Account[*].Identification
    * def arr_accType = get apiApp.Multi_Account_Information.response.Data.Account[*].AccountType

    # get market classification of accounts from ABT database in array
    * eval var recordsABT = []
    * eval for(var i = 0; i < arr_acc.length; i++) {recordsABT.push(ABTdb.get_ABT_Details_MarketClassification(arr_acc[i].substring(0,6), arr_acc[i].substring(6,14), ABT_DB_Conn).replace("\\","").replace("[","").replace("]",""))}
    # convert DB records obtained to an array of column values
    * def funct = FieldValueValidations.convertDBColumnValToArray(recordsABT)
    * set Result.DatabaseResponse = recordsABT
    # assert account type with market classification value in ABT Db
    * def temp = FieldValueValidations.CheckMrktClassification(funct , arr_accType)

     #Hit Fs services
    * set fs_data.profileId = profile.usr
    * set fs_data.X-BOI-USER = profile.usr
    * json FS_Accounts = call read('classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfile.feature')
    * def FS_AccountsRes = FS_Accounts.response
    * set Result.FS_Response = FS_AccountsRes

    #filter FS response for getting eligible accounts seen on Consent Page
    * def accdetails = get FS_AccountsRes.channelProfile._.accounts.account[*]
    * eval var FS_accdetailsFilter = []
    * eval var FS_accTypeFilter = []
    * eval for(var i = 0; i < accdetails.length; i++) if ( accdetails[i].accountPermission != 'JX') {FS_accdetailsFilter.push(accdetails[i].accountNSC + accdetails[i].accountNumber);FS_accTypeFilter.push(accdetails[i].accountType.substring(0,8))}

   #set account types obtain from API,DB and FS in Result
    * set Result.AccountType_APIResponse = arr_accType
    * set Result.AccountType_FSResponseFiltered = FS_accTypeFilter
    * set Result.AccountType_DBResponse = funct

    # Match the APi Response with FS response
    * match arr_acc == FS_accdetailsFilter
    * match arr_accType == FS_accTypeFilter

  @AISP_MultiAccInfo
  Scenario: Verify that new fields AccountSubType updated correctly SavingsAccount for Savings accounts
  #Also check if FS and DB values matching with API responses
    #Logic : AcoountType value 01 for Savings in customer profile database
    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    * def arr_acc = get apiApp.Multi_Account_Information.response.Data.Account[*].Account[*].Identification
    * def arr_accSubType = get apiApp.Multi_Account_Information.response.Data.Account[*].AccountSubType

    # get all accounts of user from channel profile
    * eval var accSubTypeDB = []
    * eval for(var i = 0; i < arr_acc.length; i++){accSubTypeDB.push(CPdb.get_Channel_Profile_ColumnVals("WHERE CHANNEL_ID = "+ profile.usr + " AND ACCOUNT_NUMBER ="+ arr_acc[i].substring(6,14),"ACCOUNT_TYPE",CHANNEL_PROFILE_DB_Conn).replace("[","").replace("]",""))}
    * set Result.DatabaseResponse = accSubTypeDB
    # convert the DB result to array
    * def funct = FieldValueValidations.convertDBColumnValToArray(accSubTypeDB)
    # assert account sub type with ACCOUNT_TYPE value in Channel Db
    * def temp = FieldValueValidations.CheckAccountSubType(funct , arr_accSubType)

     #Hit Fs services
    * set fs_data.profileId = profile.usr
    * set fs_data.X-BOI-USER = profile.usr
    * json FS_Accounts = call read('classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfile.feature')
    * def FS_AccountsRes = FS_Accounts.response
    * set Result.FS_Response = FS_AccountsRes

    #filter FS response
    * def accdetails = get FS_AccountsRes.channelProfile._.accounts.account[*]
    * eval var FS_accdetailsFilter = []
    * eval var FS_accSubTypeFilter = []
    * eval for(var i = 0; i < accdetails.length; i++) if ( accdetails[i].accountPermission != 'JX') {FS_accdetailsFilter.push(accdetails[i].accountNSC + accdetails[i].accountNumber);FS_accSubTypeFilter.push(accdetails[i].accountSubType.replace(" ",""))}

    #set account types obtain from API,DB and FS in Result
    * set Result.AccountSubType_APIResponse = arr_accSubType
    * set Result.AccountSubType_FSResponseFiltered = FS_accSubTypeFilter
    * set Result.AccountSubType_DBResponse = funct

    # Match the APi Response with FS response
    * print arr_acc
    * print FS_accdetailsFilter
    * match arr_acc == FS_accdetailsFilter
    * match arr_accSubType == FS_accSubTypeFilter

  @db1
  Scenario: Verify that Current account and Savings accounts are filtered in API and are present in consent screen and FS.
  # Logic  : AcoountType values other than 01 and 02  from current accounts in customer profile database should be filtered in API platform."
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * set info.subset = 'Consent Screen Accounts Validation'
    * def path = call  webApp.path1 = info1.path
    * set info.Screens = 'Yes'
    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    * def arr_acc = get apiApp.Multi_Account_Information.response.Data.Account[*].Account[*].Identification

   # get all accounts of user from channel profile
    * eval var accNumberDB = []
    * eval accNumberDB.push(CPdb.get_Channel_Profile_ColumnVals("WHERE CHANNEL_ID = "+ profile.usr + " AND ACCOUNT_TYPE in ('01','02')" ,"ACCOUNT_NUMBER,CREDIT_CARD_NUMBER" , CHANNEL_PROFILE_DB_Conn).replace("[","").replace("]",""))
    * set Result.Accounts.NonStandardAccountsFromDB = accNumberDB
    * def accDB = FieldValueValidations.convertDBResultSetToArray(accNumberDB)

   # check accounts fetched from DB with type equal to '01', '02' are available in API response
    * def temp = FieldValueValidations.PresenceOfAccounts(accNumberDB,arr_acc)

    #check accounts not present on consent Page
    Given def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    * def accOnConsent = Application_Details.selectedAccounts
    * set Result.Accounts.DisplayedOnConsentScreen = accOnConsent

    # convert account fetched from DB into format seen on consent page
    * eval var accDBScreenFormat = []
    * eval for(var i = 0 ; i<accDB.length;i++){accDBScreenFormat.push(("~" + accDB[i].substring(accDB[i].length-4,accDB[i].length)))}

    #assert accounts filetered from DB not on consent
    * def validate = FieldValueValidations.CheckList1PresentInList2(accDBScreenFormat,accOnConsent)
    * set Result.TestCondition = "Accounts Current & Savings retreived from database, are found in API response and present on consent screen too"





