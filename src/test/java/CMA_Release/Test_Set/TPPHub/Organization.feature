@TPP_Hub
Feature: Test the organisation tab functionality of TPP hub application

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def POM_TPPOrg = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPOrg')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2

    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """

  Scenario: Verify that PTC  is able to access Organisations tab from Main page of portal


    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def orgTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_organizationTab)
    * match orgTab == "Organisations"
    * set Result.UI.Actual = orgTab
    * set Result.UI.Expected = "Organisations"


  Scenario: Verify that header of TPP Portal after logging is having tabs for Organisations, Applications, HELP, Welcome PTC name and Logout

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    When def applicationTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Applications)
    Then match applicationTab == "Applications"
    * set Result.UI.Actual = applicationTab
    * set Result.UI.Expected = "Applications"
    When def helpTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Help)
    Then match helpTab == "Help"
    * set Result.UI.Actual = helpTab
    * set Result.UI.Expected = "Help"
    When def logoutTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Logout_Link)
    Then match logoutTab == "Logout"
    * set Result.UI.Actual = logoutTab
    * set Result.UI.Expected = "Logout"


  @Organisation_test
  Scenario Outline: Verify that below details are shown on page once PTC  selects Organisations from Organisations section

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    Given def data =
    """
    {
      "OrganisationName": <OrganisationName>
    }
    """
    * print "before calling"+data

    And def expectedValues =
    """
    {
      "TPP Organisation ID": <TPP Organisation ID>,
      "TPP Role": <TPP Role>,
      "TPP Member State Competent Authority": "FCA",
      "TPP Registration ID": "BankofIrelandPLC3",
      "TPP Email ID": "Not available",
      "TPP Phone Number": "Not available"
    }
    """

    * print "expectedValues"
    * print expectedValues

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP



    Given def selectOrg = TPPFunctions.PTC_Select_Organisation_WithoutContinueClick(app,data.OrganisationName)
    When def verifyFields = TPPFunctions.verifyApplicationFields(app)
    * print "actual values"
  * print verifyFields
    * set Result.VerifyFieldValues.ActualValues = verifyFields
    * print "***************************expected values"
  * print expectedValues
    * set Result.VerifyFieldValues.ExpectedValues = expectedValues
    * match expectedValues == verifyFields

    Examples:
    |OrganisationName|TPP Organisation ID|TPP Role|
    |'Bank of Ireland TPP - AISP and PISP'|'nBRcYAcACnghbGFOBk'|'PISP, AISP'|


  Scenario Outline: Verify that  PTC  come back after going on Application page and change Organisation to other one before adding application

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "OrganisationName2":<OrganisationName2>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data.OrganisationName)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def orgNameApplicationPageBefore = Resuable.getElementText(app,POM_TPPApp.orgNameApplicationPage)
    * set Result.orgNameApplicationPageBefore.AppName = "Organization name is captured as - " + orgNameApplicationPageBefore
    * match orgNameApplicationPageBefore == data.OrganisationName

    And def clickBackward = Resuable.browserNavigations(app,"backward")
    * set Result.clickBackward.Status = clickBackward.Status
    * set Result.clickBackward.Message = clickBackward.Message


    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data.OrganisationName2)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def orgNameApplicationPageAfter = Resuable.getElementText(app,POM_TPPApp.orgNameApplicationPage)
    * set Result.orgNameApplicationPageAfter.AppName = "Organization name is captured as - " + orgNameApplicationPageAfter
    * match orgNameApplicationPageAfter == data.OrganisationName2


    Examples:
      |GiveOrganisationName|OrganisationName2|
      |'Bank of Ireland TPP - AISP and PISP'|'Bank of Ireland TPP - PISP'|

