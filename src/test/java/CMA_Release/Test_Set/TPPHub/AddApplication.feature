@TPP_Hub

Feature: Test the forward and backward button functionality of TPP hub application

  Background:

    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')
    * def POM_TPPOrg = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPOrg')

    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2


    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  app.stop();
  }
  """


  Scenario Outline: Verify that PTC  is able to navigate to Add Application page after selecting Organisation
# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    Then def addAppButton = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * print elePresent
    * set Result.addAppButton.Actual = "User is navigated back successfully to Applications page"
    * set Result.addAppButton.Expected = "User should navigated back successfully to Applications page"

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  Scenario Outline: Verify that PTC should be able to navigate to Organisation page by clicking on Organisation link from Header of Applications and all organisations details will be shown correct

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def ClickElement = Resuable.ClickElement(app,POM_TPPApp.organizationTab)
    * set Result.ClickElement.Actual = "organization tab is clicked"
    * set Result.ClickElement.Expected = "organization tab should be clicked"
    Then def organizationLabel = Resuable.CheckElementEnabled(app,POM_TPPApp.orgLabel)
    * print organizationLabel
    * match organizationLabel == true
    * set Result.organizationLabel.Actual = "User is navigated back successfully to Organisation page"
    * set Result.organizationLabel.Expected = "User should navigated back successfully to Organisation page"

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |

  Scenario Outline: Verify that header of Application Page is having tabs for Applications, HELP, WelcomePTCname name and Logout
    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def applicationTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Applications)
    Then match applicationTab == "Applications"
    * set Result.applicationTab.Actual = "Application tab is present"
    * set Result.applicationTab.Expected = "Application tab should be present"
    When def helpTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Help)
    Then match helpTab == "Help"
    * set Result.helpTab.Actual = "Application tab is present"
    * set Result.helpTab.Expected = "Application tab should be present"
    When def logoutTab = Resuable.getElementText(app,POM_TPPOrg.TppOrg_Logout_Link)
    Then match logoutTab == "Logout"
    * set Result.logoutTab.Actual = "Application tab is present"
    * set Result.logoutTab.Expected = "Application tab should be present"

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  Scenario Outline: Verify that PTC  is able to access header HELP button on Application to access details information of the page

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    * def strExpHelp = "https://www.bankofireland.com/api/thirdparty/help/"
    When def helpTab = TPPFunctions.verifyHyperLinkUrlInTabbedBrowser(app,POM_TPPApp.TppOrg_Help,strExpHelp)
    Then match helpTab.Status == "Pass"
    * set Result.helpTab.Status = helpTab.Status
    * set Result.helpTab.Message = helpTab.Message

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |

  Scenario Outline: Verify that footer of Applications Page is having links for Cookies and Privacy policy and customer is able to access all the links

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    * def expUrl = "https://www.bankofireland.com/legal/privacy-statement/"
    When def TppOrg_Cookie_Privacy = TPPFunctions.verifyHyperLinkUrlInTabbedBrowser(app,POM_TPPApp.TppOrg_Cookie_Privacy,expUrl)
    Then match TppOrg_Cookie_Privacy.Status == "Pass"
    * set Result.TppOrg_Cookie_Privacy.Status = TppOrg_Cookie_Privacy.Status
    * set Result.TppOrg_Cookie_Privacy.Message = TppOrg_Cookie_Privacy.Message

    Examples:
      | GiveOrganisationName                  |
      | 'Bank of Ireland TPP - AISP and PISP' |


  Scenario: Verify that footer of Applications Page is having links for help and customer is able to access all the links
    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": "Bank of Ireland TPP - AISP and PISP"
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    * def expUrl = "https://www.bankofireland.com/api/thirdparty/help/"
    When def TppOrg_FooterHelp = TPPFunctions.verifyHyperLinkUrlInTabbedBrowser(app,POM_TPPApp.TppOrg_FooterHelp,expUrl)
    Then match TppOrg_FooterHelp.Status == "Pass"
    * set Result.TppOrg_FooterHelp.Status = TppOrg_FooterHelp.Status
    * set Result.TppOrg_FooterHelp.Message = TppOrg_FooterHelp.Message


  Scenario: Verify that PTC  is able to Log out from Application  by clicking on Logout button

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    When def PTC_Logout_TPP_Hub = TPPFunctions.PTC_Logout_TPP_Hub(app)
    Then match PTC_Logout_TPP_Hub.Status == "Pass"
    * set Result.PTC_Logout_TPP_Hub.Status = PTC_Logout_TPP_Hub.Status
    * set Result.PTC_Logout_TPP_Hub.Message = PTC_Logout_TPP_Hub.Message

  Scenario Outline: Verify that PTC  is able to add new application through Application page of TPP Portal

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message

    Examples:
      | GiveOrganisationName                  | FilePath                                        | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16'  |


  @TPP_Hub
  Scenario Outline: Verify that Below Tabs are available on Add Application page and Upload Software Statement field and PTC  should be able to navigate to each tab


     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def clickAddApp = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button)
    * set Result.clickAddApp.Actual = "Add Application button is clicked"

    When def CheckElementEnabled = Resuable.CheckElementEnabled(app,POM_TPPApp.Browse_Button)
    * set Result.CheckElementEnabled.Actual = "Browse Application button is present"
    * set Result.CheckElementEnabled.Expected = "Browse Application button should be present"
    Then match CheckElementEnabled == true

    When def CheckElementEnabled = Resuable.CheckElementPresent(app,POM_TPPApp.Upload_Button)
    * set Result.CheckElementEnabled.Actual = "Upload Application button is present"
    * set Result.CheckElementEnabled.Expected = "Upload Application button should be present"
    Then match CheckElementEnabled == true

    When def CheckElementEnabled = Resuable.CheckElementEnabled(app,POM_TPPApp.Cancel_Button)
    * set Result.CheckElementEnabled.Actual = "Cancel Application button is present"
    * set Result.CheckElementEnabled.Expected = "Cancel Application button should be present"
    Then match CheckElementEnabled == true


    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  @TPP_Hub
  Scenario Outline: Verify that PTC should be able to navigate to Organisation page by clicking on Organisation link from Header of Applications while on Upload SSA file page and all organisations details will be shown correct

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def clickAddApp = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button)
    * set Result.clickAddApp.Actual = "Add Application button is clicked"

    When def ClickElement = Resuable.ClickElement(app,POM_TPPApp.organizationTab)
    * set Result.ClickElement.Actual = "organization tab is clicked"
    * set Result.ClickElement.Expected = "organization tab should be clicked"
    Then def organizationLabel = Resuable.CheckElementEnabled(app,POM_TPPApp.orgLabel)
    * set Result.organizationLabel.Actual = "User is navigated back successfully to Organisation page"
    * set Result.organizationLabel.Expected = "User should navigated back successfully to Organisation page"
    * match organizationLabel == true


    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  @TPP_Hub
  Scenario Outline: Verify that Upload button gets enabled Once PTC  selects the file the name of the file will be shown in the input field

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def clickAddApp = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button)
    * set Result.clickAddApp.Actual = "Add Application button is clicked"

    * def fileName = data.SSAFilePath
    Given def clickBrowseAndSelectFile = TPPFunctions.clickBrowseAndSelectFile(app,fileName)
    * set Result.clickBrowseAndSelectFile.Actual = "Browse button is clicked and file is located"
    When def Upload_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.Upload_Button)
    * set Result.Upload_Button.Actual = "Upload Application button is enabled"
    * set Result.Upload_Button.Expected = "Upload Application button should be enabled"
    Then match Upload_Button == true

    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  @TPP_Hub
  Scenario Outline: Verify that Submit button of upload software statement is Enabled only after successfully uploading correct format of  Software statement file

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def clickAddApp = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button)
    * set Result.clickAddApp.Actual = "Add Application button is clicked"

    * def fileName = data.SSAFilePath
    Given def clickBrowseAndSelectFile = TPPFunctions.clickBrowseAndSelectFile(app,fileName)
    * set Result.clickBrowseAndSelectFile.Actual = "Browse button is clicked and file is located"

    When def Upload_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.Upload_Button)
    * set Result.Upload_Button.Actual = "Upload Application button is enabled"
    * set Result.Upload_Button.Expected = "Upload Application button should be enabled"
    Then match Upload_Button == true

    And def clickUpload = TPPFunctions.clickUpload(app)
    * set Result.clickUpload.Actual = "Upload button is clicked"

    When def addApplication_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.addApplication_Button)
    * set Result.addApplication_Button.Actual = "Add Application Application button is enabled"
    * set Result.addApplication_Button.Expected = "Add Application Application button should be enabled"
    Then match addApplication_Button == true

    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  Scenario Outline: Verify that PTC  is able to remove the uploaded certificate and to replace it with another one in case of wrong file uploaded

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name
    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Status = Add_Application.Status
    * set Result.Add_Application.Message = Add_Application.Message

    * def fileName = data.SSAFilePath
    Given def clickBrowseAndSelectFile = TPPFunctions.clickBrowseAndSelectFile(app,fileName)
    * set Result.clickBrowseAndSelectFile.Actual = "Browse button is clicked and file is located"

    When def Upload_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.Upload_Button)
    * set Result.Upload_Button.Actual = "Upload Application button is enabled"
    * set Result.Upload_Button.Expected = "Upload Application button should be enabled"
    Then match Upload_Button == true

    And def clickCancel = TPPFunctions.clickCancel(app)
    * set Result.clickCancel.Actual = "Cancel button is clicked"

    When def AddApp_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * set Result.AddApp_Button.Actual = "Add Application Application button is enabled"
    * set Result.AddApp_Button.Expected = "Add Application Application button should be enabled"
    Then match AddApp_Button == true

    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  @TPP_Hub
  Scenario Outline: Verify that PTC  is able to see uploaded file details on screen after successfully upload of Software statement file

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name


    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    When def clickAddApp = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button)
    * set Result.clickAddApp.Actual = "Add Application button is clicked"

    * def fileName = data.SSAFilePath
    Given def clickBrowseAndSelectFile = TPPFunctions.clickBrowseAndSelectFile(app,fileName)
    * set Result.clickBrowseAndSelectFile.Actual = "Browse button is clicked and file is located"

    When def Upload_Button = Resuable.CheckElementEnabled(app,POM_TPPApp.Upload_Button)
    * set Result.Upload_Button.Actual = "Upload Application button is enabled"
    * set Result.Upload_Button.Expected = "Upload Application button should be enabled"
    Then match Upload_Button == true

    And def clickUpload = TPPFunctions.clickUpload(app)
    * set Result.clickUpload.Actual = "Upload button is clicked"

    When def verifyAddedApplicationFields = TPPFunctions.verifyAddedApplicationFields(app)
    * set Result.verifyAddedApplicationFields.Status = verifyAddedApplicationFields.Status
    * set Result.verifyAddedApplicationFields.Message = verifyAddedApplicationFields.Message
    * set Result.verifyAddedApplicationFields.Actual = verifyAddedApplicationFields.Actual
    * set Result.verifyAddedApplicationFields.Expected = verifyAddedApplicationFields.Expected
    * match verifyAddedApplicationFields.Status == "Pass"

    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  Scenario Outline: Verify that success message will display for as long as the PTC  stays on this page

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName



    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"

    When def ClickElement = Resuable.ClickElement(app,POM_TPPApp.organizationTab)
    * set Result.ClickElement.Actual = "organization tab is clicked"
    * set Result.ClickElement.Expected = "organization tab should be clicked"

    Then def verifySuccessMessage = Resuable.CheckElementPresent(app,POM_TPPApp.AppSuccess_Msg)
    * set Result.verifySuccessMessage.Actual = "Success message is not present"
    * set Result.verifySuccessMessage.Expected = "Success message should not be present"
    Then match verifySuccessMessage == false


    Examples:
      | GiveOrganisationName                  | FilePath                                        | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16'  |


  @TPP_Hub
  Scenario Outline: Verify that PTC  is able to click on Show More details accordion  to view more details

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """



    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def ClickElement = Resuable.ClickElement(app,POM_TPPApp.showMoreBtn,"Show More")
    * set Result.ClickElement.Actual = ClickElement.Message
    Then match ClickElement.Status == "Pass"


    When def verifyAddedApplicationFields = TPPFunctions.verifyShowMoreApplicationFields(app)
    * set Result.verifyAddedApplicationFields.Status = verifyAddedApplicationFields.Status
    * set Result.verifyAddedApplicationFields.Message = verifyAddedApplicationFields.Message
    * set Result.verifyAddedApplicationFields.Actual = verifyAddedApplicationFields.Actual
    * set Result.verifyAddedApplicationFields.Expected = verifyAddedApplicationFields.Expected
    * match verifyAddedApplicationFields.Status == "Pass"


    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  Scenario Outline: Verify that PTC is able to add Application having both AISP and PISP role and correct Role will be shown after adding application

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<ApplicationName>,
      "Role":<Role>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message

    * def appName = data.ApplicationName
    * def role = data.Role
    Then def verifyRole = TPPFunctions.verifyRole(app,appName,role)
    * set Result.verifyRole.Status = verifyRole.Status
    * set Result.verifyRole.Message = verifyRole.Message
    * set Result.verifyRole.Actual = verifyRole.Actual
    * set Result.verifyRole.Expected = verifyRole.Expected
    * match verifyRole.Status == "Pass"


    Examples:
      | GiveOrganisationName                  | FilePath                                        | ApplicationName    | Role        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16' | 'PISP,AISP' |


  @TPP_Hub
  Scenario Outline: Verify that PTC  is able to see all registered  application with few details after clicking on Submit button of uploaded SSA file

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<ApplicationName>,
      "Role":<Role>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message

    * def appName = data.ApplicationName
    * def role = data.Role
    Then def verifyRole = TPPFunctions.verifyRole(app,appName,role)
    * set Result.verifyRole.Status = verifyRole.Status
    * set Result.verifyRole.Message = verifyRole.Message
    * set Result.verifyRole.Actual = verifyRole.Actual
    * set Result.verifyRole.Expected = verifyRole.Expected
    * match verifyRole.Status == "Pass"


    Examples:
      | GiveOrganisationName                  | FilePath                                        | ApplicationName    | Role        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16' | 'PISP,AISP' |


  @TPP_Hub
  Scenario Outline: Verify that PTC  is able to click on Show less details to hide the additional details of the selected application

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name


    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<ApplicationName>,
      "Role":<Role>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    When def showMoreBtn = Resuable.ClickElement(app,POM_TPPApp.showMoreBtn,"Show More")
    * set Result.showMoreBtn.Actual = showMoreBtn.Message
    Then match showMoreBtn.Status == "Pass"

    When def CheckElementEnabled = Resuable.CheckElementPresent(app,POM_TPPApp.showMoreApplicationFieldTbl)
    * set Result.CheckElementEnabled.Actual = "Clicking show more button is showing the application details"
    * set Result.CheckElementEnabled.Expected = "Clicking show more button should show the application details"
    Then match CheckElementEnabled == true

    When def showLessBtn = Resuable.ClickElement(app,POM_TPPApp.showLessBtn,"Show Less")
    * set Result.showLessBtn.Actual = showLessBtn.Message
    Then match showLessBtn.Status == "Pass"

    When def CheckElementEnabled = Resuable.CheckElementPresent(app,POM_TPPApp.showMoreApplicationFieldTbl)
    * set Result.CheckElementEnabled.Actual = "Clicking show less button is hiding the application details"
    * set Result.CheckElementEnabled.Expected = "Clicking show less button should hide the application details"
    Then match CheckElementEnabled == false


    Examples:
      | GiveOrganisationName                  | FilePath                                        | ApplicationName    | Role        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16' | 'PISP,AISP' |


  @testing112
  Scenario Outline: Verify that PTC  is NOT able to submit multiple applications with same software statement file

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName



    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message

    When def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,false)
    Then match Add_Application_For_Selected_Organisation.Status == "Fail"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    * def expError = "Unable to process your request. Please try again later."
    When def verifyAppFailureMessage = TPPFunctions.verifyAppFailureMessage(app,expError)
    * set Result.verifyAppFailureMessage.Status = verifyAppFailureMessage.Status
    * set Result.verifyAppFailureMessage.Message = verifyAppFailureMessage.Message
    * set Result.verifyAppFailureMessage.Actual = verifyAppFailureMessage.Actual
    * set Result.verifyAppFailureMessage.Expected = verifyAppFailureMessage.Expected
    Then match verifyAppFailureMessage.Status == "Pass"

    Examples:
      | GiveOrganisationName                  | FilePath                                        | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16'  |


  @TPP_Hub
  Scenario Outline: Verify that PTC  is able to see explanatory information after clicking on question mark of Role Client ID Client Secret on Main Page

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    Given def helpQuesMark = Resuable.MouseHoveronElement(app, POM_TPPApp.helpQuesMark,"Help")
    * set Result.helpQuesMark.Message = helpQuesMark.Message
    And def helpInfo = Resuable.CheckElementPresent(app, POM_TPPApp.helpInfo);
    * set Result.helpInfo.Actual = "Explanatory info is correctly displayed for help"
    * set Result.helpInfo.Expected = "Explanatory info should get correctly displayed for help"
    Then match helpInfo == true

    Given def ClientSecretQuesMark = Resuable.MouseHoveronElement(app, POM_TPPApp.ClientSecretQuesMark,"ClientSecret")
    * set Result.ClientSecretQuesMark.Message = ClientSecretQuesMark.Message
    And def ClientSecretInfo = Resuable.CheckElementPresent(app, POM_TPPApp.ClientSecretInfo);
    * set Result.ClientSecretInfo.Actual = "Explanatory info is correctly displayed for ClientSecret"
    * set Result.ClientSecretInfo.Expected = "Explanatory info should get correctly displayed for ClientSecret"
    Then match helpInfo == true

    Examples:
      | GiveOrganisationName                  | FilePath                                        |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' |


  Scenario Outline: Verify that newest application will be shown on Top when PTC  is having multiple applications added under Application section and columns of applications are not sortable

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName



    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"

    * def expAppName = data.ApplicationName
    When def actFirstAppName = Resuable.getElementText(app,POM_TPPApp.firstApp)
    * set Result.actFirstAppName.Actual = "Application is present at the top"
    * set Result.actFirstAppName.Expected = "Application should present at the top"
    * match actFirstAppName == expAppName


    Examples:
      | GiveOrganisationName                  | FilePath                                        | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16'  |



  Scenario Outline: Verify that newest application will be shown on Top when PTC  is having multiple applications added under Application section and columns of applications are not sortable

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def clickShowMoreButton = Resuable.ClickElement(app,POM_TPPApp.showMoreBtn,"Show More")
    * set Result.clickShowMoreButton.Status = clickShowMoreButton.Status
    * set Result.clickShowMoreButton.Actual = clickShowMoreButton.Message
    Then match clickShowMoreButton.Status == "Pass"

    When def clickShowMoreButton2 = Resuable.ClickElement(app,POM_TPPApp.showMoreBtn,"Show More")
    * set Result.clickShowMoreButton2.Status = clickShowMoreButton.Status
    * set Result.clickShowMoreButton2.Actual = clickShowMoreButton.Message
    Then match clickShowMoreButton2.Status == "Pass"

    When def showMoreText = Resuable.getElementText(app,POM_TPPApp.applicationDetailsOneAtTime)
    * set Result.showMoreText.Actual = "Only one application is visible at a time"
    * set Result.showMoreText.Expected = "Only one application should be visible at a time"
    * match showMoreText == "Show more details"




    Examples:
      | GiveOrganisationName                  | FilePath                                        | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | './src/test/java/CMA_Release/Base_Data/SSA.txt' | 'BOI_SIT_Client16'  |

