@TPP_Hub
Feature: Test the API are working as expected for the repected added TPP

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2


    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
   app.stop();
  }
  """

  @testtoday123
  Scenario Outline: Verify that Application added for both AISP and PISP role should be able to access BOI AISP services successfully

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "SSAFilePath":<FilePath>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data,true)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message


        When def Get_ClientSecrete_For_Given_Application = TPPFunctions.Get_ClientSecrete_For_Given_Application(app,data)
    * set Result.Get_ClientSecrete_For_Given_Application.Status = Get_ClientSecrete_For_Given_Application.Status
    * set Result.Get_ClientSecrete_For_Given_Application.Message = Get_ClientSecrete_For_Given_Application.Message
    * set Result.Get_ClientSecrete_For_Given_Application.CS_ID = Get_ClientSecrete_For_Given_Application.CS_ID
    * def strClientSecretId = Get_ClientSecrete_For_Given_Application.CS_ID

# verify the API call is unsuccessful
    Given def Application_Details = active_tpp.<role>

 #Hit Certificate request to server
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.client_secret = strClientSecretId


    * print Application_Details
    When call apiApp.Access_Token_CCG(Application_Details)
#
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_CCG.Input.grant_type = Application_Details.grant_type
    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And set Result.Additional_Details = Application_Details


    * set Result.Access_Token_CCG.Actual.responseStatus = apiApp.Access_Token_CCG.responseStatus
    * set Result.Access_Token_CCG.Expected.responseStatus = <statusCode>

    Then match apiApp.Access_Token_CCG.responseStatus == <statusCode>


    Examples:
      | GiveOrganisationName                  | GiveApplicationName | FilePath                                                          | role              |statusCode|scope|
      | 'BOISIT3' | 'PRC_SIT_AISP_PISP' | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP_PISP.txt' | PRC_SIT_AISP_PISP      |200       |'payments'|
      | 'BOISIT3' | 'PRC_SIT_AISP_PISP' | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP_PISP.txt' | PRC_SIT_AISP_PISP      |200       |'accounts'|
      | 'BOISIT2' | 'PRC_SIT_AISP'      | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP.txt'      | PRC_SIT_AISP      |200       |'accounts'|
      | 'BOISIT2' | 'PRC_SIT_AISP'      | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP.txt'      | PRC_SIT_AISP      |400       |'payments'|
      | 'BOISIT2' | 'PRC_SIT_PISP'      | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_PISP.txt'      | PRC_SIT_PISP      |200       |'payments'|
      | 'BOISIT2' | 'PRC_SIT_PISP'      | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_PISP.txt'      | PRC_SIT_PISP      |400       |'accounts'|


