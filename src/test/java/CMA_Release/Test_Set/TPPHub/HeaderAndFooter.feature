@TPP_Hub

Feature: Test the header and Footer button functionality of TPP hub application

  Background:

    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')
    * def POM_TPPOrg = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPOrg')

    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2


    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  app.stop();
  }
  """




  Scenario: Verify that there is no Link for Terms of Use in Footer for TPP Portal
# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)

    When def termsOfUse = Resuable.CheckElementPresent(app,POM_TPPApp.termsOfUse)
    * set Result.termsOfUse.Actual = "Terms of Use link is not present"
    * set Result.termsOfUse.Expected = "Terms of Use link should not be present"
    Then match termsOfUse == false


    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    When def termsOfUse = Resuable.CheckElementPresent(app,POM_TPPApp.termsOfUse)
    * set Result.termsOfUse.Actual = "Terms of Use link is not present"
    * set Result.termsOfUse.Expected = "Terms of Use link should not be present"
    Then match termsOfUse == false



