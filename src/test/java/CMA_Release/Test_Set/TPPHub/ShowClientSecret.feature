@TPP_Hub
Feature: Test the show client secret functionality of TPP hub application

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def applications = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.applications')
    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2

    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """
  @TPP_Hub_Quick
  Scenario Outline: Verify that PTC  is able to click on cross mark of Client Secret Alert message and alert message should be closed after clicking on it.

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "Action":<Action>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickShowClient(app,appName)
    When def ActionShowClient = applications.performActionShowCLientId(app,<Action>)
    * print ActionShowClient
    * print ActionShowClient.Action
    * print <Action>+" clicked"
    * match ActionShowClient.Action == <Action>+" clicked"
    * set Result.UI.Action = ActionShowClient.Action

    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * print elePresent
    * set Result.UI.Actual = "User is successfully navigated back to application Page after clicking the OK button"
    * set Result.UI.Expected = "User should successfully navigate back to application Page after clicking the OK button"




    Examples:
      |GiveOrganisationName                 |GiveApplicationName|Action|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|'ok'|
#      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|'showclient'|
#      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|'close'|



  Scenario Outline: Verify that PTC  is able to click on OK button of Client Secret Alert message and alert message should be closed after clicking on it.

   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickShowClient(app,appName)
    When def clickOK = Resuable.ClickElement(app,POM_TPPApp.OKBtn,"clickOK")
    * set Result.clickOK.Actual = clickOK.Message
    Then match clickOK.Status == "Pass"

    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * set Result.UI.Actual = "User is successfully navigated back to application Page after clicking the OK button"
    * set Result.UI.Expected = "User should successfully navigate back to application Page after clicking the OK button"


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|



  Scenario Outline: Verify that PTC  is able to click on showClientSecret button of Client Secret Alert message and alert message should be closed after clicking on it.

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickShowClient(app,appName)
    When def clickShowClientSecret = Resuable.ClickElement(app,POM_TPPApp.GetCS_details,"clickShowClientSecret")
    * set Result.clickShowClientSecret.Actual = clickShowClientSecret.Message
    Then match clickShowClientSecret.Status == "Pass"


    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * print elePresent
    * set Result.UI.Actual = "User is successfully navigated back to application Page after clicking the showClientSecret button"
    * set Result.UI.Expected = "User should successfully navigate back to application Page after clicking the showClientSecret button"


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|



  Scenario Outline: Verify that PTC  is able to click on cross mark of Client Secret Alert message and alert message should be closed after clicking on it.

   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickShowClient(app,appName)
    When def CloseDialogBox = Resuable.ClickElement(app,POM_TPPApp.CloseAlert_Button,"CloseDialogBox")
    * set Result.CloseDialogBox.Actual = CloseDialogBox.Message
    Then match CloseDialogBox.Status == "Pass"

    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * print elePresent
    * set Result.UI.Actual = "User is successfully navigated back to application Page after clicking the Close button"
    * set Result.UI.Expected = "User should successfully navigate back to application Page after clicking the Close button"


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|

