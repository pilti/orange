@TPP_Hub
Feature: Test the forward and backward button functionality of TPP hub application

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2

    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """


  Scenario: Verify the  Back Forward button functionality on Login with Open Banking Page

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def actUrl = Resuable.getCurrentURL(app)
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.UI.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.UI.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    And def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
      * def expUrl = "data:,"
      * match actUrl == expUrl
      * set Result.UI.clickBackward = clickBackward.Message
      * set Result.UI.Backward.ActualUrl = actUrl
      * set Result.UI.Backward.ExpectedUrl = expUrl



      And def clickforward = Resuable.browserNavigations(app,"forward")
      When def actUrl = Resuable.getCurrentURL(app)
      * def expUrl = baseUrl +"/tppportalv1/tppportal/#!/login"
      * match actUrl == expUrl
      * set Result.UI.clickforward = clickforward.Message
      * set Result.UI.Forward.ActualUrl = actUrl
      * set Result.UI.Forward.ExpectedUrl = expUrl

    And def clickRefresh = Resuable.browserNavigations(app,"refresh")
    * print clickRefresh
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/#!/login"
    * set Result.UI.clickRefresh = clickRefresh.Message
    * set Result.UI.Refresh.ActualUrl = actUrl
    * set Result.UI.Refresh.ExpectedUrl = expUrl
    * match actUrl == expUrl



  Scenario: Verify the  Back Forward  button functionality on Open Banking Sign On Page

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)

    Given def UserLogin = TPPFunctions.PTC_Navigate_TPP_Through_OBLink(app)
    Given def actUrl = Resuable.getCurrentURL(app)
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.UI.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.UI.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/#!/login"
    * match actUrl == expUrl
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl



    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.iam.openbanking.qa/as/authorization.oauth2?scope=openid+profile+address+phone+email+idp&response_type=id_token&redirect_uri=https%3A%2F%2Fauth.mit.openbanking"
    * match actUrl contains expUrl
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl

    And def clickRefresh = Resuable.browserNavigations(app,"refresh")
    * print clickRefresh
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.iam.openbanking.qa/as/authorization.oauth2?scope=openid+profile+address+phone+email+idp&response_type=id_token&redirect_uri=https%3A%2F%2Fauth.mit.openbanking.qa%2Fsp%2FeyJpc3MiOiJodHRwczpcL1wvYXV0aC5pYW0ub3BlbmJhbmtpbmcucWEifQ%2Fcb.openid&state="
    * match actUrl contains expUrl
    * set Result.UI.clickRefresh = clickRefresh.Message
    * set Result.UI.Refresh.ActualUrl = actUrl
    * set Result.UI.Refresh.ExpectedUrl = expUrl



  Scenario: Verify the  Back Forward  button functionality on Organisations Page.

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    Given def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/select-organisation"
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl,expUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.UI.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.UI.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://authenticator.pingone.eu/pingid/ppm/auth/otp"
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl
    * match actUrl == expUrl



    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl + "/tppportalv1/tppportal/home#!/login"
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl
    * match actUrl == expUrl

    And def clickRefresh = Resuable.browserNavigations(app,"refresh")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl + "/tppportalv1/tppportal/home#!/login"
    * set Result.UI.clickRefresh = clickRefresh.Message
    * set Result.UI.Refresh.ActualUrl = actUrl
    * set Result.UI.Refresh.ExpectedUrl = expUrl
    * match actUrl == expUrl



  Scenario: Verify the  Back Forward  button functionality on Organisations Page.

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl


    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    Given def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/select-organisation"
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl,expUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.UI.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.UI.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://authenticator.pingone.eu/pingid/ppm/auth/otp"
    * match actUrl == expUrl
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl



  Scenario Outline: Verify the  Back Forward  button functionality on the Popups of the Show Client Secrete page

  # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickShowClient(app,appName)

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/select-organisation"
    * match actUrl == expUrl
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl

    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/view-application"
    * match actUrl == expUrl
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl

  Examples:
  |GiveOrganisationName                 |GiveApplicationName|
  |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|


  Scenario Outline: Verify the  Back Forward  button functionality on any  Popup of the Reset Client Secrete page

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickResetClient(app,appName)

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/select-organisation"
    * match actUrl == expUrl
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl

    And def clickforward = Resuable.browserNavigations(app,"forward")
    * set Result.clickforward.Status = clickforward.Status
    * set Result.clickforward.Message = clickforward.Message
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/view-application"
    * set Result.clickforward.ActualUrl = actUrl
    * set Result.clickforward.ExpectedUrl = expUrl
    * match actUrl == expUrl

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|


  Scenario Outline: Verify the  Back Forward  button functionality on any  Popup of the Remove Client Secrete page

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl


    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickReset = TPPFunctions.clickRemoveClient(app,appName,true)

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/select-organisation"
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl
    * match actUrl == expUrl

    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/view-application"
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl
    * match actUrl == expUrl

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|




  Scenario Outline: Verify the  functionality of  Refresh  button on Application Page and PTC  changed sign out page

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl


    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def clickRefresh = Resuable.browserNavigations(app,"Refresh")
    * set Result.clickRefresh.Status = clickRefresh.Status
    * set Result.clickRefresh.Message = clickRefresh.Message
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/view-application"
    * set Result.clickRefresh.ActualUrl = actUrl
    * set Result.clickRefresh.ExpectedUrl = expUrl
    * match actUrl == expUrl


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|



  Scenario Outline: Verify the  functionality of  copy and paste URL on different tab on Application Page and PTC  changed to sign out page

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName


    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/view-application"
    Then def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl,expUrl)
    * set Result.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|



  Scenario Outline: Verify the  Back/Forward  button functionality on Upload Software statement Page.

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name
    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """



    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl



    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Message = Add_Application.Message

    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl + "/tppportalv1/tppportal/home#!/view-application"
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl
    * match actUrl == expUrl

    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl + "/tppportalv1/tppportal/home#!/add-application"
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl
    * match actUrl == expUrl


    And def clickRefresh = Resuable.browserNavigations(app,"refresh")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/add-application"
    * match actUrl contains expUrl
    * set Result.UI.clickRefresh = clickRefresh.Message
    * set Result.UI.Refresh.ActualUrl = actUrl
    * set Result.UI.Refresh.ExpectedUrl = expUrl

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|




  Scenario Outline: Verify the  functionality of  copy and paste URL on different tab on upload software statement  Page and PTC  changed to sign out page

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl



    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Message = Add_Application.Message

    Given def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/add-application"
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl,expUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|




  Scenario Outline: Verify the  Back/Forward  button functionality when upload button and submit button of Software Statement file is enabled

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "SSAFilePath":<SSAFilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl



    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Message = Add_Application.Message

  * def strFilePath = data.SSAFilePath
    When def clickBrowseAndSelectFile = TPPFunctions.clickBrowseAndSelectFile(app,strFilePath)
    * set Result.clickBrowseAndSelectFile.Status = clickBrowseAndSelectFile.Message
    * set Result.clickBrowseAndSelectFile.Message = clickBrowseAndSelectFile.Message

    When def Upload_Button = Resuable.ClickElement(app,POM_TPPApp.Upload_Button,"Upload Button")
    * set Result.Upload_Button.Message = Upload_Button.Message


    When def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl + "/tppportalv1/tppportal/home#!/add-application"
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl
    * match actUrl == expUrl


    Examples:
      |GiveOrganisationName                 |SSAFilePath|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA.txt'|




  Scenario Outline: Verify the  functionality of  copy and paste URL on different tab on upload software statement  Page and PTC  changed to sign out page

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Message = Add_Application.Message

    Then def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/add-application"
    And def verifyUrlInNewBrowserTab = TPPFunctions.verifyUrlInNewBrowserTab(app,actUrl,expUrl)
    * set Result.UI.verifyUrlInNewBrowserTab.Status = verifyUrlInNewBrowserTab.Status
    * set Result.UI.verifyUrlInNewBrowserTab.Message = verifyUrlInNewBrowserTab.Message
    * set Result.verifyUrlInNewBrowserTab.Actual = verifyUrlInNewBrowserTab.Actual
    * set Result.verifyUrlInNewBrowserTab.Expected = verifyUrlInNewBrowserTab.Expected
    * match verifyUrlInNewBrowserTab.Status == "Pass"

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|


  @TPP_HubReset2019
  Scenario Outline: Verify the  functionality of  Refresh  button on  Add Application Page and PTC  changed sign out page

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def Add_Application = Resuable.ClickElement(app,POM_TPPApp.AddApp_Button,"Add Application")
    * set Result.Add_Application.Message = Add_Application.Message

    And def clickRefresh = Resuable.browserNavigations(app,"refresh")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = "https://auth.mit.openbanking.qa/idp/startSLO.ping#!/add-application"
    * set Result.UI.clickRefresh = clickRefresh.Message
    * set Result.UI.Refresh.ActualUrl = actUrl
    * set Result.UI.Refresh.ExpectedUrl = expUrl
    * match actUrl == expUrl

    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|




  Scenario Outline: Verify the  Back/Forward  button functionality after successfully addition of the Application in Applications

   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

 # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName



    Given def Add_Application_For_Selected_Organisation = TPPFunctions.Add_Application_For_Selected_Organisation(app,data)
    Then match Add_Application_For_Selected_Organisation.Status == "Pass"
    * set Result.Add_Application_For_Selected_Organisation.Status = Add_Application_For_Selected_Organisation.Status
    * set Result.Add_Application_For_Selected_Organisation.Message = Add_Application_For_Selected_Organisation.Message

    When def Search_Application_From_Existing_List = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * set Result.Search_Application_From_Existing_List.Status = Search_Application_From_Existing_List.Status
    * set Result.Search_Application_From_Existing_List.Message = Search_Application_From_Existing_List.Message


    And def clickBackward = Resuable.browserNavigations(app,"backward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/add-application"
    * set Result.UI.clickBackward = clickBackward.Message
    * set Result.UI.Backward.ActualUrl = actUrl
    * set Result.UI.Backward.ExpectedUrl = expUrl
    * match actUrl == expUrl



    And def clickforward = Resuable.browserNavigations(app,"forward")
    When def actUrl = Resuable.getCurrentURL(app)
    * def expUrl = baseUrl +"/tppportalv1/tppportal/home#!/add-application"
    * set Result.UI.clickforward = clickforward.Message
    * set Result.UI.Forward.ActualUrl = actUrl
    * set Result.UI.Forward.ExpectedUrl = expUrl
    * match actUrl == expUrl


    Examples:
      |GiveOrganisationName|FilePath|GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA.txt'|'BOI_SIT_Client16'|




  Scenario Outline: Verify the  functionality of  Back/Forward  button on Application Page and PTC  changed Organisation before clicking forward

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "OrganisationName2":<OrganisationName2>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    When def actFirstAppNameBefore = Resuable.getElementText(app,POM_TPPApp.firstApp)
    * set Result.actFirstAppNameBefore.AppName = "Application name is captured as - " + actFirstAppNameBefore

    And def clickBackward = Resuable.browserNavigations(app,"backward")
    * set Result.clickBackward.Status = clickBackward.Status
    * set Result.clickBackward.Message = clickBackward.Message

    Given def selectOrg = TPPFunctions.PTC_Select_Organisation_WithoutContinueClick(app,data.OrganisationName2)
    And def clickForward = Resuable.browserNavigations(app,"forward")
    * set Result.clickForward.Status = clickForward.Status
    * set Result.clickForward.Message = clickForward.Message

    When def actFirstAppNameAfter = Resuable.getElementText(app,POM_TPPApp.firstApp)
    * set Result.actFirstAppNameAfter.AppName = "Application name is captured as - " + actFirstAppNameAfter
    * match actFirstAppNameBefore == actFirstAppNameAfter


    Examples:
      |GiveOrganisationName|OrganisationName2|
      |'Bank of Ireland TPP - AISP and PISP'|'Bank of Ireland TPP - PISP'|

