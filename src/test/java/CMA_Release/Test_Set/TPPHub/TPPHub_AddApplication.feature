@TPPHubAddApp

Feature: UI Baseline Functional Test Flow for TPP Hub

  Background:
    * def apiApp = new apiapp()
    * def app = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def TPPResFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def TPPLoginFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPLogin')
    * def SelResFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPResFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * json logindetails = ptc.PTC_2
    Given def UserLogin = TPPResFunctions.PTC_Login_with_OB(app,logindetails)
    * def takescreenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
  """
  function(){
   var tem = karate.info;
   if (typeof key != 'undefined'){info.key = key}
   if(tem.errorMessage == null){
          Result.TestStatus = "Pass";
    }else{
          Result.TestStatus = "Fail";
          Result.Error = tem.errorMessage;
   }
   apiApp.write(Result,info);
   app.stop();
   }
  """

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  app.path1 = info1.path


  @RCTest
  Scenario Outline: 01_PTC is able to navigate to Add Application page after selecting Organisation.
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>
    }
    """
    Given def perform = TPPResFunctions.PTC_Select_Organisation(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName|
      |'BOISIT3'|

  Scenario Outline: 02_PTC is able to navigate to Add Application page after selecting Organisation.
    * def app = new webapp
    * json ptc = read('classpath:Resources/PTC/ptc-info.json')
    * json logindetails = ptc.PTC_2
    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>
    }
    """
    * def perform = TPPResFunctions.PTC_Select_Organisation(app,data)
    Given def orgTab = SelResFunctions.getElementText(app,POM_TPPOrg.TppOrg_organizationTab)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |GiveOrganisationName|
      |'BOISIT3'|



