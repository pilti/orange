@TPP_Hub
Feature: Test the forward and backward button functionality of TPP hub application

  Background:

    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')
    * def POM_TPPOrg = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPOrg')

    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2


    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """


  Scenario Outline: Verify that 'Please upload txt file only error while trying to upload wrong format for Software Statement file

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    Given def uploadApplication = TPPFunctions.browseApplication(app,data)
    * set Result.uploadApplication.Status = uploadApplication.Status
    * set Result.uploadApplication.Message = uploadApplication.Message
    Then match uploadApplication.Status == "Pass"



    * def expErrorMessage = <Error_Message>
    Given def actErrorMessage = Resuable.getElementText(app,POM_TPPApp.errorMessageApp)
    * set Result.VerifyMessage.Actual = actErrorMessage
    * set Result.VerifyMessage.Expected = expErrorMessage
    Then match actErrorMessage == expErrorMessage

    Examples:
      |GiveOrganisationName|FilePath|Error_Message|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_wrongFormat.doc'|'Please upload a .txt file only.'|



  @TPP_Hub
  Scenario Outline: Verify that Error File size should not be greater than 50KB for Software Statement file greater that 50KB

   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    Given def uploadApplication = TPPFunctions.browseApplication(app,data)
    * set Result.uploadApplication.Status = uploadApplication.Status
    * set Result.uploadApplication.Message = uploadApplication.Message
    Then match uploadApplication.Status == "Pass"



    * def expErrorMessage = <Error_Message>
    Given def actErrorMessage = Resuable.getElementText(app,POM_TPPApp.errorMessageApp)
    * set Result.VerifyMessage.Actual = actErrorMessage
    * set Result.VerifyMessage.Expected = expErrorMessage
    Then match actErrorMessage == expErrorMessage

    Examples:
      |GiveOrganisationName|FilePath|Error_Message|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_greaterThan50KB.txt'|'File size should not be greater than 50KB.'|




  @TPP_Hub
  Scenario Outline: Verify that Error message as 'Error: Invalid SSA.Please upload a valid SSA.'  if SSA signature is not valid

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName




    Given def uploadApplication = TPPFunctions.browseApplication(app,data)
    * set Result.uploadApplication.Status = uploadApplication.Status
    * set Result.uploadApplication.Message = uploadApplication.Message
    Then match uploadApplication.Status == "Pass"

    When def uploadApplication = TPPFunctions.clickUpload(app)




    * def expErrorMessage = <Error_Message>
    When def verifyAppFailureMessage = TPPFunctions.verifyAppFailureMessage(app,expErrorMessage)
    * set Result.verifyAppFailureMessage.Status = verifyAppFailureMessage.Status
    * set Result.verifyAppFailureMessage.Message = verifyAppFailureMessage.Message
    * set Result.verifyAppFailureMessage.Actual = verifyAppFailureMessage.Actual
    * set Result.verifyAppFailureMessage.Expected = verifyAppFailureMessage.Expected
    Then match verifyAppFailureMessage.Status == "Pass"


    Examples:
      |GiveOrganisationName|FilePath|Error_Message|
      |'Bank of Ireland TPP - AISP and PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_InvalidFile.txt'|'Invalid SSA. Please upload a valid SSA.'|




  Scenario Outline: Verify that Error Message Invalid SSA if SSA token does not belong to selected Organisation

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    * print path

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName": <GiveOrganisationName>,
      "SSAFilePath":<FilePath>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    And def PTC_Select_Organisation = TPPFunctions.PTC_Select_Organisation(app,data)
    * set Result.PTC_Select_Organisation.Status = PTC_Select_Organisation.Status
    * set Result.PTC_Select_Organisation.Message = PTC_Select_Organisation.Message +" - "+ data.OrganisationName

    Given def uploadApplication = TPPFunctions.browseApplication(app,data)
    * set Result.uploadApplication.Status = uploadApplication.Status
    * set Result.uploadApplication.Message = uploadApplication.Message
    Then match uploadApplication.Status == "Pass"
    When def uploadApplication = TPPFunctions.clickUpload(app)

    * def expErrorMessage = <Error_Message>
    When def verifyAppFailureMessage = TPPFunctions.verifyAppFailureMessage(app,expErrorMessage)
    * set Result.verifyAppFailureMessage.Status = verifyAppFailureMessage.Status
    * set Result.verifyAppFailureMessage.Message = verifyAppFailureMessage.Message
    * set Result.verifyAppFailureMessage.Actual = verifyAppFailureMessage.Actual
    * set Result.verifyAppFailureMessage.Expected = verifyAppFailureMessage.Expected
    Then match verifyAppFailureMessage.Status == "Pass"


    Examples:
      |GiveOrganisationName|FilePath|Error_Message|
      |'Bank of Ireland - TPP AISP'|'./src/test/java/CMA_Release/Base_Data/SSA_InvalidFile.txt'|'Invalid SSA. Please upload a valid SSA.'|