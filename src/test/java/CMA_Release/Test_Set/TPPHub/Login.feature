@TPP_Hub


Feature: UI Baseline Functional Test Flow for TPP Hub

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def OBFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.OBDirectory')
    * def TPPTestFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubTest')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2

    * def ob = read('classpath:Resources/OB_DIC/OB.json')
    * def ss = read('classpath:Resources/OB_DIC/Soft_Stmt_template.json')
    * def k = Java.type('CMA_Release.Java_Lib.createFolder')

    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """



  Scenario: One UI test for launch TPP Portal

 # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC01"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    When match launchApp.Status == "Pass"
    Then def perform = app.stop()

  @Functional_Shakedown
  Scenario: Two UI Test to Verify that PTC is able to navigate to OB Portal after clicking on Login with Open Banking
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC02"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.PTC_Navigate_TPP_Through_OBLink(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Three UI Test to Verify that Open Banking link given in Content on Log in page is working as per guidelines
    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC03"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.Content_OB_Link_Check(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Four UI Test to Verify that PTC able to click on Help Link from Header and it is working as expected
  # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC04"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.Header_Help_Link_Check(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Five UI Test to Verify that PTC able to click on Cookies and Priavcy Link from Footer and it is working as expected
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC05"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.Footer_CookieandPrivacy_Link_Check(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario: Six UI Test to Verify that PTC able to click on Help Link from Footer and it is working as expected
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC06"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.Footer_Help_Link_Check(app)
    When match perform.Status == "Pass"
    Then def perform = app.stop()


  Scenario Outline: Seven UI test to Verify PTC is not able to Login with Invalid Username and Password
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC07_"+ <count>
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    * def data =
    """
    {
      "Email": <GivenUserName>,
      "Password":<GivenPassword>
    }
    """
    Given def perform = TPPTestFunctions.PTC_Invalid_FirstFactor_Login_Check_with_OB(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

    Examples:
      |count|GivenUserName|GivenPassword|
      |01   |'Rahul.Chavan@cts.com'|'abc123456'|
      |02   |'RahulMohan.Chavan@boi.com'|abcd12345|


  Scenario: Eight test to Verify PTC is able to Login with valid Username and Password
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC08"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def perform = TPPTestFunctions.PTC_Valid_FirstFactor_Login_Check_with_OB(app,logindetails)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Scenario Outline: Nine Test to verify that PTC is Unable to Login with Invalid OTP as second factor Authentication
   # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC09_"+ <count>
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    * def data =
    """
    {
      "InvalidOTP": <GiveInvalidOTP>
    }
    """

    * def perform = TPPTestFunctions.PTC_Valid_FirstFactor_Login_Check_with_OB(app,logindetails)
    Given def perform = TPPTestFunctions.PTC_Invalid_OTP_SecondFactAuth_Login_OB_Check(app,data)
    When match perform.Status == "Pass"
    Then def perform = app.stop()

  Examples:
  |count|GiveInvalidOTP|
  |01   |'123456'      |
  |02   |'987654'      |


  @TPP_Hub_Sanity
  Scenario: Ten UI Test to Verify that PTC is Successfully Login to TPP Hub and login details are displayed correctly


    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = "TC10"
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl

    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP
    When def perform = TPPFunctions.Verify_PTC_Login_Details(app,logindetails)
    When match perform.Status == "Pass"
    Then def perform = app.stop()