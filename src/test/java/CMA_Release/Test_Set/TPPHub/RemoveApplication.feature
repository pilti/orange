@TPP_Hub
Feature: Test the remove application functionality of TPP hub application

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()
    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2

    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
  }
  """

  Scenario Outline: Verify that PTC  is able to Remove the added application by clicking on YES button of the Alert message and success Message

      # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def selectApplicationForRemoval = TPPFunctions.selectApplicationForRemoval(app,data,true)
    * set Result.selectApplicationForRemoval.Status = selectApplicationForRemoval.Status
    * set Result.selectApplicationForRemoval.Message = selectApplicationForRemoval.Message
    * match selectApplicationForRemoval.Status == "Pass"


    When def headerMsg_Popup = Resuable.getElementText(app,POM_TPPApp.headerMsg_Popup)
    And def expHeaderMessge = "Alert - this action will result in removal of your application from our system."
    * set Result.headerMsg_Popup.Actual = headerMsg_Popup
    * set Result.headerMsg_Popup.Expected = expHeaderMessge
    * match headerMsg_Popup == expHeaderMessge

    When def BodyMsg_Popup1 = Resuable.getElementText(app,POM_TPPApp.BodyMsg_Popup1)
    And def expBodyMessge1 = "This application will no longer have access to our APIs."
    * set Result.BodyMsg_Popup1.Actual = BodyMsg_Popup1
    * set Result.BodyMsg_Popup1.Expected = expBodyMessge1
    * match BodyMsg_Popup1 == expBodyMessge1

    When def BodyMsg_Popup2 = Resuable.getElementText(app,POM_TPPApp.BodyMsg_Popup2)
    And def expBodyMessge2 = "This action cannot be undone. Are you sure you want to continue?"
    * set Result.BodyMsg_Popup2.Actual = BodyMsg_Popup2
    * set Result.BodyMsg_Popup2.Expected = expBodyMessge2
    * match BodyMsg_Popup2 == expBodyMessge2


    When def RemoveApp = TPPFunctions.clickRemoveApplicationButton(app)
    * match RemoveApp.Status == "Pass"
    * set Result.UI.Actual = RemoveApp.Description
    * set Result.UI.Expected = "Application should get removed"


    When def verifyRemovedApp = TPPFunctions.verifyApplicationRemoved(app,data.ApplicationName)
    * set Result.verifyRemovedApp.Status = verifyRemovedApp.Status
    * set Result.verifyRemovedApp.Message = verifyRemovedApp.Message
    * match verifyRemovedApp.Status == "Pass"

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  Scenario Outline: Verify that PTC  is able to click on NO popup tab while Removing the added application by clicking on Remove button

      # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def selectApplicationForRemoval = TPPFunctions.selectApplicationForRemoval(app,data,true)
    * set Result.selectApplicationForRemoval.Status = selectApplicationForRemoval.Status
    * set Result.selectApplicationForRemoval.Message = selectApplicationForRemoval.Message
    * match selectApplicationForRemoval.Status == "Pass"
    When def RemoveApp = TPPFunctions.clickDonotRemoveApplicationButton(app)


    * match RemoveApp.Status == "Pass"
    * set Result.UI.Actual = RemoveApp.Message
    * set Result.UI.Expected = "User should be able to click the 'No - donot remove' button"

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  Scenario Outline: Verify that PTC  is able to click on Remove button of Application  and Below Alert Message is displayed
      # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.ApplicationName
    When def clickRemoveApp = TPPFunctions.clickRemoveClient(app,appName,true)

    When def headerMsg_Popup = Resuable.getElementText(app,POM_TPPApp.headerMsg_Popup)
    And def expHeaderMessge = "Alert - this action will result in removal of your application from our system."
    * set Result.headerMsg_Popup.Actual = headerMsg_Popup
    * set Result.headerMsg_Popup.Expected = expHeaderMessge
    * match headerMsg_Popup == expHeaderMessge

    When def BodyMsg_Popup1 = Resuable.getElementText(app,POM_TPPApp.BodyMsg_Popup1)
    And def expBodyMessge1 = "Alert - this action will result in removal of your application from our system."
    * set Result.BodyMsg_Popup1.Actual = BodyMsg_Popup1
    * set Result.BodyMsg_Popup1.Expected = expBodyMessge1
    * match BodyMsg_Popup1 == expBodyMessge1

    When def BodyMsg_Popup2 = Resuable.getElementText(app,POM_TPPApp.BodyMsg_Popup2)
    And def expBodyMessge2 = "Alert - this action will result in removal of your application from our system."
    * set Result.BodyMsg_Popup2.Actual = BodyMsg_Popup2
    * set Result.BodyMsg_Popup2.Expected = expBodyMessge2
    * match BodyMsg_Popup2 == expBodyMessge2

    When def actBodyMessge = Resuable.getElementText(app,POM_TPPApp.BodyMsg_Popup1)
    * print actBodyMessge
    And def expBodyMessge = "This application will no longer have access to our APIs."
    * match actBodyMessge == expBodyMessge
    * set Result.UI.Actual = actBodyMessge
    * set Result.UI.Expected = expBodyMessge

    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  Scenario Outline: Verify that PTC  is able to Remove the added application by clicking on YES button of the Alert message and success Message

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def selectApplicationForRemoval = TPPFunctions.selectApplicationForRemoval(app,data,true)
    * set Result.selectApplicationForRemoval.Status = selectApplicationForRemoval.Status
    * set Result.selectApplicationForRemoval.Message = selectApplicationForRemoval.Message
    * match selectApplicationForRemoval.Status == "Pass"


    When def headerMsg_Popup = Resuable.getElementText(app,POM_TPPApp.headerMsg_Popup)
    And def expHeaderMessge = "Alert - this action will result in removal of your application from our system."
    * set Result.headerMsg_Popup.Actual = headerMsg_Popup
    * set Result.headerMsg_Popup.Expected = expHeaderMessge
    * match headerMsg_Popup == expHeaderMessge
    When def RemoveApp = TPPFunctions.clickRemoveApplicationButton(app)
    * match RemoveApp.Status == "Pass"
    * set Result.RemoveApp.Actual = RemoveApp.Description
    * set Result.RemoveApp.Expected = "Application should get removed"

    When def selectApplicationForRemoval2 = TPPFunctions.selectApplicationForRemoval(app,data,false)
    * set Result.selectApplicationForRemoval2.Status = selectApplicationForRemoval2.Status
    * set Result.selectApplicationForRemoval2.Message = selectApplicationForRemoval2.Message
    * match selectApplicationForRemoval2.Status == "Pass"

    When def RemoveApp2 = TPPFunctions.clickRemoveApplicationButton(app)
    * match RemoveApp2.Status == "Pass"
    * set Result.RemoveApp2.Actual = RemoveApp2.Description
    * set Result.RemoveApp2.Expected = "Application should get removed"


    Examples:
      | GiveOrganisationName                  | GiveApplicationName |
      | 'Bank of Ireland TPP - AISP and PISP' | 'BOI_SIT_Client16'  |


  @testtoday
  Scenario Outline: Verify that TPP having both AISP and PISP  will NOT  be able to access  any of the BOI APIs after Removing application

    # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "FilePath":<FilePath>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def selectApplicationForRemoval = TPPFunctions.selectApplicationForRemoval(app,data,true)
    * set Result.selectApplicationForRemoval.Status = selectApplicationForRemoval.Status
    * set Result.selectApplicationForRemoval.Message = selectApplicationForRemoval.Message
    * match selectApplicationForRemoval.Status == "Pass"


    When def headerMsg_Popup = Resuable.getElementText(app,POM_TPPApp.headerMsg_Popup)
    And def expHeaderMessge = "Alert - this action will result in removal of your application from our system."
    * set Result.headerMsg_Popup.Actual = headerMsg_Popup
    * set Result.headerMsg_Popup.Expected = expHeaderMessge
    * match headerMsg_Popup == expHeaderMessge
    When def RemoveApp = TPPFunctions.clickRemoveApplicationButton(app)
    * match RemoveApp.Status == "Pass"
    * set Result.RemoveApp.Actual = RemoveApp.Description
    * set Result.RemoveApp.Expected = "Application should get removed"

     # verify the API call is unsuccessful
    Given def Application_Details = active_tpp.<role>

       #Hit Certificate request to server
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)
  #
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_CCG.Input.grant_type = Application_Details.grant_type
    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And set Result.Additional_Details = Application_Details


    * set Result.Access_Token_CCG.Actual.responseStatus = apiApp.Access_Token_CCG.responseStatus
    * set Result.Access_Token_CCG.Expected.responseStatus = 401

    Then match apiApp.Access_Token_CCG.responseStatus == 401


    Examples:
      | GiveOrganisationName                  | GiveApplicationName | FilePath                                                          |role|
      | 'Bank of Ireland TPP - AISP and PISP' | 'PRC_SIT_AISP_PISP' | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP_PISP.txt' | 'PRC_SIT_AISP_PISP'   |
#      |'Bank of Ireland TPP - AISP and PISP'|'PRC_SIT_AISP_PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP.txt'|'PRC_SIT_AISP'|
#      |'Bank of Ireland TPP - AISP and PISP'|'PRC_SIT_AISP_PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_PISP.txt'|'PRC_SIT_PISP'|






