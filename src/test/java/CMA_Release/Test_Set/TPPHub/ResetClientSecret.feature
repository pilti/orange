
@TPP_Hub
Feature: Test the reset client secret functionality of TPP hub application

  Background:
    * json Result = {}
    * def apiApp = new apiapp()
    * def app = new webapp()

    * def TPPFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    * def Resuable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def POM_TPPResetCS = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPResetCS')
    * def POM_TPPApp = Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPApp')

    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def ptc = read('classpath:Resources/PTC/ptc-info.json')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * json logindetails = ptc.PTC_2


    * configure afterScenario =
  """
  function(){
  var tem = karate.info
  if (typeof webDef.key != 'undefined'){info.key = webDef.key}
  if(tem.errorMessage == null){
  Result.TestStatus = "Pass";
  }else{
  Result.TestStatus = "Fail";
  Result.Error = tem.errorMessage;
  app.stop();
  }
  Result = karate.pretty(Result)
  fileWriter.write(path,Result);
   app.stop();
  }
  """




  Scenario Outline: Verify that PTC is able to click on Reset button of Client Secrete

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """


    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP

    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def clickReset = TPPFunctions.clickResetClient(app,appName)
    And def actResetHeaderMsg = Resuable.getElementText(app,POM_TPPResetCS.TopAlert_Msg)
    * print "ACTUAL header -------"actResetHeaderMsg
    * def expResetHeaderMsg = "Alert - this action will impact applications using this client secret."
    * match actResetHeaderMsg == expResetHeaderMsg
    * set Result.UI.Actual = actResetHeaderMsg
    * set Result.UI.Expected = expResetHeaderMsg
    And def actResetBodyMsg = Resuable.getElementText(app,POM_TPPResetCS.ContinueAlert_Msg)
    * print "ACTUAL body -------"actResetBodyMsg
    * def expResetBodyMsg = "This action cannot be reversed. Are you sure you want to continue?"
    * match actResetBodyMsg == expResetBodyMsg
    * set Result.UI.Actual = actResetBodyMsg
    * set Result.UI.Expected = expResetBodyMsg


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|


  Scenario Outline: Verify that Success Message Success Client Secret for application Application Name was reset successfully after successfully resetting Client Secrete ID.

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    * def appName = data.OrganisationName
     Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
     When def searchApp = TPPFunctions.Search_Application_From_Existing_List(app,data)
    When def searchApp = TPPFunctions.Sucessfully_Reset_ClientSecrete_For_Given_Application(app,data)

    Then def actResetIdSuccessMsg = Resuable.getElementText(app,POM_TPPResetCS.ResetSuccess_Msg)
    * print "ACTUAL header -------"actResetHeaderMsg
    * def expResetIdSuccessMsg = "Success: Client secret for application"
    * set Result.UI.Actual = actResetIdSuccessMsg
    * set Result.UI.Expected = expResetIdSuccessMsg
    * match actResetIdSuccessMsg contains expResetIdSuccessMsg



   Examples:
   |GiveOrganisationName                 |GiveApplicationName|
   |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|

  Scenario Outline: Verify that PTC  is able to retain on same page after clicking on Cancel button of Reset Client Secrete  Alert

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "ResetMethod":<ResetMethod>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    And def searchApp = TPPFunctions.Search_Application_From_Existing_List(app,data)
    When def cancelReset = TPPFunctions.Cancel_Reset_ClientSecrete_For_Given_Application(app,data)
    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * set Result.UI.Actual = "User is navigated back successfully to Applications page after clicking the cancel button"
    * set Result.UI.Expected = "User should navigated back successfully to Applications page after clicking the cancel button"


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|ResetMethod|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|cancel            |
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|close            |


  Scenario Outline: Verify that PTC  is able to retain on same page after clicking on Close button of Reset Client Secrete  Alert

     # Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "ResetMethod":<ResetMethod>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    And def searchApp = TPPFunctions.Search_Application_From_Existing_List(app,data)
    When def cancelReset = TPPFunctions.Cancel_Reset_ClientSecrete_For_Given_Application(app,data)
    * print cancelReset
    Then def elePresent = Resuable.CheckElementEnabled(app,POM_TPPApp.AddApp_Button)
    * print elePresent
    * set Result.UI.Actual = "User is navigated back successfully after clicking the cancel button"
    * set Result.UI.Expected = "User should navigated back successfully after clicking the cancel button"


    Examples:
      |GiveOrganisationName                 |GiveApplicationName|ResetMethod|
      |'Bank of Ireland TPP - AISP and PISP'|'BOI_SIT_Client16'|close            |



  @testtoday
  Scenario Outline: Verify that TPP having both AISP and PISP  will NOT  be able to access  any of the BOI APIs with OLD Client Secrete after Resetting Client Secrete

# Define Result path/Output folder
    * def webDef = karate.info
    * set webDef.Screens = 'Yes'
    * set webDef.subset = karate.info.scenarioName
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'

# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    * def data =
    """
    {
      "OrganisationName":<GiveOrganisationName>,
      "ApplicationName":<GiveApplicationName>,
      "FilePath":<FilePath>
    }
    """

    * def launchBrowser = call  app.driver1 = app.start1(default_browser)
    * set Result.launchBrowser.default_browser = default_browser
    * def launchApp = TPPFunctions.Launch_TPP_Hub(app,TPPHubUrl)
    * set Result.launchApp.Url = TPPHubUrl
    Given def PTC_Login_with_OB = TPPFunctions.PTC_Login_with_OB(app,logindetails)
    * set Result.PTC_Login_with_OB.Status = PTC_Login_with_OB.Status
    * set Result.PTC_Login_with_OB.Message = PTC_Login_with_OB.Message
    * set Result.PTC_Login_with_OB.Username = PTC_Login_with_OB.Username
    * set Result.PTC_Login_with_OB.OTP = PTC_Login_with_OB.OTP


    Given def perform = TPPFunctions.PTC_Select_Organisation(app,data)
    When def perform = TPPFunctions.Search_Application_From_Existing_List(app,data)
    * def appName = data.OrganisationName
    When def searchApp = TPPFunctions.Sucessfully_Reset_ClientSecrete_For_Given_Application(app,data)

    Then def actResetIdSuccessMsg = Resuable.getElementText(app,POM_TPPResetCS.ResetSuccess_Msg)
    * print "ACTUAL header -------"actResetHeaderMsg
    * def expResetIdSuccessMsg = "Success: Client secret for application"
    * set Result.UI.Actual = actResetIdSuccessMsg
    * set Result.UI.Expected = expResetIdSuccessMsg
    * match actResetIdSuccessMsg contains expResetIdSuccessMsg

 # verify the API call is unsuccessful
    Given def Application_Details = active_tpp.<role>

   #Hit Certificate request to server
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)
#
    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_CCG.Input.grant_type = Application_Details.grant_type
    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And set Result.Additional_Details = Application_Details


    * set Result.Access_Token_CCG.Actual.responseStatus = apiApp.Access_Token_CCG.responseStatus
    * set Result.Access_Token_CCG.Expected.responseStatus = 401

    Then match apiApp.Access_Token_CCG.responseStatus == 401


    Examples:
      | GiveOrganisationName                  | GiveApplicationName | FilePath                                                          |role|
      | 'Bank of Ireland TPP - AISP and PISP' | 'PRC_SIT_AISP_PISP' | './src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP_PISP.txt' | 'PRC_SIT_AISP_PISP'   |
#      |'Bank of Ireland TPP - AISP and PISP'|'PRC_SIT_AISP_PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_AISP.txt'|'PRC_SIT_AISP'|
#      |'Bank of Ireland TPP - AISP and PISP'|'PRC_SIT_AISP_PISP'|'./src/test/java/CMA_Release/Base_Data/SSA_PRC_SIT_PISP.txt'|'PRC_SIT_PISP'|




