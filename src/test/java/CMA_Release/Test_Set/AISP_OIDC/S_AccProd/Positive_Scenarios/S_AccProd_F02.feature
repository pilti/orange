@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus
Feature:  Verify response headers of Get Product Information API (/accounts/{AccountId}/product API)

  Background:
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'Generic_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  @Regression
  Scenario: To verify that content type header in response of Single account Product API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Get Single Account Products
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/product'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    When call apiApp.Account_Product(inputJSON)
    And remove inputJSON $.Content_type
    And set Result.Account_Product.InputForScenario = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And match apiApp.Account_Product.responseHeaders['Content-Type'][0] == 'application/json;charset=UTF-8'

  @Regression
  Scenario: To verify value of x fapi interaction id header in response is same as sent in request header of Product API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Get Single Account Products
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/product'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    When call apiApp.Account_Product(inputJSON)
    And remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    Then match apiApp.Account_Product.responseHeaders['x-fapi-interaction-id'][0] == '93bac548-d2de-4546-b106-880a5018460d'

  @Regression
  Scenario: To verify x fapi interaction id response header with unique uuid is generated when it is not sent in Product API request

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token

    * remove inputJSON $.refresh_token
    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Get Single Account Products
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + 'product'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    When call apiApp.Account_Product(inputJSON)
    And remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Account_Product.InputForScenario = inputJSON
    Then match apiApp.Account_Product.responseHeaders['x-fapi-interaction-id'][0] == '#uuid'

  @Regression
  Scenario: To verify that API platform returns Http Status code 404 Not Found when Bulk Products API is triggered

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    * remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    #Get bulk Account Products
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = BulkApiUrl + 'products'
    When call apiApp.Bulk_Product(inputJSON)
    Then match apiApp.Bulk_Product.responseStatus == 404


  Scenario Outline: Test #key#
    * set Result $.TestSetDescription = "To verify that Product API returns ProductIdentifier according to the Jurisdiction and Product Type"
    * def subset = "ProductIdentifier"
    * set Result $.TestCondition = "When Jurisdiction is <Jurisdiction> & Product Type is <ProductType> then value of ProductIdentifier field contain <ProductIdentifier> column of DB"
    * def key = <TCNO> + ' ' + Result.TestCondition

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token

    * remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = (CMA_Rel_Ver == '2.0' ? AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account[0].Identification : AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification)
    And print AccountNo

    * def NSC = ( AccountNo.substring(0,6) )
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )

    #Get Single Account Products
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/product'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And def query = "JURISDICTION= '<Jurisdiction>',MARKET_CLASSIFICATION = '<MarketClValue>' "
    And def updateABT = ABT_DB.update_ABT_Details(NSC,AcNo,ABT_DB_Conn, query)
    And json getProducts = ABT_DB.get_ABT_Details(NSC,AcNo,ABT_DB_Conn)
    When call apiApp.Account_Product(inputJSON)
    Then match apiApp.Account_Product.responseStatus == 200

    And def ProductType = apiApp.Account_Product.response.Data.Product[0].ProductType
    And def PT = (CMA_Rel_Ver == '2.0' ? (ProductType == '<ProductType_2>' ? PT = "True" : PT = "False") : (ProductType == '<ProductType_1>' ? PT = "True" : PT = "False" ) )
    And match PT == "True"

    And def PI = (CMA_Rel_Ver == '2.0' ? apiApp.Account_Product.response.Data.Product[0].ProductId : apiApp.Account_Product.response.Data.Product[0].ProductIdentifier )
    And match PI == getProducts[0].<ProductIdentifier>

    Examples:
      | TCNO | Jurisdiction | MarketClValue | ProductType_1 | ProductType_2          | ProductIdentifier |
      | 01   | N            | 962           | PCA           | PersonalCurrentAccount | SUB_TYPE          |
      | 02   | N            | 959           | BCA           | BusinessCurrentAccount | FEE_RATE_CATEGORY |
      | 03   | B            | 962           | PCA           | PersonalCurrentAccount | SUB_TYPE          |
      | 04   | B            | 959           | BCA           | BusinessCurrentAccount | SUB_TYPE          |


  Scenario Outline: Test #key#
    * set Result $.TestSetDescription = "To verify that FS Product API returns ProductType as PCA if Market Classification value is inclusive 960 to 972 otherwise returns ProductType as BCA"
    * def subset = "ProductType"
    * set Result $.Condition = "If Market Classification value is <MarketClValue> then FS should return ProductType as <ProductType>"
    * def key = <TCNO> + ' ' + Result.Condition

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    And set Result $.Condition = "If Market Classification value is <MarketClValue> then FS should return ProductType as <ProductType>"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token

    * remove inputJSON $.refresh_token

  #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[1].AccountId
  #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[1].Account.Identification
    And def AccountNo = (CMA_Rel_Ver == '2.0' ? AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account[0].Identification : AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification)

    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
  #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )


  #Get Single Account Products
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/product'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And def query = "MARKET_CLASSIFICATION = '<MarketClValue>'"
    And print '<MarketClValue>'
    And def updateABT = ABT_DB.update_ABT_Details(NSC,AcNo,ABT_DB_Conn, query)
    When call apiApp.Account_Product(inputJSON)
    And set Result.Account_Product.InputForScenario = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And def ProductType = apiApp.Account_Product.response.Data.Product[0].ProductType
    And def PT = (CMA_Rel_Ver == '2.0' ? (ProductType == '<ProductType_2>' ? PT = "True" : PT = "False") : (ProductType == '<ProductType_1>' ? PT = "True" : PT = "False" ) )
    And match PT == "True"


    Examples:
      | TCNO | MarketClValue | ProductType_1 | ProductType_2          |
      | 01   | 000           | BCA           | BusinessCurrentAccount |
      | 02   | 959           | BCA           | BusinessCurrentAccount |
      | 03   | 960           | BCA           | BusinessCurrentAccount |
      | 04   | 961           | BCA           | BusinessCurrentAccount |
      | 05   | 962           | BCA           | BusinessCurrentAccount |
      | 06   | 963           | BCA           | BusinessCurrentAccount |
      | 07   | 964           | BCA           | BusinessCurrentAccount |
      | 08   | 965           | BCA           | BusinessCurrentAccount |
      | 09   | 966           | BCA           | BusinessCurrentAccount |
      | 10   | 967           | BCA           | BusinessCurrentAccount |
      | 11   | 968           | BCA           | BusinessCurrentAccount |
      | 12   | 969           | BCA           | BusinessCurrentAccount |
      | 13   | 970           | BCA           | BusinessCurrentAccount |
      | 14   | 971           | BCA           | BusinessCurrentAccount |
      | 15   | 972           | BCA           | BusinessCurrentAccount |
      | 16   | 973           | BCA           | BusinessCurrentAccount |
      | 17   | 999           | BCA           | BusinessCurrentAccount |


