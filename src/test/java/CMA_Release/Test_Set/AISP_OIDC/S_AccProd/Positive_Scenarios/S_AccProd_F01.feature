@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature:  Get Product Information for consented Account by using /accounts/{AccountId}/product API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    #* def fileName = 'Only_Products_permission_P_User_1.properties'
    * def fileName = 'Generic_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario: To verify TPP is able to call Single Account Product API and gets 200 OK response status

 # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token


    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account Products
    * remove reqHeader $.fromBookingDateTime
    * remove reqHeader $.toBookingDateTime
    * remove reqHeader $.x-fapi-customer-last-logged-time
    * remove reqHeader $.x-fapi-customer-ip-address
    * remove reqHeader $.x-fapi-interaction-id
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/product'
    When call apiApp.Account_Product(inputJSON)
    And set Result.Account_Product.InputForScenario = inputJSON
    Then set Result.Account_Product.responseStatus = apiApp.Account_Product.responseStatus
    And set Result.Account_Product.response = apiApp.Account_Product.response
    And match apiApp.Account_Product.responseStatus == 200


  Scenario: Get Product Information for an account by calling Single Account Product API with mandatory and optional headers

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """

    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token

      #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

      #Call Single account Beneficiaries API with the AccountID
    Given set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    And set reqHeader $.Accept = 'application/json'
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.RequestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then set Result.Account_Product.responseStatus = apiApp.Account_Product.responseStatus
    And set Result.Account_Product.response = apiApp.Account_Product.response
    And match apiApp.Account_Product.responseStatus == 200


  Scenario: Get Products Information for an account by calling Single Account Product API only with mandatory headers


    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And remove inputJSON $.refresh_token

    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And set Result.Account_Product.responseStatus = apiApp.Account_Product.responseStatus
    And set Result.Account_Product.response = apiApp.Account_Product.response


    @Anu85555
  Scenario: To verify the response structure of Get Single Account Product API is CMA compliant

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And def json_old =
     """
    { Data: {
          Product: [
              {
                  AccountId:'#string',
                  ProductIdentifier: '#string',
                  ProductType: '#string'
              }
           ]
    },
    Links: { Self: '#string' },
    Meta: { TotalPages: 1 } }
    """

    * def json_new =
      """
    { Data: {
          Product: [
              {
                  AccountId:'#string',
                  ProductId: '#string',
                  ProductType: '#string'
              }
           ]
    },
    Links: { Self: '#string' },
    Meta: { TotalPages: 1 } }
    """

    And def StructureToValidate = (CMA_Rel_Ver == '2.0' ? json_new : json_old)
    And match apiApp.Account_Product.response == StructureToValidate


  Scenario: Validate that response for GET Accounts products contains the parameter ProductType with value PCA or BCA only

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And def ProductType = apiApp.Account_Product.response.Data.Product[0].ProductType
    And def PT = (CMA_Rel_Ver == '2.0' ? (ProductType == "PersonalCurrentAccount" || ProductType == "BusinessCurrentAccount" ? PT = "True" : PT = "False") : (ProductType == "PCA" || ProductType == "BCA" ? PT = "True" : PT = "False" ) )
    And match PT == "True"


  Scenario: To verify that the response of Get Account Products API contains the mandatory field Links in appropriate format

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And match apiApp.Account_Product.response.Links ==  {"Self": '#string' }
    And set Result.Account_Product.responseStatus = apiApp.Account_Product.responseStatus
    And set Result.Account_Product.response = apiApp.Account_Product.response


  Scenario: To verify that the response of Get Account Products API contains a mandatory field Meta in the payload

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    #Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Product(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    Then match apiApp.Account_Product.responseStatus == 200
    And match apiApp.Account_Product.response.Meta == { TotalPages: 1}
    And set Result.Account_Product.responseStatus = apiApp.Account_Product.responseStatus
    And set Result.Account_Product.response = apiApp.Account_Product.response