@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature: Verify error code in response for Client Credential mismatch in Product API request

Background:
  * def apiApp = new apiapp()
  * json Result = {}
  * set Result.Testname = null
  * set Result.TestStatus = 'Fail'
  * def fileName = 'Only_Products_permission_P_User_1.properties'
  * def name = 'Consent'
  * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
  * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  * def info = karate.info
  * def Application_Details = active_tpp.AISP_PISP
  * call apiApp.configureSSL(Application_Details)
  * set Application_Details $.grant_type = "refresh_token"
  * set Application_Details $.request_method = 'POST'
  * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
  * set Application_Details $.refresh_token = profile.refresh_token
  * call apiApp.Access_Token_RTG(Application_Details)
  * set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Multi_Account_Information.response
  * set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Multi_Account_Information.responseS
  * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
  * match apiApp.Access_Token_RTG.responseStatus == 200
  * set Application_Details $.request_method = "GET"
  * set Application_Details $.token_type = 'Bearer'
  * call apiApp.Multi_Account_Information(Application_Details)
  * set Result.ActualOutput.MultiAccountInfo.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
  * set Result.ActualOutput.MultiAccountInfo.Output.response = apiApp.Access_Token_RTG.response
  * set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
  * match apiApp.Multi_Account_Information.responseStatus == 200

  * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify HTTP status code 401 when client credential token instead of desired oAuth token is passed in Authorization header

  Given set Application_Details.request_method = 'POST'
  And set Application_Details $.grant_type = "client_credentials"
  And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
  * def temp1 = call read('classpath:CMA_Release/Entities_API/Auth_Tokens/OIDC/Access_Token_CCG.feature') Application_Details
  * def Token = temp1.response.access_token
  * set Application_Details.access_token = temp1.response.access_token
  And set Application_Details.request_method = 'GET'
  When call apiApp.Account_Product(Application_Details)
  Then match apiApp.Account_Product.responseStatus == 401


    Scenario Outline: Validate CID matches with client network certificate client_id associated with TPP having #key# role
    * def info = karate.info
    * def key = '<TPP>'
    * set info.key = key
   # Input client_id associated with different TPP in API request
    Given set Application_Details $.client_id = active_tpp.<TPP>.client_id
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 200

    Examples:
      |TPP|
      |AISP|
      |PISP|


  Scenario Outline: Validate Client credentials match with client network certificate associated with #key# TPP

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client credentials validation - TPP'
    # No SSl configured
    # Input client_credentials associated with different TPP in API request
    Given set Application_Details $.client_id = active_tpp.<TPP>.client_id
    And set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 200


    Examples:
      |TPP|
      |AISP|
      |PISP|


    Scenario: Verify whether error is received when refresh_token is sent in API request

    * def info = karate.info

    #Send refresh token instead of access_token in API request
    Given set Application_Details $.access_token = profile.refresh_token
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.request_method = 'GET'
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 401


