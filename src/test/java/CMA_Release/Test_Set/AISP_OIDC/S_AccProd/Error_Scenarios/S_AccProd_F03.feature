Feature: Invalid or mandatory value check for optional and mandatory headers for Get Single Account Product API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
    * def fileName = 'Only_Products_permission_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * def Application_Details = active_tpp.AISP_PISP
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Result.ActualOutput.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    * set Result.ActualOutput.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details $.token_type = 'Bearer'
    * set Application_Details $.request_method = 'GET'
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Result.ActualOutput.MultiAccountInfo.Output.response = apiApp.Multi_Account_Information.response
    * set Result.ActualOutput.MultiAccountInfo.Output.responseStatus = apiApp.Multi_Account_Information.responseStatus
    * set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: Verify that HTTP status code 401 is received in the response when #key# TPP credentials are passed in Get Single Account Product API request

    * def key = <condition>
    * set info.subset = 'Invalid or blank TPP credentials'
    * set info.key = <condition>
    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * remove reqHeader.client_id
    * remove reqHeader.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers <tpp_credentials>
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    And match responseStatus == 200

    Examples:
      |count|condition|tpp_credentials|
      |01|'Empty Client id is passed in the request'|{"client_id": ' ', "client_secret": 'D5e1567dc0884CFdB0D16ae90754A995'}|
      |02|'Empty Client_secret is passed in the request'|{"client_id": 'e66ca87dab0349f9bc5f0fc62cbdaf9f', "client_secret": ' '}|
      |03|'Empty client_id and client_secret are passed in the request'|{"client_id": ' ', "client_secret": ' '}|
      |04|'Invalid client id is passed in the request'|{"client_id": 'fhfdhgfd', "client_secret": 'D5e1567dc0884CFdB0D16ae90754A995'}|
      |05|'Invalid client secret is passed in the request'|{"client_id": 'e66ca87dab0349f9bc5f0fc62cbdaf9f',"client_secret": 'D5e1567dc0884CFdB0D16ae90754A9951234'}|
      |06|'Invalid client id and client secret are passed in the request'|{"client_id": 'dgsdfhfdhgfd', "client_secret": 'fhdfhdhgg'}|


  Scenario: Verify that HTTP status code 401 is received when Get Single Account Product API request is sent without mandatory header client_id
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * def AccountId = Application_Details.AccountId
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret
    * remove reqHeader.client_id

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    Then match responseStatus == 200


  Scenario: Verify that HTTP status code 401 is received when Get Single Account Product API request is sent without mandatory header client_secret
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * def AccountId = Application_Details.AccountId
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret
    * remove reqHeader.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then match responseStatus == 200


  Scenario: Verify that HTTP status code 401 is received when Get Single Account Product API request is sent without mandatory header client_id and client_secret

    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret
    * remove reqHeader.client_id
    * remove reqHeader.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then match responseStatus == 200

  Scenario: Verify that HTTP status code 401 is received when Get Single Account Product API request is sent without mandatory header Authorization

    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    When method GET
    Then match responseStatus == 401

  Scenario: Verify that HTTP Status code 401 is received in the response when blank value of mandatory Authorization header is passed in Get Single Account Product request

    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + ' '
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then match responseStatus == 401


  Scenario: Verify that HTTP Status code 401 is received in the response when expired token is passed in Authorization header in Get Single Account Product API

    Given def AccountId = Application_Details.AccountId
    And set reqHeader $.client_id = Application_Details.client_id
    And set reqHeader $.client_secret = Application_Details.client_secret
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)
    When call apiApp.Account_Product(Application_Details)
    Then match responseStatus == 401


  Scenario: Verify Single account info gets retrieved with HTTP status code 200 is received in response when token which is about to expire(298secs of delay) passed in Get Single Account Product API

    Given def AccountId = Application_Details.AccountId
    And set reqHeader $.client_id = Application_Details.client_id
    And set reqHeader $.client_secret = Application_Details.client_secret
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(298000)
    When call apiApp.Account_Product(Application_Details)
    Then match responseStatus == 200


  Scenario: Verify HTTP status code 401 in response when invalid Authorization header(includes extra or special chars) is passed in Product API

    * def AccountId = Application_Details.AccountId
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret
    * def auth = type + Application_Details.access_token + 'dgf$#'

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then match responseStatus == 401


  Scenario: Verify HTTP status code 401 is received in response when refresh token instead of access token is passed in Authorization header of Get Single Account Product API request

    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + profile.refresh_token
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    And request permissions
    When method GET
    Then match responseStatus == 401


  Scenario Outline: Verify HTTP status code 401 is received when #key# token type is passed in Get Single Account Product request

    * def key = <condition>
    * set info.key = <condition>
    * def AccountId = Application_Details.AccountId
    * def Token = apiApp.Access_Token_RTG.response.access_token
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret

    Given url AcctInfoUrl + AccountId + '/product'
    And headers reqHeader
    And header Authorization = <type> + Token
    And request permissions
    When method GET
    Then match responseStatus == 401

    Examples:
      |count|condition|type|
      |001  |'Missing token type in the request'|" "|
      |002  |'Misspelled token type append single extra char to Bearer'|"Bearerr"|
      |003  |'Invalid token type- append multiple chars to Bearer'|"Bearerdfgdfhdh"|
      |004  |'Invalid token type- append multiple aphanumeric chars to Bearer'|"Bearer5757"|
      |005  |'Invalid token type- append multiple special chars to Bearer'|"Bearer$%&%4gd"|
      |006  |'Invalid token type- placed multiple alphanumeric chars in between Bearer'|"Beare577dgdfr"|
      |007  |'Invalid token type- placed multiple alphanumeric and special chars in between Bearer'|"Bear%^*577dfgfddgdfr"|
      |008  |'Lower case token type'                      |"bearer" |
      |009  |'Token type other than Bearer '              |"Basic" |



  Scenario Outline: Verify expected HTTP status code is received when #key# Accept header is passed in Get Single Account Product API

    * def key = <condition>
    * set info.key = <condition>

    Given def AccountId = Application_Details.AccountId
    And def Token = apiApp.Access_Token_RTG.response.access_token
    And set reqHeader $.client_id = Application_Details.client_id
    And set reqHeader $.client_secret = Application_Details.client_secret
    And set reqHeader.Accept = <accept>
    When call apiApp.Account_Product(Application_Details)
    Then match responseStatus == <expected_responseStatus>

    Examples:
      |count|condition|accept|expected_responseStatus|
     |001|'Invalid value of accept passed in the request'|'application/json/fDFGdfhdf'|406|
      |002|'Blank value of accept passed in the request' |' '|200|
     |003|'Invalid value of accept passed in the request'|'dgs&* '|406|

  @severity=normal
  @P0313
  Scenario Outline: Verify expected HTTP status code is received when financial Id is #key# in Get Single Account Product request

    * def key = <condition>
    * set info.key = <condition>

    Given def AccountId = Application_Details.AccountId
    And def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    And set reqHeader $.client_id = Application_Details.client_id
    And set reqHeader $.client_secret = Application_Details.client_secret
    And set reqHeader $.x-fapi-financial-id = <Bank_Id>
    When call apiApp.Account_Product(Application_Details)
    Then match responseStatus == <expected_responseStatus>

    Examples:
      |count|condition|Bank_Id|expected_responseStatus|
      |001|'Invalid value of financial id is passed in the request'|'UNIQUE74565'|403|
      |002|'Empty financial id is passed in the request '|' '|400|


  Scenario: Verify that HTTP status code 400 is received when Get Single Account Product request is sent without mandatory header Financial id(bank id)

    Given def AccountId = Application_Details.AccountId
    And def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    And set reqHeader $.client_id = Application_Details.client_id
    And set reqHeader $.client_secret = Application_Details.client_secret
    And remove reqHeader.x-fapi-financial-id
    When call apiApp.Account_Product(Application_Details)
    Then match responseStatus == 400