@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature: Access Token validation for Get Single Account Product Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  Scenario: Verify whether error is received when access token generated using auth_token not associated with requesting consent is sent in API request

    * def info = karate.info
    * def prerequisite = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    * json Result = prerequisite.Result1
    * def fileName = 'Generic.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   # TPP processes another consent and generates access_token from refresh_token
    Given def Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'

    #TPP triggers Standing order API request for account ID for which the consent is provided ,however access_token generated from a different consent
    Given set Application_Details.token_type = 'Bearer'
    And set Application_Details.access_token = prerequisite.apiApp.Access_Token_ACG.response.access_token
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403



  Scenario Outline:Verify whether error is received when access_token not associated with TPP having #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access Token validation'

    # TPP1 creates a new access token from refresh_token
    Given def Application_Details = active_tpp.<TPP1>
    When call apiApp.configureSSL(Application_Details)

    * def fileName = <Consent_TPP1 specific>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.TPP1_Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.TPP1_Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

 # TPP2 creates a new access token from refresh_token to fetch account details
    * def apiApp1 = new apiapp()
    And def Application_Details = active_tpp.<TPP2>
    When call apiApp1.configureSSL(Application_Details)

    * def fileName = <Consent_TPP2 specific>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.TPP2_Access_Token_RTG.Output.responseStatus = apiApp1.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.TPP2_Access_Token_RTG.Output.response = apiApp1.Access_Token_RTG.response

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp1.Multi_Account_Information(Application_Details)
    Then set Result.ActualOutput.MultiAccountInfo.Output.response = apiApp1.Multi_Account_Information.response
    Then set Result.ActualOutput.MultiAccountInfo.Output.responseStatus = apiApp1.Multi_Account_Information.responseStatus

    Given set Application_Details.AccountId = apiApp1.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    # TPP2 triggers API with access_token associated with TPP1
    # updated with defect 1335
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 401
    And set Result.ActualOutput.Account_Product.Input.URL = Application_Details.URL
    And set Result.ActualOutput.Account_Product.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Account_Product.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Account_Product.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Account_Product.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Account_Product.Output.responseStatus =  apiApp.Account_Product.responseStatus
    And set Result.ActualOutput.Account_Product.Output.response =  apiApp.Account_Product.response
    And set Result.DefectId = '1335'
    And set Result.Additional_Details = Application_Details


    Examples:
      | key                  | TPP1      | TPP2      | Consent_TPP1 specific                 | Consent_TPP2 specific                 |
      | 'AISP and PISP role' | AISP      | AISP_PISP | 'Generic_AISPTPP_P_User_1.properties' | 'Generic_P_User_1.properties'         |
      | 'AISP role'          | AISP_PISP | AISP      | 'Generic_P_User_1.properties'         | 'Generic_AISPTPP_P_User_1.properties' |




