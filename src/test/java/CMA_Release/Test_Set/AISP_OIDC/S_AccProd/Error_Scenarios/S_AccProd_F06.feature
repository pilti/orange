@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature: Validate error Code in response of Products API for revoked consent

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
    * set info.subset = info.scenarioName
    * json Application_Details = active_tpp.AISP_PISP
    * def fileName = 'RevokedConsent_P_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Validate error Code in response of Products API for revoked consent
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.token_type = 'Bearer'
    And def AccountId = profile.AccountId
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403

