@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature:  Scope and TPP validations for Get Single Account Product API

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
    * def fileName = 'Only_Products_permission_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    * set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response
    * set Application_Details $.request_method = "GET"
    * set Application_Details $.token_type = 'Bearer'
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
    """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

Scenario Outline: Verify HTTP status code 200 is received when scope #key# is passed in Product API request

    * def key = <condition>
    * set info.subset = 'Invalid/CValid Scope in Product API'
    * set info.key = <condition>

    Given set Application_Details $.scope = <scope>
    And set reqHeader $.scope = <scope>
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 200
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'


    Examples:
      | count | condition                          | scope       |
      | 01    | 'Input empty scope'                | " "         |
      | 02    | 'Input invalid scope as accounts1' | "accounts1" |
      | 03    | 'Input invalid scope as accounts$' | "accounts$" |
      | 04    | 'Input valid scope as payments'    | "payments"  |


Scenario Outline:Verify whether error is received when account ID not associated with consent having #key# is passed in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'AccountId validation'
    * def fileName1 = <Consent>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName1,name)
    * def apiApp1 = new apiapp()

    Given set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details.request_method = 'POST'
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    When call apiApp1.Multi_Account_Information(Application_Details)
    * print """""""""""""""""""
    * print apiApp1.Multi_Account_Information.response

   #AccountId not associated with originating consent is being sent in the request
    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
   # TPP triggers Product API for Account ID not associated with originating consent
    When call apiApp.Account_Product(Application_Details)
    And match apiApp.Account_Product.responseStatus == 403

    Examples:
      | key                   | Consent                       |
      | 'Generic Permissions' | 'Generic_P_User_1.properties' |
      | 'Generic Permissions' | 'Generic_P_User_2.properties' |


  Scenario Outline: Verify HTTP status code 403 is received when oAuth Token not associated with consent is passed in GET Single Account Product API
    * def info = karate.info
    * def key = <key>
    * set info.subset = 'AuthToken from other consent'
    #TPP generates new access token to get Product information
    * def fileName1 = <Consent>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName1,name)
    * def apiApp1 = new apiapp()

    Given set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details.request_method = 'POST'
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    When call apiApp.configureSSL(Application_Details)
    And call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'


    Examples:
      | key                   | Consent                       |
      | 'Generic Permissions' | 'Generic_P_User_1.properties' |
      | 'Generic Permissions' | 'Generic_P_User_2.properties' |


  Scenario: Verify HTTP status code 403 when account ID not associated with TPP initiating request is passed in GET Single Account Product API

    Given def Application_Details1 = active_tpp.AISP
    When call apiApp.configureSSL(Application_Details1)

  # TPP generates access token from refresh token which is not associated with originating consent
    * def fileName = 'Generic_AISPTPP_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def apiApp1 = new apiapp()

    Given set Application_Details1 $.grant_type = "refresh_token"
    And set Application_Details1 $.request_method = 'POST'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details1 $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details1)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200

    Given set Application_Details1.request_method = 'GET'
    And set Application_Details1.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details1.access_token = apiApp1.Access_Token_RTG.response.access_token
    And set Application_Details1.token_type = 'Bearer'
    * def temp1 = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Multi_Account_Information.feature') Application_Details1
    And def AccountId_AISP = temp1.response.Data.Account[0].AccountId
    And set Application_Details.AccountId = AccountId_AISP

  #configuring SSl again
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.request_method = 'GET'
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    And set Result.ActualOutput.Reference = 'Defect_1267'

  Scenario: Verify 403 error when access token generated using refresh_token not associated with requesting consent is sent in API request

    * def info = karate.info
  # TPP generates access token from refresh token which is not associated with originating consent
    * def fileName = 'Generic_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def apiApp1 = new apiapp()

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
 # TPP triggers Product API with access token not associated with originating consent
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403


  Scenario: Verify HTTP status code 401 when oAuth Token and account ID is of TPP having AISP_PISP role but Product API request has TPP with PISP role
    Given def info = karate.info
    * set info.subset = 'PISP TPP Role in Product API with NO PISP SSl configured'
     #with another TPP
     #Not Configuring SSl for new TPP
    Given def Application_Details = active_tpp.PISP
    And set Application_Details.client_id = Application_Details1.client_id
    And set Application_Details.client_secret = Application_Details1.client_secret
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 200
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'


  Scenario: Verify HTTP status code 403 when oAuth Token ,account ID is for AISP_PISP role but Product API has PISP Role SSl configured
    * def info = karate.info
    * set info.subset = 'PISP TPP Role in Product API with PISP SSL'
  #with another TPP
    Given def Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)
    And call apiApp.Account_Product(Application_Details)
    And set Result.Account_Product.InputForScenario = Application_Details
    Then match apiApp.Account_Product.responseStatus == 403
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    And set Result.ActualOutput.Reference = 'Defect_1267'


  Scenario Outline: Verify whether error is received when #key# value of access token is sent in API request

    * def info = karate.info
    * def key =  <key>
    * set info.subset = 'Access Token validation'

 # input blank or invalid values in token
    Given set Application_Details.token_type = <type>
    And set Application_Details.access_token = <token>
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 401

    Examples:
      | key       | token                             | type     |
      | 'Empty'   | ' '                               | ' '      |
      | 'Invalid' | 'VQsz3pnZ41j0cGD4QryeWIiwGNRa$%$' | 'Bearer' |