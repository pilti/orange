@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature:  Account Status, Credit grade validation for the Account associated with Account ID

  Background:

    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
    * def fileName = 'Generic_P_User_3.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * def Application_Details = active_tpp.AISP_PISP
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details $.request_method = "GET"
    * set Application_Details $.token_type = 'Bearer'
    * call apiApp.Multi_Account_Information(Application_Details)
     * match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario: Verify HTTP status code 400 is received in response when GET Single Account Product API is triggered for the account which was consented but now changed to Credit grade 6

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "CREDIT_GRADING='6'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 400

    * string a = "CREDIT_GRADING='2'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'


   Scenario: Verify HTTP status code 400 is received in response when GET Single Account Product API is triggered for the account which was consented but now changed to Credit grade 7

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
   #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "CREDIT_GRADING='7'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 400

    * string a = "CREDIT_GRADING='2'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'


   Scenario: Verify HTTP status code 400 is received in response when GET Single Account Product API is triggered for the account which was consented but now is INACTIVE

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
   #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "ACCOUNT_STATUS='INACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 400

    * string a = "ACCOUNT_STATUS='ACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
