@Regression
@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus
@P02

Feature: URI and account ID validation for Get Single Account Product API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'Only_Products_permission_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * def Application_Details = active_tpp.AISP_PISP
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details $.request_method = "GET"
    * set Application_Details $.token_type = 'Bearer'
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario Outline: Verify 404 error is received when Product API is called with incorrect URI #key#
    * def info = karate.info
    * set info.subset = 'Incorrect URI'
    * def key = <condition>
    * set info.key = <condition>
    * def AccountId = Application_Details.AccountId
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token
    * set reqHeader $.client_id = Application_Details.client_id
    * set reqHeader $.client_secret = Application_Details.client_secret
    * def AccountInfoUrl = ( ActiveEnv=="SIT" ? <AcctInfoUrl_SIT> : <AcctInfoUrl_UAT>)

    Given url AccountInfoUrl + <AccountId> + <endpoint>
    And headers reqHeader
    And header Authorization = auth
    When method GET
    And set Application_Details $.URL = AccountInfoUrl + <AccountId> + <endpoint>
    Then match responseStatus == <Expected_Error>

    Examples:
      | count | condition                                  | AcctInfoUrl_SIT                                               | AcctInfoUrl_UAT                                                       | AccountId | endpoint      | Expected_Error |
      | 001   | 'accounts misspelled'                      | "https://api.boitest.net/1/api/open-banking/v1.1/accountsss/" | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountsss/" | AccountId | "/product"    | 404            |
      | 002   | 'Account in mixed case'                    | "https://api.boitest.net/1/api/open-banking/v1.1/Accounts/"   | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Accounts/"   | AccountId | "/product"    | 404            |
      | 003   | 'missing open-banking'                     | "https://api.boitest.net/1/api/v1.1/accounts/"                | "https://api.u2.psd2.boitest.net/1/api/v1.1/accounts/"                | AccountId | "/product"    | 404            |
      | 004   | 'double forward slash before accounts'     | "https://api.boitest.net/1/api/open-banking/v1.1//accounts/"  | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//accounts/"  | AccountId | "/product"    | 404            |
      | 005   | 'double forward slash after open-banking'  | "https://api.boitest.net/1/api/open-banking//v1.1/accounts/"  | "https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/accounts/"  | AccountId | "/product"    | 404            |
      | 006   | 'double forward slash before open-banking' | "https://api.boitest.net/1/api//open-banking/v1.1/accounts/"  | "https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/accounts/"  | AccountId | "/product"    | 404            |
      | 007   | 'double forward slash before api'          | "https://api.boitest.net/1//api/open-banking/v1.1/accounts/"  | "https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/accounts/"  | AccountId | "/product"    | 404            |
      | 010   | 'double forward slash after accounts'      | "https://api.boitest.net/1/api/open-banking/v1.1/accounts//"  | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts//"  | AccountId | "/product"    | 404            |
      | 011   | 'triple forward slash after accounts'      | "https://api.boitest.net/1/api/open-banking/v1.1/accounts///" | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts///" | AccountId | "/product"    | 404            |
      | 012   | 'product misspelled'                       | "https://api.boitest.net/1/api/open-banking/v1.1/accounts/"   | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/"   | AccountId | "/prodduct"   | 404            |
      | 013   | 'product in mixed case'                    | "https://api.boitest.net/1/api/open-banking/v1.1/accounts/"   | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/"   | AccountId | "/Product"    | 404            |
      | 015   | 'triple forward slash after product'       | "https://api.boitest.net/1/api/open-banking/v1.1/accounts/"   | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/"   | AccountId | "/product///" | 404            |
      | 016   | 'Empty account ID'                         | "https://api.boitest.net/1/api/open-banking/v1.1/accounts/"   | "https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/"   | ""        | "/product"    | 404            |


  Scenario Outline: Verify HTTP status code 400 when #key# is passed in GetSingleAccountProduct API

    * def info = karate.info
    * set info.subset = 'Invalid Account Id'
    * def key = <condition>
    * def auth = 'Bearer ' + apiApp.Access_Token_RTG.response.access_token

    Given url AcctInfoUrl + <AccountId> + '/product'
    And headers reqHeader
    And header Authorization = auth
    When method GET
    Then set Application_Details $.URL = AcctInfoUrl + <AccountId> + '/product'
    And match responseStatus == 400


    Examples:
      | count | condition                                        | AccountId                                              |
      | 001   | 'Invalid Account ID letters'                     | "abch"                                                 |
      | 002   | 'Invalid Account ID more 40chars'                | "b53aa934-e1c1-42b2-9df0-377aaba501dghgfefhgfhghgfhgf" |
      | 003   | 'Invalid Account ID numeric'                     | "123445377"                                            |
      | 004   | 'Invalid Account ID special symbol_alphanumeric' | "b53aa934-e1c1££-42b2-9d€f0-377"                       |
      | 005   | 'Invalid Account ID special symbols'             | "$#@"                                                  |
      | 006   | 'Invalid Account ID_appendAlphanumeric'          | Application_Details.AccountId + '123abcd'              |
      | 007   | 'Invalid Account ID_appendsymbols'               | Application_Details.AccountId + '$#@123'               |