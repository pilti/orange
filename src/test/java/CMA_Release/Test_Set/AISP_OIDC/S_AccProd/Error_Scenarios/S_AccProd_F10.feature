@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature: Validate error Code in response of Products API for 'expired' OR 'About To expire' consent

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
    * set info.subset = info.scenarioName
    * json Application_Details = active_tpp.AISP_PISP

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario: Verify error code when Product API is hit for an expired consent

    * def fileName = 'ExpiredConsent_Generic_P_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'

    Given json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    When call apiApp.configureSSL(profile)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = Application_Details.access_token
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = profile.AccountId
    When call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403


  Scenario: Verify whether error is not received when Access token which is about to expire is sent in API request
   # To read Test Data
    * def fileName = 'Generic_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'

    Given json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    And call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 200
