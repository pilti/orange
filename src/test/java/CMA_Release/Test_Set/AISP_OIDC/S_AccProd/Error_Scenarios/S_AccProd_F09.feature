@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus

Feature: Permissions validations for Get Single Account Product API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline:Verify whether error is received when API is triggered with invalid #key# permissions

    * def info = karate.info
    * def key = <permissions>
    * set info.subset = 'Permissions validation'
    * def fileName = <permissions>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And call apiApp.Account_Product(Application_Details)
    Then match apiApp.Account_Product.responseStatus == 403


    Examples:
      |permissions|
      | 'Only_Account_info_permissions_P_User_1.properties' |
      | 'Only_Balance_Permission_P_User_1.properties'       |
      | 'Only_Beneficiaries_permission_P_User_1.properties' |
      | 'Only_Direct_Debits_permission_P_User_1.properties' |
      | 'Only_Standing_orders_permission_P_User_1.properties'|
      | 'Only_Transactions_Permission_P_User_1.properties'  |
      | 'Only_Products_permission_P_User_1.properties' |
