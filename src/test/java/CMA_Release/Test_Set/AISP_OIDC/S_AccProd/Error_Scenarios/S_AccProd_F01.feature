@Regression
@AISP
@AISP_API
@AISP_SingleAccProd
@AISP_Error_Bus
@P01
Feature: Method validation for Get Single Account Product API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'Only_Products_permission_P_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'

    Given json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    When call apiApp.configureSSL(profile)

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: Verify HTTP Status code 405 when method #key# is used rather than GET for Product API

    * def info = karate.info
    * def key = '<method>'
    * set info.key = key
    * set info.subset = 'Method validation'

    Given def Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200

    ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = Application_Details.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    Given def auth = Application_Details.token_type + " " + Application_Details.access_token
    And url AcctInfoUrl + Application_Details.AccountId + '/product'
    And headers reqHeader
    And header Authorization = auth
    And request permissions
    When method <method>
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/product'
    Then match responseStatus == 405

    Examples:
      |method|
      |POST|
      |DELETE  |
      |PUT   |
      |OPTIONS |
      |HEAD    |
      |PATCH   |




