@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_SingleAccTxn
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error Conditions for Transaction API.

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when used refresh Token having #key# permission

    * def info = karate.info
    * def key = <condition>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Permission'
    # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
    #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
     #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

          #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then match apiApp.Multi_Account_Information.responseStatus == 200

        #Set input for Transaction API
    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
        #Call Single Account Transaction API
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == <Expected>

    Examples:
      | Role      | condition                    | permissionsfile                               | Expected |
      | AISP_PISP | 'ReadAccountsDetail&Balance' | 'Only_Balance_Permission_G_User_1.properties' | 403      |



##########################################################################################################################################
  @Defect_1335
  @severity=normal
  Scenario Outline: Verify that error is not received when client credentials of Different TPP #key# are passed in API request


    * def info = karate.info
    * def key = '<DifferentTPP>'
    * set info.key = '<Role>'
    * set info.subset = 'Different TPP'
         # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
         #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
          #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

          #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

             #Set input for Transaction API
    Given def Application_Details1 = active_tpp.<DifferentTPP>
    And set Application_Details1 $.token_type = 'Bearer'
    And set Application_Details1 $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details1.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details1.URL = AcctInfoUrl + Application_Details1.AccountId + '/transactions'
    And set Application_Details1 $.request_method = "GET"
             #Call Single Account Transaction API with Different Client Credential
    When call apiApp.Account_Transaction(Application_Details1)
    Then match apiApp.Account_Transaction.responseStatus == <Expected>


    Examples:
      | Role      | DifferentTPP | permissionsfile                       | Expected |
      | AISP      | PISP         | 'Generic_AISPTPP_G_User_1.properties' | 200      |
      | AISP_PISP | AISP         | 'Generic_G_User_1.properties'         | 200      |

#################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed when Invoked the API using Invalid AccountId #key#

    * def info = karate.info
    * def key = <AccountId>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid AccountId'
     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
     #Configure Network Certificate
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

        #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for Transaction API
    Given set Application_Details $.AccountId = <AccountId>
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
         #Call Single Account Transaction API with invalid AccountId
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == <Expected>


    Examples:
      | Role      | AccountId | permissionsfile                                    | Expected |
      | AISP_PISP | 'abcd'    | 'Only_Transactions_Permission_G_User_1.properties' | 400      |


###############################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Invalid Access Token #key#

    Given def Application_Details = active_tpp.<Role>

    * def info = karate.info
    * def key = <accesstoken>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Access Token'
     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
     #Configure Network Certificate
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

            #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for Transaction API
    Given set Application_Details $.access_token = <accesstoken>
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
         #Call Single Account Transaction API with invalid Access token
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == <Expected>

    Examples:
      | Role      | accesstoken | permissionsfile                                    | Expected |
      | AISP_PISP | 'abcd'      | 'Only_Transactions_Permission_G_User_1.properties' | 401      |


#################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Expired Access Token for TPP Role #key#


    * def info = karate.info
    * def key = '<Role>'
    * set info.key = '<Role>'
    * set info.subset = 'Expired Access Token'
     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
     #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

            #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for Transaction API
    Given set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
     #Wait for 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And print msg
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)
         #Call Single Account Transaction API with Expired access token
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == <Expected>


    Examples:
      | Role      | permissionsfile                                    | Expected |
      | AISP_PISP | 'Only_Transactions_Permission_G_User_1.properties' | 401      |


    ############################################################################################################################################

  @Defect_1421
  Scenario: Verify that Status code 403 is displayed when Valid AccountID but the AccountRequest associated with the AccountID has been revoked using the DELETE endpoint

    * def info = karate.info
  # To read Test Data
    * def fileName = 'RevokedConsent_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
  # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
  # Generate access_token from refresh_roken
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.AccountId = profile.AccountId
  #AccountId not associated with originating TPP is being sent in the request
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
 # TPP triggers transaction API for Account ID not associated with originating consent
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 403

