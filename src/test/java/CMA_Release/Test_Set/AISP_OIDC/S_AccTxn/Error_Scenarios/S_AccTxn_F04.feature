@AISP
@AISP_API
@AISP_SingleAccTxn


Feature: Access Token validation for Get Single Account transactions Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   # Generate access_token from refresh_roken
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details.request_method = 'GET'
    * set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    * set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

    # Trigger MultipleAccountInfo API to get AccountID
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'

   #To write test result and error details to output file

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  @Defect_1426
  @severity=normal

  Scenario: Verify whether error is received when Token issued using not allowed grant_type client_credentials is sent in API request

    * def info = karate.info

    Given set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Send Client credential token instead of access_token in API request
    Given set Application_Details $.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details $.request_method = 'GET'
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 401


  @Defect_1426
  @severity=normal
  Scenario: Verify whether error is received when Token issued using not allowed grant_type refresh_token is sent in API request

    * def info = karate.info

    #Send refresh token instead of access_token in API request
    Given set Application_Details $.access_token = profile.refresh_token
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.request_method = 'GET'
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 401

  @severity=normal
  Scenario: Verify whether error is received when access token generated using refresh_token not associated with requesting consent is sent in API request

    * def info = karate.info
    * def apiApp1 = new apiapp()
  # TPP generates access token from refresh token which is not associated with originating consent
    * def fileName = 'All_Basic_permissions_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
 # TPP triggers transaction API with access token not associated with originating consent
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 403
