@AISP
@AISP_API
@AISP_SingleAccTxn

Feature: Method and permissions validation for Get Single Account transaction Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
      #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """

  Scenario Outline:Verify whether error is received when API is triggered with invalid method #key#

    * def info = karate.info
    * def key = '<method>'
    * set info.subset = 'Method validation'

    # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
     # TPP triggers transaction API with invalid method
    And url AcctInfoUrl + Application_Details.AccountId + '/transactions'
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    And request permissions
    When method <method>
    Then match responseStatus == 405

    Examples:
      | method  |
      | POST    |
      | DELETE  |
      | PUT     |
      | OPTIONS |
      | HEAD    |
      | PATCH   |


  Scenario Outline:Verify whether error is received when API is triggered with invalid #key# permissions

    * def info = karate.info
    * def key = <permissions>
    * set info.subset = 'Permissions validation'

    # To read Test Data
    * def fileName = <permissions>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

# Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
# TPP triggers transaction API with invalid permissions
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 403


    Examples:
      | permissions                                           |
      | 'Only_Account_info_permissions_G_User_1.properties'   |
      | 'Only_Balance_Permission_G_User_1.properties'         |
      | 'Only_Standing_orders_permission_G_User_1.properties' |
      | 'Only_Direct_Debits_permission_G_User_1.properties'   |
      | 'Only_Products_permission_G_User_1.properties'        |
      | 'Only_Beneficiaries_permission_G_User_1.properties'   |
      | 'No_Transactions_Permission_G_User_1.properties'      |






