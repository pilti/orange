@AISP
@AISP_API
@AISP_SingleAccTxn

Feature: Accept parameter ,token type and URI validation for Get Single Account transaction Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
      #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """

  Scenario Outline:Verify whether error is not received when empty accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'

    # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
    # set empty value to Accept parameter
    And set reqHeader $.Accept = <Accept>
   # TPP triggers transaction API with invalid Accept parameter value
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 200

    Examples:
      | Accept |
      | ' '    |


  Scenario Outline:Verify whether error is received when invalid accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
  # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
  # set invalid value to Accept parameter
    And set reqHeader $.Accept = <Accept>
 # TPP triggers transaction API with invalid Accept parameter value
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 406

    Examples:
      | Accept             |
      | 'Application-json' |

  Scenario:Verify whether error is not received when accept header is not sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'

 # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
# remove Accept header from API headers
    And remove reqHeader.Accept
# TPP triggers transaction API without Accept header
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 200

  Scenario Outline:Verify whether error is received when #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Token type validation'

# Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    # Input invalid token type values in Authorization header
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
   # Input invalid token type values in Authorization header
    And set Application_Details.token_type = <type>
# TPP triggers transaction API with invalid Accept parameter value
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 401

    Examples:
      | key                                                                                    | type                   |
      | 'Missing token type'                                                                   | " "                    |
      | 'Misspelled token type append single extra char to Bearer'                             | "Bearerr"              |
      | 'Invalid token type- append multiple chars to Bearer'                                  | "Bearerdfgdfhdh"       |
      | 'Invalid token type- append multiple aphanumeric chars to Bearer'                      | "Bearer5757"           |
      | 'Invalid token type- append multiple special chars to Bearer'                          | "Bearer$%&%4gd"        |
      | 'Invalid token type- placed multiple alphanumeric chars in between Bearer'             | "Beare577dgdfr"        |
      | 'Invalid token type- placed multiple alphanumeric and special chars in between Bearer' | "Bear%^*577dfgfddgdfr" |
      | 'Lower case token type'                                                                | "bearer"               |
      | 'Token type other than Bearer '                                                        | "Basic"                |

  @234
  Scenario Outline: Verify whether error is received when invalid URI#key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'

    # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
   # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Result.ActualOutput.MultiAccountInfo.Output.response = apiApp.Multi_Account_Information.response
    And set Result.ActualOutput.MultiAccountInfo.Output.responseStatus = apiApp.Multi_Account_Information.responseStatus
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountInfoUrl = ( ActiveEnv=="SIT" ? '<AcctInfoUrl_SIT>' : '<AcctInfoUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + AccountInfoUrl
    And set Application_Details $.AcctInfoUrl = AccountInfoUrl
    And set Application_Details $.URL = AccountInfoUrl + <AccountId> + <endpoint>
   # TPP triggers transaction API with invalid URI
    And url AccountInfoUrl + <AccountId> + <endpoint>
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    When method GET
    Then match responseStatus == 404

    Examples:
      | key  | AcctInfoUrl_SIT                                             | AcctInfoUrl_UAT                                                     | AccountId                     | endpoint           |
      | '1'  | https://api.boitest.net/1/api/open-banking/v1.1/accountsss/ | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountsss/ | Application_Details.AccountId | "/transactions"    |
      | '2'  | https://api.boitest.net/1/api/open-banking/v1.1/Accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Accounts/   | Application_Details.AccountId | "/transactions"    |
      | '3'  | https://api.boitest.net/1/api/v1.1/accounts/                | https://api.u2.psd2.boitest.net/1/api/v1.1/accounts/                | Application_Details.AccountId | "/transactions"    |
      | '4'  | https://api.boitest.net/1/api/open-banking/v1.1//accounts/  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//accounts/  | Application_Details.AccountId | "/transactions"    |
      | '5'  | https://api.boitest.net/1/api/open-banking//v1.1/accounts/  | https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/accounts/  | Application_Details.AccountId | "/transactions"    |
      | '6'  | https://api.boitest.net/1/api//open-banking/v1.1/accounts/  | https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/accounts/  | Application_Details.AccountId | "/transactions"    |
      | '7'  | https://api.boitest.net/1//api/open-banking/v1.1/accounts/  | https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/accounts/  | Application_Details.AccountId | "/transactions"    |
      | '8'  | https://api.boitest.net/1/api/open-banking/v1.1/accounts//  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts//  | Application_Details.AccountId | "/transactions"    |
      | '9'  | https://api.boitest.net/1/api/open-banking/v1.1/accounts/// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/// | Application_Details.AccountId | "/transactions"    |
      | '10' | https://api.boitest.net/1/open-banking/v1.1/accounts/       | https://api.u2.psd2.boitest.net/1/open-banking/v1.1/accounts/       | Application_Details.AccountId | "/transactions"    |
      | '11' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/   | Application_Details.AccountId | "/transaction"     |
      | '12' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/   | Application_Details.AccountId | "/transactions///" |
      | '13' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/   | Application_Details.AccountId | "//transactions/"  |
      | '14' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/   | ""                            | "/transactions"    |
      | '15' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/   | Application_Details.AccountId | "/transactions//"  |

  Scenario Outline: Verify whether error is received when URI#key# is sent with extra slashes in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'

   # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
  # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountInfoUrl = ( ActiveEnv=="SIT" ? '<AcctInfoUrl_SIT>' : '<AcctInfoUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + AccountInfoUrl
    And set Application_Details $.AcctInfoUrl = AccountInfoUrl
    And set Application_Details $.URL = AccountInfoUrl + <AccountId> + <endpoint>
 # TPP triggers transaction API with invalid URI
    And url AccountInfoUrl + <AccountId> + <endpoint>
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    When method GET
  # TPP triggers transaction API with extra forward slashes in URI
    Then match responseStatus == 200

    Examples:
      | key | AcctInfoUrl_SIT                                            | AcctInfoUrl_UAT                                                    | AccountId                     | endpoint         |
      | '1' | https://api.boitest.net//1/api/open-banking/v1.1/accounts/ | https://api.u2.psd2.boitest.net//1/api/open-banking/v1.1/accounts/ | Application_Details.AccountId | "/transactions"  |
      | '2' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/  | Application_Details.AccountId | "/transactions/" |