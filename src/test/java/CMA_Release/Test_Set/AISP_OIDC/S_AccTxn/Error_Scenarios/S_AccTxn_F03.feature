@AISP
@AISP_API
@AISP_SingleAccTxn


Feature: Access Token validation for Get Single Account Transactions Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   # Generate new access_token from refresh_roken
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details.request_method = 'GET'
    * set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    * set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);
       }
      """

  @Defect_1272
  @severity=normal

  Scenario: Verify whether error is received when Authorization header is not sent in API request

    * def info = karate.info

    Given url Application_Details.URL
    And headers reqHeader
   #Do not pass Authorization header in the request
   #And header Authorization = Type + ' ' + oAuth_token
    When method GET
    Then match responseStatus == 401


  @severity=normal
  Scenario Outline: Verify whether error is received when #key# value of access token is sent in API request

    * def info = karate.info
    * def key =  <key>
    * set info.subset = 'Access Token validation'

    # input blank or invalid values in token
    Given set Application_Details.token_type = <type>
    And set Application_Details.access_token = <token>
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 401

    Examples:
      | key       | token                             | type     |
      | 'Empty'   | ' '                               | ' '      |
      | 'Invalid' | 'VQsz3pnZ41j0cGD4QryeWIiwGNRa$%$' | 'Bearer' |


  @severity=normal
  Scenario: Verify whether error is not received when Access token which is about to expire is sent in API request

    * def info = karate.info

    # Wait for 299 secs when the token is about to expire and send it in API request

    * print "Wait starts"
    Given def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(290000)
    * print "wait ends"
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 200


