@AISP
@AISP_API
@AISP_SingleAccTxn


Feature:Verify HTTP status code 403 in response when access token having payments scope is passed in GET Single Account Transaction API

  Background:
    * def apiApp = new apiapp()
    * def info = karate.info
    * json Result = {}
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @severity=normal
  Scenario: Verify HTTP status code 403 received in response when access token having payments scope is passed in API request

    # Get AISP acess Token using refresh token

    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     ##########Configure Network Certificate####################################################################################
    Given def Application_Details1 = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details1)
    And set Application_Details1 $.grant_type = "refresh_token"
    And set Application_Details1 $.request_method = 'POST'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details1 $.refresh_token = profile.refresh_token
    And set Application_Details1 $.token_type = 'Bearer'
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details1)
    And set Application_Details1 $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    ##Get AccounID from Multi Account Info API
    Given set Application_Details1 $.request_method = "GET"
    When call apiApp.Multi_Account_Information(Application_Details1)
    And def account = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then match apiApp.Multi_Account_Information.responseStatus == 200

  ############################## Start PISP Flow ##################################################################

    Given def paymentId = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/payment_Setup.feature')
    And def consent_Url = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/createConsentUrl.feature') paymentId.Result
   # Test data to perform consent
    * def strConsentUrl = consent_Url.Result.createConsentUrl.ConsentUrl
    * def consentData = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes","url":'#(strConsentUrl)'}
    * def performConsent = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/performConsent.feature') consentData
    And set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.code = performConsent.performConsentAction.AuthCode
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200

    #######################################PISP FLOW END######################################################################
    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.AccountId = account
    And set Application_Details $.request_method = 'GET'
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/transactions'
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 403

