@AISP_SingleAccTxn
@ignore
Feature:  To verify Single Account Transactions API is able to retrieve multiple transactions at a time


  Background:

    * def UpdateABTTransactions = Java.type('CMA_Release.Entities_DB.UpdateABTTransactions')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'S_AccTxn_F05_TestData.properties'
    * def name = 'TA'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
  ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal

  @Txn71
  Scenario: To verify Single Account Transactions API is able to retrieve 20 transactions at a time

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added "
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,20,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    And match apiApp.Account_Transaction.responseStatus == 200
    * print 'count is'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    * print AccountNo
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print karate.pretty(cachedTransactions)
    * print karate.pretty(postedTransactions)
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
    * print 'Number of transactions from API:'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    #* def status = apiApp.Account_Transaction.response.Data.Transaction[*].Status
    #* def code = apiApp.Account_Transaction.response.Data.Transaction[*].ProprietaryBankTransactionCode.Code
    #* def abc = { Status: "abc", Code:"abc"}
    #* set abc $.Status = status
    #* set abc $.Code = code
    #* print karate.pretty(abc)
    Then assert apiApp.Account_Transaction.response.Data.Transaction.length == postedTransactions.length
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { CreditDebitIndicator: '#regex^(Debit|Credit)$' }
    And assert apiApp.Account_Transaction.responseStatus == 200
    And assert apiApp.Account_Transaction.response.Data.Transaction.length == 20
    And match apiApp.Account_Transaction.response.Links !contains { Next: '#string'}

#################################################################################################################################################################
###########################################Scenario 2######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify Single Account Transactions API is able to retrieve 25 transactions at a time

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,25,date,'201', ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    And match apiApp.Account_Transaction.responseStatus == 200
    * print 'count is'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    * print AccountNo
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print karate.pretty(cachedTransactions)
    * print karate.pretty(postedTransactions)
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
    * print 'Number of transactions from API:'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    #* def status = apiApp.Account_Transaction.response.Data.Transaction[*].Status
    #* def code = apiApp.Account_Transaction.response.Data.Transaction[*].ProprietaryBankTransactionCode.Code
    #* def abc = { Status: "abc", Code:"abc"}
    #* set abc $.Status = status
    #* set abc $.Code = code
    #* print karate.pretty(abc)
    Then assert apiApp.Account_Transaction.response.Data.Transaction.length == postedTransactions.length
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { CreditDebitIndicator: '#regex^(Debit|Credit)$' }
    And assert apiApp.Account_Transaction.responseStatus == 200
    And assert apiApp.Account_Transaction.response.Data.Transaction.length == 25
    And match apiApp.Account_Transaction.response.Links !contains { Next: '#string'}

#################################################################################################################################################################
###########################################Scenario 3######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify Single Account Transactions API is able to retrieve 2 transactions at a time

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,2,date,'101',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    And match apiApp.Account_Transaction.responseStatus == 200
    * print 'count is'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    * print AccountNo
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print karate.pretty(cachedTransactions)
    * print karate.pretty(postedTransactions)
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
    * print 'Number of transactions from API:'
    * print apiApp.Account_Transaction.response.Data.Transaction.length
    #* def status = apiApp.Account_Transaction.response.Data.Transaction[*].Status
    #* def code = apiApp.Account_Transaction.response.Data.Transaction[*].ProprietaryBankTransactionCode.Code
    #* def abc = { Status: "abc", Code:"abc"}
    #* set abc $.Status = status
    #* set abc $.Code = code
    #* print karate.pretty(abc)
    Then assert apiApp.Account_Transaction.response.Data.Transaction.length == postedTransactions.length
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { CreditDebitIndicator: '#regex^(Debit|Credit)$' }
    And assert apiApp.Account_Transaction.responseStatus == 200
    And assert apiApp.Account_Transaction.response.Data.Transaction.length == 2
    And match apiApp.Account_Transaction.response.Links !contains { Next: '#string'}



    #################################################################################################################################################################
###########################################Scenario 4######################################################################################################################
  @AISP_SingleAccTxn123
  @severity=normal
  Scenario: To verify Single Account Transactions API is able to retrieve 26 transactions at a time

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,26,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200

    Given def page1Result = apiApp.Account_Transaction.response
    * def page2 = page1Result.Links.Next
    # call page 2 transactions
    And url BulkApiUrl + page2
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    And def page2Result = response
    * print 'Transactions on Page2:'
    * print page2Result
    * set Result.Account_Transaction.Page2Response = page2Result
   # * def adder = function(a, b){ return a + b }
   # * def tcount = {"Value": '#(adder('#(parseInt(transactionCount))', '#(parseInt(page2Result.Transaction.length)')))}
    #* set transactionCount $.b = '#(page2Result.Transaction.length)'
    #* def foo = Test_res.response.Data.Transaction.length
    * def transactionCount = { a: '#(parseInt(page1Result.Data.Transaction.length))', b:'#(parseInt(page2Result.Data.Transaction.length))' }
    * print 'total transactions:'
    * print transactionCount
    * def adder = function(a, b){ return a + b }
    * def tcount = adder(transactionCount.a, transactionCount.b)
    * print 'Total transactions count is'
    * print tcount
    * print AccountNo
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
   # * print 'Number of transactions from API:'
    #* print Test_res.response.Data.Transaction.length
   # * def status = Test_res.response.Data.Transaction[*].Status
   # * def code = Test_res.response.Data.Transaction[*].ProprietaryBankTransactionCode.Code
    #* def abc = { Status: "abc", Code:"abc"}
    #* set abc $.Status = status
   # * set abc $.Code = code
   # * print karate.pretty(abc)
   # When assert Test_res.response.Data.Transaction.length == postedTransactions.length
   # Then print "Assertion successful"

    Then assert tcount == 26.0
    And match page1Result.Links contains { Next: '#string'}
    And match page2Result.Links !contains {Next: '#string'}




    #################################################################################################################################################################
###########################################Scenario 5######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify Single Account Transactions API is able to retrieve 50 transactions at a time

   # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,50,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200

    Given def page1Result = apiApp.Account_Transaction.response
    * def page2 = page1Result.Links.Next
    # call page 2 transactions
    And url BulkApiUrl + page2
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    And def page2Result = response
    * print 'Transactions on Page2:'
    * print page2Result
    * set Result.Account_Transaction.Page2Response = page2Result
   # * def adder = function(a, b){ return a + b }
   # * def tcount = {"Value": '#(adder('#(parseInt(transactionCount))', '#(parseInt(page2Result.Transaction.length)')))}
    #* set transactionCount $.b = '#(page2Result.Transaction.length)'
    #* def foo = Test_res.response.Data.Transaction.length
    * def transactionCount = { a: '#(parseInt(page1Result.Data.Transaction.length))', b: '#(parseInt(page2Result.Data.Transaction.length))' }
    * print 'total transactions:'
    * print transactionCount
    * def adder = function(a, b){ return a + b }
    * def tcount = adder(transactionCount.a, transactionCount.b)
    * print 'Total transactions count is'
    * print tcount
    * print AccountNo
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
   # * print 'Number of transactions from API:'
    #* print Test_res.response.Data.Transaction.length
   # * def status = Test_res.response.Data.Transaction[*].Status
   # * def code = Test_res.response.Data.Transaction[*].ProprietaryBankTransactionCode.Code
    #* def abc = { Status: "abc", Code:"abc"}
    #* set abc $.Status = status
   # * set abc $.Code = code
   # * print karate.pretty(abc)
   # When assert Test_res.response.Data.Transaction.length == postedTransactions.length
   # Then print "Assertion successful"

    Then assert tcount == 50.0
    And match page1Result.Links contains { Next: '#string'}
    And match page2Result.Links !contains {Next: '#string'}

 #################################################################################################################################################################
###########################################Scenario 6######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify Single Account Transactions API is able to retrieve 51 transactions at a time
# Get the Auth token and Refeshtoken using Authcode

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,51,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    And def page1Result = apiApp.Account_Transaction.response
    Then match apiApp.Account_Transaction.responseStatus == 200

    Given def page2 = page1Result.Links.Next
    # call page 2 transactions
    And url BulkApiUrl + page2
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def page2Result = response
    * print 'Transactions on Page2:'
    * print page2Result
    * set Result.Account_Transaction.Page2Response = page2Result
    * def page3 = page2Result.Links.Next

# call page 3 transactions
    Given url BulkApiUrl + page3
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    And def page3Result = response
    * print 'Transactions on Page3:'
    * print page3Result
    * set Result.Account_Transaction.Page3Response = page3Result
    And def transactionCount = { a: '#(parseInt(page1Result.Data.Transaction.length))', b: '#(parseInt(page2Result.Data.Transaction.length))',c: '#(parseInt(page3Result.Data.Transaction.length))' }
    * print 'total transactions:'
    * print transactionCount
    * def adder = function(a, b, c){ return a + b + c}
    * def tcount = adder(transactionCount.a, transactionCount.b, transactionCount.c)
    * print 'Total transactions count is'
    * print tcount
    * print AccountNo
  #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
  # to get transactions from DB
    * def get_data_ABT = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def cTransactions = get_data_ABT.get_Cached_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json cachedTransactions = cTransactions
    * def pTransactions = get_data_ABT.get_Posted_Transaction(NSC,AcNo,ABT_DB_Conn)
    * json postedTransactions = pTransactions
    * print 'Number of Cached transactions from DB:'
    * print cachedTransactions.length
    * print 'Number of Posted transactions from DB:'
    * print postedTransactions.length
    Then match tcount == 51.0
    And match page1Result.Links contains { Next: '#string'}
    And match page2Result.Links contains {Next: '#string'}
    And match page3Result.Links !contains {Next: '#string'}
    * print "Assertion successful for 51 transactions"



#################################################################################################################################################################
###########################################Scenario 7######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify the response of Prev link on second Page of Single account Transactions API is same as the response of First Page

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,26,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200

    Given def page1Result = apiApp.Account_Transaction.response
    * def page2 = page1Result.Links.Next
    # call page 2 transactions
    And url BulkApiUrl + page2
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def page2Result = response
    * print 'Transactions on Page2:'
    * print page2Result
    And set Result.Account_Transaction.Page2Response = page2Result
    And match page1Result.Links contains { Next: '#string'}
    And match page2Result.Links !contains {Next: '#string'}
    * def PrevLink = page2Result.Links.Prev
    * print PrevLink

 # call page 1 using Prev link provided on Second page
    Given url BulkApiUrl + PrevLink
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    And def PrevLinkResult = response
    * print 'Transactions on Page1 from Prev link:'
    * print PrevLinkResult
    Then match PrevLinkResult.Data == page1Result.Data
    And set Result.Account_Transaction.Page1ResponseFromPrevLink = PrevLinkResult
    * print "The prev link on second page points to First page of Transactions API response"

#################################################################################################################################################################
###########################################Scenario 8######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify Last and First links are not populated in the response of Single Account Transactions API

  # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * print AccountNo
    * print "transactions are added"
    #To add transactions into database
    * def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    * print "Current date is :"
    * print date
    * def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,51,date,'201',ABT_DB_Conn)
    #to get transactions for the accountID
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    And def page1Result = apiApp.Account_Transaction.response
    Then match apiApp.Account_Transaction.responseStatus == 200

    Given def page2 = page1Result.Links.Next
    # call page 2 transactions
    And url BulkApiUrl + page2
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def page2Result = response
    * print 'Transactions on Page2:'
    * print page2Result
    * set Result.Account_Transaction.Page2Response = page2Result

    Given def page3 = page2Result.Links.Next
# call page 3 transactions
    And url BulkApiUrl + page3
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    And def page3Result = response
    * print 'Transactions on Page3:'
    * print page3Result
    * set Result.Account_Transaction.Page3Response = page3Result
    Then match page3Result.Links !contains {First: '#string'}
    And match page3Result.Links !contains {Last: '#string'}
    And match page1Result.Links !contains { Last: '#string'}
    And match page1Result.Links !contains {First: '#string'}
    And match page2Result.Links !contains { Last: '#string'}
    And match page2Result.Links !contains {First: '#string'}

