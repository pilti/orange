@ignore
@AISP_SingleAccTxn
Feature:  Get Account transaction with ReadTransactionsCredits as well as ReadTransactionsDebits and ReadTransactionsBasic permission


  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'S_AccTxn_F06_TestData.properties'
    * def name = 'TA'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
  ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: Get Account transactions for account having consent for credit as well as debit transactions with ReadTransactionsBasic permission

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200
    And match each apiApp.Account_Transaction.response.Data.Transaction[*].Currency.length() == 3
    And match apiApp.Account_Transaction.response.Data.Transaction[0].AccountId == profile.AccountId
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] !contains { TransactionInformation: '#string' }
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] !contains { "Balance": '#object' }
    And match each apiApp.Account_Transaction.response.Data.Transaction[*].Amount == { Amount: '#regex^\\d{1,13}\\.\\d{1,5}$', Currency: 'GBP'  }
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { CreditDebitIndicator: '#regex^(Debit|Credit)$' }
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { Status: '#string'}
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { BookingDateTime: '#string'}
    And match each apiApp.Account_Transaction.response.Data.Transaction[*].ProprietaryBankTransactionCode == { Code: '#string'}

#############################################################################################################################################################
###################################################Scenario 2##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that both debit and credit transactions are returned when ReadTransactionsDebits and ReadTransactionsCredits permission is given in consent

    # Get the Auth token and Refeshtoken using Authcode
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #And set Result.Access_Token_RTG.Input = inputJSON
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    When call apiApp.Account_Transaction(inputJSON)
    Then match each apiApp.Account_Transaction.response.Data.Transaction[*] contains { CreditDebitIndicator: '#regex^(Debit|Credit)$' }
    And match apiApp.Account_Transaction.responseStatus == 200