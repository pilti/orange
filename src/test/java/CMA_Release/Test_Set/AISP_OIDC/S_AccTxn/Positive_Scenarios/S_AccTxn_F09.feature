@AISP_SingleAccTxn
Feature:  Verify response headers of Get Transactions Information API (/accounts/{AccountId}/transactions API)

  Background:
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'S_AccTxn_F05_TestData.properties'
    * def name = 'TA'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
  ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#############################################################################################################################################################
###################################################Scenario 1##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that content type header in response of Single account Transaction API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
   #Get Single Account Transactions
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And remove inputJSON $.Content_type
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200
    And match apiApp.Account_Transaction.responseHeaders['Content-Type'][0] == 'application/json;charset=UTF-8'


#############################################################################################################################################################
###################################################Scenario 2##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that value of x-fapi-interaction-id header in response is same as sent in request header of Single account Transactions API


    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    #Get Single Account Transactions
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/transactions'
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")
    * def interactionId = Random_id.random_id()
    And set reqHeader $.x-fapi-interaction-id = interactionId
    And remove inputJSON $.Content_type
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseHeaders['x-fapi-interaction-id'][0] == interactionId

#############################################################################################################################################################
###################################################Scenario 3##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal

  Scenario: To verify that x-fapi-interaction-id response header with unique uuid is generated when x-fapi-interaction-id header is not sent in GET Single account Transactions API request


    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
     #Get Single Account Transactions
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + 'transactions'
    And remove reqHeader $.x-fapi-interaction-id
    And remove inputJSON $.Content_type
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseHeaders['x-fapi-interaction-id'][0] == '#uuid'

#############################################################################################################################################################
###################################################Scenario 4##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that API platform returns Http Status code 404 Not Found when Bulk Transactions API is triggered


    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    #Get bulk Account Transactions
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = BulkApiUrl + 'transactions'
    When call apiApp.Bulk_Transaction(inputJSON)
    Then match apiApp.Bulk_Transaction.responseStatus == 404
