@ignore
@AISP_SingleAccTxn
Feature:  To verify Single Account Transactions API response headers

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'S_AccTxn_F05_TestData.properties'
    * def name = 'TA'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
  ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: Get Transaction Information for an account by calling Single Account Transaction API with mandatory and optional headers

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##To call single account transacitons
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")
    And set reqHeader $.x-fapi-interaction-id = Random_id.random_id()
    And set reqHeader $.Accept = 'application/json'
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And set inputJSON $.AccountId = profile.AccountId
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200



#############################################################################################################################################################
###################################################Scenario 3##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: Get Transaction Information for an account by calling Single Account Transaction API only with mandatory headers

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Call Single account Transaction API with the AccountID
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200


#############################################################################################################################################################
###################################################Scenario 6##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that the response of Get Account Transactions API contains the mandatory field Links in appropriate format

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Call Single account Transaction API with the AccountID
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200
    And match apiApp.Account_Transaction.response.Links ==  {"Self": '#string', "Next": '##string', "Prev": '##string' }

###########################################################################################################################################################################
###################################################Scenario 7##############################################################################################################
  @AISP_SingleAccTxn
  @severity=normal
  Scenario: To verify that the response of Get Account Transactions API contains a mandatory field Meta in the payload


    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    ##Call Single account Transactions API with the AccountID
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.AccountId = profile.AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Transaction(inputJSON)
    Then match apiApp.Account_Transaction.responseStatus == 200
    And match apiApp.Account_Transaction.response.Meta == { }

