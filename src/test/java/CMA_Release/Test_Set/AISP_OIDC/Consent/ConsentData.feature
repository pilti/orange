Feature: This feature is to cover the positive flow till launching consent URL

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ReusFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj_Acc_sel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection')
    * json Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    * configure afterScenario =
    """
    function(){
    var tem = karate.info;
    if (typeof key != 'undefined'){info.key = key}
      if(tem.errorMessage == null){
        Result.TestStatus = "Pass";
      }else{
        Result.TestStatus = "Fail";
        Result.Error = tem.errorMessage;
      }
    apiApp.write(Result,info);
    webApp.stop();
    }
   """

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Code & idtoken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    * def usr = Application_Details.usr
    * def otp = Application_Details.otp
    * def User = usr
    * def OTP = otp
    * def User = (usr == null ? user_details.Generic.G_User_1.user : usr)
    * def OTP = (otp == null ? user_details.Generic.G_User_1.otp : otp)
    * def data = {"usr":'#(User)',"otp":'#(OTP)',"action":"continue","Confirmation":"Yes"}

    @123400
    Scenario: Test
    Given def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Prereq_LoginConsentURL.feature') Application_Details
