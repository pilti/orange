@AISP_Consent
Feature: Test feature contains test for AISP Consent Account Selection Screen

  Background:
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def k = Java.type('CMA_Release.Java_Lib.createFolder')
    * def OBFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.OBDirectory')
    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def Result = {}
    * configure afterScenario =
      """
      function(){
       var tem = karate.info
       if (typeof webDef.key != 'undefined'){info.key = webDef.key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
              app.stop();
       }
       Result = karate.pretty(Result)
       fileWriter.write(path,Result);
       }
      """

  @AISP_ConsentX1
  Scenario: Verify that all the current accounts under Select accounts are unchecked when page is displayed for the first time

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)
    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()


  @AISP_Consent
  Scenario: Verify that Select all button is displayed when account selection page is loaded for the first time

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    # Check for Account list retried to check Select all button is enabled  - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_ConsentY
  Scenario: Verify that Selected accounts section is blank when account selection page is loaded for the first time

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result
    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_Consent
  Scenario: Verify that all accounts gets selected when either clicked on select All button or checkbox of Nickname

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

    #Then def perform = app.stop()

  @AISP_Consent
  Scenario: Verify that selected accounts are displayed in Selected accounts section section when clicked on nickname or select all button

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result
    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_Consent
  Scenario: Verify that text of Select all button is changed to Select none when clicked on Select all

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_Consent
  Scenario: Verify that selected accounts gets removed from selected accounts section and gets unchecked from select accounts section when clicked on removed icon for respective account

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Cancel request pop is displayed when user clicks on Cancel button on Account selection page

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()

  @AISP_Consent
  Scenario: Verify that user will be displayed with account selection page when user either  clicks on Yes cancel or X

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.Accountlist = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
   # Check for Account list retried to check Select accounts check box are unchecked/checked - Refer screen reference
    And match Result.ActualOutput.Consent.Accountlist != []
    And set Result.Dump = callfeature
    Then def perform = app.stop()


  @AISP_Consent123
  Scenario: Verify that consent process will be cancelled and rejected status will be returned when user clicks on Cancel button

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that continue button is enabled only when atleast one account is selected from select account section

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that pagination is displayed for select accounts and selected accounts when more than 10 accounts are selected for selected for user

 # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
 #Then def c = call  app.driver1 = app.start1(default_browser)

 # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
 # Mark this test as UI test
    * set webDef.Screens = 'Yes'
 # Set Foldername for output
    * set webDef.subset = 'Login check'
 # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
 # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

 # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Account is greyed out on Account selection screen and tool tip is displayed when Account permission is JX or JZ

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name
  
  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Account is selectable on Account selection screen and user can complete the consent process when Account permission is JA or JV

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that only NI/GB current accounts are displayed in the Account Selection screen even if the customer has valid accounts in other account types or valid accounts in ROI jurisdiction

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that appropriate message is shown to customer if the customer ID is valid but does not have any valid accounts in the 365 online profile

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that only valid NI/GB current accounts are displayed and any accounts in credit gde 6 or credit grade 7 or closed accounts are not displayed

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Account is selectable on Account selection screen and user can complete the consent process when Account permission is A or V

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()