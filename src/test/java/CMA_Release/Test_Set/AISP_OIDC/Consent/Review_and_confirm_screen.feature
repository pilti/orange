@AISP_Consent
Feature: Test feature contains test for AISP Consent Review and confirm screen

  Background:
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def k = Java.type('CMA_Release.Java_Lib.createFolder')
    * def OBFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.OBDirectory')
    * def info = read("classpath:CMA_Release/JavaScript_Lib/test_info.js")
    * def Result = {}
    * configure afterScenario =
      """
      function(){
       var tem = karate.info
       if (typeof webDef.key != 'undefined'){info.key = webDef.key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
              app.stop();
       }
       Result = karate.pretty(Result)
       fileWriter.write(path,Result);
       }
      """


  @AISP_Consent
  Scenario: Verify that Selected accounts section is displayed with max 3 accounts and show all button

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that bydefault only permission label is displayed

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify the label and description of all the permissions mentioned under Requested permissions by clicking on arrow down icon

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify multiple permission details can be expanded and details can be seen

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that From date and To date mentioned under Requested Transaction access are in appropriate format

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify the From date and To date when From Date is not passed from TPP

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result
    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify the From date and To date when To Date is not passed from TPP

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify the From date and To date when both is not passed from TPP

    # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
    #Then def c = call  app.driver1 = app.start1(default_browser)

    # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
    # Mark this test as UI test
    * set webDef.Screens = 'Yes'
    # Set Foldername for output
    * set webDef.subset = 'Login check'
    # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
    # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

    # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that consent validity date is displayed under Requested Transaction access

   # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
   #Then def c = call  app.driver1 = app.start1(default_browser)

   # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
   # Mark this test as UI test
    * set webDef.Screens = 'Yes'
   # Set Foldername for output
    * set webDef.subset = 'Login check'
   # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
   # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

   # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that confirm checkbox is displayed with apporpriate text

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that confirm button is enabled when user clicks on confirm checkbox

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)

  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Back Cancel and Confirm button are displayed below confirm checkbox

  # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
  #Then def c = call  app.driver1 = app.start1(default_browser)


  # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
  # Mark this test as UI test
    * set webDef.Screens = 'Yes'
  # Set Foldername for output
    * set webDef.subset = 'Login check'
  # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
  # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

  # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature


    #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that user is redirected to Account selection screen when clicked on Back button from Review page

 # Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
 #Then def c = call  app.driver1 = app.start1(default_browser)

 # Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
 # Mark this test as UI test
    * set webDef.Screens = 'Yes'
 # Set Foldername for output
    * set webDef.subset = 'Login check'
 # Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
 # Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

 # Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that status rejected will be sent to TPP when user selects cancel from review page

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Cancel request pop is displayed when user clicks on Cancel button on Review page

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that user will be displayed with Review page when user either  clicks on Yes cancel or X

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that consent process is completed when user clicks on confirm button on review page and auth code is displayed

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify that Important information text is displayed below the 3 buttons

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: Verify the footer

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()

  @AISP_Consent123
  Scenario: This is a scenario where the consent expiry date is not provided in account-request consent validity date is displayed under Requested Transaction access in below format

# Assign a new web application for test and Start browser for the test
    Given def app = new webapp()
#Then def c = call  app.driver1 = app.start1(default_browser)

# Get the current feature file and scenario details from karate.info
    * def webDef = karate.info
# Mark this test as UI test
    * set webDef.Screens = 'Yes'
# Set Foldername for output
    * set webDef.subset = 'Login check'
# Drive output path for Capture Screens
    * def info = call info webDef
    * def path = info.path + '/' + info.t_name + '.json'
# Set Result path to app and set test name to Result json
    * def setpath = call  app.path1 = info.path
    * def setname = call  app.t_name1 = info.t_name
    * set Result.TestName =  info.t_name

# Input TPP details from active TPPs
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info.path
    When def callfeature = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.Consent = callfeature.Result

    And set Result.ActualOutput.Consent.Input.Organization = Application_Details.Organization
    Then set Result.ActualOutput.Consent.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Consent.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Consent.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Consent.Input.AccountRequestId = Application_Details.AccountRequestId
    And set Result.ActualOutput.Consent.UIOutput = callfeature.Result.UIOutput.SelectAllAccounts_SelectedAccounts
    And match Result.ActualOutput.Consent.UIOutput.SelectAllAccounts_SelectedAccounts != null
    And set Result.Dump = callfeature

       #Then def perform = app.stop()