@AISP
@AISP_API
@AISP_AccessTokenUsingRefToken
@Regression
Feature: Refresh Token Positive scenario

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  @REf_001

  Scenario: RefreshToken_Verify that refresh token can be generated using the code in the redirect url

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC001_RefreshToken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    Then set Application_Details.refresh_token =  callE2E.Result.Access_Token_ACG.response.refresh_token

  @Ref_002
  Scenario: RefreshToken_Verify that Status code 200 is displayed in the response when valid info is provided in the API Request

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC002_RefreshToken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And set Application_Details.refresh_token =  callE2E.Result.Access_Token_ACG.response.refresh_token
    And set Application_Details.request_method = 'POST'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.grant_type = 'refresh_token'
    And call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200





