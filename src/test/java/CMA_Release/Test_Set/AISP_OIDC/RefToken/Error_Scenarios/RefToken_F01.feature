@AISP
@AISP_API
@AISP_AccessTokenUsingRefToken
Feature: Client Credentials validation for Access_token using refresh token API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-Invalid or empty client_id as #key#

    * def info = karate.info
    * def key = <client_id>
    * set info.subset = 'Client Id validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Input invalid client_id in API request
    And set Application_Details $.client_id = <client_id>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401
  
    Examples:
      |client_id|
      |'abcdef'|
      |'   '   |

  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-client_id associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client_id validation - TPP'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Input client_id associated with different TPP in API request
    And set Application_Details $.client_id = active_tpp.<TPP>.client_id
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401

   Examples:
      |TPP|
      |AISP|
      |PISP|

  @AISP_Error_Bus
  @severity=normal

  Scenario Outline: Validate Client secret matches with client network certificate-Invalid or empty client_secret as #key#

    * def info = karate.info
    * def key = <client_secret>
    * set info.subset = 'Client_secret validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Input invalid client_secret in API request
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401

    Examples:
      |client_secret|
      |'abcdef'|
      |'   '   |

  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate Client secret matches with client network certificate-client_secret associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client_secret validation - TPP'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Input client_secret associated with different TPP in API request
    And set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401

    Examples:
      |TPP|
      |AISP|
      |PISP|

  @AISP_Error_Bus
  @severity=normal
  @Regression
  Scenario Outline: Validate client credentials match with client network certificate - Invalid or empty client_credentials as #key#

    * def info = karate.info
    * def key = <client_id> + <client_secret>
    * set info.subset = 'Client credentials validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Input invalid client credentials in API request
    And set Application_Details $.client_id = <client_id>
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401

    Examples:
      |client_id|client_secret|
      |'xyz'|'abcdef'|
      |'  ' |'   '   |

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Validate Client credentials match with client network certificate-client credentials associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client credentials validation - TPP'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Input client credentials associated with different TPP in API request
    And set Application_Details $.client_id = active_tpp.<TPP>.client_id
    And set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401

    Examples:
      |TPP|
      |AISP|
      |PISP|


  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify error is received when Authorization header is not passed in the request

    * def info = karate.info
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + Application_Details.refresh_token
    And set Application_Details $.request_body = body
    #Input client credentials associated with different TPP in API request
    And url TokenUrl
    #Do not pass client Authentication in API request
    #And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 400