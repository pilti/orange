@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken

Feature: Method, Scope and URI validation for Access_token using refresh_token grant API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_Error_Tech
  @severity=normal

  Scenario Outline: Verify whether error is received when API is triggered with #key# method

    * def info = karate.info
    * def key = <Method>
    * set info.subset = 'Method validation'
   #Input not allowed method in API request
    Given set Application_Details $.request_method = <Method>
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 405

    Examples:
      |Method|
      |'GET'|
      |'DELETE'   |
      |'COPY'     |
      |'LINK'     |
      |'UNLINK'|
      |'PUT'   |
      |'PURGE' |
      |'PROPFIND'|
      |'OPTIONS' |
      |'LOCK'    |
      |'UNLOCK'  |
      |'HEAD'    |
      |'PATCH'   |
      |'VIEW'    |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP and PISP role triggers API with invalid scope #key# provided in request body

      * def info = karate.info
      * def key = <scope>
      * set info.subset = 'Scope validation'
      Given set Application_Details $.grant_type = "refresh_token"
     #Input invalid/not allowed scope in API request
      And set Application_Details $.scope = <scope>
      And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + profile.refresh_token + "&scope=" + Application_Details.scope
      And set Application_Details $.request_method = 'POST'
      And set Application_Details $.request_body = body
      And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
      And set Application_Details $.Authorization = auth
      And url TokenUrl
      And header Authorization = auth
      And header Content-Type = 'application/x-www-form-urlencoded'
      And request body
      When method POST
      Then match responseStatus == <expectedStatuscode>

    Examples:
      | scope         | expectedStatuscode |
      | 'payments'    | 400                |
      | 'accounts123' | 400                |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is not received when TPP having AISP and PISP role triggers API with invalid scope #key# provided as a query parameter

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'

    Given set Application_Details $.grant_type = "refresh_token"
    #Input invalid/not allowed scope in API request
    And set Application_Details $.scope = <scope>
    And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.request_body = body
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.Authorization = auth
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header scope = <scope>
    And request body
    When method POST
    Then match responseStatus == 200

    Examples:
      |scope|
      |'payments'|
      |'accounts123'   |

  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP role triggers API with invalid scope #key# provided in body request

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'
    # To read Test Data
    * def fileName = 'Generic_AISPTPP.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
    #Given def Application_Details = active_tpp.AISP
    Given set Application_Details $.grant_type = "refresh_token"
   #Input invalid/not allowed scope in API request
    And set Application_Details $.scope = <scope>
    And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + profile.refresh_token + "&scope=" + Application_Details.scope
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.request_body = body
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.Authorization = auth
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 400

    Examples:
      | scope         |
      | 'payments'    |
      | 'accounts123' |


  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when API is triggered with invalid URI#key#

    * def info = karate.info
    * def key = <count>
    * set info.subset = 'URI validation'

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + profile.refresh_token
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    And print "Env is:" + ActiveEnv
    And print "URL is:" + tokenurl
  #Input invalid URI in API request
    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 404

    Examples:
      | count | TokenUrl_SIT                                     | TokenUrl_UAT                                             |
      | '1'   | https://api.boitest.net/oauth/as/token           | https://api.u2.psd2.boitest.net/oauth/as/token           |
      | '2'   | https://api.boitest.net/oauth/as/oauth2          | https://api.u2.psd2.boitest.net/oauth/as/oauth2          |
      | '3'   | https://api.boitest.net/oauth/as/token.Oauth2    | https://api.u2.psd2.boitest.net/oauth/as/token.Oauth2    |
      | '4'   | https://api.boitest.net/oauth/as/Token.oauth2    | https://api.u2.psd2.boitest.net/oauth/as/Token.oauth2    |
      | '5'   | https://api.boitest.net/oauth/as//token.oauth2   | https://api.u2.psd2.boitest.net/oauth/as//token.oauth2   |
      | '6'   | https://api.boitest.net/oauth/token.oauth2       | https://api.u2.psd2.boitest.net/oauth/token.oauth2       |
      | '7'   | https://api.boitest.net/oauth//as/token.oauth2   | https://api.u2.psd2.boitest.net/oauth//as/token.oauth2   |
      | '8'   | https://api.boitest.net/as/token.oauth2          | https://api.u2.psd2.boitest.net/as/token.oauth2          |
      | '9'   | https://api.boitest.net/oauth/as/token.oauth2/// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/// |


  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether access token is generated successfully when URI#key# having extra forward slashes

    * def info = karate.info
    * def key = <count>
    * set info.subset = 'URI validation'

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + profile.refresh_token
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    And print "Env is:" + ActiveEnv
    And print "URL is:" + tokenurl
   #Input extra slashes after domain name or at the end of endpoint
    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 200
    And match response contains {"access_token": '#string'}

    Examples:
      | count | TokenUrl_SIT                                    | TokenUrl_UAT                                            |
      | '1'   | https://api.boitest.net//oauth/as/token.oauth2  | https://api.u2.psd2.boitest.net//oauth/as/token.oauth2  |
      | '2'   | https://api.boitest.net/oauth/as/token.oauth2// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2// |
      | '3'   | https://api.boitest.net/oauth/as/token.oauth2/  | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/  |