@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken

Feature: grant_type validation for Access_token using refresh_token grant API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal

  Scenario Outline: Verify whether error is received when invalid grant_type #key# is sent in API request

    * def info = karate.info
    * def key = <grant_type>
    * set info.subset = 'grant_type validation'
    Given set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    #Input invalid grant_type in API request
    And set Application_Details $.grant_type = <grant_type>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |grant_type|
      |'refresh_token123'|
      |'Refresh_token'   |

  @AISP_Error_Bus
  @severity=normal
  @Regression
  Scenario Outline: Verify whether error is received when grant_type value is not provided in API request

    * def info = karate.info
    * set info.subset = 'Empty grant_type'
    Given set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    #Input invalid grant_type in API request
    And set Application_Details $.grant_type = <grant_type>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |grant_type|
      |' '|


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when valid but not allowed grant_type #key# value is sent in API request

    * def info = karate.info
    * def key = <grant_type>
    * set info.subset = 'Not allowed grant_type'
 #Input not allowed grant_type value in API request
    Given set Application_Details $.grant_type = <grant_type>
    When set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |grant_type|
      |'authorization_code' |

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal

  Scenario: Verify whether error is received when mandatory parameter grant_type is missing in API request

    * def info = karate.info
    #Remove grant_type from API request
    Given set Application_Details $.grant_type = null
    And set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400


  @AISP_Error_Tech
  @severity=normal
    Scenario: Verify whether account request is set up on passing additional parameters in client credential API request

    * def info = karate.info
    * set info.subset = 'abcdef'
  #Input not allowed grant_type value as client_credentials and valid scope in API request
    Given set Application_Details $.grant_type = 'client_credentials&scope=openid accounts payments'
    And set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    Given set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And print Application_Details
    And def apiApp1 = new apiapp()
    When call apiApp1.Account_Request_Setup(Application_Details)
    And set Result.ActualOutput.Account_Request_Setup.Output.responseStatus = apiApp1.Account_Request_Setup.responseStatus
    And set Result.ActualOutput.Account_Request_Setup.Output.response = apiApp1.Account_Request_Setup.response
    Then match apiApp1.Account_Request_Setup.responseStatus == 201


