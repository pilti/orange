@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken
@SniperTest

Feature: TPP validation for Access_token using refresh_token grant API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_Error_Bus
  @severity=normal

  Scenario: Verify whether error is received when API is triggered with TPP having PISP role

    * def info = karate.info
    Given def Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400


  @AISP_Error_Bus
  @severity=normal
  Scenario: Verify whether error is received when network certificate is not configured for TPP invoking API

    * def info = karate.info
    Given def Application_Details = active_tpp.AISP
   #Certificate is not configured for TPP invoking API
   # And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details.grant_type = "refresh_token"
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 401


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is not received when scope as accounts passed in API request for generating access token from auth code and further generating new access token using refresh token

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'

  Given def Application_Details = active_tpp.AISP
  And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
  And set ReqObjIP $.header.kid = Application_Details.kid
  And set ReqObjIP $.payload.iss = Application_Details.client_id
  And set ReqObjIP $.payload.client_id = Application_Details.client_id
  And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
  And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
  And set ReqObjIP $.payload.scope = 'openid accounts'
  And def path = stmtpath(Application_Details) + 'signin_private.key'
  And set Application_Details.path = path
  When call apiApp.CreateRequestObject(Application_Details)
  And print ReqObjIP
  And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
  And set Application_Details $.response_type = 'code id_token'
  And set Application_Details $.state = 'af0ifjsldkj'
  And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
  And call apiApp.ConstructAuthReqUrl(Application_Details)
  And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
  And print consenturl
  And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
  And def perform = Functions.launchConsentURL(webApp,consenturl)
  Then match perform.Status == 'Pass'
  And def perform = Functions.useKeyCodeApp(webApp)
  And def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
  And def perform = Functions.scaLogin(webApp,data)
  And def perform = Functions.selectAllAccounts(webApp,data)
  And set UIResult.AccountSelectionPage = perform
  And set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
  And set Application_Details.selectedAccounts = perform.SelectedAccounts
  And def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
  And set Application_Details.Permissions = perform.Permissions
  And set UIResult.ReviewConfirmPage = perform
  And set Application_Details.code = perform.Action.Authcode
  And print Application_Details
  And set UIResult.code = perform.Action.Authcode
  And print UIResult
  And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
  And def screenshot = capturescreen.captureScreenShot2(webApp)
  And def stop = webApp.stop()
 #Input invalid/not allowed scope in API request
  Given set Application_Details $.scope = <scope>
  And set Application_Details $.grant_type = "authorization_code"
  And set Application_Details $.code = UIResult.code
  And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback" + "&scope=" + Application_Details.scope
  And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
  And set Application_Details $.request_body = body
  And set Application_Details $.Authorization = auth
 #call Access_token using Authorization code grant API
  And url TokenUrl
  And header Authorization = auth
  And header Content-Type = 'application/x-www-form-urlencoded'
  And request body
  When method POST
  Then match responseStatus == 200
  Given set Application_Details $.refresh_token = response.refresh_token
  And set Application_Details $.grant_type = "refresh_token"
  And def body = "grant_type=" + Application_Details.grant_type + "&refresh_token=" + Application_Details.refresh_token + "&scope=" + Application_Details.scope
  #call Access_token using refresh token grant API
  And url TokenUrl
  And header Authorization = auth
  And header Content-Type = 'application/x-www-form-urlencoded'
  And request body
  When method POST
  Then match responseStatus == 200
  And match response contains {"access_token": '#string'}

    Examples:
      |scope|
      |'accounts'|