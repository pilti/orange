@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken

Feature: refresh_token validation for Access_token using refresh_token grant API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)
   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  @Regression
  Scenario Outline: Verify whether error is received when invalid refresh_token is sent in API request

    * def info = karate.info
    * set info.subset = 'refresh_token validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
  #Input invalid refresh_token in API request
    And set Application_Details $.refresh_token = <refresh_token>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |refresh_token|
      |'MfhDcdfjkxwolt9OrHbT4BaSVIibf2C73zhGdUyTM72345gfdg'|


  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when Empty value of refresh_token is sent in API request

    * def info = karate.info
    * set info.subset = 'refresh_token validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    #Input empty refresh_token in API request
    And set Application_Details $.refresh_token = <refresh_token>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |refresh_token|
      |' '|

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when expired refresh_token is sent in API request

    * def info = karate.info
    * set info.subset = 'refresh_token validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    #Input expired refresh_token in API request
    And set Application_Details $.refresh_token = <Expired_RefreshToken>
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

    Examples:
      |Expired_RefreshToken|
      |'fKM1txF7QFBUcxPcfVHvMuoz8cysXAb6bQWgqu3iKR'|

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether error is received when refresh_token parameter is not sent in API request

    * def info = karate.info
    * set info.subset = 'refresh_token validation'
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
   #Do not send refresh_token parameter in API request
    And set Application_Details $.refresh_token = null
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

  @Defect_1419
  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal

  Scenario: Verify whether error is received when refresh_token generated by different TPP is sent in API request

    * def info = karate.info
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * def Refresh_token = Prerequisite.Result.Access_Token_ACG.response.refresh_token
    * print Refresh_token

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.usr = user_details.Generic.G_User_1.user
     #Hit Certificate request to server
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
   #Input refresh_token generated by different TPP is sent in API request
    And set Application_Details.refresh_token = Refresh_token
    And call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 400

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether error is received when client_credential grant token is sent in API request

    * def info = karate.info
    * set info.subset = 'Token validation'
    Given set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
   #Send Client credential token instead of refresh_token in API request
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.refresh_token = apiApp.Access_Token_CCG.response.access_token
    And call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400

  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether error is received when access token having auth code grant is sent in API request

    * def info = karate.info
    * set info.subset = 'Token validation'
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * def Access_token = Prerequisite.Result.Access_Token_ACG.response.access_token
    * print Access_token
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = Access_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 400