@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@RTRP04
Feature: This feature does UI validations for deny/Renew of Refresh Token Renewal Screen

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    Given def info = karate.info
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
    * set Application_Details.usr = user_details.Generic.G_User_1.user
    * set Application_Details.otp = '123456'
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature') Application_Details
    * json Result = Prerequisite.Result1

    * call apiApp.configureSSL(Application_Details)

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    * print Application_Details.consenturl

    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(Application_Details.usr)',"otp":'#(Application_Details.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    #........................................................................
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

  #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.refresh_token = Application_Details.refresh_token

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
################################################Background end############################################

  @Regression
  Scenario: Verify user can successfully Deny a consent by clicking the Deny button and Yes on the confirmation pop-up
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal Deny Flow'

    * def modulename = call  webApp.moduleName1 = "On clicking Deny Renewal"
    * def deny = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.denybutton)
    * set Result.ActualOutput.UI.Output.ClickDeny = deny
    * def modulename = call  webApp.moduleName1 = "On Confirming Deny"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.confrimDenybutton)
    * set Result.ActualOutput.UI.Output.ConfirmDeny = click

    * def wait = ReusableFunctions.sleep(20000)
    * def error = ReusableFunctions.getCurrentURL(webApp)
    * set Result.ActualOutput.UI.Output.Expected = 'https://www.bankofireland.com/#state=af0ifjsldkj&error=access_denied'
    * set Result.ActualOutput.UI.Output.Actual = error
    * match error == 'https://www.bankofireland.com/#state=af0ifjsldkj&error=access_denied'
    * def stop =  webApp.stop();

    #Call another API  to check previous consent and refresh token is still valid
   ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.UI.Output.Multi_Account_Information = perform.Status
    And set Result.ActualOutput.UI.Output.Multi_Account_Information = perform.Landing_Page
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 200
  ###################################################################################################################

  @Regression
  @Test
  Scenario: Verify that the user can successfully Confirm a consent by clicking the Renew authorisation button
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal ReNew Flow'
    * def modulename = call  webApp.moduleName1 = "On clicking ReNew Button"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    * set Result.ActualOutput.UI.Output.ClickRenew = click

     # take renewal journey Authcode
    * set Result.ActualOutput.UI.Output.On_Renew.Expected = 'Authcode in Redirect URL'
    * set Result.ActualOutput.UI.Output.On_Renew.Actual = Functions.getUrl(webApp)
    * def perform = Functions.getAuthCodeFromURL(webApp)
    * set Result.ActualOutput.UI.Output.NewAuthCode = perform.Authcode
    And set Application_Details.codeNew = perform.Authcode

    #Call Access_Token_ACG API with new Authcode after renewal
    When set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.code = Application_Details.codeNew
    And call apiApp.Access_Token_ACG(Application_Details)
    * print apiApp.Access_Token_ACG.response

    And set Result.ActualOutput.Access_Token_ACG_afterRenew.Output.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.ActualOutput.Access_Token_ACG_afterRenew.Output.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token_renew = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token_renew = apiApp.Access_Token_ACG.response.access_token
    * match Result.ActualOutput.Access_Token_ACG_afterRenew.Output.responseStatus == 200
    * print 'new RT: #####################'
    * def d = Application_Details.refresh_token_renew
    * print d
  #.....................................................................................................

    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * def d = Application_Details.access_token
    * print d
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 401
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_OldToken.response = apiApp.Multi_Account_Information.response
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_OldToken.responseStatus = apiApp.Multi_Account_Information.responseStatus
    * print 'MultiAcc end #####################'
    #..............................................................................
    #Call Access_Token_RTG API with newauthcode
    * print Application_Details.refresh_token_renew
    * def apiApp1 = new apiapp()
    When set Application_Details $.request_method = 'POST'
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details.refresh_token = Application_Details.refresh_token_renew
    And call apiApp1.Access_Token_RTG(Application_Details)
    * print apiApp1.Access_Token_RTG.response

    And set Result.ActualOutput.Access_Token_RTG_afterRenew.Output.responseStatus = apiApp1.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG_afterRenew.Output.response = apiApp1.Access_Token_RTG.response
    And set Application_Details $.refresh_token_final = apiApp1.Access_Token_RTG.response.refresh_token
    And set Application_Details $.access_token_final = apiApp1.Access_Token_RTG.response.access_token
    * match apiApp1.Access_Token_RTG.responseStatus == 200

    #Get AccounID from Multi Account Info API
    When set Application_Details $.request_method = 'GET'
    And set Application_Details $.access_token = apiApp1.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp1.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp1.Multi_Account_Information.responseStatus == 200
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_RenewedToken.responseStatus = apiApp1.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_RenewedToken.response = apiApp1.Multi_Account_Information.response




