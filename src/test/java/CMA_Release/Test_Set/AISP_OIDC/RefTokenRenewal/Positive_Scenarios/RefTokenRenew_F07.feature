@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@RTR7
Feature: To verify that TPP can renew refresh token for the consent when refresh token is expired

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
    #expired refresh token with its consent URl is needed

  Scenario: To verify that TPP can renew refresh token for the consent when refresh token is expired
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def fileName = 'ExpiredConsent_Generic_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def expiredRefreshToken = profile.refresh_token

   #'e2gaiPe4einhhiRiRefdTX0NUUDR0Glvjxf1ucxW9q'
     #* def expiredRefreshToken = 'Z4m4oKUAZhTjzsC4GMrsyKkx9JQB9e6Flt1AAwTuFR'
    #* def consenturl = 'https://auth.boitest.net/oauth/as/authorization.oauth2?&client_id=5R85IgDTmYPZgkUDy3r2sJ&response_type=code id_token&scope=openid accounts&state=af0ifjsldkj&nonce=n-0S6_WzA2Mj&redirect_uri=https://www.getpostman.com/oauth2/callback&request=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InMxd3U4Ylh5bnZHZzVaWEhUT2FXNWk5RlVDOCJ9.eyJpc3MiOiI1Ujg1SWdEVG1ZUFpna1VEeTNyMnNKIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLmJvaXRlc3QubmV0IiwicmVzcG9uc2VfdHlwZSI6ImNvZGUgaWRfdG9rZW4iLCJjbGllbnRfaWQiOiI1Ujg1SWdEVG1ZUFpna1VEeTNyMnNKIiwicmVkaXJlY3RfdXJpIjoiaHR0cHM6Ly93d3cuZ2V0cG9zdG1hbi5jb20vb2F1dGgyL2NhbGxiYWNrIiwic2NvcGUiOiJvcGVuaWQgYWNjb3VudHMiLCJzdGF0ZSI6ImFmMGlmanNsZGtqIiwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7InVzZXJpbmZvIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImFlOTZmOTEzLTgzNjUtNDM1My04NzczLWEwYmQwYmRmNGU5NyIsImVzc2VudGlhbCI6dHJ1ZX19LCJpZF90b2tlbiI6eyJvcGVuYmFua2luZ19pbnRlbnRfaWQiOnsidmFsdWUiOiJhZTk2ZjkxMy04MzY1LTQzNTMtODc3My1hMGJkMGJkZjRlOTciLCJlc3NlbnRpYWwgIjp0cnVlfSwiYWNyIjp7ImVzc2VudGlhbCI6dHJ1ZSwidmFsdWVzIjpbInVybjpvcGVuYmFua2luZzpwc2QyOnNjYSIsInVybjpvcGVuYmFua2luZzpwc2QyOmNhIl19fX19.MWjcD1-uCfNlMEJmWrnMwZVhi4OvTOMg-3_FvIcLh7b1y1aGxh19sYFUvzE5rmgaqFL7EHHm5jabhIgSagk88g1RazEEJJmRSHQMR4SGImk5GCFi-btez0n3SCKFUo5Yb3YJO3f5m2K-gyk0kK8yG2EufmhhAdcHyAAvWISrHX-78Crgu2rMEMaLrf1FtILDx0EZqlhG9UOsc8ZYYPlDmlC-7MBQosMl63gzUYOZ_dBNNgJktrYh-Wn7XSdTheqrs39Sih2PAvw8zYJqxBXLONrIe2Pa1HUo5e5KqlXH76Dxqa2hsR41GaT4pYcYsPNofSvbzx33O2-mElBvRu1UWg'

    Given json Application_Details1 = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details1)

    * set Result.Input.ExpiredRefreshToken = expiredRefreshToken
    * set Result.Input.ConsentURLForExpiredRefreshToken = profile.consenturl

     #Call Access_Token_RTG API to verify expired refresh token
    And set Application_Details1 $.grant_type = "refresh_token"
    And set Application_Details1 $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details1 $.request_method = 'POST'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details1 $.refresh_token = expiredRefreshToken

    When call apiApp.Access_Token_RTG(Application_Details1)
     #Validate response and update result json
    And set Result.ActualOutput.Access_Token_RTG.Input.ExpiredRefreshToken = expiredRefreshToken
    And set Result.ActualOutput.Access_Token_RTG_ExpiredRefreshToken.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG_ExpiredRefreshToken.response = apiApp.Access_Token_RTG.response
      #Perform Assertion
    Then match apiApp.Access_Token_RTG.responseStatus == 401

     #############################################################################################################
       #Launch consent renewal screen

    And def path = call  webApp.path1 = info1.path
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

      # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
#     Given json Application_Details = active_tpp.AISP_PISP
#     * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')

    Given def perform = Functions.launchConsentURL(webApp,profile.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page

   # renew the refresh token first time
    * def modulename = call  webApp.moduleName1 = "On clicking ReNew Button"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    * set Result.ActualOutput.UI.Output.ClickRenew = click

    * set Result.ActualOutput.UI.Output.On_Renew.Expected = 'Authcode in Redirect URL'
    * set Result.ActualOutput.UI.Output.On_Renew.Actual = Functions.getUrl(webApp)
    * def perform = Functions.getAuthCodeFromURL(webApp)
    * set Result.ActualOutput.UI.Output.AuthCode = perform.Action.Authcode

    * print "renewal1 start"
    #Call Access_Token_ACG API with renewal1
    When set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.code = Application_Details.code
    * print Application_Details
    And call apiApp.Access_Token_ACG(Application_Details)

    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token_renew = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token_renew = apiApp.Access_Token_ACG.response.access_token
    * match apiApp.Access_Token_ACG.responseStatus = 200

    #Call Access_Token_RTG API with renewal1
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.refresh_token = Application_Details $.refresh_token_renew
    And call apiApp.Access_Token_RTG(Application_Details)

    And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew.response = apiApp.Access_Token_RTG.response
    And set Application_Details $.refresh_token_final = apiApp.Access_Token_RTG.response.refresh_token
    And set Application_Details $.access_token_final = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus = 200

     #Call another API  to check renewed refresh token is valid
      ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Result.ActualOutput.Multi_Account_Information.responseStatus = apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.response = apiApp.Multi_Account_Information.response
