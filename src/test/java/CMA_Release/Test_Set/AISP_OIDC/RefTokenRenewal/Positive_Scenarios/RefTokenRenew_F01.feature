@AISP
@AISP_UAT
@AISP_Consent
@AISP_UAT_Consent
@AISP_RefreshTokenRenewal
@RTR01

Feature: This feature Demonstrate UI validations on successful launch of Refresh Token Renewal Screen

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    * set info.Screens = 'Yes'
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')
    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    * json Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    #* def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl
    #Configure Network Certificate#
    * call apiApp.configureSSL(Application_Details)
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    #Launch consent renewal screen
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def path = call  webApp.path1 = info1.path
    * def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * match perform.Status == 'Pass'
    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * match perform.Status == 'Pass'
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
    #Background end#

  @Regression
  Scenario: Verify that the Refresh Token Renewal Landing Page is labelled as Authorisation Renewal
    * set info.Screens = 'Yes'
    * set info.subset = "Authorization renewal Title"
    Given def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    And set Application_Details.path = path
    And def modulename = call  webApp.moduleName1 = 'Authorisation Renewal Title'
    When def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    And def txt = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    Then match element == true


  Scenario: Verify the text for user to know that TPP is requesting for an on Authorisation Renewal
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal text'
    Given def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    And set Application_Details.path = path
    When def modulename = call  webApp.moduleName1 = "Check Authorisation renewal Text"
    And def ExpectedText = 'For security purposes, your authorisation must be renewed every 90 days in order for the TPP to continue accessing your data. You previously consented to give SIT_AISP_PISP (Bank of Ireland TPP - AISP and PISP) access to the below information.SIT_AISP_PISP (Bank of Ireland TPP - AISP and PISP) has requested your renewed authorisation for repeat access to the agreed upon information.'
    And def ActualText1 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalTextPart1)
    And def ActualText2 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalTextPart2)
    And def ActualText3 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalTextPart3)
    And def ActualText4 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalTextPart4)
    And def ActualText5 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalTextPart5)
    And def CompleteText = ActualText1 + " "+ ActualText2 + " " + ActualText3 + ActualText4 + " "+ActualText5
    Then match ExpectedText == CompleteText

  @Regression
  Scenario: Verify the availability of the Deny & Confirm button on Authorisation Renewal page

    * set info.Screens = 'Yes'
    * set info.subset = 'Availability of Buttons'
    Given def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    And set Application_Details.path = path
    And def modulename = call  webApp.moduleName1 = "Check Deny & Renew Button"
    When def button1 = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.denyButton)
    Then match button1 == true
    And def button2 = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.renewButton)
    And match button2 == true


  Scenario: Verify the text in the Important Info section on Authorisation Renewal page
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = ' Authorisation Renewal Important Info'
    Given def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    And def modulename = call  webApp.moduleName1 = "Check Text in Imp Info"
    And def ExpectedHeading = 'Important Information'
    And def ExpectedText1 = 'You can revoke access to this service through the third party provider or by calling Bank of Ireland.'
    When def ExpectedText2 = 'We will provide permissions information to the extent that it is currently available on our digital banking channels.'
    And def ActualText1 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.ImpInfoText1)
    And def ActualText2 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.ImpInfoText2)
    And def ActualHeading = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.ImpInfoheading)
    Then match ExpectedText1 == ActualText1
    And match ExpectedText2 == ActualText2
    And match ExpectedHeading == ActualHeading


  Scenario: verify the end of consent date is the confirmation message below the Requested Transaction Access grid
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal consent date confirmation message'
    Given def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    And set Application_Details.path = path
    And def modulename = call  webApp.moduleName1 = "Check Consent expiry date present"
    When def scroll = ReusableFunctions.scrollTillElement(webApp, ObjectsRenewalScreen.consentValidityText)
    And def message = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.consentValidityText)
    Then match message == true
