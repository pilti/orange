@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@RTR6
Feature: To verify that TPP can renew refresh token for the consent which is renewed few minutes before

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
#    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
#    And json Result = Prerequisite.Result1
    And set Result.Input.TPPRole = 'AISP_PISP'
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl
    * set Application_Details.refresh_token = profile.refresh_token
    * set Result.Input.ConsentURL = profile.ConsentURL

    #Launch consent renewal screen
    Given def info = karate.info
    * set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
################################################Background end#####################
  @RTR61
  Scenario: To verify that TPP can renew refresh token for the consent which is renewed few minutes before
    #Call Access_Token_RTG API to generate new access token from first consent
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    When call apiApp.Access_Token_RTG(Application_Details)
     #Validate response and update result json
    And set Result.Access_Token_RTG.Input = Application_Details
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response
    And set Application_Details.access_token = apiApp.Access_Token_RTG.access_token
      #Perform Assertion
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    ##################################################################################################################
    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page

   # renew the refresh token first time
   * def modulename = call  webApp.moduleName1 = "On clicking ReNew Button"
   * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
   * set Result.ActualOutput.UI.Output.ClickRenew = click

    * set Result.ActualOutput.UI.Output.On_Renew.Expected = 'Authcode in Redirect URL'
    * set Result.ActualOutput.UI.Output.On_Renew.Actual = Functions.getUrl(webApp)
    * def perform = Functions.getAuthCodeFromURL(webApp)
    * set Result.ActualOutput.UI.Output.NewAuthCode = perform.Authcode
    And set Application_Details.codeRenew1 = perform.Authcode
   * def stop = webApp.stop()

    #Call another API  to check previous refresh token is not valid
   ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 401
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_OldToken.response = apiApp.Multi_Account_Information.response
    And set Result.ActualOutput.UI.Output.Multi_Account_Information_OldToken.responseStatus = apiApp.Multi_Account_Information.responseStatus

    * print "renewal1 start"
   #Call Access_Token_ACG API with renewal1
   When set Application_Details $.grant_type = "authorization_code"
   And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
   And set Application_Details $.code = Application_Details.codeRenew1
    * print Application_Details
    And call apiApp.Access_Token_ACG(Application_Details)
   And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew1.responseStatus = apiApp.Access_Token_ACG.responseStatus
   And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew1.response = apiApp.Access_Token_ACG.response

    And set Application_Details $.refresh_token_renew1 = apiApp.Access_Token_ACG.response.refresh_token
   And set Application_Details $.access_token_renew1 = apiApp.Access_Token_ACG.response.access_token
    * match apiApp.Access_Token_ACG.responseStatus = 200

    #Call Access_Token_RTG API with renewal1
   When set Application_Details $.grant_type = "refresh_token"
   And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
   And set Application_Details $.refresh_token = Application_Details $.refresh_token_renew1
   And call apiApp.Access_Token_RTG(Application_Details)

   And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew1.responseStatus = apiApp.Access_Token_RTG.responseStatus
   And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew1.response = apiApp.Access_Token_RTG.response
   And set Application_Details $.refresh_token_final1 = apiApp.Access_Token_RTG.response.refresh_token
   And set Application_Details $.access_token_final1 = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus = 200

  ###################Renew Refresh token second time####################################

    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page

   # renew the refresh token first time
    * def modulename = call  webApp.moduleName1 = "On clicking ReNew Button"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    * set Result.ActualOutput.UI.Output.ClickRenew = click

    * set Result.ActualOutput.UI.Output.On_Renew.Expected = 'Authcode in Redirect URL'
    * set Result.ActualOutput.UI.Output.On_Renew.Actual = Functions.getUrl(webApp)
    * def perform = Functions.getAuthCodeFromURL(webApp)
    And set Application_Details.codeRenew2 = perform.Authcode
  #######################################################################################################
       * print "renewal2 start"

    #Call Access_Token_ACG API
    When set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.code = Application_Details.codeRenew2
    And call apiApp.Access_Token_ACG(Application_Details)
    * print Application_Details
    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew2.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew2.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token_renew2 = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token_renew2 = apiApp.Access_Token_ACG.response.access_token
    * match apiApp.Access_Token_ACG.responseStatus = 200

    #Call Access_Token_RTG API
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.refresh_token = Application_Details $.refresh_token_renew2
    And call apiApp.Access_Token_RTG(Application_Details)

    And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew2.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.UI.Output.Access_Token_RTG_afterRenew2.response = apiApp.Access_Token_RTG.response
    And set Application_Details $.refresh_token_final2 = apiApp.Access_Token_RTG.response.refresh_token
    And set Application_Details $.access_token_final2 = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus = 200

    * print "Multi account API start"
    #Call another API  to check previous consent and refresh token is still valid
   ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token_final2
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Result.ActualOutput.Multi_Account_Information.responseStatus = apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.response = apiApp.Multi_Account_Information.response







