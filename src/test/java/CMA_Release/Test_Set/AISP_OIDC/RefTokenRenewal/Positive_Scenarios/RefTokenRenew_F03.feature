@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@Refresh03
Feature: This feature does UI validations on successful launch of Refresh Token Renewal Screen

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
    And set Result.Input.TPPRole = 'AISP_PISP'

    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   # user 201659
    And set user_details.usr = user_details.Generic.G_User_1.user
    And set user_details.otp = user_details.Generic.G_User_1.otp

    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl
    * set Result.Input.ConsentURL = profile.ConsentURL

   ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(Application_Details)
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token

  #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.refresh_token = profile.refresh_token

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    * print Application_Details.consenturl

    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
################################################Background end#####################

  @Regression
  Scenario: Verify that only three accounts are displayed by default in the Selected Accounts section
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'By Default only 3 Selected Accounts Displayed'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    * def modulename = call  webApp.moduleName1 = "Default No. of Accounts on Renewal page"
    * def acc = ReusableFunctions.sizeOfList(webApp, ObjectsRenewalScreen.selectedAccountNames)
    * set Result.ActualOutput.UI.Output.Expected = 'By Default only 3 Selected Accounts Displayed on Renewal screen'
    * set Result.ActualOutput.UI.Output.Actual = ReusableFunctions.findUIElementsList(webApp, ObjectsRenewalScreen.selectedAccountNames)
    * assert acc <= 3

  @RTR32
  Scenario: verify that clicking Show All the grid displays all the accounts selected by the user on the selection page

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'No. of accounts on clicking show ALL'
    * def modulename = call  webApp.moduleName1 = "Before Clicking Show ALL"
    * def screen = screenshot.TakeScreenShot(webApp)
    * def acc = ReusableFunctions.sizeOfList(webApp, ObjectsRenewalScreen.selectedAccountNames)
    * set Result.ActualOutput.UI.Output.BeforeClicking_ShowALL.Expected = 'By Default only 3 Selected Accounts Displayed'
    * set Result.ActualOutput.UI.Output.BeforeClicking_ShowALL.Actual = acc
    * match acc == 3
   # * def accounts = Functions.ReviewConfirmValidateAccounts(webApp, profile.selected_accounts)
    * def c1 = Functions.VerifyListsOnReAuthorisarionPage(webApp, ObjectsRenewalScreen.selectedAccountNamesAll , profile.selected_accounts)
    * set Result.ActualOutput.UI.Output.AfterClicking_ShowALL.Expected= 'No. of Accounts during 1st consent EQUALS No. of Accounts on Renewal screen when Show ALL is clicked'
    * set Result.ActualOutput.UI.Output.ConsentPageAccounts = Application_Details.selectedAccounts
    * set Result.ActualOutput.UI.Output.RenewalPageAccounts = ReusableFunctions.findUIElementsList(webApp, ObjectsRenewalScreen.selectedAccountNames)
    * set Result.ActualOutput.UI.Output.AfterClicking_ShowALL.Actual = c1
    * match c1 == true

  @RTR33
  Scenario: verify that the Show All text is changed to Show Less once user has clicked it to see all the accounts list

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Check Presence of show Less'
    * def modulename = call  webApp.moduleName1 = "Before Clicking Show ALL"
    * def screen = screenshot.TakeScreenShot(webApp)
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsReview_confirm.show_all)
    * def wait = ReusableFunctions.sleep(2000)
    * def modulename = call  webApp.moduleName1 = "After Clicking Show ALL"
    * set Result.ActualOutput.UI.Output.CheckPresenceOfShowLessButton.Expected = true
    * def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsReview_confirm.show_less)
    * set Result.ActualOutput.UI.Output.CheckPresenceOfShowLessButton.Actual = element

  @RTR3Enits
  Scenario: Verify that user can continue with authorisation renewal journey on clicking Go Back button in Deny authorisation PopUp

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'UI validations on Clicking Deny Renewal'
    * def modulename = call  webApp.moduleName1 = "After Clicking Deny Renewal Button"
    * def deny = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.denybutton)
    * set Result.ActualOutput.UI.Output.ClickDeny = deny

    * def modulename = call  webApp.moduleName1 = "Check Deny Renewal POP-Up Message"
    * def denyPopUpmsg = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.denyPopUpmsg)
    * def tpp =  active_tpp.AISP_PISP.Application_Name
    * match denyPopUpmsg == true
    * set Result.ActualOutput.UI.Check_DenyPopUpText.Expected = "Selecting " +"\'Yes deny\'" + " will prevent "+ tpp +" from continuing to access your account information."
    * def denyPopUpmsgText = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.denyPopUpmsg)
    * match denyPopUpmsgText ==  Result.ActualOutput.UI.Check_DenyPopUpText.Expected
    * set Result.ActualOutput.UI.Check_DenyPopUpText.Actual = denyPopUpmsgText
    * set Result.ActualOutput.UI.Output.Check_DenyPopUpAppears = denyPopUpmsg

    * def modulename = call  webApp.moduleName1 = "Check Deny Renewal PopUp heading"
    * def denyheading = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.denyPopUpHeading)
    * match denyheading == true
    * set Result.ActualOutput.UI.Output.CheckDenyPopUpHeading = denyheading

    * def modulename = call  webApp.moduleName1 = "Check GoBack Button"
    * def goBackbutton = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.goBackbutton)
    * match goBackbutton == true
    * set Result.ActualOutput.UI.Output.CheckDenyPopup_GoBackbutton = goBackbutton

    * def confrimDenybutton = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.confrimDenybutton)
    * match confrimDenybutton == true
    * set Result.ActualOutput.UI.Output.CheckDenyPopup_confrimDenybutton = confrimDenybutton

    * def modulename = call  webApp.moduleName1 = "On clicking GoBack Button"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.goBackbutton)
    * set Result.ActualOutput.UI.Output.ClickGoBackButton = click

    * def modulename = call  webApp.moduleName1 = 'Land on Authorisation renewal Page again'
    * def click = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    * def Text = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    * set Result.ActualOutput.UI.Output.ReturnToRenewalPage.Check_RenewalPageHeading.Expected = 'Authorisation renewal'
    * set Result.ActualOutput.UI.Output.ReturnToRenewalPage.Check_RenewalPageHeading.Actual = Text
    * match click == true
   