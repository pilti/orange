@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@RTR5
Feature: Verify that the consent status does not change on clicking Back to third party from authorisation renewal

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
#    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
#    And json Result = Prerequisite.Result1

    And set Result.Input.TPPRole = 'AISP_PISP'
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl
    * set Application_Details.refresh_token = profile.refresh_token
    * set Result.Input.ConsentURL = profile.ConsentURL

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = info.scenarioName
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path

    * print Application_Details.consenturl
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
        apiApp.write(Result,info);
       }
    """
################################################Background end#####################
  @RTR51
  Scenario: Verify that the consent status does not change on clicking Back to third party from authorisation renewal
    * set info.subset = "Authorisation renewal,back to third party"
     #Call Access_Token_RTG API to generate new access token from first consent
    When set Application_Details $.request_method = 'POST'
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    When call apiApp.Access_Token_RTG(Application_Details)

     #Validate response and update result json
    And set Result.Access_Token_RTG.Input = Application_Details
    And set Application_Details.access_token = apiApp.Access_Token_RTG.access_token
      #Perform Assertion
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    #launch renewal screen and Go back
    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def modulename = call  webApp.moduleName1 = 'On clicking back to third party'
    * def element = ReusableFunctions.ClickElement(webApp, ObjectsLogin.back_to_tpp_button)
    * set Result.ActualOutput.UI.Output.ClickBackToThirdParty = element

    Then def perform = Functions.getUrl(webApp)
    * set Result.ActualOutput.UI.Output.ClickBackToThirdParty.Expected = 'https://app.getpostman.com/oauth2/callback#state=af0ifjsldkj&error=access_denied'
    * set Result.ActualOutput.UI.Output.ClickBackToThirdParty.Actual = perform.ActualUrl
    * match perform.ActualUrl == Result.ActualOutput.UI.Output.ClickBackToThirdParty.Expected
    * def stop = call  webApp.stop()

     #Call another API  to check previous consent and refresh token is still valid
   ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    #Perform Assertion
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Result.ActualOutput.Multi_Account_Information.responseStatus = apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.response = apiApp.Multi_Account_Information.response




