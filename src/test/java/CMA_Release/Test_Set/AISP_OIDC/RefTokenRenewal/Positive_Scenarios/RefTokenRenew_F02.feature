@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@Refresh02
Feature: This feature does UI validations between ConsentRenewal Screen and Initial Consent Screen

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = info.scenarioName
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')
    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    * json Application_Details = active_tpp.AISP_PISP
    #* def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl
    * call apiApp.configureSSL(Application_Details)
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
    #Launch consent renewal screen
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def path = call  webApp.path1 = info1.path
    * def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * match perform.Status == 'Pass'
    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * match perform.Status == 'Pass'

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
#Background end#

  @RTR21nits
  Scenario: Verify previously consented accounts should be listed in Selected Accounts section of renewal process
    * def info = karate.info
    * set info.Screens = 'Yes'
    Given set info.subset = 'Authorisation Renewal Consented Accounts Verification'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    When def modulename = call  webApp.moduleName1 = "Verify Accounts on Consent with Renewal page"
    And def c1 = Functions.VerifyListsOnReAuthorisarionPage(webApp, ObjectsRenewalScreen.selectedAccountNames, profile.selected_accounts)
    Then match c1 == true

  @RTR22
  Scenario: Verify that the user can verify the permissions to which consent is being reauthorized
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal consented permissions verification'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path
    * print Application_Details.Permissions
    * def modulename = call  webApp.moduleName1 = "Verify Permissions on Consent with Renewal page"
    * print profile.permissions
    * def permission_OnRenewal = Functions.Permission(webApp)
    * def c1 = Functions.VerifyListsOnReAuthorisarionPage(webApp, permission_OnRenewal.RetrivedPemissionArray, profile.permissions)
    * set Result.ActualOutput.UI.Output.ConsentPagePermissions = Application_Details.Permissions
    * set Result.ActualOutput.UI.Output.RenewalPagePermissions = ReusableFunctions.findUIElementsList(webApp, ObjectsRenewalScreen.selectedPermissions)
    * set Result.ActualOutput.UI.Output.Expected = 'Permissions set during 1st consent EQUALS Permissions displayed on Renewal screen'
    * set Result.ActualOutput.UI.Output.Actual = c1
    * match c1 == true

  @Regression
  Scenario: Verify Consent Expiry date is same as given in intial consent
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Authorisation Renewal verify consent date'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

    * def modulename = call  webApp.moduleName1 = "Check RenewalConsent expiry date with 1stConsent"
    * def Date2 = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.consentValidityDate)
    * print Date2
    * set Result.ActualOutput.UI.Output.Expected = profile.ConsentExpiry
    * set Result.ActualOutput.UI.Output.Actual = Date2
    * match Date2 == profile.ConsentExpiry