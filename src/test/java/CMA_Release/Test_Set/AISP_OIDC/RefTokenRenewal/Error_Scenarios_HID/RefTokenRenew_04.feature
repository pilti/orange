@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken
@AISP_Error_Bus
@Neetika
Feature: This feature does Error validations on successful launch of Consent while Refresh Token Renewal

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def PageObj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Result.RefreshTokenRenewalPrerequisite = Prerequisite

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    * print Application_Details.consenturl
    * set Result.ConsentUrl = consenturl
    Given def perform = Functions.launchConsentURL(webApp, Application_Details.consenturl)

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    * def data = {"usr":'59000037',"otp":'',"action":"continue","Confirmation":"Yes"}
    Then def perform = Functions.scaLogin(webApp,data)

    Then def perform1 = Reuseable.cleartext(webApp,PageObj.username_text)
    * set data.otp = '123'
    Then def perform2 = Functions.scaLogin(webApp,data)

    Then def perform1 = Reuseable.cleartext(webApp,PageObj.username_text)
    Then def perform1 = Reuseable.cleartext(webApp,PageObj.otp_text)
    * set data.otp = '1234'
    Then def perform2 = Functions.scaLogin(webApp,data)
    And def closeBrowser = call  webApp.stop()

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)

    Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    Then def perform = Functions.useKeyCodeApp(webApp)

    * def data = {"usr":'59000037',"otp":"123456","action":"continue","Confirmation":"Yes"}
    Then def perform = Functions.scaLogin(webApp,data)

    Then def uiresult = Reuseable.getElementText(webApp.driver1,PageObj.error_msg)
  * print uiresult
  * set Result.UIResult = {"ActualText":'#(uiresult)'}
  * def capturescreen = capturescreen.captureScreenShot2(webApp)
  * match uiresult == "Your account  is temporarily blocked. Please try again in 1 hour."

###################################################################################################
@Blocked_User_at_365
Scenario Outline: Verify the error when User ID is not registered for 365Online

Given def info = karate.info
And set info.Screens = 'Yes'
And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
And def path = call  webApp.path1 = info1.path

Given def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)

Then def perform = Functions.useKeyCodeApp(webApp)
* print perform
* def data = {"usr":<user>,"otp":<otp>,"action":"continue","Confirmation":"Yes"}
Then def perform = Functions.scaLogin(webApp,data)


Then def uiresult = Reuseable.getElementText(webApp.driver1,PageObj.error_msg)
* print uiresult
* set Result.UIResult = {"ActualText":'#(uiresult)'}
* def capturescreen = capturescreen.captureScreenShot2(webApp)
* match uiresult == "Login was unsuccessful, your access to  Digital Channels  may be blocked (includes Account Access site and 365 online)."

Examples:
| user     | otp      |
| '59000037' | '123456' |
