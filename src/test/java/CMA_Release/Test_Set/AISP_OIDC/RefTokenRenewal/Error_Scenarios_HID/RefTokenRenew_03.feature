@AISP
@AISP_Consent
@AISP_AccessTokenUsingRefToken
@AISP_Error_Bus
@Neetika
Feature: This feature does Error validations on successful launch of Consent while Refresh Token Renewal

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def PageObj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Result.RefreshTokenRenewalPrerequisite = Prerequisite

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    * print Application_Details.consenturl
    * set Result.ConsentUrl = consenturl
    Given def perform = Functions.launchConsentURL(webApp, Application_Details.consenturl)

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
#################################################Background End#######################################################
@Sc03_01
Scenario Outline: Verify the error when User ID is #key
  Given def info = karate.info
  * def key = <key>
  * set info.Screens = 'Yes'
  * set info.key = <key>
And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

Then def perform = Functions.useKeyCodeApp(webApp)
* print perform
* def data = {"usr":<user>,"otp":<otp>,"action":"continue","Confirmation":"Yes"}
Then def perform = Functions.scaLogin(webApp,data)

Then def uiresult1 = Reuseable.getElementText(webApp.driver1,PageObj.error_msg)
 * def uiresult2 = Reuseable.getElementText(webApp.driver1,PageObj.error_msg_line2)
* print uiresult
* set Result.UIResult = {"ActualText1":'#(uiresult1)', "ActualText2":'#(uiresult2)'}
* def capturescreen = capturescreen.captureScreenShot2(webApp)
* match uiresult1 == <error_msg1>
* match uiresult2 == <error_msg2>
  Examples:
   | user     | otp     | key                             | error_msg1 | error_msg2  |
   | '170896' | '123456' |'User is not registered for HID'|'The login details you have entered are not registered with a KeyCode app.' |'To register - see 'KeyCode Help' for further information.' |
   | '170896' | '123456' |'User does not exist on HID'    |'The login details you have entered are not registered with a KeyCode app.'|'Please ensure that you have de-registered and registered your new serial number on 365 online, see 'KeyCode help' for further information.'|