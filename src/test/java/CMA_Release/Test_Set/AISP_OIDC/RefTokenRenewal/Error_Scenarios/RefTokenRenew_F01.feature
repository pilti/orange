@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus

Feature: This feature does negative UI validations on successful launch of Refresh Token Renewal Screen

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')
    * def ObjectLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    * json Application_Details = active_tpp.AISP_PISP
    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

# call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * set Application_Details.consenturl = curl

   ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(Application_Details)
        #Launch consent renewal screen
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def path = call  webApp.path1 = info1.path
    * print Application_Details.consenturl
    * def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * match perform.Status == 'Pass'
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
################################################Background end#####################

  @RTRE1

  Scenario: Verify there is no checkbox and text for user to confirm on authorisation renewal screen
    * set info.Screens = 'Yes'
    * set info.subset = 'No CheckBox for confirmation on Renewal Page'
    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    When def perform = Functions.scaLogin(webApp,data)
    And print perform
    Then match perform.Status == 'Pass'
    And def modulename = call  webApp.moduleName1 = 'Check Confirm CheckBox not present'
    And def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsReview_confirm.Check_box_confirm)
    And match element == false
 #############################################################################################

  @RTRError2
  Scenario: Verify Session timeout pop up when refresh renewal screen stays idle for more than 300 seconds
    * set info.Screens = 'Yes'
    * set info.subset = 'Session Timeout'
    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    When def perform = Functions.scaLogin(webApp,data)
    And print perform
    Then match perform.Status == 'Pass'
    # wait for 300 secs for timeout
    And def wait = ReusableFunctions.sleep(300000)
    And def modulename = call  webApp.moduleName1 = 'clicking anywhere on page after timeoutwait'
    And def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    And def popup = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.timeoutheading)
    And match popup == true
    And def popup = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.timeoutMsg)
    And match popup == true
    And def popupText = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.timeoutMsg)
    And match popupText == Result.ActualOutput.UIResult.TimeoutMsgExpectedText
    And def modulename = call  webApp.moduleName1 = 'check Return button on timeout'
    And def returnButton = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.timeoutReturnButton)
    And match returnButton == true
    And def modulename = call  webApp.moduleName1 = 'Click timeout'
    And def clicktimeout = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.timeoutReturnButton)
    # callback URl error code
    And def urlTimeout = Functions.getUrl(webApp)
    And match urlTimeout.ActualUrl == Result.ActualOutput.UIResult.OnTimeout.CallBackURLExpected
  #############################################################################################

  @RTRError3
  Scenario: Verify error on logging into B365 with diff user at time of authorisation renewal than the user provided during consent

  # log in with diff user than that used while providing consent
    * def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}

    When def perform = Functions.scaLogin(webApp,data)
    And print perform
    Then match perform.Status == 'Fail'
    And def modulename = call  webApp.moduleName1 = 'Check Login error'
    And def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.errorMsg)
    And match element == true
    And def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.loginErrorOnDiffuser)
    And match element == true
