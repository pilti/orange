@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR13

Feature: This Feature demonstrates the Error Scenarios for Refresh Renewal Token consent creation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
   * def Login_obj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    Given json Application_Details = active_tpp.AISP_PISP
    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1
  ################################################################################Pre-requisite###############################################
  @RTRE131
  Scenario Outline: Verify error message when id_token in resource is incorrect #key#

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    ############################Set input parameters for CreateRequestObjec######################################################################
    * def key = <key>
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = <value>
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = <value1>
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.consenturl = consenturl
    ##########################################################################################################################

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    Given def perform1 = Functions.launchConsentURL(webApp,consenturl)
    #* match perform1.reasonText == "Landing Page is not as expected!"

    Then def perform = Functions.getUrl(webApp)
    * print perform
    * set Result.ActualOutput.UIresult.Expected = <Expected>
    * set Result.ActualOutput.UIresult.Actual = perform
    * match perform.ActualUrl == <Expected>

    Examples:
      | key    | value                                  | value1             |Expected|
      | '_002' | Application_Details.AccountRequestId   |       ''           |  'https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error'|
      | '_003' | 'pahgjhgjkhjkkl'                       | 'abciihioaeh2456465'|'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'|

##############################################################################################################################################

  @RTRE132
  Scenario: Verify the error message when the request object Signature verification fails with the certificate

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'request object Signature verification failure with certificate'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

     ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def ip_path = active_tpp.AISP
    * def path = stmtpath(ip_path) + 'signin_private.key'

    * set Application_Details.path = path

  ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.Input.SiningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.ActualOutput.UIresult.Expected = '400 - Authorization server can not verify the signed request'
    * set Result.ActualOutput.UIresult.Actual = perform.ActualText
    * match perform.ActualText == "400 - Authorization server can not verify the signed request"
    * def stop = call  webApp.stop()

####################################################################################################################################
  @RTRE133
  Scenario Outline: Verify the error message when incorrect clientID #key# is passed in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And set info.key = <client_id>
    And set info.subset = <client_id>
      * def key = <client_id>
  ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = <client_id>
    * set ReqObjIP $.payload.client_id = <client_id>
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec###############################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.ActualOutput.UIresult.Expected = '400 - Authorization server can not verify the signed request'
    * set Result.ActualOutput.UIresult.Actual = perform
    * match perform.ActualText == "400 - Authorization server can not verify the signed request"
    * def stop = call  webApp.stop()

    Examples:
      | client_id                       |
      | 'abkjichaioycvhoaeyvhoaeihviho' |

########################################################################################################################################
  @RTRE134
  Scenario Outline: Verify the error message when incorrect clientID #key# is passed in request query parameter

    * def key = <client_id>
    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = <client_id>
    And set info.key = <client_id>
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

  ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.client_id = <client_id>
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.ActualOutput.UIresult.Expected = "400 - Unknown or invalid client_id"
    * set Result.ActualOutput.UIresult.Actual = perform
    * set Result.defect = 'query'
    * match perform.ActualText == "400 - Unknown or invalid client_id"
    * def stop = call  webApp.stop()

    Examples:
      | client_id               |
      | 'abcdefgjhkhlkjlfuyfuf' |

