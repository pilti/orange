@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@Cancelled_consent
Feature: Refresh Token Renewal error validation when First Consent is cancelled

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    Given def info = karate.info
    * set info.subset = 'Cancel First Consent'
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

    Given json Application_Details = active_tpp.AISP_PISP
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    And json Result = temp
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
  #############################################################################################################################

  Scenario: Verify refresh token renewal functionality is not launched when TPP cancels First Consent

  * set ReqObjIP $.header.kid = Application_Details.kid
  * set ReqObjIP $.payload.iss = Application_Details.client_id
  * set ReqObjIP $.payload.client_id = Application_Details.client_id
  * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
  * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId

  * set ReqObjIP $.payload.scope = 'openid accounts'
  * set Application_Details $.scope = 'openid accounts'

  * def path = stmtpath(Application_Details) + 'signin_private.key'

############################Call CreateRequestObjec######################################################################
  When call apiApp.CreateRequestObject(Application_Details)
  * set Result.CreateRequestObject.Input = ReqObjIP
  * set Result.CreateRequestObject.Input.SiningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
  * set Result.CreateRequestObject.JWT = apiApp.CreateRequestObject.jwt.request

  * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
  * set Application_Details $.redirect_uri = 'https://boi.com'
  * set Application_Details $.response_type = 'code id_token'
  * set Application_Details $.state = 'af0ifjsldkj'
  * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

  When call apiApp.ConstructAuthReqUrl(Application_Details)
  * set Result.CreateRequestObject.URL = apiApp.ConstructAuthReqUrl.Curl.consenturl
  * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    #added to save the consnet url , & launch again during authorisation renewal
  * print consenturl
  * set Application_Details $.consenturl = consenturl

##########################################################################################################################

  Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
  And def path = call  webApp.path1 = info1.path
  Given def perform = Functions.launchConsentURL(webApp,consenturl)

    #And print perform
  * match perform.Status == 'Pass'

  Then def perform = Functions.useKeyCodeApp(webApp)
    #* print perform

  * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}

  Then def perform = Functions.scaLogin(webApp,data)
    * match perform.Status == "Pass"
    #* print perform
  * def modulename = call  webApp.moduleName1 = 'Click cancel to reject the consent'
  * def click = ReusableFunctions.ClickElement(webApp, ObjectsScreen.cancel_button)
  * set Result.ActualOutput.UI.ClickCancelRenewal = click

  * def modulename = call  webApp.moduleName1 = 'Confirm Cancel'
  * def click = ReusableFunctions.ClickElement(webApp, ObjectsScreen.cancel_request)
  * set Result.ActualOutput.UI.ConfirmCancelRenewal = click

  Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
  Given def perform1 = Functions.launchConsentURL(webApp,consenturl)
    * match perform1.Status == "Fail"
    * match perform1.reasonText == "Landing Page is not as expected!"

    And def perform = Functions.getUrl(webApp)
    And def ExpectedUrl = "https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error"
    And set Result.ActualOutput.UI.ErrorMessage_Expected = ExpectedUrl
    And set Result.ActualOutput.UI.ErrorMessage_Actual = perform.ActualUrl
    And match perform.ActualUrl == ExpectedUrl
    * def stop = call  webApp.stop()

    And set Result.ActualOutput.RefreshTokenRenewal.Input.URL = Application_Details.URL
    And set Result.ActualOutput.RefreshTokenRenewal.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.RefreshTokenRenewal.Input.client_secret = Application_Details.client_secret


