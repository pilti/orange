@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@RTREnits16
Feature:  Account Status,Credit grade validation for the Account associated with Account ID

  Background:
    #####################################################################################################################################################
    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    Given def info = karate.info
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsLogin = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    # call RefreshTokenPrerequisite to perform consent for first time and launch Authorisation Renewal screen
    Given json Application_Details = active_tpp.AISP_PISP
    * set Application_Details.usr = user_details.Generic.G_User_3.user
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * json Result = Prerequisite.Result1
    * print "----------------before calling multi account------------------"
    * set Application_Details.request_method = 'GET'
    * def temp = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Multi_Account_Information.feature') Application_Details
    * json Result = temp.Result1
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
#########################################################################################################################################################
  @severity=normal
  Scenario Outline: Verify refresh renewal screen displays the account which was consented but now changed to #key#
    * set info.Screens = 'Yes'
    * def key = <condition>
    * set info.subset = <condition>
    #Change the credit grade/Status of one of the accounts
    * def AccountRequestId = Application_Details.AccountRequestId
    * def AccountId = temp.response.Data.Account[0].AccountId
    * print AccountId
    * def AccountNo = temp.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
   #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = <AccountStatus>
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    #Launch consent renewal screen
    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    Given def perform = Functions.launchConsentURL(webApp, Application_Details.consenturl)

    #Login
    * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}
    Then def perform = Functions.useKeyCodeApp(webApp)
    Then def perform = Functions.scaLogin(webApp,data)

    * print Application_Details.selectedAccounts
    * def c1 = Functions.VerifyListsOnReAuthorisarionPage(webApp, ObjectsRenewalScreen.selectedAccountNames, Application_Details.selectedAccounts)
    * set Result.UIResult.Expected = 'No. of Accounts during 1st consent = No. of Accounts on Renewal screen'
    * set Result.UIResult.Actual = c1
    * match c1 == true

    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    # take renewal journey Authcode
    * def perform = Functions.getAuthCodeFromURL(webApp)
    And set Application_Details.codeNew = perform.Action.Authcode

    * string a = <ResetAccountStatus>
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    #Call Access_Token_ACG API with new Authcode after renewal
    When set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.code = Application_Details.codeNew
    And call apiApp.Access_Token_ACG(Application_Details)
    * print apiApp.Access_Token_ACG.response

    And set Result.Access_Token_ACG_afterRenew.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.Access_Token_ACG_afterRenew.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token_renew = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token_renew = apiApp.Access_Token_ACG.response.access_token
    * match Result.Access_Token_ACG_afterRenew.responseStatus = 200

    #Call Access_Token_RTG API
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.refresh_token = Application_Details $.refresh_token_renew
    And call apiApp.Access_Token_RTG(Application_Details)
    * print apiApp.Access_Token_RTG.response

    And set Result.Access_Token_RTG_afterRenew.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG_afterRenew.response = apiApp.Access_Token_RTG.response
    And set Application_Details $.refresh_token_final = apiApp.Access_Token_RTG.response.refresh_token
    And set Application_Details $.access_token_final = apiApp.Access_Token_RTG.response.access_token
    * match Result.Access_Token_RTG_afterRenew.responseStatus = 200

    #Call Access_Token_Single account API to check new access token is valid
    When call apiApp.Account_Balance(Application_Details)
     #Validate response and update result json
    And set Result.Account_Balance.responseStatus = apiApp.Account_Balance.responseStatus
    And set Result.Account_Balance.response = apiApp.Account_Balance.response
    And set Result.Account_Balance.responseHeaders = apiApp.Account_Balance.responseHeaders
    #Perform Assertion
    Then match apiApp.Account_Balance.responseStatus == 200

    Examples:
      | AccountStatus               | ResetAccountStatus        | condition        |
      | "CREDIT_GRADING='6'"        | "CREDIT_GRADING='2'"      | 'credit grade 6' |
      | "CREDIT_GRADING='7'"        | "CREDIT_GRADING='2'"      | 'credit grade 6' |
      | "ACCOUNT_STATUS='INACTIVE'" | "ACCOUNT_STATUS='ACTIVE'" | 'Inactive'       |


