@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR07
Feature: This feature is to demonstrate the Error conditions for Invalid combinations of (valid IntentID wrt scope) in Request Object.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
        webApp.stop();
       }
      """

  #################################################################################################################################################################

  @RTRE71
  @severity=normal
  Scenario Outline: Verify Error Msg on UI for Invalid combination of IntentID,CCG and JWT scope as #key#

    ###################################################################################################################################################
    * def info = karate.info
    * def key = '<Role>' + ' , ' + <CCGscope> +' & '+ <JWTscope>
    * set info.subset = 'Invalid IntentId & scope in JWT'
    * set info.key = key

     ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <CCGscope>

     ###############################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

     ##############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = <JWTscope>
    And set Application_Details.scope = <JWTscope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'

      ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

     #####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <AuthReqscope>
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

     ############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == <Expected>
    And print Result

    Examples:
      | Scenario number | Role      | CCGscope                   | JWTscope                   | AuthReqscope      | Expected                                                                                                                                    |
      | '001'           | AISP_PISP | 'openid accounts payments' | 'openid accounts payments' | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error' |
      | '002'           | AISP_PISP | 'openid accounts payments' | 'openid payments'          | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'                                                               |
      | '003'           | AISP      | 'openid accounts'          | 'openid payments'          | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | '004'           | AISP      | 'openid accounts'          | 'accounts'                 | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | '005'           | AISP      | 'openid accounts'          | 'openid'                   | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | '006'           | AISP      | 'openid accounts'          | ''                         | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                       |
      | '007'           | AISP      | 'openid accounts'          | 'null'                     | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | '008'           | AISP      | 'openid accounts'          | 'abcd'                     | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |


#################################################################################################################################################################
  @RTRE72
  @severity=normal
  Scenario Outline: Verify NO Error Message on UI Browser for Invalid combination of IntentID and JWT scope and AuthReq scope is #key#

   ###################################################################################################################################################

    * def key = '<Role>' + ' '+ <JWTscope> +' & '+ <AuthReqscope>
    * def key1 = ' Invalid IntentId & scope in Authorise Request'
    * def info = karate.info
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <CCGscope>

    ###############################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

    ##############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = <JWTscope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

     ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

    #####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <AuthReqscope>
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

    ############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Pass"

# updated after defect for AuthReqscope 1267
    Examples:
      | Scenario number | Role      | CCGscope                   | JWTscope          | AuthReqscope                |
      | '010'           | AISP_PISP | 'openid accounts payments' | 'openid accounts' | 'openid payments'          |
      | '011'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid payments'          |
      | '012'           | AISP      | 'openid accounts'          | 'openid accounts' | 'accounts'                 |
      | '013'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid'                   |
      | '014'           | AISP      | 'openid accounts'          | 'openid accounts' | ''                         |
      | '015'           | AISP      | 'openid accounts'          | 'openid accounts' | 'null'                     |
      | '016'           | AISP      | 'openid accounts'          | 'openid accounts' | 'abcd'                     |