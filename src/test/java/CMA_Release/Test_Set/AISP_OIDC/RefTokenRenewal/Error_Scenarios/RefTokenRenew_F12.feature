@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR12
Feature: This Feature demonstrates the Error Scenarios for Refresh Token Renewal with respect to invalid ClientID

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def Login_obj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
 ##########################################################################################################################################

  @RTRE121
  Scenario Outline: Verify the error message when there is a mismatch of clientID #key#
    Given json Application_Details = <client_id>
    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

    * def key = <client_id> + 'parameter in request object ' + <client_id_1> +  'and request query'
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'CID_Mismatch'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = <client_id_1>
    * set ReqObjIP $.payload.client_id =  <client_id_1>
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.UIresult.Actual = perform.ActualText
    * set Result.UIresult.Expected = "400 - Authorization server can not verify the signed request"
    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Authorization server can not verify the signed request"

   Examples:
      | client_id                      | client_id_1               |
      | active_tpp.AISP_PISP| active_tpp.PISP.client_id |

##########################################################################################################################################
  @RTRE122
  Scenario: Verify the error message when clientID is missing in request query parameter

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'clientID missing in request query parameter'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    Given json Application_Details = active_tpp.AISP_PISP

    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1
   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value =  Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value =  Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

   ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * set Application_Details $.client_id = 'null'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.UIresult = perform.ActualText
    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Unknown or invalid client_id"
##########################################################################################################################################
  @RTRE123
  Scenario: To verify the error message when redirect URL as in request object is different from the registered one

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'redirect URL different from the registered one'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    Given json Application_Details = active_tpp.AISP_PISP
    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1
   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = 'www.google.com'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

   ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.UIresult = perform.Actualtext
    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Invalid redirect_uri"

##########################################################################################################################################
  @RTRE124
  Scenario: Verify the error message when redirect URL as in request object is missing

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'redirect URL is missing in request object'

    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    Given json Application_Details = active_tpp.AISP_PISP
    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1
   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'

    * remove ReqObjIP $.payload.redirect_uri

    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.UIresult = perform.ActualText
    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Invalid redirect_uri"

##########################################################################################################################################
  @RTRE125
  Scenario: Verify the error message when no request object is passed in the url
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'No request object passed in url'

    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    Given json Application_Details = active_tpp.AISP_PISP

    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

 ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = 'null'
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'


    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.ActualOutput.UIErrorExpected = "400 - Signed request is required"
    * set Result.ActualOutput.UIErrorActual = perform.ActualText
    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Signed request is required"



