@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@AISP_F08
@FileRTR08
Feature:  This feature is to demonstrate refresh token renewal functionality is not launched for Revoked Consent.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
        webApp.stop();
       }
      """

  #################################################################################################################################################################

  @severity=normal
  Scenario Outline:  Verify refresh token renewal functionality is not launched for revoked consent for TPP Role #key#

###############################################################################################################################################################

    * def key = '<Role>'
    * def key1 = ' Construct URL with Revoked IntentID'
    * def info = karate.info
    * set info.key = key
    * set info.subset =  'Construct URL'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

     ##########################################submit consent for firdt time######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

 #######################################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)

 #######################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

   ##############################################Call the feature file to Revoke the Account Request id ############################
    And json Application_Details1 = {}
    And set Application_Details1 $.access_token = temp.Result.Access_Token_CCG.response.access_token
    And set Application_Details1 $.token_type = temp.Result.Access_Token_CCG.response.token_type
    And set Application_Details1 $.client_id = Application_Details.client_id
    And set Application_Details1 $.client_secret = Application_Details.client_secret
    And set Application_Details1 $.AccountRequestId = Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details1.AccountRequestId
    And set Application_Details1 $.request_method = 'DELETE'
    And set Application_Details1 $.AcctReqUrl = AcctReqUrl
    And remove reqHeader.x-fapi-customer-ip-address
    And remove reqHeader.x-fapi-customer-last-logged-time
    And remove reqHeader.Content-Type
    And remove reqHeader.Accept
    And def TestRes2 = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Delete.feature') Application_Details1

#############################################Launch URL ####################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == <Expected>

    Examples:
      | Scenario number | Role      | scope             | Expected                                                                                                            |
      | '001'           | AISP_PISP | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |
      | '002'           | AISP      | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |


