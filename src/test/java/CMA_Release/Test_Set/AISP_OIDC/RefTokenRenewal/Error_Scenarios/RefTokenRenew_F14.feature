@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR14

Feature: This feature is to demonstrate negative flow for creating improper consent URL while Refresh Token renewal

  Background:
   # * configure proxy = 'http://webproxy.boigroup.net:8080'
    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def Login_obj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    Given json Application_Details =  active_tpp.AISP
    Then def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
   ############################Set input parameters for CreateRequestObjec################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  ############################################################END OF BACKGROUND###################################################
  @AISP_Error_Tech
  @severity=normal

  Scenario Outline:Verify that error is encountered when there is a mismatch in URI_#key# in the request and the URI provided while creating the application in OB
    Given def info = karate.info
    And def key = <count>
    And set info.key = key
    And set info.Screens = 'Yes'
    And set info.subset = <count>
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    # setting value for uri in jwt payload
    * set ReqObjIP.payload.redirect_uri = <jwt_redirecturi>


    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * print Application_Details $.jwt
    * set Application_Details $.redirect_uri = <Application_Details_redirecturi>
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    Then call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print  consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    * def perform = ReusableFunctions.getElementText(webApp, Login_obj.Error_text2)
    * set Result.ActualOutput.UiResult.ErrorMessageExpected = '400 - Invalid redirect_uri'
    * set Result.ActualOutput.UiResult.ErrorMessageActual = perform.ActualText
    * match perform.ActualText == '400 - Invalid redirect_uri'
    * set Result.ActualOutput.UiResult.ErrorMessageExpected = perform.ActualText

    Examples:
      | count           | jwt_redirecturi                              | Application_Details_redirecturi              |
      | 'Iteration_001' | 'XYZ'                                        | 'https://www.getpostman.com/oauth2/callback' |
      | 'Iteration_002' | 'https://www.getpostman.com/oauth2/callback' | 'XYZ'                                        |