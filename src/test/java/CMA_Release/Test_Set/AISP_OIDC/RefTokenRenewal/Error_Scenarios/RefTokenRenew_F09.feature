@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR09
Feature: This feature is to demonstrate the Error conditions for Missing Intent id in the Request Object.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  #################################################################################################################################################################
  @RTRE91
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from Token Id section of Request Object when value of TPP and scope is #key#

  ###################################################################################################################################################
    * def key = '<Role>' + ' & ' + <scope>
    * def key1 = 'Missing Intent id in JWT'
    * def info = karate.info
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

   ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

   ###############################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

   ##############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId

       #Remove intent id from Token Id section
    And remove ReqObjIP.payload.claims.id_token.openbanking_intent_id
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

   #####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

   ############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    #And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error"

    Examples:
      | Scenario number | Role      | scope             |
      | '001'           | AISP_PISP | 'openid accounts' |
      | '002'           | AISP      | 'openid accounts' |


  ###############################################################################################################################################################
  @RTRE92
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from both user info and Token Id section of Request Object when value of TPP and scope is #key#

  ################################################################################################################################################
    * def key = '<Role>' + ' & ' + <scope>
    * def key1 = 'Missing Intent id both user info and Token Id'
    * def info = karate.info
    * set info.subset = key1
    * set info.key = key
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
  ############################Set TPP Role and scope ######################################################################

    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

  ############################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1

  ############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
   #Remove intent id from user info section
    And remove ReqObjIP.payload.claims.id_token.openbanking_intent_id
   #Remove intent id from Token Id section
    And remove ReqObjIP.payload.claims.userinfo.openbanking_intent_id
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

  ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

  ####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

  ##########################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error"

    Examples:
      | Scenario number | Role      | scope             |
      | '003'           | AISP_PISP | 'openid accounts' |
      | '004'           | AISP      | 'openid accounts' |


  #################################################################################################################################################################
  @RTRE93
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from user info and incorrect in Token Id section of Request Object when value of TPP and scope is #key#

   ##################################################################################################################################################
    * def key = '<Role>' + ' & ' + <scope>
    * def key1 = 'missing from user info and incorrect in Token Id'
    * def info = karate.info
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

   ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

   ###############################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")

   ##############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    #Remove intent id from user info section
    And remove ReqObjIP.payload.claims.userinfo.openbanking_intent_id
    #Set incorrect intent id in Token Id section
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = 'abcdffd'
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

    ####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

     ##########################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    #And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error"


    Examples:
      | Scenario number | Role      | scope             |
      | '005'           | AISP_PISP | 'openid accounts' |
      | '006'           | AISP      | 'openid accounts' |



 ###############################################################################################################################################################
  @RTRE94
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is incorrect in user info and Missing from Token Id section of Request Object when value of TPP and scope is #key#

   ##############################################################################################################################################################
    * def key = '<Role>' + ' & ' + <scope>
    * def key1 = 'Intent Id is incorrect in user info and Missing from Token Id'
    * def info = karate.info
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

  #################################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

   #################################Call the feature file to create Account Request id ######################################################################
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")

   ################################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
     #Set incorrect user info in Token Id section
    And set ReqObjIP.payload.claims.userinfo.openbanking_intent_id.value = 'adaadad'
      #Remove intent id from token id section
    And remove ReqObjIP $.payload.claims.id_token.openbanking_intent_id
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

   ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

   #######################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

   ##############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    #And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def stop = call  webApp.stop()
    And match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error"

    Examples:
      | Scenario number | Role      | scope             |
      | '007'           | AISP_PISP | 'openid accounts' |
      | '008'           | AISP      | 'openid accounts' |