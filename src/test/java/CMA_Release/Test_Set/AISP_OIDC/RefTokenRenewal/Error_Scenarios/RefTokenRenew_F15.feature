@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR15
Feature: Parameter validation in request object creation for Refresh Token renewal

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    Given json Application_Details = active_tpp.AISP_PISP
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature") Application_Details
    And json Result = temp.Result1
    * configure afterScenario =
       """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

############################################################################################################################################
  @severity=normal
  @RTR150
  Scenario: Verify No Error msg is displayed on UI when diff account request IDs passed in two fields of openbaking_intent_id for request object creation

    Given def info = karate.info
    And set info.subset = 'Diff account request IDs in Request object'
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
   #Input parameter values to create request object
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * print ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value

   #Test condition: Create new account request ID which will be passed under id_token claim
    * def AccountReqId = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = AccountReqId.apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * print ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    When call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And set Result.ActualOutput.UIResult = perform.Landing_Page
    And match perform.Status == 'Pass'

    #userifo_intentID is optional parameter in JWT, however id_token_intentID if passe of same TPP then no error.

#    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
#    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.Error_text2)
#    * print ActualText
#    And set Result.UIResult = ActualText
#    Then match ActualText == ExpectedUIErrorMessage

############################################################################################################################################

  @severity=normal
  @RTR151
  Scenario: Verify Error msg is displayed on UI when client_id not associated with requesting TPP in request object creation

    * def info = karate.info
    * set info.subset = 'Client_Id not associated with requesting TPP'
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
     # Client_Id associated with different TPP will be passed in payload
    And set ReqObjIP $.payload.client_id = active_tpp.AISP.client_id

    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And def path = call  webApp.path1 = info1.path
    And set Result.ActualOutput.UIResult.Expected = '400 - Authorization server can not verify the signed request'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And set Result.ActualOutput.UIResult.ActualText = ActualText
    Then match ActualText == Result.ActualOutput.UIResult.Expected

############################################################################################################################################

  @severity=normal
  @RTR152
  Scenario: Verify error is encountered on launching consent when request object is created using a network key from diff TPP than one in client_id

    * def info = karate.info
    * set info.subset = 'Req object with network key from diff TPP'
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId

     # Use network key from different TPP while creating request object
    And def Application_Details_dummy = active_tpp.AISP
    And def path = stmtpath(Application_Details_dummy) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)

    * print Application_Details

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And def path = call  webApp.path1 = info1.path
    And set Result.ActualOutput.UIResult.Expected = '400 - Authorization server can not verify the signed request'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And set Result.ActualOutput.UIResult.ActualText = ActualText
    Then match ActualText == Result.ActualOutput.UIResult.Expected
############################################################################################################################################

  @severity=normal
  @RTR153
  Scenario Outline: Verify Error msg is displayed on UI while launching consent when scope is modified to #key#

    * def key = <scope>
    * def info = karate.info
    * set info.subset = 'scope is modified'
    * set info.Screens = 'Yes'
    * set info.key = <scope>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
   #Modify scope as openid while constructing consentURL
    And set Application_Details $.scope = <scope>

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And def path = call  webApp.path1 = info1.path
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UIResult = perform.Landing_Page

#    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
#    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
#    And set Result.UIResult = ActualText
#    Then match ActualText == ExpectedUIErrorMessage
#
    #'Scope' in query Parameter is not mandatory, although in Req Object it is.

    Examples:
      | scope                 |
      | 'openid'              |
      | 'abcdef'              |
      | active_tpp.PISP.scope |
      | '  '                  |

  @RTR154nits
  Scenario Outline: Verify Error msg on UI when id_token_intent_id of diff TPP #key# is passed in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'id_token in intent_id of Diff TPP'
    And set info.key = '<Role>'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
 #Input parameter values to create request object
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * print '#############################################'
    * print Application_Details.AccountRequestId

 #Test condition: Create new account request ID which will be passed under id_token claim
    * def Application_Details1 = active_tpp.<Role>
    * def New_AccountReqId = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details1
    * def id_Token = New_AccountReqId.apiApp.Account_Request_Setup.response.Data.AccountRequestId

    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = id_Token
    * print '#############################################'
    * print id_Token
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    When call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And set Result.ActualOutput.UIResult = perform.Landing_Page
    And match perform.Status == 'Fail'

    And set Result.ActualOutput.ExpectedError = '400 - Authorization server can not verify the signed request'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.Error_text2)
    * print ActualText
    And set Result.ActualOutput.ActualError = ActualText
    Then match ActualText == '400 - Authorization server can not verify the signed request'

    Examples:
      | Role |
      | AISP |

  @RTR155
  Scenario Outline: Verify Error msg on UI when invalid idtoken in intent_id as #key# is passed in request object

    Given def info = karate.info
    And set info.subset = 'id_token in intent_id garbage value'
    And set info.key = <value>
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
#Input parameter values to create request object
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * print ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value

#Test condition: Create new account request ID which will be passed under id_token claim

    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = <value>
    * print ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    When call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP
    And set Result.RequestObjectInput = ReqObjIP
    And set Result.ConsentURL = consenturl

    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And set Result.ActualOutput.UIResult = perform.Landing_Page
    And match perform.Status == 'Fail'

    And set Result.ActualOutput.ExpectedError = <Expected>
    And def perform = Functions.getUrl(webApp)
    And set Result.ActualOutput.ActualError = perform.ActualUrl
    Then match perform.ActualUrl == <Expected>

    Examples:
      | value   | Expected                                                                                                            |
      | ''      | 'https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error'    |
      | 'abcde' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |


















