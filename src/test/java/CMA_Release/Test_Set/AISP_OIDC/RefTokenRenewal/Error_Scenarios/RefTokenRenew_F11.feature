@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR11
Feature: This Feature demonstrates the Error Scenarios for RefreshToken Renewal with combination of scope & incorrect ReqObj

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

  ####################################Pre-requisite###############################################
    Given json Application_Details = active_tpp.AISP_PISP
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = temp.Result1
  #################################################################################################

  @RTRE111
  Scenario: Verify the error message when malformed request object is passed in the url

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'Request Object malformed'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = (apiApp.CreateRequestObject.jwt.request).replace(".","#")
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    * def modulename = call  webApp.moduleName1 = 'Launch Consent Url again'
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == "Fail"
    * match perform.reasonText == "Landing Page is not as expected!"

    * def perform = Functions.validateWebElementText(webApp)
    * set Result.ActualOutput.UI.ErrorMessage_Actual = perform.ActualText
    * set Result.ActualOutput.UI.ErrorMessage_Expected = "400 - Authorization server can not verify the signed request"

    * def stop = call  webApp.stop()
    * match perform.ActualText == "400 - Authorization server can not verify the signed request"

    And set Result.ActualOutput.RefreshTokenRenewal.Input.URL = Application_Details.URL
    And set Result.ActualOutput.RefreshTokenRenewal.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.RefreshTokenRenewal.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.RefreshTokenRenewal.Input.jwt = Application_Details.jwt
  #################################################################################################
  @RTRE112
  Scenario: Verify the error message when scope is missing in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'Missing scope in Request object'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    #remove scope
    * remove ReqObjIP $.payload.scope
    * set ReqObjIP $.payload.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    * def modulename = call  webApp.moduleName1 = 'Launch Consent Url again'
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == "Fail"
    * match perform.reasonText == "Landing Page is not as expected!"

    Then def perform = Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.ErrorMessage_Expected = "https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope"
    And set Result.ActualOutput.UI.ErrorMessage_Actual = perform.ActualUrl
    * def stop = call  webApp.stop()
    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'
  #"https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope"

 ######################################################################################################
  @RTRE113
  Scenario: Verify the error message when scope is incorrect in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'Invalid Scope in Request object'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
   ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    #invalid value of scope
    * set ReqObjIP $.payload.scope = 'openid accountss'
    * set ReqObjIP $.payload.redirect_uri =  'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

  ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    * def modulename = call  webApp.moduleName1 = 'Launch Consent Url again'
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == "Fail"
    * match perform.reasonText == "Landing Page is not as expected!"

    Then def perform = Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.ErrorMessage_Expected = "https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope"
    And set Result.ActualOutput.UI.ErrorMessage_Actual = perform.ActualUrl
    * def stop = call  webApp.stop()
    * match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope"

  #################################################################################################

  @RTRE114
  Scenario: Verify the error message when requested scope has a mismatch

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'scope mismatch in JWT & Query Parameter'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
  ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts payments'
    * set ReqObjIP $.payload.redirect_uri =  'https://www.getpostman.com/oauth2/callback'
   # scope diff in query parameter
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * set Result.CreateRequestObject.JWT_Input = ReqObjIP
    * set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    * def modulename = call  webApp.moduleName1 = 'Launch Consent Url again'
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == "Fail"
    * match perform.reasonText == "Landing Page is not as expected!"

    Then def perform = Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.ErrorMessage_Expected = "https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope"
    And set Result.ActualOutput.UI.ErrorMessage_Actual = perform.ActualUrl
    * def stop = call  webApp.stop()
    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'