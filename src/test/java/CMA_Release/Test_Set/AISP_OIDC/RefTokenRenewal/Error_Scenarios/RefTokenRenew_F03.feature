@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR03
Feature: Verify refresh token renewal is not launched when consent is expired/revoked

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
################################################Background end#####################
  @RTRE4
  Scenario: Verify refresh token renewal functionality is not launched for expired consent
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'refresh token renewal functionality is not launched for a EXPIRED consent'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(300)
    * remove permissions $.Data.TransactionFromDateTime
    * remove permissions $.Data.TransactionToDateTime
    * print permissions
    * def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * json Result = Prerequisite.Result1
    * print "waiting 5 minutes for consent to expire"
    * def waitExpire = ReusableFunctions.sleep(300000);

     #launch consent URl again
    Given json Application_Details = active_tpp.AISP_PISP
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    And print Application_Details.consenturl
    When def modulename = call  webApp.moduleName1 = 'Launch Consent Url again'
    And def perform = Functions.launchConsentURL(webApp, Application_Details.consenturl)
 #    * set perform = Functions.validateWebElementText(webApp)
 #    * match perform.ActualText == 'Unable to process your request. Please try again later.'
    And def error = Functions.getUrl(webApp)
    And set Result.ActualOutput.UI.ActualError = error.ActualUrl
    And set Result.ActualOutput.UI.ExpectedError = 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'
    Then match error.ActualUrl == Result.ActualOutput.UI.ExpectedError
####################################################################################################################


