@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@RTREIntentId
@FileRTR06
Feature: This feature is to demonstrate the Error condition when requested IntentID is incorrect in the Request Object wrt ClientID.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  #################################################################################################################################################################
  @RTRE61
  @severity=normal
  Scenario Outline: Verify Error Msg is displayed on UI when Client Id in Client Credential Grant & Request Object is for diff TPP i.e #key#
      ###################################################################################################################################################

    * def key = '<CCGRole>'+' & '+'<JWTRole>'
    * def key1 = 'Client Id provided is for different TPP in JWT'
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

       ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<CCGRole>
    And set Application_Details.usr = user_deatils.Generic.G_User_1.user
    And set Application_Details $.scope = <scope>

       ###############################Call the feature file to create Account Request id ######################################################################
    And def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And def Result =  TestRes.Result1

       ##############################Set input parameters for CreateRequestObject######################################################################
    And json Application_Details1 = active_tpp.<JWTRole>
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details1.client_id
    And set ReqObjIP $.payload.client_id = Application_Details1.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = <scope>
    And set Application_Details $.scope = <scope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

        ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

       #####################################Set input parameters for CreateConsentURL ######################################################################
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.client_id = Application_Details1.client_id
    And set Application_Details $.secret = Application_Details1.client_secret
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

       ############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And set perform.temp = Functions.validateWebElementErrorText(webApp)
    And match perform.temp.ActualText2 == <Expected>

    Examples:
      | Scenario number | CCGRole   | scope             | JWTRole   | Expected                                                       |
      | '001'           | AISP_PISP | 'openid accounts' | PISP      | '400 - Authorization server can not verify the signed request' |
      | '002'           | AISP      | 'openid accounts' | AISP_PISP | '400 - Authorization server can not verify the signed request' |
      | '003'           | AISP      | 'openid accounts' | PISP      | '400 - Authorization server can not verify the signed request' |

 #################################################################################################################################################################

  @RTRE62
  @severity=normal
  Scenario Outline: Verify Error Msg is displayed on UI when Client Id in Client Credential Grant and Authorise Request is for diff TPP as #key#

      ###################################################################################################################################################

    * def key = '<CCGRole>'+' & '+'<AuthReqRole>'
    * def key1 = 'Client Id provided is for different TPP in Request Query Parameter'
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

       ##############################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.<CCGRole>
    And set Application_Details $.scope = <scope>

       ###############################Call the feature file to create Account Request id ######################################################################
    And def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    And json Result = TestRes.Result1

       ##############################Set input parameters for CreateRequestObject######################################################################
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = <scope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

        ############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)

       #####################################Set input parameters for CreateConsentURL ######################################################################
    And json Application_Details1 = active_tpp.<AuthReqRole>
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.client_id = Application_Details1.client_id
    And set Application_Details $.secret = Application_Details1.client_secret
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

       ############################################################################################################################
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And set perform.temp = Functions.validateWebElementErrorText(webApp)
    And match perform.temp.ActualText2 == <Expected>

    # open defect for AuthReqRole
    Examples:
      | Scenario number | CCGRole   | scope             | AuthReqRole | Expected                             |
      | '004'           | AISP_PISP | 'openid accounts' | PISP        | '400 - Unknown or invalid client_id' |
      | '005'           | AISP      | 'openid accounts' | AISP_PISP   | '400 - Unknown or invalid client_id' |
      | '006'           | AISP      | 'openid accounts' | PISP        | '400 - Unknown or invalid client_id' |