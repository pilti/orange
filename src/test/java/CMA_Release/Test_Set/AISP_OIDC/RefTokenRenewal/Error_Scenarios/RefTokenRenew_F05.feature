@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@RTRResTypErr
@FileRTR05
Feature: response type Parameter validation in request object for refresh token renewal

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Application_Details = active_tpp.AISP_PISP
    * set Application_Details.usr = user_deatils.Generic.G_User_1.user
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    * def Result = temp.Result1
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @RTRE51
  @severity=normal
  Scenario Outline: Verify Error message is displayed on UI for response type as #key# in request object
    * def info = karate.info
    * def key = <response_type>
    * set info.Screens = 'Yes'
    * set info.key = <response_type>
    * set info.subset = 'response_type error validation in request object'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    * print info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'

    # Input invalid or empty response_type while request object creation
    And set ReqObjIP $.payload.response_type = <response_type>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And print ReqObjIP
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And def path = call  webApp.path1 = info1.path
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And match ActualText == ExpectedUIErrorMessage

    Examples:
      | response_type |
      | 'code'        |
      | 'id_token'    |
      | ' '           |


  @severity=normal
  @RTRE52
  Scenario Outline: Verify Error message is displayed on UI for response type as #key# in Query parameter
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <response_type>
    * set info.subset = 'response_type validation in query parameter'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And print ReqObjIP
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    # Input invalid or empty response_type while constructing consent URL
    And set Application_Details $.response_type = <response_type>
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And def path = call  webApp.path1 = info1.path
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def RT = "<response_type>"
    And def ExpectedUIErrorMessage = '400 - response_type ' + RT + ' is not allowed for client: ' + Application_Details.client_id
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And match ActualText == ExpectedUIErrorMessage

    Examples:
      | response_type |
      | 'code'        |
      | 'id_token'    |
      | ' '           |

  @severity=normal
  @RTRE053
  Scenario: Verify Error message is displayed on UI when response type is missing in request object

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'response_type is missing in RequestObject'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'

     #Remove response_type parameter while creating object request
    And remove ReqObjIP.payload.response_type
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And print ReqObjIP
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"
    And def perform = Functions.getUrl(webApp)
    And def ExpectedUrl = 'https://app.getpostman.com/oauth2/callback?error_description=response_type+is+required.&state=af0ifjsldkj&error=invalid_request#.'
    And match ExpectedUrl == perform

  @severity=normal
  @RTRE054
  @defect1336
  Scenario: Verify  Error message is displayed on UI when requested response type is missing in query parameter

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Missing response type in Query Parameter'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = Application_Details.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    # Remove response_type from query parameter
    And set Application_Details $.response_type = 'null'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = info1.path
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == "Fail"
    And match perform.reasonText == "Landing Page is not as expected!"

    #And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later'
    And def ExpectedUIErrorMessage = '401 - UnAuthorized'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And match ActualText == ExpectedUIErrorMessage