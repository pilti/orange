@AISP
@AISP_Consent
@AISP_RefreshTokenRenewal
@AISP_Error_Bus
@FileRTR02
Feature: Verify refresh token renewal is not launched for a PISP consent

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
        webApp.stop();
       apiApp.write(Result,info);
       }
    """
  @RTRE3
Scenario Outline: Verify that the refresh token renewal functionality is not launched for a PISP consent When #key#
* def info = karate.info
* def key = 'TPP Role is '+'<Role>'
* set info.key = key
* set info.Screens = 'Yes'
* set info.subset = 'Yes'
* def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
* def path = call  webApp.path1 = info1.path
*  json Application_Details = active_tpp.<Role>
* def paymentId = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/payment_Setup.feature')
* def consent_Url = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/createConsentUrl.feature') paymentId.Result
*  def strConsentUrl = consent_Url.Result.createConsentUrl.ConsentUrl
*  def consentData = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes","url":'#(strConsentUrl)'}
* def performConsent = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/performConsent.feature') consentData

  #Call Access_Token_ACG API

  Given set Application_Details $.grant_type = "authorization_code"
  And set Application_Details $.request_method = 'POST'
  And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200
  And set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
  And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
  And def path = call  webApp.path1 = info1.path
  Given def perform = Functions.launchConsentURL(webApp, strConsentUrl)
 # check error on launch of url
  And def perform = Functions.getUrl(webApp)
  And def ExpectedUrl = "https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error"
  And set Result.ActualOutput.UI.ExpectedUrl = ExpectedUrl
  When set Result.ActualOutput.UI.Actual = perform.ActualUrl
  Then match perform.ActualUrl == ExpectedUrl

 Examples:
 | Role|
 |AISP_PISP|
 |PISP|
