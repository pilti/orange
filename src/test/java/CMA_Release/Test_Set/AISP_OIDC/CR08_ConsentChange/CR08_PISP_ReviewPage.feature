@ignore
Feature: CR08 - Consent UI - TPP Details

  Background:
    * def b_fol = ""
    * def api_grp = "AISP"
    * def api_name = ""
    * def typ = "CR08_ConsentUI"
    * def feature = "CR08_AISP"

  @PISPCR08
Scenario: To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP legal entity name should be displayed on Review and Confirm Page
          Condition: Customer has only single valid account for consent authorisation
  * def user = "682833"
  * def otp = "123456"
  * def browser = 'Chrome'
  * def output_file = "TC01_CR08_AISP_ReviewPage"
  * def Test_res = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/Payment_Setup.feature')
  * def profile = {"idempotencyKey":'#(Test_res.idempotency_key.idempotency_key)',"PaymentId":'#(Test_res.response.Data.PaymentId)'}

  * def Temp = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/PISP_ConstructURL.feature') profile
  * def ConsentURL = Temp.PaymentConsentUrl.PaymentConsentUrl
  * print ConsentURL
  * def result = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Test_List').getTPPName_CR08_ReviewPage(ConsentURL,user,otp,browser,output_file)

  * assert result == tpp_credentials.application_name
  * assert result.length() <= 50
  * def Headding = {"Feature": 'CR08 - Consent UI - TPP Details',"Scenario_Description":'To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP lega entity name should be displayed on Review and Confirm Page',"Condition": Customer has only single valid account for consent authorisation}
  * def output = {"TestDescription":'#(Headding)',"ConsentURL": '#(ConsentURL)', "Output": '#(result)'}
  * set Headding $.TestStatus = 'Pass'
  * def result1 = Java.type('CMA_Release.Java_Lib.TextFileWriting').testout(karate.pretty(output),b_fol,api_grp,api_name,typ,feature,output_file)


#######################################################################################################################################################################################################################################################################################################################################################################################################################################
#################################################Scenario 2##############################################################################################################################################################################################################################################################################################################################################################
  @PISPCR08
  @ignore
  Scenario: To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP legal entity name should be displayed on Review and Confirm Page
  Condition: TPP Application name is set to maximum limit allowed

    * def user = user_details.user
    * def otp = user_details.otp
    * def browser = 'Chrome'
    * def output_file = "TC02_CR08_AISP_ReviewPage"
    * def Test_res = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/Payment_Setup.feature')
    * def profile = {"idempotencyKey":'#(Test_res.idempotency_key.idempotency_key)',"PaymentId":'#(Test_res.response.Data.PaymentId)'}

    * def Temp = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/PISP_ConstructURL.feature') profile
    * def ConsentURL = Temp.PaymentConsentUrl.PaymentConsentUrl
    * print ConsentURL
    * def result = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Test_List').getTPPName_CR08_ReviewPage(ConsentURL,user,otp,browser,output_file)

    * def Headding = {"Feature": 'CR08 - Consent UI - TPP Details',"Scenario_Description":'To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP lega entity name should be displayed on Review and Confirm Page', "Condition": Customer has multiple valid accounts for consent authorisation}
    * def output = {"TestDescription":'#(Headding)',"ConsentURL": '#(ConsentURL)', "Output": '#(result)'}
    * set Headding $.TestStatus = 'Fail'
    When assert result == tpp_credentials.application_name
    And assert result.length() <= 50
    Then set Headding $.TestStatus = 'Pass'

    * def result1 = Java.type('CMA_Release.Java_Lib.TextFileWriting').testout(karate.pretty(output),b_fol,api_grp,api_name,typ,feature,output_file)

#######################################################################################################################################################################################################################################################################################################################################################################################################################################
#################################################Scenario 3##############################################################################################################################################################################################################################################################################################################################################################
  @ignore
  Scenario: To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP legal entity name should be displayed on Review and Confirm Page
            Condition: Customer has multiple valid accounts for consent authorisation

    * def user = user_details.user
    * def otp = user_details.otp
    * def browser = 'Chrome'
    * def output_file = "TC03_CR08_AISP_ReviewPage"
    * def Test_res = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/Payment_Setup.feature')
    * def profile = {"idempotencyKey":'#(Test_res.idempotency_key.idempotency_key)',"PaymentId":'#(Test_res.response.Data.PaymentId)'}

    * def Temp = callonce read('classpath:CMA_Release/Entities_API/PISP_Resources/PISP_ConstructURL.feature') profile
    * def ConsentURL = Temp.PaymentConsentUrl.PaymentConsentUrl
    * print ConsentURL
    * def result = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Test_List').getTPPName_CR08_ReviewPage(ConsentURL,user,otp,browser,output_file)

    * def Headding = {"Feature": 'CR08 - Consent UI - TPP Details',"Scenario_Description":'To verify that the user is notified about the third party that has requested access to accounts on Review and Confirm page and TPP application name instead of TPP lega entity name should be displayed on Review and Confirm Page', "Condition": Customer has multiple valid accounts for consent authorisation}
    * def output = {"TestDescription":'#(Headding)',"ConsentURL": '#(ConsentURL)', "Output": '#(result)'}
    * set Headding $.TestStatus = 'Fail'
    When assert result == tpp_credentials.application_name
    And assert result.length() <= 50
    Then set Headding $.TestStatus = 'Pass'

    * def result1 = Java.type('CMA_Release.Java_Lib.TextFileWriting').testout(karate.pretty(output),b_fol,api_grp,api_name,typ,feature,output_file)
