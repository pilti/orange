@ignore
Feature:  Create Consents with different Transactions Date Time combinations

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def fileName = 'CR20_TestData.properties'
    * def path = './src/test/java/CMA_Release/Test_Data_' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    #* def Application_Details =
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')

#################################################################################################################################
  @CR20InProgress12
  @severity=normal
  Scenario Outline: Test #key# To verify that transactions are returned according to the Transactions Period specified in consent and API request
    * def key = <TCNO>
    * set Result.condition = <condition>
    * json inputJSON = RWPropertyFile.ReadFile(path, fileName, "<Consent>")
    * call apiApp.configureSSL(inputJSON)
    And set inputJSON.request_method = "POST"
    * def subset = "ProductType"
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
      ##And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)

    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then def profile = { accountNumber: '#(apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification)'}
    And def txnx = call read('classpath:CMA_Release/Test_Set/AISP_OIDC/Change_Requests/CR20_Transactions/CR20_addTxns.feature') profile

    * match apiApp.Multi_Account_Information.responseStatus == 200

    * def FromBookingDate = <FromDate>
    * print "Frombooking date is"
    * print FromBookingDate
    * def ToBookingDate = <ToDate>
    * print "ToBooking date is"
    * print ToBookingDate
    * def Today = DateFormat.getFormattedDate(0,0,0,0,0,0)
    * def LastOneYear = DateFormat.getFormattedDate(-1,0,1,0,0,0)
     #* set inputJSON $.FromDate = frmDate.substring()
    * def TransactionFromDate = inputJSON.From
    * def TransactionToDate = inputJSON.To
    * print "From is"
    * print TransactionFromDate
    * print "To is:"
    * print TransactionToDate
    * print "Today is"
    * print Today

    * set Result.TransactionFromDate = TransactionFromDate
    * set Result.TransactionToDate = TransactionToDate
    * set Result.FromBookingDate = FromBookingDate
    * set Result.ToBookingDate = ToBookingDate
    * def FromDate = FromBookingDate
    * def ToDate = ToBookingDate
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")

    Given url AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And param fromBookingDateTime = FromBookingDate
    And param toBookingDateTime = ToBookingDate
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And header x-fapi-interaction-id = Random_id.random_id()
    And headers reqHeader

    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def TransactionsResult = response
      #changes for commit directory
    * set Result.response = TransactionsResult
     # * def FromOpDate = /<FromOp>
     # * def ToOpDate = /<ToOp>

    * def FromOpDate = <FromOp>
    * def ToOpDate = <ToOp>


    * def compareDates =
    """
    function(bDate,fromD, toD){

        var from = fromD;
        var to = toD;
        var fromdate1 = Date.UTC(from.substring(0,4),from.substring(5,7),from.substring(8,10));
        var todate1 = Date.UTC(to.substring(0,4),to.substring(5,7),to.substring(8,10));
        for (var i=0; i<bDate.length; i++)
        {
          var bd = bDate[i].BookingDateTime;
          var bookingdate = Date.UTC(bd.substring(0,4),bd.substring(5,7),bd.substring(8,10));
          if(bookingdate >= fromdate1 && bookingdate <= todate1)
            return "pass";
          else
          {
            return "fail";
            break;
          }
        }
    }
    """

    * set Result $.TestStatus = compareDates(TransactionsResult.Data.Transaction,FromOpDate,ToOpDate)
    * print Result.TestStatus
    * assert Result.TestStatus == "pass"

    Examples:
      |TCNO| Consent   | FromDate | ToDate | FromOp        | ToOp |condition |
      | 1 | Consent01 | '' | '' | LastOneYear | Today | 'When TransactionFromDateTime is Not Specified , TransactionToDateTime is Not Specified, FromBookingDateTime is Not specified, toBookingDateTime is Not specified then As a result Return transactions between LastOneYear and Today' |
      | 2 | Consent01 | '' | '' | LastOneYear | Today | 'When TransactionFromDateTime is Not Specified , TransactionToDateTime is greater than LastOneYear and less than Today, FromBookingDateTime is Not specified, toBookingDateTime is Not specified then As a result Return transactions between LastOneYear and TransactionToDate' |

     # | 1 | Consent08 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
     # | 2 | Consent08 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and To' |
     # | 3 | Consent08 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
     # | 4 | Consent08 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and To' |
     # | 5 | Consent09 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
     # | 6 | Consent09 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
    #  | 7 | Consent09 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between From and ToDate' |
    #  | 8 | Consent09 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and T' |
    #  | 9 | Consent09 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
     # | 10 | Consent09 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 11 | Consent09 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and T' |
    #  | 12 | Consent09 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | T | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and T' |
    #  | 13 | Consent10 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 14 | Consent10 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
   #   | 15 | Consent10 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between From and ToDate' |
    #  | 16 | Consent10 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
    #  | 17 | Consent10 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
    #  | 18 | Consent10 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between From and ToDate' |
     # | 19 | Consent10 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
    #  | 20 | Consent10 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is LastOneYear /< From /< T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
   #   | 21 | Consent12 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
    #  | 22 | Consent12 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
   #   | 23 | Consent12 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
  #    | 24 | Consent12 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
   #   | 25 | Consent12 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
    #  | 26 | Consent12 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
   #   | 27 | Consent12 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
    #  | 28 | Consent12 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
    #  | 29 | Consent13 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
    #  | 30 | Consent13 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
    #  | 31 | Consent13 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
   #   | 32 | Consent13 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
   #   | 33 | Consent13 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
   #   | 34 | Consent13 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
   #   | 35 | Consent13 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
  #   | 36 | Consent13 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= T , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
    #  | 37 | Consent17 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 38 | Consent17 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and To' |
    #  | 39 | Consent17 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between LastOneYear and To ' |
    #  | 40 | Consent17 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between LastOneYear and To ' |
   #   | 41 | Consent17 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 42 | Consent17 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 43 | Consent17 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and To' |
    #  | 44 | Consent17 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 45 | Consent18 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 46 | Consent18 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and T' |
     # | 47 | Consent18 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
     # | 48 | Consent18 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
     # | 49 | Consent18 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
    #  | 50 | Consent18 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between LastOneYear and ToDate' |
     # | 51 | Consent18 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between LastOneYear and T' |
    #  | 52 | Consent18 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between LastOneYear and T' |
     # | 53 | Consent18 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 54 | Consent18 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between LastOneYear and ToDate' |
    #  | 55 | Consent18 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between LastOneYear and T' |
    #  | 56 | Consent18 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | T | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between LastOneYear and T' |
    #  | 57 | Consent18 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 58 | Consent19 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
    #  | 59 | Consent19 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 60 | Consent19 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
    #  | 61 | Consent19 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
     # | 62 | Consent19 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
     # | 63 | Consent19 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result return transactions between LastOneYear and ToDate' |
     #| 64 | Consent19 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between LastOneYear and To' |
   ##   | 65 | Consent19 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result return transactions between LastOneYear and ToDate' |
    #  | 66 | Consent19 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 67 | Consent19 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 68 | Consent19 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 69 | Consent19 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
    #  | 70 | Consent19 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
    #  | 71 | Consent21 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions for day of LastOneYear' |
    #  | 72 | Consent21 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for day of LastOneYear' |
     # | 73 | Consent21 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions for day of LastOneYear' |
     # | 74 | Consent21 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(-1,0,1,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 75 | Consent21 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,-1,-1,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions for day of LastOneYear' |
     # | 76 | Consent21 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for day of LastOneYear' |
     # | 77 | Consent21 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /< LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions for day of LastOneYear' |
    #  | 78 | Consent23 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
     # | 79 | Consent23 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and To' |
     # | 80 | Consent23 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between LastOneYear and To' |
     # | 81 | Consent23 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | LastOneYear | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between LastOneYear and To' |
    #  | 82 | Consent23 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | LastOneYear | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 83 | Consent23 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 84 | Consent23 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and To' |
    #  | 85 | Consent23 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is LastOneYear /< To /< T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 86 | Consent24 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 87 | Consent24 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and T' |
     # | 88 | Consent24 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
      #| 89 | Consent24 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions for Today' |
     # | 90 | Consent24 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
     # | 91 | Consent24 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between From and ToDate' |
     # | 92 | Consent24 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | T | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and T' |
    #  | 93 | Consent24 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and ToDate' |
     # | 94 | Consent24 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 95 | Consent24 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 96 | Consent24 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | T | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and T' |
     # | 97 | Consent24 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
     # | 98 | Consent24 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /> T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 99 | Consent25 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 100 | Consent25 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
    #  | 101 | Consent25 | DateFormat.getBookingDate(0,-1,-3,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is LastOneYear /< FromDate /< T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
    #  | 102 | Consent25 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
     # | 103 | Consent25 | DateFormat.getBookingDate(0,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= T, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and To' |
    #  | 104 | Consent25 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between From and toDate' |
     # | 105 | Consent25 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between From and To' |
    #  | 106 | Consent25 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between From and toDate' |
    #  | 107 | Consent25 | DateFormat.getBookingDate(-1,0,-2,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 108 | Consent25 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions between FromDate and ToDate' |
     # | 109 | Consent25 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions between FromDate and To' |
     # | 120 | Consent25 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions between FromDate and ToDate' |
     # | 121 | Consent25 | DateFormat.getBookingDate(-1,0,0,0,0,0) | DateFormat.getBookingDate(-1,0,0,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= T and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 122 | Consent26 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions for day of LastOneYear' |
     # | 123 | Consent26 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | From | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for day of LastOneYear' |
     # | 124 | Consent26 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | From | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions for day of LastOneYear' |
      #| 125 | Consent26 | DateFormat.getBookingDate(-1,0,-1,0,0,0) | DateFormat.getBookingDate(-1,0,1,0,0,0) | From | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /< LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions for day of LastOneYear' |
     # | 126 | Consent26 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,-1,-2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is LastOneYear /< ToDate /< T then As a result Return transactions for day of LastOneYear' |
     # | 127 | Consent26 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,0,2,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /> T then As a result Return transactions for day of LastOneYear' |
     # | 128 | Consent26 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(0,0,0,0,0,0) | FromDate | To | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= T then As a result Return transactions for day of LastOneYear' |
     # | 129 | Consent26 | DateFormat.getBookingDate(-1,0,1,0,0,0) | DateFormat.getBookingDate(-1,0,1,0,0,0) | FromDate | ToDate | 'When TransactionFromDateTime is From /= LastOneYear , TransactionToDateTime is To /= LastOneYear and FromBookingDateTime is FromDate /= LastOneYear, toBookingDateTime is ToDate /= LastOneYear then As a result Return transactions between FromDate and ToDate' |
