
#
Feature:  To verify transactions API returns Empty response when BookingDates specified does not fit in consented transaction Dates criteria

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def fileName = 'CR20_TestData.properties'
    * def path = './src/test/java/CMA_Release/Test_Data_' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def Application_Details = active_tpp.AISP_PISP
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')

#################################################################################################################################


  @BookingDatecheck12345
  @severity=normal
  Scenario Outline: Test #key# To verify that transactions API returns Empty response when BookingDates specified does not fit in consented transaction Dates criteria

    * def key = <TCNO>
    * set Result.condition = '<condition>'
    * json inputJSON = RWPropertyFile.ReadFile(path, fileName, "<Consent>")
    * call apiApp.configureSSL(inputJSON)
    And set inputJSON.request_method = "POST"
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
      ##And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
       # * remove inputJSON $.refresh_token
      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)

    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then def profile = { accountNumber: '#(apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification)'}
    And def txnx = call read('classpath:CMA_Release/Test_Set/AISP_OIDC/Change_Requests/CR20_Transactions/CR20_addTxns.feature') profile

    * match apiApp.Multi_Account_Information.responseStatus == 200

    * def frmDate = <FromDate>
    * print "Frombooking date is"
    * print frmDate
    * def toDate = <ToDate>
    * print "ToBooking date is"
    * print toDate
    * def Today = DateFormat.getFormattedDate(0,0,0,0,0,0)
    * def LastOneYear = DateFormat.getFormattedDate(-1,0,0,0,0,0)
     #* set inputJSON $.FromDate = frmDate.substring()
    * def From = inputJSON.From
    * def To = inputJSON.To
    * print "Frome is"
    * print From
    * print "To is:"
    * print To
    * print "Today is"
    * print Today

    * set Result.From = From
    * set Result.To = To
    * set Result.FromDate = frmDate
    * set Result.ToDate = toDate
    * def FromDate = frmDate
    * def ToDate = toDate
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")

    Given url AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And param fromBookingDateTime = frmDate
    And param toBookingDateTime = toDate
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And header x-fapi-interaction-id = Random_id.random_id()
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def TransactionsResult = response
    And set Result.response = TransactionsResult

    When match responseStatus == 200
    And match TransactionsResult ==
    """
    {
  "Data": {
    "Transaction": [
    ]
  },
  "Links": {
    "Self": "#string"
  },
  "Meta": {
  }
}
"""

 Examples:
      |TCNO | Consent   | FromDate | ToDate | FromOp        | ToOp |condition |
      | 62  | Consent02 | Today | Not Specified |  |  | when TransactionFromDateTime is Not Specified and TransactionToDateTime is greater than LastOneYear and less than Today and FromBookingDateTime is Today and toBookingDateTime is Not Specified then As a result Empty Response |
      | 133 | Consent05 | greater than LastOneYear and less than Today | Not Specified |  |  | when TransactionFromDateTime is Not Specified and TransactionToDateTime is less than LastOneYear and FromBookingDateTime is greater than LastOneYear and less than Today and toBookingDateTime is Not Specified then As a result Empty Response |
      | 39  | Consent01 | greater than Today | Not specified |  |  | when TransactionFromDateTime is Not Specified and TransactionToDateTime is Not Specified and FromBookingDateTime is greater than Today and toBookingDateTime is Not specified then As a result Empty Response |
      | 149 | Consent05 | LastOneYear | Not Specified |  |  | when TransactionFromDateTime is Not Specified and TransactionToDateTime is less than LastOneYear and FromBookingDateTime is LastOneYear and toBookingDateTime is Not Specified then As a result Empty Response |

      #| 5 | Consent05 | Not specified | Not specified |  |  | When TransactionFromDateTime is Not Specified and TransactionToDateTime is less than LastOneYear and FromBookingDateTime is Not specified and toBookingDateTime is Not specified then As a result Empty Response |
      #| 45 | Consent01 | less than LastOneYear | less than LastOneYear |  |  | When TransactionFromDateTime is Not Specified and TransactionToDateTime is Not Specified and FromBookingDateTime is less than LastOneYear and toBookingDateTime is less than LastOneYear then As a result Empty response |


  @BookingDatecheck
  @severity=normal
  Scenario Outline: Test #key# To verify that transactions API returns Empty response when Transactions dates specified does not fit in consented transaction Dates criteria

    * def key = <TCNO>
    * set Result.condition = <condition>
    * json inputJSON = RWPropertyFile.ReadFile(path, fileName, "<Consent>")
    * call apiApp.configureSSL(inputJSON)
    And set inputJSON.request_method = "POST"
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
      ##And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
       # * remove inputJSON $.refresh_token
      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)

    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then def profile = { accountNumber: '#(apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification)'}
    And def txnx = call read('classpath:CMA_Release/Test_Set/AISP_OIDC/Change_Requests/CR20_Transactions/CR20_addTxns.feature') profile

    * match apiApp.Multi_Account_Information.responseStatus == 200

    * def frmDate = <FromDate>
    * print "Frombooking date is"
    * print frmDate
    * def toDate = <ToDate>
    * print "ToBooking date is"
    * print toDate
    * def Today = DateFormat.getFormattedDate(0,0,0,0,0,0)
    * def LastOneYear = DateFormat.getFormattedDate(-1,0,0,0,0,0)
     #* set inputJSON $.FromDate = frmDate.substring()
    * def From = inputJSON.From
    * def To = inputJSON.To
    * print "Frome is"
    * print From
    * print "To is:"
    * print To
    * print "Today is"
    * print Today

    * set Result.From = From
    * set Result.To = To
    * set Result.FromDate = frmDate
    * set Result.ToDate = toDate
    * def FromDate = frmDate
    * def ToDate = toDate
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")

    Given url AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And param fromBookingDateTime = frmDate
    And param toBookingDateTime = toDate
    And header client_id = inputJSON.client_id
    And header client_secret = inputJSON.client_secret
    And header x-fapi-interaction-id = Random_id.random_id()
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then def TransactionsResult = response
    And set Result.response = TransactionsResult

    When match responseStatus == 200
    And match TransactionsResult ==
    """
    {
  "Data": {
    "Transaction": [
    ]
  },
  "Links": {
    "Self": "#string"
  },
  "Meta": {
  }
}
"""

    #Then set Result $.TestStatus = "pass"
   # * print Result.TestStatus
    #* assert Result.TestStatus == "pass"
    Examples:
      |TCNO| Consent   | FromDate | ToDate | FromOp        | ToOp |condition |
      | 5 | Consent05 | Not specified | Not specified |  |  | When TransactionFromDateTime is Not Specified and TransactionToDateTime is less than LastOneYear and FromBookingDateTime is Not specified and toBookingDateTime is Not specified then As a result Empty Response |
      | 14 | Consent14 | Not specified | Not specified |  |  | When TransactionFromDateTime is greater than Today and TransactionToDateTime is Not Specified and FromBookingDateTime is Not specified and toBookingDateTime is Not specified then As a result Empty Response |
      | 15 | Consent15 | Not specified | Not specified |  |  | When TransactionFromDateTime is greater than Today and TransactionToDateTime is greater than Today and FromBookingDateTime is Not specified and toBookingDateTime is Not specified then As a result Empty Response |
      | 20 | Consent20 | Not specified | Not specified |  |  | When TransactionFromDateTime is less than LastOneYear and TransactionToDateTime is less than LastOneYear and FromBookingDateTime is Not specified and toBookingDateTime is Not specified then As a result Empty Response |
