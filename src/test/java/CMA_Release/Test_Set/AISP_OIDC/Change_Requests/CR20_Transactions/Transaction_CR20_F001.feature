
Feature:  Create Consents with different Transactions Date Time combinations

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    * def fileName = 'CR20_TestData.properties'
    * def path = './src/test/java/CMA_Release/Test_Data_' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def Application_Details = active_tpp.AISP_PISP
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')

#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @CR20Consents
  @severity=normal
  Scenario Outline: Test #key# Get Consent with TransactionFromDateTime and TransactionToDateTime combination

   # * set permissions $.Data.TransactionFromDateTime = DateFormat.getFormattedDate(-1,0,0,0,0)
   # * set permissions $.Data.TransactionToDateTime = DateFormat.getFormattedDate(1,0,0,0,0)
    * set permissions $.Data.Permissions =  ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"]
    * set permissions $.Data.TransactionFromDateTime =  <From>
    * set permissions $.Data.TransactionToDateTime = <To>
    * def name = '<ConsentID>'
    * def key = name
    * def input = {"name": '#(name)' }
    * print permissions.Data.TransactionFromDateTime
    * print permissions.Data.TransactionToDateTime
    * print "<ConsentID>"
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    * set Application_Details $.refresh_token = Application_Details.refresh_token
    * set Application_Details $.access_token = Application_Details.access_token
    * def F = permissions.Data.TransactionFromDateTime
    * def T = permissions.Data.TransactionToDateTime

        And def TA =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '#(permissions.Data.Permissions)',
      refresh_token: '#(Application_Details.refresh_token)',
      From: '#(F)',
      To: '#(T)'
    }
    """
    * def name = 'test'
    * def WriteToFile = RWPropertyFile.WriteFile(path,fileName,"<ConsentID>",TA)
    * print TA

    Examples:
    | ConsentID |             From                           |              To                            |
    | Consent08 | DateFormat.getFormattedDate(0,-1,-4,0,0,0) | DateFormat.getFormattedDate(0,-1,-1,0,0,0) |
    | Consent09 | DateFormat.getFormattedDate(0,-1,-4,0,0,0) | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
    | Consent10 | DateFormat.getFormattedDate(0,-1,-4,0,0,0) | DateFormat.getFormattedDate(0,0,0,0,0,0)   |
    | Consent12 | DateFormat.getFormattedDate(0,0,0,0,0,0)   | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
    | Consent13 | DateFormat.getFormattedDate(0,0,0,0,0,0)   | DateFormat.getFormattedDate(0,0,0,0,0,0)   |
    | Consent15 | DateFormat.getFormattedDate(0,0,1,0,0,0)   | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
    | Consent17 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | DateFormat.getFormattedDate(0,-1,-1,0,0,0) |
    | Consent18 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
    | Consent19 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | DateFormat.getFormattedDate(0,0,0,0,0,0)   |
    | Consent20 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | DateFormat.getFormattedDate(-1,0,-1,0,0,0) |
    | Consent21 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | DateFormat.getFormattedDate(-1,0,1,0,0,0)  |
    | Consent23 | DateFormat.getFormattedDate(-1,0,1,0,0,0)  | DateFormat.getFormattedDate(0,-1,-1,0,0,0) |
    | Consent24 | DateFormat.getFormattedDate(-1,0,1,0,0,0)  | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
    | Consent25 | DateFormat.getFormattedDate(-1,0,1,0,0,0)  | DateFormat.getFormattedDate(0,0,0,0,0,0)   |
    | Consent26 | DateFormat.getFormattedDate(-1,0,1,0,0,0)  | DateFormat.getFormattedDate(-1,0,1,0,0,0)  |



#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @CR20Consents
  @severity=normal
  Scenario Outline: Test #key# Get Consent with only TransactionFromDateTime specified combination

   # * set permissions $.Data.TransactionFromDateTime = DateFormat.getFormattedDate(-1,0,0,0,0)
   # * set permissions $.Data.TransactionToDateTime = DateFormat.getFormattedDate(1,0,0,0,0)
    * set permissions $.Data.Permissions =  ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"]
    * set permissions $.Data.TransactionFromDateTime =  <From>
    ##TransactionToDate is not specified as per the Examples section
    #* set permissions $.Data.TransactionToDateTime = <To>
    * remove permissions.Data.TransactionToDateTime
    * def name = '<ConsentID>'
    * def key = name
    * def input = {"name": '#(name)' }
    * print permissions.Data.TransactionFromDateTime
    * print permissions.Data.TransactionToDateTime
    * print "<ConsentID>"
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)

    * set Application_Details $.refresh_token = Application_Details.refresh_token
    * set Application_Details $.access_token = Application_Details.access_token
    * def F = permissions.Data.TransactionFromDateTime
   # * def T = permissions.Data.TransactionToDateTime

    And def TA =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '#(permissions.Data.Permissions)',
      refresh_token: '#(Application_Details.refresh_token)',
      From: '#(F)',
      To: 'NotSpecified'
    }
    """
    * def name = 'test'
    * def WriteToFile = RWPropertyFile.WriteFile(path,fileName,"<ConsentID>",TA)
    * print TA

    Examples:
      | ConsentID |             From                           | To |
      | Consent07 | DateFormat.getFormattedDate(0,-1,-4,0,0,0) | '' |
      | Consent11 | DateFormat.getFormattedDate(0,0,0,0,0,0)   | '' |
      | Consent14 | DateFormat.getFormattedDate(0,0,1,0,0,0)   | '' |
      | Consent16 | DateFormat.getFormattedDate(-1,0,-2,0,0,0) | '' |
      | Consent22 | DateFormat.getFormattedDate(-1,0,1,0,0,0)  | '' |

    #################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @CR20Consents
  @severity=normal
  Scenario Outline: Test #key# Get Consent with only TransactionToDateTime specified combination
    * set permissions $.Data.Permissions =  ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"]
    ##commented TransactionFromDate as it is not required for the consents as specified in Examples section
    #* set permissions $.Data.TransactionFromDateTime =  <From>
    * set permissions $.Data.TransactionToDateTime = <To>
    * remove permissions.Data.TransactionFromDateTime
    * def name = '<ConsentID>'
    * def key = name
    * def input = {"name": '#(name)' }
    * print permissions.Data.TransactionFromDateTime
    * print permissions.Data.TransactionToDateTime
    * print "<ConsentID>"
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)


    * set Application_Details $.refresh_token = Application_Details.refresh_token
    * set Application_Details $.access_token = Application_Details.access_token
    #* def F = permissions.Data.TransactionFromDateTime
    * def T = permissions.Data.TransactionToDateTime

    And def TA =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '#(permissions.Data.Permissions)',
      refresh_token: '#(Application_Details.refresh_token)',
      From: 'NotSpecified',
      To: '#(T)'
    }
    """
    * def name = 'test'
    * def WriteToFile = RWPropertyFile.WriteFile(path,fileName,"<ConsentID>",TA)
    * print TA

    Examples:
      | ConsentID | From |              To                            |
      | Consent02 | ''   | DateFormat.getFormattedDate(0,-1,-1,0,0,0) |
      | Consent03 | ''   | DateFormat.getFormattedDate(0,0,2,0,0,0)   |
      | Consent04 | ''   | DateFormat.getFormattedDate(0,0,0,0,0,0)   |
      | Consent05 | ''   | DateFormat.getFormattedDate(-1,0,-1,0,0,0) |
      | Consent06 | ''   | DateFormat.getFormattedDate(-1,0,1,0,0,0)  |


    #################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @CR20Consents
  @severity=normal
  Scenario Outline:  Test #key# Get Consent without TransactionFromDateTime and TransactionToDateTime
    * set permissions $.Data.Permissions =  ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"]
    ##commented TransactionFromDate as it is not required for the consents as specified in Examples section
    #* set permissions $.Data.TransactionFromDateTime =  <From>
    * remove permissions.Data.TransactionToDateTime
    * remove permissions.Data.TransactionFromDateTime
    * def name = '<ConsentID>'
    * def key = name
    * def input = {"name": '#(name)' }
    * print permissions.Data.TransactionFromDateTime
    * print permissions.Data.TransactionToDateTime
    * print "<ConsentID>"
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)


    * set Application_Details $.refresh_token = Application_Details.refresh_token
    * set Application_Details $.access_token = Application_Details.access_token
    #* def F = permissions.Data.TransactionFromDateTime
    * def T = permissions.Data.TransactionToDateTime

    And def TA =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '#(permissions.Data.Permissions)',
      refresh_token: '#(Application_Details.refresh_token)',
      From: 'NotSpecified',
      To: 'NotSpecified'
    }
    """
    * def name = 'test'
    * def WriteToFile = RWPropertyFile.WriteFile(path,fileName,"<ConsentID>",TA)
    * print TA

    Examples:
      | ConsentID | From |              To                            |
      | Consent01 | ''   | ''                                         |