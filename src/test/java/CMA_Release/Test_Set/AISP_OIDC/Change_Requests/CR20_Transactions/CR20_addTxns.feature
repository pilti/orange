@ignore
Feature:  Add transactions to ABT posted transactions table in order to test CR20 dates checks

  Background:

    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')
    * def UpdateABTTransactions = Java.type('CMA_Release.Entities_DB.UpdateABTTransactions')

#################################################################################################################################

@ABC
@ignore
Scenario: to add txn for CR20

  * def dates = {}
  * set dates $.date0 = DateFormat.getTxnDate(-1,0,-3,0,0,0)
  * set dates $.date1 = DateFormat.getTxnDate(-1,0,-2,0,0,0)
  * set dates $.date2 = DateFormat.getTxnDate(-1,0,-1,0,0,0)
  * set dates $.date3 = DateFormat.getTxnDate(-1,0,0,0,0,0)
  * set dates $.date4 = DateFormat.getTxnDate(-1,0,1,0,0,0)
  * set dates $.date5 = DateFormat.getTxnDate(-1,0,2,0,0,0)
  * set dates $.date6 = DateFormat.getTxnDate(0,-1,-5,0,0,0)
  * set dates $.date7 = DateFormat.getTxnDate(0,-1,-4,0,0,0)
  * set dates $.date8 = DateFormat.getTxnDate(0,-1,-3,0,0,0)
  * set dates $.date9 = DateFormat.getTxnDate(0,-1,-2,0,0,0)
  * set dates $.date10 = DateFormat.getTxnDate(0,-1,-1,0,0,0)
  * set dates $.date11 = DateFormat.getTxnDate(0,-1,0,0,0,0)
  * set dates $.date12 = DateFormat.getTxnDate(0,0,-1,0,0,0)
  * set dates $.date13 = DateFormat.getTxnDate(0,0,0,0,0,0)
  * set dates $.date14 = DateFormat.getTxnDate(0,0,1,0,0,0)
  * set dates $.date15 = DateFormat.getTxnDate(0,0,2,0,0,0)
  * set dates $.date16 = DateFormat.getTxnDate(0,0,3,0,0,0)
  * set dates $.date17 = DateFormat.getTxnDate(0,-6,0,0,0,0)
  * set dates $.date18 = DateFormat.getTxnDate(0,-3,0,0,0,0)
  * set dates $.date19 = DateFormat.getTxnDate(0,-9,0,0,0,0)

  * print "dates for CR20 are:  "
  * print dates
  * def accountnumber = profile.accountNumber
  * def addtxns = UpdateABTTransactions.addPostedTransactionsForCR20(accountnumber,dates,123,ABT_DB_Conn)

