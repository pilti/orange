
Feature:  To verify transactions API returns 400 Bad request when BookingDates are specified in invalid format

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def fileName = 'CR20_TestData.properties'
    * def path = './src/test/java/CMA_Release/Test_Data_' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def Application_Details = active_tpp.AISP_PISP
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')

#################################################################################################################################
  @CR20BookingDate
  @severity=normal
  Scenario Outline: To verify that Transactions API returns 400 Bad request error when #key# in api parameters
    * def key = <condition>
    * set Result.condition = <condition>
    * json inputJSON = RWPropertyFile.ReadFile(path, fileName, "Consent02")
    * call apiApp.configureSSL(inputJSON)
    And set inputJSON.request_method = "POST"
    * def subset = "Invalid_Booking_Date_Format"
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
      ##And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
       # * remove inputJSON $.refresh_token
      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)

    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * string frmDate = <FromDate>
    * print "Frombooking date is"
    * print frmDate
    * string toDate = <ToDate>
    * print "ToBooking date is"
    * print toDate

    * set Result.FromBookingDateTime = frmDate
    * set Result.ToBookingDateTime = toDate
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")
    Given url AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And param fromBookingDateTime = frmDate
    And param toBookingDateTime = toDate
    And header x-fapi-interaction-id = Random_id.random_id()
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then match responseStatus == 400
    And def TransactionsResult = response
    * set Result.response = TransactionsResult
    And set Result $.TestStatus = "pass"
    * print Result.TestStatus

    Examples:
      | condition                                          |FromDate                         | ToDate                         |
      | 'BookingDates are specified with Time zone'        | '2018\-05\-18T11\:43\:12+01:00' | '2018-05-18T13\:43\:12+01\:00' |
      | 'BookingDates are not specified in ISO8601 format' | '2019-04-19'                    |'2019-04-19'                    |
      | 'BookingDates are specified with timezone as Z'    | '2018\-05\-18T11\:43\:12.000Z'  | '2018-05-18T13\:43\:12.000Z'   |


  @CR20BookingDate
  @severity=normal
  Scenario: To verify that Transactions API returns 400 Bad request in case from Booking Date is greater than toBookingDate


    * json inputJSON = RWPropertyFile.ReadFile(path, fileName, "Consent02")
    * call apiApp.configureSSL(inputJSON)
    And set inputJSON.request_method = "POST"
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
        ##And set Result.Access_Token_RTG.Input = inputJSON
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    * match apiApp.Access_Token_RTG.responseStatus == 200
         # * remove inputJSON $.refresh_token
        ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)

    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * string frmDate = DateFormat.getBookingDate(0,0,1,0,0,0)
    * print "Frombooking date is"
    * print frmDate
    * string toDate = DateFormat.getBookingDate(0,0,0,0,0,0)
    * print "ToBooking date is"
    * print toDate

    * set Result.FromBookingDateTime = frmDate
    * set Result.ToBookingDateTime = toDate
    * def Random_id = Java.type("CMA_Release.Java_Lib.RandomValue")
    Given url AcctInfoUrl + inputJSON.AccountId + '/transactions'
    And param fromBookingDateTime = frmDate
    And param toBookingDateTime = toDate
    And header x-fapi-interaction-id = Random_id.random_id()
    And headers reqHeader
    And header Authorization = 'Bearer ' + inputJSON.access_token
    When method GET
    Then match responseStatus == 400
    And def TransactionsResult = response
    * set Result.response = TransactionsResult
    And set Result $.TestStatus = "pass"
    * print Result.TestStatus

