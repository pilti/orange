Feature:  ErrorScenario_To verify AccountRequestSetup API returns 400 Bad request when Transaction dates are invalid

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}

    * set Result.Testname = null
    * set Result.TestStatus = "fail"
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

    * def fileName = 'CR20_TestData.properties'
    * def path = './src/test/java/CMA_Release/Test_Data_' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def Application_Details = active_tpp.AISP_PISP
    * def DateFormat = Java.type('CMA_Release.Java_Lib.Date_Format')

#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @CR20NoConsent
  @severity=normal
  Scenario: To verify AccountRequestSetup API returns 400 bad request when TransactionFromDateTime is greater than TransactionToDateTime

    * set permissions $.Data.Permissions =  ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"]
    * set permissions $.Data.TransactionFromDateTime = DateFormat.getFormattedDate(0,0,0,0,0,0)
    * set permissions $.Data.TransactionToDateTime = DateFormat.getFormattedDate(0,0,0,0,0,-1)
    * print "Transaction PErid specified in Request is"
    * print permissions.Data.TransactionFromDateTime
    * print permissions.Data.TransactionToDateTime
    Given call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)

    And set Result.Access_Token_CCG.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.Access_Token_CCG.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.Access_Token_CCG.response = apiApp.Access_Token_CCG.response

    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type

    When call apiApp.Account_Request_Setup(Application_Details)
    And set Result.ACcount_Request_Setup.TransactionFromDate_specified = permissions.Data.TransactionFromDateTime
    And set Result.ACcount_Request_Setup.TransactionToDate_specified = permissions.Data.TransactionToDateTime
    And set Result.Account_Request_Setup.RequestPayload = permissions
    And set Result.Account_Request_Setup.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.Account_Request_Setup.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.Account_Request_Setup.response = apiApp.Account_Request_Setup.response

    Then match apiApp.Account_Request_Setup.responseStatus == 400

