@AISP_UAT
@AISP
@AISP_SCA
@AISP_Consent
@AISP_UAT_SCA
@Lightbox2
@Regression
Feature: This feature is to demonstrate the positive flow for lightbox validations after consent URL is launched

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def takescreenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * json Application_Details = active_tpp.AISP
    * def constructUrl = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
       webApp.stop();
       apiApp.write(Result,info);
       }
    """
  Scenario: Verify that the Lightbox2 appears when user clicks on I don’t have the Bank of Ireland KeyCode app registered with 365 online CTA on Lightbox1

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "DontHaveKeyCodeApp"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2HeadingText)
    And def getText = seleniumFunctions.getElementText(webApp,ObjectFile.lightbox2HeadingText)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2HeadingText)
    And set Result.UIResult.Expected = "To use this service you will need to install Bank of Ireland KeyCode app on your smart device and register it through 365 online."
    And set Result.UIResult.Actual = getText
    Then match getText == "To use this service you will need to install Bank of Ireland KeyCode app on your smart device and register it through 365 online."

  Scenario: Verify that the main text on Lightbox2

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Lightbox2_Text"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2HeadingText)
    And def getText = seleniumFunctions.getElementText(webApp,ObjectFile.lightbox2HeadingText)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2HeadingText)
    And set Result.UIResult.Expected = "To use this service you will need to install Bank of Ireland KeyCode app on your smart device and register it through 365 online."
    And set Result.UIResult.Actual = getText
    Then match getText == "To use this service you will need to install Bank of Ireland KeyCode app on your smart device and register it through 365 online."

  Scenario: Verify that the Account Access login page is displayed when pressed Esc from keyboard on Lightbox2

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Lightbox2_Esc"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2HeadingText)
    And def c1 = call seleniumFunctions.PressKeyboardFunctions(webApp.driver1,"esc")
    And def validateText = seleniumFunctions.getPageTitle(webApp)
    Then match validateText == "Login - Bank of Ireland"

  Scenario: Verify that Account Access login page should be displayed when user clicks on X at the top right corner of Lightbox2

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Lightbox2_X"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.cross_lightbox)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.cross_lightbox)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.LoginHeaderText)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    And set Result.UIResult.Expected = "Login here to manage third party access to your Bank of Ireland accounts"
    And set Result.UIResult.Actual = checkpage
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"

  Scenario: Verify that the Account Access login page is displayed when user clicks outside the Lightbox2

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Outside_LightBox2"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.cross_lightbox)
    And def click = seleniumFunctions.mouseclick(webApp)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    And set Result.UIResult.Expected = "Login here to manage third party access to your Bank of Ireland accounts"
    And set Result.UIResult.Actual = checkpage
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"
