@AISP_UAT
@AISP
@AISP_UAT_SCA
@AISP_Consent
@AISP_UAT_SCA12
@Regression
Feature: This feature is to demonstrate the positive flow for lightbox validations after consent URL is launched

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def takescreenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * json Application_Details = active_tpp.AISP
    * def constructUrl = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details

    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
       apiApp.write(Result,info);
        webApp.stop();
      }
    """

  @Functional_Shakedown_Quick
  Scenario: To Verify that lightbox1 appears when user is redirected from TPP site to the banks SCA Landing Page

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Lightbox1_Appears"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def ElementStatus = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.lightbox1)
    And set Result.Status = ElementStatus
    Then match ElementStatus == true

  Scenario: To verify the text on Lightbox1
    * def info = karate.info
    * set info.subset = "Lightbox1_Appears"
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def ExpectedText = 'This service allows you to manage third party access to your Bank of Ireland accounts. You must be a Bank of Ireland UK current account customer registered with 365 online to use this service.'
    And def focusOnElement = seleniumFunctions.focusOnElement(webApp, ObjectFile.lightbox1HeadingText)
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.lightbox1HeadingText)
    And set Result.UIResult.LightboxText = ActualText
    And set Result.UIResult.ExpectedText = ActualText
    Then match ActualText == ExpectedText

  Scenario: Verify that the Account Access login page is displayed when pressed Esc from keyboard on Lightbox1

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Esc"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def c1 = call seleniumFunctions.PressKeyboardFunctions(webApp.driver1,"esc")
    And def validateText = seleniumFunctions.getPageTitle(webApp)
    Then match validateText == "Login - Bank of Ireland"

  @CTA
  Scenario: Verify the below 2 CTAs are available on Lightbox1

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "2_CTAs"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def ExpectedTextCTA1 = 'I have the Bank of Ireland KeyCode app registered with 365 online'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.lightbox1CTA1)
    And set Result.LightboxTextCTA1 = ActualText
    And def ExpectedTextCTA2 = 'I don\'t have the Bank of Ireland KeyCode app registered with 365 online'
    And def ActualText1 = seleniumFunctions.getElementText(webApp, ObjectFile.lightbox1CTA2)
    And set Result.LightboxTextCTA2 = ActualText1
    Then match ActualText == ExpectedTextCTA1
    And match ActualText1 == ExpectedTextCTA2

  Scenario: Verify that the Account Access login page is displayed when user clicks on X at the top right corner of Lightbox1

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "UserClicks_X"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.cross_lightbox)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.cross_lightbox)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.LoginHeaderText)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"

  Scenario: Verify that the Account Access login page is displayed when user clicks outside the Lightbox1

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "ClickOutsideLightBox"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def click = seleniumFunctions.mouseclick(webApp)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"


  Scenario: Verify that the Account Access login page is displayed when user clicks on  I have the Bank of Ireland KeyCode app registered with 365 online CTA

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "HaveKeyCodeApp"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.have_key_app_bttn)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"
