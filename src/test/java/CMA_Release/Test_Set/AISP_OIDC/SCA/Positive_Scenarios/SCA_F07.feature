@AISP
@AISP_UAT
@AISP_UAT_SCA


Feature: This feature does UI validations on successful launch of Refresh Token Renewal Screen

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }

       apiApp.write(Result,info);
       }
    """

  @Functional_Shakedown
  Scenario: Verify that login is successful using SCA for consent renewal

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Consent_SCA_Renewal"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    When def Prerequisite = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.RefreshTokenRenewalPrerequisite = Prerequisite

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.launchConsentURL(webApp, Application_Details.consenturl)
    And def perform = Functions.useKeyCodeApp(webApp)
    Then def perform = Functions.scaLogin(webApp,data)
    And set Result.UIResult = perform


  Scenario: Verify user is able to login successfully when correct credentials are provided

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Correct_Credentials"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given def Application_Details = active_tpp.AISP_PISP
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    And print apiApp.CreateRequestObject.jwt.request
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then def perform = Functions.useKeyCodeApp(webApp)

    Given def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    Then set Result.UIResult = perform


  Scenario: Verify user is not able to login successfully when incorrect credentials are provided

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Incorrect_Credentials"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given def Application_Details = active_tpp.AISP_PISP
    And def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then def perform = Functions.useKeyCodeApp(webApp)

    Given def data = {"usr":'#(user_details.usr)',"otp":'123',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    Then set Result.UIResult = perform

