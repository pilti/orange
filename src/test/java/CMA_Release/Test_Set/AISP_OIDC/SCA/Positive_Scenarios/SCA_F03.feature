@AISP_UAT
@AISP
@AISP_SCA
@AISP_Consent
@AISP_UAT_SCA
@Regression
Feature: This feature is to demonstrate the positive flow for lightbox validations after consent URL is launched

  Background:

    #* configure proxy = 'http://webproxy.boigroup.net:8080'
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def takescreenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * json Application_Details = active_tpp.AISP
    * def constructUrl = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
       webApp.stop();
       apiApp.write(Result,info);
       }
    """

  Scenario: Verify that the user will be redirected to TPP when user clicks on button Return to third party

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Return_To_TPP"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA1)
    And def clickonReturn = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA1)
    And def getURL = seleniumFunctions.getCurrentURL(webApp)
    And set Result.UIReult.Expected = "https://app.getpostman.com/oauth2/callback#state=af0ifjsldkj&error=access_denied"
    And set Result.UIReult.Expected = getURL
    Then match Result.UIReult.Expected == Result.UIReult.Expected

  Scenario: Verify that the Account Access login page is displayed when user clicks on Continue with login CTA

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "clickonContinue_CTA"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA3)
    And def clickonContinue = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA3)
    And def checkpage = seleniumFunctions.getElementText(webApp,ObjectFile.LoginHeaderText)
    And set Result.UIResult.Expected = "Login here to manage third party access to your Bank of Ireland accounts"
    And set Result.UIResult.Actual = checkpage
    Then match checkpage == "Login here to manage third party access to your Bank of Ireland accounts"

  Scenario: Verify that the Lightbox3 appears when user clicks on Register KeyCode app CTA on Lightbox2

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Continue_L3"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3)
    And def lightbox3 = seleniumFunctions.getElementText(webApp,ObjectFile.lightbox3)
    And set Result.UIResult.ActualText_LightBox3 = lightbox3
    Then assert lightbox3.contains("Pop up displayed")

#  Scenario: Verify that the Lightbox3 appears with main text
#
#    Given def info = karate.info
#    * set info.Screens = 'Yes'
#
#    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
#    And def path = call  webApp.path1 = info1.path
#    And def Text =
#    """
#    {
#    "Expected":"To register you must login to 365 online, choose 'Manage Accounts' from left hand menu, select 'Manage KeyCode app' and follow the onscreen instructions. You must have your mobile phone already registered to receive security codes to start the process.Registering KeyCode will take at least 10 minutes - your session with the third party will probably expire.If you begin the Keycode app registration now you must complete the process or you will have to begin registration again on your next attempt."
#    }
#    """
#
#    And def c = call  webApp.driver1 = webApp.start1(default_browser)
#    Given def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
#
#    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
#    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
#    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
#
#    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3HeadingText)
#    And def lightbox3 = seleniumFunctions.getElementText(webApp,ObjectFile.lightbox3HeadingText)
#    And set Result.UIResult.ActualText_LightBox3 = lightbox3.replaceAll("\\n","")
#
#    And set Text.Actual = lightbox3.replaceAll("\\n","")
#    And string Ecpected = Text.Expected
#
#    And set Result.UIResult.EcpectedText_LightBox3 = Text.Expected
#
#    And string Actual = Text.Actual
#    And match Ecpected == Actual
#

  Scenario: Verify that the Account Access login page is displayed when pressed Esc from keyboard on Lightbox3

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Esc"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3HeadingText)
    And def c1 = call seleniumFunctions.PressKeyboardFunctions(webApp.driver1,"esc")
    And def validateText = seleniumFunctions.getPageTitle(webApp)
    Then match validateText == "Login - Bank of Ireland"

  Scenario: Verify that the Account Access login page is displayed when user clicks on X at the top right corner of Lightbox3

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Click_X"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3HeadingText)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.cross_lightbox)
    And def validateText = seleniumFunctions.getPageTitle(webApp)
    And set Result.UIResult.ExpectedText = "Login - Bank of Ireland"
    And set Result.UIResult.ActualText = validateText
    Then match validateText == "Login - Bank of Ireland"