@AISP_UAT
@AISP
@AISP_SCA
@AISP_Consent
@AISP_UAT_SCA
@Regression
Feature: This feature is to demonstrate the positive flow for lightbox validations after consent URL is launched

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def takescreenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * json Application_Details = active_tpp.AISP
    * def constructUrl = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;

       }
       webApp.stop();
       apiApp.write(Result,info);
       }
    """

  Scenario: Verify that the Account Access login page is displayed when user clicks outside the Lightbox3

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Outside_Lightbox3"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3Head)
    And def click = seleniumFunctions.mouseclick(webApp)
    And def validateText = seleniumFunctions.getPageTitle(webApp)
    And set Result.UIResult.ExpectedText = "Login - Bank of Ireland"
    And set Result.UIResult.ActualText = validateText
    Then match validateText == "Login - Bank of Ireland"


  Scenario: Verify that user is navigated to Lightbox2 when clicks on 'Back to options' on Lightbox3

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "BackToOptions"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3CTA1)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox3CTA1)
    And def register = seleniumFunctions.getElementText(webApp,ObjectFile.lightbox2CTA2)
    And def getTitle = seleniumFunctions.getPageTitle(webApp)
    And set Result.UIResult.ExpectedPage =  "Login - Bank of Ireland"
    And set Result.UIResult.ActualPage =  getTitle
    Then match getTitle == "Login - Bank of Ireland"

  Scenario: The user should be navigated to 365 online on new tab when user clicks on 'Login to 365 online' on Lightbox3

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "365Online"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def click = seleniumFunctions.ClickElement(webApp,ObjectFile.dont_have_key_app_bttn)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox2CTA2)
    And def register = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox2CTA2)
    And def focus = seleniumFunctions.focusOnElement(webApp,ObjectFile.lightbox3CTA2)
    And def clickonbutton = seleniumFunctions.ClickElement(webApp,ObjectFile.lightbox3CTA2)
    And def newTab = seleniumFunctions.HandleWindow(webApp)
    And def getTitle = seleniumFunctions.getPageTitle(webApp)
    Then print getTitle

  @abc
  Scenario: Verify that user is navigated to TPP page when user selects the browser back button on SCA landing page

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Back_LightBox3"
    * def value = "Fail"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,constructUrl.Result.CreateRequestObject.URL)
    And def clickback = seleniumFunctions.BrowserBack(webApp)
    And def checkElementpresent1 = seleniumFunctions.CheckElementPresent(webApp,ObjectFile.dont_have_key_app_bttn)
    And print checkElementpresent1
    And def clickforward = seleniumFunctions.BrowserForward(webApp)
    And def checkElementpresent2 = seleniumFunctions.CheckElementPresent(webApp,ObjectFile.dont_have_key_app_bttn)
    And print checkElementpresent2
    And def value = (checkElementpresent1 == false && checkElementpresent2 == true) ? "Pass": "Fail"
    Then match value == "Pass"