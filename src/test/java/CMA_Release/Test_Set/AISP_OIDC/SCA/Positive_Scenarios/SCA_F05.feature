@AISP
@AISP_UAT
@AISP_UAT_SCA


Feature: Content validation on SCA Login page

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  @severity=normal
  Scenario:Verify presence and content of Bank name in Title of SCA login page

    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def ExpectedTitle = 'Login - Bank of Ireland'
    And def ActualTitle = seleniumFunctions.getPageTitle(webApp)
    And print ActualTitle
    And set Result.UIResult.ActualTitle = ActualTitle
    And set Result.UIResult.ExpectedTitle = ExpectedTitle
    Then match ActualTitle == ExpectedTitle


  @severity=normal
  Scenario Outline: Verify presence and content of #key# on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And  def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def ExpectedElement = true
    And def modulename = call  webApp.moduleName1 = "CheckingElementPresence"
    And def ActualElement = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.<element>)
    And print ActualElement
    And set Result.UIResult.ActualElement = ActualElement
    And set Result.UIResult.ExpectedElement = ExpectedElement
    Then match ActualElement == ExpectedElement

    Examples:
      | element              | key                      |
      | boilogo              | 'BOI Logo'               |
      | portalName           | 'Portal Name'            |
      | portalthirdpartytext | 'Text below Portal Name' |
      | LoginHeaderText      | 'LoginHeaderText'        |


  @severity=normal
  Scenario Outline: Verify presence and content of #key# module on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def temp = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def ExpectedElement = true
    And def modulename = call  webApp.moduleName1 = "CheckingElemenetPresence"
    And def ActualElement = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.<element>)
    And print ActualElement
    And set Result.UIResult.ActualElement = ActualElement
    And set Result.UIResult.ExpectedElement = ExpectedElement
    Then match ActualElement == ExpectedElement

    Examples:
      | element              | key                               |
      | UsernameTextboxlabel | 'Label of UserID textbox field'   |
      | PasswordTextboxlabel | 'Label of Password textbox field' |
      | UsernameinFieldText  | 'UserID in-field text'            |
      | PasswordinFieldText  | 'Password in-field text'          |

  @take
  @severity=normal
  Scenario Outline: Verify presence,content and Navigation of #key# link on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def clickbutton = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "MousehoverTooltip"
    And def clickbutton = seleniumFunctions.MouseHoveronElement(webApp, ObjectFile.<element>)
    And def modulename = call  webApp.moduleName1 = "GetTooltipText"
    And def ActualText = seleniumFunctions.getvaluebyAttribute(webApp, ObjectFile.<ElementText>,"content")
    And print ActualText
    And def ExpectedText = <ExpectedText>
    And set Result.UIResult.ActualText = ActualText
    And set Result.UIResult.ExpectedText = ExpectedText
    Then match ActualText == ExpectedText

    Examples:
      | element       | ElementText       | key                             | ExpectedText                                                       |
      | UserIDToolTip | UserIDToolTipText | 'Tooltip for UserID Text field' | "This is the same User ID required when logging in to 365 online." |
      |PasswordToolTip  | PasswordToolTipText |'Tooltip for Password Text field' |"To generate the password, click on the 'Password' menu option on the Bank of Ireland KeyCode app. If you have not registered the KeyCode app see help section."|

  @severity=normal
  Scenario Outline: Verify presence and content of #key# module on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def ExpectedText = <ElementText>
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def temp = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "CheckingElementText"
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.<element>)
    And print ActualText
    And set Result.UIResult.ActualText = ActualText
    And set Result.UIResult.ExpectedText = ExpectedText
    Then match ActualText == ExpectedText

    Examples:
      | element                | key                             | ElementText                                                                                                                                                               |
      | regulatoryText         | 'Regulatory Text'               | 'Bank of Ireland (UK) plc is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority.' |
      | NotebelowPasswordfield | 'Note below password Textfield' | 'PLEASE NOTE: These services are currently only available to Bank of Ireland UK customers who use 365 online.'                                                            |


  @severity=normal
  Scenario Outline: Verify presence,content and enablement of #key# module on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def ExpectedElement = true
    And def ExpectedText = <ButtonText>
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def temp = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "ElementPresenceAndEnablement"
    And def ActualElement = seleniumFunctions.CheckElementEnabled(webApp, ObjectFile.<element>)
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.<element>)
    And print ActualElement
    And print ActualText
    And set Result.UIResult.ActualElement = ActualElement
    And set Result.UIResult.ExpectedElement = ExpectedElement
    And set Result.UIResult.ActualText = ActualText
    And set Result.UIResult.ExpectedText = ExpectedText
    Then match ActualElement == ExpectedElement
    And match ActualText == ExpectedText


    Examples:
      | element            | key                          | ButtonText            |
      | continue_button    | 'Continue Button'            | 'Continue'            |
      | back_to_tpp_button | 'Back to Third Party Button' | 'Back to Third Party' |
