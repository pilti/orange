@AISP
@AISP_UAT
@AISP_UAT_SCA


Feature: Content validation on SCA Login page

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  @severity=normal
  Scenario Outline: Verify presence and content of #key# on right panel of SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def ExpectedElement = true
    And def modulename = call  webApp.moduleName1 = "CheckingElementPresence"
    And def ActualElement = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.<element>)
    And print ActualElement
    And set Result.UIResult.ActualElement = ActualElement
    And set Result.UIResult.ExpectedElement = ExpectedElement
    Then match ActualElement == ExpectedElement

    Examples:
      | element                  | key                                             |
      | RightpanelLabel          | 'Label of Right panel'                          |
      | BOIKeyCodeHeader         | 'Bank or Ireland section header on Right Panel' |
      | Registerkeycodeappheader | 'Registering KeyCode app'                       |

  @12345
  @severity=normal
  Scenario Outline: Verify presence and content of #key# module on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And print apiApp.CreateRequestObject.jwt.request
    Then set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def String1 = 'STEP 1: Login to 365 online '
    And def String2 = 'STEP 2: Choose ' + "'Manage Accounts'" + ' from left hand menu '
    And def String3 = 'STEP 3: Select the ' + "'Manage KeyCode app'" + ' option and follow the onscreen instructions'
    And def ExpectedText = <ElementText>
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def temp = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "CheckingElementText"
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.<element>)
    And print ActualText
    And def ActualText = ActualText.replaceAll('\\n',' ')
    And print ActualText
    And set Result.UIResult.ActualText = ActualText
    And set Result.UIResult.ExpectedText = ExpectedText
    Then match ActualText == ExpectedText

    Examples:
      | element                      | key                                         | ElementText                 |
      | TextunderBOIkeyCodeHeader1   | 'Text under BOI keycode Header'             | 'To login to this service you will need to have previously registered Bank of Ireland KeyCode app on 365 online.' |
      | TextunderBOIkeyCodeHeader2   | 'Text under BOI keycode Header'             | 'To find out more see KeyCode help'                                                                               |
      | TextunderRegkeyCodeappHeader | 'Text under Registering keycode app Header' | String1 + String2 + String3 |


  @severity=normal
  Scenario Outline: Verify presence,content and Navigation of #key# link on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def clickbutton = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "CheckingLinkPresence"
    And def ActualElement = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.<element>)
    And def modulename = call  webApp.moduleName1 = "ClickingElement"
    And def Clickelement = seleniumFunctions.ClickElement(webApp, ObjectFile.<element>)
    And def switchtolatestwindow = seleniumFunctions.SwitchfocustoLatestWindow(webApp.driver1)
    And def modulename = call  webApp.moduleName1 = "Link Navigation"
    And def ActualLink = seleniumFunctions.getCurrentURL(webApp)
    And def ExpectedLink = <NavigationLink>
    And set Result.UIResult.ActualLink = ActualLink
    And set Result.UIResult.ExpectedLink = ExpectedLink
    Then match ActualLink == ExpectedLink

    Examples:
      | element               | key                   | NavigationLink                                                             |
      | KeyCodehelpLink       | 'KeyCode Help'        | 'https://www.bankofireland.com/accountaccess/KeyCodehelp'                  |
      | loginlinkonrightPanel | 'Login to 365 online' | 'https://www.365online.com/online365/spring/authentication?execution=e1s1' |

  @severity=normal
  Scenario Outline: Verify presence,content and Navigation of #key# link on SCA login page

    * def key = <key>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = <key>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def ip_path = active_tpp.AISP_PISP
    And def path = stmtpath(ip_path) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Result.CreateRequestObject.JWT_Input = ReqObjIP
    And set Result.CreateRequestObject.jwt = apiApp.CreateRequestObject.jwt.request
    Then print apiApp.CreateRequestObject.jwt.request

    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And def modulename = call  webApp.moduleName1 = "ClickingKeyCodeAppButton"
    And def clickbutton = seleniumFunctions.ClickElement(webApp, ObjectFile.have_key_app_bttn)
    And def modulename = call  webApp.moduleName1 = "CheckingLinkPresence"
    And def ActualElement = seleniumFunctions.CheckElementPresent(webApp, ObjectFile.<element>)
    And def modulename = call  webApp.moduleName1 = "ClickingElement"
    And def Clickelement = seleniumFunctions.ClickElement(webApp, ObjectFile.<element>)
    And def switchtolatestwindow = seleniumFunctions.SwitchfocustoLatestWindow(webApp.driver1)
    And def modulename = call  webApp.moduleName1 = "Link Navigation"
    And def ActualLink = seleniumFunctions.getCurrentURL(webApp)
    And def ExpectedLink = <NavigationLink>
    And print ActualLink
    And set Result.UIResult.ActualLink = ActualLink
    And set Result.UIResult.ExpectedLink = ExpectedLink
    Then match ActualLink == ExpectedLink

    Examples:
      | element         | key                         | NavigationLink                                                  |
      | AboutUsLink     | 'About us Link'             | 'https://www.bankofireland.com/accountaccess/aboutus'           |
      | CookiePrivacy   | 'Cookie and Privacy Policy' | 'https://www.bankofireland.com/legal/privacy-statement/'        |
      | TermsConditions | 'Terms and conditions'      | 'https://www.365online.com/online365/spring/termsAndConditions' |
      | Help            | 'Help'                      | 'https://www.bankofireland.com/accountaccess/help'              |
      | NeedHelplink    | 'NeedHelp'                  | 'https://www.bankofireland.com/accountaccess/help'              |




