@AISP
@AISP_UAT
@AISP_UAT_SCA
@AISP_Error_Bus


Feature: This Feature demonstrates the Error Scenarios for SCA

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')

    * json Application_Details = active_tpp.AISP
    * set Result.Input.TPPRole = 'AISP'
    * set Result.Input.scope = Application_Details.scope

    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

    ###################################################################################################################################

  @Environmental_Error
  @severity=normal
  Scenario: Verify that appropriate error message is displayed when backend server is down i.e. Any internal server error scenarios including server re-tries and timeouts

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Backend Server down'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

#Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id =  Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirct_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

#Call CreateRequestObject
    And call apiApp.CreateRequestObject(Application_Details)
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Application_Details $.jwt = (apiApp.CreateRequestObject.jwt.request)
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}
    #* def data = {"usr":'682833',"otp":'123456',"action":"continue","Confirmation":"Yes"}

    And set Result.ActualOutput.UI.Input.UserId = data.usr
    And set Result.ActualOutput.UI.Input.OTP = data.otp

    When def perform = Functions.scaLogin(webApp,data)
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform.reasonText

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.SCA_error_text)
    And def ExpectedUIErrorMessage = 'Unable to process your request. Please try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    And set Result.Additional_Details = Application_Details
    * def capturescreen = capturescreen.captureScreenShot2(webApp)

    And match ActualText == ExpectedUIErrorMessage

    ##########################################################################################################################

  @severity=normal
  Scenario: Verify that the user ID gets locked after 3 unsuccessful attempts on BOI API Portal

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = '3 Unsuccessful Attempts'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

#Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id =  Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'

    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
#Call CreateRequestObject
    And call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = (apiApp.CreateRequestObject.jwt.request)
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    When def c = call  webApp.driver1 = webApp.start1(default_browser)
    Then def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'682833',"otp":'',"action":"continue","Confirmation":"Yes"}

    And set Result.ActualOutput.UI.Input.FirstAttempt_UserId = <usr>
    And set Result.ActualOutput.UI.Input.FirstAttempt_OTP = data.otp

    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform.reasonText

    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.username_text)
    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.otp_text)

    * set data.otp = '123'
    And set Result.ActualOutput.UI.Input.SecondAttempt_UserId = <usr>
    And set Result.ActualOutput.UI.Input.SecondAttempt_OTP = data.otp

    And def perform2 = Functions.scaLogin(webApp,data)
    And match perform2.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform2.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform2.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform2.reasonText

    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.username_text)
    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.otp_text)

    * set data.otp = '1234'
    And set Result.ActualOutput.UI.Input.ThirdAttempt_UserId = <usr>
    And set Result.ActualOutput.UI.Input.ThirdAttempt_OTP = data.otp

    And def perform3 = Functions.scaLogin(webApp,data)
    And match perform3.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform3.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform3.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform3.reasonText

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.SCA_error_text)
    And def ExpectedUIErrorMessage = 'Your account is temporarily blocked. Please try again in 1 hour.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    And set Result.Additional_Details = Application_Details
    * def capturescreen = capturescreen.captureScreenShot2(webApp)

    And match ActualText == ExpectedUIErrorMessage

    ############################################################################################################################
  @Anu85
  @severity=normal
  Scenario: To verify the Error message displayed when Authentication Failed when User has entered the password incorrectly 2 times on Login Screen

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = '2 Unsuccessful Attempts'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

  #Set input parameters for CreateRequestObject
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id =  Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'

    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

#Call CreateRequestObject
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = (apiApp.CreateRequestObject.jwt.request)
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'682833',"otp":'',"action":"continue","Confirmation":"Yes"}

    And set Result.ActualOutput.UI.Input.FirstAttempt_UserId = data.usr
    And set Result.ActualOutput.UI.Input.FirstAttempt_OTP = data.otp

    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform.reasonText

    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.username_text)
    And def perform1 = Reuseable.cleartext(webApp,ObjectFile.otp_text)

    * set data.otp = '123'
    And set Result.ActualOutput.UI.Input.SecondAttempt_UserId = data.usr
    And set Result.ActualOutput.UI.Input.SecondAttempt_OTP = data.otp

    And def perform2 = Functions.scaLogin(webApp,data)
    And match perform2.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform2.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform2.Landing_Page
    And set Result.ActualOutput.UI.Output.ScaLogin_reasonText = perform2.reasonText

    And def ActualText = Reuseable.getElementText(webApp, ObjectFile.SCA_error_text)
    And def ExpectedUIErrorMessage = 'Your login details are incorrect, please try again.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    And set Result.Additional_Details = Application_Details
    * def capturescreen = capturescreen.captureScreenShot2(webApp)

    And match ActualText == ExpectedUIErrorMessage


