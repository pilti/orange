@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth

Feature: Scope and access token validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')

    * def Application_Details = active_tpp.AISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.redirect_uri =  Application_Details.redirect_uri
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id

    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path
    * print ReqObjIP
    * call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    #* set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * set Result.RequestObjectInput = ReqObjIP
    * set Result.ConsentURL = consenturl
    * def c = call  webApp.driver1 = webApp.start1(default_browser)

    * def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * def perform = Functions.selectAllAccounts(webApp,data)
    * set UIResult.AccountSelectionPage = perform
    * set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts
    * def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    #And set Application_Details.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform
    * set Application_Details.code = perform.Action.Authcode
    * set UIResult.code = perform.Action.Authcode
    * def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    #And def screenshot = capturescreen.captureScreenShot2(webApp)
    #* def stop = webApp.stop()

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP role triggers API with invalid scope #key# provided as a parameter

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'

      #Input invalid/not allowed scope in API request
    Given set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://boi.com"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback"
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
   #call Access_token using Authorization code grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header scope = <scope>
    And request body
    When method POST
    Then match responseStatus == 200

    Examples:
      |scope|
      |'abcdef'|
      |'payments'   |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP role triggers API with invalid scope #key# provided in request body

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'

   #Input invalid/not allowed scope in API request
    Given set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://boi.com"
     And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=" + Application_Details.redirect_uri + "&scope=" + Application_Details.scope
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
#call Access_token using Authorization code grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == <expectedstatuscode>

    Examples:
      | scope      | expectedstatuscode |
      | 'abcdef'   | 400                |
      | 'payments' | 400                |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal
  @Regression
  @test_test_1
  Scenario: Verify whether access token generated using auth code is not valid after 5 mins

    * def info = karate.info

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://boi.com"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And set Application_Details $.request_method = "POST"
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_ACG.response.token_type
    And set Application_Details $.request_method = "GET"
    And print "Wait starts"
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(310000)
    And print "wait ends"
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 401





