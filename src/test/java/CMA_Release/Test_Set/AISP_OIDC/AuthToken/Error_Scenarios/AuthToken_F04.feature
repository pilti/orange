@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth



Feature: Method,scope, Endpoint and TPP validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')

    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set Application_Details $.redirect_uri = Application_Details.redirect_uri
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id

    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path
    * call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    #* set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.redirect-uri =  Application_Details.redirect-uri
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    * set Result.RequestObjectInput = ReqObjIP
    * set Result.ConsentURL = consenturl
    * def c = call  webApp.driver1 = webApp.start1(default_browser)

    * def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * def data = {"usr":'#(user_details.Generic.G_User_1.usr)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * def perform = Functions.selectAllAccounts(webApp,data)
    * set UIResult.AccountSelectionPage = perform
    * set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts
    * def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    * set Application_Details.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform
    * set Application_Details.code = perform.Action.Authcode

    * set UIResult.code = perform.Action.Authcode

    * def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    * def screenshot = capturescreen.captureScreenShot2(webApp)
    #* def stop = webApp.stop()

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  @AISP_Error_Tech
  @severity=normal

  Scenario Outline: Verify whether error is received when API is triggered with #key# method

    * def info = karate.info
    * def key = <Method>
    * set info.subset = 'Method validation'
     #Input not allowed method in API request

    Given set Application_Details $.request_method = <Method>
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 405

    Examples:
      | Method     |
      | 'GET'      |
      | 'DELETE'   |
      | 'COPY'     |
      | 'LINK'     |
      | 'UNLINK'   |
      | 'PUT'      |
      | 'PURGE'    |
      | 'PROPFIND' |
      | 'OPTIONS'  |
      | 'LOCK'     |
      | 'UNLOCK'   |
      | 'HEAD'     |
      | 'PATCH'    |
      | 'VIEW'     |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP and PISP role triggers API with invalid scope #key# provided in request body

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'
      #Input invalid/not allowed scope in API request

    Given set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback" + "&scope=" + Application_Details.scope
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
   #call Access_token using Authorization code grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == <ExpectedStatusCode>

    Examples:
      | scope      | ExpectedStatusCode |
      | 'abcdef'   | 400                |
      | 'payments' | 200                |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when TPP having AISP and PISP role triggers API with invalid scope #key# provided as a parameter

    * def info = karate.info
    * def key = <scope>
    * set info.subset = 'Scope validation'
    #Input invalid/not allowed scope in API request

    Given set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback"
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
 #call Access_token using Authorization code grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header scope = <scope>
    And request body
    When method POST
    Then match responseStatus == 200

    Examples:
      | scope      |
      | 'abcdef'   |
      | 'payments' |


  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when API is triggered with invalid URI#key#

    * def info = karate.info
    * def key = <count>
    * set info.subset = 'URI validation'
        #Input invalid URI in API request

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback"
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
    * def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + tokenurl
    #call Access_token using Authorization code grant API
    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 404


    Examples:
      | count | TokenUrl_SIT                                     | TokenUrl_UAT                                             |
      | '1'   | https://api.boitest.net/oauth/as/token           | https://api.u2.psd2.boitest.net/oauth/as/token           |
      | '2'   | https://api.boitest.net/oauth/as/oauth2          | https://api.u2.psd2.boitest.net/oauth/as/oauth2          |
      | '3'   | https://api.boitest.net/oauth/as/token.Oauth2    | https://api.u2.psd2.boitest.net/oauth/as/token.Oauth2    |
      | '4'   | https://api.boitest.net/oauth/as/Token.oauth2    | https://api.u2.psd2.boitest.net/oauth/as/Token.oauth2    |
      | '5'   | https://api.boitest.net/oauth/as//token.oauth2   | https://api.u2.psd2.boitest.net/oauth/as//token.oauth2   |
      | '6'   | https://api.boitest.net/oauth/token.oauth2       | https://api.u2.psd2.boitest.net/oauth/token.oauth2       |
      | '7'   | https://api.boitest.net/oauth//as/token.oauth2   | https://api.u2.psd2.boitest.net/oauth//as/token.oauth2   |
      | '8'   | https://api.boitest.net/as/token.oauth2          | https://api.u2.psd2.boitest.net/as/token.oauth2          |
      | '9'   | https://api.boitest.net/oauth/as/token.oauth2/// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/// |


  @AISP_Error_Tech
  @severity=normal

  Scenario Outline: Verify whether access token is generated successfully when URI#key# having extra forward slashes

    * def info = karate.info
    * def key = <count>
    * set info.subset = 'URI validation'
   #Input extra slashes after domain name or at the end of endpoint

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback"
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
    * def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + tokenurl
  #call Access_token using Authorization code grant API
    And url tokenurl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 200
    And match response contains {"access_token": '#string'}


    Examples:
      | count | TokenUrl_SIT                                    | TokenUrl_UAT                                            |
      | '1'   | https://api.boitest.net/oauth/as/token.oauth2   | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2   |
      | '2'   | https://api.boitest.net/oauth/as/token.oauth2// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2// |
      | '3'   | https://api.boitest.net/oauth/as/token.oauth2/  | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/  |


  @AISP_Error_Tech
  @severity=normal

  Scenario Outline: Verify whether error is received when API is triggered with TPP having PISP role

    * def info = karate.info
    #Configure network certificate of TPP having PISP role only
    Given def Application_Details = active_tpp.<TPP>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.request_method = "POST"
    #pass auth code generated by TPP having AISP and PISP role
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | TPP  |
      | PISP |

