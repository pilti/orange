@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth

Feature: Auth code validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set Application_Details $.redirect-uri = Application_Details.redirect_uri
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path
    * call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * set Result.RequestObjectInput = ReqObjIP
    * set Result.ConsentURL = consenturl
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * def perform = Functions.selectAllAccounts(webApp,data)
    * set UIResult.AccountSelectionPage = perform
    * set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts
    * def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    #And set Application_Details.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform
    * set Application_Details.code = perform.Action.Authcode
    * set UIResult.code = perform.Action.Authcode
    * def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    #And def screenshot = capturescreen.captureScreenShot2(webApp)

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  @Regression
  @Test
  @test_test
  Scenario Outline: Verify whether error is received when invalid auth code is sent in API request

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    #Input invalid auth_code in API request
    And set Application_Details $.code = <code>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400


    Examples:
      | code                                         |
      | 'Zm3VEDYf4d23_3PPCIufEuPJslVQj5oIEGU9Mdfghf' |


  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when Empty value of auth code is sent in API request

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    #Input empty auth_code in API request
    And set Application_Details $.code = <code>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | code |
      | ' '  |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Verify whether error is received when consumed auth code is sent in API request

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    #Input expired auth_code in API request
    And set Application_Details $.code = <Expired_code>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | Expired_code                               |
      | 'Zm3VEDYf4d23_3PPCIufEuPJslVQj5oIEGU9MQUy' |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether error is received when auth code parameter is not sent in API request

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
   #Send auth_code parameter as Null in API request
    And set Application_Details $.code = null
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal
  Scenario: Verify whether error is received when auth code parameter generated by different TPP is sent in API request

    * def info = karate.info

    Given def Application_Details = active_tpp.AISP
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details.grant_type = "authorization_code"
    #And set Application_Details.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
   #Input auth_code generated by different TPP is sent in API request
    And set Application_Details.code = UIResult.code
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp1.Access_Token_ACG(Application_Details)
    Then match apiApp1.Access_Token_ACG.responseStatus == 400


  @AISP_Error_Bus
  @severity=normal
  Scenario: Verify whether error is received network certificate is not configured for TPP which invokes API

    * def info = karate.info

    Given def Application_Details = active_tpp.AISP
   # And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details.grant_type = "authorization_code"
    #And set Application_Details.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details.code = UIResult.code
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401


  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether auth code is expired if not consumed in 60 secs

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And print "Wait starts"
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(45000)
    And print "wait ends"
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400


  @AISP_Error_Tech
  @severity=normal
  @Regression
  @test_test
  Scenario: Verify whether auth code is not expired if consumed before 60 secs

    * def info = karate.info
    * set info.subset = 'Auth code validation'

    Given set Application_Details $.grant_type = "authorization_code"
    #And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And print "Wait starts"
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(35000)
    And print "wait ends"
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200


