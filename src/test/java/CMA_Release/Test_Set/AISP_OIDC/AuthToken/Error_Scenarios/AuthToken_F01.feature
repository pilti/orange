@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth

Feature: Client Credentials validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')

    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path
    * call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.redirect-uri = Application_Details.redirect_uri
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * set Result.RequestObjectInput = ReqObjIP
    * set Result.ConsentURL = consenturl
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def profile = user_details.Generic.G_User_1
    * def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * def data = {"usr":'#(profile.user)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * def perform = Functions.selectAllAccounts(webApp,data)
    * set UIResult.AccountSelectionPage = perform
    * set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts
    * def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    * set Application_Details.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform
    * set Application_Details.code = perform.Action.Authcode
    * set UIResult.code = perform.Action.Authcode
    * def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    #And def screenshot = capturescreen.captureScreenShot2(webApp)
    #And def stop = webApp.stop()

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  @AISP_Error_Bus
  @severity=normal

  Scenario Outline: Validate CID matches with client network certificate-Invalid or empty client_id as #key#

    * def info = karate.info
    * def key = <client_id>
    * set info.subset = 'Client Id validation'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect-uri
    #Input invalid client_id in API request
    And set Application_Details $.client_id = <client_id>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401

    Examples:
      | client_id |
      | 'abcdef'  |
      |' '        |



  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-client_id associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client_id validation - TPP'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect-uri
      #Input client_id associated with different TPP in API request
    And set Application_Details $.client_id = active_tpp.<TPP>.client_id
    When call apiApp.Access_Token_ACG(Application_Details)
    And match apiApp.Access_Token_ACG.responseStatus == 401

    Examples:
      | TPP  |
      | AISP |
      | PISP |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate Client secret matches with client network certificate-Invalid or empty client_secret as #key#

    * def info = karate.info
    * def key = <client_secret>
    * set info.subset = 'Client_secret validation'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    #Input invalid client_secret in API request
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401


    Examples:
      | client_secret |
      | 'abcdef'      |
      | '   '         |

  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate Client secret matches with client network certificate-client_secret associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client_secret validation - TPP'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
        #Input client_secret associated with different TPP in API request
    And set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401

    Examples:
      | TPP  |
      | AISP |
      | PISP |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate client credentials match with client network certificate - Invalid or empty client_credentials as #key#

    * def info = karate.info
    * def key = <client_id> + <client_secret>
    * set info.subset = 'Client credentials validation'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
        #Input invalid client credentials in API request
    And set Application_Details $.client_id = <client_id>
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401

    Examples:
      | client_id | client_secret |
      | 'xyz'     | 'abcdef'      |
      | '  '      | '   '         |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  @Regression
  Scenario Outline: Validate Client credentials match with client network certificate-client credentials associated with TPP having #key# role

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client credentials validation - TPP'

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    #Input client credentials associated with different TPP in API request
    And set Application_Details $.client_id = active_tpp.<TPP>.client_id
    And set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 401

    Examples:
      | TPP  |
      | AISP |
      | PISP |



  @AISP_Error_Tech
  @severity=normal
  @Regression
  Scenario: Verify error is received when Authorization header is not passed in the request

    * def info = karate.info

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.request_body = body
    #Input client credentials associated with different TPP in API request
    And url TokenUrl
   # Do not pass client Authentication in API request
    #And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 401


