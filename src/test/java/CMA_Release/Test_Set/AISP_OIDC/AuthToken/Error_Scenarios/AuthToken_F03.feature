@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth


Feature: grant_type validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature")
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path
    * call apiApp.CreateRequestObject(Application_Details)
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    #* set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    * call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * set Result.RequestObjectInput = ReqObjIP
    * set Result.ConsentURL = consenturl
    * def c = call  webApp.driver1 = webApp.start1(default_browser)
    * def perform = Functions.launchConsentURL(webApp,consenturl)
    * match perform.Status == 'Pass'
    * def perform = Functions.useKeyCodeApp(webApp)
    * def data = {"usr":'#(user_details.Generic.G_User_1.usr)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    * def perform = Functions.scaLogin(webApp,data)
    * def perform = Functions.selectAllAccounts(webApp,data)
    * set UIResult.AccountSelectionPage = perform
    * set data $.SelectedAccounts = perform.SelectedAccounts
    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts
    * def perform = Functions.reviewConfirm(webApp,data)
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    #And set Application_Details.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform
    * set Application_Details.code = perform.Action.Authcode
    * set UIResult.code = perform.Action.Authcode
    * def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    * def screenshot = capturescreen.captureScreenShot2(webApp)
    #And def stop = webApp.stop()

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);
       webApp.stop();
       }
      """



  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal

  Scenario Outline: Verify whether error is received when invalid grant_type #key# is sent in API request

    * def info = karate.info
    * def key = <grant_type>
    * set info.subset = 'grant_type validation'
     #Input invalid grant_type in API request

    Given set Application_Details $.grant_type = <grant_type>
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | grant_type              |
      | 'authorization_code123' |
      | 'Authorization_code'    |


  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when grant_type value is not provided in API request

    * def info = karate.info
    * set info.subset = 'Empty grant_type'
  #Input empty grant_type value in API request
    Given set Application_Details $.grant_type = <grant_type>
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | grant_type |
      | ' '        |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Verify whether error is received when valid but not allowed grant_type #key# value is sent in API request

    * def info = karate.info
    * def key = <grant_type>
    * set info.subset = 'Not allowed grant_type'
   #Input not allowed grant_type value in API request

    Given set Application_Details $.grant_type = <grant_type>
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400

    Examples:
      | grant_type      |
      | 'refresh_token' |


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Tech
  @severity=normal
  Scenario: Verify whether error is received when mandatory parameter grant_type is missing in API request

    * def info = karate.info
    #Remove grant_type from API request

    Given set Application_Details $.grant_type = null
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 400


  @AISP_UAT
  @AISP_UAT_API
  @AISP_Error_Bus
  @severity=normal

  Scenario: Verify whether error is received when valid but not allowed grant_type client_credentials value is sent in API request

    * def info = karate.info
    * set info.subset = 'abcdef'
  #Input not allowed grant_type value as client_credentials in API request

    Given set Application_Details $.grant_type = 'client_credentials'
    #And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
    And set Application_Details $.redirect-uri =  Application_Details.redirect_uri
    And set Application_Details $.code = UIResult.code
    And set Application_Details $.scope = 'openid accounts'
    And def body = "grant_type=" + Application_Details.grant_type + "&code=" + UIResult.code + "&redirect_uri=https://www.getpostman.com/oauth2/callback" + "&scope=" + Application_Details.scope
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And set Application_Details $.request_body = body
    And set Application_Details $.Authorization = auth
  #call Access_token using Authorization code grant API
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request body
    When method POST
    Then match responseStatus == 200

    Given set Application_Details.access_token = response.access_token
    And set Application_Details.token_type = response.token_type
    And set Application_Details.x_fapi_financial_id = "0015800000jfQ9aAAe"
    And def apiApp1 = new apiapp()
    When call apiApp1.Account_Request_Setup(Application_Details)
    Then match apiApp1.Account_Request_Setup.responseStatus == 201



