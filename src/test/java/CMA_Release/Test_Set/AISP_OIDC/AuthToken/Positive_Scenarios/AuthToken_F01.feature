@AISP
@AISP_Consent
@AISP_AccessTokenUsingOAuth
@Regression

Feature: Client Credentials validation for Access_token using auth code API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
     """


  Scenario: AuthToken_Verify that access token can be generated using the code in the redirect url
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC002_AuthToken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path


    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    Then match callE2E.Result.Access_Token_ACG.response contains {"access_token": '#string'}

  Scenario: Verify that Status code 200 is displayed in the response when valid info is provided in the API request and Access token and refresh token is generated
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC001_AuthToken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    Then match callE2E.Result.Access_Token_ACG.responseStatus == 200


  Scenario: Verify that Access token generated is valid for 5 mins
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC001_AuthToken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
      #Perform Assertion for Token expiration check
    And def Expiry = callE2E.Result.Access_Token_ACG.response
    And def val = (Expiry.expires_in == 300 || Expiry.expires_in == 299 || Expiry.expires_in == 298 ? "True" : "False" )
    Then match val == "True"
    And def callwait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(240000)
    Given set Application_Details.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
