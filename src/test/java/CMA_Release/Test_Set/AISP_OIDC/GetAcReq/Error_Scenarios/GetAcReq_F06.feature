@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for Validations around AccountRequestId.

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json UIResult = {}
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """


  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed when passed #key# in Account Request Id


    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Account Request Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = <AccountRequestId>
    And def AccountRequestId = <AccountRequestId>
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition                | AccountRequestId                             | Expected |
      | 'Special Character'      | 'QWE!@'                                      | 400      |
      | 'Length greater than 40' | '1223232qddddddasddsfsfsdfsdfsdfsfsfsfsnn58' | 400      |
      | 'Null'                   | 'null'                                       | 400      |





  @severity=normal
  Scenario Outline: Verify that Status code 405 is displayed when passed Account Request Id is Missing.


    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Account Request Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = <AccountRequestId>
    And def AccountRequestId = <AccountRequestId>
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | AccountRequestId | Expected |
      | ''               | 405      |



  @severity=normal
  Scenario: Verify that Resource status is displayed as REVOKED when passed Account Request Id which for which consent is Revoked.


    * def info = karate.info
    * set info.key = 'Yes'
    * set info.subset = 'Revoked Consent'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature") Application_Details


    Given set Application_Details.access_token = TestRes.apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = TestRes.apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'DELETE'
    And remove reqHeader.Accept
    And remove reqHeader.Content-Type
    And remove reqHeader.x-fapi-customer-last-logged-time
    And remove reqHeader.x-fapi-customer-ip-address
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204



#    Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.request_method = 'GET'
   #Call Account retrieve API
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    And match apiApp.Account_Request_Retrieve.response.Data.Status == 'Revoked'





  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when passed Valid AccountRequestID but not associated with TPP initiating the request #key#.


    * def key = <Srno>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'AccountRequestId of Different TPP'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    #Call PreAuth Reusable feature file to create Account request id
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

  #Call CCG and Account Request setup API to create Account request id of different TPP
    * def Application_Details1 = active_tpp.<Role1>
    Given call apiApp1.configureSSL(Application_Details1)
    And set Application_Details1.request_method = 'POST'
    And set Application_Details1 $.grant_type = 'client_credentials'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp1.Access_Token_CCG(Application_Details1)
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    Given set Application_Details1.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details1.token_type = apiApp1.Access_Token_CCG.response.token_type
    When call apiApp1.Account_Request_Setup(Application_Details1)
    Then match apiApp1.Account_Request_Setup.responseStatus == 201

    Given set Application_Details1.AccountRequestId = apiApp1.Account_Request_Setup.response.Data.AccountRequestId
      #Setup Input Parameter for Get Account Retrieve API
    And set Application_Details.access_token = TestRes.apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = TestRes.apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = apiApp1.Account_Request_Setup.response.Data.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Srno | Role      | Role1     | Expected |
      | '1'  | AISP_PISP | AISP      | 403      |
      | '2'  | AISP      | AISP_PISP | 403      |





  Scenario: Verify that REJECTED status is displayed when provided Valid AccountRequestID but request is rejected by customer during consent.

    * def info = karate.info
    * set info.key = 'Yes'
    * set info.subset = 'Consent Rejected'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given def Application_Details = active_tpp.AISP_PISP
    And set Result.Input.TPPRole = 'AISP_PISP'
    And set Result.Input.scope = Application_Details.scope
    And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(600)
    #Call PreAuth Reusable feature file to create Account request id
    And def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And set Result.CreateRequestObject.URL = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Application_Details $.consenturl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'

    Given def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'


    Given def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    Then match perform.Status == 'Pass'


    Given def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'


    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set data.action = "cancel"
    And def perform = Functions.reviewConfirm(webApp,data)
    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    And set Application_Details.Permissions = perform.Permissions
    And set Application_Details.code = perform.Action.Authcode
    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    #And def screenshot = capturescreen.captureScreenShot2(webApp)
    And def stop = webApp.stop()
   #Invoke GET AccountRetrieve API for Rejected Account Request ID
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    And match apiApp.Account_Request_Retrieve.response.Data.Status == "Rejected"




