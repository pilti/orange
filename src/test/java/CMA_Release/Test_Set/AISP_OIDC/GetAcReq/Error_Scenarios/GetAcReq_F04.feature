@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for Invalid Financial Id and Invalid Accept Header.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """


  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when passed #key#

    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Financial Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And set reqHeader $.x-fapi-financial-id = 'OB/BOI/002'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition              | Financial Id | Role      | Expected |
      | 'Invalid Financial Id' | 'OB/BOI/002' | AISP_PISP | 403      |
      | 'Empty Financial Id'   | ''           | AISP      | 403      |



  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed when Financial Id is missing for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Financial Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And remove reqHeader.x-fapi-financial-id
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP_PISP | 400      |
      | AISP      | 400      |



  @severity=normal
  Scenario Outline: Verify that Status code 406 is displayed when passed Invalid Accept Header for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Accept Header'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And set reqHeader $.Accept = <Accept>
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Accept  | Expected |
      | AISP_PISP | 'abcd'  | 406      |
      | AISP      | '@#$$%' | 406      |



  @severity=normal
  Scenario Outline: Verify that Status code 200 is displayed when passed Blank Accept Header for TPP Role #key#


    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Blank Accept Header'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And set reqHeader $.Accept = <Accept>
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Accept | Expected |
      | AISP      | ''     | 200      |
      | AISP_PISP | ''     | 200      |



  @severity=normal
  Scenario Outline: Verify that Status code 200 is displayed when Accept Header is Missing for TPP Role #key#


    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Accept Header'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And remove reqHeader.Accept
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP      | 200      |
      | AISP_PISP | 200      |




