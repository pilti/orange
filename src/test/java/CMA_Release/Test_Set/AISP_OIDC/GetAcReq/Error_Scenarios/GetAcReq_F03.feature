@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for GET Account Request API when Access token having Invalid scope.

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """



  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when passed Access Token having Invalid scope like #key#

    * def key = '<Condition>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Access Token having Invalid scope'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    Given def Application_Details1 = active_tpp.<Role1>
    And set Application_Details1 $.scope = <scope>
    And set Application_Details1.request_method = 'POST'
    And set Application_Details1 $.grant_type = 'client_credentials'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp1.Access_Token_CCG(Application_Details1)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp1.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition                               | Role      | Role1     | scope             | Expected |
      | Access token with scope openid payments | AISP_PISP | AISP_PISP | 'openid payments' | 403      |
      | Access token with scope openid          | AISP_PISP | AISP_PISP | 'openid'          | 403      |
      | Access token with scope Blank           | AISP_PISP | AISP_PISP | ''                | 403      |




  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when passed Access Token having Invalid scope of Different TPP like #key#

    * def key = '<Condition>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Access Token having Invalid scope of Different TPP'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given def Application_Details1 = active_tpp.<Role1>
    And set Application_Details1 $.scope = <scope>
    And set Application_Details1.request_method = 'POST'
    And set Application_Details1 $.grant_type = 'client_credentials'
    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp1.Access_Token_CCG(Application_Details1)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp1.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition                                          | Role      | Role1     | scope             | Expected |
      | Access token with scope null                       | AISP_PISP | AISP_PISP | 'null'            | 401      |
      | Access token with PISP TPP & scope openid payments | AISP_PISP | PISP      | 'openid payments' | 401      |
      | Access token with AISP TPP & scope openid accounts | AISP_PISP | AISP      | 'openid accounts' | 401      |
      | Access token with AISP PISP TPP & scope payments   | AISP      | AISP_PISP | 'payments'        | 401      |
      | Access token with AISP TPP & scope accounts        | AISP_PISP | AISP      | 'accounts'        | 401      |


