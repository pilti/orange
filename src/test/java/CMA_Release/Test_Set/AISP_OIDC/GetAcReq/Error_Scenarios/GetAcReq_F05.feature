@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for Invalid Token type ,Invalid Method and URI validation.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """



  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when passed #key#

    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Token Type'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = <type>
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition                                                                              | type                   | Role      | Expected |
      | 'Missing token type'                                                                   | " "                    | AISP_PISP | 401      |
      | 'Misspelled token type append single extra char to Bearer'                             | "Bearerr"              | AISP_PISP | 401      |
      | 'Invalid token type- append multiple chars to Bearer'                                  | "Bearerdfgdfhdh"       | AISP_PISP | 401      |
      | 'Invalid token type- append multiple aphanumeric chars to Bearer'                      | "Bearer5757"           | AISP_PISP | 401      |
      | 'Invalid token type- append multiple special chars to Bearer'                          | "Bearer$%&%4gd"        | AISP      | 401      |
      | 'Invalid token type- placed multiple alphanumeric chars in between Bearer'             | "Beare577dgdfr"        | AISP      | 401      |
      | 'Invalid token type- placed multiple alphanumeric and special chars in between Bearer' | "Bear%^*577dfgfddgdfr" | AISP      | 401      |
      | 'Lower case token type'                                                                | "bearer"               | AISP      | 401      |
      | 'Token type other than Bearer '                                                        | "Basic"                | AISP      | 401      |




  @severity=normal
  Scenario Outline: Verify that Status code 404 is displayed when passed Invalid URI #key#

    * def key = <key>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid URI'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details



    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details.request_method = 'GET'
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    And set Application_Details.AcctReqUrl = tokenurl + "/" + AccountRequestId
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | key  | TokenUrl_SIT                                                        | TokenUrl_UAT                                                                | Expected |
      | '1'  | https://api.boitest.net/1/api/open-banking/v1.1/Account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Account-requests    | 404      |
      | '2'  | https://api.boitest.net/1/api/open-banking/v1.1/accountrequests     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountrequests     | 404      |
      | '3'  | https://api.boitest.net/1/api/open-banking/v1.1/account-request     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-request     | 404      |
      | '4'  | https://api.boitest.net/1/api/open-banking/v1.1//account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//account-requests   | 404      |
      | '5'  | https://api.boitest.net/1/api/open-banking/v1.0/account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.0/account-requests    | 404      |
      | '6'  | https://api.boitest.net/1/api/open-banking//v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/account-requests   | 404      |
      | '7'  | https://api.boitest.net/1/api//open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/account-requests   | 404      |
      | '8'  | https://api.boitest.net/1/api/v1.1/account-requests                 | https://api.u2.psd2.boitest.net/1/api/v1.1/account-requests                 | 404      |
      | '9'  | https://api.boitest.net/1//api/open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/account-requests   | 404      |
      | '10' | https://api.boitest.net/1//open-banking/v1.1/account-requests       | https://api.u2.psd2.boitest.net/1//open-banking/v1.1/account-requests       | 404      |
      | '11' | https://api.boitest.net/1/api/open-banking//account-requests        | https://api.u2.psd2.boitest.net/1/api/open-banking//account-requests        | 404      |
      | '12' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests/// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests/// | 404      |





  @severity=normal
  Scenario Outline: Verify that Status code 200 is displayed when passed Invalid URI with extra slash #key#


    * def key = <key>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid URI with extra slash'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details



    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details.request_method = 'GET'
    And def tokenurl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    And set Application_Details.AcctReqUrl = tokenurl + "/" + AccountRequestId
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | key | TokenUrl_SIT                                                       | TokenUrl_UAT                                                               | Expected |
      | '1' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests/  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests/  | 201      |
      | '2' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests// | 201      |
      | '3' | https://api.boitest.net//1/api/open-banking/v1.1/account-requests/ | https://api.u2.psd2.boitest.net//1/api/open-banking/v1.1/account-requests/ | 201      |



  @severity=normal
  Scenario Outline: Verify that Status code 405 is displayed when API is triggered with #key# method


    * def key = <Method>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Method Validation'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPRole = 'AISP_PISP'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = <Method>
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 405



    Examples:
      | Method     |
     # | 'GET'      |
     # | 'DELETE'   |
      | 'COPY'     |
      | 'LINK'     |
      | 'UNLINK'   |
     # | 'PUT'  |
      | 'PURGE'    |
      | 'PROPFIND' |
      | 'OPTIONS'  |
      | 'LOCK'     |
      | 'UNLOCK'   |
      | 'HEAD'     |
     # | 'PATCH'    |
      | 'VIEW'     |







