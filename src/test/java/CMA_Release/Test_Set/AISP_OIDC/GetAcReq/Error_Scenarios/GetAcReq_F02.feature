@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for GET Account Request API on Authorisation parameter Validation.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """



  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when passed invalid Access Token for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Access Token'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
     #Call PreAuth Reusable feature file
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = <AccessToken>
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | AccessToken | Expected |
      | AISP_PISP | 'abcd'      | 401      |
      | AISP      | '#$$$'      | 401      |
      | AISP      | ''          | 401      |




  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when passed Expired Access Token for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Expired Access Token'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
     #Call PreAuth Reusable feature file
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
      #Wait for 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP_PISP | 401      |
      | AISP      | 401      |




  @severity=normal
  Scenario Outline: Verify that Status code 200 is displayed when Access Token is about to Expire for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Access Token about to Expire'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
      #Wait for less than 5 min
    And def msg = "Wait Applied to expire the token"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(290000)
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP_PISP | 200      |
      | AISP      | 200      |





  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when API is triggered with access_token having refresh_token grant for TPP Role #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Use Refresh token in Access token'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Call PreAuth Reusable feature file
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

  #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.access_token = profile.refresh_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP_PISP | 401      |
      | AISP      | 401      |

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when API is triggered with access_token having Authorization_code grant

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Use Access token having refresh_Token grant'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Call PreAuth Reusable feature file
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

  #Set Input Parameter for RTG Api
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'GET'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200


    #Setup Input Parameter for Get Account Retrieve API
    Given set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>

    * print Result


    Examples:
      | Role      | Expected |
      | AISP_PISP | 401      |
      | AISP      | 401      |




  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when API is triggered without authorization parameter

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Authorisation Parameter'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
     #Call PreAuth Reusable feature file
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details
    * set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    * def AccountRequestId = Application_Details.AccountRequestId

    Given url AcctReqUrl + "/" + AccountRequestId
    And headers reqHeader
    When method GET
    Then match responseStatus == <Expected>




    Examples:
      | Role      | Expected |
      | AISP_PISP | 401      |
      | AISP      | 401      |
