@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error conditions for GET Account Request API when Invalid Client Credential are passed.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """


  @Regression
  @severity=normal
  Scenario Outline: Verify that Status code 200 is displayed when passed invalid Client Credentials like #key#
    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Client Credentials'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def Application_Details = active_tpp.<Role>
    * set Result.Input.TPPRole = '<Role>'
    * set Result.Input.scope = Application_Details.scope
    * def TestRes = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    #Setup Input Parametre for Get Account Retrieve API
    Given set Application_Details.client_id = <ClientId>
    And set Application_Details.client_secret = <ClientSecret>
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.AccountRequestId = TestRes.Application_Details.AccountRequestId
    And def AccountRequestId = Application_Details.AccountRequestId
    And set Application_Details $.AcctReqUrl = AcctReqUrl
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == <Expected>




    Examples:
      | Condition                                       | Role      | DifferentTPP | ClientId                       | ClientSecret                  | Expected |
      | 'Client Credential of Different TPP - AISP'     | AISP_PISP | AISP         | active_tpp.AISP.client_id      | active_tpp.AISP.client_secret | 200      |
      | 'Client Credential of Different TPP - PISP'     | AISP_PISP | PISP         | active_tpp.PISP.client_id      | active_tpp.PISP.client_secret | 200      |
      | 'Client Credential of Different TPP - AISPPISP' | AISP      | AISP_PISP    | active_tpp.AISP_PISP.client_id | active_tpp.AISP_PISP.client_secret | 200      |
      | 'Invalid Client Credential'                     | AISP_PISP | AISP_PISP    | 'abcd'                         | '@#$&'                        | 200      |
      | 'Blank Client Credential'                       | AISP_PISP | AISP_PISP    | ''                             | ''                            | 200      |
      | 'Null Client Credential'                        | AISP_PISP | AISP_PISP    | 'Null'                         | 'Null'                        | 200      |



