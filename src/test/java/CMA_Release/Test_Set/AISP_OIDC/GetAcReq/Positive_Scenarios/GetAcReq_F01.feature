@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq
@Functional_Shakedown
@Regression
Feature: This feature is to demonstrate the scenario to check the resource status when consent is not completed.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """



  @severity=normal
  @Functional_Shakedown_Quick

  Scenario Outline: Verify that Resource Status is Awaiting Authorisation when consent is not completed for TPP Role #key#


    * def key = '<Role>'
    * def key1 = ' Resource Status'
    * def info = karate.info
    * set info.key = key
    * set info.subset =  key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path


    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = 'openid accounts'
    And set Result.Input.TPPRole = '<Role>'
    And set Result.Input.scope = Application_Details.scope
    And  call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200


    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId


    Given set Application_Details.request_method = 'GET'
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Retrieve.feature') Application_Details
    Then match TestRes.response.Data.Status == <Expected>






    Examples:
      | Scenario number | Role      | Expected                |
      | '001'           | AISP_PISP | 'AwaitingAuthorisation' |
      | '002'           | AISP      | 'AwaitingAuthorisation' |
