@AISP
@AISP_SIT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq_SIT
@AISP_GetAcReq_abc
@Regression
Feature: Check for ExpirationDateTime for create Account requests

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  @severity=normal
  Scenario Outline: Verify that responseStatus is 201 when create account request is made with POST Method and valid ExpirationDateTime specified


    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Valid_Expiration_Date_Time_" + <count>

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.ExpirationDateTime =  <DateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data.ExpirationDateTime == <DateTime>


    Examples:

      | count | DateTime                    |
      | 001   | "2019-04-19T14:00:00+01:00" |
      | 002   | "2019-06-19T14:00:00+01:00" |
      | 003   | "2020-04-19T14:00:00+01:00" |

  @AISP_API
  @severity=normal
  Scenario Outline: Verify that responseStatus is 400 when create account request is made with inValid ExpirationDateTime
    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Invalid_Expiration_Date_Time_" + <count>
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.ExpirationDateTime =  <DateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400

    Examples:

      | count | DateTime                  |
      | 001   | "2018-09-02T00:00:00.875" |
      | 002   | "2019-27-10T00:00:00.875" |
      | 003   | "27-10-2018T00:00:00.123" |

