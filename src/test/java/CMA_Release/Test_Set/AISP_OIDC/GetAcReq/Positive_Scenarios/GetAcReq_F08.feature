@AISP
@AISP_SIT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq_SIT
@AISP_GetAcReq
Feature: Check for TransactionFromDateTime for Create Account Request

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @AISP_API
  @severity=normal
  Scenario Outline: Verify that responseStatus is 201 when create account request is made with POST Method and Valid TransactionFromDateTime


    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Valid_Transaction_From_To_" + <count>

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.TransactionFromDateTime =  <TransactionFromDateTime>
    And set permissions $.Data.TransactionToDateTime = <TransactionToDateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Examples:

      | count | TransactionFromDateTime     | TransactionToDateTime       |
      | 001   | "2015-05-03T00:00:00+00:00" | "2017-12-03T00:00:00+00:00" |
      | 002   | "2018-01-01T00:00:00+00:00" | "2018-12-03T00:00:00+00:00" |
      | 003   | "2018-12-01T00:00:00+00:00" | "2019-06-03T00:00:00+00:00" |

  @AISP_API
  @severity=normal
  Scenario Outline: Verify that responseStatus is 400 when create account request is made with POST Method and inValid TransactionFromDateTime


    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "In-valid_Transaction_From_To_" + <count>
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.TransactionFromDateTime =  <TransactionFromDateTime>
    And set permissions $.Data.TransactionToDateTime =  <TransactionToDateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400


    Examples:
      | count | TransactionFromDateTime     | TransactionToDateTime       |
      | 001   | "2018-05-03T00:00:00+00:00" | "2015-05-03T00:00:00+00:00" |
      | 002   | "06-06-2017T00:00:00:875"   | "2015-05-03T00:00:00+00:00" |
