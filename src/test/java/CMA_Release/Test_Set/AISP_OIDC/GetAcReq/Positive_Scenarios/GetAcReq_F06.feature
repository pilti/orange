@AISP
@AISP_SIT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq_SIT
@AISP_GetAcReq

Feature: Check for Methods used for Create account requests

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @AISP_API
  Scenario Outline: Verify that responseStatus is 201 when create account request is made with valid Permissions combinations

    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Permission_Combination_" + <count>
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions.Data.Permissions = <Permission>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

  Examples:

  | count | Permission                                                                                                                      | expected                                                                                                                        |
  | 001   | ["ReadAccountsDetail"]                                                                                                          | ["ReadAccountsDetail","ReadAccountsBasic"]                                                                                      |
  | 002   | ["ReadAccountsBasic"]                                                                                                           | ["ReadAccountsBasic"]                                                                                                           |
  | 003   | ["ReadBalances"]                                                                                                                | ["ReadBalances"]                                                                                                                |
  | 004   | ["ReadBeneficiariesBasic"]                                                                                                      | ["ReadBeneficiariesBasic"]                                                                                                      |
  | 005   | ["ReadBeneficiariesDetail"]                                                                                                     | ["ReadBeneficiariesDetail","ReadBeneficiariesBasic"]                                                                            |
  | 006   | ["ReadDirectDebits"]                                                                                                            | ["ReadDirectDebits"]                                                                                                            |
  | 007   | ["ReadStandingOrdersBasic"]                                                                                                     | ["ReadStandingOrdersBasic"]                                                                                                     |
  | 008   | ["ReadStandingOrdersDetail"]                                                                                                    | ["ReadStandingOrdersDetail","ReadStandingOrdersBasic"]                                                                          |
  | 009   | ["ReadTransactionsBasic","ReadTransactionsCredits"]                                                                             | ["ReadTransactionsBasic","ReadTransactionsCredits"]                                                                             |
  | 010   | ["ReadTransactionsDetail","ReadTransactionsDebits"]                                                                             | ["ReadTransactionsDetail","ReadTransactionsDebits","ReadTransactionsBasic"]                                                     |
  | 011   | ["ReadProducts"]                                                                                                                | ["ReadProducts"]                                                                                                                |
  | 012   | ["ReadAccountsBasic","ReadAccountsDetail","ReadBalances","ReadBeneficiariesBasic","ReadBeneficiariesDetail","ReadDirectDebits"] | ["ReadAccountsBasic","ReadAccountsDetail","ReadBalances","ReadBeneficiariesBasic","ReadBeneficiariesDetail","ReadDirectDebits"] |

