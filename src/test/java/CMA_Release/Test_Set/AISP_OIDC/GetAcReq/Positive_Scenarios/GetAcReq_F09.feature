@AISP
@AISP_SIT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq_SIT
@AISP_GetAcReq
@Regression
Feature: Check for TransactionToDateTime for Create Account Request

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @AISP_API
  @severity=normal

  Scenario Outline: Verify that responseStatus is 201 when create account request is made with POST Method and Valid TransactionToDateTime

    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Valid_Transaction_To_" + <count>


    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)
    #Set input with the request Method
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.TransactionFromDateTime =  <TransactionFromDateTime>
    And set permissions $.Data.TransactionToDateTime = <TransactionToDateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Examples:

      | count | TransactionToDateTime       | TransactionFromDateTime     |
      | 001   | "2017-05-02T00:00:00+00:00" | "2016-05-02T00:00:00+00:00" |
  @AISP_API
  @severity=normal

  Scenario Outline: Verify that responseStatus is 400 when create account request is made with POST Method and inValid TransactionToDateTime specified

     * def info = karate.info
     * set info.key = <count>
    * set info.subset = "In-valid_Transaction_To_" + <count>

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set permissions $.Data.TransactionToDateTime =  <TransactionToDateTime>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400


    Examples:
      | count | TransactionToDateTime     |

      | 001   | "06-06-2017T00:00:00:875" |
      | 002   | "00-00-0000T00:00:00:000" |
      | 003   | "aa-aa-aaaaxaa:aa:aa:aaa" |