@AISP
@AISP_SIT
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_GetAcReq_SIT
@AISP_GetAcReq
@Regression
Feature: Check for Account-request and Authorization status for Create

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  @severity=normal

  Scenario: Verify that responseStatus is 201 and Account request id is generated having length less than 40

    * def info = karate.info
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And assert apiApp.Account_Request_Setup.response.Data.AccountRequestId.length() < 40


  @AISP_API
  @severity=normal

  Scenario Outline: Verify AccountRequestId does not have special characcters #key#

    * def info = karate.info
    * def key = <count>
    * set info.key = <count>
    * set info.subset = "Special_Char_Validation_" + <count>
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And assert <Condition>

    Examples:
      | count | Condition                                                                          |
      | 001   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('#') == false |
      | 002   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('`') == false |
      | 003   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('!') == false |
      | 004   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('@') == false |
      | 005   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('$') == false |
      | 006   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('%') == false |
      | 007   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('^') == false |
      | 008   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('&') == false |
      | 009   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('*') == false |
      | 010   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('(') == false |
      | 011   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains(')') == false |
      | 012   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('+') == false |
      | 013   | apiApp.Account_Request_Setup.response.Data.AccountRequestId.contains('=') == false |

  @AISP_API
  @severity=normal
  Scenario: Verify Status of intent id

    * def info = karate.info
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data.Status == 'AwaitingAuthorisation'

  @AISP_API
  @severity=normal
  Scenario Outline: Test to verify interaction id returned in the response as per the input


    * def info = karate.info
    * set info.key = <count>
    * set info.subset = "Interaction_Id_NOT_Passed"
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)
    #Set input with the request Method
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"
    And set reqHeader $.x-fapi-interaction-id = <x-fapi-interaction-id>
    And <condition>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match <expected> == apiApp.Account_Request_Setup.responseHeaders['x-fapi-interaction-id'][0]



    Examples:
      | count | x-fapi-interaction-id | expected                                                                 | condition                                |
      #| 001   | 'Pass123'             | "Pass123"                                                                | def x = "0"                              |
      | 002   | ''                    | apiApp.Account_Request_Setup.responseHeaders['x-fapi-interaction-id'][0] | remove reqHeader $.x-fapi-interaction-id |