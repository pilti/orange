@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation


Feature: This feature is to demonstrate successful creation of consent url by Parameterizing intentID and signing key

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * json Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    * def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id

    * configure afterScenario =
    """
    function(){
    var tem = karate.info;
    if (typeof key != 'undefined'){info.key = key}
    if(tem.errorMessage == null){
        Result.TestStatus = "Pass";
    }else{
        Result.TestStatus = "Fail";
        Result.Error = tem.errorMessage;
    }
    apiApp.write(Result,info);
    webApp.stop();
      }
    """


  @Regression
  Scenario Outline: Verify that Consent URL is successfully launched when correct intend id is passed
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = 'Yes'
    * set info.subset = 'Correct Intent Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = <intent_id>
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = <intent_id>
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And set Application_Details.path = path
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page


    Examples:
      | intent_id                                                   |
      | apiApp.Account_Request_Setup.response.Data.AccountRequestId |


 #########################################################################################################################################
  @Regression
  Scenario Outline: Verify that Consent URL is successfully launched when correct network key is passed
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = 'Yes'
    * set info.subset = 'Correct Network key'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details.path = <singing_key_path>
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And set Application_Details.path = path
    #############################Launch consent URL##################################################################################
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Examples:
      | singing_key_path                                     |
      | stmtpath(Application_Details) + 'signin_private.key' |

