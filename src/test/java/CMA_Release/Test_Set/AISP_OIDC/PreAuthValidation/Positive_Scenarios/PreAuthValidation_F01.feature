@AISP
@AISP_UAT
@AISP_Consent
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@Functional_Shakedown
@Regression

Feature: This feature is to demonstrate the positive flow for generation of refresh and access token using the code in the redirect url

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * json Application_Details = active_tpp.AISP_PISP
    * set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    * configure afterScenario =
    """
    function(){
    var tem = karate.info;
    if (typeof key != 'undefined'){info.key = key}
      if(tem.errorMessage == null){
        Result.TestStatus = "Pass";
      }else{
        Result.TestStatus = "Fail";
        Result.Error = tem.errorMessage;
      }
    apiApp.write(Result,info);
    webApp.stop();
    }
   """


   #####################################Background End####################################################################################################
  @severity=normal
  Scenario: Verify that code and id_token are obtained in consent creation

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Code & idtoken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
     #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    Given def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)
    And set Application_Details.path = path
    And set Result.ActualOutput.UI.Output.ReviewConfirm_Permissions = perform.Permissions
    And eval if (perform.Authcode != '' &&  perform.idtoken!='' ) {perform.Status = "Pass"} else {perform.Status = "Fail"}
    And set Result.Status = perform.Status
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Consent.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.idtoken = perform.Action.idtoken
    And set Result.Additional_Details = Application_Details
    And set Result.Additional_Details = TestRes.Application_Details


  ######################################################################################################################################################

  Scenario: Verify that access token can be generated using the code in the redirect url
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Generate Access Token using Authcode'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
   #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    Given def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    When def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    And def perform = Functions.reviewConfirm(webApp,data)
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Application_Details.path = path
    And set Application_Details $.grant_type = 'authorization_code'
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.idtoken = perform.Action.idtoken
     #Call Access_Token_ACG API
    When call apiApp.Access_Token_ACG(Application_Details)
    And eval if (apiApp.Access_Token_ACG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    Then match apiApp.Access_Token_ACG.responseStatus == 200


  #################################################################################################################################

  Scenario: Verify that refresh token can be generated using the code in the redirect url

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Generate Refresh Token using Authcode'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details
  #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    Given def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    When def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Application_Details.path = path
    And set Application_Details.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.idtoken = perform.Action.idtoken
    And set Application_Details $.grant_type = 'authorization_code'
    And set Application_Details $.redirect_uri = 'https://boi.com'
      #Call Access_Token_ACG API
    When call apiApp.Access_Token_ACG(Application_Details)
    And eval if (apiApp.Access_Token_ACG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}
    Then match apiApp.Access_Token_ACG.responseStatus == 200

    Given set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.grant_type = 'refresh_token'
      #Call Refresh_Token_RTG API
    When call apiApp.Access_Token_RTG(Application_Details)
    And eval if (apiApp.Access_Token_RTG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200

