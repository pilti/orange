@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error conditions for Invalid combination of valid IntentID wrt scope in Request Object.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
        webApp.stop();
       }
      """


  #################################################################################################################################################################
  @Defect_1344
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Invalid combination of IntentID and scope is provided and value of TPP , CCG scope and JWT scope is #key#

    * def key = '<Role>' + ' , ' + <CCGscope> +' & '+ <JWTscope>
    Given def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid IntentId wrt scope in JWT_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

  #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <CCGscope>

    And set Result.Input.CCG_TPPRole = '<Role>'
    And set Result.Input.CCG_scope = <CCGscope>

     #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

     #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id

    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    And set ReqObjIP $.payload.scope = <JWTscope>
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input.scope = <JWTscope>
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

      #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

     #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <AuthReqscope>

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.Input.scope = <AuthReqscope>
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    And set Result.ActualOutput.Reference = 'Defect_1344'
    And set Result.Additional_Details = Application_Details

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    * match perform.ActualUrl == <Expected>

    * print Result


    Examples:
      | Scenario number | Role      | CCGscope                   | JWTscope                   | AuthReqscope      | Expected                                                                                                                                                                      |
      | '001'           | AISP_PISP | 'openid accounts payments' | 'openid accounts payments' | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
      | '002'           | AISP_PISP | 'openid accounts payments' | 'openid payments'          | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'                                                           |
      | '003'           | AISP      | 'openid accounts'          | 'openid payments'          | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | '004'           | AISP      | 'openid accounts'          | 'accounts'                 | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
      | '005'           | AISP      | 'openid accounts'          | 'openid'                   | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
      | '006'           | AISP      | 'openid accounts'          | ''                         | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
      | '007'           | AISP      | 'openid accounts'          | 'null'                     | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
      | '008'           | AISP      | 'openid accounts'          | 'abcd'                     | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |



#################################################################################################################################################################
  @Defect_1267
  @severity=normal
  Scenario Outline: Verify that BOI API Portal screen is displayed on UI Browser when Invalid combination of IntentID and scope is provided and value of TPP ,JWT scope and AuthReq scope is #key#

    * def key = '<Role>' + ' '+ <JWTscope> +' & '+ <AuthReqscope>
    Given def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid IntentId wrt scope in Authorise Request_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <CCGscope>

    And set Result.Input.CCG_TPPRole = '<Role>'
    And set Result.Input.CCG_scope = <CCGscope>

    #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

    #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    And set ReqObjIP $.payload.scope = <JWTscope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

     #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <AuthReqscope>

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.Input.scope = <AuthReqscope>
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    And set Result.ActualOutput.Reference = 'Defect_1267'
    And set Result.Additional_Details = Application_Details

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    * print Result

    Examples:
      | Scenario number | Role      | CCGscope                   | JWTscope          | AuthReqscope               |
      | '001'           | AISP_PISP | 'openid accounts payments' | 'openid accounts' | 'openid accounts payments' |
      | '002'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid payments'          |
      | '003'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid'                   |
      | '004'           | AISP      | 'openid accounts'          | 'openid accounts' | ''                         |
      | '005'           | AISP      | 'openid accounts'          | 'openid accounts' | 'null'                     |
      | '006'           | AISP      | 'openid accounts'          | 'openid accounts' | 'abcd'                     |
#
#    Examples:
#      | Scenario number | Role      | CCGscope                   | JWTscope          | AuthReqscope               | Expected                                                                                                                                                                      |
#      | '001'           | AISP_PISP | 'openid accounts payments' | 'openid accounts' | 'openid accounts payments' | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
#      | '002'           | AISP_PISP | 'openid accounts payments' | 'openid accounts' | 'openid payments'          | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'                                                           |
#      | '003'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid payments'          | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
#      | '004'           | AISP      | 'openid accounts'          | 'openid accounts' | 'accounts'                 | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
#      | '005'           | AISP      | 'openid accounts'          | 'openid accounts' | 'openid'                   | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
#      | '006'           | AISP      | 'openid accounts'          | 'openid accounts' | ''                         | 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'                                   |
#      | '007'           | AISP      | 'openid accounts'          | 'openid accounts' | 'null'                     | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |
#      | '008'           | AISP      | 'openid accounts'          | 'openid accounts' | 'abcd'                     | 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope' |


