@AISP1
@AISP_UAT1
@AISP_UAT_OIDC1
@AISP_PreAuthValidation1
@AISP_Error_Bus1

Feature: This feature is to demonstrate the Error condition when requested intentID has Revoked status in Request Object.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
        webApp.stop();
       }
      """


  #################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify TPP Redirect URL is displayed on UI Browser when Requested Intent Id has Revoked Status for TPP Role #key#

    * def key = '<Role>'
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset =  'Construct URL with Revoked IntentID_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

 #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

    And set Result.Input.CCG_TPPRole = '<Role>'
    And set Result.Input.CCG_scope = <scope>

 #Call the Reusable feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

 #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)

 #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

   #Open Browser and Launch URL
    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(user_details.Generic.G_User_1.usr)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page

    Then def perform = Functions.selectAllAccounts(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    * set data $.SelectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts

    Then def perform = Functions.reviewConfirm(webApp,data)
    * print perform
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions

    * set Application_Details.path = path

    * set Application_Details.code = perform.Action.Authcode

    And set Result.ActualOutput.UI.Consent.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Consent.idtoken = perform.Action.idtoken

    * def stop = call  webApp.stop()

  #Call Access_Token_ACG API
    When call apiApp.Access_Token_ACG(Application_Details)

    And eval if (apiApp.Access_Token_ACG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}

    And set Result.ActualOutput.Access_Token_ACG.Input.Endpoint = apiApp.Access_Token_ACG.TokenUrl
    And set Result.ActualOutput.Access_Token_ACG.Output.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.ActualOutput.Access_Token_ACG.Output.response = apiApp.Access_Token_ACG.response

    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    And set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    * print Application_Details

#    #Set Input Parameter for RTG
#    When set Application_Details $.grant_type = "refresh_token"
#    And set Application_Details $.request_method = 'POST'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#    And set Application_Details $.refresh_token = Application_Details.refresh_token
#
#      #Call RTG API to generate access token
#    And call apiApp.Access_Token_RTG(Application_Details)
#
#    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
#    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response


     #Call Delete API to Revoke the Account Request id
    Given json Application_Details1 = {}
    And set Application_Details1 $.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details1 $.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details1 $.client_id = Application_Details.client_id
    And set Application_Details1 $.client_secret = Application_Details.client_secret
    And set Application_Details1 $.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And def AccountRequestId = Application_Details1.AccountRequestId
    And set Application_Details1 $.request_method = 'DELETE'
    #And def AcctReqUrl = AcctReqUrl+'/'+AccountRequestId
    And set Application_Details1 $.AcctReqUrl = AcctReqUrl

    * remove reqHeader.x-fapi-customer-ip-address
    * remove reqHeader.x-fapi-customer-last-logged-time
    * remove reqHeader.Content-Type
    * remove reqHeader.Accept


    * print "herre:"
    * print Application_Details1
    #When call apiApp.Account_Request_Delete(Application_Details)
    When def TestRes1 = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Request_Delete.feature') Application_Details1
    And set Result.ActualOutput.Account_Request_Delete.Output.responseStatus = apiApp.Account_Request_Delete.responseStatus
    And set Result.ActualOutput.Account_Request_Delete.Output.response = apiApp.Account_Request_Delete.response



#       #Call Delete API to revoke the consent
#    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
#    And set Application_Details.request_method = 'DELETE'
#    And set Application_Details.token_type = 'Bearer'
#    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
#    And def AccountRequestId = Application_Details.AccountRequestId
#
#    And call apiApp.Account_Request_Delete(Application_Details)

    #Launch URL
#    Then def c = call  webApp.driver1 = webApp.start1(default_browser)
#
#    Given def perform = Functions.launchConsentURL(webApp,consenturl)
#
#    Then match perform.Status == 'Fail'
#    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
#    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
#
#    Then def perform = Functions.getUrl(webApp)
#
#    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl
#
#    * match perform.ActualUrl == <Expected>
#
#    And set Result.Additional_Details = Application_Details
#
#    * def stop = call  webApp.stop()
#
#    * print Result


#    Given def perform = Functions.launchConsentURL(webApp,consenturl)
#    * match perform.Status == "Fail"
#    * match perform.reasonText == "Landing Page is not as expected!"
#
#    Then def perform = Functions.getUrl(webApp)
#    * print perform
#    * set Result.UIresult = perform
#    * def stop = call webApp.stop()
#    * match perform.ActualUrl == <Expected>
#    * print Result


    Examples:
      | Scenario number | Role      | scope             | Expected                                                                                                            |
      | '001'           | AISP_PISP | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |
      #| '002'           | AISP      | 'openid accounts' | 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error' |


