@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Bus


Feature: Parameter validation in request object creation

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * def Application_Details = active_tpp.AISP_PISP
    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details
    * configure afterScenario =
       """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

###############################################################################################################################################
  @Regression
  @Defect_1267
  @severity=normal
  Scenario: Verify that BOI API Portal screen is displayed on browser when different account request IDs passed in the two fields of openbaking_intent_id while request object creation

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'IntentId Mismatch in userinfo & idtoken'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    * def path = call  webApp.path1 = info1.path

   #Input parameter values to create request object
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
   #Test condition: Create new account request ID for same TPP which will be passed under id_token claim
    And call apiApp1.Account_Request_Setup(Application_Details)
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp1.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And set Result.ActualOutput.Reference = 'Defect_1267'
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page



#######################################################################################################################################################################################

  @Defect_1542
  @severity=normal
  Scenario: Verify that Error message is displayed on UI browser when client_id not associated with same TPP is passed in payload used for request object creation

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Client Id of Different TPP'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
     # Client_Id associated with different TPP will be passed in payload
    And set ReqObjIP $.payload.client_id = active_tpp.AISP.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And call apiApp.ConstructAuthReqUrl(Application_Details
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText
    And set Result.ActualOutput.Reference = 'Defect_1542'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    And match ActualText == ExpectedUIErrorMessage
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage



########################################################################################################################################

  @Defect_1542
  @severity=normal
  Scenario: Verify that Error Message is displayed on UI Browser when request object is created using a Signing key that is not of the same TPP for which the client_id is provided

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'Signing Certificate of Different TPP'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    * def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
     # Use Signing key from different TPP while creating request object
    And def Application_Details_dummy = active_tpp.AISP
    And def path = stmtpath(Application_Details_dummy) + 'signin_private.key'
    And set Application_Details.path = path
    And call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonTex
    And set Result.ActualOutput.Reference = 'Defect_1542'
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    And match ActualText == ExpectedUIErrorMessage
    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    And match ActualText == ExpectedUIErrorMessage


    ####################################################################################################################################

  @Defect_1267
  @severity=normal
  Scenario Outline: Verify that BOI API portal screen is displayed when scope is modified to #key# while creating the consent URL

    * def key = <scope>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <scope>
    * set info.subset = 'Scope validation in ConsentURL_'+<Scenario>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

     #Modify scope as openid while constructing consentURL
    And set Application_Details $.scope = <scope>

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Result.ActualOutput.ConstructAuthReqUrl.Input.scope = Application_Details.scope

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    * print ReqObjIP

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    And def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform

    Then match perform.Status == 'Pass'

    And set Result.ActualOutput.Reference = 'Defect_1267'
    And set Result.Additional_Details = Application_Details

    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    * print Result

    Examples:
      | Scenario | scope                      |
      | '001'    | 'openid'                   |
      | '002'    | 'abcdef'                   |
      | '003'    | 'openid accounts payments' |
      | '004'    | 'openid payments'          |
      | '005'    | 'accounts'                 |
      | '006'    | ''                         |



















