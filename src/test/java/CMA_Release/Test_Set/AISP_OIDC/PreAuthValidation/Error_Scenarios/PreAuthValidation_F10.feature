@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Bus


Feature: Parameter validation in request object creation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * def Application_Details = active_tpp.AISP_PISP

    And set Result.Input.CCG_TPPRole = 'AISP_PISP'
    And set Result.Input.CCG_scope = Application_Details.scope

    * def temp = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
      webApp.stop();
       }
      """

  @Defect_1542
  @severity=normal
  Scenario Outline: Verify that Error message is displayed on UI browser when response type as #key# is in the request object

    * def key = <response_type>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <response_type>
    * set info.subset = 'Invalid Response Type in JWT_'+<Scenario Number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

  # Input invalid or empty response_type while request object creation
    And set ReqObjIP $.payload.response_type = <response_type>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    And call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Defect_1542'

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = <Expected>
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    Examples:
      | Scenario Number | response_type | Expected                                                       |
      | '001'           | 'code'        | '400 - Authorization server can not verify the signed request' |
      | '002'           | 'id_token'    | '400 - Authorization server can not verify the signed request' |
      | '003'           | ' '           | '400 - Authorization server can not verify the signed request' |


    #####################################################################################################################################
  @Defect_1542
  @severity=normal
  Scenario Outline: Verify that Error message is displayed on UI browser when response type as #key# in passed in the query parameter

    * def key = <response_type>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <response_type>
    * set info.subset = 'Invalid Response Type in query parameter_'+<Scenario Number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri

    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    And call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
      # Input invalid or empty response_type while constructing consent URL
    And set Application_Details $.response_type = <response_type>
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.Input.response_type = <response_type>
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Defect_1542'

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def RT = "<response_type>"
    And def ExpectedUIErrorMessage = '400 - response_type ' + RT + ' is not allowed for client: ' + Application_Details.client_id
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    Examples:
      | Scenario Number | response_type |
      | '001'           | 'code'        |
      | '002'           | 'id_token'    |
      | '003'           | ' '           |


  ##################################################################################################################################

  @severity=normal
  Scenario: Verify that TPP Redirect URL is displayed on UI browser when requested response type is missing in the request object

    * def info = karate.info
    * set info.key = 'Yes'
    * set info.subset = 'Missing response type in JWT'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

     #Remove response_type parameter while creating object request
    And remove ReqObjIP.payload.response_type
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    And call apiApp.CreateRequestObject(Application_Details)
    * print ReqObjIP

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback?error_description=response_type+is+required.&state=af0ifjsldkj&error=invalid_request#.'

    * print Result


##############################################################################################################################################


  @Defect_1367
  @severity=normal
  Scenario: Verify Error message is displayed on UI browser when requested response type is missing in the query parameter

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = 'Yes'
    * set info.subset = 'Missing response type in Query Parameter'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    And call apiApp.CreateRequestObject(Application_Details)

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
      # Remove response_type from query parameter
    And set Application_Details $.response_type = 'null'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Result.ActualOutput.ConstructAuthReqUrl.input.response_type = Application_Details.response_type

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    And def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Defect_1367'


    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    #And def ExpectedUIErrorMessage = <Expected>
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
   # Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result
