@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Bus


Feature: This Feature demonstrates the Error Scenarios for PreAuthValidation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')
    * json Result = {}
    * set Result.Testname = null
    Given json Application_Details = active_tpp.AISP_PISP

    And set Result.Input.TPPRole = 'AISP_PISP'
    And set Result.Input.scope = Application_Details.scope

    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
      webApp.stop();
       }
      """


  ##################################################################################################################################

  Scenario Outline: Verify TPP Redirect URL is displayed when passed incorrect Intentid in idtoken and user launches the consent url

    Given def info = karate.info
    * set info.key = <key>
    * set info.subset = 'Incorrect IntentId in idtoken'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    #Set input parameters for CreateRequestObject
    * def key = <key>
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = <value>
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = <value1>
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)
    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)
    * print perform

    And set Result.Additional_Details = Application_Details

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    * match perform.ActualUrl == "https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error"

    * print Result

    Examples:
      | key   | value                                                       | value1               |
      | '001' | apiApp.Account_Request_Setup.response.Data.AccountRequestId | 'abciihioaeh2456465' |



   ##################################################################################################################################
  @Defect_1267
  Scenario Outline: Verify the BOI API Portal screen is displayed when passed incorrect intentid in userinfo and user launches the consent url

    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = <key>
    * set info.subset = 'Incorrect IntentId in userinfo'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

############################Set input parameters for CreateRequestObjec######################################################################
    * def key = <key>
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = <value>
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = <value1>
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

#Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform

    And set Result.ActualOutput.Reference = 'Defect_1267'
    And set Result.Additional_Details = Application_Details

    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    * print Result

    Examples:
      | key   | value                | value1                                                      |
      | '001' | 'abciihioaeh2456465' | apiApp.Account_Request_Setup.response.Data.AccountRequestId |


      ################################################################################################################################################
  @Defect_1542
  Scenario: Verify that Error Message is displayed when the request object Signature verification fails with the certificate

    Given def info = karate.info
    And set info.Screens = 'Yes'
    * set info.subset = 'Signature Validation with Certificate'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

       #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def ip_path = active_tpp.AISP
    * def path = stmtpath(ip_path) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.SigningKey = 'AISP'
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    ##########################################################################################################################################
  @Defect_1542
  @severity=normal
  Scenario Outline: Verify that Error Message is displayed when incorrect clientID #key# is passed in request object

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And set info.key = <client_id>
    And set info.subset = 'Incorrect Client id in Request Object'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

    * def key = <client_id>

     #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = <client_id>
    * set ReqObjIP $.payload.client_id = <client_id>
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result

    Examples:
      | client_id                       |
      | 'abkjichaioycvhoaeyvhoaeihviho' |


    #########################################################################################################################################
  @Defect_1542
  Scenario Outline: Verify that Error Message is displayed when incorrect clientID #key# is passed in request query parameter

    * def key = <client_id>
    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'Incorrect Client id in Request Query Parameter'
    And set info.key = <client_id>
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

        #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

      #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)
    * print Application_Details

    * print apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.client_id = <client_id>
    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.client_Id = <client_id>
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = '400 - Unknown or invalid client_id'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result

    Examples:
      | client_id               |
      | 'abcdefgjhkhlkjlfuyfuf' |

