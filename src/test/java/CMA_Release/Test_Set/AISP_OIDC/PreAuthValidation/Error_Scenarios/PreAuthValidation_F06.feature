@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error conditions for Missing Intent id in the Request Object.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  #################################################################################################################################################################
  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from Token Id section of Request Object when value of TPP and scope is #key#

    * def key = '<Role>' + ' & ' + <scope>
    Given def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Intent id in idtoken_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

   #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

    And set Result.Input.CCG_TPPRole = '<Role>'
    And set Result.Input.CCG_scope = <scope>

   #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

   #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
       #Remove intent id from idtoken section
    And remove ReqObjIP.payload.claims.id_token.openbanking_intent_id
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

   #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error'

    * print Result


    Examples:
      | Scenario number | Role      | scope             |
      | '001'           | AISP_PISP | 'openid accounts' |
      | '002'           | AISP      | 'openid accounts' |


  ###############################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from both user info and Token Id section of Request Object when value of TPP and scope is #key#

    * def key = '<Role>' + ' & ' + <scope>
    Given def info = karate.info
    * set info.key = key
    * set info.subset = 'Missing Intent id in userinfo and idtoken_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

  #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

    And set Result.Input.CCG_TPPRole = '<Role>'
    And set Result.Input.CCG_scope = <scope>

  #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

  #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
   #Remove intent id from user info section
    And remove ReqObjIP.payload.claims.id_token.openbanking_intent_id
   #Remove intent id from Token Id section
    And remove ReqObjIP.payload.claims.userinfo.openbanking_intent_id
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

  #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

  #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=Server+Error&state=af0ifjsldkj&error=server_error'

    * print Result

    Examples:
      | Scenario number | Role      | scope             |
      | '001'           | AISP_PISP | 'openid accounts' |
      | '002'           | AISP      | 'openid accounts' |


  #################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that TPP Redirect URL is displayed on UI Browser when Intent Id is missing from user info and incorrect in Token Id section of Request Object when value of TPP and scope is #key#

    * def key = '<Role>' + ' & ' + <scope>
    Given def info = karate.info
    * set info.key = key
    * set info.subset = 'Incorrect Intentid in idtoken_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

   #Set TPP Role and scope
    Given json Application_Details = active_tpp.<Role>
    And set Application_Details $.scope = <scope>

   #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

    #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    #Remove intent id from user info section
    And remove ReqObjIP.payload.claims.userinfo.openbanking_intent_id
    #Set incorrect intent id in Token Id section
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = 'abcdffd'
    And set ReqObjIP $.payload.scope = Application_Details.scope
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=invalid_request&state=af0ifjsldkj&error=server_error'

    * print Result


    Examples:
      | Scenario number | Role      | scope             |
      | '001'           | AISP_PISP | 'openid accounts' |
      | '002'           | AISP      | 'openid accounts' |


