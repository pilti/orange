@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Bus


Feature: This Feature demonstrates the Error Scenarios for PreAuthValidation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    Given json Application_Details = active_tpp.AISP

    And set Result.Input.CCG_TPPRole = 'AISP'
    And set Result.Input.CCG_scope = Application_Details.scope

    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  @Regression
  @PT
  @Defect_1542
  @severity=normal
  Scenario: Verify that Error message is displayed when malformed request object is passed in the url

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'Malformed Request Object in URL'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

      #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = (apiApp.CreateRequestObject.jwt.request).replace(".","#")
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    #appears no more
#    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.Error_text1)
#    * print ActualText
#    And def ExpectedUIErrorMessage = '400 - Authorization server can not verify the signed request'
#    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
#    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
   # And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And def ExpectedUIErrorMessage = 'Due to problems with the third party site, we are unable to process your request. Please contact the third party provider or try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    ##############################################################################################################################################
  @severity=normal
  Scenario: Verify that TPP Redirect URL is displayed when scope is missing in request object

    Given def info = karate.info
    And set info.key = 'Yes'
    And set info.subset = 'Missing scope in Request Object'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

 #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * remove ReqObjIP $.payload.scope
    * set ReqObjIP $.payload.redirect_uri = 'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

#Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.Additional_Details = Application_Details

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=Invalid+scopes+provided+in+the+request.&state=af0ifjsldkj&error=server_error'

    * print Result


  #####################################################################################################################################################

  @Defect_1344
  @severity=normal
  Scenario: Verify that TPP Redirect URL is displayed when scope is incorrect in request object

    Given def info = karate.info
    And set info.key = 'Yes'
    And set info.subset = 'Incorrect scope in Request Object'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

   #Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accountss'
    * set ReqObjIP $.payload.redirect_uri =  'https://boi.com'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path
    And set Result.ActualOutput.CreateRequestObject.scope = 'openid accountss'
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

 #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Defect_1344'

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope%28s%29+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope'
    * print Result


    ######################################################################################################################################################

  @Defect_1344
  @severity=normal
  Scenario: Verify that TPP Redirect URL is displayed when requested scope has a mismatch

    Given def info = karate.info
    And set info.key = 'Yes'
    And set info.subset = 'Scope Mismatch in Request Object & Request Query Parameter'
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

#Set input parameters for CreateRequestObject
    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id =  Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts payments'
    * set ReqObjIP $.payload.redirect_uri =  'https://www.getpostman.com/oauth2/callback'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.scope = 'openid accounts payments'
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

#Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.redirect_uri = 'https://boi.com'
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Result.ActualOutput.ConstructAuthReqUrl.scope = Application_Details.scope

    When call apiApp.ConstructAuthReqUrl(Application_Details)

    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)
    Given def perform = Functions.launchConsentURL(webApp,consenturl)

    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    Then def perform = Functions.getUrl(webApp)

    And set Result.ActualOutput.UI.Output.launchConsentURL_TPPRedirectURL = perform.ActualUrl

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Defect_1344'

    * match perform.ActualUrl == 'https://app.getpostman.com/oauth2/callback#error_description=The+requested+scope(s)+must+be+blank+or+a+subset+of+the+provided+scopes.&state=af0ifjsldkj&error=invalid_scope'

    * print Result