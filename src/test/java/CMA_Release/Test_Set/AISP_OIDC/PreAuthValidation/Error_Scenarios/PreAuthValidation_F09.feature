@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error condition when requested IntentID is incorrect in the Request Object wrt ClientID.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  #################################################################################################################################################################
  @Defect_1542
  @severity=normal
  Scenario Outline: Verify that Error Message is displayed on UI Browser when Client Id provided in Client Credential Grant and Request Object is for different TPP i.e #key#

    * def key = '<CCGRole>'+' & '+'<JWTRole>'
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Client Id of different TPP in JWT_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

           #Set TPP Role and scope
    Given json Application_Details = active_tpp.<CCGRole>
    And set Application_Details $.scope = <scope>

    And set Result.Input.CCG_TPPRole = '<CCGRole>'
    And set Result.Input.CCG_scope = <scope>

        #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

        #Set input parameters for CreateRequestObject
    Given json Application_Details1 = active_tpp.<JWTRole>

    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details1.client_id
    And set ReqObjIP $.payload.client_id = Application_Details1.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    And set ReqObjIP $.payload.scope = <scope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input.client_id = Application_Details1.client_id
    And set Result.ActualOutput.CreateRequestObject.Input.client_secret = Application_Details1.client_secret
    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

         #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

        #Set input parameters for CreateConsentURL
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.client_id = Application_Details1.client_id
    And set Application_Details $.secret = Application_Details1.client_secret

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = <Expected>
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result

    Examples:
      | Scenario number | CCGRole   | scope             | JWTRole   | Expected                                                       |
      | '001'           | AISP_PISP | 'openid accounts' | PISP      | '400 - Authorization server can not verify the signed request' |
      | '002'           | AISP      | 'openid accounts' | AISP_PISP | '400 - Authorization server can not verify the signed request' |
      | '003'           | AISP      | 'openid accounts' | PISP      | '400 - Authorization server can not verify the signed request' |



 #################################################################################################################################################################

  @Query_1383
  @Defect_1542
  @severity=normal
  Scenario Outline: Verify that Error Message is displayed on UI Browser when Client Id provided in Client Credential Grant and Authorise Request is for different TPP and value is #key#

    * def key = '<CCGRole>'+' & '+'<AuthReqRole>'
    Given def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Client Id provided is for different TPP in Authorise Request_'+<Scenario number>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path
    And def path = call  webApp.path1 = info1.path

    #Set TPP Role and scope
    Given json Application_Details = active_tpp.<CCGRole>
    And set Application_Details $.scope = <scope>

    And set Result.Input.CCG_TPPRole = '<CCGRole>'
    And set Result.Input.CCG_scope = <scope>

     #Call the feature file to create Account Request id
    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

     #Set input parameters for CreateRequestObject
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    And set ReqObjIP $.payload.scope = <scope>
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    #Call CreateRequestObject
    When call apiApp.CreateRequestObject(Application_Details)

     #Set input parameters for CreateConsentURL
    Given json Application_Details1 = active_tpp.<AuthReqRole>

    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.client_id = Application_Details1.client_id
    And set Application_Details $.secret = Application_Details1.client_secret

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt
    And set Result.ActualOutput.ConstructAuthReqUrl.Input.scope = <scope>
    And set Result.ActualOutput.ConstructAuthReqUrl.Input.client_id = Application_Details1.client_id
    And set Result.ActualOutput.ConstructAuthReqUrl.Input.client_secret = Application_Details1.client_id

    And call apiApp.ConstructAuthReqUrl(Application_Details)

    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl
    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    And set Result.Additional_Details = Application_Details
    And set Result.ActualOutput.Reference = 'Query_1383'
    And set Result.ActualOutput.Reference = 'Defect_1542'

    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = <Expected>
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    Examples:
      | Scenario number | CCGRole   | scope             | AuthReqRole | Expected                                                       |
      | '001'           | AISP_PISP | 'openid accounts' | PISP        | '400 - Authorization server can not verify the signed request' |
      | '002'           | AISP      | 'openid accounts' | AISP_PISP   | '400 - Authorization server can not verify the signed request' |
      | '003'           | AISP      | 'openid accounts' | PISP        | '400 - Authorization server can not verify the signed request' |



 #################################################################################################################################################################
