@AISP
@AISP_UAT
@AISP_UAT_OIDC
@AISP_PreAuthValidation
@AISP_Error_Tech


Feature: This feature is to demonstrate negative flow for creating improper consent URL

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectFile = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Login_obj')

    Given json Application_Details =  active_tpp.AISP

    And set Result.Input.TPPRole = 'AISP'
    And set Result.Input.scope = Application_Details.scope

    And def TestRes = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature') Application_Details

    ############################Set input parameters for CreateRequestObject#######################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    * set Application_Details $.scope = 'openid accounts'
    * def path = stmtpath(Application_Details) + 'signin_private.key'
    * set Application_Details.path = path

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  ############################################################END OF BACKGROUND###################################################
  @Defect_1542
  @severity=normal

  Scenario Outline: Verify that Error message is displayed when there is URI Mismatch in Request object and Request Query parameter in compare to the URL provided while creating the application in OB
    Given def info = karate.info
    And def key = <count>
    And set info.key = key
    And set info.Screens = 'Yes'
    And set info.subset = 'URI Mismatch in JWT and Query Parameter_'+<count>
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

      # setting value for uri in jwt payload
    * set ReqObjIP.payload.redirect_uri = <jwt_redirecturi>

    And set Result.ActualOutput.CreateRequestObject.Input = ReqObjIP

    When call apiApp.CreateRequestObject(Application_Details)

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * print Application_Details $.jwt
    * set Application_Details $.redirect_uri = <QueryParameter_redirecturi>
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    And set Result.ActualOutput.CreateRequestObject.Output.JWT = Application_Details.jwt

    And set Result.ActualOutput.ConstructAuthReqUrl.Input.redirect_uri = <QueryParameter_redirecturi>

    Then call apiApp.ConstructAuthReqUrl(Application_Details)
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print  consenturl

    And set Result.ActualOutput.ConstructAuthReqUrl.ConsentUrl = consenturl

    Then def c = call  webApp.driver1 = webApp.start1(default_browser)

    And def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Fail'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_reasonText = perform.reasonText

    And set Result.ActualOutput.Reference = 'Defect_1542'
    And set Result.Additional_Details = Application_Details

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text)
    * print ActualText
    And def ExpectedUIErrorMessage = '400 - Invalid redirect_uri'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    And def ActualText = seleniumFunctions.getElementText(webApp, ObjectFile.preAuth_error_text1)
    * print ActualText
    And def ExpectedUIErrorMessage = 'We are unable to process your request at this time, please return to the Third Party Site and try again later.'
    And set Result.ActualOutput.UI.Output.launchConsentURL_ErrorMessage1 = ActualText
    Then match ActualText == ExpectedUIErrorMessage

    * print Result


    Examples:
      | count | jwt_redirecturi   | QueryParameter_redirecturi |
      | '001' | 'XYZ'             | 'https://boi.com'          |
      | '002' | 'https://boi.com' | 'XYZ'                      |