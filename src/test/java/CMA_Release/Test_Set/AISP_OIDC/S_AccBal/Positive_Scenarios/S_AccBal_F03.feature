@AISP
@AISP_UAT
@AISP_API
@AISP_SIT_API
@AISP_S_AccBal
@AISP_S_AccBal_SIT_NA

Feature: Check for CreditDebitIndicator in Get Single Account Balance API


  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    #Read refresh token from properties file
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
    * call apiApp.configureSSL(profile)
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')

    #After scenario for output
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify if Account id with negative balance is getting fetched in response(data specific)

    * def info = karate.info
    * def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """


    Given set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}

    Given def inputJSONSB = inputJSON
    And set inputJSONSB.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #DB manipulation
    And def NSC = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification.substring(0,6)
    And def ACCTNO = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification.substring(6,14)
    And def query = "LEDGER_BALANCE =" + <balance>
    And def connection = ABT_DB.update_ABT_Balance(NSC,ACCTNO,ABT_DB_Conn,query)
    When call apiApp.Account_Balance(inputJSONSB)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response.Data.Balance[0] contains {AccountId: '#notnull'}
    And match apiApp.Account_Balance.response.Data.Balance[0].AccountId == inputJSONSB.AccountId
    And match apiApp.Account_Balance.response.Data.Balance[0].Amount.Amount == <balance>.replace("-","")
    And match apiApp.Account_Balance.response.Data.Balance[0].CreditDebitIndicator == "Debit"

    Examples:
      | balance |
      | "-3.33" |


  Scenario Outline: Verify if Account id with Positive balance is getting fetched in response(data specific)

    * def info = karate.info
    * def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """

    Given set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    And match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}

    Given def inputJSONSB = inputJSON
    And set inputJSONSB.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #DB manipulation
    And def NSC = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification.substring(0,6)
    And def ACCTNO = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification.substring(6,14)
    And def query = "LEDGER_BALANCE =" + <balance>
    And def connection = ABT_DB.update_ABT_Balance(NSC,ACCTNO,ABT_DB_Conn,query)
    When call apiApp.Account_Balance(inputJSONSB)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response.Data.Balance[0] contains {AccountId: '#notnull'}
    And match apiApp.Account_Balance.response.Data.Balance[0].AccountId == inputJSONSB.AccountId
    And match apiApp.Account_Balance.response.Data.Balance[0].Amount.Amount == <balance>.replace("-","")
    And match apiApp.Account_Balance.response.Data.Balance[0].CreditDebitIndicator == "Credit"

    Examples:
      | balance |
      | "3.33" |









