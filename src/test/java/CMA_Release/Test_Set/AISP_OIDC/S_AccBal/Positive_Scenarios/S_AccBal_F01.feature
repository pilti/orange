@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_S_AccBal
@AISP_S_AccBal_SIT


Feature: This feature is to demonstrate the Balance API response when Balance Permission is provided in the request.

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """



  @Functional_Shakedown
  @Functional_Shakedown_Quick
  Scenario Outline: Verify the Structure and response of Balance info API when used refresh Token having #key# permission

    Given def Application_Details = active_tpp.<Role>

    * def info = karate.info
    * def key = <condition>
    * set info.subset = 'Structure validation'
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.<Role>
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then  match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
     #Set input for Balance API
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/balances'
     #Call Single Account Balance API
    When call apiApp.Account_Balance(Application_Details)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response ==
    """
    { "Data": {
        "Balance": [
          {
            "AccountId": '#string',
            "Amount": {
              "Amount": '#string',
              "Currency": '#string'
            },
            "CreditDebitIndicator": '#string',
            "Type": '#string',
            "DateTime": '#string'
          }
        ]
      },
      "Links": {
        "Self": '#string'
      },
      "Meta": {
        "TotalPages": 1
      }
    }        """




    Examples:
      | Role      | condition                          | permissionsfile                      |
      | AISP_PISP | 'ReadAccountsDetail & ReadBalance' | 'Only_Balance_Permission_G_User_1.properties' |

