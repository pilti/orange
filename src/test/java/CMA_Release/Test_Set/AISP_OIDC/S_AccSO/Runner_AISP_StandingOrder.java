package CMA_Release.Test_Set.AISP_OIDC.S_AccSO;

import Runners.Test_Runner_Parallel;
import cucumber.api.CucumberOptions;

@CucumberOptions(tags = {"@AISP_SingleAccSO"})
public class Runner_AISP_StandingOrder extends Test_Runner_Parallel {
}
