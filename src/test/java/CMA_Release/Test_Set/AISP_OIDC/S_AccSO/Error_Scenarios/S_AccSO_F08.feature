@AISP
@AISP_API
@AISP_SingleAccSO
@AISP_SingleAccSO_Err

Feature: Method and permissions validation for Get Single Account Standing order Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
    * def fileName = 'Generic_S_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(Application_Details)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """


  Scenario Outline:Verify whether error is received when API is triggered with invalid method #key#

    * def info = karate.info
    * def key = '<method>'
    * set info.subset = 'Method validation'

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/standing-orders'
    And url AcctInfoUrl + Application_Details.AccountId + '/standing-orders'
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    And request permissions
    When method <method>
    And set Result.Account_Standing_Orders.Input.URL = Application_Details.URL
    And set Result.Account_Standing_Orders.Input.Method = '<method>'
    And set Result.Account_Standing_Orders.Input.client_id = Application_Details.client_id
    And set Result.Account_Standing_Orders.Input.client_secret = Application_Details.client_secret
    And set Result.Account_Standing_Orders.Input.token_type = Application_Details.token_type
    And set Result.Account_Standing_Orders.Input.access_token = Application_Details.access_token
    And set Result.Account_Standing_Orders.Output.responseStatus = responseStatus
    And set Result.Account_Standing_Orders.Output.response =  response
    Then match responseStatus == 405

    Examples:
      | method  |
      | POST    |
      | DELETE  |
      | PUT     |
      | OPTIONS |
      | HEAD    |
      | PATCH   |


  Scenario Outline:Verify whether error is received when API is triggered with invalid #key# permissions

    * def info = karate.info
    * def key = <permissions>
    * set info.subset = 'Permissions validation'
    * def fileName = <permissions>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    Given json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/standing-orders'
    When call apiApp.Account_Standing_Orders(Application_Details)
    Then match apiApp.Account_Standing_Orders.responseStatus == 403

    Examples:
      | permissions                                         |
      | 'Only_Account_info_permissions_S_User_1.properties' |
      | 'Only_Balance_Permission_S_User_1.properties'       |
      | 'Only_Beneficiaries_permission_S_User_1.properties' |
      | 'Only_Direct_Debits_permission_S_User_1.properties' |
      | 'Only_Products_permission_S_User_1.properties'      |
      | 'Only_Transactions_Permission_S_User_1.properties'  |
      | 'Only_StandingOrder_permission_S_User_1.properties' |
