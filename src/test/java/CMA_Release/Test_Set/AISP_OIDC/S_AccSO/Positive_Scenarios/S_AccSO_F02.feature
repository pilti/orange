@ignore
@AISP_SingleAccSO
@Regression
Feature:  Verify response headers of Get Standing Orders API (/accounts/{AccountId}/standing-orders API)

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'S_AccSO_TestData.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccSO/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_SingleAccSO
  @severity=normal
  Scenario: To verify that content type header in response of Single Account Standing Orders API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

     ##Get AccounID from Multi Account Info API
    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

   #Get Single Account standing orders
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/standing-orders'
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match apiApp.Account_Standing_Orders.responseHeaders['Content-Type'][0] == 'application/json;charset=UTF-8'

  @AISP_SingleAccSO
  @severity=normal
  Scenario: To verify that value of x-fapi-interaction-id header in response is same as sent in request header of Single Account Standing Orders API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/standing-orders'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseHeaders['x-fapi-interaction-id'][0] == '93bac548-d2de-4546-b106-880a5018460d'

  @AISP_SingleAccSO
  @severity=normal
  Scenario: To verify that x-fapi-interaction-id response header with unique uuid is generated when x-fapi-interaction-id header is not sent in Single Account Standing Orders API request

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + 'standing-orders'
    And remove reqHeader $.x-fapi-interaction-id
    When call apiApp.Account_Standing_Orders(inputJSON)
    And remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    Then match apiApp.Account_Standing_Orders.responseHeaders['x-fapi-interaction-id'][0] == '#uuid'

  @AISP_SingleAccSO
  @severity=normal
  Scenario: To verify that API platform returns Http Status code 404 Not Found when Bulk Standing Orders API is triggered

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = BulkApiUrl + 'standing-orders'
    When call apiApp.Bulk_Standing_Orders(inputJSON)
    Then match apiApp.Bulk_Standing_Orders.responseStatus == 404