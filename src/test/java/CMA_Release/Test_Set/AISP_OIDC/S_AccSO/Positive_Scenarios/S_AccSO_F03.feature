@Regression
@ignore
@AISP_SingleAccSO
Feature:  To verify response of Single Account Standing Orders API is as per the permission provided in consent-ReadStandingOrdersBasic

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = "S_AccSO_TestData_Basic_permissions.properties"
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccSO/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_SingleAccSO
  @severity=normal
  Scenario: To verify only basic information is retrieved by Standing Orders API when only ReadStandingOrdersBasic permission is given

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match each apiApp.Account_Standing_Orders.response.Data.StandingOrder[*] ==
    """
    {
      "AccountId": "#string",
      "StandingOrderId": "#string",
      "Frequency": "#string",
      "Reference": "##string",
      "NextPaymentDateTime": "#string",
      "NextPaymentAmount":
      {
        "Amount": "#string",
        "Currency": "GBP"
      }
    }
    """
    And match each apiApp.Account_Standing_Orders.response.Data.StandingOrder[*] !contains { "CreditorAccount": { "SchemeName": "SortCodeAccountNumber", "Identification": "#string" }}

  @AISP_SingleAccSO
  @severity=normal
  Scenario: Get Standing Orders Basic information for an account by calling Single Account Standing Orders API with mandatory and optional headers
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    And set reqHeader $.Accept = 'application/json'
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200

  @AISP_SingleAccSO
  @severity=normal
  Scenario: Get Standing Orders Basic information for an account by calling Single Account Standing Orders API only with mandatory headers

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And  remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
