@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@Regression

  @Anu_reg_cc
Feature: This feature is to demonstrate the Positive Flow for valid combinations of scope and TPP role in CCG and Account Setup API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
     """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @AISP_CCToken1
  @Functional_Shakedown
  Scenario Outline: Verify that Access token is generated using Client Credential Grant API for all valid combination of scope when #key#

    # Set variables for Business report for this scenario
    * def key = 'TPP role is ' + '<Role>' +'& scope is '+ <scope>
    * def key1 = ' Valid Combination of scope'
    * def info = karate.info
    * set info.key = key
    * set info.subset =  key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    #BDD for get Access Token
     Given json Application_Details = active_tpp.<Role>
     And call apiApp.configureSSL(Application_Details)
     And set Application_Details $.TPPRole = '<Role>'
     And set Application_Details.request_method = 'POST'
     And set Application_Details $.scope = <scope>
     And set Application_Details $.grant_type = 'client_credentials'
     And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
     When call apiApp.Access_Token_CCG(Application_Details)
     And set Result.Additional_Details = Application_Details
     Then match apiApp.Access_Token_CCG.responseStatus == <Expected>

    Examples:
      | Scenario number | Role      | scope                      | Expected |
      | '001'           | AISP_PISP | 'openid accounts payments' | 200      |
      | '002'           | AISP_PISP | 'payments accounts openid' | 200      |
      | '003'           | AISP_PISP | 'payments openid accounts' | 200      |
      | '004'           | AISP_PISP | 'accounts openid payments' | 200      |
      | '005'           | AISP_PISP | 'accounts payments openid' | 200      |
      | '006'           | AISP_PISP | 'openid accounts'          | 200      |
      | '007'           | AISP_PISP | 'openid payments'          | 200      |
      | '008'           | AISP_PISP | 'accounts'                 | 200      |
      | '009'           | AISP_PISP | 'payments'                 | 200      |
      | '010'           | AISP_PISP | 'accounts payments'        | 200      |
      | '011'           | AISP_PISP | 'payments accounts'        | 200      |
      | '012'           | AISP_PISP | 'openid'                   | 200      |
      | '013'           | AISP      | 'openid accounts'          | 200      |
      | '014'           | AISP      | 'accounts openid'          | 200      |
      | '015'           | AISP      | 'accounts'                 | 200      |
      | '016'           | AISP      | 'openid'                   | 200      |
      | '017'           | PISP      | 'openid payments'          | 200      |
      | '018'           | PISP      | 'payments openid'          | 200      |
      | '019'           | PISP      | 'payments'                 | 200      |
      | '020'           | PISP      | 'openid'                   | 200      |
      | '021'           | AISP_PISP | ''                         | 200      |
      | '022'           | AISP      | ''                         | 200      |
      | '023'           | PISP      | ''                         | 200      |


 @severity=normal
  @CITest12
  Scenario Outline: Verify that Access token is generated using Client Credential Grant API when scope is missing for #key#
    * def key = 'TPP Role ' + '<Role>'
    * def key1 = ' Missing scope'
    * def info = karate.info
    * set info.key = key
    * set info.subset =  key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given json Application_Details = active_tpp.<Role>
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details.scope = null
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Result.Additional_Details = Application_Details
    Then match apiApp.Access_Token_CCG.responseStatus == <Expected>


    Examples:
      | Scenario number | Role      | Expected |
      | '009'           | AISP_PISP | 200      |
      | '010'           | AISP      | 200      |
      | '011'           | PISP      | 200      |
      | '009'           | AISP_PISP | 200      |
      | '010'           | AISP      | 200      |
      | '011'           | PISP      | 200      |
      | '009'           | AISP_PISP | 200      |
      | '010'           | AISP      | 200      |
      | '011'           | PISP      | 200      |
      | '009'           | AISP_PISP | 200      |
      | '010'           | AISP      | 200      |
      | '011'           | PISP      | 200      |

