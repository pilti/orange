@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@Regression


Feature: This feature is to demonstrate the positive flow for Client Credential API when Valid Grant Type is passed

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
    """
    function(){
    var tem = karate.info;
    if (typeof key != 'undefined'){info.key = key}
    if(tem.errorMessage == null){
        Result.TestStatus = "Pass";
    }else{
        Result.TestStatus = "Fail";
        Result.Error = tem.errorMessage;
    }
    apiApp.write(Result,info);
      }
    """
  @Functional_Shakedown
  Scenario Outline: Verify Access token is generated for valid Grant type using Client Credential Grant API when #key#
    * def key = 'TPP role is ' + '<Role>' +'& grant type is '+ <grant type>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Valid Grant Type'
    * json Application_Details =  active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    * set Application_Details.request_method = 'POST'
    * def mtd = Application_Details.request_method
    * set Application_Details $.scope = <scope>
    * def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    * def Body = 'grant_type='+ <grant type> + '&scope=' + <scope>

    Given url TokenUrl
      And header Authorization = auth
      And header Content-Type = 'application/x-www-form-urlencoded'
      And request Body
    When method mtd
      And set Result.ActualOutput.Access_Token_CCG.Input.RequestBody = Body
      And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
      And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
      And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
      And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
      And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
      And set Result.Additional_Details = Application_Details
    Then match responseStatus == <Expected>

    Examples:
      | Role      | grant type           | scope                      | Expected |
      | AISP_PISP | 'client_credentials' | 'openid accounts payments' | 200      |