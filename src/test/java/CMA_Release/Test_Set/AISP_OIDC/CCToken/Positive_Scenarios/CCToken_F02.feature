@AISP
@AISP_UAT
@AISP_API
@AISP_SIT_OIDC
@AISP_CCToken
@AISP_CCToken_SIT
@Regression

Feature: Test to verify Get Client Credential Token API
  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API12
  @severity=normal
  Scenario: Verify that response Status is 200 when all valid PARAMS are used with correct URL
    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

  @AISP_API
  @severity=blocker
  Scenario: Verify that response of authorisation token request using client grant contains valid token_type as bearer
    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And match apiApp.Access_Token_CCG.response.token_type == "Bearer"

  @AISP_API
  @Severity=minor
  Scenario: Verify that response of authorisation token request using client grant contains valid expiry duration

    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
      And call apiApp.configureSSL(Application_Details)
      And set Application_Details.request_method = 'POST'
      And set Application_Details $.grant_type = "client_credentials"
      And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
      And def Expiry = apiApp.Access_Token_CCG.response
      And def val = (Expiry.expires_in == 300 || Expiry.expires_in == 299 || Expiry.expires_in == 298 ? "True" : "False" )
      And match val == "True"
    And set Result.ActualOutput.Access_Token_CCG.Output.Expiry_check = val


  @severity=normal
  Scenario: Verify the response status is 200 when Scope is not provided in Get client credentials token request

    * def info = karate.info
    Given json Application_Details = active_tpp.AISP_PISP
      And call apiApp.configureSSL(Application_Details)
      And set Application_Details.request_method = 'POST'
      And set Application_Details $.grant_type = "client_credentials"
      And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
      And set Application_Details.scope = null
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

