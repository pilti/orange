@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@AISP_Error_Bus

Feature: This feature is to demonstrate the Error Conditions for combinations of scope and TPP role with valid client credentials but not as in certificate

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Query_1373
  @severity=normal
  Scenario Outline:Verify that access token is not generated (client credential grant) with body as grant_type=client_credentials&scope=#key#

    * def info = karate.info
      #* def key = '<scope> and TPP holding role <Role> + <subset>'
    * def key = '<scope> and TPP holding role <Role>'
    * set info.key = key
    * set info.subset = 'Invalid Certificate_'+<subset>

    Given json Application_Details = active_tpp.<TPPCertificate>
    * call apiApp.configureSSL(Application_Details)
    And json Application_Details = active_tpp.<Role>
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.scope = '<scope>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == <Expected>

    Examples:
      | subset | Role      | scope                    | Expected | TPPCertificate |
      | '001'  | AISP_PISP | openid accounts payments | 401      | PISP           |
      | '002'  | PISP      | openid accounts payments | 401      | AISP           |
      | '003'  | AISP      | openid accounts payments | 401      | PISP           |
      | '004'  | AISP      | openid accounts          | 401      | PISP           |
      | '005'  | PISP      | openid payments          | 401      | AISP           |
