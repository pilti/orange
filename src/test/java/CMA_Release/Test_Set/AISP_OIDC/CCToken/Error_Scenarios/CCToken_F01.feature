@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@AISP_Error_Bus
@Regression

Feature: This feature is to demonstrate the Error Conditions for invalid combinations of scope and TPP role for CCG API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Regression
  @Defect_1337
  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed in the response when #key#
    * def key = 'TPP Role is '+'<Role>'+' & Scope is '+<scope>
    * def key1 = 'Invalid Combination of TPPRole & scope'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Combination of TPPRole & scope'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given json Application_Details = active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == <Expected>


    Examples:
      | Scenario number | Role | scope                      | Expected |
      | '001'           | PISP | 'openid accounts payments' | 400      |
      | '002'           | AISP | 'openid accounts payments' | 400      |
      | '003'           | AISP | 'openid payments'          | 400      |
      | '004'           | PISP | 'openid accounts'          | 400      |
      | '005'           | PISP | 'accounts payments'        | 400      |
      | '006'           | AISP | 'accounts payments'        | 400      |
      | '007'           | PISP | 'accounts'                 | 400      |
      | '008'           | AISP | 'payments'                 | 400      |
