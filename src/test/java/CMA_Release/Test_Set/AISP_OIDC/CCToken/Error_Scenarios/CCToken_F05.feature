@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@AISP_Error_Bus
@Regression


Feature: TPP Client credentials Validation for Client credential token API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
    """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential grant by TPP having AISP and PISP role when invalid client credentials and scope as #key# is passed in the request
    * def key = <scope>
    * def info = karate.info
    * set info.key = <scope>
    * set info.subset = 'Invalid CC - AISP & PISP role'

    Given def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
      # set values to parameters required to hit client credential token API
    And set Application_Details.scope = <scope>
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.request_method = 'POST'
    And set Application_Details.client_id = <client_id>
    And set Application_Details.client_secret = <client_secret>
   # call Client credential token API in order to get client credential token
    When call apiApp.Access_Token_CCG(Application_Details)
      # Validate response and update result json
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | client_id | client_secret | scope                      |
      | 'abc'     | 'xyz'         | 'openid accounts payments' |
      | 'abc'     | 'xyz'         | 'openid payments accounts' |
      | 'abc'     | 'xyz'         | 'openid accounts'          |
      | 'abc'     | 'xyz'         | 'openid payments'          |


  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential grant by TPP having #key# role when invalid client_credentials are sent in the request

    * def key = '<role>'
    * def info = karate.info
    * set info.key = '<role>'
    * set info.subset = 'Invalid CC - AISP or PISP role'

    Given def Application_Details = active_tpp.<role>
       # Hit Certificate request to server
    * call apiApp.configureSSL(Application_Details)
        # set values to parameters required to hit client credential token API
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.request_method = 'POST'
    And set Application_Details.client_id = <client_id>
    And set Application_Details.client_secret = <client_secret>
    # call Client credential token API in order to get client credential token
    When call apiApp.Access_Token_CCG(Application_Details)
     # Validate response and update result json
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | role | client_id | client_secret |
      | AISP | 'abc'     | 'xyz'         |
      | PISP | 'abc'     | 'xyz'         |

  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential grant by TPP having AISP and PISP role when invalid client credentials and scope as #key# is passed in the request
    * def key = <scope>
    * def info = karate.info
    * set info.key = <scope>
    * set info.subset = 'Invalid CC - AISP & PISP role'

    Given def Application_Details = active_tpp.AISP_PISP
       # Hit Certificate request to server
    * call apiApp.configureSSL(Application_Details)
       # set values to parameters required to hit client credential token API
    And set Application_Details.scope = <scope>
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.request_method = 'POST'
    And set Application_Details.client_id = <client_id>
    And set Application_Details.client_secret = <client_secret>
     # call Client credential token API in order to get client credential token
    When call apiApp.Access_Token_CCG(Application_Details)
     # Validate response and update result json
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | client_id | client_secret | scope               |
      | 'abc'     | 'xyz'         | 'accounts payments' |
      | 'abc'     | 'xyz'         | 'payments accounts' |
      | 'abc'     | 'xyz'         | 'accounts'          |
      | 'abc'     | 'xyz'         | 'payments'          |
      | 'abc'     | 'xyz'         | 'openid'            |


  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential grant by TPP having AISP role when scope as #key#  passed in the request

    * def key = <scope>
    * def info = karate.info
    * set info.key = <scope>
    * set info.subset = 'Invalid CC - AISP role'

    Given def Application_Details = active_tpp.AISP
      # Hit Certificate request to server
    * call apiApp.configureSSL(Application_Details)
      # set values to parameters required to hit client credential token API
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.scope = <scope>
    And set Application_Details.request_method = 'POST'
    And set Application_Details.client_id = <client_id>
    And set Application_Details.client_secret = <client_secret>
    # call Client credential token API in order to get client credential token
    When call apiApp.Access_Token_CCG(Application_Details)
      # Validate response and update result json
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | client_id | client_secret | scope      |
      | 'abc'     | 'xyz'         | 'accounts' |
      | 'abc'     | 'xyz'         | 'openid'   |

  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential grant by TPP having PISP role when #key# passed in the request

    * def key = <scope>
    * def info = karate.info
    * set info.key = <scope>
    * set info.subset = 'Invalid CC - PISP role'

    Given def Application_Details = active_tpp.PISP
         # Hit Certificate request to server
    * call apiApp.configureSSL(Application_Details)
        # set values to parameters required to hit client credential token API
    And set Application_Details.grant_type = 'client_credentials'
    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details.scope = <scope>
    And set Application_Details.request_method = 'POST'
    And set Application_Details.client_id = <client_id>
    And set Application_Details.client_secret = <client_secret>
     # call Client credential token API in order to get client credential token
    When call apiApp.Access_Token_CCG(Application_Details)
      # Validate response and update result json
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | client_id | client_secret | scope      |
      | 'abc'     | 'xyz'         | 'payments' |
      | 'abc'     | 'xyz'         | 'openid'   |










