@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@AISP_Error_Bus


Feature: This feature is to demonstrate the negative flow for Client Credential API when Valid/Invalid Grant Type values are passed for different TPPs

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
    """
    function(){
    var tem = karate.info;
    if (typeof key != 'undefined'){info.key = key}
    if(tem.errorMessage == null){
        Result.TestStatus = "Pass";
    }else{
        Result.TestStatus = "Fail";
        Result.Error = tem.errorMessage;
    }
    apiApp.write(Result,info);

      }
    """

  @severity=normal
  Scenario Outline: Verify Access token is not generated using Client Credential Grant API for an Invalid Grant Type when #key#

    * def key = 'TPP role is ' + '<Role>' +'& grant type is '+ <grant type>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid GrantType'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given json Application_Details =  active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.scope = <scope>
    And set Application_Details.request_method = 'POST'
    And def mtd = Application_Details.request_method
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request 'grant_type='+ <grant type> + '&scope=' + <scope>
    When method mtd
    Then match responseStatus == <Expected>
    #And def Body = 'grant_type='+ <grant type> + '&scope=' + <scope>

    Examples:
      | Role      | grant type              | scope                      | Expected |
      | AISP      | 'authorization_code'    | 'openid accounts'          | 400      |
      | PISP      | 'authorization_code'    | 'openid payments'          | 400      |
      | AISP_PISP | 'refresh_token'         | 'openid accounts payments' | 400      |
      | AISP_PISP | 'client_credentials123' | 'openid accounts payments' | 400      |

  Scenario: Verify Access token is not generated using Client Credential Grant API when Request Body has No Grant Type mentioned
    * def info = karate.info

    Given json Application_Details =  active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And def mtd =  Application_Details.request_method
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request 'scope=openid payments'
    When method mtd
    Then match responseStatus == 400
   # And def Body = 'scope=openid payments'

  Scenario: Verify Access token is not generated using Client Credential Grant API when Grant Type is passed Blank
    * def info = karate.info

    Given json Application_Details =  active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And def mtd =  Application_Details.request_method
    And def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Application_Details.client_id)', password: '#(Application_Details.client_secret)'}
    And url TokenUrl
    And header Authorization = auth
    And header Content-Type = 'application/x-www-form-urlencoded'
    And request 'grant_type=&scope=openid accounts payments'
    When method mtd
    Then match responseStatus == 400
    #And def Body = 'grant_type=&scope=openid accounts payments'


