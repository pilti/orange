@AISP
@AISP_API
@AISP_CCToken
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error Conditions for invalid client_id and client_secret

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Regression
  @severity=normal

  Scenario Outline: Verify that Status code 401 is displayed in the response when API is invoked by #key# value of client_id is sent in the request

    * def info = karate.info
    * def key = '<Role>' + ' TPP and ' + <key>
    * set info.subset = 'Client ID validation'

    Given json Application_Details = active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.client_id = <client_id>
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | Role      | key       | client_id |
      | AISP      | 'Invalid' | 'abcd'    |
      | AISP      | 'Empty'   | ' '       |
      | PISP      | 'Invalid' | 'abcd'    |
      | PISP      | 'Empty'   | ' '       |
      | AISP_PISP | 'Invalid' | 'abcd'    |
      | AISP_PISP | 'Empty'   | ' '       |

  @severity=normal

  Scenario Outline: Verify that Status code 401 is displayed in the response when API is invoked by #key# value of client_secret is sent in the request
    * def info = karate.info
    * def key = '<Role>' + ' TPP and ' + <key>
    * set info.subset = 'Client secret validation'

    Given json Application_Details = active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | Role      | key       | client_secret |
      | AISP      | 'Invalid' | 'abcd'        |
      | AISP      | 'Empty'   | ' '           |
      | PISP      | 'Invalid' | 'abcd'        |
      | PISP      | 'Empty'   | ' '           |
      | AISP_PISP | 'Invalid' | 'abcd'        |
      | AISP_PISP | 'Empty'   | ' '           |


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed in the response when API is invoked by #key# role client_id is sent in the request

    * def info = karate.info
    * def key = '<TPP1Role>' + ' TPP and ' + '<TPP2Role>'
    * set info.subset = 'Client ID validation -TPP'

    Given json Application_Details = active_tpp.<TPP1Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<TPP1Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.client_id = active_tpp.<TPP2Role>.client_id
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | TPP1Role  | TPP2Role  |
      | AISP      | AISP_PISP |
      | AISP      | PISP      |
      | PISP      | AISP      |
      | PISP      | AISP_PISP |
      | AISP_PISP | AISP      |
      | AISP_PISP | PISP      |


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed in the response when API is invoked by #key# role client_secret is sent in the request
    * def info = karate.info
    * def key = '<TPP1Role>' + ' TPP and ' + '<TPP2Role>'
    * set info.subset = 'Client secret validation -TPP'

    Given json Application_Details = active_tpp.<TPP1Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<TPP1Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.client_secret = active_tpp.<TPP2Role>.client_secret
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 401

    Examples:
      | TPP1Role  | TPP2Role  |
      | AISP      | AISP_PISP |
      | AISP      | PISP      |
      | PISP      | AISP      |
      | PISP      | AISP_PISP |
      | AISP_PISP | AISP      |
      | AISP_PISP | PISP      |
