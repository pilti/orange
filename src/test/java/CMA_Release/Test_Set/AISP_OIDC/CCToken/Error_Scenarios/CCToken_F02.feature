@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_OIDC
@AISP_CCToken
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error Conditions for invalid and missing scope

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Defect_1266
  @Defect_1398
  @severity=normal
  Scenario Outline: Verify that correct Status code is displayed in response when invalid scope is passed where #key#
    * def key = 'TPP Role is '+'<Role>'+' & Scope is '+<scope>
    * def key1 = ' Invalid scope'
    * def info = karate.info
    * set info.key = key
    * set info.subset =  key1
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

    Given json Application_Details = active_tpp.<Role>
    * call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = '<Role>'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == <Expected>


    Examples:
      | Scenario number | Role      | scope                                               | Expected |
      | '001'           | AISP_PISP | 'null'                                              | 400      |
      | '002'           | AISP_PISP | 'abcd'                                              | 400      |
      | '003'           | AISP_PISP | 'openid accounts accounts'                          | 200      |
      | '004'           | AISP_PISP | 'openid payments payments'                          | 200      |
      | '005'           | AISP_PISP | 'openid accounts payments openid accounts payments' | 200      |
      | '006'           | AISP      | 'ADA'                                               | 400      |
      | '007'           | AISP      | 'null'                                              | 400      |
      | '008'           | PISP      | 'null'                                              | 400      |
      | '009'           | PISP      | '##$$$'                                             | 400      |
      | '010'           | AISP      | 'openid accounts accounts'                          | 200      |
      | '011'           | AISP      | 'openid payments payments'                          | 400      |
      | '012'           | PISP      | 'openid accounts accounts'                          | 400      |
      | '013'           | PISP      | 'openid payments payments'                          | 200      |
      | '014'           | AISP      | 'openid accounts openid accounts'                   | 200      |
      | '015'           | PISP      | 'openid payments openid payments'                   | 200      |



