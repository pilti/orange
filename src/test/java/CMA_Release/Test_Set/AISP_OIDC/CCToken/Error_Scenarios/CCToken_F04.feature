@AISP
@AISP_API
@AISP_CCToken
@AISP_Error_Bus


Feature: This feature is to demonstrate URI and HTTP method validation

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Regression
  @severity=normal
  Scenario Outline: Verify that Status code 404 is received in the response when invalid URI#key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * def tokenUrl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + tokenUrl
    And set Application_Details $.TokenUrl = tokenUrl
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 404

    Examples:
      | key  | TokenUrl_SIT                                     | TokenUrl_UAT                                             |
      | '1'  | https://api.boitest.net/oauth/as/token           | https://api.u2.psd2.boitest.net/oauth/as/token           |
      | '2'  | https://api.boitest.net/oauth/as/oauth2          | https://api.u2.psd2.boitest.net/oauth/as/oauth2          |
      | '3'  | https://api.boitest.net/oauth/as/token.Oauth2    | https://api.u2.psd2.boitest.net/oauth/as/token.Oauth2    |
      | '4'  | https://api.boitest.net/oauth/as/Token.oauth2    | https://api.u2.psd2.boitest.net/oauth/as/Token.oauth2    |
      | '5'  | https://api.boitest.net/oauth/as//token.oauth2   | https://api.u2.psd2.boitest.net/oauth/as//token.oauth2   |
      | '6'  | https://api.boitest.net/oauth/token.oauth2       | https://api.u2.psd2.boitest.net/oauth/token.oauth2       |
      | '7'  | https://api.boitest.net/oauth//as/token.oauth2   | https://api.u2.psd2.boitest.net/oauth//as/token.oauth2   |
      | '8'  | https://api.boitest.net/as/token.oauth2          | https://api.u2.psd2.boitest.net/as/token.oauth2          |
      | '9'  | https://api.boitest.net/oauth/as/token.oauth2/// | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/// |
      | '10' | https://api.boitest.net/oauth/as/token.oauth     | https://api.u2.psd2.boitest.net/oauth/as/token.oauth     |
      | '11' | https://api.boitest.net/oauth/as/token.oauth2//  | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2//  |

  @Defect_804
  @severity=normal
  Scenario Outline: Verify that error is not received when URI#key# is sent with extra slashes

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * def tokenUrl = ( ActiveEnv=="SIT" ? '<TokenUrl_SIT>' : '<TokenUrl_UAT>')
    * print "Env is:" + ActiveEnv
    * print "URL is:" + tokenUrl
    And set Application_Details $.TokenUrl = tokenUrl
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200
    And match apiApp.Access_Token_CCG.response contains {"access_token": '#string'}
    * set Result.Reference = 'Defect_804'

    Examples:
      | key | TokenUrl_SIT                                   | TokenUrl_UAT                                           |
      | '1' | https://api.boitest.net//oauth/as/token.oauth2 | https://api.u2.psd2.boitest.net//oauth/as/token.oauth2 |
      | '2' | https://api.boitest.net/oauth/as/token.oauth2/ | https://api.u2.psd2.boitest.net/oauth/as/token.oauth2/ |

  @Regression
  @severity=normal
  Scenario Outline: Verify that error is received API is invoked with invalid method #key#

    * def info = karate.info
    * def key = <Method>
    * set info.subset = 'Method validation'

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.request_method = <Method>
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 405

    Examples:
      | Method     |
      | 'GET'      |
      | 'DELETE'   |
      | 'COPY'     |
      | 'LINK'     |
      | 'UNLINK'   |
      | 'PUT'      |
      | 'PURGE'    |
      | 'PROPFIND' |
      | 'OPTIONS'  |
      | 'LOCK'     |
      | 'UNLOCK'   |
      | 'HEAD'     |
      | 'PATCH'    |
      | 'VIEW'     |

