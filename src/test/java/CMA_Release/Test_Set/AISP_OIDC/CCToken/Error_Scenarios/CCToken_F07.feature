
#Feature: This feature is to demonstrate the Error Conditions for combinations of scope and TPP role with valid client credentials when certificate is not uploaded
#
#  Background:
#
#    * def apiApp = new apiapp()
#    * json Result = {}
#    * set Result.Testname = null
#    * configure afterScenario =
#      """
#      function(){
#       var tem = karate.info;
#       if (typeof key != 'undefined'){info.key = key}
#       if(tem.errorMessage == null){
#              Result.TestStatus = "Pass";
#        }else{
#              Result.TestStatus = "Fail";
#              Result.Error = tem.errorMessage;
#       }
#       apiApp.write(Result,info);
#       }
#      """
#
#
##############################################################################################################################################
#  @Query_1400
#  @severity=normal
#  Scenario Outline: Verify that access token is not generated (client credential grant) with body as grant_type=client_credentials&scope=#key#
#
#    Given def info = karate.info
#
#    * def key = '<scope> and TPP holding role <Role>'
#    * set info.key = key
#    * set info.subset = 'No Certificate'
#     #Set input with the request Method
#    And json Application_Details = active_tpp.<Role>
#    And set Application_Details $.TPPRole = '<Role>'
#    And set Application_Details $.scope = '<scope>'
#    And set Application_Details.request_method = 'POST'
#    And set Application_Details.grant_type = 'client_credentials'
#    And set Application_Details.Content_type = 'application/x-www-form-urlencoded'
#     ##########################################################################################################################################
#
#     #Call Access_Token_CCG API
#    When call apiApp.Access_Token_CCG(Application_Details)
#
#     #Validate response and update result json
#    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_scope = Application_Details.scope
#    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
#    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
#    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
#    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
#    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
#    And set Result.Additional_Details = Application_Details
#
#    And set Result.ActualOutput.Reference = 'Query_1400'
#
#
#    Examples:
#      | subset | Role      | scope                    | Expected |
#      | '001'  | AISP_PISP | openid accounts payments | 504      |
#      | '002'  | AISP      | openid accounts payments | 504      |
#      | '003'  | PISP      | openid accounts payments | 504      |
#
