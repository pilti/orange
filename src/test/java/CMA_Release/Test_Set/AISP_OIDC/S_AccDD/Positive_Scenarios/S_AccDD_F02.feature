@ignore
@Regression
Feature:  Verify response headers of Get direct-debits API (/accounts/{AccountId}/direct-debits API)

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
   ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

#############################################################################################################################################################
###################################################Scenario 1##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: To verify that Content-Type header in response of Single account Direct Debit API has value application json

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account direct-debits
    Given remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then remove inputJSON $.Content_type
    And match apiApp.Account_Direct_Debits.responseStatus == 200
    And match apiApp.Account_Direct_Debits.responseHeaders['Content-Type'][0] == 'application/json;charset=UTF-8'
  #############################################################################################################################################################
###################################################Scenario 2##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: To verify that value of x-fapi-interaction-id header in response is same as sent in request header of Single account direct debits API

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account direct-debits
    Given remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    And match apiApp.Account_Direct_Debits.responseHeaders['x-fapi-interaction-id'][0] == '93bac548-d2de-4546-b106-880a5018460d'

#############################################################################################################################################################
###################################################Scenario 3##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: To verify that x-fapi-interaction-id response header with unique uuid is generated when x-fapi-interaction-id header is not sent in GET Single account Direct Debits API request

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account Direct Debits
    Given remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + 'direct-debits'
    And remove reqHeader $.x-fapi-interaction-id
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    And match apiApp.Account_Direct_Debits.responseHeaders['x-fapi-interaction-id'][0] == '#uuid'

    #############################################################################################################################################################
###################################################Scenario 4##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  @Regression
  Scenario: To verify that API platform returns Http Status code 404 Not Found when Bulk Direct Debits API is triggered

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    #Get bulk Account direct-debits
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = BulkApiUrl + 'direct-debits'
    When call apiApp.Bulk_Direct_Debits(inputJSON)
    Then match apiApp.Bulk_Direct_Debits.responseStatus == 404