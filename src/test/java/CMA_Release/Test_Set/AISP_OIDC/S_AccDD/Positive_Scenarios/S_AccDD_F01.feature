@ignore
@Regression
Feature:  Get DirectDebits Information for consented Account by using /accounts/{AccountId}/direct-debits API

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
   ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_SingleAccDD
  @Functional_Shakedown
  @Functional_Shakedown_Quick
  Scenario: To verify TPP is able to call Single Account Direct Debits API and gets 200 OK response status
 # Get the Auth token and Refeshtoken using Authcode
    * print profile

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token

    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    And match apiApp.Multi_Account_Information.responseStatus == 200
    #Get Single Account Direct Debits
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id

    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then match apiApp.Account_Direct_Debits.responseStatus == 200

#############################################################################################################################################################
###################################################Scenario 2##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  @Regression
  Scenario: Get Direct Debits Information for an account by calling Single Account Direct Debits API with mandatory and optional headers
    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200
      ##Call Single account Beneficiaries API with the AccountID
    Given set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    And set reqHeader $.Accept = 'application/json'
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then set inputJSON $.RequestHeaders = reqHeader
    And match apiApp.Account_Direct_Debits.responseStatus == 200

#############################################################################################################################################################
###################################################Scenario 3##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: Get Direct Debits Information for an account by calling Single Account Direct Debits API only with mandatory headers

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token

    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    ##Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    And match apiApp.Account_Direct_Debits.responseStatus == 200


#############################################################################################################################################################
###################################################Scenario 4##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: To verify the response structure of Get Single Account Direct Debits API is CMA compliant

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200
    ##Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    And match apiApp.Account_Direct_Debits.responseStatus == 200
    And match apiApp.Account_Direct_Debits.response ==
    """
    { Data: { DirectDebit: [
     ]
    },
    Links: { Self: '#string' },
    Meta: { TotalPages: 1 } }        """

#############################################################################################################################################################
###################################################Scenario 5##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  @Regression
  Scenario: To verify that the response of Get Account Direct Debits API contains the mandatory field Links in appropriate format

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200
    ##Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then set inputJSON $.requestHeaders = reqHeader
    And match apiApp.Account_Direct_Debits.responseStatus == 200
    And match apiApp.Account_Direct_Debits.response.Links ==  {"Self": '#string' }

###########################################################################################################################################################################
###################################################Scenario 6##############################################################################################################
  @AISP_SingleAccDD
  @severity=normal
  Scenario: To verify that the response of Get Account Direct Debits API contains a mandatory field Meta in the payload

    * print profile
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200
    ##Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then set inputJSON $.requestHeaders = reqHeader
    And set inputJSON $.requestHeaders = reqHeader
    And set Result.Input = inputJSON
    And match apiApp.Account_Direct_Debits.responseStatus == 200
    And match apiApp.Account_Direct_Debits.response.Meta == { TotalPages: 1}
