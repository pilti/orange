#@AISP
#@AISP_API
#@AISP_SingleAccDD
#
#Feature:Verify HTTP status code 403 in response when access token having payments scope is passed in GET Single Account Direct debits API
#
#  Background:
#    * def apiApp = new apiapp()
#    * def info = karate.info
#    * json Result = {}
#    * configure afterScenario =
#      """
#      function(){
#       var info = karate.info;
#       if (typeof subset != 'undefined'){info.subset = subset}
#       if (typeof key != 'undefined'){info.key = key}
#       if(info.errorMessage == null){
#              Result.TestStatus = "pass";
#        }else{
#              Result.TestStatus = "fail";
#              Result.Error = info.errorMessage;
#       }
#       apiApp.write(Result,info);
#       }
#      """
#
#  @severity=normal

#  Scenario: Verify HTTP status code 403 received in response when access token having payments scope is passed in API request
#
#    # Get AISP acess Token using refresh token
#
#    * def fileName = 'Generic_G_User_1.properties'
#    * def name = 'Consent'
#    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
#    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
#    * print "Profile is: " + karate.pretty(profile)
#     ##########Configure Network Certificate####################################################################################
#    Given def Application_Details1 = active_tpp.AISP_PISP
#    * call apiApp.configureSSL(Application_Details1)
#    When set Application_Details1 $.grant_type = "refresh_token"
#    And set Application_Details1 $.request_method = 'POST'
#    And set Application_Details1 $.Content_type = 'application/x-www-form-urlencoded'
#    And set Application_Details1 $.refresh_token = profile.refresh_token
#    And set Application_Details1 $.token_type = 'Bearer'
#
#    #Call RTG API to generate access token
#    And call apiApp.Access_Token_RTG(Application_Details1)
#    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
#    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
#    And set Application_Details1 $.access_token = apiApp.Access_Token_RTG.response.access_token
#    * match apiApp.Access_Token_RTG.responseStatus == 200
#
#    ##Get AccounID from Multi Account Info API
#    Given set Application_Details1 $.request_method = "GET"
#    When call apiApp.Multi_Account_Information(Application_Details1)
#
#    * def account = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
#    * match apiApp.Multi_Account_Information.responseStatus == 200
#
#  ############################## Start PISP Flow ##################################################################
#
#    * def paymentId = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/payment_Setup.feature')
#    * def consent_Url = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/createConsentUrl.feature') paymentId.Result
#
#   # Test data to perform consent
#    * def strConsentUrl = consent_Url.Result.createConsentUrl.ConsentUrl
#    * def consentData = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes","url":'#(strConsentUrl)'}
#    * def performConsent = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/performConsent.feature') consentData
#
#    When set Application_Details $.grant_type = "authorization_code"
#    And set Application_Details $.redirect_uri = "https://www.getpostman.com/oauth2/callback"
#    And set Application_Details $.token_type = 'Bearer'
#    And set Application_Details $.code = performConsent.performConsentAction.AuthCode
#    And set Application_Details $.request_method = 'POST'
#    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
#
#    And call apiApp.Access_Token_ACG(Application_Details)
#    And set Result.Access_Token_ACG.responseStatus = apiApp.Access_Token_ACG.responseStatus
#    And set Result.Access_Token_ACG.response = apiApp.Access_Token_ACG.response
#    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
#    #######################################PISP FLOW END######################################################################
#
#    And set Application_Details $.AccountId = account
#    And set Application_Details $.request_method = 'GET'
#    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
#    When call apiApp.Account_Direct_Debits(Application_Details)
#    And set Result.ActualOutput.Account_Direct_Debits.Input.URL = Application_Details.URL
#    And set Result.ActualOutput.Account_Direct_Debits.Input.client_id = Application_Details.client_id
#    And set Result.ActualOutput.Account_Direct_Debits.Input.client_secret = Application_Details.client_secret
#    And set Result.ActualOutput.Account_Direct_Debits.Input.token_type = Application_Details.token_type
#    And set Result.ActualOutput.Account_Direct_Debits.Input.access_token = Application_Details.access_token
#    And set Result.ActualOutput.Account_Direct_Debits.Output.responseStatus =  apiApp.Account_Direct_Debits.responseStatus
#    And set Result.ActualOutput.Account_Direct_Debits.Output.response =  apiApp.Account_Direct_Debits.response
#    And set Result.Additional_Details = Application_Details
#    And match apiApp.Account_Direct_Debits.responseStatus == 403
#
