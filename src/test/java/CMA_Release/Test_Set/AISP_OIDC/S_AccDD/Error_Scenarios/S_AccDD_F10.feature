@AISP
@AISP_API
@AISP_SingleAccDD

Feature:  Account Status, Credit grade validation for the Account associated with Account ID

  Background:

    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    Given def info = karate.info
  ############To read Test Data###########################################################################################
    * def fileName = 'Generic_G_User_3.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
   ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)

    Given def Application_Details = active_tpp.AISP_PISP
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type

  ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

#########################################################################################################################################################
###############################Scenario 01###############################################################################################

  @severity=normal
    @anshu

  Scenario Outline: Verify HTTP status code 400 is received in response when API is triggered for the account which was consented but now #key#

    * def key = '<tc_desc>'
    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = <query>
    * def setPrerequisiteForDB = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 400

    And string b = <reverse_changes>
    And def reverseDBChanges = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,b)

    Examples:
    |       tc_desc             |       query                 | reverse_changes           |
    | changed to Credit grade 6 | "CREDIT_GRADING='6'"        | "CREDIT_GRADING='2'"      |
    | changed to Credit grade 7 | "CREDIT_GRADING='7'"        | "CREDIT_GRADING='2'"      |
    | changed to Inactive       | "ACCOUNT_STATUS='INACTIVE'" | "ACCOUNT_STATUS='ACTIVE'" |


