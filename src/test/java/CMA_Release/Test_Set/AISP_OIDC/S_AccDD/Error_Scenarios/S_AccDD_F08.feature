@AISP
@AISP_API
@AISP_SingleAccDD
@anshul2018
Feature: Accept parameter,token type and URI validation for Get Single Account Direct debits API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    Given def Application_Details = active_tpp.AISP_PISP

   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
   # Configure Network Certificate
    * call apiApp.configureSSL(profile)

      #To write test result and error details to output file

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """

  @Regression
  Scenario Outline:Verify whether error is not received when empty accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'

    # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200


    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
    # set empty value to Accept parameter
    And set reqHeader $.Accept = <Accept>
   # TPP triggers Direct Debits API with invalid Accept parameter value
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 200

    Examples:
      | Accept |
      | ' '    |

  @Regression
  Scenario Outline:Verify whether error is received when invalid accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'
  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
  # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
  # set invalid value to Accept parameter
    And set reqHeader $.Accept = <Accept>
 # TPP triggers Direct Debits API with invalid Accept parameter value
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 406

    Examples:
      | Accept             |
      | 'Application-json' |


  Scenario:Verify whether error is not received when accept header is not sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'
 # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And print apiApp.Access_Token_RTG.response

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId


    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
# remove Accept header from API headers
    And remove reqHeader.Accept
# TPP triggers Direct Debits API without Accept header
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 200


  Scenario Outline:Verify whether error is received when #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Token type validation'
# Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And print apiApp.Access_Token_RTG.response


    Given set Application_Details.request_method = 'GET'
    # Input invalid token type values in Authorization header
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
   # Input invalid token type values in Authorization header
    And set Application_Details.token_type = <type>
# TPP triggers Direct Debits API with invalid Accept parameter value
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 401

    Examples:
      | key                                                                                    | type                   |
      | 'Missing token type'                                                                   | " "                    |
      | 'Misspelled token type append single extra char to Bearer'                             | "Bearerr"              |
      | 'Invalid token type- append multiple chars to Bearer'                                  | "Bearerdfgdfhdh"       |
      | 'Invalid token type- append multiple aphanumeric chars to Bearer'                      | "Bearer5757"           |
      | 'Invalid token type- append multiple special chars to Bearer'                          | "Bearer$%&%4gd"        |
      | 'Invalid token type- placed multiple alphanumeric chars in between Bearer'             | "Beare577dgdfr"        |
      | 'Invalid token type- placed multiple alphanumeric and special chars in between Bearer' | "Bear%^*577dfgfddgdfr" |
      | 'Lower case token type'                                                                | "bearer"               |
      | 'Token type other than Bearer '                                                        | "Basic"                |



  Scenario Outline: Verify whether error is received when invalid URI#key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'
  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
 # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = <AcctInfoUrl> + <AccountId> + <endpoint>

 # TPP triggers Direct debits API with invalid URI

    Given url <AcctInfoUrl> + <AccountId> + <endpoint>
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    When method GET
    Then match responseStatus == 404

    #updated after defect 804
    Examples:
      |key|AcctInfoUrl|AccountId|endpoint|
      |'1'| apiServer + '/1/api/open-banking/v1.1/accountsss/'|Application_Details.AccountId|"/direct-debits"|
      |'2'| apiServer + '/1/api/open-banking/v1.1/Accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'3'| apiServer + '/1/api/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'4'| apiServer + '/1/api/open-banking/v1.1//accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'5'| apiServer + '/1/api/open-banking//v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'6'| apiServer + '/1/api//open-banking/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'7'| apiServer + '/1//api/open-banking/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'8'| apiServer + '/1/api/open-banking/v1.1/accounts//'|Application_Details.AccountId|"/direct-debits"|
      |'9'| apiServer + '/1/api/open-banking/v1.1/accounts///'|Application_Details.AccountId|"/direct-debits"|
      |'10'| apiServer + '/1/open-banking/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'11'| apiServer + '/1/api/open-banking/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits"|
      |'12'| apiServer + '/1/api/open-banking/v1.1/accounts/'|Application_Details.AccountId|"/direct-debits///"|
      |'13'| apiServer + '/1/api/open-banking/v1.1/accounts/'|Application_Details.AccountId|"//direct-debits/"|
      |'14'| apiServer + '/1/api/open-banking/v1.1/accounts/'|""|"/direct-debits"|

