@AISP
@AISP_API
@AISP_SingleAccDD

Feature: Account ID, financial id and scope validation for Get Single Account Direct debits API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * def apiApp1 = new apiapp()
    * json Result = {}
    * set Result.Testname = null

      #To write test result and error details to output file

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }

      """
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * def name = 'Consent'


  Scenario Outline:Verify whether error is received when account ID not associated with TPP #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Account ID validation'
    * def Application_Details = active_tpp.<TPP1>

  # To read Test Data
    * def fileName = <Consent_TPP1 specific>
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

  # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And print apiApp.Access_Token_RTG.response


    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
   # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given def apiApp1 = new apiapp()
    And def Application_Details = active_tpp.<TPP2>
   # To read Test Data
    And def fileName = <Consent_TPP2 specific>
    And json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    And call apiApp1.configureSSL(Application_Details)
  # Generate access_token from refresh_roken
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200
    And print apiApp1.Access_Token_RTG.response


    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
  #AccountId not associated with originating TPP is being sent in the request
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
 # TPP triggers direct-debits API for Account ID not associated with originating consent
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 403

    Examples:
      |key                  |TPP1     |TPP2             |Consent_TPP1 specific       |Consent_TPP2 specific       |
      |'AISP and PISP role' |AISP     |AISP_PISP        |'Generic_AISPTPP_G_User_1.properties'|'Generic_G_User_1.properties'        |
      |'AISP role'          |AISP_PISP|AISP             |'Generic_G_User_1.properties'        |'Generic_AISPTPP_G_User_1.properties'|

###to be done again
  Scenario:Verify whether error is received when consent is expired and accountId for which consent was provided is sent in APi request

    * def info = karate.info
    * def Application_Details = active_tpp.AISP_PISP

    # To read Test Data
    * def fileName = 'ExpiredConsent_Generic_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)

    # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

    # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And print apiApp.Access_Token_RTG.response

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.AccountId = profile.AccountId
    #AccountId not associated with originating TPP is being sent in the request
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
   # TPP triggers direct debits API for Account ID not associated with originating consent
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 403


  Scenario:Verify whether error is received when invalid financial header value is sent in API request

    * def info = karate.info
    * def Application_Details = active_tpp.AISP_PISP

  # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)

  # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
    # set invalid value to financial id parameter
    And set reqHeader $.x-fapi-financial-id = 'OB/BOI/002'
   # TPP triggers Direct debits API with invalid financial id
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 403



  Scenario:Verify whether error is received when empty value of financial header  is sent in API request

    * def info = karate.info
    * def Application_Details = active_tpp.AISP_PISP

  # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
  # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
    # set blank value to financial id parameter
    And set reqHeader $.x-fapi-financial-id = ' '
   # TPP triggers Direct Debits API with empty financial id
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 403


  Scenario:Verify whether error is received when financial header parameter is not sent in API request

    * def info = karate.info
    * def Application_Details = active_tpp.AISP_PISP

# To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
# Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

# Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
# set blank value to financial id parameter
    And remove reqHeader.x-fapi-financial-id
# TPP triggers Direct debits API without financial id
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 400

  @anshul123
  Scenario Outline:Verify whether error is not received when #key# value of scope sent in API request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Scope validation'
    * def Application_Details = active_tpp.AISP_PISP

# To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)

# Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

# Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
# Trigger MultipleAccountInfo API to get AccountID
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
  # TPP triggers Direct Debits API with invalid scope parameter
    And url Application_Details.URL
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    And header scope = <scope>
    When method GET
    Then match responseStatus == 200

    Examples:
      |key|scope|
      |'Invalid'|'accounts123'|
      |'Empty'  |'  '         |
