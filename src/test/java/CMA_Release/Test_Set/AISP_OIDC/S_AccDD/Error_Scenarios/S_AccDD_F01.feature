@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_SingleAccDD
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error Conditions for Direct Debits API.

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def seleniumFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when used refresh Token having #key# permission

     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def info = karate.info
    * def key = <condition>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Permission'

    Given def Application_Details = profile
    #Configure Network Certificate
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
     #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'

      #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Set input for Direct debits API
    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
    #Call direct debits API
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == <Expected>

    Examples:
      | Role      | condition                    | permissionsfile                               | Expected |
      | AISP_PISP | 'ReadAccountsDetail&Balance' | 'Only_Balance_Permission_G_User_1.properties' | 403      |



##########################################################################################################################################

  @Defect_1335
  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Different TPP #key#

    * def info = karate.info
    * def key = '<DifferentTPP>'
    * set info.key = '<Role>'
    * set info.subset = 'Different TPP'
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    Given def Application_Details = profile
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
        #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

     #Set input for direct debits API
    Given def Application_Details1 = active_tpp.<DifferentTPP>
    And set Application_Details1 $.token_type = 'Bearer'
    And set Application_Details1 $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details1.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details1.URL = AcctInfoUrl + Application_Details1.AccountId + '/direct-debits'
    And set Application_Details1 $.request_method = "GET"
    #Call direct debits API with Different Client Credential
    When call apiApp.Account_Direct_Debits(Application_Details1)
    Then set Result.ActualOutput.Account_Direct_Debits.Input.TPP2_Role = '<DifferentTPP>'
    And set Result.ActualOutput.Reference = 'Defect_1335'
    And match apiApp.Account_Direct_Debits.responseStatus == <Expected>

    Examples:
      | Role      | DifferentTPP | permissionsfile                             | Expected |
      | AISP      | PISP         | 'Generic_AISPTPP_G_User_1.properties'       | 403      |
      | AISP_PISP | AISP         | 'All_Basic_permissions_G_User_1.properties' | 403      |


#################################################################################################################################################

  @severity=normal
  @DD1
  Scenario Outline: Verify that Status code 400 is displayed when Invoked the API using Invalid AccountId #key#

    * def info = karate.info
    * def key = <AccountId>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid AccountId'
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(Application_Details)

    Given def Application_Details = active_tpp.<Role>
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'
            #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for direct debits API
    Given set Application_Details $.AccountId = <AccountId>
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
         #Call Direct debits API with invalid AccountId
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then set Result.ActualOutput.Account_Direct_Debits.Input.AccountId = <AccountId>
    And match apiApp.Account_Direct_Debits.responseStatus == <Expected>

    Examples:
      | Role      | AccountId | permissionsfile               | Expected |
      | AISP_PISP | 'abcd'    | 'Generic_G_User_1.properties' | 400      |


###############################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Invalid Access Token #key#

    * def info = karate.info
    * def key = <accesstoken>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Access Token'
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(Application_Details)

    Given def Application_Details = active_tpp.<Role>
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
        #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'

          #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

           #Set input for direct debits API
    Given set Application_Details $.access_token = <accesstoken>
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
           #Call direct debits API with invalid AccountId
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == <Expected>

    Examples:
      | Role      | accesstoken | permissionsfile               | Expected |
      | AISP_PISP | 'abcd'      | 'Generic_G_User_1.properties' | 401      |


#################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Expired Access Token for TPP Role #key#

    * def info = karate.info
    * def key = '<Role>'
    And set info.key = '<Role>'
    * set info.subset = 'Expired Access Token'
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
     #Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

    Given def Application_Details = active_tpp.<Role>
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'

        #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    And match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for direct debits API
    Given set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
      #Wait for 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And print msg
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)
         #Call direct debits API with invalid AccountId
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == <Expected>

    Examples:
      | Role      | permissionsfile               | Expected |
      | AISP_PISP | 'Generic_G_User_1.properties' | 401      |


    ############################################################################################################################################


  @Defect_1421
  Scenario: Verify whether error is received when consent is revoked and accountId for which consent was provided is sent in API request

    * def info = karate.info
  # To read Test Data
    * def fileName = 'RevokedConsent_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.AccountId = profile.AccountId
  #AccountId not associated with originating TPP is being sent in the request
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
 # TPP triggers direct debits API for Account ID not associated with originating consent
    When call apiApp.Account_Direct_Debits(Application_Details)
    Then match apiApp.Account_Direct_Debits.responseStatus == 403

###########################Scenario designed by Anu#########################################################
#    * def info = karate.info
#    And set info.Screens = 'Yes'
#    And set info.key = '<Role>'
#    * def key = '<Role>'
#    * set info.subset = 'Consent Revoked'
#
#    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
#    And def path = call  webApp.path1 = info1.path
#    * print info1.path
#
#    And set user_details $.usr = '247449'
#    And json Application_Details = active_tpp.<Role>
#
#    And set Application_Details.TestCasePath = info1.path
#    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
#
#    And json Result = callE2E.Result1
#
#    And set Result.Input.TPPDetails.TPPRole = '<Role>'
#    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
#    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
#
#    #Call Multi Account API
#    And set Application_Details.request_method = 'GET'
#    And set Application_Details.access_token = callE2E.apiApp.Access_Token_ACG.response.access_token
#
#    When call apiApp.Multi_Account_Information(Application_Details)
#
#    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus = apiApp.Multi_Account_Information.responseStatus
#    And set Result.ActualOutput.Multi_Account_Information.Output.response = apiApp.Multi_Account_Information.response
#    And set Result.ActualOutput.Multi_Account_Information.responseHeaders = apiApp.Multi_Account_Information.responseHeaders
#    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
#
#    #Call Delete API to revoke the consent
#    And set Application_Details.access_token = callE2E.apiApp.Access_Token_CCG.response.access_token
#    And set Application_Details.request_method = 'DELETE'
#    And set Application_Details.token_type = 'Bearer'
#    And set Application_Details.AccountRequestId = callE2E.Application_Details.AccountRequestId
#    And def AccountRequestId = Application_Details.AccountRequestId
#
#    And call apiApp.Account_Request_Delete(Application_Details)
#
#    And set Result.ActualOutput.Account_Request_Delete.Output.responseStatus = apiApp.Account_Request_Delete.responseStatus
#    And set Result.ActualOutput.Account_Request_Delete.Output.response = apiApp.Account_Request_Delete.response
#
#    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId + '/direct-debits'
#    And set Application_Details.access_token = callE2E.Result.Access_Token_ACG.response.access_token
#    And set Application_Details.request_method = 'GET'
#
#    #Call direct debits API
#    When call apiApp.Account_Direct_Debits(Application_Details)
#
#    And set Result.ActualOutput.Account_Direct_Debits.Input.Endpoint = Application_Details.URL
#    And set Result.ActualOutput.Account_Direct_Debits.Input.access_token = Application_Details.access_token
#    And set Result.ActualOutput.Account_Direct_Debits.Input.client_id = Application_Details.client_id
#    And set Result.ActualOutput.Account_Direct_Debits.Input.client_secret = Application_Details.client_secret
#
#    And set Result.ActualOutput.Account_Direct_Debits.Output.responseStatus = apiApp.Account_Direct_Debits.responseStatus
#    And set Result.ActualOutput.Account_Direct_Debits.Output.response = apiApp.Account_Direct_Debits.response
#    And set Result.Additional_Details = Application_Details
#
#    And match apiApp.Account_Direct_Debits.responseStatus == <Expected>
#
#    * print Result
#
#    Examples:
#      | Role      | Expected |
#      | AISP_PISP | 403     |
