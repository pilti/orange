@AISP_SingleAccDD
@abc123

Feature: Method validation for Get Single Account DirectDebits API

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    ############To read Test Data###########################################################################################
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
  ##########Configure Network Certificate####################################################################################
    * call apiApp.configureSSL(profile)
   ###############To write test result and error details to output file############
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  @severity=normal
  @Test112
  Scenario Outline: Verify HTTP Status code 405 is received in response when method #key# is used rather than GET for GetSingleAccount Direct Debits API
    # Get the Auth token and Refeshtoken using Authcode
    * def info = karate.info
    * print profile
    * def key = <condition>
    * set info.subset = key
    * set info.key = <condition>

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def auth = inputJSON.token_type + " " + inputJSON.access_token
    And set inputJSON $.request_method = "<method>"
    And  def body = ""
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then match apiApp.Account_Direct_Debits.responseStatus == 405

    Examples:
      | count | condition              | method   |
      | 001   | 'Input View method'    | VIEW     |
      | 002   | 'Input Copy method'    | COPY     |
      | 003   | 'Input PROFIND method' | PROPFIND |
      | 004   | 'Input PURGE method'   | PURGE    |
      | 005   | 'Input LOCK method'    | LOCK     |
      | 006   | 'Input UNLOCK method'  | UNLOCK   |
      | 007   | 'Input LINK method'    | LINK     |
      | 008   | 'Input UNLINK method'  | UNLINK   |
      #| 009   | 'Input PUT method'     | PUT      |
     # | 010   | 'Input PATCH method'   | PATCH    |
      | 011   | 'Input OPTIONS method' | OPTIONS  |
     # | 012    |'Input POST method'    | POST     |
      | 013   | 'Input HEAD method'    | HEAD     |
      | 014   | 'Input DELETE method'  | DELETE   |



  @Regression
  @severity=normal
  Scenario Outline: Test2Verify HTTP Status code 405 is received in response when method #key# is used rather than GET for GetSingleAccount Direct Debits API

    * def info = karate.info
    * print profile
    * def key = <condition>
    * set info.subset = key
    * set info.key = <condition>

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
      #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(inputJSON)
      #And set Result.Access_Token_RTG.Input = inputJSON
    Then match apiApp.Access_Token_RTG.responseStatus == 200

      ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def auth = inputJSON.token_type + " " + inputJSON.access_token
    And set inputJSON $.request_method = "<method>"
    And  def body = ""

    Given url AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    And headers reqHeader
    And header Authorization = auth
    And request body
    When method <method>

    Then match responseStatus == 405

    Examples:
      | count | condition              | method   |
      | 009   | 'Input PUT method'     | PUT      |
      | 010   | 'Input PATCH method'   | PATCH    |
      | 012   |'Input POST method'     | POST     |


