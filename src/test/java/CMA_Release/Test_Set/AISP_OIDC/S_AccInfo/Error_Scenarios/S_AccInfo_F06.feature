@AISP
@AISP_API
@AISP_SingleAccInfo
@AISP_SingleAccInfo_run

Feature: Account ID,TPP validation validation for Get Single Account Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP

   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   # Configure Network Certificate
    * call apiApp.configureSSL(Application_Details)

   # Generate access_token from refresh_roken
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * print apiApp.Access_Token_RTG.response
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details.request_method = 'GET'
    * set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    * set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

    # Trigger MultipleAccountInfo API to get AccountID
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

   #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  Scenario Outline:Verify whether error is received when invalid account ID #key# is passed in the request

    * def info = karate.info
    * def key = <AccountId>
    * set info.subset = 'AccountId validation'

    Given set Application_Details $.URL = AcctInfoUrl + <AccountId>
    And set Application_Details.AccountId = <AccountId>
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 400

    Examples:
      | AccountId                                 |
      | '12345'                                   |
      | Application_Details.AccountId + '123abcd' |
      | Application_Details.AccountId + '$#@123'  |


  Scenario Outline:Verify whether error is received when account ID not associated with consent having #key# is passed in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'AccountId validation'

    #TPP generates new access token to get Customer transactions information
    * def fileName1 = <Consent>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName1,name)
    * def apiApp1 = new apiapp()

    Given set Application_Details $.refresh_token = profile.refresh_token
    And set Application_Details.request_method = 'POST'
    When call apiApp1.Access_Token_RTG(Application_Details)
    And print apiApp1.Access_Token_RTG.response
    Then match apiApp1.Access_Token_RTG.responseStatus == 200
    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    When call apiApp1.Multi_Account_Information(Application_Details)
    And print apiApp1.Multi_Account_Information.response

  #AccountId not associated with originating consent is being sent in the request
    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

  # TPP triggers Account Information API for Account ID not associated with originating consent
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 403

    Examples:
      | key                             | Consent                                             |
      | 'Only_Account_info_permissions' | 'Only_Account_info_permissions_G_User_1.properties' |
      | 'Generic Permissions'           | 'Generic_G_User_2.properties'                       |


  @Defect_1335
  Scenario:Verify whether error is received when TPP having PISP role triggers API

    * def info = karate.info

    Given def Application_Details1 = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details1)
    And set Application_Details1 $.URL = AcctInfoUrl + Application_Details.AccountId
    And set Application_Details1.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details1.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details1.AccountId = Application_Details.AccountId
    And set Application_Details1.request_method = 'GET'

   # TPP having PISP role triggers transaction API where account ID and token will be taken from TPP having AISP role
    And call apiApp.Account_Information(Application_Details1)
    Then match apiApp.Account_Information.responseStatus == 401