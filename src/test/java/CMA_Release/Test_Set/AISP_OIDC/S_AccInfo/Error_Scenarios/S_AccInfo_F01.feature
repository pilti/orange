@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_Error_Bus
@AISP_SingleAccInfo_run

Feature: This feature is to test Error conditions of SingleAccountInfo API 

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  #Assert changed to be moved to positive
  Scenario Outline: Verify that Status code 200 is displayed when client credentials of different TPP is provided in the API request

    * def info = karate.info
    * def key = '<DifferentTPP>'
    * set info.key = '<Role>'
    * set info.subset = 'Different TPP'

         # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

         #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token

          #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    And print apiApp.Access_Token_RTG.response
    Then match apiApp.Access_Token_RTG.responseStatus == 200

            #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

     #Set input for Single Account Info API
    Given def Application_Details1 = active_tpp.<DifferentTPP>
    And set Application_Details1 $.token_type = 'Bearer'
    And set Application_Details1 $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details1.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details1.URL = AcctInfoUrl + Application_Details1.AccountId 
    And set Application_Details1 $.request_method = "GET"

     #Call Single Account Info API with Different Client Credential
    When call apiApp.Account_Information(Application_Details1)
    Then match apiApp.Account_Information.responseStatus == <Expected>
    And print Result

    Examples:
      | Role      | DifferentTPP | permissionsfile              | Expected |
      | AISP_PISP | AISP         | 'Generic_G_User_1.properties'         | 200     |


  Scenario Outline: Verify that Status code 400 is displayed when Invoked the API using Invalid AccountId #key#

    * def info = karate.info
    * def key = <AccountId>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid AccountId'

     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token

      #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    And print apiApp.Access_Token_RTG.response
    Then match apiApp.Access_Token_RTG.responseStatus == 200

        #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for Single Account Info API
    Given set Application_Details $.AccountId = <AccountId>
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId

         #Call Single Account Info API with invalid AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == <Expected>
    And print Result

    Examples:
      | Role      | AccountId | permissionsfile                           | Expected |
      | AISP_PISP | 'abcd'    | 'Only_Account_info_permissions_G_User_1.properties' | 400      |


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Invalid Access Token #key#

    * def info = karate.info
    * def key = <accesstoken>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Access Token'
      # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

      #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token

       #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    And print apiApp.Access_Token_RTG.response
    Then match apiApp.Access_Token_RTG.responseStatus == 200

         #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

          #Set input for Single Account Info API
    Given set Application_Details $.access_token = <accesstoken>
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId

          #Call Single Account Info API with invalid Access token
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == <Expected>
    And print Result

    Examples:
      | Role      | accesstoken | permissionsfile                           | Expected |
      | AISP_PISP | 'abcd'      | 'Only_Account_info_permissions_G_User_1.properties' | 401      |


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Expired Access Token for TPP Role #key#

    * def info = karate.info
    * def key = '<Role>'
    * set info.key = '<Role>'
    * set info.subset = 'Expired Access Token'

     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token

      #Call RTG API to generate access token
    And call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And print apiApp.Access_Token_RTG.response

        #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

         #Set input for Single Account Info API
    Given set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.URL = AcctInfoUrl + Application_Details.AccountId 

         #Wait for 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And print msg
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)

         #Call Single Account Info API with Expired access token
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == <Expected>
    And print Result

    Examples:
      | Role      | permissionsfile                           | Expected |
      | AISP_PISP | 'Only_Account_info_permissions_G_User_1.properties' | 401      |


  @Defect_1421
  Scenario: Verify that Status code 403 is displayed when Valid AccountID but the AccountRequest associated with the AccountID has been revoked using the DELETE endpoint

    * def info = karate.info

   # To read Test Data
    * def fileName = 'RevokedConsent_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   # Configure Network Certificate
    Given def Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    #Generate access_token from refresh_roken
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    And print apiApp.Access_Token_RTG.response
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #TPP triggers Single account Info API for Account ID  associated with revoked consent
    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.AccountId = profile.AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 403