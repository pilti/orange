@AISP
@AISP_SingleAccInfo
@AISP_SingleAccInfo_SIT1
@Regression

Feature: Verify the Account ID used in request of Get Single Account Details API

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    #Read refresh token from properties file
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
    * call apiApp.configureSSL(profile)
    #After scenario for output
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  @Functional_Shakedown_Quick_Run
  Scenario: Verify the response status is 200 when valid Account ID used in Get Single Account Details API

    * def info = karate.info
    * def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """

    Given set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    And set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    And call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}
    Given def inputJSONSI = inputJSON
    And set inputJSONSI.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Information(inputJSONSI)
    Then match apiApp.Account_Information.responseStatus == 200
    And match apiApp.Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}