@AISP
@AISP_API
@AISP_SingleAccBen

Feature:  Account Status, Credit grade validation for the Account associated with Account ID

  Background:

    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
  #To read Test Data#
    * def fileName = 'Generic_B_User_3.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate#
    * call apiApp.configureSSL(profile)
    * def Application_Details = active_tpp.AISP_PISP
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token

  #Call RTG API to generate access token
    * call apiApp.Access_Token_RTG(Application_Details)
    * set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    * set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type
    * match apiApp.Access_Token_RTG.responseStatus == 200

  ##Get AccounID from Multi Account Info API
    * set Application_Details $.request_method = "GET"
    * call apiApp.Multi_Account_Information(Application_Details)

    * set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * match apiApp.Multi_Account_Information.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @severity=normal
  Scenario: Verify HTTP status code 400 is received in response when API is triggered for the account which was consented but now changed to Credit grade 6

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
    #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "CREDIT_GRADING='6'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 400

    * string a = "CREDIT_GRADING='2'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

  @severity=normal
  Scenario: Verify HTTP status code 400 is received in response when  API is triggered for the account which was consented but now changed to Credit grade 7

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
   #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "CREDIT_GRADING='7'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 400

    * string a = "CREDIT_GRADING='2'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

  @severity=normal
  Scenario: Verify HTTP status code 400 is received in response when  API is triggered for the account which was consented but now is INACTIVE

    * def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    * def NSC = ( AccountNo.substring(0,6) )
   #To get the account number extracted from SortCodeAccountNumber
    * def AcNo = ( AccountNo.substring(6,14) )
    * string a = "ACCOUNT_STATUS='INACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    Given set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 400

    * string a = "ACCOUNT_STATUS='ACTIVE'"
    * def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
