@AISP
@AISP_API
@AISP_SingleAccBen
Feature: Access Token validation for Get Single Account Beneficiary Information API
  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """
  @severity=normal
  Scenario: Verify whether error is received when access token generated using auth_token not associated with requesting consent is sent in API request

    * def info = karate.info
    * def Application_Details = active_tpp.AISP_PISP
    * def prerequisite = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature")
    * def fileName = 'Generic_B_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And call apiApp.Multi_Account_Information(Application_Details)
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.access_token = prerequisite.apiApp.Access_Token_ACG.response.access_token
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 403

  @severity=normal
  Scenario Outline: Verify whether error is received when access_token not associated with TPP having #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access Token validation'
    Given def Application_Details = active_tpp.<TPP1>
    And call apiApp.configureSSL(Application_Details)
    And def fileName = <Consent_TPP1 specific>
    And def name = 'Consent'
    And def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    And json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    * def apiApp1 = new apiapp()
    Given def Application_Details = active_tpp.<TPP2>
    And call apiApp1.configureSSL(Application_Details)
    * def fileName = <Consent_TPP2 specific>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp1.Access_Token_RTG(Application_Details)
    Then match apiApp1.Access_Token_RTG.responseStatus == 200
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp1.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_RTG.response.access_token
    And call apiApp1.Multi_Account_Information(Application_Details)
    And set Application_Details.AccountId = apiApp1.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 401
    Examples:
      | key                  | TPP1      | TPP2      | Consent_TPP1 specific                 | Consent_TPP2 specific                 |
      | 'AISP and PISP role' | AISP      | AISP_PISP | 'Generic_AISPTPP_B_User_1.properties' | 'Generic_B_User_1.properties'         |
      | 'AISP role'          | AISP_PISP | AISP      | 'Generic_B_User_1.properties'         | 'Generic_AISPTPP_B_User_1.properties' |




