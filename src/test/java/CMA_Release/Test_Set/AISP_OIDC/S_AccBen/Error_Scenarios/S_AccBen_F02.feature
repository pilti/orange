@AISP
@AISP_API
@AISP_SingleAccBen
Feature: Client Credentials validation for Get Single Account Beneficiary Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'Generic_B_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    * set Application_Details $.grant_type = "refresh_token"
    * set Application_Details $.request_method = 'POST'
    * set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * set Application_Details $.refresh_token = profile.refresh_token
    * call apiApp.Access_Token_RTG(Application_Details)
    * match apiApp.Access_Token_RTG.responseStatus == 200
    * set Application_Details.request_method = 'GET'
    * set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    * set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    * call apiApp.Multi_Account_Information(Application_Details)
    * set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/beneficiaries'

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """



  @Defect_1335
  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-Invalid or empty client_id as #key#
    * def info = karate.info
    * def key = <client_id>
    * set info.subset = 'Client Id validation'
    Given set Application_Details $.client_id = <client_id>
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And set Result.DefectID = '1335'

    Examples:
      | client_id |
      | 'abcdef'  |
      | '   '     |


  @Defect_1335
  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-client_id associated with TPP having #key# role
    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client_id validation - TPP'
    Given set Application_Details $.client_id = active_tpp.<TPP>.client_id
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And set Result.DefectId = '1335'
    Examples:
      | TPP  |
      | AISP |
      | PISP |

  @Defect_1335
  @AISP_Error_Bus
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-Invalid or empty client_secret as #key#
    * def info = karate.info
    * def key = <client_secret>
    * set info.subset = 'Client secret validation'
    Given set Application_Details $.client_secret = <client_secret>
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And set Result.DefctId = '1335'
    Examples:
      | client_secret |
      | 'abcdef'      |
      | '   '         |

  @Defect_1335
  @AISP_Error_Tech
  @severity=normal
  Scenario Outline: Validate CID matches with client network certificate-client_secret associated with TPP having #key# role
    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'Client secret validation - TPP'
    Given set Application_Details $.client_secret = active_tpp.<TPP>.client_secret
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And set Result.DefectId = '1335'
    Examples:
      | TPP  |
      | AISP |
      | PISP |


  @Defect_1335
  @AISP_Error_Bus
  @severity=normal
  @Regression
  Scenario Outline: Validate client credentials match with client network certificate - Invalid or empty client_credentials as #key#

    * def info = karate.info
    * def key = <client_id> + <client_secret>
    * set info.subset = 'Client credentials validation'
    Given set Application_Details $.client_id = <client_id>
    And set Application_Details $.client_secret = <client_secret>
    When call apiApp.Customer_Beneficiaries(Application_Details)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And set Result.DefectId = '1335'
    Examples:
      | client_id | client_secret |
      | 'xyz'     | 'abcdef'      |
      | '  '      | '   '         |


