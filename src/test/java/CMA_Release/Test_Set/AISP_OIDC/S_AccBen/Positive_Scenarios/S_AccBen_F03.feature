@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_SingleAccBen
Feature:  To verify response of Single Account Beneficiaries API is as per the permission provided in consent-ReadBeneficiariesBasic

  Background:
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    #To read Test Data#
    * def fileName = "All_Basic_permissions_B_User_1.properties"
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
      * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
      * print profile

      #Configure Network Certificate#
      * call apiApp.configureSSL(profile)
      #To write test result and error details to output file#
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_SingleAccBen_rup01
  @severity=normal
  Scenario: To verify only basic information is retrieved by Beneficiaries API when only ReadBeneficiariesBasic permission is given

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    #And set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    #And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId

    ##Call Single account Beneficiaries API with the AccountID
    Given set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] ==
    """
    {
        "AccountId": "#string? _.length <= 40 && _.length >= 1",
        "BeneficiaryId": "#string? _.length <= 40 && _.length >= 1",
        "Reference": "##string? _.length <= 35 && _.length >= 1"
    }

    """
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] contains { CreditorAccount: '#notpresent' }
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] contains{ Servicer: '#notpresent'}


  @AISP_SingleAccBen
  @severity=normal
  Scenario: Get Beneficiaries Basic information for an account by calling Single Account Beneficiaries API with mandatory and optional headers
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then match apiApp.Multi_Account_Information.responseStatus == 200
      ##Call Single account Beneficiaries API with the AccountID
    Given set reqHeader $.x-fapi-customer-last-logged-time = 'Sun, 10 Sep 2017 19:43:31 UTC'
    And set reqHeader $.x-fapi-customer-ip-address = '127.0.0.1'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    And set reqHeader $.Accept = 'application/json'
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Customer_Beneficiaries(inputJSON)
    And set inputJSON $.RequestHeaders = reqHeader
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200


  @AISP_SingleAccBen_03r
  @severity=normal
  Scenario: Get Beneficiaries Basic information for an account by calling Single Account Beneficiaries API only with mandatory headers

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    ##Call Single account Beneficiaries API with the AccountID
    Given remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Customer_Beneficiaries(inputJSON)
    And set inputJSON $.requestHeaders = reqHeader
  Then match apiApp.Customer_Beneficiaries.responseStatus == 200
