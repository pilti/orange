@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_SingleAccBen

Feature: Verify response headers of Get beneficiaries API (/accounts/{AccountId}/beneficiaries API)
  Background:
    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    #To read Test Data#
    * def fileName = 'Generic_B_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   #Configure Network Certificate#
    * call apiApp.configureSSL(profile)
   #To write test result and error details to output file#
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_SingleAccBen
  @severity=normal
  Scenario: To verify that content type header in response of Single Account Beneficiaries API
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account beneficiaries
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/beneficiaries'
    When call apiApp.Customer_Beneficiaries(inputJSON)
    And remove inputJSON $.Content_type
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And match apiApp.Customer_Beneficiaries.responseHeaders['Content-Type'][0] == 'application/json;charset=UTF-8'


  @AISP_SingleAccBen
  @severity=normal
  Scenario: To verify that value of x-fapi-interaction-id header in response is same as sent in request header of Single Account Beneficiaries API

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account beneficiaries
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/beneficiaries'
    And set reqHeader $.x-fapi-interaction-id = "93bac548-d2de-4546-b106-880a5018460d"
    When call apiApp.Customer_Beneficiaries(inputJSON)
    And remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    Then match apiApp.Customer_Beneficiaries.responseHeaders['x-fapi-interaction-id'][0] == '93bac548-d2de-4546-b106-880a5018460d'

  @AISP_SingleAccBen
  @severity=normal
  @Regression
  Scenario: To verify that x-fapi-interaction-id response header with unique uuid is generated when x-fapi-interaction-id header is not sent in Single Account beneficiaries API request

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """

    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    And set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Get Single Account beneficiaries
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    Given set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + 'beneficiaries'
    And remove reqHeader $.x-fapi-interaction-id
    When call apiApp.Customer_Beneficiaries(inputJSON)
    And remove inputJSON $.Content_type
    And set inputJSON $.requestHeaders = reqHeader
    Then match apiApp.Customer_Beneficiaries.responseHeaders['x-fapi-interaction-id'][0] == '#uuid'

  @AISP_SingleAccBen
  @severity=normal
  Scenario: To verify that API platform returns Http Status code 404 Not Found when Bulk Beneficiaries API is triggered

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    #And set Result.Access_Token_RTG.Input = inputJSON
    And set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And remove inputJSON $.refresh_token
    ##Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    #Get bulk Account Beneficiaries
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = BulkApiUrl + 'beneficiaries'
    When call apiApp.Bulk_Beneficiaries(inputJSON)
    Then match apiApp.Bulk_Beneficiaries.responseStatus == 404
