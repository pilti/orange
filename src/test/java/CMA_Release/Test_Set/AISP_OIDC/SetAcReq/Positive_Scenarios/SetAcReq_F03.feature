@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@Regression

Feature: This feature is to demonstrate the Scenario for Past Dated TransactionFromDate in Permission.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

   #################################################################################################################################################################
  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed in the response of Account Setup Request when #key# is passed
###################################################################################################################################################
    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Past Date in TransactionFromDate'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
###################################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.ExpirationDateTime = <ExpirationDate>
    And set permissions $.Data.TransactionFromDateTime = <TransactionFromDate>
    And set permissions $.Data.TransactionToDateTime = <TransactionToDate>
##################################Call feature file to Create Consent URL #################################################################################
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And call apiApp.Access_Token_CCG(Application_Details)
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And match apiApp.Access_Token_CCG.responseStatus == 200
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And call apiApp.Account_Request_Setup(Application_Details)
    And set Result.ActualOutput.Account_Request_Setup.Input.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.ActualOutput.Account_Request_Setup.Input.permissions = permissions
    And set Result.ActualOutput.Account_Request_Setup.Input.ExpirationDateTime = <ExpirationDate>
    And set Result.ActualOutput.Account_Request_Setup.Input.TransactionFromDate = <TransactionFromDate>
    And set Result.ActualOutput.Account_Request_Setup.Input.TransactionToDate = <TransactionToDate>
    And set Result.ActualOutput.Account_Request_Setup.Output.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.ActualOutput.Account_Request_Setup.Output.response = apiApp.Account_Request_Setup.response
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


#Rest all combination are part of CR20
    Examples:
      | Srno  | Condition                  | ExpirationDate              | TransactionFromDate         | TransactionToDate           | Expected |
      | '001' | 'Past TransactionFromDate' | '2018-11-30T00:00:00+00:00' | '2010-03-03T00:00:00+00:00' | '2017-09-30T00:00:00+00:00' | 201      |
