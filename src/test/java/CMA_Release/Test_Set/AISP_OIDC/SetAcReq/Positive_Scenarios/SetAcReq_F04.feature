@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@Regression

Feature: This feature is to demonstrate the Error Condition when required scope is not passed.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """
###########################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when valid scope is passed like #key#

##############################################################################################################################################

    * def key = 'TPP Role is '+'<Role>'+' & Scope is '+<Scope>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Valid scope'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

  ####################################################################################################################################
    Given json Application_Details = active_tpp.<Role>
  #################################################################################################################################################
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
  ##########################################################################################################################################
    And set Application_Details $.scope = <Scope>
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Result.ActualOutput.Access_Token_CCG.Input.TPPRole = '<Role>'
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_CCG.Input.scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    Then match apiApp.Access_Token_CCG.responseStatus == 200

  ##########################################################################################################################################

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.Content_type = 'application/json'
  #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)
    And set Result.ActualOutput.Account_Request_Setup.Input.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.ActualOutput.Account_Request_Setup.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Account_Request_Setup.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Account_Request_Setup.Output.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.ActualOutput.Account_Request_Setup.Output.response = apiApp.Account_Request_Setup.response
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Scenario number | Role      | Scope                      | Expected |
      | '001'           | AISP_PISP | 'openid accounts payments' | 201      |
      | '002'           | AISP_PISP | 'openid accounts'          | 201      |
      | '003'           | AISP      | 'openid accounts'          | 201      |
      | '004'           | AISP_PISP | 'payments accounts openid' | 201      |
      | '005'           | AISP_PISP | 'payments openid accounts' | 201      |
      | '006'           | AISP_PISP | 'accounts'                 | 201      |







