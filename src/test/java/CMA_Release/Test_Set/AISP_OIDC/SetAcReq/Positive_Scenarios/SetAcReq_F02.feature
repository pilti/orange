@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@Regression
@test
Feature: This feature is to demonstrate the Positive flow till consent when Date is missing in Permissions.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  #################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed and successfully navigate to Consent Screen when #key# is passed
  ###################################################################################################################################################
    * def key = <Condition>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Missing Date in Permissions_'+<Condition>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path

  ###################################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And remove <Date>
    And set Result.ActualOutput.Account_Request_Setup.Input.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.ActualOutput.Account_Request_Setup.Input.permissions = permissions
  ##################################Call feature file to Create Consent URL #################################################################################
    And def CreateConsentURL = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    When def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    And def consenturl = CreateConsentURL.Result.CreateRequestObject.URL
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle
    And set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts


    Examples:
      | Srno  | Condition                      | Date                                       |
      | '001' | 'Missing Expiration Date'      | permissions $.Data.ExpirationDateTime      |
      | '002' | 'Missing TransactionFrom Date' | permissions $.Data.TransactionFromDateTime |
      | '003' | 'Missing TransactionTo Date'   | permissions $.Data.TransactionToDateTime   |
