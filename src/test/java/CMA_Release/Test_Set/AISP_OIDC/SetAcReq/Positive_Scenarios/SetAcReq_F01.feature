@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
Feature: This feature is to demonstrate the Positive flow till consent for valid combination of Permissions.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """

  #################################################################################################################################################################

  @severity=normal
  @Functional_Shakedown

  Scenario Outline: Verify that Consent is completed when Account Setup Request API is submitted with valid permission combination like #key#

    * def key = <Condition>
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Part1_Valid Permissions_'+<Condition>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

#Set TPP Role and scope
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
    And def CreateConsentURL = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    And def consenturl = CreateConsentURL.Result.CreateRequestObject.URL
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    When def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)
    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.idtoken = perform.Action.idtoken
    #And set Additional_Details = Application_Details
    And eval if (perform.Action.Authcode != '' &&  perform.Action.idtoken!='' ) {perform.Status = "Pass"} else {perform.Status = "Fail"}
     #And set Result.Status = perform.Status
    Then match perform.Status == 'Pass'



    Examples:
      | Srno  | Condition                       | permissions                                                                                                                                                                                                                                                                                            |
      | '001' | 'All Permissions'               | ["ReadAccountsBasic","ReadAccountsDetail","ReadBalances","ReadBeneficiariesBasic","ReadBeneficiariesDetail","ReadDirectDebits","ReadProducts","ReadStandingOrdersBasic""ReadStandingOrdersDetail","ReadTransactionsBasic","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"] |
      | '002' | 'Only ReadAccountsBasic'        | ["ReadAccountsBasic"]                                                                                                                                                                                                                                                                                  |
      | '003' | 'Only ReadAccountsDetail'       | ["ReadAccountsDetail"]                                                                                                                                                                                                                                                                                 |
      | '004' | 'Only ReadBalances'             | ["ReadBalances"]                                                                                                                                                                                                                                                                                       |
      | '005' | 'Only ReadBeneficiariesBasic'   | ["ReadBeneficiariesBasic"]                                                                                                                                                                                                                                                                             |
      | '006' | 'Only ReadBeneficiariesDetail'  | ["ReadBeneficiariesDetail"]                                                                                                                                                                                                                                                                            |
      | '007' | 'Only ReadDirectDebits'         | ["ReadDirectDebits"]                                                                                                                                                                                                                                                                                   |
      | '008' | 'Only ReadProducts'             | ["ReadProducts"]                                                                                                                                                                                                                                                                                       |
      | '009' | 'Only ReadStandingOrdersBasic'  | ["ReadStandingOrdersBasic"]                                                                                                                                                                                                                                                                            |
      | '010' | 'Only ReadStandingOrdersDetail' | ["ReadStandingOrdersDetail"]                                                                                                                                                                                                                                                                           |


################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Consent is completed when Account Setup Request API is submitted with valid permission combination like #key#

  ###################################################################################################################################################
    * def key = <Condition>
      #* def key1 = ' Part2_Valid Permissions'
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Part2_Valid Permissions_'+<Condition>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path
    * print info1.path


  ###################################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
  ##################################Call feature file to Create Consent URL #################################################################################
    And def CreateConsentURL = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details
    When def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def consenturl = CreateConsentURL.Result.CreateRequestObject.URL
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)
    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.idtoken = perform.Action.idtoken
    #And set Additional_Details = Application_Details
    And eval if (perform.Action.Authcode != '' &&  perform.Action.idtoken!='' ) {perform.Status = "Pass"} else {perform.Status = "Fail"}
     #And set Result.Status = perform.Status
    Then match perform.Status == 'Pass'



    Examples:
      | Srno  | Condition                                        | permissions                                      |
      | '011' | 'ReadBalances and ReadAccountsBasic'             | ["ReadBalances","ReadAccountsBasic"]             |
      | '012' | 'ReadAccountsDetail and ReadBalances'            | ["ReadAccountsDetail","ReadBalances"]            |
      | '013' | 'ReadAccountsBasic and ReadBeneficiariesBasic'   | ["ReadAccountsBasic","ReadBeneficiariesBasic"]   |
      | '014' | 'ReadAccountsBasic and ReadBeneficiariesDetail'  | ["ReadAccountsBasic","ReadBeneficiariesDetail"]  |
      | '015' | 'ReadAccountsDetail and ReadBeneficiariesBasic'  | ["ReadAccountsDetail","ReadBeneficiariesBasic"]  |
      | '016' | 'ReadAccountsDetail and ReadBeneficiariesDetail' | ["ReadAccountsDetail","ReadBeneficiariesDetail"] |
      | '017' | 'ReadAccountsBasic and ReadDirectDebits'         | ["ReadAccountsBasic","ReadDirectDebits"]         |
      | '018' | 'ReadAccountsDetail and ReadDirectDebits'        | ["ReadAccountsDetail","ReadDirectDebits"]        |
      | '019' | 'ReadAccountsBasic and ReadProducts'             | ["ReadAccountsBasic","ReadProducts"]             |
      | '020' | 'ReadAccountsDetail and ReadProducts'            | ["ReadAccountsDetail","ReadProducts"]            |


  #######################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Consent is completed when Account Setup Request API is submitted with valid permission combination like #key#

###################################################################################################################################################
    * def key = <Condition>
    #* def key1 = ' Part3_Valid Permissions'
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.key = key
    * set info.subset = 'Part3_Valid Permissions_'+<Condition>
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * print info1.path


###################################Set TPP Role and scope ######################################################################
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>

##################################Call feature file to Create Consent URL #################################################################################

    And def CreateConsentURL = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreReq_ConstructConsentURL.feature') Application_Details

    When def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = info1.path
    And def consenturl = CreateConsentURL.Result.CreateRequestObject.URL
    And def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page
    And def perform = Functions.useKeyCodeApp(webApp)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page
    * def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    And def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.ScaLogin_Status = perform.Status
    And set Result.ActualOutput.UI.Output.ScaLogin_Landing_Page = perform.Landing_Page
    And def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_Status = perform.Status
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_PageTitle = perform.PageTitle

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)
    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.Permissions = perform.Permissions
    And set Result.ActualOutput.UI.Output.ReviewConfirm.ConsentExpiry = perform.ConsentExpiry
    And set Application_Details.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.code = perform.Action.Authcode
    And set Result.ActualOutput.UI.Output.idtoken = perform.Action.idtoken
    #And set Additional_Details = Application_Details
    And eval if (perform.Action.Authcode != '' &&  perform.Action.idtoken!='' ) {perform.Status = "Pass"} else {perform.Status = "Fail"}
     #And set Result.Status = perform.Status
    Then match perform.Status == 'Pass'



    Examples:
      | Srno  | Condition                                                                   | permissions                                                                   |
      | '021' | 'ReadAccountsBasic and ReadStandingOrdersBasic'                             | ["ReadAccountsBasic","ReadStandingOrdersBasic"]                               |
      | '022' | 'ReadAccountsDetail and ReadStandingOrdersDetail'                           | ["ReadAccountsDetail","ReadStandingOrdersDetail"]                             |
      | '023' | 'ReadAccountsBasic and ReadStandingOrdersBasic'                             | ["ReadAccountsBasic","ReadStandingOrdersBasic"]                               |
      | '024' | 'ReadAccountsDetail and ReadStandingOrdersDetail'                           | ["ReadAccountsDetail","ReadStandingOrdersDetail"]                             |
      | '025' | 'ReadTransactionsBasic ReadTransactionsCredits and ReadTransactionsDebits'  | ["ReadTransactionsBasic","ReadTransactionsCredits","ReadTransactionsDebits"]  |
      | '026' | 'ReadTransactionsBasic and ReadTransactionsCredits'                         | ["ReadTransactionsBasic","ReadTransactionsCredits"]                           |
      | '027' | 'ReadTransactionsBasic and ReadTransactionsDebits'                          | ["ReadTransactionsBasic","ReadTransactionsDebits"]                            |
      | '028' | 'ReadTransactionsDetail ReadTransactionsCredits and ReadTransactionsDebits' | ["ReadTransactionsDetail","ReadTransactionsCredits","ReadTransactionsDebits"] |
      | '029' | 'ReadTransactionsDetail and ReadTransactionsCredits'                        | ["ReadTransactionsDetail","ReadTransactionsCredits"]                          |
      | '030' | 'ReadTransactionsDetail and ReadTransactionsDebits'                         | ["ReadTransactionsDetail","ReadTransactionsDebits"]                           |





