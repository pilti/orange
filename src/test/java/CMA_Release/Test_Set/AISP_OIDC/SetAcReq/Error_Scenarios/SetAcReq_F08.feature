@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error Condition when Token is expired.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """

###########################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Expired token is passed for TPP #key#

    * def key = '<Role>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Expired Token'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given json Application_Details = active_tpp.<Role>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = <Scope>
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.Content_type = 'application/json'
 #Call Account_Request_Setup API
    And def msg = "Wait Applied to expire the token"
    And print msg
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(300000)
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Scenario number | Role      | Scope             | Expected |
      | '001'           | AISP_PISP | 'openid accounts' | 401      |
      | '002'           | AISP      | 'openid accounts' | 401      |