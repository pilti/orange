@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error Condition for invalid Financial Id.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """


  #################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 403 is displayed when #key# Financial Id is passed

    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Financial Id'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
#Set TPP Role and scope
    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set reqHeader $.x-fapi-financial-id = <FinancialId>
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Srno  | Condition | FinancialId | Expected |
      | '001' | 'Invalid' | 'abcd'      | 403      |
      | '002' | 'Null'    | 'null'      | 403      |
      | '003' | 'Blank'   | ''          | 403      |


 #################################################################################################################################################################
  @Defect_1541
  @severity=normal
  Scenario Outline: Verify that Status code 415 is displayed when #key# Content Type is passed

    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Content Type'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
#Set TPP Role and scope
    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set reqHeader $.Content-Type = <ContentType>
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    And set Result.ActualOutput.Reference = 'Defect_1541'
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Srno  | Condition | ContentType | Expected |
      | '001' | 'Invalid' | 'abcd'      | 415      |
      | '002' | 'Null'    | 'null'      | 415      |
      | '003' | 'Blank'   | ''          | 415      |