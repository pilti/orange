@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error Condition for invalid date in Permission.

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       webApp.stop();
       }
      """
  #################################################################################################################################################################
  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed when #key# is passed

    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Date in Permissions'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

  #Set TPP Role and scope
    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.ExpirationDateTime = <ExpirationDate>
    And set permissions $.Data.TransactionFromDateTime = <TransactionFromDate>
    And set permissions $.Data.TransactionToDateTime = <TransactionToDate>
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Srno  | Condition                      | ExpirationDate              | TransactionFromDate         | TransactionToDate           | Expected |
      | '001' | 'Invalid Expiration Date'      | '2018-11-31T00:00:00+00:00' | '2018-03-03T00:00:00+00:00' | '2018-11-03T00:00:00+00:00' | 400      |
      | '002' | 'Invalid TransactionFrom Date' | '2018-12-29T00:00:00+00:00' | '2018-04-31T00:00:00+00:00' | '2018-12-03T00:00:00+00:00' | 400      |
      | '003' | 'Invalid TransactionTo Date'   | '2018-11-30T00:00:00+00:00' | '2018-03-03T00:00:00+00:00' | '2018-09-31T00:00:00+00:00' | 400      |
