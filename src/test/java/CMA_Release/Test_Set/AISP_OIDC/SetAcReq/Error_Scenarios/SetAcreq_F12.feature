@AISP
@AISP_API
@AISP_SetAcReq
@AISP_Error_Bus


Feature: Client credential token and token_type validation for Account request Set up API

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """


  @severity=normal
  Scenario: Verify that Status cod 401 is displayed when API is triggered without authorization parameter

    * def info = karate.info
    * set info.subset = 'Missing Authorisation Parameter'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And url AcctReqUrl
    And headers reqHeader
    And request permissions
    When method POST

    And match responseStatus == 401

    ############################################################################################################################################


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when API is triggered with #key# authorization parameter

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access_token validation'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    #Input invalid or empty value of client credential token
    And set Application_Details.access_token = <token>
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 401


    Examples:
      | key       | token       |
      | 'Empty'   | ' '         |
      | 'invalid' | 'abcdf1234' |

    ###########################################################################################################################################

  @severity=normal
  Scenario: Verify that Status code 201 is displayed when API is triggered with client_credential token which is about to expire

    * def info = karate.info
    * set info.subset = 'Access_token validation'

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And print "Wait starts"
    And def result = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(290000)
    And print "wait ends"
    #Hit API with client credential token which will be expired in a sec
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    ######################################################################################################################################


  @severity=normal
  Scenario: Verify that Status code 401 is displayed when API is triggered with access_token having Authorization_code grant

    * def info = karate.info
    * set info.subset = 'Use Access token having Authorization_code grant'

      # To read Test Data
    * def fileName = 'Generic.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Input access token having authorization_code grant in API request
    Given set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 401

    And set Result.ActualOutput.Reference = 'Defect_1426'
    #####################################################################################################################################

  @Defect_1426
  @severity=normal
  Scenario: Verify that Status code 401 is displayed when API is triggered with access_token having refresh_token grant

    * def info = karate.info
    * set info.subset = 'Use Access_token having refresh_token grant'
   # To read Test Data
    * def fileName = 'Generic.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.token_type = 'Bearer'
    # Input refresh_token in API request
    And set Application_Details.access_token = profile.refresh_token
    And set Application_Details.request_method = 'POST'
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 401

    And set Result.ActualOutput.Reference = 'Defect_1426'