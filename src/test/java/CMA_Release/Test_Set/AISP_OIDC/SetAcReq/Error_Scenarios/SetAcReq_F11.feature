@AISP
@AISP_API
@AISP_SetAcReq
@AISP_Error_Bus


Feature: Method,URI and TPP validations for Account request Set up API

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """


  @severity=normal
  Scenario Outline: Verify that Status code 405 is displayed when API is triggered with #key# method

    * def info = karate.info
    * def key = <Method>
    * set info.key = key
    * set info.subset = 'Method validation'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.response.Status == 200

   #Trigger API with not allowed method
    Given set Application_Details.request_method = <Method>
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 405


    Examples:
      | Method     |
      | 'GET'      |
      | 'DELETE'   |
      | 'COPY'     |
      | 'LINK'     |
      | 'UNLINK'   |
      | 'PUT'      |
      | 'PURGE'    |
      | 'PROPFIND' |
      | 'OPTIONS'  |
      | 'LOCK'     |
      | 'UNLOCK'   |
      | 'HEAD'     |
      | 'PATCH'    |
      | 'VIEW'     |



    #############################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 404 is displayed when API is triggered with Invalid URI #key#

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'URI validation'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And def AcctRequestUrl = ( ActiveEnv=="SIT" ? '<AcctRequestUrl_SIT>' : '<AcctRequestUrl_UAT>')
    And set Application_Details.AcctReqUrl = AcctRequestUrl
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | key  | AcctRequestUrl_SIT                                                        | AcctRequestUrl_UAT                                                                | Expected |
      | '1'  | https://api.boitest.net/1/api/open-banking/v1.1/Account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Account-requests    | 404      |
      | '2'  | https://api.boitest.net/1/api/open-banking/v1.1/accountrequests     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountrequests     | 404      |
      | '3'  | https://api.boitest.net/1/api/open-banking/v1.1/account-request     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-request     | 404      |
      | '4'  | https://api.boitest.net/1/api/open-banking/v1.1//account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//account-requests   | 404      |
      | '5'  | https://api.boitest.net/1/api/open-banking/v1.0/account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.0/account-requests    | 404      |
      | '6'  | https://api.boitest.net/1/api/open-banking//v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/account-requests   | 404      |
      | '7'  | https://api.boitest.net/1/api//open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/account-requests   | 404      |
      | '8'  | https://api.boitest.net/1/api/v1.1/account-requests                 | https://api.u2.psd2.boitest.net/1/api/v1.1/account-requests                 | 404      |
      | '9'  | https://api.boitest.net/1//api/open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/account-requests   | 404      |
      | '10' | https://api.boitest.net/1//open-banking/v1.1/account-requests       | https://api.u2.psd2.boitest.net/1//open-banking/v1.1/account-requests       | 404      |
      | '11' | https://api.boitest.net/1/api/open-banking//account-requests        | https://api.u2.psd2.boitest.net/1/api/open-banking//account-requests        | 404      |
      | '12' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests/// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests/// | 404      |

    #################################################################################################################################


  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when API is triggered with extra forward slashes placed in URI#key#

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'URI validation'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And def AcctRequestUrl = ( ActiveEnv=="SIT" ? '<AcctRequestUrl_SIT>' : '<AcctRequestUrl_UAT>')
    And set Application_Details.AcctReqUrl = AcctRequestUrl
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | key | AcctRequestUrl_SIT                                                       | AcctRequestUrl_UAT                                                               | Expected |
      | '1' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests/  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests/  | 201      |
      | '2' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests// | 201      |
      | '3' | https://api.boitest.net//1/api/open-banking/v1.1/account-requests/ | https://api.u2.psd2.boitest.net//1/api/open-banking/v1.1/account-requests/ | 201      |



    ##################################################################################################################################

  @Defect_1335
  @severity=normal
  Scenario: Verify that Status code 201 is displayed when API is triggered with TPP for which network certificate is not configured

    * def info = karate.info
    * set info.key = 'Yes'
    * set info.subset = 'Network Certificate configured for different TPP'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

   #Trigger API with TPP for which certificate is not configured
    Given json Application_Details = active_tpp.AISP
    And set Application_Details.request_method = 'POST'
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Result.ActualOutput.Reference = 'Defect_1335'

  ###############################################################################################################################################

  @Defect_1335
  @Defect_1426
  @severity=normal
  Scenario: Verify that Status code 401 is displayed when API is triggered with TPP having PISP role and token provided has AISP and PISP scopes

    * def info = karate.info
    * set info.key = 'Yes'
    * set info.subset = 'Token provided of Different TPP'

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts payments'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

     #Trigger API with TPP having PISP role
    Given json Application_Details = active_tpp.PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 401
    And set Result.ActualOutput.Reference = 'Defect_1335'
    And set Result.ActualOutput.Reference = 'Defect_1426'