@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error Condition for invalid combination of Permissions.
  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """
  #################################################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 400 is displayed when invalid permission are passed #key#
    * def key = <Condition>
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Invalid Permissions'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>


    Examples:
      | Scenario number | Condition                                        | permissions                                      | Expected |
      | '001'           | 'Only ReadTransactionsBasic'                     | ["ReadTransactionsBasic"]                        | 400      |
      | '002'           | 'Only ReadTransactionsCredits'                   | ["ReadTransactionsCredits"]                      | 400      |
      | '003'           | 'Only ReadTransactionsDebits'                    | ["ReadTransactionsDebits"]                       | 400      |
      | '004'           | 'Only ReadTransactionsDetail'                    | ["ReadTransactionsDetail"]                       | 400      |
      | '005'           | 'ReadAccountsBasic and ReadTransactionsBasic'    | ["ReadAccountsBasic","ReadTransactionsBasic"]    | 400      |
      | '006'           | 'ReadAccountsBasic and ReadTransactionsDetail'   | ["ReadAccountsBasic","ReadTransactionsDetail"]   | 400      |
      | '007'           | 'ReadAccountsBasic and ReadTransactionsCredits'  | ["ReadAccountsBasic","ReadTransactionsCredits"]  | 400      |
      | '008'           | 'ReadAccountsBasic and ReadTransactionsDebits'   | ["ReadAccountsBasic","ReadTransactionsDebits"]   | 400      |
      | '009'           | 'ReadAccountsDetail and ReadTransactionsBasic'   | ["ReadAccountsDetail","ReadTransactionsBasic"]   | 400      |
      | '010'           | 'ReadAccountsDetail and ReadTransactionsCredits' | ["ReadAccountsDetail","ReadTransactionsCredits"] | 400      |
      | '011'           | 'ReadAccountsDetail and ReadTransactionsDebits'  | ["ReadAccountsDetail","ReadTransactionsDebits"]  | 400      |
      | '012'           | 'ReadAccountsDetail and ReadTransactionsDetail'  | ["ReadAccountsDetail","ReadTransactionsDetail"]  | 400      |
      | '013'           | 'Empty permissions'                              | [" "]                                            | 400      |
      | '014'           | 'Garbage permissions'                            | ["ReadAccountsDetail", "abcdef"]                 | 400      |
      | '015'           | 'Misspelled permissions'                         | ["ReadAccountDetail", "ReadAccountsBasic"]       | 400      |
      | '016'           | 'lower case permissions'                         | ["readAccountDetail", "ReadAccountsBasic"]       | 400      |
