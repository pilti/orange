@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_UAT_All_API
@AISP_SetAcReq
@AISP_Error_Tech


Feature: This feature is to demonstrate the Error Condition when Client Credential of different TPP is passed.

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """
###########################################################################################################################################################


  @Defect_1335
  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when Client Credentials passed is of different TPP and Value is #key#

    * def key = 'CCG TPP ' + '<CCGRole>' +'& AccountSetup TPP '+ '<AccountSetupRole>'
    * def info = karate.info
    * set info.key = key
    * set info.subset = 'Client Credential of Different TPP'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    Given json Application_Details = active_tpp.<CCGRole>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = <scope>
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

   #Update ip json with parameters required by next API
    Given json Application_Details1 = active_tpp.<AccountSetupRole>
    And set Application_Details.client_id = Application_Details1.client_id
    And set Application_Details.client_secret = Application_Details1.client_secret
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.Content_type = 'application/json'
   #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == <Expected>
    And set Result.ActualOutput.Reference = 'Defect_1335'


    Examples:
      | Scenario number | CCGRole   | scope             | AccountSetupRole | Expected |
      | '001'           | AISP_PISP | 'openid accounts' | AISP             | 201      |
      | '002'           | AISP_PISP | 'openid accounts' | PISP             | 201      |
      | '003'           | AISP      | 'openid accounts' | PISP             | 201      |