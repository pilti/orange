@AISP
@AISP_API
@AISP_SetAcReq
@AISP_Error_Bus


Feature: This feature is to demonstrate the Error Condition when Client Id,Client secret is invalid,empty or of different TPP is passed

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "Pass";
        }else{
              Result.TestStatus = "Fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
             }
      """

  @Defect_1335
  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when #key# value of Client Id is passed in API request

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'Client id validation'

    Given json Application_Details = active_tpp.<tpprole>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 201

    #Input invalid or empty client_id value
    Given set Application_Details.client_id = <client_id>
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Result.ActualOutput.Reference = 'Defect_1335'

    Examples:
      | key       | tpprole | client_id |
      | 'Invalid' | AISP    | 'abcdef'  |
      | 'Empty'   | AISP    | ' '       |


  ####################################################################################################################################
  @Defect_1335
  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when #key# value of Client secret is passed in API request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Client secret validation'

    Given json Application_Details = active_tpp.<tpprole>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 201

   #Input invalid or empty client_secret value
   # And set Application_Details.client_id = <client_secret>
    Given set Application_Details.client_secret = <client_secret>
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Result.ActualOutput.Reference = 'Defect_1335'


    Examples:
      | key       | tpprole | client_secret |
      | 'Invalid' | AISP    | 'abcdef'      |
      | 'Empty'   | AISP    | ' '           |


    #####################################################################################################################################

  @severity=normal
  Scenario Outline: Verify that Status code 201 is displayed when Client secret of different TPP having #key# role is passed in API request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Client secret validation - TPP'

    Given json Application_Details = active_tpp.<tpprole>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 201

  #Input client_secret associated with different TPP
    Given set Application_Details.client_secret = <client_secret>
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Result.ActualOutput.Reference = 'Defect_1335'


    Examples:
      | key    | tpprole   | client_secret                 |
      | 'AISP' | AISP_PISP | active_tpp.AISP.client_secret |
      | 'PISP' | AISP_PISP | active_tpp.PISP.client_secret |