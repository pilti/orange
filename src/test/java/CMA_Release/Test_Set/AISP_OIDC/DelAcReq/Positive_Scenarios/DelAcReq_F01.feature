@AISP
@AISP_UAT
@AISP_UAT_API
@AISP_Consent
@AISP_DelAccReq
@Regression
@AISP_UAT_All_API
@AISP_DeleteAccREQ
@Functional_Shakedown
Feature: This feature is to verify the status code for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Functional_Shakedown_Quick
  Scenario: Verify that Status code 204 is displayed when all valid header values are provided in DELETE Account Request.
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC09_Delete_Positive'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    #Create AccountRequestId and change its status to Authorized by Authorising Consent
    Given json Application_Details = active_tpp.AISP_PISP
      And set Application_Details.TestCasePath = info1.path
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.UI_Output = callE2E.UIResult
      And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token

    #Creating new AccessToken using Client Credential grant API
    Given json Application_Details1 = active_tpp.AISP_PISP
      And set Application_Details1.request_method = 'POST'
      And set Application_Details1 $.grant_type = "client_credentials"
      And set Application_Details1 $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details1)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Delete AccReqId
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
      And set Application_Details.request_method = 'DELETE'
      And set Application_Details.token_type = 'Bearer'
      And set Application_Details.AccountRequestId = callE2E.AccReQ.AccountRequestId
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204

  Scenario: Verify the status is Revoked when expired AccountRequestId is Deleted
    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = 'TC10_Delete_Positive'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
      And set Application_Details.TestCasePath = info1.path
      And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.Date_Format').getFormattedDate(1,0,0,0,0,30)
    When def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    Then set Result.UI_Output = callE2E.UIResult

    Given set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
      And json Application_Details1 = active_tpp.AISP_PISP
      And set Application_Details1.request_method = 'POST'
      And set Application_Details1 $.grant_type = "client_credentials"
      And set Application_Details1 $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details1)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.AccountRequestId = callE2E.AccReQ.AccountRequestId
      And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
      And set Application_Details.request_method = 'DELETE'
      And set Application_Details.token_type = 'Bearer'
      And def currtime = Java.type('CMA_Release.Java_Lib.Date_Format').getFormattedDate(1,0,0,0,0,0)
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204

    Given def napiApp = new apiapp()
      And json Application_Details1 = active_tpp.AISP_PISP
      And set Application_Details1.access_token = apiApp.Access_Token_CCG.response.access_token
      And set Application_Details1.request_method = 'GET'
      And set Application_Details1.token_type = 'Bearer'
      And def AcctReqUrl = AcctReqUrl + "/" + Application_Details1.AccountRequestId
    When call napiApp.Account_Request_Setup(Application_Details1)
    Then match napiApp.Account_Request_Setup.response.Data.Status == 'Revoked'







