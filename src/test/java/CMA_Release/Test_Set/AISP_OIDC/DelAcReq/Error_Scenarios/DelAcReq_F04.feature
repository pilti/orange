@AISP
@AISP_Consent
@AISP_DelAccReq
@Regression

Feature: This feature is to validate accept and financial id headers for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId

   #Set input with the request Method
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"

       #Call Access_Token_CCG API
    * call apiApp1.Access_Token_CCG(Application_Details)

    #Validate response and update result json
    #Perform Assertion
    * match apiApp1.Access_Token_CCG.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario Outline: Verify that error is received when #key# accept header value is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Accept Header Validation'
    * set reqHeader $.Accept = <Accept>
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == <ExpectedStatusCode>

    Examples:
      | key       | Accept             | ExpectedStatusCode |
      | 'Invalid' | 'Application-json' | 406                |
      | 'Empty'   | ' '                | 204                |


  Scenario Outline: Verify that error is received when #key# Financial id header value is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Financial id validation'
    * set reqHeader $.x-fapi-financial-id = <BankID>
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 403

    Examples:
      | key       | BankID              |
      | 'Invalid' | '0015800000jfQ9aAA' |
      | 'Empty'   | ' '                 |

  Scenario: Verify that error is received when Financial id header is not sent in the request

    * def info = karate.info

    Given remove reqHeader.x-fapi-financial-id
    And set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 400



