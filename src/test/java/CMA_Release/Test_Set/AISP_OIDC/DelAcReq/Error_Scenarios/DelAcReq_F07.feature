@AISP_DelAccReq_Err_07_Rup

Feature: Account request ID validation for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json Result1 = {}
    * json UIResult = {}
    * json AccReQ = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Application_Details = active_tpp.AISP_PISP

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario: Verify whether error is received when DELETE endpoint is revoked for valid AccountRequestID but the request was expired after authorization

    * def info = karate.info
    Given set Application_Details $.client_id = active_tpp.AISP_PISP.client_id
    And set Application_Details $.client_secret = active_tpp.AISP_PISP.client_secret
    And set Application_Details $.kid = active_tpp.AISP_PISP.kid
    And set Application_Details $.Organization = active_tpp.AISP_PISP.Organization
    And set Application_Details $.scope = "accounts"

    #Provide Account request expiration time as 10 mins from now
    And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(600)
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    And call apiApp.configureSSL(Application_Details)
    And call apiApp.E2EConsent_GenerateToken(Application_Details)
    And def msg = "Wait Applied to expire account request"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(610000)

    #Call Access_Token_CCG API
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp1.Access_Token_CCG(Application_Details)
   #Validate response and update result json
   #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.token_type =  apiApp1.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204

  Scenario: Verify whether error is received when DELETE endpoint is revoked for valid AccountRequestID but the request was already revoked

    * def info = karate.info
    Given json Application_Details = active_tpp.AISP_PISP
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And def AccountRequestId = callE2E.AccReQ.AccountRequestId
    #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
   #Validate response and update result json
   #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type =  apiApp1.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204
    #Invoke DELETE endpoint for Account request ID which is already revoked
    And call apiApp1.Account_Request_Delete(Application_Details)
    And match apiApp1.Account_Request_Delete.responseStatus == 204


  @CriticalScenario
  @Regression
  Scenario: Verify whether error is received when DELETE endpoint is invoked for valid AccountRequestID which is not authorized

    * def info = karate.info
    Given def Application_Details = active_tpp.AISP_PISP
    And def AccountRequestsetup = call read("classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/PreAuth_PreRequisite.feature") Application_Details
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'DELETE'
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 400


  Scenario:Verify error is received when DELETE endpoint is invoked for valid account request but associated with different TPP

    * def info = karate.info
    Given json Application_Details = active_tpp.AISP_PISP
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And def AccountRequestId = callE2E.AccReQ.AccountRequestId
    And json Application_Details = active_tpp.AISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
    #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.request_method = 'DELETE'
    And set Application_Details $.AccountRequestId = AccountRequestId
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.access_token = apiApp1.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 403


  @change
  Scenario:Verify error is received when DELETE endpoint is invoked for valid account request but account request ID is expired before it gets authorized

    Given def info = karate.info
    And json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
  #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
  #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)
  #Perform Assertion
    Then match apiApp.Access_Token_CCG.responseStatus == 200

 #Update ip json with parameters required by next API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(600)
 #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
  #Set input parameters for CreateRequestObjec#

    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
  #Call CreateRequestObject#
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
  #added to save the consnet url , & launch again during authorisation renewal
    And set Application_Details $.consenturl = consenturl
  #* def consenturl = 'http://www.bbc.com'
    Given def c = call  webApp.driver1 = webApp.start1(user_details.Generic.G_User_1.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    Then match perform.Status == 'Pass'

    Given def perform = Functions.selectAllAccounts(webApp,data)
    When set UIResult.AccountSelectionPage = perform
    Then match perform.Status == 'Pass'

    And set data $.SelectedAccounts = perform.SelectedAccounts
  #added to save the accounts selected during SCA, & verify during authorisation renewal
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set Result1.ActualOutput.UI.Output.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts
    Then def perform = Functions.reviewConfirm(webApp,data)

    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
  #added to save the permissions selected during SCA , & verify during authorisation renewal
    And set Application_Details.Permissions = perform.Permissions
    And set UIResult.ReviewConfirmPage = perform
    And set Application_Details.code = perform.Action.Authcode
    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    And def screenshot = capturescreen.captureScreenShot2(webApp)
    And def stop = webApp.stop()
    And def msg = "Wait Applied to expire Account request resource"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(630000)
    And set Application_Details.request_method = 'POST'
    And set Application_Details.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details.grant_type = "client_credentials"
  #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
  #Validate response and update result json
  #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200
  #Trigger API with TPP2 client credentials passed in the request
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.AccountRequestId = Application_Details.AccountRequestId

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 400


  Scenario:Verify error is received when DELETE endpoint is invoked for valid account request but consent is rejected

    Given def info = karate.info
    And json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)

      #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
      #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)
      #Perform Assertion
    Then match apiApp.Access_Token_CCG.responseStatus == 200

     #Update ip json with parameters required by next API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(600)
     #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
     #Set input parameters for CreateRequestObjec#

    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    #Call CreateRequestObject#
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
      #added to save the consnet url , & launch again during authorisation renewal
    And set Application_Details $.consenturl = consenturl
      #* def consenturl = 'http://www.bbc.com'
    Then def c = call  webApp.driver1 = webApp.start1(user_details.Generic.G_User_1.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    Then def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'
    And def data = {"usr":'#(user_details.Generic.G_User_1.user)',"otp":'#(user_details.Generic.G_User_1.otp)',"action":"continue","Confirmation":"Yes"}
    Then def perform = Functions.scaLogin(webApp,data)
    Then match perform.Status == 'Pass'
    Then def perform = Functions.selectAllAccounts(webApp,data)
    And set UIResult.AccountSelectionPage = perform
    Then match perform.Status == 'Pass'
    And set data $.SelectedAccounts = perform.SelectedAccounts
      #added to save the accounts selected during SCA, & verify during authorisation renewal
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And set data.action = "cancel"
    And def perform = Functions.reviewConfirm(webApp,data)
    Given set Application_Details.ConsentExpiry = perform.ConsentExpiry
      #added to save the permissions selected during SCA , & verify during authorisation renewal
    And set Application_Details.Permissions = perform.Permissions
    And set UIResult.ReviewConfirmPage = perform
    And set Application_Details.code = perform.Action.Authcode
    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    And def screenshot = capturescreen.captureScreenShot2(webApp)
    And def stop = webApp.stop()
    And set Application_Details.request_method = 'POST'
    And set Application_Details.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details.grant_type = "client_credentials"
    #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
    #Validate response and update result json
    #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    #Invoke Delete Endpoint for Rejected Account Request ID
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.AccountRequestId = Application_Details.AccountRequestId
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    And match apiApp.Account_Request_Retrieve.response.Data.Status == "Rejected"

    #Invoke Delete Endpoint for Rejected Account Request ID
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.AccountRequestId = Application_Details.AccountRequestId
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 400




