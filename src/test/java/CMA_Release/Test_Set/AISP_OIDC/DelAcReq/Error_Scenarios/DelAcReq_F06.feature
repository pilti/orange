@AISP
@AISP_Consent
@AISP_DelAccReq
  @123

Feature: Account request ID validation for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId


    #Set input with the request Method
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"

       #Call Access_Token_CCG API
    * call apiApp1.Access_Token_CCG(Application_Details)

    #Perform Assertion
    * match apiApp1.Access_Token_CCG.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  @severity=normal
  Scenario Outline: Verify that error is received when #key# Account request ID passed in API request

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'Account request ID validation'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)

    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.URL = AcctReqUrl + "/" + <AccountRequestId>


    Given url AcctReqUrl + "/" + <AccountRequestId>
    And header Authorization = Application_Details.token_type + " " + Application_Details.access_token
    And headers reqHeader
    When method Delete
    Then match responseStatus == 400

    Examples:
      | key                                       | AccountRequestId                          |
      | 'invalid'                                 | 'abcd'                                    |
      | 'invalid having special chars'            | '9b6835da-0a26-4f81-bae3-96c35ec743d1#$'  |
      | 'invalid having extra alphanumeric chars' | '9b6835da-0a26-4f81-bae3-96c35ec743d112A' |

