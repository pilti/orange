@AISP
@AISP_Consent
@AISP_DelAccReq


Feature: This feature is to verify access token for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId
     #Set input with the request Method
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify that error is received when expired client credential token is sent in the request

    Given def info = karate.info
    #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
   #Validate response and update result json
    #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200
    #Wait for more than 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(310000)
    #Trigger Delete endpoint with expired access token
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 401

  Scenario: Verify that error is not received when client credential token about to expire is sent in the request

    Given def info = karate.info
    #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
    #Validate response and update result json
    #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200
      #Wait for less than 5 min
    And def msg = "Wait Applied to expire the token"
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(296000)
    #Trigger Delete endpoint with about to expire access token
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204

  Scenario Outline: Verify that error is received when #key# value of client credential token is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access Token validation'

    #Trigger Delete endpoint with invalid or empty access token
    Given set Application_Details.token_type = <token_type>
    And set Application_Details.access_token = <access_token>
    And set Application_Details.request_method = 'DELETE'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 401

  Examples:
  | key       | token_type | access_token |
  | 'Empty'   | ' '        | ' '          |
  | 'Invalid' | 'Bearer'   | 'abcd'       |


 Scenario: Verify that error is  received when Authorization header is not sent in the request

    * def info = karate.info
    #Trigger Delete endpoint without Authorization header
    Given url AcctReqUrl + "/" + AccountRequestId
    #And header Authorization = Type + " " + Token
    And headers reqHeader
    When method DELETE
    Then match responseStatus == 401