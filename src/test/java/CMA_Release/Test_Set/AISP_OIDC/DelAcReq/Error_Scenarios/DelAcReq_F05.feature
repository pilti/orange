@AISP
@AISP_Consent
@AISP_DelAccReq


Feature: Token type and URI validation for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId

    #Set input with the request Method
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"

       #Call Access_Token_CCG API
    * call apiApp1.Access_Token_CCG(Application_Details)
    #Validate response and update result json
    #Perform Assertion
    * match apiApp1.Access_Token_CCG.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario Outline: Verify whether error is received when #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Token type validation'

    Given set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = <type>

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 401

    Examples:
      | key                                                                                    | type                   |
      | 'Missing token type'                                                                   | " "                    |
      | 'Misspelled token type append single extra char to Bearer'                             | "Bearerr"              |
      | 'Invalid token type- append multiple chars to Bearer'                                  | "Bearerdfgdfhdh"       |
      | 'Invalid token type- append multiple aphanumeric chars to Bearer'                      | "Bearer5757"           |
      | 'Invalid token type- append multiple special chars to Bearer'                          | "Bearer$%&%4gd"        |
      | 'Invalid token type- placed multiple alphanumeric chars in between Bearer'             | "Beare577dgdfr"        |
      | 'Invalid token type- placed multiple alphanumeric and special chars in between Bearer' | "Bear%^*577dfgfddgdfr" |
      | 'Lower case token type'                                                                | "bearer"               |
      | 'Token type other than Bearer '                                                        | "Basic"                |


  @severity=normal
  Scenario Outline: Verify that Status code 404 is displayed when API is triggered with Invalid URI #key#

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'URI validation'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)

    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    And def AcctRequestUrl = ( ActiveEnv=="SIT" ? '<AcctRequestUrl_SIT>' : '<AcctRequestUrl_UAT>')
    And set Application_Details.AcctReqUrl = AcctRequestUrl
    And set Application_Details.request_method = 'DELETE'
    Then call apiApp.Account_Request_Delete(Application_Details)
    And match apiApp.Account_Request_Delete.responseStatus == 404

    Examples:
      | key  | AcctRequestUrl_SIT                                                  | AcctRequestUrl_UAT                                                          |
      | '1'  | https://api.boitest.net/1/api/open-banking/v1.1/Account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Account-requests    |
      | '2'  | https://api.boitest.net/1/api/open-banking/v1.1/accountrequests     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountrequests     |
      | '3'  | https://api.boitest.net/1/api/open-banking/v1.1/account-request     | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-request     |
      | '4'  | https://api.boitest.net/1/api/open-banking/v1.1//account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//account-requests   |
      | '5'  | https://api.boitest.net/1/api/open-banking/v1.0/account-requests    | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.0/account-requests    |
      | '6'  | https://api.boitest.net/1/api/open-banking//v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/account-requests   |
      | '7'  | https://api.boitest.net/1/api//open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/account-requests   |
      | '8'  | https://api.boitest.net/1/api/v1.1/account-requests                 | https://api.u2.psd2.boitest.net/1/api/v1.1/account-requests                 |
      | '9'  | https://api.boitest.net/1//api/open-banking/v1.1/account-requests   | https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/account-requests   |
      | '10' | https://api.boitest.net/1//open-banking/v1.1/account-requests       | https://api.u2.psd2.boitest.net/1//open-banking/v1.1/account-requests       |
      | '11' | https://api.boitest.net/1/api/open-banking//account-requests        | https://api.u2.psd2.boitest.net/1/api/open-banking//account-requests        |
      | '12' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests/// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests/// |
      | '13' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests// |


  @severity=normal
  Scenario Outline: Verify that Status code 204 is displayed when API is triggered with extra forward slashes placed in URI#key#

    * def info = karate.info
    * def key = <key>
    * set info.key = key
    * set info.subset = 'URI validation'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)

    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    And def AcctRequestUrl = ( ActiveEnv=="SIT" ? '<AcctRequestUrl_SIT>' : '<AcctRequestUrl_UAT>')
    And set Application_Details.AcctReqUrl = AcctRequestUrl

      Given url AcctReqUrl + "/" + AccountRequestId + <TestCondition>
      And header Authorization = Application_Details.token_type + " " + Application_Details.access_token
      And headers reqHeader
      When method Delete
      Then match responseStatus == <expectedResponseStatus>

      Examples:
        | key | AcctRequestUrl_SIT                                                 | AcctRequestUrl_UAT                                                         |TestCondition|expectedResponseStatus|
       | '1' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests  |  '/'             |204              |
       | '2' | https://api.boitest.net/1/api/open-banking/v1.1/account-requests  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/account-requests  |  '//'            |404                |


  @severity=normal
  Scenario Outline: Verify that Status code 204 is displayed when API is triggered with extra forward slashes placed after domain name

    * def info = karate.info
    * set info.subset = 'URI validation'

    Given json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'

    When call apiApp.Access_Token_CCG(Application_Details)

    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token

    And def AcctRequestUrl = ( ActiveEnv=="SIT" ? '<AcctRequestUrl_SIT>' : '<AcctRequestUrl_UAT>')
    And set Application_Details.AcctReqUrl = AcctRequestUrl

    Given url AcctReqUrl + "/" + AccountRequestId
    And header Authorization = Application_Details.token_type + " " + Application_Details.access_token
    And headers reqHeader
    When method Delete
    Then match responseStatus == 204

    Examples:
      | AcctRequestUrl_SIT                                                 | AcctRequestUrl_UAT                                                         |
      | https://api.boitest.net//1/api/open-banking/v1.1/account-requests | https://api.u2.psd2.boitest.net//1/api/open-banking/v1.1/account-requests |

