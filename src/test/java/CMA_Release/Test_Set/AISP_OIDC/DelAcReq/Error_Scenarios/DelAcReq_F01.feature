@AISP
@AISP_Consent
@AISP_DelAccReq

Feature: This feature is to verify client credentials for Delete Account Request API

  Background:

    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    * call apiApp1.Access_Token_CCG(Application_Details)
    * match apiApp1.Access_Token_CCG.responseStatus == 200

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: Verify that account request resource is deleted successfully with 204 HTTP status code received in response when #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Client credentials validation'

    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
      And set Application_Details.request_method = 'DELETE'
      And set Application_Details.token_type = 'Bearer'
      And set Application_Details.client_id = <client_id>
      And set Application_Details.client_secret = <client_secret>
      When call apiApp.Account_Request_Delete(Application_Details)
      Then match apiApp.Account_Request_Delete.responseStatus == 204

    Examples:
      | key                        | client_id | client_secret |
      | 'Invalid client credentials'          | 'abcd'                    | 'xyz'                         |
      | 'empty client credentials' | ' '       | ' '           |
      | 'Client credentials of different TPP' | active_tpp.AISP.client_id | active_tpp.AISP.client_secret |