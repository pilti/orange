@AISP
@AISP_API
@AISP_DelAccReq

Feature: This feature is to verify access token for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * json Application_Details = active_tpp.AISP_PISP
    * def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    * set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    * def AccountRequestId = callE2E.AccReQ.AccountRequestId
    #Set input with the request Method
    * set Application_Details.request_method = 'POST'
    * set Application_Details $.grant_type = "client_credentials"
    * set Application_Details $.Content_type = "application/x-www-form-urlencoded"

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify that error is received when access token having authorization_code grant is sent in the request

    * def info = karate.info
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.refresh_token = profile.refresh_token
       #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
  #Trigger Delete endpoint with invalid or empty access token
    Given set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details.request_method = 'DELETE'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 401


  Scenario: Verify that error is received when access token having refresh_token grant is sent in the request

    * def info = karate.info
  # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    Given set Application_Details $.refresh_token = profile.refresh_token

  #Trigger Delete endpoint with invalid or empty access token
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.access_token = Application_Details.refresh_token
    And set Application_Details.request_method = 'DELETE'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 401


  Scenario Outline: Verify that error is received when client credential token generated using different TPP #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access Token Validation'
    Given json Application_Details1 = active_tpp.<TPProle>
    And call apiApp1.configureSSL(Application_Details1)

    And set Application_Details1.request_method = 'POST'
    And set Application_Details1.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details1.grant_type = "client_credentials"

   #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details1)
   #Validate response and update result json
   #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

   #Trigger API with TPP2 client credentials passed in the request
    And call apiApp.configureSSL(Application_Details)
    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == <ExpectedStatusCode>

    Examples:
      | key    | TPProle | ExpectedStatusCode |
      | 'AISP' | AISP    | 401                |
      | 'PISP' | PISP    | 403                |

  Scenario: Verify that error is received when client credential token having payments scope is sent in the request

    * def info = karate.info
    Given set Application_Details.scope = 'openid payments'
   #Call Access_Token_CCG API
    When call apiApp1.Access_Token_CCG(Application_Details)
   #Validate response and update result json
   #Perform Assertion
    Then match apiApp1.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp1.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'

    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 403
