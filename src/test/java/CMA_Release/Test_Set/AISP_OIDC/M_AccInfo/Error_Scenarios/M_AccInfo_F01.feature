@AISP
@AISP_UAT
@AISP_API
@AISP_UAT_API
@AISP_MultiAccInfo

Feature: This feature is to test Error conditions of MultipleAccountInfo API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def Application_Details = active_tpp.AISP_PISP
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  #Assert changed to be moved to positive

  Scenario Outline: Verify that MultiAccount Info is retrieved successfully when client credentials of different TPP is provided in the API request
    * def info = karate.info
    * def key = '<DifferentTPP>'
    * set info.key = '<Role>'
    * set info.subset = 'Client credentials-Different TPP'
         # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
         #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPP1Details.TPPRole = '<Role>'
    And set Result.Input.TPP1Details.client_id = Application_Details.client_id
    And set Result.Input.TPP1Details.client_secret = Application_Details.client_secret

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type
    And def Application_Details1 = active_tpp.<DifferentTPP>
    And set Application_Details $.client_id = Application_Details1.client_id
    And set Application_Details $.client_secret = Application_Details1.client_secret
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == <Expected>
    And set Result.ActualOutput.Multi_Account_Information.Input.TPP2_Role = '<DifferentTPP>'
    And set Result.ActualOutput.Multi_Account_Information.Input.TPP2_client_id = Application_Details.client_id
    And set Result.ActualOutput.Multi_Account_Information.Input.TPP2_client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Reference = 'Defect_1335'

    Examples:
      | Role      | DifferentTPP | permissionsfile              | Expected |
      | AISP_PISP | AISP         | 'Generic_G_User_1.properties'         | 200     |

  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when Invoked the API using Invalid Access Token #key#

    * def info = karate.info
    * def key = <accesstoken>
    * set info.key = '<Role>'
    * set info.subset = 'Invalid Access Token'
      # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
      #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = <accesstoken>
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == <Expected>

    Examples:
      | Role      | accesstoken | permissionsfile                           | Expected |
      | AISP_PISP | 'abcd'      | 'Only_Account_info_permissions_G_User_1.properties' | 401      |


  @severity=normal
  Scenario Outline: Verify that Status code 401 is displayed when invoked the API using Expired Access Token for TPP Role #key#

    * def info = karate.info
    * def key = '<Role>'
    * set info.key = '<Role>'
    * set info.subset = 'Expired Access Token'
     # To read Test Data
    * def fileName = <permissionsfile>
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
     #Configure Network Certificate
    Given def Application_Details = active_tpp.<Role>
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = '<Role>'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    #Wait for 5 min to expire the token
    And def msg = "Wait Applied to expire the token"
    And print msg
    And def callWait = Java.type('CMA_Release.Java_Lib.Utility').UTY_Delay(310000)

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == <Expected>

    Examples:
      | Role      | permissionsfile                           | Expected |
      | AISP_PISP | 'Only_Account_info_permissions_G_User_1.properties' | 401      |

  @Defect_1421
  Scenario: Verify that Status code 403 is displayed when AccountRequest associated with access_token is revoked using the DELETE endpoint

    * def info = karate.info
      # To read Test Data
    * def fileName = 'RevokedConsent_Generic_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

   # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
     #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

     #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type
    When call apiApp.Multi_Account_Information(Application_Details)
    And match apiApp.Multi_Account_Information.responseStatus == 403
    And set Result.ActualOutput.Reference = 'Defect_1421'
