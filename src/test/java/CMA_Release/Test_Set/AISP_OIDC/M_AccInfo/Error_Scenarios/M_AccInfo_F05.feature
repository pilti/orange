@AISP
@AISP_API
@AISP_MultiAccInfo
@AISP_MultiAccInfo_SIT
Feature: Access Token validation for Get Multiple Account Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    #To write test result and error details to output file

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);

       }
      """

  @1335
  @severity=normal
  Scenario Outline:Verify whether error is received when API is invoked with #key# TPP and access_token not associated with requesting TPP is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Access Token validation'

    # TPP1 creates a new access token from refresh_token
    Given def Application_Details = active_tpp.<TPP1>
    And call apiApp.configureSSL(Application_Details)
    And def fileName = <Consent_TPP1 specific>
    And def name = 'Consent'
    And def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    And json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.ActualOutput.TPP1_Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.TPP1_Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

 # TPP2 creates a new access token from refresh_token to fetch account details
    * def apiApp1 = new apiapp()
    Given def Application_Details = active_tpp.<TPP2>
    And call apiApp1.configureSSL(Application_Details)
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

    # TPP2 triggers Account information API with access_token associated with TPP1
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.Reference = 'Defect_1335'
    And match apiApp.Multi_Account_Information.responseStatus == 401

    Examples:
      | key                  | TPP1      | TPP2      | Consent_TPP1 specific        |
      | 'AISP and PISP role' | AISP      | AISP_PISP | 'Generic_AISPTPP_G_User_1.properties' |
      | 'AISP role'          | AISP_PISP | AISP      | 'Generic_G_User_1.properties'         |
      | 'PISP role'          | AISP_PISP | PISP      | 'Generic_G_User_1.properties'         |



