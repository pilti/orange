@AISP
@AISP_API
@AISP_MultiAccInfo
@AISP_MultiAccInfo_SIT


Feature: Financial id and scope validation for Get Multiple Account Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
      #To write test result and error details to output file
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }

      """

  @Regression
  Scenario:Verify whether error is received when access_token associated with expired consent is sent in API request

    * def info = karate.info
     # To read Test Data
    * def fileName = 'ExpiredConsent_Generic_G_User_4.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   # Generate access_token from refresh_roken
    Given def Application_Details = active_tpp.AISP_PISP
    # Configure Network Certificate
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
     #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

     #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = apiApp.Access_Token_RTG.response.token_type
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.Reference = 'Defect_1421'
    Then match apiApp.Multi_Account_Information.responseStatus == 403

  @Regression
  Scenario:Verify whether error is received when empty value of financial header  is sent in API request

    * def info = karate.info
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

  # Generate access_token from refresh_roken
    Given def Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    # set empty value to financial id parameter
    And set reqHeader $.x-fapi-financial-id = ' '
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
   # TPP triggers Multiple Account Info API with empty financial id
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 403


  Scenario:Verify whether error is received when financial header parameter is not sent in API request

    * def info = karate.info
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    # Generate access_token from refresh_roken
    Given def Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # set blank value to financial id parameter
    And remove reqHeader.x-fapi-financial-id
    # TPP triggers Multi Account Info API without financial id
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 400

  @Regression
  Scenario Outline:Verify whether error is not received when #key# value of scope sent in API request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Scope validation'
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)

  # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
    # TPP triggers Multi Account Info API with invalid scope parameter

    Given url AcctInfoUrl
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    And header scope = <scope>
    When method GET
    Then set Result.ActualOutput.Multi_Account_Information.Input.scope = <scope>
    And match responseStatus == 200

    Examples:
      | key       | scope         |
      | 'Invalid' | 'accounts123' |
      | 'Empty'   | '  '          |
