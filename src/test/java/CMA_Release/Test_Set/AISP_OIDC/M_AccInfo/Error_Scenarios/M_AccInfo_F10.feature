@AISP
@AISP_Consent
@AISP_MultiAccInfo

Feature:Verify HTTP status code 403 in response when access token having payments scope is passed in GET Multiple Account Info API

  Background:
    * def apiApp = new apiapp()
    * def info = karate.info
    * json Result = {}
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
     """


  @severity=normal
  Scenario: Verify HTTP status code 403 received in response when access token having payments scope is passed in API request

    * def paymentId = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/payment_Setup.feature')
    * def consent_Url = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/createConsentUrl.feature') paymentId.Result
   # Test data to perform consent
    * def strConsentUrl = consent_Url.Result.createConsentUrl.ConsentUrl
    * def consentData = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes","url":'#(strConsentUrl)'}
    * def performConsent = call read('classpath:CMA_Release/Test_Set/PISP/PISP_OIDC/performConsent.feature') consentData

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.code = performConsent.performConsentAction.AuthCode
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_ACG(Application_Details)
    Then set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    #######################################PISP FLOW END######################################################################

    Given set Application_Details $.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 403

