@AISP
@AISP_API
@AISP_MultiAccInfo
@AISP_MultiAccInfo_SIT

Feature: Accept parameter ,token type and URI validation for Get Multiple Account Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """

  Scenario Outline:Verify whether error is not received when empty accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'
    # Generate access_token from refresh_roken

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    And match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token    
    # set empty value to Accept parameter
    And set reqHeader $.Accept = <Accept>
   # TPP triggers Multi_Account_Information API with invalid Accept parameter value
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Examples:
      | Accept |
      | ' '    |

  @Regression
  Scenario Outline:Verify whether error is received when invalid accept header value is sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'
  # Generate access_token from refresh_roken
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.permissions = profile.permissions
    And set Result.Input.refresh_token = profile.refresh_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
  # set invalid value to Accept parameter
    And set reqHeader $.Accept = <Accept>

 # TPP triggers Multi_Account_Information API with invalid Accept parameter value
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.Multi_Account_Information.Input.URL = AcctInfoUrl
    And set Result.ActualOutput.Multi_Account_Information.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Multi_Account_Information.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Multi_Account_Information.Input.reqHeader = reqHeader
    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus =  apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.Output.response =  apiApp.Multi_Account_Information.response
    And set Result.Additional_Details = Application_Details
    And match apiApp.Multi_Account_Information.responseStatus == 406

    Examples:
      | Accept             |
      | 'Application-json' |

  Scenario:Verify whether error is not received when accept header is not sent in the request

    * def info = karate.info
    * set info.subset = 'Accept parameter validation'
 # Generate access_token from refresh_roken
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.permissions = profile.permissions
    And set Result.Input.refresh_token = profile.refresh_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
  # remove Accept header from API headers
    And remove reqHeader.Accept

 # TPP triggers Multi_Account_Information API without Accept header
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.Multi_Account_Information.Input.URL = AcctInfoUrl
    And set Result.ActualOutput.Multi_Account_Information.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Multi_Account_Information.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Multi_Account_Information.Input.reqHeader = reqHeader
    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus =  apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.Output.response =  apiApp.Multi_Account_Information.response
    And set Result.Additional_Details = Application_Details
    And match apiApp.Multi_Account_Information.responseStatus == 200

  @Regression
  Scenario Outline:Verify whether error is received when #key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'Token type validation'
# Generate access_token from refresh_roken
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)   
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.permissions = profile.permissions
    And set Result.Input.refresh_token = profile.refresh_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Application_Details.request_method = 'GET'
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
   # Input invalid token type values in Authorization header
    And set Application_Details.token_type = <type>

   # TPP triggers Multi_Account_Information API with invalid Accept parameter value
    When call apiApp.Multi_Account_Information(Application_Details)
    And set Result.ActualOutput.Multi_Account_Information.Input.URL = AcctInfoUrl
    And set Result.ActualOutput.Multi_Account_Information.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Multi_Account_Information.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Multi_Account_Information.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Multi_Account_Information.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus =  apiApp.Multi_Account_Information.responseStatus
    And set Result.ActualOutput.Multi_Account_Information.Output.response =  apiApp.Multi_Account_Information.response
    And set Result.Additional_Details = Application_Details
    And match apiApp.Multi_Account_Information.responseStatus == 401

    Examples:
      | key                                                                                    | type                   |
      | 'Missing token type'                                                                   | " "                    |
      | 'Misspelled token type append single extra char to Bearer'                             | "Bearerr"              |
      | 'Invalid token type- append multiple chars to Bearer'                                  | "Bearerdfgdfhdh"       |
      | 'Invalid token type- append multiple aphanumeric chars to Bearer'                      | "Bearer5757"           |
      | 'Invalid token type- append multiple special chars to Bearer'                          | "Bearer$%&%4gd"        |
      | 'Invalid token type- placed multiple alphanumeric chars in between Bearer'             | "Beare577dgdfr"        |
      | 'Invalid token type- placed multiple alphanumeric and special chars in between Bearer' | "Bear%^*577dfgfddgdfr" |
      | 'Lower case token type'                                                                | "bearer"               |
      | 'Token type other than Bearer '                                                        | "Basic"                |


  Scenario Outline: Verify whether error is received when invalid URI#key# is sent in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'
  # Generate access_token from refresh_roken
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)  
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.permissions = profile.permissions
    And set Result.Input.refresh_token = profile.refresh_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

    And def AccountInfoUrl = ( ActiveEnv=="SIT" ? '<AcctInfoUrl_SIT>' : '<AcctInfoUrl_UAT>')
    And print "Env is:" + ActiveEnv
    And print "URL is:" + AccountInfoUrl

    And set Application_Details $.AcctInfoUrl = AccountInfoUrl

 # TPP triggers Multiple Account info API with invalid URI

    Given url AccountInfoUrl
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    When method GET

    And set Result.ActualOutput.Multi_Account_Information.Input.URL = AccountInfoUrl
    And set Result.ActualOutput.Multi_Account_Information.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Multi_Account_Information.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Multi_Account_Information.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Multi_Account_Information.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus =  responseStatus
    And set Result.ActualOutput.Multi_Account_Information.Output.response = response
    And set Result.Additional_Details = Application_Details
    Then match responseStatus == 404

    Examples:
      | key | AcctInfoUrl_SIT                                             | AcctInfoUrl_UAT                                                     |
      | '1' | https://api.boitest.net/1/api/open-banking/v1.1/accountsss/ | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accountsss/ |
      | '2' | https://api.boitest.net/1/api/open-banking/v1.1/Accounts/   | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/Accounts/   |
      | '3' | https://api.boitest.net/1/api/v1.1/accounts/                | https://api.u2.psd2.boitest.net/1/api/v1.1/accounts/                |
      | '4' | https://api.boitest.net/1/api/open-banking/v1.1//accounts/  | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1//accounts/  |
      | '5' | https://api.boitest.net/1/api/open-banking//v1.1/accounts/  | https://api.u2.psd2.boitest.net/1/api/open-banking//v1.1/accounts/  |
      | '6' | https://api.boitest.net/1/api//open-banking/v1.1/accounts/  | https://api.u2.psd2.boitest.net/1/api//open-banking/v1.1/accounts/  |
      | '7' | https://api.boitest.net/1//api/open-banking/v1.1/accounts/  | https://api.u2.psd2.boitest.net/1//api/open-banking/v1.1/accounts/  |
      | '8' | https://api.boitest.net/1/api/open-banking/v1.1/accounts/// | https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/// |
      | '9' | https://api.boitest.net/1/open-banking/v1.1/accounts/       | https://api.u2.psd2.boitest.net/1/open-banking/v1.1/accounts/       |

  Scenario Outline: Verify whether error is received when URI#key# is sent with extra slashes in the request

    * def info = karate.info
    * def key = <key>
    * set info.subset = 'URI validation'
   # Generate access_token from refresh_roken
    When set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    And call apiApp.Access_Token_RTG(Application_Details)
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And set Result.Input.TPPDetails.TPPRole = 'AISP_PISP'
    And set Result.Input.TPPDetails.client_id = Application_Details.client_id
    And set Result.Input.TPPDetails.client_secret = Application_Details.client_secret
    And set Result.Input.permissions = profile.permissions
    And set Result.Input.refresh_token = profile.refresh_token
    And set Result.ActualOutput.Access_Token_RTG.Output.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.ActualOutput.Access_Token_RTG.Output.response = apiApp.Access_Token_RTG.response

    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token

    And def AccountInfoUrl = ( ActiveEnv=="SIT" ? '<AcctInfoUrl_SIT>' : '<AcctInfoUrl_UAT>')
    And print "Env is:" + ActiveEnv
    And print "URL is:" + AccountInfoUrl

    And set Application_Details $.AcctInfoUrl = AccountInfoUrl


  # TPP triggers Multiple Account info API with extra forward slashes in URI

    Given url AccountInfoUrl
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    When method GET

    And set Result.ActualOutput.Multi_Account_Information.Input.URL = AccountInfoUrl
    And set Result.ActualOutput.Multi_Account_Information.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Multi_Account_Information.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Multi_Account_Information.Input.token_type = Application_Details.token_type
    And set Result.ActualOutput.Multi_Account_Information.Input.access_token = Application_Details.access_token
    And set Result.ActualOutput.Multi_Account_Information.Output.responseStatus =  responseStatus
    And set Result.ActualOutput.Multi_Account_Information.Output.response =  response
    And set Result.Additional_Details = Application_Details
    And match responseStatus == 200

    Examples:
      |key|AcctInfoUrl_SIT|AcctInfoUrl_UAT|
      |'1'|https://api.boitest.net/1/api/open-banking/v1.1/accounts/|https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts/|
      |'2'|https://api.boitest.net/1/api/open-banking/v1.1/accounts//|https://api.u2.psd2.boitest.net/1/api/open-banking/v1.1/accounts//|
      |'3'|https://api.boitest.net//1/api/open-banking/v1.1/accounts/|https://api.u2.psd2.boitest.net//1/api/open-banking/v1.1/accounts/|