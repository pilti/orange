@AISP
@AISP_MultiAccInfo_SIT
@AISP_MultiAccInfo


Feature: Method and permissions validation for Get Multiple Account Information API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
   # To read Test Data
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   # Configure Network Certificate
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(Application_Details)
      #To write test result and error details to output file

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);

       }
      """


  @AISP_API
  Scenario Outline:Verify whether error is received when API is triggered with invalid method #key#

    * def info = karate.info
    * def key = '<method>'
    * set info.subset = 'Method validation'
    # Generate access_token from refresh_roken
    Given def Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
     # TPP triggers transaction API with invalid method

    Given url AcctInfoUrl
    And headers reqHeader
    And header Authorization = Application_Details.token_type + ' ' + Application_Details.access_token
    And request permissions
    When method <method>
    Then set Result.ActualOutput.Multi_Account_Information.Input.Method = '<method>'
    And match responseStatus == 405

    Examples:
      | method  |
      | POST    |
      | DELETE  |
      | PUT     |
      | OPTIONS |
      | HEAD    |
      | PATCH   |

  @AISP_API
  Scenario Outline:Verify whether error is received when API is triggered with #key#

    * def info = karate.info
    * def key = '<permissions>'
    * set info.subset = 'Permissions validation'
    # To read Test Data
    * def fileName = '<permissions>.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

      # Generate access_token from refresh_roken
    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = apiApp.Access_Token_RTG.response.token_type
    And set Application_Details.access_token = apiApp.Access_Token_RTG.response.access_token
     # TPP triggers Multiple Account Info API with insufficient permissions
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 403

    Examples:
      | permissions                     |
      | No_Accounts_Permission_G_User_1 |

  @AISP_Consent
  Scenario Outline:Verify whether error is received when API is triggered with #key#

    * def info = karate.info
    * def key = '<key>'
    * set info.subset = 'Permissions validation'

    Given set permissions $.Data.Permissions = <permissions>
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    And call apiApp.E2EConsent_GenerateToken(Application_Details)
    And set Application_Details.request_method = 'GET'
    And set Application_Details.token_type = 'Bearer'

      # TPP trigger multiple Account Info API with insufficient permissions
    Then call apiApp.Multi_Account_Information(Application_Details)
    And match apiApp.Multi_Account_Information.responseStatus == 403

    Examples:
      | key                                 | permissions                                                                     |
      | Only Balance permission             | ["ReadBalances"]                                                                |
      | Only Products permission            | ["ReadProducts"]                                                                |
      | Only Direct Debit permissions       | ["ReadDirectDebits"]                                                            |
      | Only Standing Order permissions     | ["ReadStandingOrdersDetail"]                                                    |
      | Only beneficiary permissions        | ["ReadBeneficiariesDetail"]                                                     |
      | Only transaction permissions        | ["ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"] |
      | Only transaction debit permissions  | ["ReadTransactionsDetail", "ReadTransactionsDebits"]                            |
      | Only transaction credit permissions | ["ReadTransactionsDetail", "ReadTransactionsCredits"]                           |




