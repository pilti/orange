@AISP
@AISP_API
@AISP_MultiAccInfo

Feature: Account Status, Credit grade validation for the Account associated with Account ID

  Background:

    * def SP_SHARED_DATA_DB = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def apiApp = new apiapp()
    * def apiApp1 = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def info = karate.info
  ############To read Test Data###########################################################################################
    * def fileName = 'Generic_G_User_3.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
   ##########Configure Network Certificate####################################################################################
    * def Application_Details = active_tpp.AISP_PISP
    * call apiApp.configureSSL(profile)

    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And match apiApp.Multi_Account_Information.responseStatus == 200


    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       apiApp1.write(Result,info);
       }
      """

#########################################################################################################################################################
###############################Scenario 01###############################################################################################
  @severity=normal

  Scenario: Verify account details are not retrieved in response for the account which was consented but now changed to Credit grade 6

    Given set Application_Details $.request_method = "GET"
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    #To get the sort code extracted from SortCodeAccountNumber
    And def NSC = ( AccountNo.substring(0,6) )
    * print NSC
    #To get the account number extracted from SortCodeAccountNumber
    And def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo

    And string a = "CREDIT_GRADING='6'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
    When call apiApp1.Multi_Account_Information(Application_Details)
    Then match apiApp1.Multi_Account_Information.response.Data.Account[*].AccountId !contains any Application_Details.AccountId
    And match apiApp1.Multi_Account_Information.responseStatus == 200
    * string a = "CREDIT_GRADING='2'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

########################################################################################################################################################################################################################################################
##################################Scenario 02###########################################################################################

  @severity=normal
  Scenario: Verify account details are not retrieved in response for the account which was consented but now changed to Credit grade 7

    Given def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    And def NSC = ( AccountNo.substring(0,6) )
    * print NSC
   #To get the account number extracted from SortCodeAccountNumber
    And def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "CREDIT_GRADING='7'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    When call apiApp1.Multi_Account_Information(Application_Details)
    Then match apiApp1.Multi_Account_Information.response.Data.Account[*].AccountId !contains any Application_Details.AccountId
    And match apiApp1.Multi_Account_Information.responseStatus == 200
    * string a = "CREDIT_GRADING='2'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

########################################################################################################################################################################################################################################################
########################################Scenario 03##################################################################################

  @severity=normal
  Scenario: Verify account details are not retrieved in response for the account which was consented but now it is INACTIVE

    Given def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
   #To get the sort code extracted from SortCodeAccountNumber
    And def NSC = ( AccountNo.substring(0,6) )
    * print NSC
   #To get the account number extracted from SortCodeAccountNumber
    And def AcNo = ( AccountNo.substring(6,14) )
    * print AcNo
    * string a = "ACCOUNT_STATUS='INACTIVE'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)

    When call apiApp1.Multi_Account_Information(Application_Details)
    Then match apiApp1.Multi_Account_Information.response.Data.Account[*].AccountId !contains any Application_Details.AccountId
    And match apiApp1.Multi_Account_Information.responseStatus == 200
    * string a = "ACCOUNT_STATUS='ACTIVE'"
    And def setCreditGrade = SP_SHARED_DATA_DB.update_SP_Shared_Data(NSC,AcNo,SP_SHARED_DATA_DB_Conn,a)
