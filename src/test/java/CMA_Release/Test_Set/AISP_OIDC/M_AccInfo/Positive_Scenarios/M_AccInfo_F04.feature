@AISP
@AISP_API
@AISP_MultiAccInfo
@AISP_MultiAccInfo_SIT
@AISP_MultiAccInfo_SIT1
@Regression
Feature: Verify the request header used in Get Multi Account Details

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null

    #Read refresh token from properties file
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * print "Profile is: " + karate.pretty(profile)
    * call apiApp.configureSSL(profile)

    #After scenario for output
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  @severity=normal
  Scenario: Verify the response status is 200 when get method is used for Multi Account Details API

    Given def info = karate.info
    And def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print "INPUTJSON is" + inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token

    Given set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[*].AccountId contains '#notnull'

  @AISP_API_Content
  @severity=normal
  Scenario: Verify the response status is 200 when invalid Content-Type is used for Multi Account Details API

    Given def info = karate.info
    And def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print "INPUTJSON is" + inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token

    Given set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    And set inputJSON $.Content_type = "application/xml"
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[*].AccountId contains '#notnull'

  @AISP_API
  @severity=normal
  Scenario: Verify the response status is 200 when blank Content-Type is used for Multi Account Details API

    Given def info = karate.info
    And def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print "INPUTJSON is" + inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token

    Given set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    And set inputJSON $.Content_type = " "
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[*].AccountId contains '#notnull'


  @AISP_API_structure
  @severity=normal
  Scenario: Verify the response message structure when valid access token in provided as intput to Get Multi Accounts API

    Given def info = karate.info
    And def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And print "INPUTJSON is" + inputJSON
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token

    Given set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    And set inputJSON $.Content_type = "application/xml"
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].AccountId contains  '#string'
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].Currency contains   '#string'
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].Nickname contains   '#string'
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].Account[*].SchemeName contains  '#string'
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].Account[*].Identification contains  '#string'
    And def flag1 = (CMA_Rel_Ver == '2.0' ? '#string' : '#notpresent')

    And match each apiApp.Multi_Account_Information.response.Data.Account[*].AccountType contains flag1
    And match each apiApp.Multi_Account_Information.response.Data.Account[*].AccountSubType contains flag1






