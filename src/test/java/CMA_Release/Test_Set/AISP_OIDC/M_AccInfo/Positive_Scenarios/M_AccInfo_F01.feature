@AISP_UAT_All_API
@AISP
@AISP_Consent
@AISP_MultiAccInfo
@Functional_Shakedown

Feature: This feature is to Test functionality of MultiAccountInfo API with valid combination of Permissions

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify that Status code 200 is displayed with no Account id in the response body when ReadAccountbasic is provided in permissions of Account Setup API

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Permission_ReadAccountbasic"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set permissions $.Data.Permissions = ["ReadAccountsBasic"]
    And set Application_Details $.usr = user_details.Generic.G_User_1.user
    And set Application_Details.TestCasePath = info1.path
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Result.UI_Output = callE2E.UIResult
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And set Application_Details.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.response.Data.Account[0] !contains {Account: '#notnull'}

  Scenario: Verify that Status code 200 is displayed when ReadAccountDetail is provided in permissions of Account Setup API and Account id is displayed in response body for which user has provided consent

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Permission_ReadAccountsDetail"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.usr = user_details.Generic.G_User_1.user
    And set permissions $.Data.Permissions = ["ReadAccountsDetail"]
    And set Application_Details.TestCasePath = info1.path
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Result.UI_Output = callE2E.UIResult
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And set Application_Details.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.response.Data.Account[0] contains {Account: '#notnull'}

  Scenario: Verify that Status code 200 is displayed when ReadAccountBasic and ReadAccountDetail is provided in permissions of Account Setup API and Account id is displayed in response body for which user has provided consent

    * def info = karate.info
    * set info.Screens = 'Yes'
    * set info.subset = "Permission_ReadAccountsBasic_ReadAccountsDetail"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set permissions $.Data.Permissions = ["ReadAccountsBasic","ReadAccountsDetail"]
    And set Application_Details $.usr = user_details.Generic.G_User_1.user
    And set Application_Details.TestCasePath = info1.path
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Result.UI_Output = callE2E.UIResult
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And set Application_Details.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.response.Data.Account[0] contains {Account: '#notnull'}


