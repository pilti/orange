@Functional_Shakedown_E2E
Feature: CCToken creation

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/PISP/Data/'

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }

      """
    * def Application_Details = {}

  Scenario Outline: To Verify if CC_Token is getting generated when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = "Access_Token_CCG"
    Given json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    * def access_token = apiApp.Access_Token_CCG.response.access_token
    * def fileName = 'Access_Token_CCG.properties'
    * def id = 'Access_Token_CCG'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      access_token: '#(access_token)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)
  @PISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @PISP_Sanity=PISP
    Examples:
      | TPP  |
      | PISP |