@Functional_Shakedown_E2E
Feature: This feature is to create access_token using authcode

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'AuthCode.properties'
    * def name = 'AuthCode'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/PISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify if access_token and refresh_token is getting generated using authcode when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = "AccessTokenUsingRefreshToken"
    Given json Application_Details = active_tpp.<TPP>
    And set Application_Details $.code = profile.code
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect_uri = Application_Details.redirect_uri
    When call apiApp.Access_Token_ACG(Application_Details)
    Then match apiApp.Access_Token_ACG.responseStatus == 200

    * set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    * set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    * def fileName = 'access_token.properties'
    * def id = 'token'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      access_token: '#(Application_Details.access_token)',
      refresh_token: '#(Application_Details.refresh_token)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)

  @PISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @PISP_Sanity=PISP
    Examples:
      | TPP  |
      | PISP |
