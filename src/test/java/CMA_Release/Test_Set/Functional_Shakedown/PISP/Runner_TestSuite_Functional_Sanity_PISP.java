package CMA_Release.Test_Set.Functional_Shakedown.PISP;

import CMA_Release.Test_Set.Functional_Shakedown.AISP.Runner_AISP_Functional_AISP;
import CMA_Release.Test_Set.Functional_Shakedown.AISP.Runner_AISP_Functional_AISP_PISP;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                Runner_PISP_Functional_PISP.class,
                Runner_PISP_Functional_AISP_PISP.class,
        }
)
public class Runner_TestSuite_Functional_Sanity_PISP {

}
