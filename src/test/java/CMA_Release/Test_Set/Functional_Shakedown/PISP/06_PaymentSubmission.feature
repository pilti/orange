@Functional_Shakedown_E2E
Feature: This feature is to create access_token using authcode

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'access_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/PISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * json profile1 = RWPropertyFile.ReadFile(Datapath,"Payment_Setup.properties","Payment_Setup")

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify if Payment submission is successful when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = "Payment_Submission"

    Given json Application_Details = active_tpp.<TPP>
    And remove pisp_payload.Data.Initiation.DebtorAccount
    And set Application_Details $.access_token = profile.access_token
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.PaymentId = profile1.PaymentId
    And set Application_Details $.token_type = "Bearer"
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.idempotency_key = profile1.idempotency_key
    And set pisp_payload.Data.PaymentId = profile1.PaymentId
    When call apiApp.Payment_Submission(Application_Details)
    Then match apiApp.Payment_Submission.responseStatus == 201
    And match apiApp.Payment_Submission.response.Data.Status != "Rejected"

  @PISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @PISP_Sanity=PISP
    Examples:
      | TPP  |
      | PISP |
