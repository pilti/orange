@Functional_Shakedown_E2E
Feature: This feature is to construct consent url and customer providing consent

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def fileName = 'JWT.properties'
    * def name = 'JWT'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/PISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * def oidc_AISP_Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def oidc_PISP_Functions =  Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_PISP.Entities.OIDC_PISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Forming authorize request using jwt and performing consent when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.Screens = 'Yes'
    * set info.subset = "Consent"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given json Application_Details = active_tpp.<TPP>
    And set Application_Details $.jwt = profile.request
    And set Application_Details $.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl



    #* def consenturl = 'http://www.bbc.com'
###########################################################################################################################

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = oidc_AISP_Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'

    Given def consentData = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    And set consentData.Confirmation = "Yes"
    And set consentData.debtorFlag = (pisp_payload.Data.Initiation.DebtorAccount == null ? 'No' : 'Yes')
    When def perform_loginConsent = oidc_PISP_Functions.loginConsent(webApp,consentData)
    Then match perform_loginConsent.Status == 'Pass'
   # And def ActPaymentDetails = oidc_PISP_Functions.verifyPaymentDetails(webApp)
    And def Payee_name = pisp_payload.Data.Initiation.CreditorAccount.Name
    And json PayloadInput = {"Payee name":'#(Payee_name)'}
    And set PayloadInput.Amount = pisp_payload.Data.Initiation.InstructedAmount.Amount
    And set PayloadInput.Reference = pisp_payload.Data.Initiation.CreditorAccount.SecondaryIdentification
    #And match ActPaymentDetails == PayloadInput
    And def perform_selectFromAccount = oidc_PISP_Functions.selectFromAccount(webApp,consentData)
    And match perform_selectFromAccount.Status == 'Pass'
    And def performConsentAction = oidc_PISP_Functions.performConsentAction(webApp,consentData)
    And set Application_Details.code = performConsentAction.AuthCode
    And set UIResult.code = performConsentAction.AuthCode
    #And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    #And def screenshot = capturescreen.captureScreenShot2(webApp)
    #And def stop = webApp.stop()

    * def fileName = 'AuthCode.properties'
    * def id = 'AuthCode'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      code: '#(UIResult.code)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)
  @PISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @PISP_Sanity=PISP
    Examples:
      | TPP  |
      | PISP |
