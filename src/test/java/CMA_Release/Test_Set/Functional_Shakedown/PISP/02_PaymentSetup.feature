@Functional_Shakedown_E2E
Feature: Payment Setup

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null

    * def fileName = 'Access_Token_CCG.properties'
    * def name = 'Access_Token_CCG'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/PISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }

      """
    * def Application_Details = {}

  Scenario Outline: To Verify if Payment ID is getting generated when TPP is #key#

    Given def info = karate.info
    And def key = '<TPP>'
    And set info.subset = "Payment_Setup"
    And json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.access_token = profile.access_token
    And set Application_Details.request_method = "POST"
    And remove pisp_payload.Data.Initiation.DebtorAccount
    When call apiApp.Payment_Setup(Application_Details)
    Then match apiApp.Payment_Setup.responseStatus == 201

    * def idempotency_key = apiApp.Payment_Setup.idempotency_value
    * def PaymentId = apiApp.Payment_Setup.response.Data.PaymentId
    * def fileName = 'Payment_Setup.properties'
    * def id = 'Payment_Setup'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      PaymentId: '#(PaymentId)',
      idempotency_key: '#(idempotency_key)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)

  @PISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @PISP_Sanity=PISP
    Examples:
      | TPP  |
      | PISP |