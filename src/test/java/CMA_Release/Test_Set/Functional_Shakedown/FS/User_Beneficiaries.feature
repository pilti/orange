
@FS_Shakedown
Feature: FS customer profile request - single account

  Background:
  # Parameters
    * def data = fs_data
    * remove data $.accountNumber
    * remove data $.nsc

  Scenario: GET customer profile using FS service
    Given url fsAcctBeneficiaryUrl + data.profileId + '/beneficiaries'
    And param userId = data.profileId
    And headers data
    When method GET
    Then status 200
