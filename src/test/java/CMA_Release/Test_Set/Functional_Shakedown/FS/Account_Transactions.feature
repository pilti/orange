
@FS_Shakedown
Feature: FS customer profile request - single account

  Background:/fs-abt-service/services/abt/
  # Parameters
    * def data = fs_data
   #hard coded with working data
    * set data.accountNumber = '11960154'
    * set data.nsc = '900295'
    * set data.X-BOI-USER = '986676'

  Scenario: GET customer profile using FS service
    Given url fsAcctBalUrl + data.nsc + '/' + data.accountNumber+ '/transactions'
    And param startDate = '2016-01-01'
    And param endDate = '2019-06-20'
    And param pageNumber = '1'
    And param pageSize = '6'
    And param txnType = 'ALL'
    And headers data
    When method GET
    Then status 200
