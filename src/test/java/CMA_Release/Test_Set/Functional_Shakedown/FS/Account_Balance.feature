@FS_Shakedown
Feature: FS customer profile request - single account

  Background:
  # Parameters
    * def data = fs_data

  Scenario: GET customer profile using FS service
    Given url fsAcctBalUrl + data.nsc + '/' + data.accountNumber
    And headers data
    When method GET
    Then status 200
