
@FS_Shakedown
Feature: FS customer profile request - single account

  Background:
  # Parameters
    * def data = fs_data
    #hard coded with working data
    * set data.accountNumber = '61412682'
    * remove data.profileId

  Scenario: GET customer profile using FS service
    Given url fsAcctSOUrl + data.nsc + '/' + data.accountNumber+ '/standingorders'
    And headers data
    When method GET
    Then status 200
