
@FS_Shakedown
Feature: FS customer profile request

  Background:
  # Parameters
    * def data = fs_data
    * remove data $.accountNumber
    * remove data $.nsc


  Scenario: GET customer profile using FS service
    Given url fsCusProfileUrl + data.profileId +'/accounts'
    And headers data
    When method GET
    Then status 200
    * string out = karate.prettyXml(response)
    #* def result = Java.type('CMA_Release.Java_Lib.TextFileWriting').testout(out,"Account_Request_Out.xml")
    * print karate.prettyXml(response)