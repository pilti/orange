@Environment_Shakedown

Feature: This feature is for Environment shakedown test

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @TppHub
  Scenario: Test TPPHub and OpenBanking connectivity from TPP portal

    Given def info = karate.info
    And set info.Screens = 'Yes'
    And set info.subset = 'TPP_HUB'
    And def TPPResFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubResusables')
    And def TPPTestFunctions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.TPPHub.Entities.TPPHubTest')
    And json ptc = read('classpath:Resources/PTC/ptc-info.json')
    And json logindetails = ptc.PTC_2
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    And def path = call  webApp.path1 = info1.path

    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = TPPTestFunctions.Launch_TPP_Hub(webApp,TPPHubUrl)
    And def perform = TPPResFunctions.PTC_Login_with_OB(webApp,logindetails)
    And def perform = TPPResFunctions.Verify_PTC_Login_Details(webApp,logindetails)
    When match perform.Status == "Pass"
    Then def perform = webApp.stop()