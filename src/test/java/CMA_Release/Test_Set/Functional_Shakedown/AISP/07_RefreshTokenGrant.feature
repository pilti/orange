@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_7
Feature: This feature is to create access Token using refresh_token

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'access_refresh_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
     And call apiApp.configureSSL(profile)

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: Access token is generated_TPP_#key#

    * def info = karate.info
    * set info.subset = "RefreshToken"
    * def key = '<TPP>'
    * match profile != {}
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |

###############################################################################################################

#  Scenario Outline: Access token is not generated when it is used by other TPP#key#
#
#    * def info = karate.info
#    * set info.subset = "RefreshToken"
#    * def key = '<TPP>'
#    * def apiApp1 = new apiapp()
#    * def apiApp2 = new apiapp()
#    * match profile != {}
#    Given def inputJSON =
#    """
#    { client_id: '#(profile.client_id)',
#      client_secret: '#(profile.client_secret)',
#      request_method: "POST",
#      refresh_token: '#(profile.refresh_token)'
#    }
#    """
#
#    And def Application_Details = active_tpp.AISP
#    And call apiApp1.configureSSL(Application_Details)
#    And set Application_Details $.grant_type = "refresh_token"
#    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
#    And set Application_Details $.request_method = inputJSON.request_method
#    And set Application_Details $.refresh_token = inputJSON.refresh_token
#    When call apiApp1.Access_Token_RTG(Application_Details)
#    Then match apiApp1.Access_Token_RTG.responseStatus == 400
#
#    Given def Application_Details2 = active_tpp.<TPP>
#    And call apiApp2.configureSSL(Application_Details2)
#    And set Application_Details2 $.grant_type = "refresh_token"
#    And set Application_Details2 $.Content_type = "application/x-www-form-urlencoded"
#    And set Application_Details2 $.request_method = inputJSON.request_method
#    And set Application_Details2 $.refresh_token = inputJSON.refresh_token
#    When call apiApp2.Access_Token_RTG(Application_Details2)
#    Then match apiApp2.Access_Token_RTG.responseStatus == 200
#
#  @AISP_Sanity=AISP_PISP
#    Examples:
#      | TPP       |
#      | AISP_PISP |

