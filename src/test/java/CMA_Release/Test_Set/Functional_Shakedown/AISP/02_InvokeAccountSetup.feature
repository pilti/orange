@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_2
Feature: AccountRequestId Creation using CCG token

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * def fileName = 'Access_Token_CCG.properties'
    * def name = 'Access_Token_CCG'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify_responseStatus_PostAccRerq_made_TPP_#key#

    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}
    Given json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)
    #Set input with the request Method

    Given set Application_Details.access_token = profile.access_token
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.request_method = 'POST'
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response == SchemaAccountRequest.success
    #Generate Properties file that will act as an input for further API
    * def AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * def fileName = 'Account_Request_Setup.properties'
    * def id = 'Account_Request_Setup'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(AccountRequestId)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |


