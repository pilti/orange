@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_5
Feature: This feature is to construct consent url and customer providing consent

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def fileName = 'JWT.properties'
    * def name = 'JWT'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Forming_authorize_request_TPP_#key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.Screens = 'Yes'
    * set info.subset = "Consent"
    * match profile != {}
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given json Application_Details = active_tpp.<TPP>
    And set Application_Details $.jwt = profile.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    #added to save the consnet url , & launch again during authorisation renewal
    And print consenturl
    And set Application_Details $.consenturl = consenturl
    And def consent_redirecturi = apiApp.ConstructAuthReqUrl.redirect_uri

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform = Functions.selectAllAccounts(webApp,data)
    And set UIResult.AccountSelectionPage = perform
    And match perform.Status == 'Pass'
    And set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And def perform = Functions.reviewConfirm(webApp,data)

    * print "****************************************************"
    And print perform.Action.Authcode


    And set Application_Details.ConsentExpiry = perform.ConsentExpiry
    #added to save the permissions selected during SCA , & verify during authorisation renewal
    And set Application_Details.Permissions = perform.Permissions
    And set Result.UIOutput.ReviewConfirm.Permissions =  perform.Permissions
    And set Result.UIOutput.ReviewConfirm.Permissions = perform.Permissions
    And set Result.UIOutput.ReviewConfirm.ConsentExpiry = perform.ConsentExpiry
    And set UIResult.ReviewConfirmPage = perform
    Then set Application_Details.code = perform.Action.Authcode
    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    And set UIResult.redirect_uri = perform.Action.redirect_uri
    And def PostConsent_redirecturi = perform.Action.redirect_uri
#    And match PostConsent_redirecturi == consent_redirecturi
#    And print "matchdone1"
#    And match PostConsent_redirecturi == profile.redirect_uri
#    And print "matchdone2"
    * def stop = webApp.stop()


    #Generate Properties file that will act as an input for further API
    * def fileName = 'AuthCode.properties'
    * def id = 'AuthCode'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      code: '#(UIResult.code)',
      consenturl: '#(consenturl)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |