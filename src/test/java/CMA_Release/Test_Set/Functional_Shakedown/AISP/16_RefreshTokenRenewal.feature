@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_16
Feature:  Get DirectDebits Information for consented Account by using /accounts/{AccountId}/direct-debits API

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'

    * def ReusableFunctions = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def ObjectsRenewalScreen = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.RefreshTokenRenewal')
    * def screenshot = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def ObjectsReview_confirm = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Review_confirm')


    * def fileNameURL = 'AuthCode.properties'
    * def nameURL = 'AuthCode'
    * def DatapathURL = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profileURL = RWPropertyFile.ReadFile(DatapathURL,fileNameURL,nameURL)
    * match profileURL != {}
    * def curl = Functions.split_replace_Consenturl(profileURL.consenturl,'https:')
    * set Result.Input.ConsentURL = profileURL.ConsentURL
    * print curl


    * def fileNameRT = 'access_refresh_token.properties'
    * def nameRT = 'token'
    * def DatapathRT = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profileRT = RWPropertyFile.ReadFile(DatapathRT,fileNameRT,nameRT)

    * call apiApp.configureSSL(profileURL)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_RefreshTokenRenewal
  @Functional_Shakedown
  @Functional_Shakedown_Quick
  Scenario Outline: Verify that the Refresh Token Renewal Landing Page is labelled as Authorisation Renewal when TPP is #key#
 # Get the Auth token and Refeshtoken using Authcode

    Given def inputJSON =
    """
    {
      client_id: '#(profileURL.client_id)',
      client_secret: '#(profileURL.client_secret)',
      request_method: "POST",
      refresh_token: '#(profileRT.refresh_token)'
    }
    """

    * def info = karate.info
    * def key = '<TPP>'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then set Result.Access_Token_RTG.responseStatus = apiApp.Access_Token_RTG.responseStatus
    And set Result.Access_Token_RTG.response = apiApp.Access_Token_RTG.response
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200

    Given remove inputJSON $.refresh_token
    When def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def perform = Functions.launchConsentURL(webApp,curl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.launchConsentURL_Status = perform.Status
    And set Result.ActualOutput.UI.Output.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Status = perform.Status
    And set Result.ActualOutput.UI.Output.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)

    * set info.Screens = 'Yes'
    * set info.subset = "Authorization renewal Title"

    * def modulename = call  webApp.moduleName1 = 'Authorisation Renewal Title'
    * def element = ReusableFunctions.CheckElementPresent(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    * def txt = ReusableFunctions.getElementText(webApp, ObjectsRenewalScreen.refreshTokenRenewalHeading)
    * set Result.ActualOutput.UI.Output.AuthorisationRenewal.PageLableExpected = 'Authorisation renewal'
    * set Result.ActualOutput.UI.Output.AuthorisationRenewal.PageLableActual = txt
    * match element == true
    * match txt == Result.ActualOutput.UI.Output.AuthorisationRenewal.PageLableExpected
    * def modulename = call  webApp.moduleName1 = "On clicking ReNew Button"
    * def click = ReusableFunctions.ClickElement(webApp, ObjectsRenewalScreen.renewButton)
    * set Result.ActualOutput.UI.Output.ClickRenew = click

     # take renewal journey Authcode
    * set Result.ActualOutput.UI.Output.On_Renew.Expected = 'Authcode in Redirect URL'
    * set Result.ActualOutput.UI.Output.On_Renew.Actual = Functions.getUrl(webApp)
    * def perform = Functions.getAuthCodeFromURL(webApp)
    * set Result.ActualOutput.UI.Output.NewAuthCode = perform.Authcode
    * def codeRenew = perform.Authcode
    * def stop = webApp.stop()

    Given json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.code = codeRenew
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"
    When call apiApp.Access_Token_ACG(Application_Details)

    Then match apiApp.Access_Token_ACG.responseStatus == 200
    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.ActualOutput.UI.Output.Access_Token_ACG_afterRenew.response = apiApp.Access_Token_ACG.response

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |