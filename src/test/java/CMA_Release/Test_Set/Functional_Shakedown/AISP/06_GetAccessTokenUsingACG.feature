@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_6
Feature: This feature is to create access_token using authcode

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'AuthCode.properties'
    * def name = 'AuthCode'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify_access_token and refresh_token_using_authcode_#key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = "AccessTokenUsingRefreshToken"
    * match profile != {}
    Given json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.code = profile.code
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"
    When call apiApp.Access_Token_ACG(Application_Details)

    Then match apiApp.Access_Token_ACG.responseStatus == 200

    * set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    * set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token
    * def fileName = 'access_refresh_token.properties'
    * def id = 'token'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      access_token: '#(Application_Details.access_token)',
      refresh_token: '#(Application_Details.refresh_token)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)
#    * def refreshTokenFolder = Java.type('CMA_Release.Java_Lib.createFolder').Folder_path("./src/test/java/Resources/Reftoken/")
#    * def refreshTokenFile = Java.type('CMA_Release.Java_Lib.CreateFiles').write("./src/test/java/Resources/Reftoken/Reftoken.json",karate.pretty(output))

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |
