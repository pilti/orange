@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_10
Feature: Verify the method used in Get Multi Account Details

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'access_refresh_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    And call apiApp.configureSSL(profile)

    #After scenario for output
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: Verify the response status is 200 when GET method is used for Single Account Balance API when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}
    Given def inputJSON =
    """
    {
      client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}

    * def accNumber = (accNumber == null ? "x" : accNumber)
    * def array_account_num = get apiApp.Multi_Account_Information.response.Data.Account[*].Account.Identification
    * def index = null
    * eval for(var i = 0; i < array_account_num.length; i++) if (array_account_num[i].contains(accNumber)) index = i
    * def index = (index != -1 ? index : null )
    * def index = (index == null ? 0 : index )

    Given def inputJSONSB = inputJSON
    And set inputJSONSB.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Balance(inputJSONSB)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response.Data.Balance[0] contains {AccountId: '#notnull'}
    And match apiApp.Account_Balance.response.Data.Balance[0].AccountId == inputJSONSB.AccountId

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |


  Scenario Outline: Verify SingleAccBalabce response when access Token generated using Refresh Token when TPP is #key#

    * def path = 'classpath:Resources/Reftoken/Reftoken_' +ActiveEnv+ '.json'
    * def profile = karate.read(path)
    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}
    Given def inputJSON =
    """
    {
      client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}

    * def accNumber = (accNumber == null ? "x" : accNumber)
    * def array_account_num = get apiApp.Multi_Account_Information.response.Data.Account[*].Account.Identification
    * def index = null
    * eval for(var i = 0; i < array_account_num.length; i++) if (array_account_num[i].contains(accNumber)) index = i
    * def index = (index != -1 ? index : null )
    * def index = (index == null ? 0 : index )

    Given def inputJSONSB = inputJSON
    And set inputJSONSB.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Balance(inputJSONSB)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response.Data.Balance[0] contains {AccountId: '#notnull'}
    And match apiApp.Account_Balance.response.Data.Balance[0].AccountId == inputJSONSB.AccountId

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |



