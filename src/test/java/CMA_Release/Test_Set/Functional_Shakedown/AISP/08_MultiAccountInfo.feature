@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_8
Feature: Verify the method used in Get Multi Account Details

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'access_refresh_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    And call apiApp.configureSSL(profile)

    #After scenario for output
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Functional_Shakedown_Quick_Run
  @AISP_API_Multi
  @severity=normal
  Scenario Outline: Verify the response status is 200 when get method is used for Multi Account Details API when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    * print (inputJSON)

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}
    And print "************ " + SchemaMultiAccInfo.success.Data.Account[0]
    And match apiApp.Multi_Account_Information.response.Data.Account[*] contains SchemaMultiAccInfo.success.Data.Account[0]

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
   Examples:
      | TPP  |
      | AISP |

  Scenario Outline: Verify MultiAccount response when access Token generated using Refresh Token when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * def path = 'classpath:Resources/Reftoken/Reftoken_' +ActiveEnv+ '.json'
    * def profile = karate.read(path)
    * match profile != {}
    Given def inputJSON =
      """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)

    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    * print (inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountId: '#notnull'}
    And match apiApp.Multi_Account_Information.response.Data.Account[*] contains SchemaMultiAccInfo.success.Data.Account[0]

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |

