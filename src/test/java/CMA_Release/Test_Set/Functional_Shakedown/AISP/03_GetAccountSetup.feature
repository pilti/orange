@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_3
Feature: AccountRequestId Creation using CCG token

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * def fileName = 'Access_Token_CCG.properties'
    * def fileName1 = 'Account_Request_Setup.properties'
    * def name = 'Access_Token_CCG'
    * def name1 = 'Account_Request_Setup'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * json profile1 = RWPropertyFile.ReadFile(Datapath,fileName1,name1)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Verify_responseStatus_AccountReqRetrieve_TPP_#key#

    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}
    Given json Application_Details = active_tpp.<TPP>
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.access_token = profile.access_token
    And set Application_Details.AccountRequestId = profile1.AccountRequestId
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    And match apiApp.Account_Request_Retrieve.response == SchemaAccountRequest.success

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |