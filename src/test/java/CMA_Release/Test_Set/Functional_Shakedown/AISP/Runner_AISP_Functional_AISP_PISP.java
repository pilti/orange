package CMA_Release.Test_Set.Functional_Shakedown.AISP;

import Runners.Test_Runner_Parallel;
import cucumber.api.CucumberOptions;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;

import java.io.File;
import java.io.IOException;

@CucumberOptions(tags = {"@AISP_Sanity=AISP_PISP"})
public class Runner_AISP_Functional_AISP_PISP extends Test_Runner_Parallel {
    @BeforeClass
    public static void prerequisite() {
        File dir = new File("./src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data");
        if (dir.exists()) {
            try {
                FileUtils.deleteDirectory(dir);
            } catch (IOException e) {
                System.out.println("Exception Occurred is: " + e.getMessage());
            }
        }
    }
}
