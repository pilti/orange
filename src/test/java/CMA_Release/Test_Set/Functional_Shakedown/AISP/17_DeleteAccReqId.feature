@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_17
Feature: This feature is to verify the status code for Delete Account Request API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'Account_Request_Setup.properties'
    * def name = 'Account_Request_Setup'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @Functional_Shakedown_Quick
  @Umesh
  Scenario Outline: Verify that Status code 204 is displayed when all valid header values are provided in DELETE Account Request when TPP is #key#

    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = 'TC09_Delete_Positive'
    * match profile != {}

    Given json Application_Details = active_tpp.<TPP>
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AccountRequestId = profile.AccountRequestId
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.request_method = 'DELETE'
    And set Application_Details.token_type = 'Bearer'
    And def AcctReqUrl = AcctReqUrl + "/" + AccountRequestId
    And call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 204

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |