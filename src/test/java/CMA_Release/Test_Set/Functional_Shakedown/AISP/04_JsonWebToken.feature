@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_4
Feature: Create JWT

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'Account_Request_Setup.properties'
    * def name = 'Account_Request_Setup'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_API
  Scenario Outline: Creating_JWT_TPP_#key#
    * def info = karate.info
    * def key = '<TPP>'
    * set info.subset = "JWT"
    * match profile != {}

    Given json Application_Details = active_tpp.<TPP>
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = profile.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = profile.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And print ReqObjIP
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.JWT.Input.SigningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    When call apiApp.CreateRequestObject(Application_Details)
    Then match apiApp.CreateRequestObject.jwt.request != null

    * def request1 = apiApp.CreateRequestObject.jwt.request
    * def fileName = 'JWT.properties'
    * def id = 'JWT'
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      ReqObjIP: '#(ReqObjIP)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      request: '#(request1)'
    }
    """

    * def profile = RWPropertyFile.WriteFile(Datapath,fileName,id,output)

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |