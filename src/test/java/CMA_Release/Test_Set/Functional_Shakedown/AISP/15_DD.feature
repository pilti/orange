@Functional_Shakedown_E2E
@Functional_Shakedown_E2E_15
Feature:  Get DirectDebits Information for consented Account by using /accounts/{AccountId}/direct-debits API

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'access_refresh_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @AISP_SingleAccDD
  @Functional_Shakedown
  @Functional_Shakedown_Quick
  Scenario Outline: To verify TPP is able to call Single Account Direct Debits API and gets 200 OK response status when TPP is #key#
 # Get the Auth token and Refeshtoken using Authcode
    * print profile
    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}

    Given def inputJSON =
    """
    {
      client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    * def accNumber = (accNumber == null ? "x" : accNumber)
    * def array_account_num = get apiApp.Multi_Account_Information.response.Data.Account[*].Account.Identification
    * def index = null
    * eval for(var i = 0; i < array_account_num.length; i++) if (array_account_num[i].contains(accNumber)) index = i
    * def index = (index != -1 ? index : null )
    * def index = (index == null ? 0 : index )

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[index].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then match apiApp.Account_Direct_Debits.responseStatus == 200

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |


  Scenario Outline: Verify DD response when access Token generated using Refresh Token when TPP is #key#

    * def path = 'classpath:Resources/Reftoken/Reftoken_' +ActiveEnv+ '.json'
    * def profile = karate.read(path)

    * def info = karate.info
    * def key = '<TPP>'
    * match profile != {}

    Given def inputJSON =
    """
    {
      client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    * def accNumber = (accNumber == null ? "x" : accNumber)
    * def array_account_num = get apiApp.Multi_Account_Information.response.Data.Account[*].Account.Identification
    * def index = null
    * eval for(var i = 0; i < array_account_num.length; i++) if (array_account_num[i].contains(accNumber)) index = i
    * def index = (index != -1 ? index : null )
    * def index = (index == null ? 0 : index )

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[index].AccountId
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And set inputJSON $.URL = AcctInfoUrl + inputJSON.AccountId + '/direct-debits'
    When call apiApp.Account_Direct_Debits(inputJSON)
    Then match apiApp.Account_Direct_Debits.responseStatus == 200

  @AISP_Sanity=AISP_PISP
    Examples:
      | TPP       |
      | AISP_PISP |

  @AISP_Sanity=AISP
    Examples:
      | TPP  |
      | AISP |