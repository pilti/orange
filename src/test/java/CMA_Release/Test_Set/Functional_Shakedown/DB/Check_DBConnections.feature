@DB_Shakedown
Feature: Check DB Connections

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    And def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

Scenario: Test ABT DB connection

Given def info = karate.info
And set info.subset = 'ABT_DB_Connection'

And def connection = ABT_DB.ConnectionCheck(ABT_DB_Conn)

And match connection == "Connected database successfully"

And set Result.ABT_DB_Connection = "Connection established successfully"

Scenario: Test SP SHARED DATA DB connection

Given def info = karate.info
And set info.subset = 'SP_SHARED_DATA_Connection'

And def connection = ABT_DB.ConnectionCheck(SP_SHARED_DATA_DB_Conn)
And match connection == "Connected database successfully"

And set Result.SP_SHARED_DATA_DB_Conn = "Connection established successfully"

Scenario: Test CHANNEL_PROFILE_DB connection

Given def info = karate.info
And set info.subset = 'CHANNEL_PROFILE_DB_Connection'

And def connection = ABT_DB.ConnectionCheck(CHANNEL_PROFILE_DB_Conn)
And match connection == "Connected database successfully"

Then set Result.CHANNEL_PROFILE_DB_Conn = "Connection established successfully"