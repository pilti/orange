@check_trans
Feature: Verify that valid response is obtained for SingleAccountInfo API

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def fileName = 'access_refresh_token.properties'
    * def name = 'token'
    * def Datapath = './src/test/java/CMA_Release/Test_Set/Functional_Shakedown/AISP/Data/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)

    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify that valid response is obtained when response of multiAccountInfo is feeded as input to SingleAccountInfo API
    * def info = karate.info
    * def profile = karate.read('classpath:Resources/Reftoken/Reftoken_SIT.json')
    * match profile != {}
    Given def inputJSON =
    """
      {
        client_id: '#(profile.client_id)',
        client_secret: '#(profile.client_secret)',
        request_method: "POST",
        refresh_token: '#(profile.refresh_token)'
      }
    """

    * print profile
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
#Basic N2d6Y2tyckRGOHNIc0phVmh5ajFDMTpBSEhZdURQQzhvMUtmMjR5b0ZrY3pseDFaTW5UTGEyYg==
    Given set inputJSON.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And set inputJSON.request_method = 'GET'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given json SingleAccInfo_input = get apiApp.Multi_Account_Information.response.Data.Account[*]
    And def access_token = inputJSON.access_token
    And def client_id = inputJSON.client_id
    And def client_secret = inputJSON.client_secret
    And def request_method = inputJSON.request_method
    And def token_type = inputJSON.token_type

    When def SingleAccInfo_call = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/Account_Transaction.feature') SingleAccInfo_input

    Then match each SingleAccInfo_call[*].responseStatus == 200
    And match each SingleAccInfo_call[*].response contains SchemaTransactions.success

    * def requestUrl = get SingleAccInfo_call[*].requestUri
    * def SingleAccInfo = get SingleAccInfo_call[*].response
    * def requestHeader = get SingleAccInfo_call[*].requestHeaders
    * set Result.AccountInformation.Output.response = get SingleAccInfo
    * set Result.AccountInformation.Input.requestHeader = requestHeader
    * set Result.AccountInformation.Input.URL = requestUrl
    * remove Result.AccountInformation.Input.Header

    And match SingleAccInfo_call[*].responseStatus !contains 400
    And match SingleAccInfo_call[*].responseStatus !contains 500
    And match SingleAccInfo_call[*].responseStatus !contains 405


