@Consent_Release2
  @Regression
Feature: This feature is to test if new permissions are not reflected in consent

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario: Negative test to validate new permissions are not populated on consent screen

    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    ##CCToken
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    ##PostAccSetup

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.request_method = 'POST'
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    ##RequestObject Creation
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.JWT.Input.SigningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    When call apiApp.CreateRequestObject(Application_Details)
    Then assert apiApp.CreateRequestObject.jwt.request != null

    ##ConsentUrlCreation
    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.redirect_uri = 'https://boi.com'
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    Given set Application_Details $.consenturl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'

    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform = Functions.selectAllAccounts(webApp,data)
    Then match perform.Status == 'Pass'

    Given set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    When def perform = Functions.reviewConfirm(webApp,data)

    * json val = perform.Permissions.PermissionsInDetail
    * def retrieved_permission_heading = Java.type('CMA_Release.Java_Lib.GetKey').getKeyVal(val)
    * string retrieved_permission_heading_str = retrieved_permission_heading
    * def offer_val = retrieved_permission_heading_str.contains("Offers")
    * def Pan_val = retrieved_permission_heading_str.contains("PAN")
    * def Party_val = retrieved_permission_heading_str.contains("Party")
    * def PSU_val = retrieved_permission_heading_str.contains("PSU")
    * def Scheduled_val = retrieved_permission_heading_str.contains("Scheduled")
    * def Statement_val = retrieved_permission_heading_str.contains("Statements")

    Then match offer_val == false
    And match Pan_val == false
    And match Party_val == false
    And match PSU_val == false
    And match Scheduled_val == false
    And match Statement_val == false
