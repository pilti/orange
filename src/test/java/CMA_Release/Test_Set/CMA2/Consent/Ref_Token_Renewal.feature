@Consent_Release2
  @Regression
Feature: This feature is to test if new permissions are not reflected in RefreshTokenRenewal

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * def curl = Functions.split_replace_Consenturl(profile.ConsentURL,'https:')
    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario: Negative test to validate new permissions are not populated on RefreshTokenRenewal screen

    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path
    * print "Path of Screenshot is:" + webApp.path1

    Given def Application_Details = {}
    And set Application_Details $.consenturl = curl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,Application_Details.consenturl)
    Then match perform.Status == 'Pass'

    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(profile.usr)',"otp":'#(profile.otp)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'

    And def perform = Functions.Permission(webApp)

    * json val = perform.Permissions.PermissionsInDetail
    * def retrieved_permission_heading = Java.type('CMA_Release.Java_Lib.GetKey').getKeyVal(val)
    Then match retrieved_permission_heading == profile.retrieved_permission_heading

    * string retrieved_permission_heading_str = retrieved_permission_heading
    * def offer_val = retrieved_permission_heading_str.contains("Offers")
    * def Pan_val = retrieved_permission_heading_str.contains("PAN")
    * def Party_val = retrieved_permission_heading_str.contains("Party")
    * def PSU_val = retrieved_permission_heading_str.contains("PSU")
    * def Scheduled_val = retrieved_permission_heading_str.contains("Scheduled")
    * def Statement_val = retrieved_permission_heading_str.contains("Statements")

    Then match offer_val == false
    And match Pan_val == false
    And match Party_val == false
    And match PSU_val == false
    And match Scheduled_val == false
    And match Statement_val == false
