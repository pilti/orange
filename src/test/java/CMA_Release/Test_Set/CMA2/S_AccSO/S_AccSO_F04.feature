@AISP_StandingOrder
@Regression
Feature: API - SO -  CAM1.1 End point backword compability


  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
  #To read Test Data#
    * def fileName = 'S_AccSO_TestData.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccSO/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  #Configure Network Certificate#
    * call apiApp.configureSSL(profile)
 #To write test result and error details to output file#
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


  Scenario: Verify that data section of response doesn't have Status field

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account SO API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.response.Data.StandingOrder[*] !contains ("StandingOrderStatusCode")


  Scenario: Verify that Self field of Link section of response is not affected and has cma1.1 link
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

#Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.response.Links contains {"Self": '#present'}
    And def link = '/accounts/' + inputJSON.AccountId + '/standing-orders'
    And match apiApp.Account_Standing_Orders.response.Links.Self == link


      @ad
  Scenario: Verify that Self field of Link section of response as account request id as per Data section
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

 #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    And def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.response.Links contains {"Self": '#present'}
    And def text = apiApp.Account_Standing_Orders.response.Links.Self
    And assert text.contains(inputJSON.AccountId)