@AISP_StandingOrder

Feature:  Get Single Account Standing Orders API with ReadStandingOrdersDetail permission

  Background:

    * def ABT_DB = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * set Result.TestStatus = 'Fail'
    * def fileName = 'S_AccSO_TestData.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccSO/Positive_Scenarios/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    * call apiApp.configureSSL(profile)
    * configure afterScenario =
      """
      function(){
       var info = karate.info;

       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """


    @Anudtter
    @Regression
  Scenario: To verify the response structure of Get Single Account Standing Orders API is CMA2.0 compliant

    Given def inputJSON =
"""
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match apiApp.Account_Standing_Orders.response.Data.StandingOrder[*] contains SchemaStandingOrder.success.Data.StandingOrder[0]


    @Regression
    Scenario: Verify that overall response json Structure has Data,links and Meta sections as per CMA 2.0 Specifications

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match apiApp.Account_Standing_Orders.response contains SchemaStandingOrder.Structure


    @Regression
    Scenario: Verify that link section of  response json has correct structure as per CMA 2.0 Specifications
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match apiApp.Account_Standing_Orders.response.Links contains SchemaStandingOrder.success.Links


    @Regression
    Scenario: Verify that Meta section of response json has correct structure as per CMA 2.0 Specifications
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    Given set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And remove inputJSON $.refresh_token
    And set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[2].AccountId
    And remove reqHeader $.x-fapi-customer-last-logged-time
    And remove reqHeader $.x-fapi-customer-ip-address
    And remove reqHeader $.x-fapi-interaction-id
    And remove reqHeader $.Accept
    And remove reqHeader $.Content-Type
    And remove reqHeader $.fromBookingDateTime
    And remove reqHeader $.toBookingDateTime
    When call apiApp.Account_Standing_Orders(inputJSON)
    Then match apiApp.Account_Standing_Orders.responseStatus == 200
    And match apiApp.Account_Standing_Orders.response.Meta contains SchemaStandingOrder.success.Meta



