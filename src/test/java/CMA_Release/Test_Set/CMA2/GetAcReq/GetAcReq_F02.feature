@AISP_GetAcReq
@Val_Whole_response11
@Regression
Feature: API - GET account request response json structure validation for CMA2.0

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @Val_Whole_response
  Scenario:Verify that overall  response json structure has Data, risk, links and Meta sections as per CMA 2.0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response == SchemaAccountRequest.success

  @Val_data_resp
  Scenario: Verify that Data section of response json has correct structure as per CMA 2.0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Data contains SchemaAccountRequest.success.Data

  @Val_Risk_Apiresp
  Scenario:Verify that Risk section of response json has correct structure as per CMA 2.0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Risk == SchemaAccountRequest.success.Risk


  @Val_Link_self_apiresp

  Scenario:Verify that link section of  response json has correct structure as per CMA 2.0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Links == SchemaAccountRequest.success.Links

  @Val_Meta_apiresp
  Scenario: Verify that Meta section of  response json has correct structure as per CMA 2.0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Meta == SchemaAccountRequest.success.Meta



