@AISP_GetAcReq
@Regression
Feature: API - GET account request -  CAM1.1 End point backward compatibility

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @CMA1.1StatusUpdateDateTime
  Scenario: Verify that the new field StatusUpdateDateTime is not present in CAM1.1 response

    Given json Application_Details = active_tpp.AISP_PISP
    When  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Data contains {StatusUpdateDateTime: '#notpresent'}

  @Val_Linksection
  Scenario: Verify that Self field of Link section of response is not affected and has cma1.1 link

    Given json Application_Details = active_tpp.AISP_PISP
    When  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Links.Self == "/account-requests/" + apiApp.Account_Request_Setup.response.Data.AccountRequestId


  @ResponseLinkValidation
  Scenario: Verify that Self field of Link section of response as account request id as per Data section

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Links.Self contains apiApp.Account_Request_Setup.response.Data.AccountRequestId

