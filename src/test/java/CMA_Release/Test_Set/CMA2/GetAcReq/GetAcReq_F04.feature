@AISP_GetAcReq
@Regression
Feature: API - GET account request Status and Statusupdate time field validation

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def info = karate.info
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def PageObject_AccSel = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')

    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario: Verify that CreationDateTime and StatusUpdateDateTime are same when the status is Awaiting Authorization

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    Given set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    Then match apiApp.Account_Request_Retrieve.response.Data contains { StatusUpdateDateTime : '#string'}
    And match apiApp.Account_Request_Retrieve.response.Data.Status == "AwaitingAuthorisation"
    And match apiApp.Account_Request_Retrieve.response.Data.CreationDateTime == apiApp.Account_Request_Retrieve.response.Data.StatusUpdateDateTime

  @authorized
  Scenario: Verify that StatusUpdateDateTime is updated correctly as per consent approved time when the status is Authorized

    * set info.Screens = "Yes"
    * set info.subset = "Authorised"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)
    * set Application_Details.TestCasePath = path

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    Then match apiApp.E2EConsent_GenerateToken.Application_Details.access_token != null

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.AccountRequestId = apiApp.E2EConsent_GenerateToken.Application_Details.AccountRequestId
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Data.Status == "Authorised"
    And match apiApp.Account_Request_Retrieve.response.Data contains {StatusUpdateDateTime : '#present'}
    * def StatusUpdateDateTime_created = apiApp.E2EConsent_GenerateToken.apiApp.Account_Request_Setup.response.Data.StatusUpdateDateTime
    * def StatusUpdateDateTime_authorised = apiApp.Account_Request_Retrieve.response.Data.StatusUpdateDateTime
    * def comapareDates = Java.type('CMA_Release.Java_Lib.Date_Format').compareTwoDates(StatusUpdateDateTime_created,StatusUpdateDateTime_authorised)
    * print comapareDates
    And assert comapareDates.equals("Before")
    * call webApp.stop()

  @Revoked
  Scenario: Verify that StatusUpdateDateTime is updated correctly as per consent revoked (using DEL account request) time  when the status is Revoked

    * set info.Screens = "Yes"
    * set info.subset = "Revoked"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)
    * set Application_Details.TestCasePath = path

    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    Then match apiApp.E2EConsent_GenerateToken.Application_Details.access_token != null

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.request_method = 'DELETE'
    And set Application_Details.AccountRequestId = apiApp.E2EConsent_GenerateToken.Application_Details.AccountRequestId
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Delete(Application_Details)
    Then match apiApp.Account_Request_Delete.responseStatus == 204

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.AccountRequestId = apiApp.E2EConsent_GenerateToken.Application_Details.AccountRequestId
    And set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200
    And match apiApp.Account_Request_Retrieve.response.Data.Status == "Revoked"
    * def StatusUpdateDateTime_created = apiApp.E2EConsent_GenerateToken.apiApp.Account_Request_Setup.response.Data.StatusUpdateDateTime
    * def StatusUpdateDateTime_authorised = apiApp.Account_Request_Retrieve.response.Data.StatusUpdateDateTime
    * def comapareDates = Java.type('CMA_Release.Java_Lib.Date_Format').compareTwoDates(StatusUpdateDateTime_created,StatusUpdateDateTime_authorised)
    And assert comapareDates.equals("Before")
    * call webApp.stop()

  Scenario: Verify that StatusUpdateDateTime  is updated correctly as per consent rejected time when the status is Rejected

    * set info.Screens = "Yes"
    * set info.subset = "Rejected"
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path =  info1.path
    * def path = call  webApp.path1 = path

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

  ##CCToken
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

  ##PostAccSetup

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = 'Bearer'
    And set Application_Details.request_method = 'POST'
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

  ##RequestObject Creation
    Given set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    And set Result.ActualOutput.JWT.Input.SigningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    When call apiApp.CreateRequestObject(Application_Details)
    Then assert apiApp.CreateRequestObject.jwt.request != null

  ##ConsentUrlCreation
    Given set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * print consenturl

    Given set Application_Details $.consenturl = consenturl
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'

    When def perform = Functions.useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(B365_User)',"otp":'#(B365_OTP)',"action":"continue","Confirmation":"Yes"}
    When def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'

    Given def click = "Click on Cancel"
    When def perform = Reuseable.ClickElement(webApp,PageObject_AccSel.cancel_button)
    And def performCancel = Reuseable.ClickElement(webApp,PageObject_AccSel.cancel_request)
    Then def SystemTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').CurrentTimeStamp()

    Given set Application_Details.request_method = 'GET'
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.responseStatus == 200

    * def StatusUpdateDateTime_rejected = apiApp.Account_Request_Retrieve.response.Data.StatusUpdateDateTime
    * def comapareDates = Java.type('CMA_Release.Java_Lib.Date_Format').compareTwoDates(SystemTime,StatusUpdateDateTime_rejected)
    And assert comapareDates.equals("Before")
    * call webApp.stop()