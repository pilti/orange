@AISP_GetAcReq
@Regression
Feature: API - GET account request - New CAM2.0 End point check

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @Val_Endpoint_GetAcc
    @test01234
  Scenario: Verify that new CMA 2.0 end point is available and working for GET account request (not 404 or 504  timeout)

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.scope = 'openid accounts'
    And  call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then assert apiApp.Account_Request_Retrieve.responseStatus != 404 && apiApp.Account_Request_Retrieve.responseStatus != 504


  Scenario: Verify that new endpoint is correctly updated in Self field of Link section of response

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Links.Self == AcctReqUrl + "/" + Application_Details.AccountRequestId


  Scenario: Verify that Self field of Link section of response as AccountRequestId as per Data section

    Given json Application_Details = active_tpp.AISP_PISP
    And  call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set Application_Details.request_method = 'GET'
    When call apiApp.Account_Request_Retrieve(Application_Details)
    Then match apiApp.Account_Request_Retrieve.response.Links.Self contains apiApp.Account_Request_Retrieve.response.Data.AccountRequestId
