@AISP_SingleAccInfo
@Regression
Feature: API - SingleAccRequest - New CAM2.0 End point check

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }
@1

Scenario: Verify that new CMA 2.0 end point is available and working for SingleAcc Req (not 404 or 504 timeout)

    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

   When call apiApp.Account_Information(Application_Details)
    Then assert apiApp.Account_Information.responseStatus != 404 || apiApp.Account_Information.responseStatus != 504
   And match apiApp.Account_Information.responseStatus == 200
   And match each apiApp.Account_Information.response.Data.Account[*] contains {AccountId: '#notnull'}


  Scenario: Verify that new endpoint is correctly updated in Self field of Link section of response

    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And def urlToValidate = (CMA_Rel_Ver == '2.0' ? AcctInfoUrl + Application_Details.AccountId : "/accounts/" + Application_Details.AccountId)
    And match apiApp.Account_Information.response.Links.Self == urlToValidate


  @n22334
  Scenario: Verify that Self field of Link section of response as account request id as per Data section
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And def LinkStructure = apiApp.Account_Information.response.Links.Self
    And match LinkStructure contains apiApp.Account_Information.response.Data.Account[0].AccountId