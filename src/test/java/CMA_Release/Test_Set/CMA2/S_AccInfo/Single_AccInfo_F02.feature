@AISP_SingleAccInfo
@Regression
Feature: API - Single account request response json Structure validation for CMA2.0

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }


   Scenario: Verify that overall  response json Structure has Data, links and Meta sections as per CMA 2.0 Specifications
      * def fileName = 'Generic_G_User_1.properties'
      * def name = 'Consent'
      * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
      * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

      Given def Application_Details = active_tpp.AISP_PISP
      When call apiApp.configureSSL(Application_Details)
      And set Application_Details $.grant_type = "refresh_token"
      And set Application_Details $.request_method = 'POST'
      And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
      And set Application_Details $.refresh_token = profile.refresh_token
      #Call RTG API to generate access token
      And call apiApp.Access_Token_RTG(Application_Details)
      Then match apiApp.Access_Token_RTG.responseStatus == 200

      #Set input for Multi Account API
      Given set Application_Details $.request_method = "GET"
      And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
      And set Application_Details $.token_type = 'Bearer'
      When call apiApp.Multi_Account_Information(Application_Details)
      Then match apiApp.Multi_Account_Information.responseStatus == 200

      Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
      And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
      When call apiApp.Account_Information(Application_Details)
      Then match apiApp.Account_Information.responseStatus == 200
      And match apiApp.Account_Information.response == SchemaSingAccInfo.success


   Scenario: Verify that Data section of response json has correct structure as per CMA 2.0 Specifications
     * def fileName = 'Generic_G_User_1.properties'
     * def name = 'Consent'
     * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
     * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     Given def Application_Details = active_tpp.AISP_PISP
     When call apiApp.configureSSL(Application_Details)
     And set Application_Details $.grant_type = "refresh_token"
     And set Application_Details $.request_method = 'POST'
     And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
     And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
     And call apiApp.Access_Token_RTG(Application_Details)
     Then match apiApp.Access_Token_RTG.responseStatus == 200

     #Set input for Multi Account API
     Given set Application_Details $.request_method = "GET"
     And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
     And set Application_Details $.token_type = 'Bearer'
     When call apiApp.Multi_Account_Information(Application_Details)
     Then match apiApp.Multi_Account_Information.responseStatus == 200
     And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
     And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

     When call apiApp.Account_Information(Application_Details)
     Then match apiApp.Account_Information.responseStatus == 200
     And match apiApp.Account_Information.response.Data == {"Account": '#[]#notnull'}
     And match apiApp.Account_Information.response.Data == SchemaSingAccInfo.success.Data

  Scenario: Verify that Link section of response json has correct structure as per CMA 2.0 Specifications
    * def fileName = 'Generic_G_User_1.properties'
     * def name = 'Consent'
     * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
     * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     Given def Application_Details = active_tpp.AISP_PISP
     Then call apiApp.configureSSL(Application_Details)

     Given set Application_Details $.grant_type = "refresh_token"
     And set Application_Details $.request_method = 'POST'
     And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
     And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
     When call apiApp.Access_Token_RTG(Application_Details)
     Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
     Given set Application_Details $.request_method = "GET"
     And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
     And set Application_Details $.token_type = 'Bearer'
     When call apiApp.Multi_Account_Information(Application_Details)
     Then match apiApp.Multi_Account_Information.responseStatus == 200
     And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
     And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId

    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And match apiApp.Account_Information.response.Links == SchemaSingAccInfo.success.Links
    And def urlToValidate = (CMA_Rel_Ver == '2.0' ? AcctInfoUrl + Application_Details.AccountId : "/accounts/" + Application_Details.AccountId)
    And match apiApp.Account_Information.response.Links.Self == urlToValidate

  Scenario: Verify that Meta section of response json has correct structure as per CMA 2.0 Specifications
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + profile.AccountRequestId

    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And match apiApp.Account_Information.response.Meta == SchemaSingAccInfo.success.Meta