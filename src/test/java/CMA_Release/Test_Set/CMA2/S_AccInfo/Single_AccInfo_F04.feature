@AISP_SingleAccInfo
Feature: API - Single account request New filed validation

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def info = karate.info
    * def CPdb = Java.type('CMA_Release.Entities_DB.CHANNEL_PROFILE_DB')
    * def SPShareddb = Java.type('CMA_Release.Entities_DB.SP_SHARED_DATA_DB')
    * def ABTdb = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def FieldValueValidations = Java.type('CMA_Release.Java_Lib.AISP_Scenarios.FieldValueValidations')
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @AISP_SingleAccInfo
  Scenario: Verify that new field AccountType updated correctly as Personal account or Business Account
  #Also check if FS and DB values are matching
  #Logic : If the market classification is between 960-972 inclusive, then the account is a personal account otherwise Business accouont

    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200

    * def arr_acc = get apiApp.Account_Information.response.Data.Account[0].Account[0].Identification
    * def arr_accType = get apiApp.Account_Information.response.Data.Account[0].AccountType

    # get market classification of account from ABT database
    * eval var recordsABT = []
    * eval recordsABT.push(ABTdb.get_ABT_Details_ColumnValue(arr_acc.substring(0,6), arr_acc.substring(6,14),"MARKET_CLASSIFICATION", ABT_DB_Conn).replace("\\","").replace("[","").replace("]",""))
    * def funct = FieldValueValidations.convertDBColumnValToArray(recordsABT)
    * set Result.DatabaseResponse = recordsABT

    # assert account type with market classification value in ABT Db
    * eval var arrayOfaccType = []
    * eval arrayOfaccType.push(arr_accType)
    * def temp = FieldValueValidations.CheckMrktClassification(funct,arrayOfaccType)

    #Hit Fs services
    * set fs_data.profileId = profile.usr
    * set fs_data.X-BOI-USER = profile.usr
    * set fs_data.accountNumber = arr_acc.substring(6,14)
    * set fs_data.nsc = arr_acc.substring(0,6)
    * json FS_Accounts = call read('classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfileSingle.feature')
    * def FS_AccountsRes = FS_Accounts.response
    * set Result.FS_Response = FS_AccountsRes

   #filter FS response for getting eligible accounts seen on Consent Page
    * def accdetails = FS_AccountsRes.accounts._.account
    * eval var FS_accdetailsFilter = []
    * eval var FS_accTypeFilter = []
    * eval if(accdetails.accountPermission != 'JX' && accdetails.accountSubType != 'Savings Account') {FS_accdetailsFilter.push(accdetails.accountNSC + accdetails.accountNumber);FS_accTypeFilter.push(accdetails.accountType.substring(0,8))}

     #set account types obtain from API,DB and FS in Result
    * set Result.API_AccountType = arr_accType
    * set Result.FS_AccountType = FS_accTypeFilter
    * set Result.DB_AccountType = funct

   # Match the APi Response with FS response
    * eval var arrayOf_arr_acc = []
    * eval var arrayOf_arr_accType = []

    * eval arrayOf_arr_acc.push(arr_acc)
    * eval arrayOf_arr_accType.push(arr_accType)

    * def arrayOf_arr_acc = arrayOf_arr_acc
    * def FS_accdetailsFilter = FS_accdetailsFilter
    * match arrayOf_arr_acc == FS_accdetailsFilter

    * def arrayOf_arr_accType = arrayOf_arr_accType
    * def FS_accTypeFilter = FS_accTypeFilter
    * match arrayOf_arr_accType == FS_accTypeFilter

  @AISP_SingleAccInfo
  Scenario: Verify that new fields AccountSubType updated correctly SavingsAccount for Savings accounts
  # Also check if FS and DB values matching with API responses
    #Logic : AcoountType value 01 for Savings in customer profile database
    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200

    * def arr_acc = get apiApp.Account_Information.response.Data.Account[0].Account[0].Identification
    * def arr_accSubType = get apiApp.Account_Information.response.Data.Account[0].AccountSubType

    # get all accounts of user from channel profile
    * eval var accSubTypeDB = []
    * eval accSubTypeDB.push(CPdb.get_Channel_Profile_ColumnVals("WHERE CHANNEL_ID = "+ profile.usr + " AND ACCOUNT_NUMBER ="+ arr_acc.substring(6,14),"ACCOUNT_TYPE",CHANNEL_PROFILE_DB_Conn).replace("[","").replace("]",""))
    * set Result.DBResponse = accSubTypeDB

    # convert the DB result to array
    * def funct = FieldValueValidations.convertDBColumnValToArray(accSubTypeDB)

    # assert account type in APi response with ACCOUNT_TYPE value in Channel Db
    * eval var arrayOfaccSubType = []
    * eval arrayOfaccSubType.push(arr_accSubType)
    * def temp = FieldValueValidations.CheckAccountSubType(funct ,arrayOfaccSubType)

     #Hit Fs services
    * set fs_data.profileId = profile.usr
    * set fs_data.X-BOI-USER = profile.usr
    * set fs_data.accountNumber = arr_acc.substring(6,14)
    * set fs_data.nsc = arr_acc.substring(0,6)
    * json FS_Accounts = call read('classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfileSingle.feature')
    * def FS_AccountsRes = FS_Accounts.response
    * set Result.FS_Response = FS_AccountsRes

    #filter FS response
    * def accdetails = FS_AccountsRes.accounts._.account
    * eval var FS_accdetailsFilter = []
    * eval var FS_accSubTypeFilter = []
    * eval if( accdetails.accountPermission != 'JX' && accdetails.accountSubType != 'Savings Account') {FS_accdetailsFilter.push(accdetails.accountNSC + accdetails.accountNumber); FS_accSubTypeFilter.push(accdetails.accountSubType.replace(" ",""))}

    # Match the APi Response with FS response
    * eval var arrayOf_arr_acc = []
    * eval var arrayOf_arr_accSubType = []
    * eval arrayOf_arr_acc.push(arr_acc)
    * eval arrayOf_arr_accSubType.push(arr_accSubType)

    * set Result.API_Accounts = arrayOf_arr_acc
    * set Result.FS_Accounts = FS_accdetailsFilter

    * def arrayOf_arr_acc = arrayOf_arr_acc
    * def FS_accdetailsFilter = FS_accdetailsFilter
    * match arrayOf_arr_acc == FS_accdetailsFilter

    * set Result.API_AccountSubType = arrayOf_arr_accSubType
    * set Result.FS_AccountSubType = FS_accSubTypeFilter

    * def arrayOf_arr_accSubType = arrayOf_arr_accSubType
    * def FS_accSubTypeFilter = FS_accSubTypeFilter
    * match arrayOf_arr_accSubType == FS_accSubTypeFilter

  @Regression
  @AISP_SingleAccInfo
  Scenario: Verify if account has multiple accountID then each account is updated with new fields
    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

  #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And print apiApp.Account_Information.response
    And match apiApp.Account_Information.response.Data.Account[0] contains {AccountType: "#present"}
    And match apiApp.Account_Information.response.Data.Account[0] contains {AccountSubType: "#present"}

  @Regression
  Scenario: Verify that Account section of response has new fields AccountType and AccountSubType for both Sole and Joint accounts
   #Logic : Sole and Joint account have permissions 'A' and 'JA/JV' in FS response and retrieved in API response
    * def fileName = 'Generic_M_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
   #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

   #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And print apiApp.Account_Information.response

    * def arr_acc = get apiApp.Account_Information.response.Data.Account[0].Account[0].Identification
    * def arr_accType = get apiApp.Account_Information.response.Data.Account[0].AccountType
    * def arr_accSubType = get apiApp.Account_Information.response.Data.Account[0].AccountSubType

     #Hit Fs services
    * set fs_data.profileId = profile.usr
    * set fs_data.X-BOI-USER = profile.usr
    * set fs_data.accountNumber = arr_acc.substring(6,14)
    * set fs_data.nsc = arr_acc.substring(0,6)
    * json FS_Accounts = call read('classpath:CMA_Release/Entities_FS/AISP_FS/customerAccountProfileSingle.feature')
    * def FS_AccountsRes = FS_Accounts.response
    * set Result.FS_resposne = FS_AccountsRes

    # Filter FS response to get Sole and valid Joint accounts
    * def accdetails = get FS_AccountsRes.accounts._.account
    * print "-----" + accdetails
    * eval var FS_accdetailsFilter = []
    * eval var FS_accTypeFilter = []
    * eval var FS_accSubTypFilter = []
    * eval if((accdetails.accountPermission == 'A' || accdetails.accountPermission == 'JA' || accdetails.accountPermission == 'JV') && (accdetails.accountSubType != 'Savings Account')) { FS_accdetailsFilter.push(accdetails.accountNSC + accdetails.accountNumber) ;FS_accTypeFilter.push(accdetails.accountType.substring(0,8)) ;FS_accSubTypFilter.push(accdetails.accountSubType.replace(" ",""))}

    # Match the APi Response with FS response
    * eval var arrayOf_arr_acc = []
    * eval var arrayOf_arr_accType = []
    * eval var arrayOf_arr_accSubType = []

    * eval arrayOf_arr_acc.push(arr_acc)
    * eval arrayOf_arr_accType.push(arr_accType)
    * eval arrayOf_arr_accSubType.push(arr_accSubType)

    * def arrayOf_arr_acc = arrayOf_arr_acc
    * def FS_accdetailsFilter = FS_accdetailsFilter
    * print arrayOf_arr_acc
    * print FS_accdetailsFilter
    * match arrayOf_arr_acc == FS_accdetailsFilter

    * def arrayOf_arr_accType = arrayOf_arr_accType
    * def FS_accTypeFilter = FS_accTypeFilter
    * match arrayOf_arr_accType == FS_accTypeFilter

    * def arrayOf_arr_accSubType = arrayOf_arr_accSubType
    * def FS_accSubTypFilter = FS_accSubTypFilter
    * match arrayOf_arr_accSubType == FS_accSubTypFilter

  @AISP_SingleAccInfo
  @Regression
  Scenario: Verify the impresence of new optional field Description in Singles Acc info API

    * def info = karate.info
    * set info.subset = 'Optional Field validation'
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
 #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

 #Set input for Multi Account API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId
    When call apiApp.Account_Information(Application_Details)
    Then match apiApp.Account_Information.responseStatus == 200
    And print apiApp.Account_Information.response
    And match apiApp.Account_Information.response.Data.Account[0] contains {Description: "##string"}




