@AISP_SingleAccBen
@beneficiaries_rup_01_newR
@Regression
Feature:Verify that the response of AccBen is updated with new optional field and its sub field

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    #To read Test Data#
    * def fileName = 'Generic_B_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
    #Configure Network Certificate#
    * call apiApp.configureSSL(profile)
   #To write test result and error details to output file#
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  @AISP_SingleAccBen_Rup011
  @Anu8525461
  Scenario: To verify the response structure of CreditorAgent in Get Single Account beneficiaries API is as per CMA specification
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And def beneficiariesStructure =
    """

   {
    "AccountId": "#string",
    "BeneficiaryId": "#string",
    "Reference": "##string",
    "CreditorAgent":"##object",
     "CreditorAccount":"##object"
   }

   """

    And def creditorAgent =

   """

     {
        "SchemeName": "#regex^(SortCodeAccountNumber|IBAN)$",
        "Identification": "#string",
     }

   """
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] == beneficiariesStructure
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*].CreditorAgent == creditorAgent


  Scenario: To Verify that CreditorAgent optional field  should display in response of AccBen API
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*].CreditorAgent == '##object'


  @sce2_val_ben_2011
  Scenario: To Verify that CreditorAgent optional field with its sub fields should display in response of AccBen API
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    Given def CredAgentJSON =
    """
    {
    CreditorAgent:
         {
            SchemeName:'#string',
            Identification:'#string'
         }
    }

    """

    Then match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*].CreditorAgent == CredAgentJSON
#    And match apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] contains CreditorAgent
#    And match apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] == CredAgentJSON

  @sce4_val_ben
  Scenario: To verify the response without CreditorAgent in Get Single Account beneficiaries API is as per CMA specification
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

   #Get AccounID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

   #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] contains {"CreditorAgent": '#notpresent'}


