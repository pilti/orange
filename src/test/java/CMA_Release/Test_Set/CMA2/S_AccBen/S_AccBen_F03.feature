@AISP_SingleAccBen
@Regression
Feature:  - API - Beneficiaries response json Structure validation for CMA2.0

  Background:
    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
  #To read Test Data#
    * def fileName = 'Generic_B_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)
  #Configure Network Certificate#
    * call apiApp.configureSSL(profile)
 #To write test result and error details to output file#
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
  @Val_Whole_response
  Scenario Outline:Verify that overall  response json Stacture has Data,links and Meta sections as per CMA 2.0 Specifications

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    #And def responseStructure =
#    """
#     {
#      "Data": {
#       "Beneficiary": '#[]'
#    },
#      "Links": {
#      "Self": "#string"
#     },
#      "Meta": {
#    "TotalPages": "#number"
#    }
#   }
    # """
    #And match apiApp.Customer_Beneficiaries.response contains responseStructure
    #And match each apiApp.Customer_Beneficiaries.response contains SchemaBeneficiary.Structure\

    * print "ddd" + SchemaBeneficiary.success.Data
    And match each apiApp.Customer_Beneficiaries.response.Data.Beneficiary[*] == SchemaBeneficiary.success.Data.Beneficiary[0]

    Examples:
      | Role      |
      | AISP_PISP |

  @Val_data_resp_datarup
  Scenario Outline: Verify that Data section of  response json has correct structure as per CMA 2.0 Specifications

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
#    And def datastructure =
#
#    """
#    {
#      "Beneficiary": '#[]'
#    }
#    """
   # Then match apiApp.Customer_Beneficiaries.response.Data contains datastructure
    And match apiApp.Customer_Beneficiaries.response.Data == SchemaBeneficiary.Structure.Data
    Examples:
      | Role      |
      | AISP_PISP |


  @Val_Link_self_apiresp
  Scenario Outline:Verify that link section of  response json has correct structure as per CMA 2.0 Specifications
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
#    And def LinkStructure =
#
#     """
#      {
#
#    "Self": "#string"
#
#    }
#     """
#    Then match apiApp.Customer_Beneficiaries.response.Links == LinkStructure
    Then match apiApp.Customer_Beneficiaries.response.Links contains SchemaBeneficiary.success.Links
    Examples:
      | Role      |
      | AISP_PISP |

  @Val_Meta_apiresp
  Scenario Outline:Verify that Meta section of  response json has correct structure as per CMA 2.0 Specifications
    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
#    And def MetaStructure =
#
#     """
#     {
#     "TotalPages": "#number"
#
#     }
#     """
#    Then match apiApp.Customer_Beneficiaries.response.Meta == MetaStructure
    Then match apiApp.Customer_Beneficiaries.response.Meta contains SchemaBeneficiary.success.Meta
    Examples:
      | Role      |
      | AISP_PISP |

  @abc_array
  Scenario: Verify that Data section has beneficiary as a Array list

    Given def inputJSON =
    """
    { client_id: '#(profile.client_id)',
      client_secret: '#(profile.client_secret)',
      request_method: "POST",
      refresh_token: '#(profile.refresh_token)'
    }
    """
    And set inputJSON $.grant_type = "refresh_token"
    And set inputJSON $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_RTG(inputJSON)
    Then match apiApp.Access_Token_RTG.responseStatus == 200

    #Get AccountID from Multi Account Info API
    Given set inputJSON $.request_method = "GET"
    And set inputJSON $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set inputJSON $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(inputJSON)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set inputJSON $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Customer_Beneficiaries(inputJSON)
    Then match apiApp.Customer_Beneficiaries.responseStatus == 200
    And match apiApp.Customer_Beneficiaries.response.Data contains SchemaBeneficiary.Structure.Data


