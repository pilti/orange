@AISP_MultiAccInfo
@N
@Regression
Feature: API - Multi account request - CAM1.1 End point backward compatibility

Background:
  * def apiApp = new apiapp()
  * def webApp = new webapp()
  * json Result = {}
  * set Result.Testname = null
  * def info = karate.info
  * configure afterScenario = function(){ karate.call(afterScenario) }


 Scenario: Verify that the new field AccountSubType is not present in CAM1.1 response
   * def fileName = 'Generic_G_User_1.properties'
   * def name = 'Consent'
   * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
   * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

   Given def Application_Details = active_tpp.AISP_PISP
   Then call apiApp.configureSSL(Application_Details)

   Given set Application_Details $.grant_type = "refresh_token"
   And set Application_Details $.request_method = 'POST'
   And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
   And set Application_Details $.refresh_token = profile.refresh_token
  #Call RTG API to generate access token
   When call apiApp.Access_Token_RTG(Application_Details)
   Then match apiApp.Access_Token_RTG.responseStatus == 200
   And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token

   Given def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    #Set input for Multi Account API
   And set Application_Details $.request_method = "GET"
   And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
   And set Application_Details $.token_type = 'Bearer'
   When call apiApp.Multi_Account_Information(Application_Details)
   Then match apiApp.Multi_Account_Information.responseStatus == 200
   And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountSubType: '#notpresent'}
   And match apiApp.Multi_Account_Information.response.Data.Account[0] contains {AccountType: '#notpresent'}

  @1234
  Scenario: Verify that Self field of Link section of response is not affected and has cma1.1 link
     * def fileName = 'Generic_G_User_1.properties'
     * def name = 'Consent'
     * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
     * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

     Given def Application_Details = active_tpp.AISP_PISP
     Then call apiApp.configureSSL(Application_Details)

     Given set Application_Details $.grant_type = "refresh_token"
     And set Application_Details $.request_method = 'POST'
     And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
     And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
     When call apiApp.Access_Token_RTG(Application_Details)
     Then match apiApp.Access_Token_RTG.responseStatus == 200
     And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token

     Given def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    #Set input for Multi Account API
     And set Application_Details $.request_method = "GET"
     And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
     And set Application_Details $.token_type = 'Bearer'
     When call apiApp.Multi_Account_Information(Application_Details)
     Then match apiApp.Multi_Account_Information.responseStatus == 200
     And match apiApp.Multi_Account_Information.response.Links.Self == "/accounts"

  Scenario: Verify that Self field of Link section of response does not have accountID for multi account request
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    #Call RTG API to generate access token
    When call apiApp.Access_Token_RTG(Application_Details)
    Then match apiApp.Access_Token_RTG.responseStatus == 200
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token

    Given def AcctInfoUrl = AcctInfoUrl.replace('2.0','1.1')
    #Set input for Multi Account API
    And set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And def linkText = apiApp.Multi_Account_Information.response.Links.Self
    And match linkText !contains profile.AccountRequestId
