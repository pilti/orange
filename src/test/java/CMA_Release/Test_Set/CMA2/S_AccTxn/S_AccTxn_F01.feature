@AISP_Acc_Trxn
@Regression
Feature: This feature is to validate response structure of Account Direct Debits API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify the response structure of Account Direct Debits in CMA 2.0

    * def info = karate.info
    * set info.Screens = 'Yes'
    * def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)
    * def path = call  webApp.path1 = info1.path

    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.usr = user_details.Generic.G_User_2.user
    And set Application_Details.TestCasePath = info1.path
    And def callE2E = call read('classpath:CMA_Release/Entities_API/AISP_Resources/OIDC/E2EConsent_GenerateToken.feature')
    And set Result.UI_Output = callE2E.UIResult
    And set Application_Details.access_token =  callE2E.Result.Access_Token_ACG.response.access_token
    And set Application_Details.request_method = 'GET'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200

    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    When call apiApp.Account_Transaction(Application_Details)
    Then match apiApp.Account_Transaction.responseStatus == 200
    And match each apiApp.Account_Transaction.response.Data.Transaction[*] == SchemaTransactions.success.Data.Transaction
    And match apiApp.Account_Transaction.response.Links == SchemaTransactions.success.Links
    And match apiApp.Account_Transaction.response.Meta == SchemaTransactions.success.Meta
    And match apiApp.Account_Transaction.response.Links.Self == AcctInfoUrl + Application_Details.AccountId + '/transactions'