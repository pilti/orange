@AISP_Acc_Balance
@Regression
Feature: This feature is to validate response structure of Account Balance API

  Background:
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def info = karate.info
    * set Result.Testname = null
    * configure afterScenario =
      """
      function(){
       var tem = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(tem.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = tem.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario: Verify the response structure of Account Balance in CMA 2.0
    * def fileName = 'Generic_G_User_1.properties'
    * def name = 'Consent'
    * def Datapath = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv+'/AISP/General/TestData_Output/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    Given def Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.grant_type = "refresh_token"
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    And set Application_Details $.refresh_token = profile.refresh_token
    When call apiApp.Access_Token_RTG(Application_Details)
    #And set Result.Access_Token_RTG.Input = inputJSON

    Then set profile $.access_token = apiApp.Access_Token_RTG.response.access_token
    And match apiApp.Access_Token_RTG.responseStatus == 200
    And remove Application_Details $.refresh_token

    ##Get AccounID from Multi Account Info API
    Given set Application_Details $.request_method = "GET"
    And set Application_Details $.access_token = apiApp.Access_Token_RTG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    When call apiApp.Multi_Account_Information(Application_Details)
    Then set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    #And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    And match apiApp.Multi_Account_Information.responseStatus == 200

    #Call Single account Beneficiaries API with the AccountID
    Given set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
      #Set input for Balance API
    And set Application_Details $.URL = AcctInfoUrl + Application_Details.AccountId + '/balances'
     #Call Single Account Balance API
    When call apiApp.Account_Balance(Application_Details)
    Then match apiApp.Account_Balance.responseStatus == 200
    And match apiApp.Account_Balance.response.Data.Balance[0] contains SchemaBalance.success.Data.Balance[0]
    And match apiApp.Account_Balance.response.Links.Self == AcctInfoUrl +  Application_Details.AccountId + '/balances'