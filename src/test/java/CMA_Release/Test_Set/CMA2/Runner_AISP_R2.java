package CMA_Release.Test_Set.CMA2;

import Runners.Test_Runner_Parallel;
import cucumber.api.CucumberOptions;

@CucumberOptions(tags = {"@AISP_GetAcReq,@AISP_MultiAccInfo,@AISP_Acc_Balance,@AISP_SingleAccBen,@AISP_Acc_DD,@AISP_SingleAccInfo,@AISP_SingleAccProd,@AISP_StandingOrder,@AISP_Acc_Trxn,@AISP_SetAcReq"})
public class Runner_AISP_R2 extends Test_Runner_Parallel {

}
