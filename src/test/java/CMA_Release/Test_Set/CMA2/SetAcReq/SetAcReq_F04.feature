
@AISP_SetAcReq
@AISP_Release2
@Regression
Feature: API - POST account request New field validation

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario: Verify that respective status is updated with  statusdatetime field with systemDatetime

    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains {"StatusUpdateDateTime" : '#string' }
    * def date_time = apiApp.Account_Request_Setup.response.Data.StatusUpdateDateTime
    * def val = date_time.substring(0,16)
    * print val
    * def sysval = Java.type('CMA_Release.Java_Lib.GetCurrentTime').CurrSysTime()
    * print sysval
    And match val == sysval

  Scenario: Verify that  respective status is updated with  statusdatetime field  in getAPI response

    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains {"StatusUpdateDateTime" : '#string' }

  @pqr
  Scenario: Verify that respective status is updated with  statusUpdatedatetime field and is same as CreationDateTime

    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains {"StatusUpdateDateTime" : '#string' }
    And match apiApp.Account_Request_Setup.response.Data.StatusUpdateDateTime == apiApp.Account_Request_Setup.response.Data.CreationDateTime
