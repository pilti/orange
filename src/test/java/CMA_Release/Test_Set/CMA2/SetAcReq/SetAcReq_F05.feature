
@AISP_SetAcReq
@AISP_Release2
@Regression
Feature: API - POST account request -  CAM1.1 End point backward compability

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @Tester01
  Scenario: Verify that the new field StatusUpdateDateTime is not present in CAM1.1 response

    * def info = karate.info

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And  def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains {StatusUpdateDateTime: #notpresent}


  Scenario Outline: Verify that account request id not created when new permission is provided
    * def key = <Condition>

    * set info.key = key
    * set info.subset = key

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

      #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set permissions $.Data.Permissions = <permissions>
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400

    Examples:
      | Condition            | permissions                                                                                              |
      | 'All_NewPermissions' | ["ReadOffers","ReadPAN","ReadParty","ReadPartyPSU","ReadScheduledPaymentsDetail","ReadStatementsDetail"] |

  @abc123_xyz
  Scenario: Verify that Self field of Link section of response is not affected and has cma1.1 link
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

      #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Links.Self == "/account-requests/" + apiApp.Account_Request_Setup.response.Data.AccountRequestId

  Scenario: Verify that Self field of Link section of response as account request id as per Data section
    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)
          #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given def AcctReqUrl = AcctReqUrl.replace('2.0','1.1')
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Links.Self contains apiApp.Account_Request_Setup.response.Data.AccountRequestId
