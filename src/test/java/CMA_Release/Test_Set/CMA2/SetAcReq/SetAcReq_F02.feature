@AISP_SetAcReq
@AISP_Release2
@Regression
Feature: API - POST account request response json Structure validation for CMA2.0

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  @Tester012_hello
  Scenario: Verify that overall response json Structure has Data, risk, links and Meta sections as per CMA 2_0 Specifications


    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    And call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    And match apiApp.Account_Request_Setup.response contains SchemaAccountRequest.success


  Scenario: Verify that Data section of  response json has correct structure as per CMA 2_0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains SchemaAccountRequest.success.Data

  Scenario: Verify that Risk section of  response json has correct structure as per CMA 2_0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response contains SchemaAccountRequest.success.Risk

  Scenario: Verify that Links section of  response json has correct structure as per CMA 2_0 Specification

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Links contains SchemaAccountRequest.success.Links

  Scenario:  Verify that Meta section of  response json has correct structure as per CMA 2_0 Specifications

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Meta contains SchemaAccountRequest.success.Meta

  Scenario: Verify that StatusUpdateDateTime is present

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Data contains {"StatusUpdateDateTime": '#present'}