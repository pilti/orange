@AISP_SetAcReq
@AISP_Release2
@Regression
Feature: API - POST account request Permission validation

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario Outline: Verify that the CMA2.0 permission [<Condition>] is not accepted (as it is not implemeted) as per CMA 2.0 specification
#    * def key = <Condition>
#    * def info = karate.info
#    * set info.key = key
#    * set info.subset = key

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

    #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400

    Examples:
      | Condition                     | permissions                                                                                              |
      | 'All_CMA2.0'                     | ["ReadOffers","ReadPAN","ReadParty","ReadPartyPSU","ReadScheduledPaymentsDetail","ReadStatementsDetail"] |
      | 'ReadOffers'                  | ["ReadOffers"]                                                                                           |
      | 'ReadPAN'                     | ["ReadPAN"]                                                                                              |
      | 'ReadParty'                   | ["ReadParty"]                                                                                            |
      | 'ReadPartyPSU'                | ["ReadPartyPSU"]                                                                                         |
      | 'ReadScheduledPaymentsDetail' | ["ReadScheduledPaymentsDetail"]                                                                          |
      | 'ReadStatementsDetail'        | ["ReadAccountsBasic","ReadStatementsDetail"]                                                             |


  Scenario Outline: Verify that combination of CMA1.1 permissions with CMA2.0 permissions [<Condition>] are not accepted and reflected in permission section of the response as per CMA 2.0 specification

#    * def key = <Condition>
#    * def info = karate.info
#    * set info.key = key
#    * set info.subset = key

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

   #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 400

    Examples:
      | Condition                     | permissions                                                                                                                                                                                                                                                                                                                                                                                                    |
      | 'All'                         | ["ReadAccountsBasic","ReadAccountsDetail","ReadBalances","ReadBeneficiariesBasic","ReadBeneficiariesDetail","ReadDirectDebits","ReadProducts","ReadStandingOrdersBasic","ReadStandingOrdersDetail","ReadTransactionsBasic","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail","ReadOffers","ReadPAN","ReadParty","ReadPartyPSU","ReadScheduledPaymentsDetail","ReadStatementsDetail"] |
      | 'ReadOffers'                  | ["ReadAccountsBasic","ReadOffers"]                                                                                                                                                                                                                                                                                                                                                                             |
      | 'ReadPAN'                     | ["ReadAccountsBasic","ReadPAN"]                                                                                                                                                                                                                                                                                                                                                                                |
      | 'ReadParty'                   | ["ReadAccountsBasic","ReadParty"]                                                                                                                                                                                                                                                                                                                                                                              |
      | 'ReadParty_ReadPartyPSU'      | ["ReadAccountsBasic","ReadParty","ReadPartyPSU"]                                                                                                                                                                                                                                                                                                                                                               |
      | 'ReadPartyPSU'                | ["ReadAccountsBasic","ReadPartyPSU"]                                                                                                                                                                                                                                                                                                                                                                           |
      | 'ReadScheduledPaymentsDetail' | ["ReadAccountsBasic","ReadScheduledPaymentsDetail"]                                                                                                                                                                                                                                                                                                                                                            |
      | 'ReadScheduledPaymentsDetail' | ["ReadAccountsBasic","ReadStatementsDetail","ReadScheduledPaymentsDetail"]                                                                                                                                                                                                                                                                                                                                     |
      | 'ReadStatementsDetail'        | ["ReadAccountsBasic","ReadStatementsDetail"]                                                                                                                                                                                                                                                                                                                                                                   |


  Scenario Outline: Verify that CMA1.1 permissions [<Condition>] are accepted and reflected  in permission section of the response as per CMA 2.0 specification

#    * def key = <Condition>
#    * def info = karate.info
#    * set info.key = key
#    * set info.subset = key

    Given json Application_Details = active_tpp.AISP_PISP
    Then call apiApp.configureSSL(Application_Details)

   #CC_Token
    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.scope = 'openid accounts'
    And set permissions $.Data.Permissions = <permissions>
    And set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201


    Examples:
      | Condition                                       | permissions                                                                                                                                                                                                                                                                                             |
      | 'All_CMA1.1'                                       | ["ReadAccountsBasic","ReadAccountsDetail","ReadBalances","ReadBeneficiariesBasic","ReadBeneficiariesDetail","ReadDirectDebits","ReadProducts","ReadStandingOrdersBasic","ReadStandingOrdersDetail","ReadTransactionsBasic","ReadTransactionsCredits","ReadTransactionsDebits","ReadTransactionsDetail"] |
      | 'ReadOffers_Permissions'                        | ["ReadAccountsBasic","ReadAccountsDetail"]                                                                                                                                                                                                                                                              |
      | 'ReadAccountsDetail'                            | ["ReadAccountsBasic","ReadBalances"]                                                                                                                                                                                                                                                                    |
      | 'ReadBeneficiariesBasic'                        | ["ReadAccountsBasic","ReadBeneficiariesBasic"]                                                                                                                                                                                                                                                          |
      | 'ReadBeneficiariesDetail'                       | ["ReadAccountsBasic","ReadBeneficiariesDetail"]                                                                                                                                                                                                                                                         |
      | 'ReadDirectDebits'                              | ["ReadAccountsBasic","ReadDirectDebits"]                                                                                                                                                                                                                                                                |
      | 'ReadStandingOrdersBasic'                       | ["ReadAccountsBasic","ReadStandingOrdersBasic"]                                                                                                                                                                                                                                                         |
      | 'ReadTransactionsBasic,ReadTransactionsDebits'  | ["ReadAccountsBasic","ReadTransactionsBasic","ReadTransactionsDebits"]                                                                                                                                                                                                                                  |
      | 'ReadTransactionsBasic,ReadTransactionsCredits' | ["ReadAccountsBasic","ReadTransactionsBasic","ReadTransactionsCredits"]                                                                                                                                                                                                                                 |

