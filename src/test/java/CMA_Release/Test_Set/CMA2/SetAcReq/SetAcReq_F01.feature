@AISP_SetAcReq
@AISP_Release2
@AISP_SetAcReq
@Regression
Feature: API - POST account request - New CAM2.0 End point check

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * def info = karate.info
    * configure afterScenario = function(){ karate.call(afterScenario) }

  Scenario: Verify that new CMA 2.0 end point is available and working for POST account request (not 404 or 504 timeout)

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then assert apiApp.Account_Request_Setup.responseStatus != 404 || apiApp.Account_Request_Setup.responseStatus != 504

  Scenario: Validate links section has url as per CMA 2.0

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    And call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Links.Self == AcctReqUrl + '/' + apiApp.Account_Request_Setup.response.Data.AccountRequestId

  Scenario: Verify that Self field of Link section of response has account request id as per Data section

    Given json Application_Details = active_tpp.AISP_PISP
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    Given set Application_Details $.access_token =  apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type =  "Bearer"
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And match apiApp.Account_Request_Setup.response.Links.Self contains apiApp.Account_Request_Setup.response.Data.AccountRequestId
