package CMA_Release.Test_Set.Cucumber_Selenium.Selenium;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Arrays;

public class Selenium_Test_BDD {
    static WebDriver driver;

    public static WebDriver InitiateBrowser() {

        System.setProperty("webdriver.chrome.driver", "C:\\Automation\\develop_3\\src\\Z_Drivers\\chromedriver2.41.exe");
        driver = new ChromeDriver();
        return driver;
    }

    @Given("^link is \"(.*)\"$")
    public static void Login(String url) throws InterruptedException {
        WebDriver driver;
        driver = InitiateBrowser();
        driver.get(url);
        Thread.sleep(1000);
        System.out.println(url);
    }

    @When("^user enters the text \"(.*)\"$")
    public static void wxecuteWhen(String str) throws InterruptedException {
        System.out.println(str);
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input")).sendKeys(str);

    }
    @And("clicks on \"(.*)\"$")
    public static void when_continued(String str) throws InterruptedException {
        if (str.equalsIgnoreCase("Google Search"))
            Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input")).submit();
    }
}

