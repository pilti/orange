package CMA_Release.Test_Set;

import CMA_Release.Entities_UI.General.GetApplication;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

import static CMA_Release.Entities_UI.Selenium.ReusableFunctions.WaitForElement;

public class gnib {
    public static void gnib_appointent() {
        System.setProperty("webdriver.chrome.driver", "C:\\Automation\\develop_3\\src\\Z_Drivers\\chromedriver2.41.exe");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(capabilities);

        //launch url
        driver.get("https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppSelect?OpenForm");

        //select work
        WaitForElement(driver,By.xpath("//*[@id=\"Category\"]"));
        Select category = new Select(driver.findElement(By.xpath("//*[@id=\"Category\"]")));
       // category.selectByVisibleText("Work");
        category.selectByIndex(3);


        //WaitForElement(driver,By.xpath("//*[@id=\"SubCategory\"]"));
        Select subcategory = new Select(driver.findElement(By.xpath("//*[@id=\"SubCategory\"]")));
        subcategory.selectByVisibleText("Work Permit Holder");

        //WaitForElement(driver,By.xpath("//*[@id=\"ConfirmGNIB\"]"));
        Select confirmGNIB = new Select(driver.findElement(By.xpath("//*[@id=\"ConfirmGNIB\"]")));
        confirmGNIB.selectByVisibleText("No");

        driver.findElement(By.xpath("//*[@id=\"UsrDeclaration\"]")).click();

        driver.findElement(By.xpath("//*[@id='GivenName']")).sendKeys("Ruchira Anil");
        driver.findElement(By.xpath("//*[@id=\"SurName\"]")).sendKeys("More");


        //WaitForElement(driver,By.xpath("//*[@id=\"Nationality\"]"));
        Select nationality = new Select(driver.findElement(By.xpath("//*[@id=\"Nationality\"]")));
        nationality.selectByVisibleText("India, Republic of");

        driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ruchiramore16@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"EmailConfirm\"]")).sendKeys("ruchiramore16@gmail.com");

        Select applicationtype = new Select(driver.findElement(By.xpath("//*[@id=\"FamAppYN\"]")));
        applicationtype.selectByVisibleText("No");

        Select traveldoc = new Select(driver.findElement(By.xpath("//*[@id=\"PPNoYN\"]")));
        traveldoc.selectByVisibleText("Yes");

        WaitForElement(driver,By.xpath("//*[@id=\"PPNo\"]"));
        driver.findElement(By.xpath("//*[@id=\"PPNo\"]")).sendKeys("M1464787");

        //Calender for DOB
        driver.findElement(By.xpath("//*[@id=\"DOB\"]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div[3]/table/thead/tr/th[1]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div[3]/table/thead/tr/th[1]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div[3]/table/tbody/tr/td/span[4]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div[2]/table/tbody/tr/td/span[4]")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/table/tbody/tr[3]/td[5]")).click();

        driver.findElement(By.xpath("//*[@id=\"btLook4App\"]")).click();
        WaitForElement(driver,By.xpath("//*[@id=\"AppSelectChoice\"]"));
        Select app = new Select(driver.findElement(By.xpath("//*[@id=\"AppSelectChoice\"]")));
        app.selectByIndex(2);

        WaitForElement(driver,By.xpath("//*[@id=\"btSrch4Apps\"]"));
        driver.findElement(By.xpath("//*[@id=\"btSrch4Apps\"]")).click();
    }
    public static void main(String[] args){
        gnib_appointent();
    }
}
