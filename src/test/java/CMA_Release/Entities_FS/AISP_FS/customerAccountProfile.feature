#This feature hits Customer profile service to fetch all accounts details
@FS_Shakedown
Feature: FS customer profile request
  HTTP Method : GET
  FS URI : /fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/v2/profile/{profileId}/accounts

  Input   :  Profile_Id,X-BOI-USER,X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID
  Output  : Response for Account Details

  Background:
  # Parameters
    * def data = fs_data
    * def profileId = data.profileId
    * json FS_Response =  {}
    * def headers_data = {X-BOI-USER: data.X-BOI-USER, X-BOI-CHANNEL: data.X-BOI-CHANNEL, X-BOI-PLATFORM: data.X-BOI-PLATFORM, X-CORRELATION-ID: data.X-CORRELATION-ID}


  Scenario: GET customer profile using FS service
    Given url fsCusProfileUrl + profileId +'/accounts'
    And headers headers_data
    When method GET
    Then status 200
    And json FS_Response = response.channelProfile._.accounts.account
    * print FS_Response
      ##################<End of Feature>#########################################################################
#    * set Result.FS_Accounts.URL = fsCusProfileUrl + profileId +'/accounts'
#    * set Result.FS_Accounts.Response = FS_Response