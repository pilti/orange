#This feature hits Account Transaction service
@FS_Shakedown
Feature: FS Account Transactions request - single account

  HTTP Method : GET
  FS URI : /fs-abt-service/services/abt/accounts/{nsc}/{accountNumber}/transactions

  Input   : X-BOI-USER,X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID ,nsc , accountNumber
  Output  : Response for Customer Transactions
  parameters: PageNumber , PageSize , Transaction Type


  Background:
  # Parameters
    * def data = fs_data
    * def nsc = data.nsc
    * def accountNumber = data.accountNumber
    * def headers_data = {X-BOI-USER: data.X-BOI-USER, X-BOI-CHANNEL: data.X-BOI-CHANNEL, X-BOI-PLATFORM: data.X-BOI-PLATFORM, X-CORRELATION-ID: data.X-CORRELATION-ID}

  Scenario: GET customer profile using FS service
    Given url fsAcctBalUrl + nsc + '/' + accountNumber+ '/transactions'
    And param startDate = data.startDate
    And param endDate = data.endDate
    And param pageNumber = data.pageNumber
    And param pageSize = data.pageSize
    And param txnType = data.txnType
    And headers headers_data
    When method GET
    Then status 200
