#This feature hits Customer profile service to fetch single account details
@FS_Shakedown_1

Feature: FS customer profile request - single account
  HTTP Method : GET
  FS URI : /fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/v2/account/{NSC}/{ACCOUNTNUMBER}

  Input   :  Profile_Id, X-BOI-USER, X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID, NSC , Account_Number
  Output  : Response for Single Account details

  Background:
  # Parameters
    * def data = fs_data
    * def nsc = data.nsc
    * def accountNumber = data.accountNumber
    * def headers_data = {X-BOI-USER: data.X-BOI-USER, X-BOI-CHANNEL: data.X-BOI-CHANNEL, X-BOI-PLATFORM: data.X-BOI-PLATFORM, X-CORRELATION-ID: data.X-CORRELATION-ID}

  Scenario: GET customer profile using FS service
    Given url fsCusProfileSingelUrl + 'account/' + nsc + '/' + accountNumber
    And headers headers_data
    When method GET
    Then status 200

