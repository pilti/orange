#This feature hits santanding orders foundation service
@FS_Shakedown
Feature: FS Standing order request - single account
  HTTP Method : GET
  FS URI : /fs-abt-service/services/account/{nsc}/{accountNumber}/standingorders

  Input   : X-BOI-USER,X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID ,nsc , accountNumber
  Output  : Response for customers Standing orders

  Background:
  # Parameters
    * def data = fs_data
    * def nsc = data.nsc
    * def accountNumber = data.accountNumber
    * def headers_data = {X-BOI-USER: data.X-BOI-USER, X-BOI-CHANNEL: data.X-BOI-CHANNEL, X-BOI-PLATFORM: data.X-BOI-PLATFORM, X-CORRELATION-ID: data.X-CORRELATION-ID}

  Scenario: GET customer profile using FS service
    Given url fsAcctSOUrl + nsc + '/' + accountNumber + '/standingorders'
    And headers headers_data
    When method GET
    Then status 200
