#This feature hits Beneficiaries FS by using only USERID
@FS_Shakedown
Feature: FS customer profile request - single account
  HTTP Method : GET
  FS URI : fs-abt-service/services/user/{userId}/beneficiaries

  Input   :  Profile_Id,X-BOI-USER,X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID
  Output  : Response for user beneficiaries

  Background:
  # Parameters
    * def data = fs_data
    * def profileId = data.profileId
    * json FS_Response =  {}
    * def headers_data = {X-BOI-USER: data.X-BOI-USER, X-BOI-CHANNEL: data.X-BOI-CHANNEL, X-BOI-PLATFORM: data.X-BOI-PLATFORM, X-CORRELATION-ID: data.X-CORRELATION-ID}

  Scenario: GET customer profile using FS service
    Given url fsUserBeneficiaryUrl + profileId + '/beneficiaries'
    And param userId = profileId
    And headers headers_data
    When method GET
    Then status 200
    And json FS_Response = response.Beneficiaries._

   ##################<End of Feature>#########################################################################

    * set Result.FS_AccountBalance.URL = fsUserBeneficiaryUrl + profileId + '/beneficiaries'
    * set Result.FS_AccountBalance.Response = FS_Response
