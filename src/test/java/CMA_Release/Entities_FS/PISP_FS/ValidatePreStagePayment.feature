#This feature hits PISP Pre Stage Payment Validation Service

@FS_Shakedown
Feature: FS Validate Pre-Stage Payment
  HTTP Method : GET
  FS URI : /fs-payment-web-service/services/ValidatePreStagePayment

  Input   :  X-BOI-USER,X-BOI-CHANNEL, X-BOI-PLATFORM, X-CORRELATION-ID
  Output  : Response for Pre-Stage Payment

  Background:
  # Parameters
    * def data = fs_data

  Scenario: GET customer profile using FS service
  #
    Given url fsAcctBalUrl + data.nsc + '/' + data.accountNumber
    And headers data
    When method GET
    Then status 200
