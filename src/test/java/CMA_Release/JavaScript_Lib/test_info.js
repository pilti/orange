function(s) {
      var t_info = new Object();


      if(s.key != null)
        {
                 if(s.Screens == 'Yes')
                 {
                    t_info.t_name = s.scenarioName.replace('#key#', s.key);
                    t_info.outDir = s.featureDir.replace("test-classes","testout");
                    t_info.path = t_info.outDir + '/'+ s.featureFileName + '/' + "UI_Sets_" + s.subset;
                 }
                 else
                 {
                    t_info.t_name = s.scenarioName.replace('#key#', s.key);
                    t_info.outDir = s.featureDir.replace("test-classes","testout");
                    t_info.path = t_info.outDir + '/'+ s.featureFileName + '/' + "API_Sets_" + s.subset;
                 }
        }
        else
        {
                if(s.Screens == 'Yes')
                {
                    t_info.t_name = s.scenarioName;
                    t_info.outDir = s.featureDir.replace("test-classes","testout");
                    t_info.path = t_info.outDir + '/'+ s.featureFileName + '/' + "UI_Tests" + '/'+ s.subset + '/';
                }
                else
                {
                    t_info.t_name = s.scenarioName;
                    t_info.outDir = s.featureDir.replace("test-classes","testout");
                    t_info.path = t_info.outDir + '/'+ s.featureFileName + '/' + "API_Tests"+'/';
                    }
         }

      return t_info;
 }