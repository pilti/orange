  function()
  {
        var m_names = new Array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
        var d = new Date();
        var curr_date = d.getDate();
        if(curr_date<10){
            curr_date='0'+curr_date;
        }
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear().toString().substr(-2);
        return(curr_date + "-" + m_names[curr_month] + "-" + curr_year);
  }