package CMA_Release.Test_data_Create.AISP.S_AccTxn.Positive_Scenarios.DataCreation;

import CMA_Release.Java_Lib.JsonTree;
import Runners.Test_Runner_Parallel;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import cucumber.api.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;

@CucumberOptions(tags = {"@AISP_Data_Create=Regression"})
public class Test_CI_Runner_Parallel extends Test_Runner_Parallel{


}
