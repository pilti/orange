@Data
@S_AccTxnTestData
@AISP_Data_Create=Regression
@ignore
Feature:  To create and store consent details with different permissions combinations

  Background:

    * def apiApp = new apiapp()
    * json Result = {}
    * set Result.Testname = null
    * def path = './src/test/java/CMA_Release/Test_Data/' + ActiveEnv + '/AISP/S_AccTxn/Positive_Scenarios/TestData_Output/'
    * def fileName = 'S_AccTxn_F02_TestData.properties'
    * def name = "TA"
    * def Application_Details = {}
    * def permissions1 = ["ReadAccountsDetail","ReadBalances","ReadTransactionsCredits","ReadTransactionsBasic"]
    * def UpdateABTTransactions = Java.type('CMA_Release.Entities_DB.UpdateABTTransactions')
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
#################################################################################################################################################################
###########################################Scenario 1######################################################################################################################
  @severity=normal
  Scenario: TestData For Transactions Positive feature 001
    Given set Application_Details $.client_id = active_tpp.AISP_PISP.client_id

    And set Application_Details.usr = user_details.Generic.G_User_1.user
    And set Application_Details.otp = user_details.Generic.G_User_1.otp

    And set Application_Details $.client_secret = active_tpp.AISP_PISP.client_secret
    And set Application_Details $.kid = active_tpp.AISP_PISP.kid
    And set Application_Details $.Organization = active_tpp.AISP_PISP.Organization
    And set Application_Details $.scope = "accounts"
    * print Application_Details
    And set permissions $.Data.Permissions = permissions1
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    And remove permissions $.Data.ExpirationDateTime
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    Then match Application_Details.refresh_token != ""
    When set Application_Details $.request_method = "GET"
    And call apiApp.Multi_Account_Information(Application_Details)
    Then match apiApp.Multi_Account_Information.responseStatus == 200
    And match apiApp.Multi_Account_Information.response.Data.Account[0].AccountId != ""
    And set Result.Multi_Account_Information.responseStatus = apiApp.Multi_Account_Information.responseStatus
    And set Result.Multi_Account_Information.response = apiApp.Multi_Account_Information.response
    And set Application_Details $.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    And def AccountNo = apiApp.Multi_Account_Information.response.Data.Account[0].Account.Identification
    * def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '#(permissions1)',
      refresh_token: '#(Application_Details.refresh_token)',
      AccountId: '#(Application_Details.AccountId)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(path,fileName,name,output)
     #To add transactions into database
    And def date = call read("classpath:CMA_Release/JavaScript_Lib/getCurrentDate.js")
    And print "Current date is :"
    And print date
    And def txn1 = UpdateABTTransactions.addPostedTransactions(AccountNo,10,date,'201', ABT_DB_Conn)




