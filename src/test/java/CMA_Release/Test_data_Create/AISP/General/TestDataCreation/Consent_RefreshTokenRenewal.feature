@Data1
@RTR

Feature: This feature is to construct ConsentURL and perform Consent to generate Authcode for RefreshTokenRenewal Flow

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()

    * json Result = {}
    * json UIResult = {}
    * json AccReQ = {}
    * set Result.Testname = null
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def capturescreen = Java.type('CMA_Release.Entities_UI.General.capturescreen')
    * def op_path = './src/test/java/CMA_Release/Test_Data/AISP/General/TestData_Output/'
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """

  Scenario Outline: To generate consent URL for Refresh Token Renewal and run ACG

    * set user_details.usr = <User>
    * set user_details.otp = '123456'
    * def User = user_details.usr

    Given json Application_Details = active_tpp.AISP_PISP
    #Hit Certificate request to server
    And call apiApp.configureSSL(Application_Details)

    #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"

    ##########################################################################################################################################

    #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)

    #Validate response and update result json
    And set Result.Access_Token_CCG.Input = Application_Details
    And set Result.Access_Token_CCG.Endpoint = apiApp.Access_Token_CCG.TokenUrl
    And set Result.Access_Token_CCG.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.Access_Token_CCG.response = apiApp.Access_Token_CCG.response
    And set Result.Access_Token_CCG.responseHeaders = apiApp.Access_Token_CCG.responseHeaders

    #Perform Assertion
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    ##########################################################################################################################################

    #Update ip json with parameters required by next API
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    And set Application_Details.x_fapi_financial_id = "OB/BOI/001"

    #Call Account_Request_Setup API
    When call apiApp.Account_Request_Setup(Application_Details)

    #Valideate response and form result json
    And set Result.Account_Request_Setup.Input = Application_Details
    And set Result.Account_Request_Setup.Endpoint = apiApp.Account_Request_Setup.AcctReqUrl
    And set Result.Account_Request_Setup.responseStatus = apiApp.Account_Request_Setup.responseStatus
    And set Result.Account_Request_Setup.response = apiApp.Account_Request_Setup.response
    And set Result.Account_Request_Setup.responseHeaders = apiApp.Account_Request_Setup.responseHeaders

    ############################################Write output File##############################################################################################
    Then match apiApp.Account_Request_Setup.responseStatus == 201
    And set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    ############################Set input parameters for CreateRequestObjec######################################################################

    * set ReqObjIP $.header.kid = Application_Details.kid
    * set ReqObjIP $.payload.iss = Application_Details.client_id
    * set ReqObjIP $.payload.client_id = Application_Details.client_id
    * set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    * set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId

    * set ReqObjIP $.payload.scope = 'openid accounts'
    * set Application_Details $.scope = 'openid accounts'

    * def path = stmtpath(Application_Details) + 'signin_private.key'

    * set Application_Details.path = path

############################Call CreateRequestObjec######################################################################
    When call apiApp.CreateRequestObject(Application_Details)
    * set Result.CreateRequestObject.Input = ReqObjIP
    * set Result.CreateRequestObject.Input.SiningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)
    * set Result.CreateRequestObject.JWT = apiApp.CreateRequestObject.jwt.request

    * set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    * set Result.CreateRequestObject.URL = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    #added to save the consnet url , & launch again during authorisation renewal
    * print consenturl
    * set Application_Details $.consenturl = consenturl
    #* def consenturl = 'http://www.bbc.com'
##########################################################################################################################

    Then def c = call  webApp.driver1 = webApp.start1(user_details.browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    Given def perform = Functions.launchConsentURL(webApp,consenturl)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.launchConsentURL_Status = perform.Status
    And set Result.UIOutput.launchConsentURL_Landing_Page = perform.Landing_Page

    Then def perform = Functions.useKeyCodeApp(webApp)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.useKeyCodeApp_Status = perform.Status
    And set Result.UIOutput.useKeyCodeApp_Landing_Page = perform.Landing_Page

    * def data = {"usr":'#(user_details.usr)',"otp":'#(user_details.otp)',"action":"continue","Confirmation":"Yes"}

    Then def perform = Functions.scaLogin(webApp,data)
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.ScaLogin_Status = perform.Status
    And set Result.UIOutput.ScaLogin_Landing_Page = perform.Landing_Page

    Then def perform = Functions.selectAllAccounts(webApp,data)
    #* print perform
    * set UIResult.AccountSelectionPage = perform
    * print perform
    Then match perform.Status == 'Pass'
    And set Result.UIOutput.SelectAllAccounts_Status = perform.Status
    And set Result.UIOutput.SelectAllAccounts_PageTitle = perform.PageTitle

    * set data $.SelectedAccounts = perform.SelectedAccounts

    #added to save the accounts selected during SCA, & verify during authorisation renewal
    * set Application_Details.selectedAccounts = perform.SelectedAccounts

    And set Result.UIOutput.SelectAllAccounts_SelectedAccounts = perform.SelectedAccounts

    Then def perform = Functions.reviewConfirm(webApp,data)
    #* print perform

    #added to save the permissions selected during SCA , & verify during authorisation renewal
    * set Application_Details.Permissions = perform.Permissions
    And set Result.UIOutput.ReviewConfirm.Permissions = perform.Permissions
    * set UIResult.ReviewConfirmPage = perform

    ###################################Calling Access_Token_ACG API########################################################################################################

    And set Application_Details.code = perform.Action.Authcode

    And set UIResult.code = perform.Action.Authcode
    And set UIResult.idtoken = perform.Action.idtoken
    #* print Result.code
    And def perform = Reuseable.AuthCodeDisplay(webApp,UIResult.code)
    And def screenshot = capturescreen.captureScreenShot2(webApp)
    * def stop = webApp.stop()

    #####################################################################################################################################################################

    #Call Access_Token_ACG API

    When set Application_Details $.grant_type = "authorization_code"
    And set Application_Details $.redirect-uri = "https://www.getpostman.com/oauth2/callback"
    And call apiApp.Access_Token_ACG(Application_Details)

    * print apiApp.Access_Token_ACG.response

    And eval if (apiApp.Access_Token_ACG.responseStatus == 200) {Result.Status = "Pass"} else {Result.Status = "Fail"}
    And set Result.Access_Token_ACG.responseStatus = apiApp.Access_Token_ACG.responseStatus
    And set Result.Access_Token_ACG.response = apiApp.Access_Token_ACG.response
    And set Application_Details $.refresh_token = apiApp.Access_Token_ACG.response.refresh_token
    And set Application_Details $.access_token = apiApp.Access_Token_ACG.response.access_token


    * print Application_Details
    * print permissions.Data.Permissions
    * print Application_Details.selectedAccounts
    Then def fileName = 'RefreshTokenRenewal_' + user_details.usr + '.properties'
    And set Result.E2EConsent_GenerateToken.Consent = Application_Details
    And def id = 'Consent'
    And def output =
    """
    {
      ConsentURL: '#(Application_Details.consenturl)',
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      selected_accounts: '#(Application_Details.selectedAccounts)',
      permissions: '#( Application_Details.Permissions.RetrivedPemissionArray)',
      refresh_token: '#(Application_Details.refresh_token)',
      ConsentExpiry: '#(Application_Details.ConsentExpiry)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)

    Examples:
      | User             |
      | user_details.usr |
      | 201659           |




