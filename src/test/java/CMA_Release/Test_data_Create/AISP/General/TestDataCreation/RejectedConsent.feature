Feature: This reusable feature is to do Rejected status after clicking cancel on Consent

  Background:
    * json Result1 = {}
    * json UIResult = {}
    * json AccReQ = {}
    #added just in case to run this file individually
    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * def op_path = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * def Functions = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP')
    * def Reuseable = Java.type('CMA_Release.Entities_UI.Selenium.ReusableFunctions')
    * def PageObj = Java.type('CMA_Release.Entities_UI.PageObjects.Consent_UI_AISP.Account_selection')
    * def User = user_details.Generic.G_User_1.user
    * def OTP = user_details.Generic.G_User_1.otp
    * def data = {"usr":'#(User)',"otp":'#(OTP)',"action":"continue","Confirmation":"Yes"}


  Scenario: Consent Authorisation and generate Authcode
    #Step 1: Get Access_Token_CCG
    Given def Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    When call apiApp.Access_Token_CCG(Application_Details)
    Then match apiApp.Access_Token_CCG.responseStatus == 200

    #Step 2: Account_Request_Setup
    Given set Application_Details.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details.token_type = apiApp.Access_Token_CCG.response.token_type
    When call apiApp.Account_Request_Setup(Application_Details)
    Then match apiApp.Account_Request_Setup.responseStatus == 201

    #Step 3: Create RequestObject and Construct consenturl
    Given set Application_Details.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set AccReQ.AccountRequestId = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = apiApp.Account_Request_Setup.response.Data.AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path
    When call apiApp.CreateRequestObject(Application_Details)
    And set Application_Details $.jwt = apiApp.CreateRequestObject.jwt.request
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    Then call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    And print consenturl
    And set Application_Details $.consenturl = consenturl

    #Step 4: Consent Authorisation
    Given def webApp = new webapp()
    And def c = call  webApp.driver1 = webApp.start1(default_browser)
    And def path = call  webApp.path1 = Application_Details.TestCasePath
    When def perform = Functions.launchConsentURL(webApp,consenturl)
    Then match perform.Status == 'Pass'
    Then def perform = Functions.useKeyCodeApp(webApp)
    Then def perform = Functions.scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def clickoncancel = Reuseable.ClickElement(webApp,PageObj.cancel_button)
    And def cancelConfirm = Reuseable.ClickElement(webApp,PageObj.cancel_request)

    * def fileName = 'DeletedConsent.properties'
    And def id = 'DeletedConsent'
    And def key = 'DeletedConsent'
    And def output =
    """
    {
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      ConsentURL: '#(Application_Details.consenturl)',
      usr: '#(User)',
      otp: '#(OTP)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)
