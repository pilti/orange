@AISP_Data_Create
@AISP_Data_Create=Regression
@AISP_Data_Create=Additional
Feature: Verify whether consent is expired

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def Application_Details = {}
##########################################################################################################################################
##########################################################################################################################################
  @ANu771
  @Expired_Consent
  Scenario Outline: To verify account request is revoked successfully post consent Authorization

    Given set Application_Details $.client_id = active_tpp.AISP_PISP.client_id
    And set Application_Details.usr = <User>
    And set Application_Details.otp = "123456"
    And set Application_Details $.client_secret = active_tpp.AISP_PISP.client_secret
    And set Application_Details $.kid = active_tpp.AISP_PISP.kid
    And set Application_Details $.Organization = active_tpp.AISP_PISP.Organization
    And set Application_Details $.scope = "accounts"
    * print Application_Details

    #Provide Account request expiration time as 10 mins from now
    And set permissions $.Data.ExpirationDateTime = Java.type('CMA_Release.Java_Lib.GetCurrentTime').Time_stamp(600)
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    #And remove permissions $.Data.ExpirationDateTime
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)

    #Get account details of account for which consent is provided
    And set Application_Details.request_method = 'GET'
    And call apiApp.Multi_Account_Information(Application_Details)
    And set Application_Details.AccountId = apiApp.Multi_Account_Information.response.Data.Account[0].AccountId
    * print Application_Details
    Then def fileName = '<Filename>.properties'
    And set Result.E2EConsent_GenerateToken.Consent = Application_Details
    And def id = 'Consent'
    And def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      AccountId: '#(Application_Details.AccountId)',
      permissions: '#(permissions)',
      refresh_token: '#(Application_Details.refresh_token)',
      consenturl: '#(Application_Details.consenturl)',
      permissions: '#( Application_Details.Permissions.RetrivedPemissionArray)',
      ConsentExpiry: '#(Application_Details.ConsentExpiry)',
      ConsentURL: '#(Application_Details.consenturl)',
       usr: '#(Application_Details.usr)',
      otp: '#(Application_Details.otp)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)

    Examples:
      | User                                     | Filename                        |
      | user_details.Generic.G_User_4.user       | ExpiredConsent_Generic_G_User_4 |
      | user_details.Beneficiary.B_User_4.user   | ExpiredConsent_Generic_B_User_4 |
      | user_details.Products.P_User_4.user      | ExpiredConsent_Generic_P_User_4 |
      | user_details.StandingOrder.S_User_4.user | ExpiredConsent_Generic_S_User_4 |
