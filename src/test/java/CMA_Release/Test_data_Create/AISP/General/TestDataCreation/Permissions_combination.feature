@AISP_Data_Create
Feature: Permission Combinations

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def Application_Details = {}
##########################################################################################################################################
##########################################################################################################################################
  @PermissionsCombinations

  Scenario Outline: To verify consent creation with #key# permissions combination
    * def subset = "TestData_Permissions"
    Given json Application_Details = active_tpp.AISP_PISP
    And set Application_Details $.client_id = active_tpp.AISP_PISP.client_id
    And set Application_Details.usr = <User>
    And set Application_Details.otp = "123456"
    And set Application_Details $.client_secret = active_tpp.AISP_PISP.client_secret
    And set Application_Details $.kid = active_tpp.AISP_PISP.kid
    And set Application_Details $.Organization = active_tpp.AISP_PISP.Organization
    And set Application_Details $.scope = "accounts"
    * print Application_Details
    And set permissions $.Data.Permissions = <permissions>
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    #And remove permissions $.Data.ExpirationDateTime
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    * print Application_Details
    Then def fileName = '<filename>.properties'
    And set Result.E2EConsent_GenerateToken.Consent = Application_Details
    And def id = 'Consent'
    And def key = '<filename>'
    And def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      refresh_token: '#(Application_Details.refresh_token)',
      selected_accounts: '#(Application_Details.selectedAccounts)',
      permissions: '#( Application_Details.Permissions.RetrivedPemissionArray)',
      ConsentExpiry: '#(Application_Details.ConsentExpiry)',
      ConsentURL: '#(Application_Details.consenturl)',
      retrieved_permission_heading: '#(Application_Details.retrieved_permission_heading)',
      usr: '#(Application_Details.usr)',
      otp: '#(Application_Details.otp)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)

  @AISP_Data_Create=Regression1
    Examples:
      | User                                     | filename                               | permissions                                                                                                                                                                                                      |
#     | user_details.Generic.G_User_1.user       | All_Basic_permissions_G_User_1         | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]     |
      | user_details.Generic.G_User_1.user       | Generic_G_User_1                       | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
#      | user_details.Beneficiary.B_User_1.user   | All_Basic_permissions_B_User_1         | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]     |
#      | user_details.Beneficiary.B_User_1.user   | Generic_B_User_1                       | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
#      | user_details.Products.P_User_1.user      | All_Basic_permissions_P_User_1         | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]     |
#      | user_details.Products.P_User_1.user      | Generic_P_User_1                       | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
#      | user_details.StandingOrder.S_User_1.user | All_Basic_permissions_S_User_1         | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]     |
#      | user_details.StandingOrder.S_User_1.user | Generic_S_User_1                       | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
#      | user_details.Generic.G_User_1.user       | No_Accounts_Permission_G_User_1        | ["ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                       |
#      | user_details.Generic.G_User_1.user       | Only_Direct_Debits_permission_G_User_1 | ["ReadAccountsDetail", "ReadDirectDebits"]                                                                                                                                                                       |
#      | user_details.Products.P_User_1.user      | Only_Products_permission_P_User_1      | ["ReadAccountsDetail", "ReadProducts"]                                                                                                                                                                           |
#      | user_details.Generic.G_User_1.user       | Only_Transactions_Permission_G_User_1   | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"]                                                                                                             |
#      | user_details.Generic.G_User_2.user       | Generic_G_User_2                        | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
#      | user_details.MultiAccounts.M_User_1.user    | Generic_M_User_1                       | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |


  @AISP_Data_Create=Additional
    Examples:
      | User                                     | filename                                 | permissions                                                                                                                                                                                                       |
      | user_details.Generic.G_User_1.user       | No_Accounts_Permission_G_User_1          | ["ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                        |
      | user_details.Generic.G_User_1.user       | No_Balance_Permission_G_User_1           | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Generic.G_User_1.user       | No_Transactions_Permission_G_User_1      | ["ReadAccountsDetail", "ReadBalances", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                                                                                 |
      | user_details.Generic.G_User_1.user       | No_Products_permission_G_User_1          | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Generic.G_User_1.user       | No_Direct_Debits_permission_G_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                      |
      | user_details.Generic.G_User_1.user       | No_Standing_orders_permission_G_User_1   | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail"]                              |
      | user_details.Generic.G_User_1.user       | No_Beneficiaries_permission_G_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadStandingOrdersDetail"]                             |
      | user_details.Generic.G_User_1.user       | Only_Account_info_permissions_G_User_1   | ["ReadAccountsDetail"]                                                                                                                                                                                            |
      | user_details.Generic.G_User_1.user       | Only_Balance_Permission_G_User_1         | ["ReadAccountsDetail", "ReadBalances"]                                                                                                                                                                            |
      | user_details.Generic.G_User_1.user       | Only_Transactions_Permission_G_User_1    | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"]                                                                                                             |
      | user_details.Generic.G_User_1.user       | Only_Products_permission_G_User_1        | ["ReadAccountsDetail", "ReadProducts"]                                                                                                                                                                            |
      | user_details.Generic.G_User_1.user       | Only_Direct_Debits_permission_G_User_1   | ["ReadAccountsDetail", "ReadDirectDebits"]                                                                                                                                                                        |
      | user_details.Generic.G_User_1.user       | Only_Standing_orders_permission_G_User_1 | ["ReadAccountsDetail", "ReadStandingOrdersDetail"]                                                                                                                                                                |
      | user_details.Generic.G_User_1.user       | Only_Beneficiaries_permission_G_User_1   | ["ReadAccountsDetail", "ReadBeneficiariesDetail"]                                                                                                                                                                 |
      | user_details.Generic.G_User_1.user       | All_Basic_permissions_G_User_1           | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]      |
      | user_details.Generic.G_User_1.user       | Generic_G_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Generic.G_User_2.user       | Generic_G_User_2                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Generic.G_User_3.user       | Generic_G_User_3                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Beneficiary.B_User_1.user   | No_Accounts_Permission_B_User_1          | ["ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                        |
      | user_details.Beneficiary.B_User_1.user   | No_Balance_Permission_B_User_1           | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Beneficiary.B_User_1.user   | No_Transactions_Permission_B_User_1      | ["ReadAccountsDetail", "ReadBalances", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                                                                                 |
      | user_details.Beneficiary.B_User_1.user   | No_Products_permission_B_User_1          | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Beneficiary.B_User_1.user   | No_Direct_Debits_permission_B_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                      |
      | user_details.Beneficiary.B_User_1.user   | No_Standing_orders_permission_B_User_1   | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail"]                              |
      | user_details.Beneficiary.B_User_1.user   | No_Beneficiaries_permission_B_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadStandingOrdersDetail"]                             |
      | user_details.Beneficiary.B_User_1.user   | Only_Account_info_permissions_B_User_1   | ["ReadAccountsDetail"]                                                                                                                                                                                            |
      | user_details.Beneficiary.B_User_1.user   | Only_Balance_Permission_B_User_1         | ["ReadAccountsDetail", "ReadBalances"]                                                                                                                                                                            |
      | user_details.Beneficiary.B_User_1.user   | Only_Transactions_Permission_B_User_1    | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"]                                                                                                             |
      | user_details.Beneficiary.B_User_1.user   | Only_Products_permission_B_User_1        | ["ReadAccountsDetail", "ReadProducts"]                                                                                                                                                                            |
      | user_details.Beneficiary.B_User_1.user   | Only_Direct_Debits_permission_B_User_1   | ["ReadAccountsDetail", "ReadDirectDebits"]                                                                                                                                                                        |
      | user_details.Beneficiary.B_User_1.user   | Only_Standing_orders_permission_B_User_1 | ["ReadAccountsDetail", "ReadStandingOrdersDetail"]                                                                                                                                                                |
      | user_details.Beneficiary.B_User_1.user   | Only_Beneficiaries_permission_B_User_1   | ["ReadAccountsDetail", "ReadBeneficiariesDetail"]                                                                                                                                                                 |
      | user_details.Beneficiary.B_User_1.user   | All_Basic_permissions_B_User_1           | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]      |
      | user_details.Beneficiary.B_User_1.user   | Generic_B_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Beneficiary.B_User_2.user   | Generic_B_User_2                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Beneficiary.B_User_3.user   | Generic_B_User_3                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Products.P_User_1.user      | No_Accounts_Permission_P_User_1          | ["ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                        |
      | user_details.Products.P_User_1.user      | No_Balance_Permission_P_User_1           | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Products.P_User_1.user      | No_Transactions_Permission_P_User_1      | ["ReadAccountsDetail", "ReadBalances", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                                                                                 |
      | user_details.Products.P_User_1.user      | No_Products_permission_P_User_1          | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.Products.P_User_1.user      | No_Direct_Debits_permission_P_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                      |
      | user_details.Products.P_User_1.user      | No_Standing_orders_permission_P_User_1   | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail"]                              |
      | user_details.Products.P_User_1.user      | No_Beneficiaries_permission_P_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadStandingOrdersDetail"]                             |
      | user_details.Products.P_User_1.user      | Only_Account_info_permissions_P_User_1   | ["ReadAccountsDetail"]                                                                                                                                                                                            |
      | user_details.Products.P_User_1.user      | Only_Balance_Permission_P_User_1         | ["ReadAccountsDetail", "ReadBalances"]                                                                                                                                                                            |
      | user_details.Products.P_User_1.user      | Only_Transactions_Permission_P_User_1    | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"]                                                                                                             |
      | user_details.Products.P_User_1.user      | Only_Products_permission_P_User_1        | ["ReadAccountsDetail", "ReadProducts"]                                                                                                                                                                            |
      | user_details.Products.P_User_1.user      | Only_Direct_Debits_permission_P_User_1   | ["ReadAccountsDetail", "ReadDirectDebits"]                                                                                                                                                                        |
      | user_details.Products.P_User_1.user      | Only_Standing_orders_permission_P_User_1 | ["ReadAccountsDetail", "ReadStandingOrdersDetail"]                                                                                                                                                                |
      | user_details.Products.P_User_1.user      | Only_Beneficiaries_permission_P_User_1   | ["ReadAccountsDetail", "ReadBeneficiariesDetail"]                                                                                                                                                                 |
      | user_details.Products.P_User_1.user      | All_Basic_permissions_P_User_1           | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]      |
      | user_details.Products.P_User_1.user      | Generic_P_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Products.P_User_2.user      | Generic_P_User_2                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.Products.P_User_3.user      | Generic_P_User_3                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.StandingOrder.S_User_1.user | No_Accounts_Permission_S_User_1          | ["ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                   |
      | user_details.StandingOrder.S_User_1.user | No_Balance_Permission_S_User_1           | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]             |
      | user_details.StandingOrder.S_User_1.user | No_Transactions_Permission_S_User_1      | ["ReadAccountsDetail", "ReadBalances", "ReadStandingOrder", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                                                                            |
      | user_details.StandingOrder.S_User_1.user | No_StandingOrder_permission_S_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                  |
      | user_details.StandingOrder.S_User_1.user | No_Direct_Debits_permission_S_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]                 |
      | user_details.StandingOrder.S_User_1.user | No_Standing_orders_permission_S_User_1   | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadDirectDebits", "ReadBeneficiariesDetail"]                         |
      | user_details.StandingOrder.S_User_1.user | Only_Products_permission_S_User_1        | ["ReadAccountsDetail", "ReadProducts"]                                                                                                                                                                            |
      | user_details.StandingOrder.S_User_1.user | No_Beneficiaries_permission_S_User_1     | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadDirectDebits", "ReadStandingOrdersDetail"]                        |
      | user_details.StandingOrder.S_User_1.user | Only_Account_info_permissions_S_User_1   | ["ReadAccountsDetail"]                                                                                                                                                                                            |
      | user_details.StandingOrder.S_User_1.user | Only_Balance_permission_S_User_1         | ["ReadAccountsDetail", "ReadBalances"]                                                                                                                                                                            |
      | user_details.StandingOrder.S_User_1.user | Only_Transactions_permission_S_User_1    | ["ReadAccountsDetail", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits"]                                                                                                             |
      | user_details.StandingOrder.S_User_1.user | Only_StandingOrder_permission_S_User_1   | ["ReadAccountsDetail", "ReadStandingOrder"]                                                                                                                                                                       |
      | user_details.StandingOrder.S_User_1.user | Only_Direct_Debits_permission_S_User_1   | ["ReadAccountsDetail", "ReadDirectDebits"]                                                                                                                                                                        |
      | user_details.StandingOrder.S_User_1.user | Only_Standing_orders_permission_S_User_1 | ["ReadAccountsDetail", "ReadStandingOrdersDetail"]                                                                                                                                                                |
      | user_details.StandingOrder.S_User_1.user | Only_Beneficiaries_permission_S_User_1   | ["ReadAccountsDetail", "ReadBeneficiariesDetail"]                                                                                                                                                                 |
      | user_details.StandingOrder.S_User_1.user | All_Basic_permissions_S_User_1           | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"]      |
      | user_details.StandingOrder.S_User_1.user | Generic_S_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.StandingOrder.S_User_1.user | All_Basic_permissions_S_User_1           | ["ReadAccountsBasic", "ReadBalances", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadStandingOrder", "ReadDirectDebits", "ReadBeneficiariesBasic", "ReadStandingOrdersBasic"] |
      | user_details.StandingOrder.S_User_1.user | Generic_S_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.StandingOrder.S_User_2.user | Generic_S_User_2                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.StandingOrder.S_User_3.user | Generic_S_User_3                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
      | user_details.MultiAccounts.M_User_1.user | Generic_M_User_1                         | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"]  |
