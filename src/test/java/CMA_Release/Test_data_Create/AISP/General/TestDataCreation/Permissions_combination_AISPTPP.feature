@AISP_Data_Create
@AISP_Data_Create=Regression
@AISP_Data_Create=Additional
Feature: Permission Combinations

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/AISP/General/TestData_Output/'
    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }
      """
    * def Application_Details = {}
##########################################################################################################################################
##########################################################################################################################################
  @PermissionsCombinations_AISPTPP
  Scenario Outline: To verify consent creation with #key# permissions combination
    * def subset = "TestData_Permissions"
    Given json Application_Details = active_tpp.AISP
    And set Application_Details $.client_id = active_tpp.AISP.client_id
    And set Application_Details = active_tpp.AISP
    And set Application_Details.usr = <User>
    And set Application_Details.otp = "123456"
    And set Application_Details $.client_secret = active_tpp.AISP.client_secret
    And set Application_Details $.kid = active_tpp.AISP.kid
    And set Application_Details $.Organization = active_tpp.AISP.Organization
    And set Application_Details $.scope = "accounts"
    * print Application_Details
    And set permissions $.Data.Permissions = <permissions>
    And remove permissions $.Data.TransactionFromDateTime
    And remove permissions $.Data.TransactionToDateTime
    #And remove permissions $.Data.ExpirationDateTime
    And call apiApp.configureSSL(Application_Details)
    When call apiApp.E2EConsent_GenerateToken(Application_Details)
    * print Application_Details
    Then def fileName = '<filename>.properties'
    And set Result.E2EConsent_GenerateToken.Consent = Application_Details
    And def id = 'Consent'
    And def key = '<filename>'
    And def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      AccountRequestId: '#(Application_Details.AccountRequestId)',
      permissions: '<permissions>',
      refresh_token: '#(Application_Details.refresh_token)',
      permissions: '#( Application_Details.Permissions.RetrivedPemissionArray)',
      ConsentExpiry: '#(Application_Details.ConsentExpiry)',
      ConsentURL: '#(Application_Details.consenturl)',
       usr: '#(Application_Details.usr)',
      otp: '#(Application_Details.otp)'
    }
    """
    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)


    Examples:
      | User                                     | filename                 | permissions                                                                                                                                                                                                      |
      | user_details.Generic.G_User_1.user       | Generic_AISPTPP_G_User_1 | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
      | user_details.Beneficiary.B_User_1.user   | Generic_AISPTPP_B_User_1 | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
      | user_details.StandingOrder.S_User_1.user | Generic_AISPTPP_S_User_1 | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
      | user_details.Products.P_User_1.user      | Generic_AISPTPP_P_User_1 | ["ReadAccountsDetail", "ReadBalances", "ReadTransactionsDetail", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadProducts", "ReadDirectDebits", "ReadBeneficiariesDetail", "ReadStandingOrdersDetail"] |
