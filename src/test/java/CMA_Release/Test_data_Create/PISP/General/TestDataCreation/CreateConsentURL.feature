@CreateConsentURL
Feature: CCToken creation

  Background:

    * def apiApp = new apiapp()
    * def webApp = new webapp()
    * json Result = {}
    * json UIResult = {}
    * set Result.Testname = null
    * def op_path = './src/test/java/CMA_Release/Test_Data/'+ ActiveEnv +'/PISP/General/TestData_Output/'

    * configure afterScenario =
      """
      function(){
       var info = karate.info;
       if (typeof subset != 'undefined'){info.subset = subset}
       if (typeof key != 'undefined'){info.key = key}
       if(info.errorMessage == null){
              Result.TestStatus = "pass";
        }else{
              Result.TestStatus = "fail";
              Result.Error = info.errorMessage;
       }
       apiApp.write(Result,info);
       }

      """
    * def Application_Details = {}


  Scenario: PISP Create Consent URL
    Given def info = karate.info
    And set info.subset = "Access_Token_CCG"
    And json Application_Details = active_tpp.AISP_PISP
    And call apiApp.configureSSL(Application_Details)
  #Set input with the request Method
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"

  ##########################################################################################################################################

  #Call Access_Token_CCG API
    When call apiApp.Access_Token_CCG(Application_Details)

  #Validate response and update result json

    And set Result.ActualOutput.Access_Token_CCG.Input.CCG_scope = Application_Details.scope
    And set Result.ActualOutput.Access_Token_CCG.Input.request_method = Application_Details.request_method
    And set Result.ActualOutput.Access_Token_CCG.Input.grant_type = Application_Details.grant_type
    And set Result.ActualOutput.Access_Token_CCG.Input.Content_type = Application_Details.Content_type
    And set Result.ActualOutput.Access_Token_CCG.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Access_Token_CCG.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Access_Token_CCG.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Access_Token_CCG.Output.responseStatus = apiApp.Access_Token_CCG.responseStatus
    And set Result.ActualOutput.Access_Token_CCG.Output.response = apiApp.Access_Token_CCG.response
    And set Result.Additional_Details = Application_Details
    And def access_token = apiApp.Access_Token_CCG.response.access_token
    Then match apiApp.Access_Token_CCG.responseStatus == 200


    #Setup API

    And call apiApp.configureSSL(Application_Details)

    And set Application_Details.access_token = access_token
    And set Application_Details.request_method = "POST"


    #Manipulate data

    And call apiApp.Payment_Setup(Application_Details)

    And set Result.ActualOutput.Payment_Setup.Input.CCG_scope = apiApp.Payment_Setup.scope
    And set Result.ActualOutput.Payment_Setup.Input.request_method = Application_Details.request_method

    And set Result.ActualOutput.Payment_Setup.Input.client_id = Application_Details.client_id
    And set Result.ActualOutput.Payment_Setup.Input.client_secret = Application_Details.client_secret
    And set Result.ActualOutput.Payment_Setup.Input.Endpoint = TokenUrl
    And set Result.ActualOutput.Payment_Setup.Output.responseStatus = apiApp.Payment_Setup.responseStatus
    And set Result.ActualOutput.Payment_Setup.Output.response = apiApp.Payment_Setup.response
    And set Result.Additional_Details = Application_Details
    And def idempotency_key = apiApp.Payment_Setup.idempotency_key.idempotency_key
    Then match apiApp.Payment_Setup.responseStatus == 201
    And def PaymentId = apiApp.Payment_Setup.response.Data.PaymentId



    #Creating Token

    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = PaymentId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = PaymentId

    And set ReqObjIP $.payload.scope = 'openid payments'
    And set Application_Details $.scope = 'openid payments'

    And def path = stmtpath(Application_Details) + 'signin_private.key'

    And set Application_Details.path = path

    And set Result.ActualOutput.JWT.Input.header = ReqObjIP.header
    And set Result.ActualOutput.JWT.Input.payload = ReqObjIP.payload
    And set Result.ActualOutput.JWT.Input.SigningKey = Java.type('CMA_Release.Java_Lib.ReadTextFile').ReadFile(path)

    When call apiApp.CreateRequestObject(Application_Details)
    And def request1 = apiApp.CreateRequestObject.jwt.request
    And set Result.ActualOutput.JWT.Output.request = request1


    #Forming Consent
    And def info1 = karate.call("classpath:CMA_Release/JavaScript_Lib/test_info.js",info)

    And def path = call  webApp.path1 = info1.path
    And print "Path of Screenshot is:" + webApp.path1
    And json Application_Details = active_tpp.AISP_PISP


    * set Application_Details $.jwt = request1
    * set Application_Details $.response_type = 'code id_token'
    * set Application_Details $.state = 'af0ifjsldkj'
    * set Application_Details $.nonce = 'n-0S6_WzA2Mj'

    When call apiApp.ConstructAuthReqUrl(Application_Details)
    And def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    #added to save the consnet url , & launch again during authorisation renewal
    * print consenturl
    * set Application_Details $.consenturl = consenturl



    And set Result.ActualOutput.FormingAuthorizeRequest.Input.Scope = Application_Details.scope
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.Request = request1
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.redirect_uri = Application_Details.redirect_uri
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.response_type = Application_Details.response_type
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.state = Application_Details.state
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.nonce = Application_Details.nonce
    And set Result.ActualOutput.FormingAuthorizeRequest.Input.client_id = Application_Details.client_id

    And set Result.ActualOutput.FormingAuthorizeRequest.Output.ConsentUrl = Application_Details.consenturl

    Then def fileName = 'ConsentURL.properties'
    And def id = 'ConsentURL'
    And def output =
    """
    {
      Organization: '#(Application_Details.Organization)',
      client_id: '#(Application_Details.client_id)',
      client_secret: '#(Application_Details.client_secret)',
      kid: '#(Application_Details.kid)',
      Application_Name: '#(Application_Details.Application_Name)',
      Legal_Entity_Name: '#(Application_Details.Legal_Entity_Name)',
      consent: '#(consenturl)'
    }
    """

    And def profile = RWPropertyFile.WriteFile(op_path,fileName,id,output)