//Functions to query SP_Shared_data
package CMA_Release.Entities_DB;

import com.google.gson.Gson;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import java.sql.*;
import java.util.List;
import java.util.Map;
/**
 * Created by C962653 on 29/01/2018.
 */
public class SP_SHARED_DATA_DB {

    public static Connection connection = null;
    public static Statement stmt = null;
    public static PreparedStatement pstmt = null;

    public static void getConnection(Map<String, String> SP_SHARED_DATA_DB_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+SP_SHARED_DATA_DB_Conn.get("URL")+":"+SP_SHARED_DATA_DB_Conn.get("Port")+":"+SP_SHARED_DATA_DB_Conn.get("SID"),SP_SHARED_DATA_DB_Conn.get("UserId"), SP_SHARED_DATA_DB_Conn.get("Password"));
            System.out.println("Connected database successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String get_SP_Shared_Data(String NSC, String ACCTNO, Map<String, String> SP_SHARED_DATA_DB_Conn) {
        getConnection(SP_SHARED_DATA_DB_Conn);
        return resultSetToJson(connection, "Select * from SP_SHARED_DATA.TBL_ACCOUNT where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "BRANCH_NSC=" + NSC);
    }

    public static String update_SP_Shared_Data(String NSC, String ACCTNO, Map<String, String> SP_SHARED_DATA_DB_Conn,String query) {
        getConnection(SP_SHARED_DATA_DB_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE SP_SHARED_DATA.TBL_ACCOUNT " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND BRANCH_NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT * FROM SP_SHARED_DATA.TBL_ACCOUNT WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND BRANCH_NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("BRANCH_NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));
            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }// do nothing
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return "fail";

    }  //tested
    public static String insert_SP_Shared_Data(Map<String, String> SP_SHARED_DATA_DB_Conn,String columns, String values) {
        getConnection(SP_SHARED_DATA_DB_Conn);
        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "insert into SP_SHARED_DATA.TBL_ACCOUNT ("+columns+") values("+values+")";

            stmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Insert Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }// do nothing
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return "fail";

    } //tested
    public static String delete_SP_Shared_Data(String NSC, String ACCTNO, Map<String, String> SP_SHARED_DATA_DB_Conn) {
        getConnection(SP_SHARED_DATA_DB_Conn);
        try{
            //STEP 4: Execute a query
            String sql = "DELETE FROM SP_SHARED_DATA.TBL_ACCOUNT where BRANCH_NSC = "+NSC+" AND ACCOUNT_NUMBER = "+ACCTNO;
            System.out.println("Creating statement...");
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Delete Successful...");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            //finally block used to close resources
            try{
                if(pstmt!=null)
                    connection.close();
            }catch(SQLException se){
            }// do nothing
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return "fail";
    }

    public static String resultSetToJson(Connection connection, String query) {
        List<Map<String, Object>> listOfMaps = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            listOfMaps = queryRunner.query(connection, query, new MapListHandler());
        } catch (SQLException se) {
            throw new RuntimeException("Couldn't query the database.", se);
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return new Gson().toJson(listOfMaps);
    }
}
