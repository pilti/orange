#This feature hits Customer profile service to fetch all accounts details
@FS_Shakedown
Feature: DB customer profile request
  DB : CHAN_PROF.CHANNEL_ACCOUNT_PROFILE
  Input   :  User_ID
  Output  : Response for Customer Accounts

  Background:
  # Parameters
    * def data = db_data
    * def ABTdb = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def CPdb = Java.type('CMA_Release.Entities_DB.CHANNEL_PROFILE_DB')
    * def ChannelDB = CHANNEL_PROFILE_DB_Conn
    * def ABT_DB = ABT_DB_Conn

  Scenario: GET customer account details using ChannelAccountProfile DB
    Given def userId = data.user
    When json CustomerAccount_details = CPdb.get_Channel_Profile(userId,ChannelDB)
    # Define output json
    * def outjson = {}

  # Get ABT data for each Account in CHANNEL_PROFILE output and populate outjson
  # populate array out[] with each account
    * eval
  """
  var out = []
  for (i = 0; i < CustomerAccount_details.length; i++)
  {
    if(CustomerAccount_details[i].ACCOUNT_NUMBER != null)
    {

     db_record = ABTdb.get_ABT_Details(CustomerAccount_details[i].NSC, CustomerAccount_details[i].ACCOUNT_NUMBER,  ABT_DB_Conn);
     var obj = JSON.parse(db_record);

     outjson.accountNumber = CustomerAccount_details[i].ACCOUNT_NUMBER;
     outjson.accountNSC = CustomerAccount_details[i].NSC;
     outjson.accountType = CustomerAccount_details[i].ACCOUNT_TYPE;

     outjson.accountSubType = obj[0].SUB_TYPE;
     outjson.MARKET_CLASSIFICATION = obj[0].MARKET_CLASSIFICATION;
     outjson.bic = obj[0].BIC;
     outjson.iban = obj[0].IBAN;
     outjson.currency = obj[0].CURRENCY;
     out.push(outjson);
     outjson = {};
     }

  }
  """

    * set Result.DB_Response_AccountBalance = outjson