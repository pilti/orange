#This feature hits Customer profile service to fetch all details of a Single Account
@FS_Shakedown
Feature: DB customer profile request for a particular Account
  DB : CHAN_PROF.CHANNEL_ACCOUNT_PROFILE
  Input   :  Account_number, NSC
  Output  : Response for Single Account Info

  Background:
  # Parameters
    * def data = db_data
    * def ABTdb = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def CPdb = Java.type('CMA_Release.Entities_DB.CHANNEL_PROFILE_DB')
    * def ChannelDB = CHANNEL_PROFILE_DB_Conn
    * def ABT_DB = ABT_DB_Conn

  Scenario: GET customer account details using ChannelAccountProfile DB
    Given def accountNumber = data.accountNumber
    And def nsc = data.nsc
    When json CustomerAccount_details = CPdb.get_Channel_Profile_ColumnVals("WHERE ACCOUNT_NUMBER =" +accountNumber+ "AND NSC=" +nsc, "*",ChannelDB)
     # Define output json
    * def outjson = {}
  # Get ABT data for each Account in CHANNEL_PROFILE output and populate outjson
  # populate array out[] with each account
    * eval
  """
  var out = []

    if(CustomerAccount_details[0].ACCOUNT_NUMBER != null)
    {

     db_record = ABTdb.get_ABT_Details(CustomerAccount_details[0].NSC, CustomerAccount_details[0].ACCOUNT_NUMBER,  ABT_DB_Conn);
     var obj = JSON.parse(db_record);

     outjson.accountNumber = CustomerAccount_details[0].ACCOUNT_NUMBER;
     outjson.accountNSC = CustomerAccount_details[0].NSC;
     outjson.accountType = CustomerAccount_details[0].ACCOUNT_TYPE;

     outjson.accountSubType = obj[0].SUB_TYPE;
     outjson.MARKET_CLASSIFICATION = obj[0].MARKET_CLASSIFICATION;
     outjson.bic = obj[0].BIC;
     outjson.iban = obj[0].IBAN;
     outjson.currency = obj[0].CURRENCY;
     out.push(outjson);
     outjson = {};
     }

  """
    * set Result.DB_Response_AccountBalance = outjson