#This feature hits database for Account Balance

@DB_Shakedown
Feature: DB Balance enquiry - ABT database
  DB : PSD2.ABT_ACCOUNT_BALANCE ,  PSD2.ABT_ACCOUNT_DETAILS
  Input   :  nsc , accountNumber
  Output  : records with balance info from the ABT database

  Background:
   # Parameters
    * def data = db_data
    * def db_functions = Java.type('CMA_Release.Entities_DB.ABT_DB')
    * def ABT = ABT_DB_Conn

  Scenario: Hit ABT database for balance enquiry

    Given def nsc = data.nsc
    And def accountNumber = data.accountNumber
    When json ABT_bal_details = db_functions.get_ABT_Balance(nsc,accountNumber,ABT)
    And json ABT_acc_details = db_functions.get_ABT_Details(nsc,accountNumber,ABT)
    Then def Balance_DB_Response =
  """
  { "balance":
    { "LEDGER_BALANCE": ABT_bal_details[0].LEDGER_BALANCE,
       "EOD_BALANCE": ABT_bal_details[0].EOD_BALANCE,
       "CURRENCY": ABT_acc_details[0].CURRENCY
     }
  }


  """
    * set Result.DB_Response_AccountBalance = Balance_DB_Response