//function related to querying ABT Database
package CMA_Release.Entities_DB;
import com.google.gson.Gson;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.*;
import java.util.List;
import java.util.Map;

/**
 * Created by C962653 on 26/01/2018.
 */
public class ABT_DB {

    public static Connection connection = null;
    public static Statement stmt = null;
    public static PreparedStatement pstmt = null;

    //function to setup DB connection
    public static void getConnection(Map<String, String> ABT_DB_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+ABT_DB_Conn.get("URL")+":"+ABT_DB_Conn.get("Port")+":"+ABT_DB_Conn.get("SID"),ABT_DB_Conn.get("UserId"), ABT_DB_Conn.get("Password"));
            System.out.println("Connected database successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //function to check database connection
    public static String ConnectionCheck(Map<String, String> ABT_DB_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+ABT_DB_Conn.get("URL")+":"+ABT_DB_Conn.get("Port")+":"+ABT_DB_Conn.get("SID"),ABT_DB_Conn.get("UserId"), ABT_DB_Conn.get("Password"));
            System.out.println("Connected database successfully...");
            return "Connected database successfully";
        } catch (SQLException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    //To convert query result into map
    public static String resultSetToJson(Connection connection, String query) {
        List<Map<String, Object>> listOfMaps = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            listOfMaps = queryRunner.query(connection, query, new MapListHandler());
        } catch (SQLException se) {
            throw new RuntimeException("Couldn't query the database.", se);
        } finally {
            DbUtils.closeQuietly(connection);
        }
        System.out.print(new Gson().toJson(listOfMaps));
        return new Gson().toJson(listOfMaps);
    }

    // query to get balance from ABT DB
    public static String get_ABT_Balance(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_ACCOUNT_BALANCE where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "NSC=" + NSC);
    }

    //query to update balance on ABT DB
    public static String update_ABT_Balance(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn,String query) {
        getConnection(ABT_DB_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_ACCOUNT_BALANCE " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT INTRA_DAY_BALANCE, NSC, ACCOUNT_NUMBER FROM PSD2.ABT_ACCOUNT_BALANCE WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));
                System.out.print(", Intra day balance: " + rs.getString("INTRA_DAY_BALANCE"));
            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
               if(stmt!=null)
                    connection.close();
             }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }


    //Insert row in ABT
    public static String insert_ABT_Balance(Map<String, String> ABT_DB_Conn,String columns, String values) {
        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "insert into PSD2.ABT_ACCOUNT_BALANCE ("+columns+") values("+values+")";

            stmt.executeUpdate(sql);
           connection.commit();
            System.out.println("Insert Successful");
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }


    //delete balance from ABT
    public static String delete_ABT_Balance(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            String sql = "DELETE FROM PSD2.ABT_ACCOUNT_BALANCE where NSC = "+NSC+" AND ACCOUNT_NUMBER = "+ACCTNO;
            System.out.println("Creating statement...");
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Delete Successful...");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(pstmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }


    //fetch details from ABT
    public static String get_ABT_Details(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_ACCOUNT_DETAILS where ACCOUNT_NUMBER =" + ACCTNO + " AND " + "NSC=" + NSC);
    }

    public static String get_ABT_Details_ColumnValue(String NSC, String ACCTNO, String Col_name,Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select " +Col_name+" from PSD2.ABT_ACCOUNT_DETAILS  where ACCOUNT_NUMBER =" + ACCTNO + " AND " + "NSC=" + NSC);
    }


    //update details in ABT
    public static String update_ABT_Details(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn,String query) {
        getConnection(ABT_DB_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_ACCOUNT_DETAILS " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT * FROM PSD2.ABT_ACCOUNT_DETAILS WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));

            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

    //INSERT details in ABT details
    public static String insert_ABT_Details(Map<String, String> ABT_DB_Conn,String columns, String values) {

        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "insert into PSD2.ABT_ACCOUNT_DETAILS ("+columns+") values("+values+")";
            stmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Insert Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

    //delete details from ABT
    public static String delete_ABT_Details(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            String sql = "DELETE FROM PSD2.ABT_ACCOUNT_DETAILS where NSC = "+NSC+" AND ACCOUNT_NUMBER = "+ACCTNO;
            System.out.println("Creating statement...");
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Delete Successful...");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{

            try{
                if(pstmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

    //FETCH posted transactions
    public static String get_Posted_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn,String startDate, String endDate) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_POSTED_TRANSACTION where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "NSC=" + NSC + " AND POSTED_DATE >= '"+startDate+"' AND POSTED_DATE<= '"+endDate+"'");
    }

    //Method overloading for get_Posted_Transaction: start and end date not sent
    public static String get_Posted_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_POSTED_TRANSACTION where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "NSC=" + NSC);
    }


    //update posted transactions in ABT
    public static String update_Posted_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn,String query) {
        getConnection(ABT_DB_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_POSTED_TRANSACTION " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            //query: INTRA_DAY_BALANCE = "+balance+"
            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT * FROM PSD2.ABT_POSTED_TRANSACTION WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));
                // System.out.print(", Intra day balance: " + rs.getString("INTRA_DAY_BALANCE"));
            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{

            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }


    //insert posted transactions in ABT
    public static String insert_Posted_Transaction(Map<String, String> ABT_DB_Conn,String columns, String values) {
        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            ResultSet rs = null;
            String a = "SELECT MAX(ID) FROM PSD2.ABT_POSTED_TRANSACTION";
            stmt = connection.createStatement();
            rs= stmt.executeQuery(a);
            //Display values
            rs.next();
            int id = rs.getInt(1)+1;
            System.out.println("ID value max is:"+id);
            id++;
            System.out.println("old values="+values);
            values = values.replaceFirst("000", String.valueOf(id));
            System.out.println("new values="+values);
            String sql = "insert into PSD2.ABT_POSTED_TRANSACTION ("+columns+") values("+values+")";
            stmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Insert Successful");
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }finally{

            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    //delete posted transactions
    public static String delete_Posted_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn, String query) {
        getConnection(ABT_DB_Conn);
        try{
            //STEP 4: Execute a query
            String sql = "DELETE FROM PSD2.ABT_POSTED_TRANSACTION where NSC = "+NSC+" AND ACCOUNT_NUMBER = "+ACCTNO+" AND "+query;
            System.out.println("Creating statement...");
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Delete Successful...");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(pstmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    public static String get_Cached_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn, String startDate, String endDate) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_CACHED_TRANSACTION where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "NSC=" + NSC + " AND TXN_DATE >= '"+startDate+"' AND TXN_DATE<= '"+endDate+"'");
    }

    //Method overloading for get_Cached_Transaction: start and end date not sent

    public static String get_Cached_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn) {
        getConnection(ABT_DB_Conn);
        return resultSetToJson(connection, "Select * from PSD2.ABT_CACHED_TRANSACTION where ACCOUNT_NUMBER =" + ACCTNO + "AND " + "NSC=" + NSC);
    }

    public static String update_Cached_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn,String query) {
        getConnection(ABT_DB_Conn);

        try{
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_CACHED_TRANSACTION " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            System.out.println("Update Successful");
            // to see the updated records
            sql = "SELECT * FROM PSD2.ABT_CACHED_TRANSACTION WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));
            }
            rs.close();

            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

    public static String insert_Cached_Transaction(Map<String, String> ABT_DB_Conn,String columns, String values) {
        getConnection(ABT_DB_Conn);
        try{
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "insert into PSD2.ABT_CACHED_TRANSACTION ("+columns+") values("+values+")";

            stmt.executeUpdate(sql);
            connection.commit();

            System.out.println("Insert Successful");
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    public static String delete_Cached_Transaction(String NSC, String ACCTNO, Map<String, String> ABT_DB_Conn, String query) {
        getConnection(ABT_DB_Conn);
        try{

            String sql = "DELETE FROM PSD2.ABT_CACHED_TRANSACTION where NSC="+NSC+" AND ACCOUNT_NUMBER="+ACCTNO+" AND "+query;
            System.out.println("Creating statement...");
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Delete Successful...");
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();

        }finally{
            try{
                if(pstmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

    public static String update_Branch_Status(String NSC,Map<String, String> ABT_DB_Conn, String query)
    {
        getConnection(ABT_DB_Conn);

        try{

            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_BRANCH_STATUS " +
                    "SET "+query+" WHERE NSC ='"+NSC+"'";
            stmt.executeUpdate(sql);
            // to see the updated records
            System.out.println("Update Successful");
            return "Update Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "Update failed";
    }
}