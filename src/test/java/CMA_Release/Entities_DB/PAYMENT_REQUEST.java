package CMA_Release.Entities_DB;


import com.google.gson.Gson;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import java.sql.*;
import java.util.List;
import java.util.Map;
/**
 * Created by C965116 on 06/08/2018.
 */
public class PAYMENT_REQUEST {

    public static Connection connection = null;
    public static Statement stmt = null;
    public static PreparedStatement pstmt = null;

    public static void getConnection(Map<String, String>  PSD2_PISP_PID_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+ PSD2_PISP_PID_Conn.get("URL")+":"+ PSD2_PISP_PID_Conn.get("Port")+":"+ PSD2_PISP_PID_Conn.get("SID"), PSD2_PISP_PID_Conn.get("UserId"),  PSD2_PISP_PID_Conn.get("Password"));
            System.out.println(connection);
            System.out.println("Connected database successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String resultSetToJson(Connection connection, String query) {
        List<Map<String, Object>> listOfMaps = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            listOfMaps = queryRunner.query(connection, query, new MapListHandler());
        } catch (SQLException se) {
            throw new RuntimeException("Couldn't query the database.", se);
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return new Gson().toJson(listOfMaps);
    }

    public static String get_PaymentDetails(String PYMT_ID,  Map<String, String> PSD2_PISP_PID_Conn) {
        getConnection(PSD2_PISP_PID_Conn);
        return resultSetToJson(connection, "Select * FROM PSD2PID.PAYMENT_REQUEST where PYMT_ID = '" + PYMT_ID + "'");
    }

    //update details in ABT
    public static String update_PaymentRequest_Details(String NSC, String ACCTNO, Map<String, String> PSD2_PISP_PID_Conn,String query) {
        getConnection(PSD2_PISP_PID_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.ABT_ACCOUNT_DETAILS " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT * FROM PSD2.ABT_ACCOUNT_DETAILS WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));

            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }

}
