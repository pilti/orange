//Functions to update transactions in ABT
package CMA_Release.Entities_DB;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by C962653 on 12/01/2018.
 */
public class UpdateABTTransactions {

    public static Connection connection = null;
    public static Statement stmt = null;

    public static void getConnection(Map<String, String> ABT_DB_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+ABT_DB_Conn.get("URL")+":"+ABT_DB_Conn.get("Port")+":"+ABT_DB_Conn.get("SID"),ABT_DB_Conn.get("UserId"), ABT_DB_Conn.get("Password"));
            System.out.println("Connected database successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String deletePostedTransactions(String accountNumber,Map<String, String> ABT_DB_Conn)
    {
        String nsc= accountNumber.substring(0,6);
        String acNo= accountNumber.substring(6,14);
        try{
            //STEP 2: Register JDBC driver
            getConnection(ABT_DB_Conn);
            System.out.println("Connected database successfully...");
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "DELETE FROM PSD2.ABT_POSTED_TRANSACTION where NSC = '"+nsc+"' and ACCOUNT_NUMBER ='"+acNo+"'";
            stmt.executeUpdate(sql);
            connection.commit();
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    public static String deleteCachedTransactions(String accountNumber,Map<String, String> ABT_DB_Conn)
    {
        String nsc= accountNumber.substring(0,6);
        String acNo= accountNumber.substring(6,14);
        try{
            //STEP 2: Register JDBC driver
            getConnection(ABT_DB_Conn);
            System.out.println("Connected database successfully...");
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "DELETE FROM PSD2.ABT_CACHED_TRANSACTION where NSC = '"+nsc+"' and ACCOUNT_NUMBER ='"+acNo+"'";
            stmt.executeUpdate(sql);
            connection.commit();
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }


    public static String updatePostedTransactions(String accountNumber, String amount, String date,String txnCode, Map<String, String> ABT_DB_Conn)
    {
        String nsc= accountNumber.substring(0,6);
        String acNo= accountNumber.substring(6,14);
        try{
            //STEP 2: Register JDBC driver
            getConnection(ABT_DB_Conn);
            System.out.println("Connected database successfully...");
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
                stmt = connection.createStatement();
                String sql = "INSERT INTO PSD2.ABT_POSTED_TRANSACTION (ID, NSC, ACCOUNT_NUMBER, POSTED_DATE, AMOUNT, TXN_TYPE, PICN, TXN_CODE, SOURCE_CODE, SERIAL_NUMBER, EURO_AMOUNT, NARRATIVE_TXT1, EOD_BALANCE, POSTED_TIMESTAMP) VALUES ('2', '"+nsc+"', '"+acNo+"','"+date+"','"+amount+"', '1', '11111111111111', '"+txnCode+"', 'Y', '0', '1.01', 'XSP05.67', '2.01', TO_TIMESTAMP('2017-05-12 08:05:46.644191000', 'YYYY-MM-DD HH24:MI:SS.FF'))";
                stmt.executeUpdate(sql);
            connection.commit();
            System.out.println("Commit successful...");
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    public static String addPostedTransactions(String accountNumber,int count, String date, String txnCode,Map<String, String> ABT_DB_Conn)
    {
        String nsc= accountNumber.substring(0,6);
        String acNo= accountNumber.substring(6,14);

        try{
            String accNumber = nsc + acNo;
            deletePostedTransactions(accNumber,ABT_DB_Conn);
            deleteCachedTransactions(accNumber,ABT_DB_Conn);
            getConnection(ABT_DB_Conn);
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
            Calendar c1 = Calendar.getInstance();
            try {
                c1.setTime(formatter.parse(date));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // number of days to add
            String d1 = formatter.format(c1.getTime());
            int transactionscount;

            System.out.println("Connected database successfully...");
            //STEP 4: Execute a query
            ResultSet rs = null;
            System.out.println("Creating statement...");
            //To count available transactions in database
            String sql1 = "SELECT COUNT(*) FROM PSD2.ABT_POSTED_TRANSACTION WHERE ACCOUNT_NUMBER ='" + acNo + "' AND NSC ='" + nsc + "'";
            stmt = connection.createStatement();
            rs= stmt.executeQuery(sql1);
            //Display values
            rs.next();
            transactionscount = rs.getInt(1);
            rs.close();
            // TO GET HIGHEST VALUE OF id FIELD TO VALIDATE PRIMARY KEY CONSTRAINT
            String a = "SELECT MAX(ID) FROM PSD2.ABT_POSTED_TRANSACTION";
            stmt = connection.createStatement();
            rs= stmt.executeQuery(a);
            //Display values
            rs.next();
            int id = rs.getInt(1)+1;
            System.out.println("ID value max is:"+id);

            rs.close();
            d1 = formatter.format(c1.getTime());

            //delete old transactions of the customer to manae the transactions count to required value


            int c = count;
            c1.add(Calendar.YEAR, -1);
            int d = c + 10;
            while (c > 0)
            {
                stmt = connection.createStatement();
                c1.add(Calendar.DAY_OF_MONTH, 1);  // number of days to add
                d1 = formatter.format(c1.getTime());
                String sql = "INSERT INTO PSD2.ABT_POSTED_TRANSACTION (ID, NSC, ACCOUNT_NUMBER, POSTED_DATE, AMOUNT, TXN_TYPE, PICN, TXN_CODE, SOURCE_CODE, SERIAL_NUMBER, EURO_AMOUNT, NARRATIVE_TXT1, EOD_BALANCE, POSTED_TIMESTAMP) VALUES ('"+id+"', '" + nsc + "', '" + acNo + "','" + d1 + "','" + d + "', '1', '11111111111111', '"+txnCode+"', 'Y', '0', '1.01', 'XSP05.67', '2.01', '"+d1.toString()+" 08:05:46.644191000')";
                stmt.executeUpdate(sql);
                connection.commit();
                connection.setAutoCommit(true);
                c--;
                d = -d;
                id++;
            }
            return "Success";
        }catch(SQLException se){
            se.printStackTrace();        }catch(Exception e){
            e.printStackTrace();        }
        finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";
    }

    public static String addPostedTransactionsForCR20(String accountNumber,Map<String, String> dates, String txnCode,Map<String, String> ABT_DB_Conn)
    {
        String nsc= accountNumber.substring(0,6);
        String acNo= accountNumber.substring(6,14);
        try {
        //STEP 1: Register JDBC driver
        getConnection(ABT_DB_Conn);
        System.out.println("Connected database successfully...");

        ResultSet rs = null;
        System.out.println("Creating statement...");
        String accNumber = nsc + acNo;

        String a = "SELECT MAX(ID) FROM PSD2.ABT_POSTED_TRANSACTION";
        stmt = connection.createStatement();
        rs= stmt.executeQuery(a);
        //Display values
        rs.next();
        int id = rs.getInt(1)+1;
        System.out.println("ID value max is:"+id);
        rs.close();

        //delete old transactions of the customer to manae the transactions count to required value
        deletePostedTransactions(accNumber,ABT_DB_Conn);
        deleteCachedTransactions(accNumber,ABT_DB_Conn);
        getConnection(ABT_DB_Conn);
        int c = 10;
        String txndate;
        int i;
        stmt = connection.createStatement();
        for(i = 0; i<dates.size();i++)
        {
            txndate = dates.get("date"+i+"");
            String sql = "INSERT INTO PSD2.ABT_POSTED_TRANSACTION (ID, NSC, ACCOUNT_NUMBER, POSTED_DATE, AMOUNT, TXN_TYPE, PICN, TXN_CODE, SOURCE_CODE, SERIAL_NUMBER, EURO_AMOUNT, NARRATIVE_TXT1, EOD_BALANCE, POSTED_TIMESTAMP) VALUES ('"+id+"', '" + nsc + "', '" + acNo + "','" + txndate + "','" + (c + 10) + "', '1', '11111111111111', '"+txnCode+"', 'Y', '0', '1.01', 'XSP05.67', '2.01', '"+txndate+" 08:05:46.644191000')";
            stmt.executeUpdate(sql);
            c++;
            id++;
        }
            connection.commit();
            connection.setAutoCommit(true);

        connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return "fail";
        }
        return "Success";
    }
}