//functions to query channel Profile DB
package CMA_Release.Entities_DB;

import com.google.gson.Gson;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.*;
import java.util.List;
import java.util.Map;

/**
 * Created by C962653 on 30/01/2018.
 */
public class CHANNEL_PROFILE_DB {

    public static Connection connection = null;
    public static Statement stmt = null;
    public static PreparedStatement pstmt = null;


    //function to setup connection with Channel_Profile Db
    public static void getConnection(Map<String, String> CHANNEL_PROFILE_DB_Conn) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@"+CHANNEL_PROFILE_DB_Conn.get("URL")+":"+CHANNEL_PROFILE_DB_Conn.get("Port")+":"+CHANNEL_PROFILE_DB_Conn.get("SID"),CHANNEL_PROFILE_DB_Conn.get("UserId"), CHANNEL_PROFILE_DB_Conn.get("Password"));
            System.out.println("Connected database successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //function to convert query result into Json
    public static String resultSetToJson(Connection connection, String query) {
        List<Map<String, Object>> listOfMaps = null;
        try {
            QueryRunner queryRunner = new QueryRunner();
            listOfMaps = queryRunner.query(connection, query, new MapListHandler());
        } catch (SQLException se) {
            throw new RuntimeException("Couldn't query the database.", se);
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return new Gson().toJson(listOfMaps);
    }

    //wrapper function to call above two functions
    public static String get_Channel_Profile(String userId, Map<String, String> CHANNEL_PROFILE_DB_Conn) {
        getConnection(CHANNEL_PROFILE_DB_Conn);
        return resultSetToJson(connection, "SELECT * FROM CHAN_PROF.CHANNEL_ACCOUNT_PROFILE WHERE CHANNEL_ID = "+userId);
    }

    //query CHANNEL_PROFILE_DB_Conn
    public static String get_Channel_Profile_ColumnVals(String query, String colname, Map<String, String> CHANNEL_PROFILE_DB_Conn) {
        getConnection(CHANNEL_PROFILE_DB_Conn);
        System.out.println("SELECT " + colname + " FROM CHAN_PROF.CHANNEL_ACCOUNT_PROFILE "+query);
        return resultSetToJson(connection, "SELECT " + colname + " FROM CHAN_PROF.CHANNEL_ACCOUNT_PROFILE "+query);
    }

    public static String update_Channel_Profile(String NSC, String ACCTNO, Map<String, String> CHANNEL_PROFILE_DB_Conn,String query) {
        getConnection(CHANNEL_PROFILE_DB_Conn);

        try{
            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = connection.createStatement();
            String sql = "UPDATE PSD2.CHANNEL_PROFILE_ACCOUNT_DETAILS " +
                    "SET "+query+" WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";

            stmt.executeUpdate(sql);
            // to see the updated records
            sql = "SELECT * FROM PSD2.CHANNEL_PROFILE_ACCOUNT_DETAILS WHERE ACCOUNT_NUMBER ='"+ACCTNO+"' AND NSC ='"+NSC+"'";
            ResultSet rs = stmt.executeQuery(sql);
            //Display values
            while(rs.next()) {
                System.out.print("NSC: " + rs.getInt("NSC"));
                System.out.print(", Account Number: " + rs.getString("ACCOUNT_NUMBER"));

            }
            rs.close();
            System.out.println("Update Successful");
            return "Success";
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();

        }finally{
            try{
                if(stmt!=null)
                    connection.close();
            }catch(SQLException se){
            }
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return "fail";

    }


}
