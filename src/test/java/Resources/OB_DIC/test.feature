Feature: test OB json

  Background:
    * def ob = read('OB.json')


  Scenario: Get URL of Open banking Directory
    * print ob.OB_Directory.URL

  Scenario: Get SSA path for a given Software Statement 6rY9le1k2WYd7yyczHC9xu

   * def getOBJ1 =
    """
        function(organisations,id)
        {
              var org;
              var i;
              for(org in organisations)
              {
              for(i in organisations[org].Software_Statements)
              {
                if(organisations[org].Software_Statements[i].Software_STMT.ID == id)
                  return (organisations[org].Software_Statements[i].Software_STMT);
              }
              }
              return "not Found";
        }
    """
    * def Software_Statement = getOBJ1(ob.OB_Directory.Organisations,'6rY9le1k2WYd7yyczHC9xu')
    * print Software_Statement

  Scenario: Get SSA path for a given Software Statement 6rY9le1k2WYd7yyczHC9xu using javascript function

   * def input = {organisations:'#(ob.OB_Directory.Organisations)',id:'Template'}
    * def SS = call read("classpath:CMA_Release/JavaScript_Lib/getSoftwareStmt.js") input
    * print SS


  Scenario:
    * set getSStemplate.Clinet_ID = input.ClientID
    * set getSStemplate.Clinet_ID = input.Organisation
    * set getSStemplate.Path = path.outfolderpath
    * print getSStemplate