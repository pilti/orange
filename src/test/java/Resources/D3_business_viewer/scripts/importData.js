function importData()
{
 d3.json("treeData.json", function (data)
 {
    var total = data.Gpass + data.Gfail;
    var cma_var = data.CMA_Ver
    var runner = data.Runner
    var pp = (data.Gpass/total * 100).toFixed(1);
    var ff = (data.Gfail/total * 100).toFixed(1);
    var output = "Release " + cma_var + " Test : "+ runner + " Total Test Cases : " +  total + ";   " + "Pass : " + data.Gpass + "    (" + pp + "%);" + "  Fail : " + data.Gfail + "  (" +ff+"%)";
    document.getElementById("Out").innerHTML = output;
    //document.getElementById("R").innerHTML = "Release : " + cma_var;
 });
}