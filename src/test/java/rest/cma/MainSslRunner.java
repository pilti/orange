package rest.cma;

import org.junit.Test;

public class MainSslRunner {
    
    @Test
    public void testMain() {
        Main.main(new String[]{"-m", "src/test/java/rest/cma/JWT_REST.feature", "-p", "8443", "-s"});
    }
    
}
