Feature: REST end point to Generate JWT token and Consent URL for specific environment
  Request method : POST, GET for info
  Body : Refer body.json

  Configurations through config script
  ServerURL and port

  Background:
  #Function returns signed JWT
    * def FunctionJwt = Java.type('CMA_Release.Java_Lib.Jwt')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')

  Scenario: pathMatches('/oauth/as/token.oauth2') && methodIs('post')

    Given def return =
    """
   {
      access_token: '3VomtM1MQz19gsB8glrN3Q',
      token_type: 'Bearer',
      expires_in: 300
   }
    """
    Then def response = return
    And def responseStatus = 200
    And print "CC Token generated"

  Scenario: pathMatches('/1/api/open-banking/v1.1/account-requests') && methodIs('post')

    * def ARID = "3ba70185-f1ef-4709-8dd9-d3a004d20a01"
    * def st = "AwaitingAuthorisation"

    Given def return =
    """
    {
        "Data": {
        "AccountRequestId": "3ba70185-f1ef-4709-8dd9-d3a004d20a01",
        "Status": "AwaitingAuthorisation",
        "CreationDateTime": "2018-07-26T05:11:31+00:00",
        "Permissions": [
          "ReadAccountsBasic",
          "ReadAccountsDetail",
          "ReadBalances",
          "ReadBeneficiariesBasic",
          "ReadBeneficiariesDetail",
          "ReadDirectDebits",
          "ReadProducts",
          "ReadStandingOrdersBasic",
          "ReadStandingOrdersDetail",
          "ReadTransactionsBasic",
          "ReadTransactionsCredits",
          "ReadTransactionsDebits",
          "ReadTransactionsDetail"
        ],
        "ExpirationDateTime": "2019-04-19T14:00:00+01:00",
        "TransactionFromDateTime": "2017-03-01T00:00:00+00:00",
        "TransactionToDateTime": "2018-01-30T14:00:00+00:00"
      },
      "Risk": {
      },
      "Links": {
        "Self": "/account-requests/3ba70185-f1ef-4709-8dd9-d3a004d20a01"
      },
      "Meta": {
        "TotalPages": 1
       }
     }
    """
    Then def response = return
    And def responseStatus = 200
    And print "CC Token generated"

  Scenario: End point Not Found
    # catch-all
    * def responseStatus = 404
    * def responseHeaders = { 'Content-Type': 'text/html; charset=utf-8' }
    * def response = <html><body>Not Found</body></html>

