package rest.cma;

import org.junit.Test;

public class MainTestRunner {
    
    @Test
    public void testMain() {

        System.setProperty("karate.config", "src/test/java/karate-config.js");
        Main.main( new String[]{"-t", "src/test/java/rest/cma/JWT_REST.feature"});
    }
    
}
