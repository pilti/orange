Feature: REST end point to Generate JWT token and Consent URL for specific environment
  Request method : POST, GET for info
  Body : Refer body.json

  Configurations through config script
  ServerURL and port

  Background:
    * def FunctionJwt = Java.type('CMA_Release.Java_Lib.Jwt')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def fileReader = Java.type('CMA_Release.Java_Lib.ReadTextFile')


  Scenario: pathMatches('/orangeFMW/psd2_cma/activeTPP')&& methodIs('get')
    * def restenv = requestParams.env[0]
    * print restenv
#    * def restRole = requestParams.role[0] refreshToken
#    * print restRole
    * call read('classpath:karate-config.js')
    * def response = karate.pretty(active_tpp.AISP)

  Scenario: pathMatches('/orangeFMW/psd2_cma/refreshToken')&& methodIs('get')
    * def restenv = requestParams.env[0]
    * call read('classpath:karate-config.js')
    * def response = karate.pretty(refreshToken.refresh_token)

  Scenario: pathMatches('/v1/binary/download')
    * def responseHeaders = { 'Content-Type': 'application/octet-stream', 'Content-Disposition': 'attachment; filename="output.pdf"'}
    * def Utils = Java.type('CMA_Release.Java_Lib.PdfToByteArray')
    * def response = Utils.getByteArray()

  Scenario: pathMatches('/orangeFMW/psd2_cma/cctoken') && methodIs('get')
    * def restenv = requestParams.env[0]
    * def restRole = requestParams.role[0]
    * print requestParams.role
    * call read('classpath:karate-config.js')
    * def apiApp = new apiapp()
    * json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Result.out = apiApp.Access_Token_CCG.responseStatus
    * def out = {}
    * set out.response = Result
    #* set out.consolelog = fileReader.ReadFile('./target/Orange.log')
    Then def response = karate.pretty(out.response.ClientCredentialGrant.Output.response)

  Scenario: pathMatches('/orangeFMW/psd2_cma/cctoken') && methodIs('post')
    * def restenv = request.Environment
    * def restRole = request.Role
    * call read('classpath:karate-config.js')
    * def apiApp = new apiapp()
    * json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = 'AISP_PISP'
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts payments'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Result.out = apiApp.Access_Token_CCG.responseStatus
    * def out = {}
    * set out.response = Result
    #* set out.consolelog = fileReader.ReadFile('./target/Orange.log')
    Then def response = out

  Scenario: pathMatches('/orangeFMW/psd2_cma/testcaserun') && methodIs('get')
    * def testcase =  Java.type('CMA_Release.Test_Set.AISP_OIDC.CCToken.fullrun')
    * def r = testcase.test1()
    * def response = "Test Run Completed"


  Scenario: pathMatches('/v1/jwt') && methodIs('post')
  #Generating JWT for SIT
    * def AuthUrl = 'https://auth.boitest.net' + '/oauth/as/authorization.oauth2?'

    Given string header = request.header
    And string payload = request.payload
    * json Input = request.payload
    When def auth = FunctionJwt.Create_JWT(header,payload,request.key)
    And set Input.jwt = auth
    And def constructurl = AuthUrl + "client_id=" + Input.client_id + "&response_type=" + Input.response_type + "&scope=" + Input.scope + "&state=" + Input.state + "&nonce=" + Input.nonce + "&redirect_uri=" + Input.redirect_uri + "&request=" + Input.jwt
    And json jwt = {"JToken": '#(auth)',"ConsentURL":'#(constructurl)',"KeyUsed":'#(request.key)'}
    Then def response = jwt
    And def responseStatus = 200
    And print "JWT Generated Successfully"

  Scenario: pathMatches('/v1/jwt/uat') && methodIs('post')
  #Generating JWT for UAT
    * def AuthUrl = 'https://auth.u2.psd2.boitest.net' + '/oauth/as/authorization.oauth2?'

    Given string header = request.header
    And string payload = request.payload
    * json Input = request.payload
    When def auth = FunctionJwt.Create_JWT(header,payload,request.key)
    And set Input.jwt = auth
    And def constructurl = AuthUrl + "client_id=" + Input.client_id + "&response_type=" + Input.response_type + "&scope=" + Input.scope + "&state=" + Input.state + "&nonce=" + Input.nonce + "&redirect_uri=" + Input.redirect_uri + "&request=" + Input.jwt
    And json jwt = {"JToken": '#(auth)',"ConsentURL":'#(constructurl)',"KeyUsed":'#(request.key)'}
    Then def response = jwt
    And def responseStatus = 200
    And print "JWT Generated Successfully"

  Scenario: pathMatches('/v1/jwt/preProd') && methodIs('post')
   #Generating JWT for preProd
    * def AuthUrl = 'https://auth.prep.openapi.boicloudtest.net' + '/oauth/as/authorization.oauth2?'

    Given string header = request.header
    And string payload = request.payload
    * json Input = request.payload
    When def auth = FunctionJwt.Create_JWT(header,payload,request.key)
    And set Input.jwt = auth
    And def constructurl = AuthUrl + "client_id=" + Input.client_id + "&response_type=" + Input.response_type + "&scope=" + Input.scope + "&state=" + Input.state + "&nonce=" + Input.nonce + "&redirect_uri=" + Input.redirect_uri + "&request=" + Input.jwt
    And json jwt = {"JToken": '#(auth)',"ConsentURL":'#(constructurl)',"KeyUsed":'#(request.key)'}
    Then def response = jwt
    And def responseStatus = 200
    And print "JWT Generated Successfully"

  Scenario: pathMatches('/v1/jwt/Prod') && methodIs('post')
 #Generating JWT for preProd
    * def AuthUrl = 'https://auth.obapi.bankofireland.com' + '/oauth/as/authorization.oauth2?'

    Given string header = request.header
    And string payload = request.payload
    * json Input = request.payload
    When def auth = FunctionJwt.Create_JWT(header,payload,request.key)
    And set Input.jwt = auth
    And def constructurl = AuthUrl + "client_id=" + Input.client_id + "&response_type=" + Input.response_type + "&scope=" + Input.scope + "&state=" + Input.state + "&nonce=" + Input.nonce + "&redirect_uri=" + Input.redirect_uri + "&request=" + Input.jwt
    And json jwt = {"JToken": '#(auth)',"ConsentURL":'#(constructurl)',"KeyUsed":'#(request.key)'}
    Then def response = jwt
    And def responseStatus = 200
    And print "JWT Generated Successfully"


  Scenario: pathMatches('/v1/jwt') && methodIs('get')
    # GET Method response
    * def response = 'Use POST to get JWT token. Body is required with header, payload and key'
    * print "Get Method used"

  Scenario: pathMatches('/v1/consenturl') && methodIs('get')
    * print "Umesh"
    * def k = karate.call(classpath:CMA_Release\Entities_API\AISP_Resources\OIDC\Consent_Screen_Reusable.feature")
    * def response = 'Use POST to get JWT token. Body is required with header, payload and key'
    * print "Get Method used"

  Scenario: End point Not Found
    # catch-all
    * def responseStatus = 404
    * def responseHeaders = { 'Content-Type': 'text/html; charset=utf-8' }
    * def response = <html><body>Not Found</body></html>

