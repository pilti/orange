Feature: REST end points for Orange framework

  Background:
    * def FunctionJwt = Java.type('CMA_Release.Java_Lib.Jwt')
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def fileReader = Java.type('CMA_Release.Java_Lib.ReadTextFile')
  # Endpoint check
  Scenario: pathMatches('/orangeFMW/psd2_cma/connectiontest')&& methodIs('get')
    * print "Connection Test endpoint reached :)"
    * def response = "Connected to Orange Server"

  #Log request
  Scenario: pathMatches('/orangeFMW/psd2_cma/getlogs')&& methodIs('get')
    * print "Logs Requested :)"
    * def response = fileReader.ReadFile('./target/Orange.log')

  # Get active TPP list - parameters -> env = Environment;  role = TPP role
  Scenario: pathMatches('/orangeFMW/psd2_cma/activeTPP')&& methodIs('get')
    * def restenv = requestParams.env[0]
    * def restRole = requestParams.role[0]
    * call read('classpath:karate-config.js')
    * def response = karate.pretty(active_tpp.get(restRole))

  # Get active RefreshTokenList - parameters -> env = Environment;  role = TPP role
  Scenario: pathMatches('/orangeFMW/psd2_cma/RefreshTokenList')&& methodIs('get')
    * def restenv = requestParams.env[0]
    * call read('classpath:karate-config.js')

    * def fileName = 'RefreshToken.properties'
    * def name = 'reftokens'
    * def Datapath = './src/test/java/Resources/'
    * json profile = RWPropertyFile.ReadFile(Datapath,fileName,name)

    * eval var preProd_arr = []
    * eval var SIT_arr = []
    * eval var UAT_arr = []
    * eval var PIT_arr = []
    * eval
    """
      for(var i = 0 ;i < profile.size(); i++)
      {
           if(profile[i].env == "preProd")
             preProd_arr.push(profile[i].RT)

           if(profile[i].env == "SIT")
              SIT_arr.push(profile[i].RT)

           if(profile[i].env == "UAT")
              UAT_arr.push(profile[i].RT)

          if(profile[i].env == "PIT")
              PIT_arr.push(profile[i].RT)

      }
     """
    #Need to read from the list and send as array, Now just send below token
    * def res = null
    * eval
    """
      if(restenv == "preProd"){res = preProd_arr}
      if(restenv == "SIT"){res = SIT_arr}
      if(restenv == "UAT"){res = UAT_arr}
      if(restenv == "PIT"){res =PIT_arr}
    """
    * def response = karate.pretty(res)

  # Get GetAuthTokenFromRefreshToken - parameters -> env = Environment;  RefreshToken =  ActiveRefreshToken ; role = TPP role
  Scenario: pathMatches('/orangeFMW/psd2_cma/GetAuthTokenFromRefreshToken')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def RefreshToken = requestParams.RefreshToken[0]
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)

    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.refresh_token = RefreshToken
    And set Application_Details $.grant_type = 'refresh_token'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Access_Token_RTG(Application_Details)
    Then def response = karate.pretty(Result.AccessTokenRTG)

  # Get Clent Credential token - parameters -> env = Environment;  role = TPP role
  Scenario: pathMatches('/orangeFMW/psd2_cma/cctoken') && methodIs('get')

    # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'POST'
    And set Application_Details $.scope = 'openid accounts'
    And set Application_Details $.grant_type = 'client_credentials'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    When call apiApp.Access_Token_CCG(Application_Details)
    Then def response = karate.pretty(Result)

  # Get SetupAccountRequestID - parameters -> permission = permissions env = Environment;  CMA_Rel =  CMA Release ; role = TPP role
  # T_Start = Transaction Start date, T_End = Transaction End date, Expiry = Permission Expiry date
  Scenario: pathMatches('/orangeFMW/psd2_cma/SetupAccountRequestID')&& methodIs('get')
    Given def restenv = requestParams.env[0]
      And def restRole = requestParams.role[0]
      And def CMARel = requestParams.CMARel[0]
      And def Expiry = requestParams.Expiry[0]
      And def Tran_start = requestParams.T_Start[0]
      And def Tran_End = requestParams.T_End[0]
      And eval karate.properties['CMA_Rel_Ver'] = CMARel
      And json j = requestParams.permission[0]
    When call read('classpath:karate-config.js')
      And set permissions.Data.Permissions = j
    * print permissions
    * print Expiry
      And set permissions.Data.ExpirationDateTime = (Expiry.replace(' ','+'))
      And set permissions.Data.TransactionFromDateTime = (Tran_start.replace(' ','+'))
      And set permissions.Data.TransactionToDateTime = (Tran_End.replace(' ','+'))
    * print permissions
    Then def apiApp = new apiapp()
      And json Result = {}

    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.grant_type = "client_credentials"
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'POST'
    When call apiApp.Access_Token_CCG(Application_Details)
    And set Application_Details $.access_token = apiApp.Access_Token_CCG.response.access_token
    And set Application_Details $.token_type = 'Bearer'
    * print Application_Details
    When call apiApp.Account_Request_Setup(Application_Details)
    And def statusCode = apiApp.Account_Request_Setup.response
    * def response = karate.pretty(Result.AccountRequestSetup)

  # Get Consent URL - env = Environment; role = TPP role ; AccountRequestId = ActiveAccReqID
  Scenario: pathMatches('/orangeFMW/psd2_cma/consentURL')&& methodIs('get')
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccountRequestId = requestParams.AccountRequestId[0]
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    * def Result = {}

    Given json Application_Details = active_tpp.get(restRole)
    * print karate.pretty(Application_Details)
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    * print path
    When call apiApp.CreateRequestObject(Application_Details)
    Then def request1 = apiApp.CreateRequestObject.jwt.request
    * print request1
    Given set Application_Details $.jwt = request1
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl
    * def response = karate.pretty(consenturl)

  # Get Consent URL - env = Environment; role = TPP role ; AccountRequestId = ActiveAccReqID ; UserID = B365USER ; OTP = B365OTP
  Scenario: pathMatches('/orangeFMW/psd2_cma/consentAuthoriseAuto')&& methodIs('get')
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def User = requestParams.userId[0]
    And def Password = requestParams.password[0]
    And def AccountRequestId = requestParams.AccountRequestId[0]
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And def webApp = new webapp()
    * def Result = {}

    Given json Application_Details = active_tpp.get(restRole)
    * print karate.pretty(Application_Details)
    And set ReqObjIP $.header.kid = Application_Details.kid
    And set ReqObjIP $.payload.iss = Application_Details.client_id
    And set ReqObjIP $.payload.client_id = Application_Details.client_id
    And set ReqObjIP $.payload.redirect_uri = Application_Details.redirect_uri
    And set ReqObjIP $.payload.claims.userinfo.openbanking_intent_id.value = AccountRequestId
    And set ReqObjIP $.payload.claims.id_token.openbanking_intent_id.value = AccountRequestId
    And set ReqObjIP $.payload.scope = 'openid accounts'
    And set Application_Details $.scope = 'openid accounts'
    And def path = stmtpath(Application_Details) + 'signin_private.key'
    And set Application_Details.path = path

    * print path
    When call apiApp.CreateRequestObject(Application_Details)
    Then def request1 = apiApp.CreateRequestObject.jwt.request
    * print request1
    Given set Application_Details $.jwt = request1
    And set Application_Details $.response_type = 'code id_token'
    And set Application_Details $.state = 'af0ifjsldkj'
    And set Application_Details $.nonce = 'n-0S6_WzA2Mj'
    When call apiApp.ConstructAuthReqUrl(Application_Details)
    Then def consenturl = apiApp.ConstructAuthReqUrl.Curl.consenturl

    And set Application_Details $.consenturl = consenturl
    And def consent_redirecturi = apiApp.ConstructAuthReqUrl.redirect_uri

    Given def c = call  webApp.driver1 = webApp.start1(default_browser)
    When def perform = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP').launchConsentURL(webApp,consenturl)
    And match perform.Status == 'Pass'
    And def perform = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP').useKeyCodeApp(webApp)
    Then match perform.Status == 'Pass'

    Given def data = {"usr":'#(User)',"otp":'#(Password)',"action":"continue","Confirmation":"Yes"}
    When def perform = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP').scaLogin(webApp,data)
    And match perform.Status == 'Pass'
    And def perform = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP').selectAllAccounts(webApp,data)
    And match perform.Status == 'Pass'
    And set data $.SelectedAccounts = perform.SelectedAccounts
    And set Application_Details.selectedAccounts = perform.SelectedAccounts
    And def perform = Java.type('CMA_Release.Entities_UI.Selenium_Scripts.Consent_AISP.Entities.OIDC_AISP').reviewConfirm(webApp,data)
    * print "****************************************************"
    And print perform.Action.Authcode
    Then def response = karate.pretty(perform.Action.Authcode)

   # Get GetAuthTokenFromAuthCode - parameters -> env = Environment;  AuthCode =  ActiveAuthCode ; role = TPP role
  Scenario: pathMatches('/orangeFMW/psd2_cma/GetAuthTokenFromAuthCode')&& methodIs('get')

    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AuthCode = requestParams.AuthCode[0]
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    * def Result = {}
    Given json Application_Details = active_tpp.get(restRole)
    When call apiApp.configureSSL(Application_Details)

    Given set Application_Details $.code = AuthCode
    And set Application_Details $.request_method = 'POST'
    And set Application_Details $.Content_type = "application/x-www-form-urlencoded"
    And set Application_Details $.grant_type = "authorization_code"
    When call apiApp.Access_Token_ACG(Application_Details)

    * def response = karate.pretty(apiApp.Access_Token_ACG.response)

  # Get GetMultiAccountInfo - parameters -> env = Environment;  AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/GetMultiAccountInfo')&& methodIs('get')
     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Multi_Account_Information(Application_Details)
    And def statusCode = apiApp.Multi_Account_Information.response
    #Then def response = karate.pretty(Result.MultiAccountInfo.Output.response)
    * def response = karate.pretty(Result)

  # Get SingleAccountInfo - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/SingleAccountInfo')&& methodIs('get')

     # Get the data from request and configuree
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get Access Token
    Given json Application_Details = active_tpp.get(restRole)

    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Account_Information(Application_Details)
    And def statusCode = apiApp.Account_Information.response
    #Then def response = karate.pretty(Result.Account_Information.Output.response)
    * def response = karate.pretty(Result)

  # Get SingleAccountBalance - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/SingleAccountBalance')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
      And def restRole = requestParams.role[0]
      And def AccessToken = requestParams.AccessToken[0]
      And def AccountID = requestParams.AccountID[0]
      And def CMARel = requestParams.CMARel[0]
      And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
     And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
      And call apiApp.configureSSL(Application_Details)
      And set Application_Details $.TPPRole = restRole
      And set Application_Details.request_method = 'GET'
      And set Application_Details $.access_token = AccessToken
      And set Application_Details $.token_type = 'Bearer'
      And set Application_Details $.AccountId = AccountID
      And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
      * print Application_Details
    When call apiApp.Account_Balance(Application_Details)
      And def statusCode = apiApp.Account_Balance.response
    Then def response = karate.pretty(Result)

  # Get SingleAccountTransaction - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/SingleAccountTransaction')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Account_Transaction(Application_Details)
    And def statusCode = apiApp.Account_Transaction.response
    Then def response = karate.pretty(Result)

  # Get Product - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/Product')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Account_Product(Application_Details)
    And def statusCode = apiApp.Account_Product.response
    Then def response = karate.pretty(Result)

  # Get StandingOrder - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/StandingOrder')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Account_Standing_Orders(Application_Details)
    And def statusCode = apiApp.Account_Standing_Orders.response
    Then def response = karate.pretty(Result)

  # Get CustBeneficiaries - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/CustBeneficiaries')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Customer_Beneficiaries(Application_Details)
    And def statusCode = apiApp.Customer_Beneficiaries.response
    Then def response = karate.pretty(Result)

  # Get SingleAccountDirectDebits - parameters -> env = Environment; AccountID = ValidAccountID AccessToken =  ActiveAccessTokenn ; role = TPP role ; CMARel = CMA Release
  Scenario: pathMatches('/orangeFMW/psd2_cma/SingleAccountDirectDebits')&& methodIs('get')

     # Get the data from request and configure
    Given def restenv = requestParams.env[0]
    And def restRole = requestParams.role[0]
    And def AccessToken = requestParams.AccessToken[0]
    And def AccountID = requestParams.AccountID[0]
    And def CMARel = requestParams.CMARel[0]
    And eval karate.properties['CMA_Rel_Ver'] = CMARel
    When call read('classpath:karate-config.js')
    Then def apiApp = new apiapp()
    And json Result = {}

    #BDD for get SingleAccountBalance
    Given json Application_Details = active_tpp.get(restRole)
    And call apiApp.configureSSL(Application_Details)
    And set Application_Details $.TPPRole = restRole
    And set Application_Details.request_method = 'GET'
    And set Application_Details $.access_token = AccessToken
    And set Application_Details $.token_type = 'Bearer'
    And set Application_Details $.AccountId = AccountID
    And set Application_Details $.Content_type = 'application/x-www-form-urlencoded'
    * print Application_Details
    When call apiApp.Account_Direct_Debits(Application_Details)
    And def statusCode = apiApp.Account_Direct_Debits.response
    Then def response = karate.pretty(Result)

  # Test
  Scenario: pathMatches('/orangeFMW/psd2_cma/test')&& methodIs('get')
    * def response = requestParams.permission[0]

  # Sample for send any binary as a response
  Scenario: pathMatches('/v1/binary/download')
    * def responseHeaders = { 'Content-Type': 'application/octet-stream', 'Content-Disposition': 'attachment; filename="output.pdf"'}
    * def Utils = Java.type('CMA_Release.Java_Lib.PdfToByteArray')
    * def response = Utils.getByteArray()

  # Sample for running a test case
  Scenario: pathMatches('/orangeFMW/psd2_cma/testcaserun') && methodIs('get')
    * def testcase =  Java.type('CMA_Release.Test_Set.AISP_OIDC.CCToken.fullrun')
    * def r = testcase.test1()
    * def response = "Test Run Completed"
