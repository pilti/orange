Feature: This feature to update Jenkins.

  Background:
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def basicAuth = read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js")

    Given def jenkinsServerUrl =  'http://tcoecbs-w7-v007.boigroup.net:8080/'
    When def Jenkinsuser = 'admin'
    And def password = 'Mother123'
    Then def auth = call basicAuth {username: '#(Jenkinsuser)', password: '#(password)'}

  Scenario Outline: Create Job(s) in Jenkins Server (using config.xml created for Pipeline seedJob)
    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA/job/03_preProd'

    #Get current job config.xml from Jenkins and update config.xml tag details
    Given url jenkinsServerUrl + 'job/' + folderName +'/job/' + jobName + '/'+ 'config.xml'
    And header Authorization = auth
    When method GET
    Then match responseStatus == 200
    And def configfile = response
    And set configfile <TagPathtoUpdate> = '<newvalue>' + ' for ' + <JobName>
    And print karate.prettyXml(configfile)

    # Send updated job config.xml to Jenkins
    Given url jenkinsServerUrl + 'job/' + folderName +'/job/' + jobName + '/'+ 'config.xml'
    And header Authorization = auth
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200


  @AISP
    Examples:
      | JobName                              | TagPathtoUpdate              | newvalue                                 |
      | '00A_AISP_Sanity'                    | /flow-definition/description | Auto Generated job from Orange Framework |
      | '00B_AISP_Regression'                | /flow-definition/description | Auto Generated job from Orange Framework |
      | '001_AISP_Full'                      | /flow-definition/description | Auto Generated job from Orange Framework |
      | '002_AISP_CC_Token'                  | /flow-definition/description | Auto Generated job from Orange Framework |
      | '003_AISP_AccountRequest'            | /flow-definition/description | Auto Generated job from Orange Framework |
      | '004_AISP_PreAuth'                   | /flow-definition/description | Auto Generated job from Orange Framework |
      | '005_AISP_SCA'                       | /flow-definition/description | Auto Generated job from Orange Framework |
      | '006_AISP_Consent'                   | /flow-definition/description | Auto Generated job from Orange Framework |
      | '007_AISP_AuthTokenAndRefreshToken'  | /flow-definition/description | Auto Generated job from Orange Framework |
      | '008_AISP_AuthTokenFromRefreshToken' | /flow-definition/description | Auto Generated job from Orange Framework |
      | '009_AISP_MultiAccountInfo'          | /flow-definition/description | Auto Generated job from Orange Framework |
      | '010_AISP_SingleAccountInfo'         | /flow-definition/description | Auto Generated job from Orange Framework |
      | '011_AISP_AccountBalance'            | /flow-definition/description | Auto Generated job from Orange Framework |
      | '012_AISP_AccountTransactions'       | /flow-definition/description | Auto Generated job from Orange Framework |
      | '013_AISP_DirectDebits'              | /flow-definition/description | Auto Generated job from Orange Framework |
      | '014_AISP_StandingOrders'            | /flow-definition/description | Auto Generated job from Orange Framework |
      | '015_AISP_AccountProducts'           | /flow-definition/description | Auto Generated job from Orange Framework |
      | '016_AISP_Beneficiaries'             | /flow-definition/description | Auto Generated job from Orange Framework |
      | '017_AISP_RefreshtokenRenewal'       | /flow-definition/description | Auto Generated job from Orange Framework |

  @PISP
    Examples:
      | JobName               | TagPathtoUpdate              | newvalue                                 |
      | '00A_PISP_Sanity'     | /flow-definition/description | Auto Generated job from Orange Framework |
      | '00B_PISP_Regression' | /flow-definition/description | Auto Generated job from Orange Framework |
      | '001_PISP_Full'       | /flow-definition/description | Auto Generated job from Orange Framework |
      | '002_PISP_CC_Token'   | /flow-definition/description | Auto Generated job from Orange Framework |
      | '003_PISP_Initiation' | /flow-definition/description | Auto Generated job from Orange Framework |
      | '004_PISP_PreAuth'    | /flow-definition/description | Auto Generated job from Orange Framework |
      | '005_PISP_SCA'        | /flow-definition/description | Auto Generated job from Orange Framework |
      | '006_PISP_Consent'    | /flow-definition/description | Auto Generated job from Orange Framework |
      | '007_PISP_AuthToken'  | /flow-definition/description | Auto Generated job from Orange Framework |
      | '008_PISP_Submission' | /flow-definition/description | Auto Generated job from Orange Framework |

  @TPP
    Examples:
      | JobName              | TagPathtoUpdate              | newvalue                                 |
      | '00A_TPP_Sanity'     | /flow-definition/description | Auto Generated job from Orange Framework |
      | '00B_TPP_Regression' | /flow-definition/description | Auto Generated job from Orange Framework |
      | '001_TPP_Full'       | /flow-definition/description | Auto Generated job from Orange Framework |
