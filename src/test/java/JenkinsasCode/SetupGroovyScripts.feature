Feature: Groovy scripts


  Scenario Outline: Create Groovy script(s)

    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA/job/01_SIT'

    Given def configfile = read("classpath:Resources/Jenkins/seedJob.xml")
    When def PipelineScriptPath = <GroovyScriptPath>
    Then set configfile  /flow-definition/definition/scriptPath = PipelineScriptPath

    Given url jenkinsServerUrl+ '/job/' + folderName +'/' + 'createItem'
    And header Authorization = auth
    And param name = jobName
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200

  @AISP
    Examples:
     | JobName                              | GroovyScriptPath                           |
#      | '00A_AISP_Sanity'                    | 'JF/AISP_Sanity.groovy'                    |
#      | '00B_AISP_Regression'                | 'JF/AISP_Regression.groovy'                |
#      | '001_AISP_Full'                      | 'JF/AISP_Full.groovy'                      |
#      | '002_AISP_CC_Token'                  | 'JF/AISP_CC_Token.groovy'                  |
      | '003_AISP_AccountRequest'            | 'JF/AISP_AccountRequest_R2.groovy'            |
#      | '004_AISP_PreAuth'                   | 'JF/AISP_PreAuth.groovy'                   |
#      | '005_AISP_SCA'                       | 'JF/AISP_SCA.groovy'                       |
#      | '006_AISP_Consent'                   | 'JF/AISP_Consent.groovy'                   |
#      | '007_AISP_AuthTokenAndRefreshToken'  | 'JF/AISP_AuthTokenAndRefreshToken.groovy'  |
#      | '008_AISP_AuthTokenFromRefreshToken' | 'JF/AISP_AuthTokenFromRefreshToken.groovy' |
      | '009_AISP_MultiAccountInfo'          | 'JF/AISP_MultiAccountInfo_R2.groovy'          |
      | '010_AISP_SingleAccountInfo'         | 'JF/AISP_SingleAccountInfo_R2.groovy'         |
#      | '011_AISP_AccountBalance'            | 'JF/AISP_AccountBalance.groovy'            |
#      | '012_AISP_AccountTransactions'       | 'JF/AISP_AccountTransactions.groovy'       |
#      | '013_AISP_DirectDebits'              | 'JF/AISP_DirectDebits.groovy'              |
#      | '014_AISP_StandingOrders'            | 'JF/AISP_StandingOrders.groovy'            |
      | '015_AISP_AccountProducts'           | 'JF/AISP_AccountProducts_R2.groovy'           |
     | '016_AISP_Beneficiaries'             | 'JF/AISP_Beneficiaries_R2.groovy'             |
#      | '017_AISP_RefreshtokenRenewal'       | 'JF/AISP_RefreshtokenRenewal.groovy'       |
#
#  @PISP
#    Examples:
#      | JobName               | GroovyScriptPath            |
#      | '00A_PISP_Sanity'     | 'JF/AISP_Sanity.groovy'     |
#      | '00B_PISP_Regression' | 'JF/AISP_Regression.groovy' |
#      | '001_PISP_Full'       | 'JF/PISP_Full.groovy'       |
#      | '002_PISP_CC_Token'   | 'JF/PISP_CC_Token.groovy'   |
#      | '003_PISP_Initiation' | 'JF/PISP_Initiation.groovy' |
#      | '004_PISP_PreAuth'    | 'JF/PISP_PreAuth.groovy'    |
#      | '005_PISP_SCA'        | 'JF/PISP_SCA.groovy'        |
#      | '006_PISP_Consent'    | 'JF/PISP_Consent.groovy'    |
#      | '007_PISP_AuthToken'  | 'JF/PISP_AuthToken.groovy'  |
#      | '008_PISP_Submission' | 'JF/PISP_Submission.groovy' |
#
#  @TPP
#    Examples:
#      | JobName              | GroovyScriptPath            |
#      | '00A_TPP_Sanity'     | 'JF/AISP_Sanity.groovy'     |
#      | '00B_TPP_Regression' | 'JF/AISP_Regression.groovy' |
#      | '001_TPP_Full'       | 'JF/TPP_Full.groovy'        |
