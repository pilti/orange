#Input required for this file is

Feature: This Feature is to create groovy ip file for jobs.

  Scenario Outline: Create Groovy input file for jobs

    Given string template = read('Template_Script.txt')
    When def template = template.replace("<Name_of_the_test>",<TestName>)
    * def template = template.replace("<Name_of_the_test>",<TestName>)
    * def template = template.replace("<Runner_Class_name>",<Runner>)
    Then def writefile = Java.type('CMA_Release.Java_Lib.TextFileWriting').FileOP(<GroovyScriptPath>,template)

#    Examples:
#      | JobName           | GroovyScriptPath             | Runner          | TestName    |
#      | '00A_AISP_Sanity' | 'JF/AISP_Sanity_Demo.groovy' | 'AISP - Sanity' | 'ActiveEnv' |

  @AISP
    Examples:
      | JobName                              | GroovyScriptPath                           | Runner                                       | TestName                         |
#      | '00A_AISP_Sanity'                    | 'JF/AISP_Sanity_old.groovy'                |                                              |                                  |
##      | '00B_AISP_Regression'                | 'JF/AISP_Regression.groovy'                |                                            |                             |
##      | '001_AISP_Full'                      | 'JF/AISP_Full.groovy'                      |                                            |                             |
#      | '002_AISP_CC_Token'                  | 'JF/AISP_CC_Token.groovy'                  | 'Runner_AISP_CCTokens_Regression'            | 'AISP CCTokens Regression'       |
#      | '003_AISP_AccountRequest_R2'            | 'JF/AISP_AccountRequest_R2.groovy'            | 'Runner_AISP_AccReq_R2'           | 'AISP_AccReq_CMA2.0'      |
#      | '004_AISP_PreAuth'                   | 'JF/AISP_PreAuth.groovy'                   | 'Runner_AISP_PreAuth_Regression'             | 'PreAuth_Regression'             |
#      | '005_AISP_SCA'                       | 'JF/AISP_SCA.groovy'                       | 'Runner_AISP_SCA_Regression'                 | 'SCA_Regression'                 |
#      | '006_AISP_Consent'                   | 'JF/AISP_Consent.groovy'                   | 'Runner_AISP_Consent'                        | 'AISP_Consent'                   |
#      | '007_AISP_AuthTokenAndRefreshToken'  | 'JF/AISP_AuthTokenAndRefreshToken.groovy'  | 'Runner_AISP_AuthToken_Regression'           | 'AuthToken_Regression'           |
#      | '008_AISP_AuthTokenFromRefreshToken' | 'JF/AISP_AuthTokenFromRefreshToken.groovy' | 'Runner_AISP_RefToken_Regression'            | 'RefToken_Regression'            |
#      | '009_AISP_MultiAccountInfo_R2'          | 'JF/AISP_MultiAccountInfo_R2.groovy'          | 'Runner_AISP_MultiAccoutInfo_R2'     | 'MultiAccoutInfo_CMA2.0'     |
#      | '010_AISP_SingleAccountInfo_R2'         | 'JF/AISP_SingleAccountInfo_R2.groovy'         | 'Runner_AISP_SingleAccoutInfo_R2'    | 'SingleAccoutInfo_CMA2.0'    |
##      | '011_AISP_AccountBalance'            | 'JF/AISP_AccountBalance.groovy'            | 'Runner_AISP_AccBalance_Regression'          | 'AccBalance_Regression'          |
##      | '012_AISP_AccountTransactions'       | 'JF/AISP_AccountTransactions.groovy'       | 'Runner_AISP_Transactions_Regression'        | 'Transactions_Regression'        |
##      | '013_AISP_DirectDebits'              | 'JF/AISP_DirectDebits.groovy'              | 'Runner_AISP_DirectDebits_Regression'        | 'DirectDebits_Regression'        |
      | '014_AISP_StandingOrders_R2'            | 'JF/AISP_StandingOrders_R2.groovy'            | 'Runner_AISP_StandingOrder_R2'       | 'StandingOrder_CMA2.0'       |
#     | '015_AISP_AccountProducts_R2'           | 'JF/AISP_AccountProducts_R2.groovy'           | 'Runner_AISP_Products_R2'            | 'Products_CMA2.0'            |
#     | '016_AISP_Beneficiaries_R2'             | 'JF/AISP_Beneficiaries_R2.groovy'             | 'Runner_AISP_Beneficiaries_R2'       | 'Beneficiaries_CMA2.0'       |
##      | '017_AISP_RefreshtokenRenewal'       | 'JF/AISP_RefreshtokenRenewal.groovy'       | 'Runner_AISP_RefreshTokenRenewal_Regression' | 'RefreshTokenRenewal_Regression' |


