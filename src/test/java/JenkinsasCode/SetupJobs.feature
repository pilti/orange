Feature: This feature is to create jenkins Folder, sub Folders and Jenkins Jobs for PSD2 CMA R1

  Background:
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def basicAuth = read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js")

   Given def jenkinsServerUrl =  'http://tcoecbs-w7-v007.boigroup.net:8080/'
   # Given def jenkinsServerUrl =  'http://localhost:8080/'
    When def Jenkinsuser = 'Umesh'
    And def password = 'Mother123'
    Then def auth = call basicAuth {username: '#(Jenkinsuser)', password: '#(password)'}

  Scenario Outline: Create new Folder in Jenkins Server(using config.xml created for Folder Job (folder plugin))
    * def configfile = read("<configFilepath>")

    Given url jenkinsServerUrl+ '/' + 'createItem'
    And header Authorization = auth
    And param name = <FolderName>
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200

    Examples:
      | FolderName | configFilepath                         |
      | 'PSD2_CMA_R2' | classpath:Resources/Jenkins/seedJobFolder.xml |


  Scenario Outline: Create new sub Folders in Jenkins Server(using config.xml created for Folder Job (folder plugin))
    * def configfile = read("<configFilepath>")

    Given url jenkinsServerUrl + '/job/' + <path> + '/' + 'createItem'
    And header Authorization = auth
    And param name = <FolderName>
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200
    Examples:
      | path       | FolderName   | configFilepath                         |
      | 'PSD2_CMA_R2' | '01_SIT'     | classpath:Resources/Jenkins/seedJobFolder.xml |
#      | 'PSD2_CMA_R1' | '02_UAT'     | classpath:Resources/Jenkins/seedJobFolder.xml |
#      | 'PSD2_CMA_R1' | '03_preProd' | classpath:Resources/Jenkins/seedJobFolder.xml |


  Scenario Outline: Create Job(s) in Jenkins Server (using config.xml created for Pipeline seedJob)
    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA_R2/job/01_SIT'

    Given def configfile = read("classpath:Resources/Jenkins/seedJobPipline.xml")
##    And  def seedjobdescription = configfile/flow-definition/description
##    Then set configfile /flow-definition/description = seedjobdescription + ' for ' + '<JobName>'
    And set configfile  /flow-definition/definition/scriptPath = <GroovyScriptPath>

    Given url jenkinsServerUrl+ '/job/' + folderName +'/' + 'createItem'
    And header Authorization = auth
    And param name = jobName
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200

#    * def jobName = <JobName>
#    * def folderName = 'PSD2_CMA_R1/job/02_UAT'
#
#    Given def configfile = read("classpath:Resources/Jenkins/seedJobPipline.xml")
##    When def PipelineScriptPath = <GroovyScriptPath>
##    And  def seedjobdescription = configfile.flow-definition.description
##    Then set configfile /flow-definition/description = seedjobdescription + ' for ' + '<JobName>'
#    And set configfile  /flow-definition/definition/scriptPath = <GroovyScriptPath>
#
#    Given url jenkinsServerUrl+ '/job/' + folderName +'/' + 'createItem'
#    And header Authorization = auth
#    And param name = jobName
#    And header Content-Type = 'application/xml'
#    And request configfile
#    When method POST
#    Then match responseStatus == 200
#
#    * def jobName = <JobName>
#    * def folderName = 'PSD2_CMA_R1/job/03_preProd'
#
#    Given def configfile = read("classpath:Resources/Jenkins/seedJobPipline.xml")
###    And  def seedjobdescription = configfile.flow-definition.description
###    Then set configfile /flow-definition/description = seedjobdescription + ' for ' + '<JobName>'
#    And set configfile  /flow-definition/definition/scriptPath = <GroovyScriptPath>
#
#    Given url jenkinsServerUrl+ '/job/' + folderName +'/' + 'createItem'
#    And header Authorization = auth
#    And param name = jobName
#    And header Content-Type = 'application/xml'
#    And request configfile
#    When method POST
#    Then match responseStatus == 200

  @AISP
    Examples:
      | JobName                              | GroovyScriptPath                           |
###      | '00A_AISP_Sanity'                    | 'JF/AISP_Sanity.groovy'                    |
###      | '00B_AISP_Regression'                | 'JF/AISP_Regression.groovy'                |
###      | '001_AISP_Full'                      | 'JF/AISP_Full.groovy'                      |
##      | '002_AISP_CC_Token'                  | 'JF/AISP_CC_Token.groovy'                  |
#      | '003_AISP_AccountRequest'            | 'JF/AISP_AccountRequest_R2.groovy'            |
##      | '004_AISP_PreAuth'                   | 'JF/AISP_PreAuth.groovy'                   |
##      | '005_AISP_SCA'                       | 'JF/AISP_SCA.groovy'                       |
##      | '006_AISP_Consent'                   | 'JF/AISP_Consent.groovy'                   |
##      | '007_AISP_AuthTokenAndRefreshToken'  | 'JF/AISP_AuthTokenAndRefreshToken.groovy'  |
##      | '008_AISP_AuthTokenFromRefreshToken' | 'JF/AISP_AuthTokenFromRefreshToken.groovy' |
#      | '009_AISP_MultiAccountInfo'          | 'JF/AISP_MultiAccountInfo_R2.groovy'          |
#      | '010_AISP_SingleAccountInfo'         | 'JF/AISP_SingleAccountInfo_R2.groovy'         |
##      | '011_AISP_AccountBalance'            | 'JF/AISP_AccountBalance.groovy'            |
##      | '012_AISP_AccountTransactions'       | 'JF/AISP_AccountTransactions.groovy'       |
##      | '013_AISP_DirectDebits'              | 'JF/AISP_DirectDebits.groovy'              |
      | '014_AISP_StandingOrders'            | 'JF/AISP_StandingOrders_R2.groovy'            |
#      | '015_AISP_AccountProducts'           | 'JF/AISP_AccountProducts_R2.groovy'           |
#      | '016_AISP_Beneficiaries'             | 'JF/AISP_Beneficiaries_R2.groovy'             |
##      | '017_AISP_RefreshtokenRenewal'       | 'JF/AISP_RefreshtokenRenewal.groovy'       |

#  @PISP
#    Examples:
#      | JobName               | GroovyScriptPath            |
#      | '00A_PISP_Sanity'     | 'JF/PISP_Sanity.groovy'     |
#      | '00B_PISP_Regression' | 'JF/PISP_Regression.groovy' |
#      | '001_PISP_Full'       | 'JF/PISP_Full.groovy'       |
#      | '002_PISP_CC_Token'   | 'JF/PISP_CC_Token.groovy'   |
#      | '003_PISP_Initiation' | 'JF/PISP_Initiation.groovy' |
#      | '004_PISP_PreAuth'    | 'JF/PISP_PreAuth.groovy'    |
#      | '005_PISP_SCA'        | 'JF/PISP_SCA.groovy'        |
#      | '006_PISP_Consent'    | 'JF/PISP_Consent.groovy'    |
#      | '007_PISP_AuthToken'  | 'JF/PISP_AuthToken.groovy'  |
#      | '008_PISP_Submission' | 'JF/PISP_Submission.groovy' |
#
#  @TPP
#    Examples:
#      | JobName              | GroovyScriptPath           |
#      | '00A_TPP_Sanity'     | 'JF/TPP_Sanity.groovy'     |
#      | '00B_TPP_Regression' | 'JF/TPP_Regression.groovy' |
#      | '001_TPP_Full'       | 'JF/TPP_Full.groovy'       |
