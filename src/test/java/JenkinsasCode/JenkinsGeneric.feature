Feature: This feature is to access/update Jenkins details through webservice API

  Background:
    * def fileWriter = Java.type('CMA_Release.Java_Lib.CreateFiles')
    * def jenkinsServerUrl =  'http://jenkins.boigroup.net/'
    * def Jenkinsuser = 'C961818'
    * def password = 'Mother-1234'
    * def auth = call read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js") {username: '#(Jenkinsuser)', password: '#(password)'}

  Scenario: Get Jenkins Job list
    Given url jenkinsServerUrl + 'api/json?pretty=true'
    And header Authorization = auth
    When method GET
    Then match responseStatus == 200
    Then print karate.pretty(response.jobs)

  Scenario: Get Job's config xml
    * def jobName = "seedPipline"

    Given url jenkinsServerUrl + '/job/Test_Lab/job/' + jobName + '/config.xml'
    And header Authorization = auth
    When method GET
    Then print karate.prettyXml(response)
    Then call fileWriter.write('./src/test/java/Resources/Jenkins/seedJob.xml',karate.prettyXml(response))

  Scenario Outline: Build/Start Job(s) in Jenkins Server
    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA'

    Given url jenkinsServerUrl+ '/job/' +  jobName + '/build'
    And header Authorization = auth
    And request "Dummy"
    When method POST
    Then match responseStatus == 201

    Examples:
      | JobName           |
      | 'PISP_SanityTest' |
      | 'AISP_SanityTest' |

  Scenario Outline: Build/Start Job(s) in Jenkins Server with parameters
    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA'
    * def E = <Environment>

    Given url jenkinsServerUrl+ '/job/' + jobName + '/buildWithParameters'
    And header Authorization = auth
    And param Env = E
    And request "Dummy"
    When method POST
    Then match responseStatus == 201

    Examples:
      | JobName           | Environment |
      | 'PISP_SanityTest' | 'SIT'       |
      | 'AISP_SanityTest' | 'SIT'       |


  Scenario Outline: Update Jobs in Jenkins Server (using config.xml)
    * def jobName = <JobName>
    * def folderName = 'PSD2_CMA'

    #Get current job config.xml from Jenkins and update config.xml tag details
    Given url jenkinsServerUrl + 'job/' + folderName +'/job/' + jobName + '/'+ 'config.xml'
    And header Authorization = auth
    When method GET
    Then match responseStatus == 200
    And def configfile = response
    And set configfile <TagPathtoUpdate> = <newvalue>
    And print karate.prettyXml(configfile)

    # Send updated job config.xml to Jenkins
    Given url jenkinsServerUrl + 'job/' + folderName +'/job/' + jobName + '/'+ 'config.xml'
    And header Authorization = auth
    And header Content-Type = 'application/xml'
    And request configfile
    When method POST
    Then match responseStatus == 200

    Examples:
      | JobName    | TagPathtoUpdate              | newvalue                                    |
   #   | 'AISP_Consent' | /flow-definition/description | 'This is seedJob for Module level Updated2' |
      | 'CC_Token' | /flow-definition/description | 'This is seedJob for Module level Updated2' |

  Scenario Outline: Delete Jobs
    * def jobName = <JobName>

    Given url jenkinsServerUrl + 'job/PSD2_CMA/job/'+ jobName + '/doDelete'
    And header Authorization = auth
    And request 'Dummy'
    When method POST
    Then match responseStatus == 200
    Examples:
      | JobName                    | GroovyScriptPath                     |
      | 'AISP_Sanity'              | 'JF/AISP_Sanity.groovy'              |
      | 'PISP_Sanity'              | 'JF/PISP_Sanity.groovy'              |


  Scenario: Get Job's config xml
    * def jobName = "AISP_SanityTest"

    Given url jenkinsServerUrl + '/job/' + jobName + '/config.xml'
    And header Authorization = auth
    When method GET
    Then print karate.prettyXml(response)
    Then call fileWriter.write('./src/test/java/Resources/Jenkins/seedJob.xml',karate.prettyXml(response))

