function(){

     var env = karate.env;
     karate.log("Environment requested :" ,env)
     var default_env = 'preProd';
     if (typeof restenv == 'undefined'){restenv = null};
     if (env == null){env = restenv};
     if (env == null){env = default_env};
     karate.log("Environment for the test : ", env);

     var CMA_Rel_Ver = karate.properties['CMA_Rel_Ver']
     if (CMA_Rel_Ver == null){CMA_Rel_Ver = '2.0'};
     karate.log("CMA version selected : " ,CMA_Rel_Ver)
     karate.properties['CMA_Rel_Ver'] = CMA_Rel_Ver

     var webbrowser = karate.properties['webbrowser']
     karate.log("Browser requested : " ,webbrowser)
     var default_browser = 'Chrome';
     if (webbrowser == null){webbrowser = default_browser};
     karate.log("Browser for the test" ,webbrowser)

     var B365_User = karate.properties['B365_User']
     karate.log("Generic B365_User requested : " ,B365_User)
     var default_B365_User = '139694';
     if (B365_User == null){B365_User = default_B365_User};
     karate.log("B365_User for the test" ,B365_User)

     var B365_OTP = karate.properties['B365_OTP']
     karate.log("Generic B365_OTP requested :" ,B365_OTP)
     var default_B365_OTP = '123456';
     if (B365_OTP == null){B365_OTP = default_B365_OTP};
     karate.log("B365_OTP for the test" ,B365_OTP)

     var screenlogs = karate.properties['screenlogs']
     karate.log("Selenium screenlogs flag requested: " , screenlogs)
     var default_screenlogs = 'Yes';
     if (screenlogs == null){karate.properties['screenlogs'] = default_screenlogs};
     karate.log("Selenium screenlogs flag for the test " , screenlogs)

     var selenium_grid = karate.properties['selenium_grid']
     karate.log("Selenium grid requested" , selenium_grid)
     var default_selenium_grid = 'No';
     if (selenium_grid == null){karate.properties['selenium_grid'] = default_selenium_grid};
     karate.log("Selenium grid set " , selenium_grid)

     var SeleniumHubUrl = karate.properties['SeleniumHubUrl']
     var default_SeleniumHubUrl = 'http://10.64.103.231:4444//wd/hub';
     if (SeleniumHubUrl == null){karate.properties['SeleniumHubUrl'] = default_SeleniumHubUrl};
     karate.log("Selenium Grid URL " , SeleniumHubUrl)

    var accNumber = "90204761412682"

    if(CMA_Rel_Ver == '1.1'){
        var SchemaAccountRequest = karate.read('classpath:Resources/API_Schemas/CMA_1-1/AccountRequest.json');
        var SchemaStandingOrder = karate.read('classpath:Resources/API_Schemas/CMA_1-1/StandingOrder.json');
        var SchemaCCToken = karate.read('classpath:Resources/API_Schemas/CMA_1-1/CCToken.json');
        var SchemaAccessTokenUsingACG = karate.read('classpath:Resources/API_Schemas/CMA_1-1/AccessTokenUsingACG.json');
        var SchemaBalance = karate.read('classpath:Resources/API_Schemas/CMA_1-1/Balance.json');
         var SchemaBeneficiary = karate.read('classpath:Resources/API_Schemas/CMA_1-1/Beneficiary.json');
         var SchemaDirectDebit = karate.read('classpath:Resources/API_Schemas/CMA_1-1/DirectDebit.json');
         var SchemaMultiAccInfo = karate.read('classpath:Resources/API_Schemas/CMA_1-1/MultiAccountInfo.json');
         var SchemaSingAccInfo = karate.read('classpath:Resources/API_Schemas/CMA_1-1/SingAccountInfo.json');
        // var SchemaProducts = karate.read('classpath:Resources/API_Schemas/CMA_1-1/Products.json');

        }
    if(CMA_Rel_Ver == '2.0'){
               var SchemaAccountRequest = karate.read('classpath:Resources/API_Schemas/CMA_2-0/AccountRequest.json');
               var SchemaStandingOrder = karate.read('classpath:Resources/API_Schemas/CMA_2-0/StandingOrder.json');
               var SchemaBeneficiary = karate.read('classpath:Resources/API_Schemas/CMA_2-0/Beneficiary.json');
               var SchemaCCToken = karate.read('classpath:Resources/API_Schemas/CMA_2-0/CCToken.json');
               var SchemaMultiAccInfo = karate.read('classpath:Resources/API_Schemas/CMA_2-0/MultiAccountInfo.json');
               var SchemaSingAccInfo = karate.read('classpath:Resources/API_Schemas/CMA_2-0/SingAccountInfo.json');
               var SchemaAccessTokenUsingACG = karate.read('classpath:Resources/API_Schemas/CMA_2-0/AccessTokenUsingACG.json');
               var SchemaBalance = karate.read('classpath:Resources/API_Schemas/CMA_2-0/Balance.json');
               var SchemaDirectDebit = karate.read('classpath:Resources/API_Schemas/CMA_2-0/DirectDebit.json');
               var SchemaTransactions = karate.read('classpath:Resources/API_Schemas/CMA_2-0/Transactions.json');
            //   var SchemaProducts = karate.read('classpath:Resources/API_Schemas/CMA_2-0/Products.json');

    }
  if (env == 'PIT') {
        var apiServer = 'https://api.int.apiezy.com';
        var fsServer = 'http://192.168.68.81:1414';
        var DPUrl = 'https://developer.boicloudtest.net';
        var TPPHubUrl= 'https://tpp.int.apiezy.com/tppportalv1/tppportal/';
        var OBUrl= 'https://manager.mit.openbanking.qa';
        var authServer = 'https://auth.int.apiezy.com';
        var db_details = karate.read('classpath:Resources/DB_Details/ST.json');
        var tpp_details = karate.read('classpath:Resources/Active_TPP/PIT_TPP.json');
        var user_details = karate.read('classpath:Resources/Active_B365_User/user_details_PIT.json');
         //karate.configure('proxy','http://webproxy.boigroup.net:8080');
        //karate.configure('proxy','http://ADSAWS.boigroup.net:8080');
    }

  if (env == 'SIT') {
      //var apiServer = 'https://api.boitest.net';
      var apiServer = 'https://api.sit3.obapi.boitest.net';
     //var fsServer = 'http://10.64.103.231:49512';
      var fsServer = 'http://192.168.68.81:1414';
      var DPUrl = 'https://developer.boicloudtest.net';
      //var TPPHubUrl= 'https://tpp.boitest.net/tppportalv1/tppportal/';
      var TPPHubUrl= 'https://tpp.sit3.obapi.boitest.net/tppportalv1/tppportal/';
      var OBUrl= 'https://manager.mit.openbanking.qa';
      var authServer = 'https://auth.boitest.net';
      var db_details = karate.read('classpath:Resources/DB_Details/SIT.json');
      var tpp_details = karate.read('classpath:Resources/Active_TPP/SIT_TPP.json');
      //var baseUrl = 'https://tpp.boitest.net';
      var baseUrl = 'https://tpp.sit3.obapi.boitest.net';
      var user_details = karate.read('classpath:Resources/Active_B365_User/user_details_SIT.json');
  }

  if (env == 'UAT') {
      var apiServer = 'https://api.u2.psd2.boitest.net';
      var fsServer = 'http://192.168.68.81:1414';
      var DPUrl = 'https://developer.boicloudtest.net';
      var TPPHubUrl= 'https://tpp.u2.psd2.boitest.net/tppportalv1/tppportal/';
      var OBUrl= 'https://manager.mit.openbanking.qa';
      var authServer = 'https://auth.u2.psd2.boitest.net';
      var db_details = karate.read('classpath:Resources/DB_Details/UAT.json');
      var tpp_details = karate.read('classpath:Resources/Active_TPP/UAT_TPP.json');
      var baseUrl = 'https://tpp.u2.psd2.boitest.net';
      var user_details = karate.read('classpath:Resources/Active_B365_User/user_details_UAT.json');
    }

  if (env == 'preProd') {
          var apiServer = 'https://api.prep.openapi.boicloudtest.net';
          //var fsServer = 'http://10.64.103.231:49512';
         var fsServer = 'http://192.168.68.81:1414';
          var DPUrl = 'https://developer.boicloudtest.net';
          var TPPHubUrl= 'https://tpp.prep.openapi.boicloudtest.net/tppportalv1/tppportal/';
          var OBUrl= 'https://manager.mit.openbanking.qa';
          var authServer = 'https://auth.prep.openapi.boicloudtest.net';
          var db_details = karate.read('classpath:Resources/DB_Details/preProd.json');
          var tpp_details = karate.read('classpath:Resources/Active_TPP/preProd_TPP.json');
          var baseUrl = 'https://tpp.u2.psd2.boitest.net';
          var user_details = karate.read('classpath:Resources/Active_B365_User/user_details_preProd.json');
          karate.configure('proxy','http://webproxy.boigroup.net:8080');
      }
      var port = karate.properties['karate.server.port'];
        port = port || '8088';

  var config = {

  // URIs API Product
      TokenUrl: apiServer + '/oauth/as/token.oauth2',
      AcctReqUrl: apiServer + '/1/api/open-banking/v'+ CMA_Rel_Ver + '/account-requests',
      AuthCodeUrl: apiServer +'/openbanking-oauth-authorizev1/authorize',
      AuthUrl: authServer + '/oauth/as/authorization.oauth2?',
      AcctInfoUrl:  apiServer + '/1/api/open-banking/v' + CMA_Rel_Ver + '/accounts/',
      PmtSetupUrl: apiServer + '/1/api/open-banking/v'+ CMA_Rel_Ver + '/payments',
      PmtSubUrl: apiServer + '/1/api/open-banking/v' + CMA_Rel_Ver + '/payment-submissions',
      BulkApiUrl: apiServer + '/1/api/open-banking/v'+ CMA_Rel_Ver,
      authServer: authServer,
      ActiveEnv: env,
      apiServer: apiServer,
  // Developer Portal
      DevPortalUrl: DPUrl,
       CMA_Rel_Ver: CMA_Rel_Ver,
      //B365 User details
      B365_User: B365_User,
      B365_OTP: B365_OTP,
      selenium_grid: selenium_grid,
      SchemaAccountRequest: SchemaAccountRequest,
       SchemaStandingOrder:  SchemaStandingOrder,
       SchemaBeneficiary: SchemaBeneficiary,
       SchemaCCToken: SchemaCCToken,
       SchemaMultiAccInfo: SchemaMultiAccInfo,
       SchemaSingAccInfo: SchemaSingAccInfo,
       SchemaAccessTokenUsingACG: SchemaAccessTokenUsingACG,
       SchemaBalance: SchemaBalance,
       SchemaDirectDebit: SchemaDirectDebit,
       SchemaTransactions: SchemaTransactions,
      // SchemaProducts: SchemaProducts,
      //Selenium screen logs
      screenlogs: screenlogs,
     SeleniumHubUrl: SeleniumHubUrl,

  //Foundation Service URIs
      fsCusProfileUrl: fsServer + '/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/v3/profile/',
      fsCusProfileSingelUrl: fsServer + '/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/v2/',
      fsAcctBalUrl: fsServer + '/fs-abt-service/services/abt/accounts/',
      fsUserBeneficiaryUrl: fsServer + '/fs-abt-service/services/user/',
      fsAcctSOUrl: fsServer + '/fs-abt-service/services/account/',
      fsPreStagePayment: fsServer + '/fs-payment-web-service/services/ValidatePreStagePayment',
      fsValidatePayment: fsServer + '/fs-payment-web-service/services/validatePayment',

  //TPP Hub
      TPPHubUrl: TPPHubUrl,
      OBUrl: OBUrl,
      baseUrl: baseUrl,

  // Global Variables

      permissions: karate.read('classpath:CMA_Release/Base_Data/permission.json'),
      reqHeader: karate.read('classpath:CMA_Release/Base_Data/ReqHeader.json'),
      fs_data: karate.read('classpath:CMA_Release/Base_Data/FS_RequestParameters.json'),
      db_data: karate.read('classpath:CMA_Release/Base_Data/database.json'),
      pisp_reqHeader: karate.read('classpath:CMA_Release/Base_Data/PISP_ReqHeader.json'),
      pisp_payload: karate.read('classpath:CMA_Release/Base_Data/Payment_Setup_Payload.json'),
      PO: Java.type('CMA_Release.Entities_UI.PageObjects.TPPHub.POM_TPPLogin'),
      ReqObjIP: karate.read('classpath:Resources/OB_DIC/ReqObjIP.json'),
      webapp: Java.type('Apps.webapp'),
      default_browser: webbrowser,
      active_tpp: tpp_details,
      user_details: user_details,
      accNumber: accNumber,
      textwriter: Java.type('CMA_Release.Java_Lib.CreateFiles'),
      pdfgen: Java.type('CMA_Release.Java_Lib.PDFGenerator'),
      stmtpath: read("classpath:CMA_Release/JavaScript_Lib/stmt_path.js"),
      basic_auth: read("classpath:CMA_Release/JavaScript_Lib/basic-auth.js"),
      afterScenario: "classpath:CMA_Release/JavaScript_Lib/afterScenario.js",
      apiapp: read("classpath:Apps/apiapp.js"),
      fsapp: read("classpath:Apps/fsapp.js"),
      mockServerUrl: 'http://localhost:' + port + '/v1/',
      RWPropertyFile: Java.type("CMA_Release.Java_Lib.PropertiesFileWritingReading"),

      //Database Connections
      ABT_DB_Conn: db_details.ABT_DB_Conn,
      SP_SHARED_DATA_DB_Conn: db_details.SP_SHARED_DATA_DB_Conn,
      CHANNEL_PROFILE_DB_Conn: db_details.CHANNEL_PROFILE_DB_Conn,
      PSD2_PISP_PID_Conn: db_details.PSD2_PISP_PID_Conn,
      ran: function(){return (java.lang.System.currentTimeMillis())},
   };
   //Global Configurations
      //karate.configure('proxy','http://webproxy.boigroup.net:8080');
      //karate.configure('proxy','http://ADSAWS.boigroup.net:8080');

      karate.configure('connectTimeout', 25000);
      karate.configure('readTimeout', 25000);
      karate.configure('logPrettyResponse', true);
      karate.configure('report', { logEnabled: false, showAllSteps: false })
      // Selinium Grid 10.64.103.231
  return config;
}