package Runners;

import CMA_Release.Java_Lib.JsonTree;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Test;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class Test_Runner_Parallel {

  public static String className;

    @Test
    public void testParallel() {
        String karateOutputPath = "target/surefire-reports";
        KarateStats stats = CucumberRunner.parallel(getClass(), 1, karateOutputPath);
        className = this.getClass().getName();
        generateReport(karateOutputPath, className);

        assertTrue("scenarios failed", stats.getFailCount() == 0);

    }
    private static void generateReport(String karateOutputPath, String className1) {
        String CMA_Ver = System.getProperty("CMA_Rel_Ver");
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        List<String> jsonPaths = new ArrayList(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File("target"),
                "BOI - PSD2 - CMA Release : " + CMA_Ver +", " + className1.substring(className1.lastIndexOf(".")+8));
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }

    @AfterClass
    public static void ExecutionEndTime() throws IOException {
        //System.out.println(className);
        JsonTree.jsonTree(className.substring(className.lastIndexOf(".")+8));
        Date date=java.util.Calendar.getInstance().getTime();
        String datetime = date.toString().replace(" ","_").replace(":","_");
        ZipUtil.pack(new File("target"), new File("target_on_"+datetime+".zip"));
        ZipUtil.pack(new File("target/cucumber-html-reports"), new File("target/Cucumber_Reports_"+datetime+".zip"));
        ZipUtil.pack(new File("target/D3_business_viewer"), new File("target/Bussiness_Reports_"+datetime+".zip"));
        ZipUtil.pack(new File("target/testout"), new File("target/TestOut_"+datetime+".zip"));
    }
}