package junit.junit4;

import com.intuit.karate.cucumber.KarateFeature;
import com.intuit.karate.cucumber.KarateRuntime;

/**
 *
 * @author pthomas3
 */
public class KarateFeatureRunner {

    protected final KarateFeature feature;
    protected final KarateRuntime runtime;

    public KarateFeatureRunner(KarateFeature feature, KarateRuntime runtime) {
        this.feature = feature;
        this.runtime = runtime;
    }

}