package ML_Neuralnetwork.A01_testcase_select;

import ML_Neuralnetwork.AntColonyOpt_Framework;
import ML_Neuralnetwork.ML_model.Data;
import ML_Neuralnetwork.ML_model.NeuralNetwork;
import ML_Neuralnetwork.tools.Utils;

import java.io.BufferedReader;

/**
 * Created by Umesh on 10/8/18.
 */

public class ANN {

    public static void main(String[] args) {
        int numInputs = 7;
        int numOutputs = 1;
        int numTrainCases = 210;
        int numTestCases = 20;
        int repoSize = 20;

        String trainingFileName = "BigData/ant_dataset.dat";
        String testFileName = "BigData/ant_dataset_test.dat";

        BufferedReader trainingFile = Utils.getFileContent(trainingFileName);
        BufferedReader testFile = Utils.getFileContent(testFileName);

        Data trainData = new Data(numInputs, numOutputs, numTrainCases);
        Data testData = new Data(numInputs, numOutputs, numTestCases);

        Utils.parse(trainingFile, trainData);
        Utils.parse(testFile, testData);

        trainData.scaleDown();
        testData.scaleDown();

        int[] nodesPerLayer = new int[3];
        nodesPerLayer[0] = numInputs;
        nodesPerLayer[1] = numInputs;
        nodesPerLayer[2] = numOutputs;
        NeuralNetwork neuralNetwork = new NeuralNetwork(3, numInputs, nodesPerLayer);
        neuralNetwork.dataSet = trainData;

        AntColonyOpt_Framework antColonyOptFramework = new AntColonyOpt_Framework(neuralNetwork, repoSize);

        if (antColonyOptFramework.trainNeuralNet()) {
            System.out.println("******Test Output*****");
            neuralNetwork.dataSet = testData;
            antColonyOptFramework.testNeuralNet();
        }
        else {
            System.exit(0);
        }
    }
}
