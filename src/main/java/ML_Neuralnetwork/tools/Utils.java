package ML_Neuralnetwork.tools;

import ML_Neuralnetwork.ML_model.Data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by Umesh on 10/8/18.
 */

public class Utils {

    public static BufferedReader getFileContent(String fileName) {
        String filePath = "./src/Z_Machine_Learning/BigData/" + fileName;//Utils.class.getClassLoader()
                                    //.getResource(fileName)
                                  //  .getPath();

        BufferedReader fileContent = null;
        try {
            fileContent = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            System.exit(1);
        }

        return fileContent;
    }

    public static void parse(BufferedReader content, Data data) {
        try {
            for (int i = 0; i < data.numCases; i++) {
                String[] line = (content.readLine().split("\\s+"));

                for (int j = 0; j < (data.numInputs + data.numOutputs); j++) {
                    data.data[i][j] = Double.parseDouble(line[j]);

                    if (data.data[i][j] < data.extrema[j][0])
                        data.extrema[j][0] = data.data[i][j];
                    if (data.data[i][j] > data.extrema[j][1])
                        data.extrema[j][1] = data.data[i][j];
                }
            }

            for (int i = 0; i < (data.numInputs + data.numOutputs); i++)
                if (data.extrema[i][0] == data.extrema[i][1])
                    data.extrema[i][1] = data.extrema[i][0] + 1;

        } catch (Exception ex) {
            System.out.println("Error while parsing file!");
        }
    }
}
